﻿namespace Rimed.Tools.BinFileWrapperUI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.panelMain = new System.Windows.Forms.Panel();
            this.labelFileCompareResult = new System.Windows.Forms.Label();
            this.labelVerifyHeaderResult = new System.Windows.Forms.Label();
            this.labelCompareSignatureResult = new System.Windows.Forms.Label();
            this.checkBoxCompareFiles = new System.Windows.Forms.CheckBox();
            this.checkBoxCompareSignature = new System.Windows.Forms.CheckBox();
            this.checkBoxVerifyHeader = new System.Windows.Forms.CheckBox();
            this.checkBoxCreateDescriptionFile = new System.Windows.Forms.CheckBox();
            this.buttonOpenFolder = new System.Windows.Forms.Button();
            this.textBoxTargetFile = new System.Windows.Forms.TextBox();
            this.textBoxResult = new System.Windows.Forms.TextBox();
            this.buttonWrapFile = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.maskedTextBoxVerBuild = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBoxVerRever = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBoxVerMinor = new System.Windows.Forms.MaskedTextBox();
            this.maskedTextBoxVerMajor = new System.Windows.Forms.MaskedTextBox();
            this.labelFileVer = new System.Windows.Forms.Label();
            this.comboBoxFileType = new System.Windows.Forms.ComboBox();
            this.labelFileType = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.textBoxFilePath = new System.Windows.Forms.TextBox();
            this.labelFilePath = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panelMain.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 445);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(694, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // panelMain
            // 
            this.panelMain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelMain.Controls.Add(this.labelFileCompareResult);
            this.panelMain.Controls.Add(this.labelVerifyHeaderResult);
            this.panelMain.Controls.Add(this.labelCompareSignatureResult);
            this.panelMain.Controls.Add(this.checkBoxCompareFiles);
            this.panelMain.Controls.Add(this.checkBoxCompareSignature);
            this.panelMain.Controls.Add(this.checkBoxVerifyHeader);
            this.panelMain.Controls.Add(this.checkBoxCreateDescriptionFile);
            this.panelMain.Controls.Add(this.buttonOpenFolder);
            this.panelMain.Controls.Add(this.textBoxTargetFile);
            this.panelMain.Controls.Add(this.textBoxResult);
            this.panelMain.Controls.Add(this.buttonWrapFile);
            this.panelMain.Controls.Add(this.panel1);
            this.panelMain.Controls.Add(this.labelFileVer);
            this.panelMain.Controls.Add(this.comboBoxFileType);
            this.panelMain.Controls.Add(this.labelFileType);
            this.panelMain.Controls.Add(this.button1);
            this.panelMain.Controls.Add(this.textBoxFilePath);
            this.panelMain.Controls.Add(this.labelFilePath);
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(0, 0);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(694, 445);
            this.panelMain.TabIndex = 3;
            // 
            // labelFileCompareResult
            // 
            this.labelFileCompareResult.AutoSize = true;
            this.labelFileCompareResult.Location = new System.Drawing.Point(187, 206);
            this.labelFileCompareResult.Name = "labelFileCompareResult";
            this.labelFileCompareResult.Size = new System.Drawing.Size(13, 13);
            this.labelFileCompareResult.TabIndex = 21;
            this.labelFileCompareResult.Text = "?";
            // 
            // labelVerifyHeaderResult
            // 
            this.labelVerifyHeaderResult.AutoSize = true;
            this.labelVerifyHeaderResult.Location = new System.Drawing.Point(187, 160);
            this.labelVerifyHeaderResult.Name = "labelVerifyHeaderResult";
            this.labelVerifyHeaderResult.Size = new System.Drawing.Size(13, 13);
            this.labelVerifyHeaderResult.TabIndex = 20;
            this.labelVerifyHeaderResult.Text = "?";
            // 
            // labelCompareSignatureResult
            // 
            this.labelCompareSignatureResult.AutoSize = true;
            this.labelCompareSignatureResult.Location = new System.Drawing.Point(187, 183);
            this.labelCompareSignatureResult.Name = "labelCompareSignatureResult";
            this.labelCompareSignatureResult.Size = new System.Drawing.Size(13, 13);
            this.labelCompareSignatureResult.TabIndex = 19;
            this.labelCompareSignatureResult.Text = "?";
            // 
            // checkBoxCompareFiles
            // 
            this.checkBoxCompareFiles.AutoSize = true;
            this.checkBoxCompareFiles.Checked = true;
            this.checkBoxCompareFiles.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxCompareFiles.Location = new System.Drawing.Point(13, 205);
            this.checkBoxCompareFiles.Name = "checkBoxCompareFiles";
            this.checkBoxCompareFiles.Size = new System.Drawing.Size(168, 17);
            this.checkBoxCompareFiles.TabIndex = 14;
            this.checkBoxCompareFiles.Text = "Compare files (byte compare): ";
            this.checkBoxCompareFiles.UseVisualStyleBackColor = true;
            // 
            // checkBoxCompareSignature
            // 
            this.checkBoxCompareSignature.AutoSize = true;
            this.checkBoxCompareSignature.Checked = true;
            this.checkBoxCompareSignature.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxCompareSignature.Location = new System.Drawing.Point(13, 182);
            this.checkBoxCompareSignature.Name = "checkBoxCompareSignature";
            this.checkBoxCompareSignature.Size = new System.Drawing.Size(125, 17);
            this.checkBoxCompareSignature.TabIndex = 13;
            this.checkBoxCompareSignature.Text = "Compare signatures: ";
            this.checkBoxCompareSignature.UseVisualStyleBackColor = true;
            // 
            // checkBoxVerifyHeader
            // 
            this.checkBoxVerifyHeader.AutoSize = true;
            this.checkBoxVerifyHeader.Checked = true;
            this.checkBoxVerifyHeader.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxVerifyHeader.Location = new System.Drawing.Point(13, 159);
            this.checkBoxVerifyHeader.Name = "checkBoxVerifyHeader";
            this.checkBoxVerifyHeader.Size = new System.Drawing.Size(132, 17);
            this.checkBoxVerifyHeader.TabIndex = 12;
            this.checkBoxVerifyHeader.Text = "Verify wrapper header:";
            this.checkBoxVerifyHeader.UseVisualStyleBackColor = true;
            // 
            // checkBoxCreateDescriptionFile
            // 
            this.checkBoxCreateDescriptionFile.AutoSize = true;
            this.checkBoxCreateDescriptionFile.Checked = true;
            this.checkBoxCreateDescriptionFile.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxCreateDescriptionFile.Location = new System.Drawing.Point(13, 94);
            this.checkBoxCreateDescriptionFile.Name = "checkBoxCreateDescriptionFile";
            this.checkBoxCreateDescriptionFile.Size = new System.Drawing.Size(127, 17);
            this.checkBoxCreateDescriptionFile.TabIndex = 8;
            this.checkBoxCreateDescriptionFile.Text = "Create description file";
            this.checkBoxCreateDescriptionFile.UseVisualStyleBackColor = true;
            // 
            // buttonOpenFolder
            // 
            this.buttonOpenFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOpenFolder.Location = new System.Drawing.Point(656, 128);
            this.buttonOpenFolder.Name = "buttonOpenFolder";
            this.buttonOpenFolder.Size = new System.Drawing.Size(24, 23);
            this.buttonOpenFolder.TabIndex = 11;
            this.buttonOpenFolder.Text = "...";
            this.buttonOpenFolder.UseVisualStyleBackColor = true;
            this.buttonOpenFolder.Click += new System.EventHandler(this.buttonOpenFolder_Click);
            // 
            // textBoxTargetFile
            // 
            this.textBoxTargetFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTargetFile.Enabled = false;
            this.textBoxTargetFile.Location = new System.Drawing.Point(90, 130);
            this.textBoxTargetFile.Name = "textBoxTargetFile";
            this.textBoxTargetFile.Size = new System.Drawing.Size(560, 20);
            this.textBoxTargetFile.TabIndex = 10;
            // 
            // textBoxResult
            // 
            this.textBoxResult.AcceptsReturn = true;
            this.textBoxResult.AcceptsTab = true;
            this.textBoxResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxResult.BackColor = System.Drawing.Color.White;
            this.textBoxResult.Font = new System.Drawing.Font("Miriam Fixed", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.textBoxResult.Location = new System.Drawing.Point(13, 228);
            this.textBoxResult.Multiline = true;
            this.textBoxResult.Name = "textBoxResult";
            this.textBoxResult.ReadOnly = true;
            this.textBoxResult.Size = new System.Drawing.Size(667, 210);
            this.textBoxResult.TabIndex = 15;
            this.textBoxResult.Text = "\r\n\r\n\r\n\r\n\r\n";
            // 
            // buttonWrapFile
            // 
            this.buttonWrapFile.Enabled = false;
            this.buttonWrapFile.Location = new System.Drawing.Point(13, 130);
            this.buttonWrapFile.Name = "buttonWrapFile";
            this.buttonWrapFile.Size = new System.Drawing.Size(60, 23);
            this.buttonWrapFile.TabIndex = 9;
            this.buttonWrapFile.Text = "Wrap file...";
            this.buttonWrapFile.UseVisualStyleBackColor = true;
            this.buttonWrapFile.Click += new System.EventHandler(this.buttonWrapFile_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.maskedTextBoxVerBuild);
            this.panel1.Controls.Add(this.maskedTextBoxVerRever);
            this.panel1.Controls.Add(this.maskedTextBoxVerMinor);
            this.panel1.Controls.Add(this.maskedTextBoxVerMajor);
            this.panel1.Location = new System.Drawing.Point(90, 62);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(113, 17);
            this.panel1.TabIndex = 8;
            // 
            // maskedTextBoxVerBuild
            // 
            this.maskedTextBoxVerBuild.AsciiOnly = true;
            this.maskedTextBoxVerBuild.BeepOnError = true;
            this.maskedTextBoxVerBuild.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.maskedTextBoxVerBuild.Culture = new System.Globalization.CultureInfo("en-US");
            this.maskedTextBoxVerBuild.CutCopyMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.maskedTextBoxVerBuild.Dock = System.Windows.Forms.DockStyle.Left;
            this.maskedTextBoxVerBuild.HideSelection = false;
            this.maskedTextBoxVerBuild.Location = new System.Drawing.Point(76, 0);
            this.maskedTextBoxVerBuild.Mask = "99999";
            this.maskedTextBoxVerBuild.Name = "maskedTextBoxVerBuild";
            this.maskedTextBoxVerBuild.PromptChar = ' ';
            this.maskedTextBoxVerBuild.Size = new System.Drawing.Size(32, 13);
            this.maskedTextBoxVerBuild.TabIndex = 7;
            this.maskedTextBoxVerBuild.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.maskedTextBoxVerBuild.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.maskedTextBoxVerBuild.Enter += new System.EventHandler(this.maskedTextBoxVerBuild_Enter);
            // 
            // maskedTextBoxVerRever
            // 
            this.maskedTextBoxVerRever.AsciiOnly = true;
            this.maskedTextBoxVerRever.BeepOnError = true;
            this.maskedTextBoxVerRever.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.maskedTextBoxVerRever.Culture = new System.Globalization.CultureInfo("en-US");
            this.maskedTextBoxVerRever.CutCopyMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.maskedTextBoxVerRever.Dock = System.Windows.Forms.DockStyle.Left;
            this.maskedTextBoxVerRever.HideSelection = false;
            this.maskedTextBoxVerRever.Location = new System.Drawing.Point(44, 0);
            this.maskedTextBoxVerRever.Mask = "99999.";
            this.maskedTextBoxVerRever.Name = "maskedTextBoxVerRever";
            this.maskedTextBoxVerRever.PromptChar = ' ';
            this.maskedTextBoxVerRever.Size = new System.Drawing.Size(32, 13);
            this.maskedTextBoxVerRever.TabIndex = 6;
            this.maskedTextBoxVerRever.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.maskedTextBoxVerRever.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.maskedTextBoxVerRever.Enter += new System.EventHandler(this.maskedTextBoxVerRever_Enter);
            // 
            // maskedTextBoxVerMinor
            // 
            this.maskedTextBoxVerMinor.AsciiOnly = true;
            this.maskedTextBoxVerMinor.BeepOnError = true;
            this.maskedTextBoxVerMinor.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.maskedTextBoxVerMinor.Culture = new System.Globalization.CultureInfo("en-US");
            this.maskedTextBoxVerMinor.Dock = System.Windows.Forms.DockStyle.Left;
            this.maskedTextBoxVerMinor.HideSelection = false;
            this.maskedTextBoxVerMinor.Location = new System.Drawing.Point(22, 0);
            this.maskedTextBoxVerMinor.Mask = "990.";
            this.maskedTextBoxVerMinor.Name = "maskedTextBoxVerMinor";
            this.maskedTextBoxVerMinor.PromptChar = ' ';
            this.maskedTextBoxVerMinor.Size = new System.Drawing.Size(22, 13);
            this.maskedTextBoxVerMinor.TabIndex = 5;
            this.maskedTextBoxVerMinor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.maskedTextBoxVerMinor.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.maskedTextBoxVerMinor.Enter += new System.EventHandler(this.maskedTextBoxVerMinor_Enter);
            // 
            // maskedTextBoxVerMajor
            // 
            this.maskedTextBoxVerMajor.AsciiOnly = true;
            this.maskedTextBoxVerMajor.BeepOnError = true;
            this.maskedTextBoxVerMajor.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.maskedTextBoxVerMajor.Culture = new System.Globalization.CultureInfo("en-US");
            this.maskedTextBoxVerMajor.Dock = System.Windows.Forms.DockStyle.Left;
            this.maskedTextBoxVerMajor.HideSelection = false;
            this.maskedTextBoxVerMajor.Location = new System.Drawing.Point(0, 0);
            this.maskedTextBoxVerMajor.Mask = "990.";
            this.maskedTextBoxVerMajor.Name = "maskedTextBoxVerMajor";
            this.maskedTextBoxVerMajor.PromptChar = ' ';
            this.maskedTextBoxVerMajor.Size = new System.Drawing.Size(22, 13);
            this.maskedTextBoxVerMajor.TabIndex = 4;
            this.maskedTextBoxVerMajor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.maskedTextBoxVerMajor.TextMaskFormat = System.Windows.Forms.MaskFormat.ExcludePromptAndLiterals;
            this.maskedTextBoxVerMajor.Enter += new System.EventHandler(this.maskedTextBoxVerMajor_Enter);
            // 
            // labelFileVer
            // 
            this.labelFileVer.AutoSize = true;
            this.labelFileVer.Location = new System.Drawing.Point(10, 66);
            this.labelFileVer.Name = "labelFileVer";
            this.labelFileVer.Size = new System.Drawing.Size(63, 13);
            this.labelFileVer.TabIndex = 5;
            this.labelFileVer.Text = "File version:";
            // 
            // comboBoxFileType
            // 
            this.comboBoxFileType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxFileType.FormattingEnabled = true;
            this.comboBoxFileType.Location = new System.Drawing.Point(90, 7);
            this.comboBoxFileType.Name = "comboBoxFileType";
            this.comboBoxFileType.Size = new System.Drawing.Size(113, 21);
            this.comboBoxFileType.TabIndex = 1;
            this.comboBoxFileType.SelectedIndexChanged += new System.EventHandler(this.comboBoxFileType_SelectedIndexChanged);
            // 
            // labelFileType
            // 
            this.labelFileType.AutoSize = true;
            this.labelFileType.Location = new System.Drawing.Point(10, 10);
            this.labelFileType.Name = "labelFileType";
            this.labelFileType.Size = new System.Drawing.Size(49, 13);
            this.labelFileType.TabIndex = 3;
            this.labelFileType.Text = "File type:";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(656, 32);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(24, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBoxFilePath
            // 
            this.textBoxFilePath.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFilePath.Enabled = false;
            this.textBoxFilePath.Location = new System.Drawing.Point(90, 34);
            this.textBoxFilePath.Name = "textBoxFilePath";
            this.textBoxFilePath.Size = new System.Drawing.Size(560, 20);
            this.textBoxFilePath.TabIndex = 2;
            // 
            // labelFilePath
            // 
            this.labelFilePath.AutoSize = true;
            this.labelFilePath.Location = new System.Drawing.Point(10, 37);
            this.labelFilePath.Name = "labelFilePath";
            this.labelFilePath.Size = new System.Drawing.Size(26, 13);
            this.labelFilePath.TabIndex = 0;
            this.labelFilePath.Text = "File:";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "DSP files|*.bin|FPGA files|*.rbf|All files|*.*";
            this.openFileDialog1.InitialDirectory = "..";
            this.openFileDialog1.ReadOnlyChecked = true;
            this.openFileDialog1.RestoreDirectory = true;
            this.openFileDialog1.ShowReadOnly = true;
            this.openFileDialog1.SupportMultiDottedExtensions = true;
            this.openFileDialog1.Title = "Source file selection";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(694, 467);
            this.Controls.Add(this.panelMain);
            this.Controls.Add(this.statusStrip1);
            this.Name = "MainForm";
            this.Text = "Rimed binary file wrapper";
            this.panelMain.ResumeLayout(false);
            this.panelMain.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.ComboBox comboBoxFileType;
        private System.Windows.Forms.Label labelFileType;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBoxFilePath;
        private System.Windows.Forms.Label labelFilePath;
        private System.Windows.Forms.Label labelFileVer;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxVerBuild;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxVerRever;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxVerMinor;
        private System.Windows.Forms.MaskedTextBox maskedTextBoxVerMajor;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button buttonWrapFile;
        private System.Windows.Forms.TextBox textBoxResult;
        private System.Windows.Forms.TextBox textBoxTargetFile;
        private System.Windows.Forms.Button buttonOpenFolder;
        private System.Windows.Forms.CheckBox checkBoxCreateDescriptionFile;
        private System.Windows.Forms.Label labelFileCompareResult;
        private System.Windows.Forms.Label labelVerifyHeaderResult;
        private System.Windows.Forms.Label labelCompareSignatureResult;
        private System.Windows.Forms.CheckBox checkBoxCompareFiles;
        private System.Windows.Forms.CheckBox checkBoxCompareSignature;
        private System.Windows.Forms.CheckBox checkBoxVerifyHeader;
    }
}

