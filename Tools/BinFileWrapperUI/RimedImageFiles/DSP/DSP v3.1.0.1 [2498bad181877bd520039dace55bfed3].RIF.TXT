---------------------------------------------------------------------------------------------
 Rimed Digi-Lite™ Rev6 binary file wrapper                               2013.08.28 15:40:31
---------------------------------------------------------------------------------------------
Work path:               'C:\OFERB\SOURCE\TCD\MAIN\Rev6\MAIN\Tools\BinFileWrapperUI\RimedImageFiles\DSP\'

Input file:              'DSP v3.1.0.1.bin'
Input file create date:  2013.08.28 15:36:28
Input file size:         490640 Bytes
Input file signature:    c81b83ff41c74c9b2701e342541e343c
Input file type:         DSP
Input file version:      v3.1.0.1

Output file:             'DSP v3.1.0.1 [2498bad181877bd520039dace55bfed3].RIF'
Output file create date: 2013.08.28 15:40:31
Output file signature:   2498bad181877bd520039dace55bfed3

