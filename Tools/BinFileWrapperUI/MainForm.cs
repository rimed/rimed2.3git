﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Rimed.Framework.Common;
using Rimed.Tools.BinFileWrapper;

namespace Rimed.Tools.BinFileWrapperUI
{
    public partial class MainForm : Form
    {
        private const string APPLICATION_CAPTION    = "Rimed HW image file wrapper";
        private const string DIALOG_FILE_FILTERS    = "DSP files|*.bin|FPGA files|*.rbf|All files|*.*";
        private const int DIALOG_FILE_FILTER_DSP    = 1;
        private const int DIALOG_FILE_FILTER_FPGA   = 2;
        private const int DIALOG_FILE_FILTER_ALL    = 3;

        private const string VERIFY_SKIPPED = "SKIPPED";
        private const string VERIFY_PASS = "PASS";
        private const string VERIFY_FAIL = "FAIL";

        private static readonly Color s_verifySkippedColor  = Color.Black;
        private static readonly Color s_verifyPassColor     = Color.Green;
        private static readonly Color s_verifyFailColor     = Color.Red;

        public MainForm()
        {
            InitializeComponent();

            Text = APPLICATION_CAPTION;

            openFileDialog1.Filter      = DIALOG_FILE_FILTERS;
            openFileDialog1.FilterIndex = DIALOG_FILE_FILTER_ALL;

            // Init FileType ComboBox
            comboBoxFileType.Items.Add(EFileType.UNKNOWN);
            comboBoxFileType.Items.Add(EFileType.DSP);
            comboBoxFileType.Items.Add(EFileType.FPGA);
            comboBoxFileType.SelectedIndex = 0;

            //LoggedDialog.DefaultOwner = this;
        }

        private EFileType SelectedFileType { get { return (EFileType)comboBoxFileType.SelectedItem; } }

        private void button1_Click(object sender, EventArgs e)
        {
            if (SelectedFileType == EFileType.DSP)
                openFileDialog1.FilterIndex = DIALOG_FILE_FILTER_DSP;
            else if (SelectedFileType == EFileType.FPGA)
                openFileDialog1.FilterIndex = DIALOG_FILE_FILTER_FPGA;
            else
                openFileDialog1.FilterIndex = DIALOG_FILE_FILTER_ALL;

            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                textBoxFilePath.Text    = openFileDialog1.FileName;
                buttonWrapFile.Enabled  = true;
            }
        }

        private void buttonWrapFile_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBoxFilePath.Text))
            {
                LoggedDialog.ShowError(this, "Sorce file not selected.");
                return;
            }



			var fileVer = new Files.Version();
            ushort tmp;
            ushort.TryParse(maskedTextBoxVerMajor.Text, out tmp);
            fileVer.Major = tmp;
            ushort.TryParse(maskedTextBoxVerMinor.Text, out tmp);
            fileVer.Minor = tmp;
            ushort.TryParse(maskedTextBoxVerRever.Text, out tmp);
            fileVer.Reversion = tmp;
            ushort.TryParse(maskedTextBoxVerBuild.Text, out tmp);
            fileVer.Build = tmp;

            if (fileVer.IsEmpty)
            {
				LoggedDialog.ShowError(this, "Image version can NOT be empty.");
                return;
            }

            textBoxResult.Text      = string.Empty;
            var sourceFile          = textBoxFilePath.Text;
            var isWriteDescFile     = checkBoxCreateDescriptionFile.Checked;
            string descriptionFile;
            var targetFile          = Wrapper.WrapFile(sourceFile, SelectedFileType, fileVer, out descriptionFile, isWriteDescFile);
            textBoxTargetFile.Text  = targetFile;

            if (isWriteDescFile && File.Exists(descriptionFile))
            {
                using (var sr = new StreamReader(descriptionFile))
                {
                    textBoxResult.Text = sr.ReadToEnd();
                }
            }
            else
            {
                textBoxResult.Text = string.Format("Source file: {0}.  \n\n\nTarget file: {1}.  ", sourceFile, targetFile);
            }

            if (verify())
                MessageBox.Show(this,"Done", "Wrap result");
            else
                MessageBox.Show(this, "FAIL", "Wrap result");
        }

        private void buttonOpenFolder_Click(object sender, EventArgs e)
        {
            var path = Files.GetFilePath(textBoxTargetFile.Text);
            if (!Files.IsPathValid(path))
                return;

            if (!Directory.Exists(path))
            return;

            Process.Start(path);
        }

        private bool verify()
        {
            labelVerifyHeaderResult.ForeColor       = s_verifySkippedColor;
            labelCompareSignatureResult.ForeColor   = s_verifySkippedColor;
            labelFileCompareResult.ForeColor        = s_verifySkippedColor;

            if (!verifyWrappedFileHeader(textBoxTargetFile.Text))
                return false;


            return verifyWrappedFile(textBoxFilePath.Text, textBoxTargetFile.Text);
        }

        private bool verifyWrappedFileHeader(string wrappedFile)
        {
            if (!checkBoxVerifyHeader.Checked)
            {
                labelVerifyHeaderResult.Text = VERIFY_SKIPPED;
            }
            else
            {
                var isVerifiled = Wrapper.ValidateWrappedFile(wrappedFile);
                setVerifyResult(labelVerifyHeaderResult, isVerifiled);
                if (!isVerifiled)
                    return false;
            }

            return true;
        }

        private bool verifyWrappedFile(string src, string wrappedFile)
        {
            if (!checkBoxCompareSignature.Checked && !checkBoxCompareFiles.Checked)
            {
                labelCompareSignatureResult.Text    = VERIFY_SKIPPED;
                labelFileCompareResult.Text         = VERIFY_SKIPPED;

                return true;
            }
            
            var result      = true;

            var rewriteFile = wrappedFile + ".ReWrite";
            var reader      = new WrapperFwdReader(wrappedFile, 4000);
            reader.Init();

            using (var fsWrite = new FileStream(rewriteFile, FileMode.CreateNew, FileAccess.Write, FileShare.None))
            {
                byte[] block;
                do
                {
                    block = reader.ReadNext();
                    fsWrite.Write(block, 0, block.Length);
                    fsWrite.Flush();
                } while (block.Length > 0);
            }

            reader.Close();

            if (!checkBoxCompareSignature.Checked) // Signature validation
            {
                labelCompareSignatureResult.Text = VERIFY_SKIPPED;
            }
            else
            {
                var sig1 = Files.GetFileStringSinature(rewriteFile);
                var sig2 = Files.GetFileStringSinature(src);


                result = (string.Compare(sig1, sig2, StringComparison.Ordinal) == 0);
                setVerifyResult(labelCompareSignatureResult, result);
            }

            if (!checkBoxCompareFiles.Checked)
            {
                labelFileCompareResult.Text = VERIFY_SKIPPED;
            }
            else if (result)
            {
                using (var fsOrgReader = new FileStream(textBoxFilePath.Text, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    using ( var fsUnwrapReader = new FileStream(rewriteFile, FileMode.Open, FileAccess.Read, FileShare.Read) )
                    {
                        var orgByte = fsOrgReader.ReadByte();
                        var unwrapByte = fsUnwrapReader.ReadByte();
                        while (orgByte == unwrapByte)
                        {
                            orgByte     = fsOrgReader.ReadByte();
                            unwrapByte  = fsUnwrapReader.ReadByte();

                            if (orgByte < 0 || unwrapByte < 0)
                                break;
                        }

                        result = (orgByte == unwrapByte);
                        setVerifyResult(labelFileCompareResult, result);
                    }
                }
            }

            Files.Delete(rewriteFile);

            return result;
        }

        private void setVerifyResult(Label lbl, bool isVerifired)
        {
            if (lbl == null)
                return;

            if (isVerifired)
            {
                lbl.Text        = VERIFY_PASS;
                lbl.ForeColor   = s_verifyPassColor;
            }
            else
            {
                lbl.Text        = VERIFY_FAIL;
                lbl.ForeColor   = s_verifyFailColor;
            }
            
        }

        private void maskedTextBoxVerMajor_Enter(object sender, EventArgs e)
        {
            maskedTextBoxVerMajor.SelectAll();
        }

        private void maskedTextBoxVerMinor_Enter(object sender, EventArgs e)
        {
            maskedTextBoxVerMinor.SelectAll();
        }

        private void maskedTextBoxVerRever_Enter(object sender, EventArgs e)
        {
            maskedTextBoxVerRever.SelectAll();
        }

        private void maskedTextBoxVerBuild_Enter(object sender, EventArgs e)
        {
            maskedTextBoxVerBuild.SelectAll();
        }

        private void comboBoxFileType_SelectedIndexChanged(object sender, EventArgs e)
        {
            Text = string.Format("{0}: {1}", APPLICATION_CAPTION, SelectedFileType);
            reset();
        }

        private void reset()
        {
            textBoxFilePath.Text = string.Empty;
            textBoxTargetFile.Text = string.Empty;
            textBoxResult.Text = string.Empty;

            maskedTextBoxVerMajor.Text = string.Empty;
            maskedTextBoxVerMinor.Text = string.Empty;
            maskedTextBoxVerRever.Text = string.Empty;
            maskedTextBoxVerBuild.Text = string.Empty;

            checkBoxCreateDescriptionFile.Checked = true;

            checkBoxVerifyHeader.Checked = true;
            labelVerifyHeaderResult.Text = string.Empty;

            checkBoxCompareSignature.Checked = true;
            labelCompareSignatureResult.Text = string.Empty;

            checkBoxCompareFiles.Checked = true;
            labelFileCompareResult.Text = string.Empty;

            buttonWrapFile.Enabled = false;
        }
    }
}
