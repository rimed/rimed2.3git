﻿using System;
using System.Windows.Forms;
using Rimed.Framework.Common;

namespace Rimed.Tools.HDSN
{
	public partial class MainForm : Form
	{
		public MainForm()
		{
			InitializeComponent();
			TopMost = true;
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			var info = VolumeInfo.CurrentVolume();
			textBoxHDSN.Text = info.SerialNumber.ToString();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			Close();
		}
	}
}
