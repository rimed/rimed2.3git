﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ScreenCapture_MedSim
{
    public enum CaptureState
    {
        /// <summary>Capture is stopped.</summary>
        Stopped = 0,

        /// <summary>Capture is started.</summary>
        Started = 1,

        /// <summary>Capture is paused.</summary>
        Paused = 2
    }

    public delegate void ProgressChange(int encode, int decompress, int merge);

    public interface IScreenCapture
    {
        event ProgressChange OnProgressChange;
        event EventHandler OnFinished;

        string Author { get; set; }

        string Copyright { get; set; }

        string Description { get; set; }

        CaptureState State { get; set; }

        string Start(string fileName);

        void Stop();

        void Pause();
    }
}
