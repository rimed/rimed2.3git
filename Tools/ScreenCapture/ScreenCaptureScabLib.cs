﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using ScapLIB;

namespace ScreenCapture_MedSim
{
    public class ScreenCaptureScabLib:IScreenCapture
    {
        public event ProgressChange OnProgressChange;
        public event EventHandler OnFinished;

        private const string OUTPUTFILE = @"OutputFile.wmv";


        public ScreenCaptureScabLib()
        {
            //int fps = Convert.ToInt16(Author);
            //Cap = new ScapCapture(true, 5, 10, ScapVideoFormats.WMV1, ScapImageFormats.Bmp, 0, 0,
            //    Screen.PrimaryScreen.WorkingArea.Width, Screen.PrimaryScreen.WorkingArea.Height);
            //ScapBackendConfig.ScapBackendSetup(Cap);
            timer1 = new Timer();
            timer1.Tick += new EventHandler(timer1_Tick);
            timer1.Interval = 10;
            timer1.Enabled = true;
        }

        void timer1_Disposed(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        public string Author { get; set; }

        public string Copyright { get; set; }

        public string Description { get; set; }

        public CaptureState State { get; set; }



        public string Start(string fileName)
        {
            try
            {
                int fps = Convert.ToInt32(ConfigurationManager.AppSettings["FPS"]);
                Cap = new ScapCapture(true, 5, fps, ScapVideoFormats.WMV1, ScapImageFormats.Jpeg, 0, 0,
                                        Screen.PrimaryScreen.WorkingArea.Width, Screen.PrimaryScreen.WorkingArea.Height);
                ScapBackendConfig.ScapBackendSetup(Cap);

                ScapCore.StartCapture();
                State = CaptureState.Started;
                finishDecompress = false;
                finishEncode = false;
                return "";
            }
            catch (Exception ex)
            {
                State = CaptureState.Stopped;
                return ex.Message;
            }
        }

        public void Stop()
        {
            ScapCore.StopCapture();
            ScapCore.DecompressCapture(false);
            State = CaptureState.Stopped;
        }

        public void Pause()
        {
            ScapCore.PauseCapture();
            State = CaptureState.Paused;
        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            timerCounter++;
            if (timerCounter == 200)
            {
                encode = ScapCore.GetEncodeProgress();
                if (encode >= 1 && !finishEncode)
                {
                    Merge();
                    finishEncode = true;
                }

                decompress = ScapCore.GetDecompressionProgress();
                if (decompress >= 1 && !finishDecompress)
                {
                    ScapCore.EncodeCapture(false);
                    finishDecompress = true;
                }
                timerCounter = 0;

                if (OnProgressChange != null)
                    OnProgressChange((int)(encode * 100), (int)(decompress * 100), merge);
            }

        }               

        private void Merge()
        {
            // create batch file
            var startInfo = new ProcessStartInfo("Merge.bat") { Arguments = "/wait Merge.bat ", CreateNoWindow = true, WindowStyle = ProcessWindowStyle.Hidden };
            Process.Start(startInfo);
            if (OnProgressChange != null)
            {
                OnProgressChange((int) (encode*100), (int) (decompress*100), 50);
            }

            double koeff = CalcDurations();
            if (koeff > 0.01)
            {
                var process = new Process();
                var startinfo = new ProcessStartInfo("cmd.exe", @"/C FFmpeg -i OutputFileAsync.wmv -vf setpts=" + koeff + "*PTS OutputFile.wmv -y");
                startinfo.WindowStyle = ProcessWindowStyle.Hidden;
                startinfo.CreateNoWindow = true;
                process.StartInfo = startinfo;
                process.Start();
                process.WaitForExit();
            }
            else
            {
                File.Copy("OutputFileAsync.wmv", "OutputFile.wmv");
            }

            if (OnProgressChange != null)
            {
                OnProgressChange((int)(encode * 100), (int)(decompress * 100), 100);
            }

            
            if (OnFinished != null)
            {
                OnFinished(this, null);
            }
        }


        //Duration


        public double CalcDurations()
        {

            var tVideo = new TimeSpan();
            var tAudio = new TimeSpan();
            Process mProcess = null;
            StreamReader SROutput = null;

            string outPut = "";

            string param = string.Format("-i \"{0}\"", "0-Vid.wmv");

            ProcessStartInfo oInfo = null;

            System.Text.RegularExpressions.Regex re = null;
            System.Text.RegularExpressions.Match m = null;

            //Get ready with ProcessStartInfo
            oInfo = new ProcessStartInfo("ffMPEG.exe", param);
            oInfo.CreateNoWindow = true;

            //ffMPEG uses StandardError for its output.
            oInfo.RedirectStandardError = true;
            oInfo.WindowStyle = ProcessWindowStyle.Hidden;
            oInfo.UseShellExecute = false;

            // Lets start the process

            mProcess = Process.Start(oInfo);

            // Divert output
            SROutput = mProcess.StandardError;

            // Read all
            outPut = SROutput.ReadToEnd();

            // Please donot forget to call WaitForExit() after calling SROutput.ReadToEnd

            mProcess.WaitForExit();
            mProcess.Close();
            mProcess.Dispose();
            SROutput.Close();
            SROutput.Dispose();

            //get duration

            re = new System.Text.RegularExpressions.Regex("[D|d]uration:.((\\d|:|\\.)*)");
            m = re.Match(outPut);

            if (m.Success)
            {
                //Means the output has cantained the string "Duration"
                string temp = m.Groups[1].Value;
                string[] timepieces = temp.Split(new char[] { ':', '.' });
                if (timepieces.Length == 4)
                {

                    // Store duration
                    tVideo = new TimeSpan(0, Convert.ToInt16(timepieces[0]), Convert.ToInt16(timepieces[1]), Convert.ToInt16(timepieces[2]), Convert.ToInt16(timepieces[3]));
                }
            }
            tAudio = SoundInfo.GetSoundLength("AudioCap.wav");
            double s = tAudio.Ticks / tVideo.Ticks;
            return s - (s / 10);
        }

        private ScapCapture Cap;
        private Timer timer1;
        private bool finishDecompress = false;
        private bool finishEncode = false;
        private int timerCounter = 0;
        private double encode = 0;
        private double decompress = 0;
        private int merge = 0;
    }

    public static class SoundInfo
    {
        [DllImport("winmm.dll")]
        private static extern uint mciSendString(
            string command,
            StringBuilder returnValue,
            int returnLength,
            IntPtr winHandle);

        public static TimeSpan GetSoundLength(string fileName)
        {
            StringBuilder lengthBuf = new StringBuilder(32);

            mciSendString(string.Format("open \"{0}\" type waveaudio alias wave", fileName), null, 0, IntPtr.Zero);
            mciSendString("status wave length", lengthBuf, lengthBuf.Capacity, IntPtr.Zero);
            mciSendString("close wave", null, 0, IntPtr.Zero);

            int length = 0;
            int.TryParse(lengthBuf.ToString(), out length);

            return TimeSpan.FromMilliseconds(length);
        }
    }
}
