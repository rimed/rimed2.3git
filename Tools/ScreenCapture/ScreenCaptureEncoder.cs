﻿using System;
using System.IO;
using System.Windows.Forms;
using WMEncoderLib;

//Added by Alex Feature #672 v.2.2.3.02 02/07/2015
namespace ScreenCapture_MedSim
{
    //public enum CaptureState
    //{
    //    /// <summary>Capture is stopped.</summary>
    //    Stopped = 0,

    //    /// <summary>Capture is started.</summary>
    //    Started = 1,

    //    /// <summary>Capture is paused.</summary>
    //    Paused = 2
    //}


    public class ScreenCaptureEncoder:IScreenCapture
    {

        public event ProgressChange OnProgressChange;
        public event EventHandler OnFinished;

        public ScreenCaptureEncoder()
        {
            Description = "";
            Author = "Med-Dev Design Ltd";
            Copyright = "Med-Dev Design Ltd ©";
        }


        private WMEncoder m_encoder;
        private CaptureState state = CaptureState.Stopped;
        private string FileName = "";


        public string Author { get; set; }

        public string Copyright { get; set; }

        public string Description { get; set; }

        public CaptureState State
        {
            get { return state; }
            set { state = value; }
        }


        // *********************************************************************
        /// <summary>
        /// Starts capturing the video after taking the output video file from
        /// user.The file type is decide based on what kind of capture is being
        /// done which is based on the selected capture profile.
        /// </summary>
        public string Start(string fileName)
        {
            bool status = false;
            FileName = fileName;
            try
            {
                this.StartCapture(fileName);
                status = true;
                return "";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        } // end start

        // *********************************************************************
        /// <summary>
        /// Starts capturing the video into specified output video file from
        /// user.
        /// </summary>
        /// <param name="targetFile">The video file where video is to be saved.
        /// </param>
        private void StartCapture(string targetFile)
        {
            if (m_encoder == null) m_encoder = new WMEncoder();

            // Retrieve the source group collection and add a source group.

            IWMEncSourceGroup SrcGrp;
            IWMEncSourceGroupCollection SrcGrpColl;

            SrcGrpColl = m_encoder.SourceGroupCollection;
            SrcGrp = SrcGrpColl.Add("SourceGroupName");

            // Add a video and audio source to the source group.
            IWMEncSource SrcVid = null;
            IWMEncSource SrcAud = null;

            SrcVid = SrcGrp.AddSource(WMENC_SOURCE_TYPE.WMENC_VIDEO);
            // Identify the source files to encode.
            SrcVid.SetInput("ScreenCap://ScreenCapture1", "", "");

            SrcAud = SrcGrp.AddSource(WMENC_SOURCE_TYPE.WMENC_AUDIO);
            // Identify the source files to encode.
            SrcAud.SetInput("Device://Default_Audio_Device", "", "");

            IWMEncProfileCollection ProColl = m_encoder.ProfileCollection;
            IWMEncProfile Pro;
            for (int i = 0; i < ProColl.Count; i++)
            {
                Pro = ProColl.Item(i);

                if (Pro.Name == "Screen Video/Audio High (CBR)")
                {
                    SrcGrp.set_Profile(Pro);
                    break;
                }
            }
            // Fill in the description object members.
            IWMEncDisplayInfo Descr;
            Descr = m_encoder.DisplayInfo;
            Descr.Author = Author;
            Descr.Copyright = Copyright;
            Descr.Description = Description;
            Descr.Rating = "";
            Descr.Title = "";

            IWMEncAttributes Attr;
            Attr = m_encoder.Attributes;
            Attr.Add("URL", "www.meddev-design.com");
            // Specify a file object in which to save encoded content.
            IWMEncFile File;
            File = m_encoder.File;
            File.LocalFileName = targetFile;
            if (null != SrcVid)
            {
                ((IWMEncVideoSource)SrcVid).CroppingBottomMargin = 0;
                ((IWMEncVideoSource)SrcVid).CroppingTopMargin = 0;
                ((IWMEncVideoSource)SrcVid).CroppingLeftMargin = 0;
                ((IWMEncVideoSource)SrcVid).CroppingRightMargin = 0;
            }
            try
            {
                // Start the encoding process.
                m_encoder.Start();
                State = CaptureState.Started;
            }
            catch (Exception ex)
            {
                // *** Important! *** For correct job, use only one monitor, or change the option "Multiple displays: Extended those displays" in Screen resolution settings
                MessageBox.Show("Could not start capturing! Try again. Error was: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                State = CaptureState.Stopped;
            }
        } // end StartCapture

        // *********************************************************************
        /// <summary>
        /// Stops capturing the video if already started.
        /// </summary>
        public void Stop()
        {
            if (m_encoder != null)
            {
                if (m_encoder.RunState != WMENC_ENCODER_STATE.WMENC_ENCODER_STOPPED)
                {
                    m_encoder.Stop();
                }
            }
            m_encoder = null;
            GC.Collect(); // Important for Vista users.
            State = CaptureState.Stopped;
        } // end Stop

        // *********************************************************************
        /// <summary>
        /// Pauses capturing the video if already started.
        /// </summary>
        public void Pause()
        {
            if (m_encoder != null)
            {
                if (m_encoder.RunState != WMENC_ENCODER_STATE.WMENC_ENCODER_STOPPED)
                {
                    m_encoder.Pause();
                    State = CaptureState.Paused;
                }
            }
        } // end Pause

        public void CutFile(string sourceName, string destinationName, int startTime, int endTime)
        {
            Int32 StartTime = startTime * 1000;
            Int32 EndTime = endTime * 1000;
            String SourceName = sourceName;
            String DestinationName = destinationName;
            IWMEncBasicEdit SplitFile = new WMEncBasicEdit();
            SplitFile.MediaFile = SourceName;
            SplitFile.OutputFile = DestinationName;
            SplitFile.MarkIn = StartTime;
            SplitFile.MarkOut = EndTime;
            SplitFile.Start();
        }

    } // end class Encoder
}

//using System;
//using System.IO;
//using System.Windows.Forms;
//using WMEncoderLib;

////Added by Alex Feature #672 v.2.2.3.02 02/07/2015
//namespace ScreenCapture_MedSim
//{
//    public class ScreenCaptureEncoder : IScreenCapture
//    {

//        public ScreenCaptureEncoder()
//        {
//            Description = "";
//            Author = "Med-Dev Design Ltd";
//            Copyright = "Med-Dev Design Ltd ©";
//        }


//        private WMEncoder m_encoder;
//        private CaptureState state = CaptureState.Stopped; 
//        private string FileName = "";


//        public string Author { get; set; }

//        public string Copyright { get; set; }

//        public string Description { get; set; }

//        public CaptureState State
//        {
//            get { return state; }
//            set { state = value; }
//        }


//        // *********************************************************************
//        /// <summary>
//        /// Starts capturing the video after taking the output video file from
//        /// user.The file type is decide based on what kind of capture is being
//        /// done which is based on the selected capture profile.
//        /// </summary>
//        public string Start(string fileName)
//        {
//            bool status = false;
//            FileName = fileName;
//            try
//            {
//                this.StartCapture(fileName);
//                status = true;
//                return "";
//            }
//            catch (Exception ex)
//            {
//                return ex.Message;
//            }
//        } // end start

//        // *********************************************************************
//        /// <summary>
//        /// Starts capturing the video into specified output video file from
//        /// user.
//        /// </summary>
//        /// <param name="targetFile">The video file where video is to be saved.
//        /// </param>
//        private void StartCapture(string targetFile)
//        {
//            if (m_encoder == null) m_encoder = new WMEncoder();

//            // Retrieve the source group collection and add a source group.

//            IWMEncSourceGroup SrcGrp;
//            IWMEncSourceGroupCollection SrcGrpColl;

//            SrcGrpColl = m_encoder.SourceGroupCollection;
//            SrcGrp = SrcGrpColl.Add("SourceGroupName");

//            // Add a video and audio source to the source group.
//            IWMEncSource SrcVid = null;
//            IWMEncSource SrcAud = null;

//            SrcVid = SrcGrp.AddSource(WMENC_SOURCE_TYPE.WMENC_VIDEO);
//            // Identify the source files to encode.
//            SrcVid.SetInput("ScreenCap://ScreenCapture1", "", "");

//            SrcAud = SrcGrp.AddSource(WMENC_SOURCE_TYPE.WMENC_AUDIO);
//            // Identify the source files to encode.
//            SrcAud.SetInput("Device://Default_Audio_Device", "", "");

//            IWMEncProfileCollection ProColl = m_encoder.ProfileCollection;
//            IWMEncProfile Pro;
//            for (int i = 0; i < ProColl.Count; i++)
//            {
//                Pro = ProColl.Item(i);

//                if (Pro.Name == "Screen Video/Audio High (CBR)")
//                {
//                    SrcGrp.set_Profile(Pro);
//                    break;
//                }
//            }
//            // Fill in the description object members.
//            IWMEncDisplayInfo Descr;
//            Descr = m_encoder.DisplayInfo;
//            Descr.Author = Author;
//            Descr.Copyright = Copyright;
//            Descr.Description = Description;
//            Descr.Rating = "";
//            Descr.Title = "";

//            IWMEncAttributes Attr;
//            Attr = m_encoder.Attributes;
//            Attr.Add("URL", "www.meddev-design.com");
//            // Specify a file object in which to save encoded content.
//            IWMEncFile File;
//            File = m_encoder.File;
//            File.LocalFileName = targetFile;
//            if (null != SrcVid)
//            {
//                ((IWMEncVideoSource)SrcVid).CroppingBottomMargin = 0;
//                ((IWMEncVideoSource)SrcVid).CroppingTopMargin = 0;
//                ((IWMEncVideoSource)SrcVid).CroppingLeftMargin = 0;
//                ((IWMEncVideoSource)SrcVid).CroppingRightMargin = 0;
//            }
//            try
//            {
//                // Start the encoding process.
//                m_encoder.Start();
//                State = CaptureState.Started;
//            }
//            catch (Exception ex)
//            {
//                // *** Important! *** For correct job, use only one monitor, or change the option "Multiple displays: Extended those displays" in Screen resolution settings
//                MessageBox.Show("Could not start capturing! Try again. Error was: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
//                State = CaptureState.Stopped;
//            }
//        } // end StartCapture

//        // *********************************************************************
//        /// <summary>
//        /// Stops capturing the video if already started.
//        /// </summary>
//        public void Stop()
//        {
//            if (m_encoder != null)
//            {
//                if (m_encoder.RunState != WMENC_ENCODER_STATE.WMENC_ENCODER_STOPPED)
//                {
//                    m_encoder.Stop();
//                }
//            }
//            m_encoder = null;
//            GC.Collect(); // Important for Vista users.
//            State = CaptureState.Stopped;
//        } // end Stop

//        // *********************************************************************
//        /// <summary>
//        /// Pauses capturing the video if already started.
//        /// </summary>
//        public void Pause()
//        {
//            if (m_encoder != null)
//            {
//                if (m_encoder.RunState != WMENC_ENCODER_STATE.WMENC_ENCODER_STOPPED)
//                {
//                    m_encoder.Pause();
//                    State = CaptureState.Paused;
//                }
//            }
//        } // end Pause

//        public void CutFile(string sourceName, string destinationName, int startTime, int endTime)
//        {
//            Int32 StartTime = startTime * 1000;
//            Int32 EndTime = endTime * 1000;
//            String SourceName = sourceName;
//            String DestinationName = destinationName;
//            IWMEncBasicEdit SplitFile = new WMEncBasicEdit();
//            SplitFile.MediaFile = SourceName;
//            SplitFile.OutputFile = DestinationName;
//            SplitFile.MarkIn = StartTime;
//            SplitFile.MarkOut = EndTime;
//            SplitFile.Start();
//        }

//    } // end class Encoder
//}