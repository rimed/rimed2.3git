﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WMEncoderLib;

namespace Rimed.Tools.ScreenCapture
{
    public static class CutVideoFile
    {
        /// <summary>
        /// Cuts the file.
        /// </summary>
        /// <param name="sourceName">Name of the source.</param>
        /// <param name="destinationName">Name of the destination.</param>
        /// <param name="startTime">The start time.</param>
        /// <param name="endTime">The end time.</param>
        public static void CutFile(string sourceName, string destinationName, int startTime, int endTime)
        {
            Int32 StartTime = startTime * 1000;
            Int32 EndTime = endTime * 1000;
            String SourceName = sourceName;
            String DestinationName = destinationName;
            IWMEncBasicEdit SplitFile = new WMEncBasicEdit();
            SplitFile.MediaFile = SourceName;
            SplitFile.OutputFile = DestinationName;
            SplitFile.MarkIn = StartTime;
            SplitFile.MarkOut = EndTime;
            SplitFile.Start();
        }
    }
}
