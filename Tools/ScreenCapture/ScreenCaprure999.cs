﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BytescoutScreenCapturingLib;

namespace ScreenCapture_MedSim
{
    public class ScreenCaprure999:IScreenCapture
    {
        public event ProgressChange OnProgressChange;
        public event EventHandler OnFinished;

        private Capturer capturer;

        public ScreenCaprure999()
        {
            Capturer capturer = new Capturer();
            capturer.CapturingType = CaptureAreaType.catScreen;
        }

        public string Author { get; set; }

        public string Copyright { get; set; }

        public string Description { get; set; }

        public CaptureState State  { get; set; }

        public string Start(string fileName)
        {
            try
            {
                capturer.AudioEnabled = true;
                capturer.OutputFileName = fileName;
                capturer.OutputWidth = Screen.PrimaryScreen.WorkingArea.Width;
                capturer.OutputHeight = Screen.PrimaryScreen.WorkingArea.Height;
                capturer.OutputHeight = 480;
                State = CaptureState.Started;
                return "";
            }
            catch (Exception ex)
            {
                State = CaptureState.Stopped;
                return ex.Message;
            }
        }

        public void Stop()
        {
            capturer.Stop();
            State = CaptureState.Stopped;
        }

        public void Pause()
        {
            capturer.Pause();
        }
    }
}
