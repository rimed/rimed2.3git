﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
//using System.Text;
using System.Threading;
using System.Windows.Forms;
using Geming.SimpleRec;
using ScreenCapture_MedSim;
using System.Timers;
using Timer = System.Timers.Timer;

using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Runtime.InteropServices;

namespace Rimed.Tools.ScreenCapture
{
    public class ScreenCaptureAlex: IScreenCapture
    {

        private const string OUTPUTFILE = @"OutputFile.wmv";
        private const string VIDEODIR = @"Video";
        private const string AUDIOFILE = VIDEODIR + @"\audio.wav";
        private const string VIDEOFILE = VIDEODIR + @"\video.wmv";
        private const string BATCHFILE = VIDEODIR + @"\movie.bat";
        private const string MOVIELISTFILE = VIDEODIR + @"\movie.txt";


        public event ProgressChange OnProgressChange;
        public event EventHandler OnFinished;

//)>    v.2.02.05.001 Updated MediaRecorder feature.
        private ConcurrentBag<Task> Tasks = new ConcurrentBag<Task>();
        EncoderParameters myEncoderParameters = null;
        ImageCodecInfo myImageCodecInfo = null;
        private static ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int j;
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }
//)>

        public ScreenCaptureAlex()
        {
            Initialisation();

//)>        v.2.02.05.001 Updated MediaRecorder feature. 
            //timer1.Elapsed += new ElapsedEventHandler(timer1_Elapsed);
            timer1.Elapsed += new ElapsedEventHandler(timer1_Elapsed2);
            timer1.Interval = Convert.ToInt32(ConfigurationManager.AppSettings["TimerIterval"]);

            // v.2.02.05.001 Updated MediaRecorder feature. Jpeg for snapshots.
            Encoder myEncoder = Encoder.Quality;
            EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 90L);
            myEncoderParameters = new EncoderParameters(1);
            myEncoderParameters.Param[0] = myEncoderParameter;
            myImageCodecInfo = GetEncoderInfo("image/jpeg");
//)>
            _sndrec = SndRec.Instance;
        }


        public string Author { get; set; }

        public string Copyright { get; set; }

        public string Description { get; set; }

        public CaptureState State { get; set; }


        public string Start(string fileName)
        {
//)>        v.2.02.05.001 Updated MediaRecorder feature.             
            OnProgressChange(0, 0, 0);
            counter = 0;
            timer1.Enabled = true;
//)>
            timer1.Start();

            _sndrec.Close();
            if (!_sndrec.IsInitialized)
                _sndrec.Initialize();
            _sndrec.Start(true);
            
            return "";
        }

        public void Stop()
        {
            timer1.Stop();

//)>        v.2.02.05.001 Updated MediaRecorder feature. 
            //counter = 0;
            timer1.Enabled = false;
            counter = -1;
//)>
            try
            {
                _sndrec.Stop();
                _sndrec.Save(AUDIOFILE);
                _sndrec.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            File.WriteAllLines(MOVIELISTFILE, MovieList.ToArray());
//)>        v.2.02.05.001 Updated MediaRecorder feature.
            Task item;
            while (Tasks.TryTake(out item))
            {
                if (!item.IsCompleted)
                    item.Wait();
            };                          
//)>                  
            RunBatchFile(BATCHFILE);
//)>        v.2.02.05.001 Updated MediaRecorder feature.
            MovieList.Clear();   
//)>
            if (OnFinished != null)
                OnFinished(this, null);
        }

        public void Pause()
        {
            throw new NotImplementedException();
        }


        private void Initialisation()
        {
            if (!Directory.Exists(VIDEODIR))
                Directory.CreateDirectory(VIDEODIR);

//)>        v.2.02.05.001 Updated MediaRecorder feature.
            //if (!File.Exists(BATCHFILE))
            if (!File.Exists(BATCHFILE) || Convert.ToBoolean(ConfigurationManager.AppSettings["UpdateBatFile"]) == true)            
            {
                List<string> batchFileList = new List<string>();
                //batchFileList.Add("ffmpeg -y -f concat -i " + MOVIELISTFILE + " " + VIDEOFILE);
                batchFileList.Add("ffmpeg -y -f concat -safe 0 -i " + MOVIELISTFILE + " -q:v 5 -threads 2 -r 15 " + VIDEOFILE);

                //batchFileList.Add("ffmpeg -y -i " + VIDEOFILE + " -i " + AUDIOFILE + " " + OUTPUTFILE);
                batchFileList.Add("ffmpeg -y -i " + VIDEOFILE + " -i " + AUDIOFILE + " -vcodec copy " + OUTPUTFILE);

                //batchFileList.Add(@"del " + VIDEODIR + @"\*.png");
                batchFileList.Add(@"del " + VIDEODIR + @"\*.jpeg");
                batchFileList.Add(@"del " + AUDIOFILE);
                batchFileList.Add(@"del " + VIDEOFILE);
                batchFileList.Add(@"del " + MOVIELISTFILE);

//)>
                File.WriteAllLines(BATCHFILE, batchFileList.ToArray());
            }
        }
//)>    v.2.02.05.001 Updated MediaRecorder feature. 
/*
        void timer1_Elapsed(object sender, ElapsedEventArgs e)
        {
            Rectangle bounds = Screen.PrimaryScreen.WorkingArea;// GetBounds(Point.Empty);
            if (counter == 0)
            {
                d = DateTime.Now;
            }
            using (Bitmap bitmap = new Bitmap(bounds.Width, bounds.Height))
            {
                using (Graphics g = Graphics.FromImage(bitmap))
                {
                    g.CopyFromScreen(Point.Empty, Point.Empty, bounds.Size);
                }
                string fileName = VIDEODIR + @"\test" + counter + ".png";
                bitmap.Save(fileName, ImageFormat.Png);

                var lastDiff = DateTime.Now.Subtract(prevDateTime);
                if (counter > 0)
                    MovieList.Add("duration " + String.Format("{0}.{1}", lastDiff.Seconds, lastDiff.Milliseconds));

                MovieList.Add("file '" + fileName + "'");
                prevDateTime = DateTime.Now;
                counter++;
            }
        }
 */
        static Size sz = Screen.PrimaryScreen.Bounds.Size;
        static Rectangle bounds = Screen.PrimaryScreen.WorkingArea;// GetBounds(Point.Empty);
        static object locker = new Object();
        
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern IntPtr keybd_event(int key, int dummy, int flags, IntPtr info);
        // P/Invoke declarations
        [DllImport("gdi32.dll")]
        static extern bool BitBlt(IntPtr hdcDest, int xDest, int yDest, int wDest, int hDest, IntPtr hdcSource, int xSrc, int ySrc, CopyPixelOperation rop);
        [DllImport("user32.dll")]
        static extern bool ReleaseDC(IntPtr hWnd, IntPtr hDc);
        [DllImport("gdi32.dll")]
        static extern IntPtr DeleteDC(IntPtr hDc);
        [DllImport("gdi32.dll")]
        static extern IntPtr DeleteObject(IntPtr hDc);
        [DllImport("gdi32.dll")]
        static extern IntPtr CreateCompatibleBitmap(IntPtr hdc, int nWidth, int nHeight);
        [DllImport("gdi32.dll")]
        static extern IntPtr CreateCompatibleDC(IntPtr hdc);
        [DllImport("gdi32.dll")]
        static extern IntPtr SelectObject(IntPtr hdc, IntPtr bmp);
        [DllImport("user32.dll")]
        public static extern IntPtr GetDesktopWindow();
        [DllImport("user32.dll")]
        public static extern IntPtr GetWindowDC(IntPtr ptr);

        void timer1_Elapsed2(object sender, ElapsedEventArgs e)
        {                                  
            timer1.Enabled = false;
            counter++;
            int LocalCounter = counter;
            if (LocalCounter == 0)
                return;
            
            string fileName = VIDEODIR + @"\test" + LocalCounter + ".jpeg";

            IntPtr hDesk = GetDesktopWindow();
            IntPtr hSrce = GetWindowDC(hDesk);
            IntPtr hDest = CreateCompatibleDC(hSrce);
            IntPtr hBmp = CreateCompatibleBitmap(hSrce, sz.Width, sz.Height);
            IntPtr hOldBmp = SelectObject(hDest, hBmp);
            bool b = BitBlt(hDest, 0, 0, sz.Width, sz.Height, hSrce, 0, 0, CopyPixelOperation.SourceCopy | CopyPixelOperation.CaptureBlt);
            Bitmap bmp = Bitmap.FromHbitmap(hBmp);
            SelectObject(hDest, hOldBmp);
            DeleteObject(hBmp);
            DeleteDC(hDest);
            ReleaseDC(hDesk, hSrce);
            //  bmp.Save(fileName, myImageCodecInfo, myEncoderParameters);
            //  bmp.Dispose();

            Task Task = new Task(() =>
            {
                bmp.Save(fileName, myImageCodecInfo, myEncoderParameters);
                bmp.Dispose();               
            });
            Tasks.Add(Task);
            Task.Start();
            
            var lastDiff = DateTime.Now.Subtract(prevDateTime);
            prevDateTime = DateTime.Now;

            lock (locker)
            {
                if (counter > 1)                   
                    MovieList.Add("duration " + String.Format("{0}", lastDiff.ToString(@"s\.fff")));

                if (counter > 0)
                    MovieList.Add("file '" + fileName + "'");
            }

            if (counter > 0)
                timer1.Enabled = true;                   
        }                
//)>

        private void RunBatchFile(string fileName)
        {
            var process = new Process();
            var startinfo = new ProcessStartInfo(fileName);
            startinfo.WindowStyle = ProcessWindowStyle.Hidden;
            startinfo.CreateNoWindow = true;
            process.StartInfo = startinfo;

//)>        v.2.02.05.001 Updated MediaRecorder feature.
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardError = true;
//)>
            process.Start();

//)>        v.2.02.05.001 Updated MediaRecorder feature.
            if (OnProgressChange != null)
            {
                bool bEncodingVideo = true;
                StreamReader stream = process.StandardError;
                while (!stream.EndOfStream)
                {
                    //Application.DoEvents();
                    string s = stream.ReadLine();
                    Trace.WriteLine(s);

                    if (s.Contains(@"Video\audio.wav"))
                    {
                        bEncodingVideo = false;
                        OnProgressChange(95, 95, 95);
                    }

                    if (s.StartsWith("frame=") && bEncodingVideo)
                    {
                        string[] sCurrentFrame = s.Split(' ');
                        int nFrame = 0;
                        for (int i = 0; i < sCurrentFrame.Length; i++)
                        {
                            if (Int32.TryParse(sCurrentFrame[i], out nFrame))
                            {
                                Trace.WriteLine(nFrame);
                                int nFramePercent = (int)(((float)nFrame / (float)MovieList.Count * 1.5) * 90);
                                if (nFramePercent > 92)
                                    nFramePercent = 92;
                                OnProgressChange(nFramePercent, nFramePercent, nFramePercent);
                                break;
                            }
                        }
                    }
                }
                OnProgressChange(100, 100, 100);
                Thread.Sleep(500);
            }
//)>
            process.WaitForExit();
        }


        private Timer timer1 = new Timer();
        private int counter = 0;
        private readonly SndRec _sndrec;
        private List<String> MovieList = new List<string>();
        private DateTime d;
        private DateTime prevDateTime;
    }
}
