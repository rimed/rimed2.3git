﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Expression.Encoder.Devices;
using Microsoft.Expression.Encoder.ScreenCapture;

namespace ScreenCapture_MedSim
{
    public class ScreenCaptureJobXesc: IScreenCapture
    {

        public event ProgressChange OnProgressChange;
        public event EventHandler OnFinished;

        public string Author { get; set; }

        public string Copyright { get; set; }

        public string Description { get; set; }

        public CaptureState State
        {
            get { return state; }
            set { state = value; }
        }

        public string Start(string fileName)
        {
            try
            {
                System.Drawing.Size monitorSize = SystemInformation.PrimaryMonitorSize;
                Rectangle capRect = new Rectangle(0, 0, monitorSize.Width, monitorSize.Height);

                Collection<EncoderDevice> audioDevices = EncoderDevices.FindDevices(EncoderDeviceType.Audio);
                EncoderDevice foundDevice = audioDevices.First(delegate(EncoderDevice item) { return item.Name.StartsWith(@"Speakers"); });

                string filePath = Path.GetDirectoryName(fileName);
                job.CaptureRectangle = capRect;
                job.ScreenCaptureVideoProfile.FrameRate = 10;
//                job.CaptureMouseCursor = true;
//                job.ShowFlashingBoundary = true;
                job.AddAudioDeviceSource(foundDevice);
                job.OutputPath = filePath;
                job.OutputScreenCaptureFileName = fileName;
                job.Start();
                State = CaptureState.Started;
                return "";
            }
            catch (Exception ex)
            {
                State = CaptureState.Stopped;
                return ex.Message;
            }
        }

        public void Stop()
        {
            job.Stop();
            State = CaptureState.Stopped;
        }

        public void Pause()
        {
            job.Pause();
            State = CaptureState.Paused;
        }

        ScreenCaptureJob job = new ScreenCaptureJob();
        private CaptureState state = CaptureState.Stopped;
    }
}
