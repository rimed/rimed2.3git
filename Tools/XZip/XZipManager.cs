﻿using System;
using System.IO;
using Ionic.Zip;
using Rimed.Framework.Common;


namespace Rimed.Tools.XZip
{
    public class XZipManager
    {
        static private readonly XZipManager s_xZipManager = new XZipManager();

        /// <summary>Instance of class</summary>
        static public XZipManager theXZip
        {
            get
            {
                return s_xZipManager;
            }
        }

        /// <summary>Create archive</summary>
        /// <param name="fileName">File name to archive</param>
        /// <param name="archiveFileName"></param>
        /// <param name="innerFileName"></param>
        /// <returns></returns>
        public bool ZipFile(string fileName, string archiveFileName, string innerFileName)
        {
            if (string.IsNullOrWhiteSpace(fileName) || string.IsNullOrWhiteSpace(archiveFileName) || string.IsNullOrWhiteSpace(innerFileName))
            {
                Logger.LogWarning("Unzip error: input parameters (fileName, archiveFileName or innerFileName) are empty or null.");
                return false;
            }

            if (!File.Exists(fileName))
            {
                Logger.LogWarning(string.Format("File ('{0}') not found.", fileName));
                return false;
            }

            try
            {
                using (var archiveFile = new ZipFile(archiveFileName))
                {
                    using (var ff = File.OpenRead(fileName))
                    {
                        archiveFile.AddEntry(innerFileName, ff);
                        archiveFile.Save();
                        ff.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                return false;
            }

            return true;
        }

        /// <summary>Unpack archive</summary>
        /// <param name="archiveFileName">Archive file name</param>
        /// <param name="fileName">File name to unpack</param>
        /// <returns></returns>
        public bool UnzipFile(string archiveFileName, string fileName)
        {
            if (string.IsNullOrWhiteSpace(archiveFileName) || string.IsNullOrWhiteSpace(fileName))
            {
                Logger.LogWarning("Unzip error: input parameters - archiveFileName or fileName are empty or null.");
                return false;
            }

            if (!File.Exists(archiveFileName))
            {
                Logger.LogWarning(string.Format("Unzip error: Archive file ('{0}') not found.", archiveFileName));
                return false;
            }

            if (!Ionic.Zip.ZipFile.IsZipFile(archiveFileName))
            {
                Logger.LogWarning(string.Format("Unzip error: Archive file ('{0}') is not valid.", archiveFileName));
                return false;
            }

            try
            {
                using (var zipFile = Ionic.Zip.ZipFile.Read(archiveFileName))
                {
                    if (zipFile.Count == 0)
                    {
                        Logger.LogInfo(string.Format("Unzip: Archive file ('{0}') is empty.", archiveFileName));
                        return false;
                    }

                    using (var fs = new FileStream(fileName, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite))
                    {
                        foreach (var entry in zipFile)
                        {
                            entry.Extract(fs);
                        }

                        fs.Close();
                    }
                }
            }
            catch(Exception ex)
            {
                Logger.LogError(ex);
                return false;
            }

            return true;
        }
    }
}
