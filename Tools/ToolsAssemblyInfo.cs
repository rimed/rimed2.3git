﻿//
// General Information about the solution
//

using System.Reflection;

[assembly: AssemblyCompany("Rimed ltd.")]
[assembly: AssemblyProduct("Rimed Tools")]
[assembly: AssemblyCopyright("Copyright © Rimed ltd. 2013")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

//
// Version information for an assembly consists of the following four values: Major.Minor.Build.Revision
//
[assembly: AssemblyFileVersion("2.0.6.0")]

// For AssemblyVersionAttribute specify all or default the Revision and Build Numbers by using the '*'.
[assembly: AssemblyVersion("2.0.*")]
