using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Diagnostics;
using Microsoft.ManagedSpy;
using System.Threading;

using System.IO;

namespace Rimed.Tools.SpyWrapper
{
	public partial class mainSpy : Form
	{
		private string[] m_names;

		private Process m_echoProc;

		public mainSpy()
		{
			InitializeComponent();
        }

		/*private ControlProxy FindSpecifiedControl(ControlProxy currentNode, string[] names, int pos, string filename)
		{
			ControlProxy[] proxies = currentNode.Children;
			foreach (ControlProxy proxy in proxies)
			{
				if (string.Equals(proxy.GetComponentName(), names[pos], StringComparison.OrdinalIgnoreCase))
				{
					TraceWriteLine(filename, String.Format("Find component {0};\r\n", proxy.GetComponentName()));
					if (pos + 1 < names.Length)
						return FindSpecifiedControl(proxy, names, pos + 1, filename);
					else
						return proxy;
				}
			}
			return null;
		}*/

		private ControlProxy findMinimizeButton(ControlProxy currentNode, string[] names, int pos, string filename)
		{
			if (pos + 1 < names.Length)
			{
				var proxies = currentNode.Children;
				foreach (var proxy in proxies)
				{
					if (string.Equals(proxy.GetComponentName(), names[pos], StringComparison.OrdinalIgnoreCase))
					{
                        File.AppendAllText(filename, String.Format("Find component {0};\r\n", proxy.GetComponentName()));
						return findMinimizeButton(proxy, names, pos + 1, filename);
					}
				}
			}
			else
				return findSpecifiedControl(currentNode, names[pos], filename);
			
			return null;
		}

		private ControlProxy findSpecifiedControl(ControlProxy currentNode, string name, string filename)
		{
			var proxies = currentNode.Children;
			foreach (var proxy in proxies)
			{
                File.AppendAllText(filename, string.Format("Find component {0};\r\n", proxy.GetComponentName()));
				if (string.Equals(proxy.GetComponentName(), name, StringComparison.OrdinalIgnoreCase))
					return proxy;

				var control = findSpecifiedControl(proxy, name, filename);
				if (control != null)
					return control;
			}

			return null;
		}

		private void mainSpy_Load(object sender, EventArgs e)
		{
		    var filename = Application.ExecutablePath + ".log";// Path.Combine(Application.StartupPath, "log.txt");
			File.Delete(filename);

            File.AppendAllText(filename, "Start to find Digi-Imager...\r\n");
			var list = new ControlList();
			m_names = list.GetControlNameList("EchoWave");
			var processes = Process.GetProcessesByName("EchoWave");
			if (processes.Length > 0)
			{
				m_echoProc = processes[0];
				if (m_echoProc != null && m_echoProc.Id != Process.GetCurrentProcess().Id)
				{
                    File.AppendAllText(filename, "Digi-Imager was finding...\r\n");

					int counter = 0;
					while (true)
					{
						counter++;
                        if (counter > 50)
                        {
                            File.AppendAllText(filename, "50 DKP minus! Spy was closed\r\n");
                            Close();
                        }

						var proxy = new ControlProxy(m_echoProc.MainWindowHandle);
                        File.AppendAllText(filename, string.Format("Current Main Window title is: {0}\r\n", m_echoProc.MainWindowTitle));
						var rez = findMinimizeButton(proxy, m_names, 0, filename);
						if (rez != null)
						{
							foreach (EventDescriptor ed in rez.GetEvents())
							{
								if (ed.Name.Equals("MouseClick"))
								{
									rez.SubscribeEvent(ed);
								}
							}
							rez.EventFired += new ControlProxyEventHandler(ProxyEventFired);

							break;
						}

						Thread.Sleep(2000);
                        File.AppendAllText(filename, counter + ". Minimize was not finded. Try again...\r\n");
						processes = Process.GetProcessesByName("EchoWave");
						m_echoProc = processes[0];
						if (m_echoProc == null)
						{
                            File.AppendAllText(filename, "Digi-Imager process was lost\r\n");
							break;
						}
					}
				}
                else
                    File.AppendAllText(filename, "Digi-Imager hasn't been found\r\n");
			}
			else
			{
                File.AppendAllText(filename, "Digi-Imager application is not running\r\n");
                MessageBox.Show("DigiLite™ IP application is not running");
			}
        
            Visible = false;
        }

        //private void TraceWriteLine(string filename, string line)
        //{
        //    File.AppendAllText(filename, line);
        //}

		/// <summary>This is called when the selected ControlProxy raises an event</summary>
		private void ProxyEventFired(object sender, ProxyEventArgs args)
		{
			IntPtr hWnd = Win32.FindWindow(null, "Main Menu");
			Win32.ShowWindow(hWnd, Win32.SW_SHOWNORMAL);
		}
	}
}