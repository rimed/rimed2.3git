using System;
using System.Windows.Forms;
using System.Threading;

namespace Rimed.Tools.SpyWrapper
{
	public static class Program
	{
		public const string SPY_WRAPPER_MUTEX_NAME = "Rimed.Tools.SpyWrapper:Mutex";

		[STAThread]
		static void Main()
		{
			var oneInstanceOnly = false;
            using (var mx = new Mutex(true, SPY_WRAPPER_MUTEX_NAME, out oneInstanceOnly))
			{
				if (oneInstanceOnly)
				{
					Application.EnableVisualStyles();
					Application.SetCompatibleTextRenderingDefault(false);
					Application.Run(new mainSpy());
				}
			}
		}
	}
}