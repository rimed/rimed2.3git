using System;
using System.Runtime.InteropServices;

namespace Rimed.Tools.SpyWrapper
{
    public static class Win32
    {
        public const int SW_HIDE = 0x0000;
        public const int SW_SHOWNORMAL = 0x0001;
        public const int SW_RESTORE = 0x0009;
        public const int WM_SHOWWINDOW = 0x0018;

        [DllImport("user32.dll")]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll")]
        internal static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool SendMessage(
            uint hWnd,
            int Msg,
            uint wParam,
            uint lParam);
    }
}
