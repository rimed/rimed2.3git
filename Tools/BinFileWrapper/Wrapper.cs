using System;
using System.IO;
using Rimed.Framework.Common;

namespace Rimed.Tools.BinFileWrapper
{
    public static class Wrapper
    {
        private const string FMT_TEMP_FILE_NAME     = "{0}.RimedTmp";
        private const string FMT_RESULT_FILE_NAME   = "{0}\\{1} v{2} [{3}].RIF";    //RimedImageFile
        private const string FMT_DESC_FILE_NAME     = "{0}.TXT";
        private const string FMT_DATETIME           = "yyyy.MM.dd HH:mm:ss";

		public static string WrapFile(string sourceFile, EFileType fileType, Files.Version fileversion, out string descFile, bool isWriteDescription = true)
        {
            if (string.IsNullOrWhiteSpace(sourceFile))
                throw new ApplicationException("Source file not selected.");

            if (!Files.IsPathValid(sourceFile))
                throw new ApplicationException(string.Format("Source file path ({0}) is not valid.", sourceFile));

            if (!File.Exists(sourceFile))
                throw new ApplicationException(string.Format("Source file ({0}) not exist.", sourceFile));

            var header = createHeader(sourceFile, fileType, fileversion);

            // Create temporary file
            var tmpFile = string.Format(FMT_TEMP_FILE_NAME, sourceFile);
            using (var fsWrite = new FileStream(tmpFile, FileMode.CreateNew, FileAccess.Write, FileShare.None))
            {
                using (var fsRead = new FileStream(sourceFile, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    // Write file header
                    var headerBuffer = header.GetBuffer();
                    fsWrite.Write(headerBuffer, 0, headerBuffer.Length);
                    fsWrite.Flush();

                    // Write source file
                    fsRead.CopyTo(fsWrite);
                    fsWrite.Flush();
                }
            }

            // Calculate temporary file Signature
            var sig = Files.SignatureToString(Files.GetFileSinature(tmpFile));
            if (string.IsNullOrWhiteSpace(sig))
                throw new ApplicationException("Error calculating result file signature.");

            // Rename temporary file (Signature inc. in name)
            var targetFile = string.Format(FMT_RESULT_FILE_NAME, Files.GetFilePath(tmpFile), header.FileType, header.FileVer, sig);
            if (!Files.Move(tmpFile, targetFile))
            {
                Files.Delete(tmpFile);

                throw new ApplicationException("Error renaming result file.");
            }

            descFile = null;
            if (isWriteDescription)
                descFile = writeDescription(sourceFile, ref header, targetFile, sig);

            return targetFile;
        }

        public static bool ValidateWrappedFile(string path)
        {
            if (!File.Exists(path))
            {
                Logger.LogError(string.Format("Wrapped file ({0}) not exist.", path));
                return false;
            }

            // Calculate file signature
            var sig = Files.GetFileSinature(path);
            if (sig == null)
            {
                Logger.LogError("Error calculating wrapped file signature.");
                return false;
            }

            // Check if file signature exists in file name
            if (path.IndexOf(Files.SignatureToString(sig), StringComparison.InvariantCultureIgnoreCase) < 0)
            {
                Logger.LogError("File signature mismatch");
                return false;
            }

            return validateWrappedFileHeader(path);
        }


        public static bool GetVerifiedHeader(string path, ref WrapperHeader header)
        {
            if (!File.Exists(path))
            {
                Logger.LogError(string.Format("Wrapped file ({0}) not exist.", path));
                return false;
            }

            // Calculate file signature
            var sig = Files.GetFileSinature(path);
            if (sig == null)
            {
                Logger.LogError("Error calculating wrapped file signature.");
                return false;
            }

            // Check if file signature exists in file name
            if (path.IndexOf(Files.SignatureToString(sig), StringComparison.InvariantCultureIgnoreCase) < 0)
            {
                Logger.LogError("File signature mismatch");
                return false;
            }

            var array = new byte[WrapperHeader.Size];
            using (var fsRead = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                var sourceSize = fsRead.Length - WrapperHeader.Size;
                if (sourceSize <= 0)
                {
                    Logger.LogError("File size mismatch");
                    return false;
                }

                // Read file header
                var count = fsRead.Read(array, 0, array.Length);
                if (count != array.Length)
                {
                    Logger.LogError("File header mismatch");
                    return false;
                }

                // Check if header is valid
                header.SetBuffer(array);
                if (!header.IsValid)
                {
                    Logger.LogError("File header is invalid.");

                    return false;
                }

                // Check source file size
                if (header.FileSize != sourceSize)
                {
                    Logger.LogError("Source file size mismatch");
                    return false;
                }

                // Check source file signature
                sig = Files.GetStreamSinature(fsRead);
                if (sig != null)
                {
                    var sigStr          = BitConverter.ToString(sig).Replace("-", "").ToLower();
                    var headerSigStr    = Files.SignatureToString(header.GetFileSignature());
                    if (string.Compare(sigStr, headerSigStr, StringComparison.InvariantCultureIgnoreCase) == 0)
                        return true;

                    Logger.LogError("Source file signature mismatch.");
                }
            }

            return false;
        }

       
        private static string writeDescription(string sourceFile, ref WrapperHeader header, string targetFile, string targetSignature)
        {
            var descFile = string.Format(FMT_DESC_FILE_NAME, targetFile);

            using (var outfile = new StreamWriter(descFile))
            {
                outfile.WriteLine("---------------------------------------------------------------------------------------------");
                outfile.WriteLine(" Rimed TCD system Rev6 binary file wrapper                               " + DateTime.UtcNow.ToString(FMT_DATETIME));
                outfile.WriteLine("---------------------------------------------------------------------------------------------");
                outfile.WriteLine("Work path:               '{0}\\'", Files.GetFilePath(sourceFile));
                outfile.WriteLine();
                outfile.WriteLine("Input file:              '{0}'", Files.GetFileName(sourceFile));
                outfile.WriteLine("Input file create date:  {0}", header.FileCreateTime.ToString(FMT_DATETIME));
                outfile.WriteLine("Input file size:         {0} Bytes", header.FileSize);
                outfile.WriteLine("Input file signature:    {0}", Files.SignatureToString(header.GetFileSignature()));
                outfile.WriteLine("Input file type:         {0}", header.FileType);
                outfile.WriteLine("Input file version:      v{0}", header.FileVer);
                outfile.WriteLine();
                outfile.WriteLine("Output file:             '{0}'", Files.GetFileName(targetFile));
                outfile.WriteLine("Output file create date: {0}", DateTime.UtcNow.ToString(FMT_DATETIME));
                outfile.WriteLine("Output file signature:   {0}", targetSignature);
                outfile.WriteLine();
            }

            return descFile;
        }

		private static WrapperHeader createHeader(string sourceFile, EFileType fileType, Files.Version fileversion)
        {
            var fileInfo = Files.GetFileInfo(sourceFile);
            if (fileInfo == null)
                throw new ApplicationException(string.Format("Error loading source file ({0}).", sourceFile));

            var sig = Files.GetFileSinature(sourceFile);
            if (sig == null || sig.Length != Files.FILE_SIGNATURE_BYTE_COUNT)
                throw new ApplicationException(string.Format("Error calculating source file ({0}) signature.", sourceFile));

            var header = new WrapperHeader(fileType, fileversion.Value, fileInfo.CreationTimeUtc, (uint) fileInfo.Length, sig);
            if (!header.IsValid)
                throw new ApplicationException("Wrapper header generation fail.");

            return header;
        }

        private static bool validateWrappedFileHeader(string path)
        {
            var header = new WrapperHeader();
            return GetVerifiedHeader(path, ref header);
        }
    }
}
