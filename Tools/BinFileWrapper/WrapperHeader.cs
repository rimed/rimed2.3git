using System;
using System.Runtime.InteropServices;
using System.Text;
using Rimed.Framework.Common;

namespace Rimed.Tools.BinFileWrapper
{
    public enum EFileType : byte
    {
        UNKNOWN = 0,
        DSP = 1,
        FPGA = 2
    }

    [StructLayout(LayoutKind.Explicit, Pack = 1, Size = STRUCT_SIZE, CharSet = CharSet.Ansi)]
    public unsafe struct WrapperHeader
    {
        private const int       STRUCT_SIZE             = 61;

        private const string    PRIFIX_STRING           = "Rimed.Tools.ImgFileWrapper";
        private const int       SIZE_FILE_SIGNATUREX    = Files.FILE_SIGNATURE_BYTE_COUNT;


        private static readonly DateTime s_refDate = new DateTime(2000, 01, 01, 0, 0, 0, DateTimeKind.Utc);
        
        [FieldOffset(0)]    public fixed byte   HeaderPrefix[26];                       // 0 ... 25  = PRIFIX_STRING.Length
        [FieldOffset(26)]   public byte         HeaderVersion;                          //26 ... 26
        [FieldOffset(27)]   public byte         HeaderSize;                             //27 ... 27
        [FieldOffset(28)]   public EFileType    FileType;                               //28 ... 28
		[FieldOffset(29)]	public Files.Version FileVer;                                //29 ... 36
        [FieldOffset(37)]   public uint         FileDate;                               //37 ... 40
        [FieldOffset(41)]   public uint         FileSize;                               //41 ... 44
        [FieldOffset(45)]   public fixed byte   FileSignature[SIZE_FILE_SIGNATUREX];    //45 ... 60

        /// <summary>Byte array union</summary>
        [FieldOffset(0)]    public fixed byte   Data[STRUCT_SIZE];                      // 0 .. 60



        private void        setPrefix()
        {
            var array = Encoding.ASCII.GetBytes(PRIFIX_STRING);
            fixed (byte* ptr = HeaderPrefix)
            {
                Marshal.Copy(array, 0, (IntPtr)ptr, array.Length);
            }
        }

        private string      getPrefix()
        {
            if (PRIFIX_STRING.Length >= Size)
                return string.Empty;

            var res = new byte[PRIFIX_STRING.Length];
            fixed (byte* ptr = HeaderPrefix)
            {
                Marshal.Copy((IntPtr)ptr, res, 0, res.Length);
            }

            return Encoding.ASCII.GetString(res);
        }

        private void        init()
        {
            setPrefix();
            HeaderVersion = 1;
            HeaderSize = (byte)Size;
        }


        public WrapperHeader(EFileType fileType, ulong fileVer, DateTime fileCreateDate, uint fileSize, byte[] fileSignature)
            : this()
        {
            init();

            FileType        = fileType;
            FileVer.Value = fileVer;
            FileCreateTime  = fileCreateDate;
            FileSize        = fileSize;
            SetFileSignature(fileSignature);
        }


        public              WrapperHeader(byte[] buffer): this()
        {
            SetBuffer(buffer);
        }

           
        public byte[]       GetBuffer()
        {
            var res = new byte[Size];
            fixed (byte* ptr = Data)
            {
                Marshal.Copy((IntPtr)ptr, res, 0, res.Length);
            }

            return res;
        }

        public void         SetBuffer(byte[] buffer)
        {
            if (buffer == null)
                throw new ArgumentNullException("buffer");

            var size = buffer.Length;
            if (size > Size)
                size = Size;

            fixed (byte* ptr = Data)
            {
                Marshal.Copy(buffer, 0, (IntPtr)ptr, size);
            }
        }

        /// <summary>Validate header size and prefix</summary>
        public bool         IsValid
        {
            get
            {
                if (HeaderSize != Size)
                    return false;

                return (string.Compare(getPrefix(), PRIFIX_STRING, StringComparison.Ordinal) == 0 && !FileVer.IsEmpty);
            }
        }

        public byte[]       GetFileSignature()
        {
            var sig = new byte[SIZE_FILE_SIGNATUREX];
            fixed (byte* ptr = FileSignature)
            {
                Marshal.Copy((IntPtr)ptr, sig, 0, sig.Length);
            }

            return sig;
        }

        public void         SetFileSignature(byte[] fileSignature)
        {
            if (fileSignature == null)
                throw new ArgumentNullException("fileSignature");

            if (fileSignature.Length != SIZE_FILE_SIGNATUREX)
                throw new ArgumentException(string.Format("File signature size must be {0} bytes long. Current signature size is {1} bytes.", SIZE_FILE_SIGNATUREX, fileSignature.Length), "fileSignature");

            fixed (byte* ptr = FileSignature)
            {
                Marshal.Copy(fileSignature, 0, (IntPtr)ptr, fileSignature.Length);
            }
        }

        public DateTime     FileCreateTime
        {
            get { return s_refDate.AddSeconds(FileDate); }
            set { FileDate = (uint)value.Subtract(s_refDate).TotalSeconds; }
        }


        public override string ToString()
        {
            return string.Format("FileType: {0}, FileVer: v{1}, FileDate: {2}, FileSize: {3}, FileSignature: '{4}', Header[Prefix: '{5}', Ver: {6}, Size: {7}], IsValid={8}"
                                 , FileType, FileVer, FileCreateTime.ToString("yyyy.MM.dd HH:mm:ss"), FileSize, Files.SignatureToString(GetFileSignature())
                                 , getPrefix(), HeaderVersion, HeaderSize
                                 , IsValid);
        }


        public static int Size { get { return sizeof(WrapperHeader); } }
    }
}
