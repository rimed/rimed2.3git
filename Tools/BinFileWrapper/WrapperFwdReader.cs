using System;
using System.IO;
using Rimed.Framework.Common;

namespace Rimed.Tools.BinFileWrapper
{
    public class WrapperFwdReader : Disposable
    {
        private readonly string m_targetFile;

        private Stream          m_fileStream;
        private bool            m_isInitiated;

        public WrapperFwdReader(string targetFile, int blockSize)
        {
            m_targetFile    = targetFile;
            BlockSize       = blockSize;
            BlockCount      = -1;
            CurrentBlock    = -1;
            IsValid         = false;
        }

        public bool     Init()
        {
            m_isInitiated = true;

            if (!Wrapper.ValidateWrappedFile(m_targetFile))
            {
                Logger.LogError(string.Format("Target file ({0}) validation fail.", m_targetFile));
                return false;
            }

            IsValid = setTotalBlockCount();

            return IsValid;
        }

        public bool     IsOpen          { get { return m_fileStream != null && m_fileStream.CanRead; } }
        public bool     IsLastBlock     { get { return CurrentBlock == BlockCount-1; } }
        public bool     IsValid         { get; private set; }
        public int      BlockSize       { get; private set; }
        public int      BlockCount      { get; private set; }
        public int      CurrentBlock    { get; private set; }

        public uint     SourceFileSize  { get; private set; }

        public WrapperHeader Header     { get; private set; }

        public bool     Open()
        {
            if (!m_isInitiated)
                Init();

            if (!IsValid)
                return false;

            m_fileStream = new FileStream(m_targetFile, FileMode.Open, FileAccess.Read, FileShare.Read);
            IsValid = IsOpen;

            if (IsOpen)
            {
                var headerBuff  = read(WrapperHeader.Size);
                Header          = new WrapperHeader(headerBuff);
                CurrentBlock    = -1;
            }

            return IsOpen;
        }

        public void     Close()
        {
            Header = new WrapperHeader();

            if (m_fileStream == null)
                return;

            m_fileStream.Close();
            m_fileStream.Dispose();
            m_fileStream = null;
        }

        public byte[]   ReadFirst()
        {
            if (!IsOpen)
                Open();

            if (!IsValid || !IsOpen)
                return new byte[0];

            m_fileStream.Position   = WrapperHeader.Size;
            CurrentBlock            = -1;

            return ReadNext();
        }

        public byte[]   ReadNext()
        {
            var array = read(BlockSize);
            if (array.Length > 0)
                CurrentBlock++;

            return array;
        }

        public int      CurrentBlockStart
        {
            get
            {
                if (BlockSize <= 0 || BlockCount <= 0 || CurrentBlock < 0)
                    return -1;

                return CurrentBlock * BlockSize;
            }
        }

        private bool    setTotalBlockCount()
        {
            if (BlockSize <= 0)
                return false;

            var fi = Files.GetFileInfo(m_targetFile);
            if (fi == null)
                return false;

            var size = fi.Length - WrapperHeader.Size;
            if (size <= 0)
                return false;

            SourceFileSize  = (uint) size;
            BlockCount      = (int)((SourceFileSize / BlockSize) + 1);

            return (BlockCount > 0);
        }

        private byte[]  read(int count)
        {
            if (!IsOpen)
                Open();

            if (!IsValid || !IsOpen)
                return new byte[0];

            var array = new byte[count];
            var readSize = m_fileStream.Read(array, 0, array.Length);

            if (readSize == 0)
                return new byte[0];

            if (readSize != array.Length)
            {
                var newArr = new byte[readSize];
                Array.Copy(array, newArr, newArr.Length);

                array = newArr;
            }

            return array;
        }



        protected override void DisposeManagedResources()
        {
            Close();
            base.DisposeManagedResources();
        }
    }
}
