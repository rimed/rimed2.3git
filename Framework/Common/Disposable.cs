namespace Rimed.Framework.Common
{
    public abstract class Disposable : DisposableBase
    {
        protected virtual void DisposeManagedResources()
        {
        }

        protected virtual void DisposeUnmanagedResources()
        {
        }

        /// <summary>NOTE: DO NOT Override this method. Override DisposeManagedResources() and / or DisposeUnmanagedResources methods as needed</summary>
        /// <param name="isDisposing"></param>
        protected override void Dispose(bool isDisposing)
        {
            if (isDisposing)
            {
                DisposeManagedResources();
            }

            DisposeUnmanagedResources();
        }
    }
}
