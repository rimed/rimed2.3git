﻿using System;
using System.Linq;
using System.Reflection;
using JetBrains.Annotations;

namespace Rimed.Framework.Common
{
	[UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
	public static class ExtensionMethods
	{
		private static readonly MethodInfo s_enumerableCast = typeof(Enumerable).GetMethod("Cast");
		private static readonly MethodInfo s_enumerableToArray = typeof(Enumerable).GetMethod("ToArray");

		public static bool TryDispose(this IDisposable disposable)
		{
			if (disposable == null)
				return false;

			try
			{
				disposable.Dispose();
				return true;
			}
			catch (Exception ex)
			{
                System.Diagnostics.Trace.WriteLine(string.Format("TryDispose: An exception was raised when disposing of \"{0}\": {1}", disposable.GetType().Name, ex));
			}

			return false;
		}
	}
}
