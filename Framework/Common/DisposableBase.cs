﻿using System;
using System.Threading;

namespace Rimed.Framework.Common
{
    /// <summary>Implement IDisposable pattern</summary>
    public abstract class DisposableBase : IDisposable
    {
        // A derived class should not be able to override this method.
        public void Dispose()
        {
            // Check that Dispose method has NOT been called
            // NOTE: DO NOT implement in Dispose(bool) since derived classes need to invoke base.Dispose(bool).
            if (checkIsDisposed())
                return;

            // Object cleanup
            Dispose(true);

            // Call GC.SupressFinalize to take this object off the finalization queue.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // isDisposing == true:  method has been called by a user's code. Managed and unmanaged resources can be disposed.
        // isDisposing == false: method has been called by the runtime from inside the finalizer and you should not reference other objects. Only unmanaged resources can be disposed.
        /// <summary>Object cleanup</summary>
        protected abstract void Dispose(bool isDisposing);

        /// <summary>C# destructor: Run only if the Dispose method does not get called.</summary>
        /// <remarks>NOTE: Do not provide destructors in types derived from this class.</remarks>
        ~DisposableBase()
        {
            // Check that Dispose method has NOT been called
            // NOTE: DO NOT implement in Dispose(bool) since derived classes need to invoke base.Dispose(bool).
            if (checkIsDisposed())
                return;

            // Object cleanup
            Dispose(false);
        }

        public bool IsDisposed
        {
            get { return (m_iIsDisposedFlag > 0); }
        }

        private int m_iIsDisposedFlag = 0;

        /// <summary>Verify that Dispose method executed only once. Incriment dispose ref count.</summary>
        private bool checkIsDisposed()
        {
            // Verify that Dispose method executed only once.
            var iIsDisposed = Interlocked.Increment(ref m_iIsDisposedFlag);
            return (iIsDisposed != 1);
        }
    }
}
