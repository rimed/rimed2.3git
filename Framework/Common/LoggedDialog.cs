using System;
using System.Windows.Forms;

namespace Rimed.Framework.Common
{
	public static class LoggedDialog
    {
        private const string        DEFAULT_CAPTION         = "Rimed ltd.";

        private static string       s_baseCaption           = string.Empty;
        private static string       s_fatalErrorCaption     = "Fatal error";
        private static string       s_errorCaption          = "Error";
        private static string       s_warningCaption        = "Warning";
        private static string       s_infoCaption           = "Information";
        private static string       s_notificationCaption   = "Notification";
        private static string       s_questionCaption       = "Question";
        private static string       s_confirmationCaption   = "Confirmation";

		private static DialogResult topMostMessageBox(string text, string caption = null, MessageBoxButtons buttons = MessageBoxButtons.OK, MessageBoxIcon icon = MessageBoxIcon.Information, MessageBoxDefaultButton defaultButton = MessageBoxDefaultButton.Button1)
		{
			var result = DialogResult.None;
			using (var topmostForm = new Form { Size = new System.Drawing.Size(1, 1), StartPosition = FormStartPosition.Manual })
			{
				if (topmostForm.InvokeRequired)
					topmostForm.Invoke((Action)delegate() { result = topMostMessageBox(text, caption, buttons, icon, defaultButton); });
				else
				{
					WinAPI.User32.SetWindowTopMost(topmostForm.Handle, false);
					result = MessageBox.Show(topmostForm, text, caption, buttons, icon, defaultButton);
				}
			}

			return result;
		}


        public static string BaseCaption
        {
            get { return s_baseCaption; }
            set { s_baseCaption = (string.IsNullOrWhiteSpace(value) ? string.Empty : value.Trim()); }
        }

        public static string FatalErrorCaption
        {
            get { return s_fatalErrorCaption; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    return;

                s_fatalErrorCaption = value;
            }
        }

        public static string ErrorCaption
        {
            get { return s_errorCaption; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    return;

                s_errorCaption = value;
            }
        }

        public static string WarningCaption
        {
            get { return s_warningCaption; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    return;

                s_warningCaption = value;
            }
        }

        public static string InformationCaption
        {
            get { return s_infoCaption; }
            set { s_infoCaption = ((value == null) ? string.Empty : value); }
        }

        public static string NotificationCaption
        {
            get { return s_notificationCaption; }
            set { s_notificationCaption = ((value == null) ? string.Empty : value); }
        }

        public static string ConfirmationCaption
        {
            get { return s_confirmationCaption; }
            set { s_confirmationCaption = ((value == null) ? string.Empty : value);}
        }

        public static string QuestionCaption
        {
            get { return s_questionCaption; }
            set { s_questionCaption = ((value == null) ? string.Empty : value);}
        }


		public static DialogResult Show(Form owner, string text, string caption = null, MessageBoxButtons buttons = MessageBoxButtons.OK, MessageBoxIcon icon = MessageBoxIcon.Information, MessageBoxDefaultButton defaultButton = MessageBoxDefaultButton.Button1, Exception exception = null)
		{
			return Show(text, caption, buttons, icon, defaultButton, exception, owner);
		}
		public static DialogResult Show(string text, string caption = null, MessageBoxButtons buttons = MessageBoxButtons.OK, MessageBoxIcon icon = MessageBoxIcon.Information, MessageBoxDefaultButton defaultButton = MessageBoxDefaultButton.Button1, Exception exception = null, Form owner = null)
		{
			var res = DialogResult.None;
			if (owner != null && owner.InvokeRequired)
			{
				owner.BeginInvoke((Action)delegate() { res = Show(text, caption, buttons, icon, defaultButton, exception, owner); });
				return res;
			}

			if (!string.IsNullOrWhiteSpace(BaseCaption))
				caption = string.Format("{0}{1} {2}", BaseCaption, string.IsNullOrWhiteSpace(BaseCaption) ? "" : ":", caption);

			if (caption == null)
				caption = DEFAULT_CAPTION;

			if (exception != null)
				Logger.LogError(exception);

			try
			{
				if (owner == null || !WinAPI.User32.IsWindowTopMost(owner.Handle)) //Bug# 329: All Studies - "Unsaved tests"pop-up stuck under application
				{
					res = topMostMessageBox(text, caption, buttons, icon, defaultButton); //Bug# 271: Report Generator Wizard - "Are you sure..." massage stuck under application
				}
				else
				{
					res = MessageBox.Show(owner, text, caption, buttons, icon, defaultButton);
					owner.Focus();
				}
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);
			}
			finally
			{
				Logger.LogInfo("LoggedDialog('{0}', '{1}'). User action: {2}.", caption.Replace("\n", "<CR>"), text.Replace("\n", "<CR>"), res);
			}

			return res;
		}
	
		public static DialogResult ShowFatalError(Form owner, string text, string caption = null, Exception exception = null)
		{
			return ShowFatalError(text, caption, exception, owner);
		}
		public static DialogResult ShowFatalError(string text, string caption = null, Exception exception = null, Form owner = null)
		{
			if (caption == null)
				caption = FatalErrorCaption;

			if (exception != null)
				Logger.LogFatalError(exception);

			return Show(text, caption, icon: MessageBoxIcon.Stop, owner: owner);
		}

		public static DialogResult ShowError(Form owner, string text, string caption = null, MessageBoxButtons buttons = MessageBoxButtons.OK, MessageBoxDefaultButton defaultButton = MessageBoxDefaultButton.Button1, Exception exception = null)
		{
			return Show(text, caption, buttons, MessageBoxIcon.Error, defaultButton, exception, owner);
		}
		public static DialogResult ShowError(string text, string caption = null, MessageBoxButtons buttons = MessageBoxButtons.OK, MessageBoxDefaultButton defaultButton = MessageBoxDefaultButton.Button1, Exception exception = null, Form owner = null)
		{
			if (caption == null)
				caption = ErrorCaption;

			return Show(text, caption, buttons, MessageBoxIcon.Error, defaultButton, exception, owner);
		}

		public static DialogResult ShowWarning(Form owner, string text, string caption = null, MessageBoxButtons buttons = MessageBoxButtons.OK, MessageBoxDefaultButton defaultButton = MessageBoxDefaultButton.Button1, Exception exception = null)
		{
			return ShowWarning(text, caption, buttons, defaultButton, exception, owner);
		}
		public static DialogResult ShowWarning(string text, string caption = null, MessageBoxButtons buttons = MessageBoxButtons.OK, MessageBoxDefaultButton defaultButton = MessageBoxDefaultButton.Button1, Exception exception = null, Form owner = null)
		{
			if (caption == null)
				caption = WarningCaption;

			return Show(text, caption, buttons, MessageBoxIcon.Warning, defaultButton, exception, owner);
		}

		public static DialogResult ShowNotification(Form owner, string text, string caption = null, MessageBoxButtons buttons = MessageBoxButtons.OK, MessageBoxDefaultButton defaultButton = MessageBoxDefaultButton.Button1, Exception exception = null)
		{
			return ShowNotification(text, caption, buttons, defaultButton, exception, owner);
		}
		public static DialogResult ShowNotification(string text, string caption = null, MessageBoxButtons buttons = MessageBoxButtons.OK, MessageBoxDefaultButton defaultButton = MessageBoxDefaultButton.Button1, Exception exception = null, Form owner = null)
		{
			if (caption == null)
				caption = s_notificationCaption;

			return Show(text, caption, buttons, MessageBoxIcon.Exclamation, defaultButton, exception, owner);
		}

		public static DialogResult ShowQuestion(Form owner, string text, string caption = null, MessageBoxButtons buttons = MessageBoxButtons.YesNo, MessageBoxDefaultButton defaultButton = MessageBoxDefaultButton.Button1)
		{
			return ShowQuestion(text, caption, buttons, defaultButton, owner);
		}
		public static DialogResult ShowQuestion(string text, string caption = null, MessageBoxButtons buttons = MessageBoxButtons.YesNo, MessageBoxDefaultButton defaultButton = MessageBoxDefaultButton.Button1, Form owner = null)
		{
			if (caption == null)
				caption = QuestionCaption;

			return Show(text, caption, buttons, MessageBoxIcon.Question, defaultButton, owner: owner);
		}

		public static DialogResult ShowConfirmation(Form owner, string text, string caption = null, MessageBoxButtons buttons = MessageBoxButtons.YesNoCancel, MessageBoxDefaultButton defaultButton = MessageBoxDefaultButton.Button1)
		{
			return ShowConfirmation(text, caption, buttons, defaultButton, owner);
		}
		public static DialogResult ShowConfirmation(string text, string caption = null, MessageBoxButtons buttons = MessageBoxButtons.YesNoCancel, MessageBoxDefaultButton defaultButton = MessageBoxDefaultButton.Button1, Form owner = null)
		{
			if (caption == null)
				caption = ConfirmationCaption;

			return Show(text, caption, buttons, MessageBoxIcon.Question, defaultButton, owner: owner);
		}

		public static DialogResult ShowInformation(Form owner, string text, string caption = null, MessageBoxButtons buttons = MessageBoxButtons.OK, MessageBoxDefaultButton defaultButton = MessageBoxDefaultButton.Button1)
		{
			return ShowInformation(text, caption, buttons, defaultButton, owner);
		}
		public static DialogResult ShowInformation(string text, string caption = null, MessageBoxButtons buttons = MessageBoxButtons.OK, MessageBoxDefaultButton defaultButton = MessageBoxDefaultButton.Button1, Form owner = null)
		{
			if (caption == null)
				caption = InformationCaption;

			return Show(text, caption, buttons, MessageBoxIcon.Information, defaultButton, owner: owner);
		}
	}
}
