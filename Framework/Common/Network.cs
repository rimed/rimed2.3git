﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading;

namespace Rimed.Framework.Common
{
	public static class Network
	{
		/// <summary>Check whether an IP address is local.</summary>
		/// <param name="host">IP address / computer name</param>
		/// <returns></returns>
		/// <see cref="http://www.csharp-examples.net/local-ip/"/>
		public static bool IsLocalIpAddress(string host)
		{
			try
			{
				// get host IP addresses
				var hostIPs = Dns.GetHostAddresses(host);

				// get local IP addresses
				var localIPs = Dns.GetHostAddresses(Dns.GetHostName());

				// test if any host IP equals to any local IP or to localhost
				foreach (var hostIp in hostIPs)
				{
					// is localhost
					if (IPAddress.IsLoopback(hostIp))
						return true;

					// is local address
					foreach (var localIp in localIPs)
					{
						if (hostIp.Equals(localIp))
							return true;
					}
				}
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);
			}

			return false;
		}

		public static PingReply Ping(string address, int timeoutMs = 2500)
		{
			if (string.IsNullOrWhiteSpace(address))
				address = string.Empty;

			IPAddress ip;
			if (!IPAddress.TryParse(address, out ip))
			{
				Logger.LogWarning("Ping address (IP={0}) is invalid.", address);
				return null;
			}

			return Ping(ip, timeoutMs);
			
		}

		public static PingReply Ping(IPAddress address, int timeoutMs = 2500)
		{
			PingReply reply;

			try
			{
				using (var pingSender = new Ping())
				{

                    reply = pingSender.Send(address, timeoutMs);
				}
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);
				return null;
			}

			return reply;
		}

	}
}
