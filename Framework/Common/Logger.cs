using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using log4net;
using log4net.Appender;

namespace Rimed.Framework.Common
{
	public class Logger
	{
		//TODO, Ofer: Add default logger(s) - DebugTraceOut(INFO), EventLog(WARN), File(ERROR) , when configuration file not found / not contain log4net section
		private static ILog s_log = LogManager.GetLogger(typeof (Logger));

		private const string TRACE_PREFIX = "[RIMED]";

		public static void SystemInfoLog(string msg)
		{
			if (IsInfoEnabled)
				Log.Info(flatMessage(msg));
		}

		public static void LogError(string message, Exception ex = null)
		{
			LogError(ex, message);
		}

		public static void LogError(Exception ex, string message = null)
		{
			if (Log == null)
				return;

			message = ExceptionToString(ex, message);
			Log.Error(message);
		}

		public static void LogFatalError(string message)
		{
			if (string.IsNullOrWhiteSpace(message))
				return;

			LogFatalError(null, message);
		}

		public static void LogFatalError(Exception ex, string message = null)
		{
			if (Log == null)
				return;

			message = ExceptionToString(ex, message);
			Log.Fatal(message);
		}

		public static void LogWarning(Exception ex, string msg = null)
		{
			if (ex == null)
				return;

			msg = ExceptionToString(ex, msg);
			LogWarning(msg);
		}

		public static void LogWarning(string format, params object[] args)
		{
			if (Log == null)
				return;

			if (args == null || args.Length == 0)
				Log.Warn(format);
			else
				Log.Warn(string.Format(format, args));
		}

		public static void LogInfo(string format, params object[] args)
		{
			if (!IsInfoEnabled)
				return;

			if (args == null || args.Length == 0)
				Log.Info(format);
			else
				Log.Info(string.Format(format, args));
		}


		#region Trace (DebugStringOut)
		public static void LogTrace(string text, string frmName = "")
		{
			LogTrace(ETraceLevel.L2, frmName, flatMessage(text));
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="level">Message level (filtered out if higher than TraceFilter)</param>
		/// <param name="componentName">Object name</param>
		/// <param name="format">String format / text</param>
		/// <param name="args">String format parameters</param>
		public static void LogTrace(ETraceLevel level, string componentName, string format, params object[] args)
		{
			if (Log == null)
				return;

			if (!IsLogTrace(level) || string.IsNullOrWhiteSpace(format))
				return;

			if (args != null && args.Length > 0)
				format = string.Format(format, args);

			var msg = string.Format("[{0}] {1}.{2}", level, componentName, format);
			if (level >= ETraceLevel.RT0)
				Trace(msg);
			else
				Log.Debug(msg);
		}

		public static void Trace(string msg)
		{
			if (string.IsNullOrWhiteSpace(msg))
				return;

			System.Diagnostics.Trace.WriteLine(TRACE_PREFIX + " " + msg);
		}

		public static void Trace(string format, params object[] args)
		{
			if (string.IsNullOrWhiteSpace(format))
				return;

			if (args != null && args.Length > 0)
				format = string.Format(format, args);

			Trace(format);
		}

		#endregion

		public static void LogDebug(string format, params object[] args)
		{
			if (!IsDebugEnabled || string.IsNullOrWhiteSpace(format))
				return;

			if (args != null && args.Length > 0)
				format = string.Format(format, args);

			Log.Debug(format);
		}

		public static bool IsInfoEnabled
		{
			get { return Log != null && Log.IsInfoEnabled; }
		}

		public static bool IsDebugEnabled
		{
			get { return Log != null && Log.IsDebugEnabled; }
		}

		public static bool IsLogTrace(ETraceLevel level)
		{
			return IsDebugEnabled && level <= TraceFilter && level != ETraceLevel.NONE;
		}

		public enum ETraceLevel
		{
			NONE = 0,
			L1 = 1, // Basic
			L2 = 2, // Normal (inc Basic)
			L3 = 3, // Detailed (inc. Normal)
			L4 = 4,
			L5 = 5,
			L6 = 6,
			L7 = 7,
			L8 = 8,
			L9 = 9,

			RT0 = 10, // Realtime, Not written to log - Trace.WriteLine
			RT1 = 11, // Realtime, Not written to log - Trace.WriteLine
			RT2 = 12, // Realtime, Not written to log - Trace.WriteLine

			ENTER = L9,
			LEAVE = L9,
		}

		public static ILog Log
		{
			get { return s_log; }
		}

		private static ETraceLevel s_traceFilter = ETraceLevel.L3;

		public static ETraceLevel TraceFilter
		{
			get { return s_traceFilter; }
			set
			{
				if (s_traceFilter == value)
					return;

				s_traceFilter = value;
				LogInfo("Logger TraceFilter updated: {0}.", GetDescription());
			}
		}

		public static void SetTraceFilter(string traceFilter)
		{
			ETraceLevel level;
			if (Enum.TryParse(traceFilter, true, out level))
				TraceFilter = level;
		}

		public static string GetDescription()
		{
			if (s_log == null)
				return string.Empty;

			var fmt = "Name: {0}, Base threshold: {1}";
			if (s_log.IsDebugEnabled)
				fmt += string.Format(", TraceLevel: {0}", s_traceFilter);

			return string.Format(fmt, s_log.Logger.Name, s_log.Logger.Repository.Threshold);
		}

		private static void addExceptionDesc(Exception ex, StringBuilder sb)
		{
			if (ex == null || sb == null)
				return;

			sb.Append(flatMessage(ex.Message));
			var hRes = getExceptionHResult(ex);
			if (hRes != 0)
				sb.Append(string.Format(" (ErrorCode: 0x{0:X8}).", hRes));

			sb.Append("\n\t").Append(ex);

			if (ex is OutOfMemoryException)
				sb.Append(" [").Append(getMemoryStatus()).Append("]");
		}

		private static string getMemoryStatus()
		{
			try
			{
				var proc = System.Diagnostics.Process.GetCurrentProcess();
				var sb = new StringBuilder(512);
				sb.Append("Total memory: ").Append(GC.GetTotalMemory(true));
				sb.Append(", MinWorkingSet: ").Append(proc.MinWorkingSet);
				sb.Append(", MaxWorkingSet: ").Append(proc.MaxWorkingSet);
				sb.Append(", WorkingSet: ").Append(proc.WorkingSet64);
				sb.Append(", PeakWorkingSet: ").Append(proc.PeakWorkingSet64);

				sb.Append(", HandleCount: ").Append(proc.HandleCount);
				sb.Append(", Threads.Count: ").Append(proc.Threads.Count);

				sb.Append(", PagedSystemMemorySize: ").Append(proc.PagedSystemMemorySize64);
				sb.Append(", NonpagedSystemMemorySize: ").Append(proc.NonpagedSystemMemorySize64);
				sb.Append(", PrivateMemorySize: ").Append(proc.PrivateMemorySize64);

				sb.Append(", PagedMemorySize: ").Append(proc.PagedMemorySize64);
				sb.Append(", VirtualMemorySize: ").Append(proc.VirtualMemorySize64);
				sb.Append(", PeakVirtualMemorySize: ").Append(proc.PeakVirtualMemorySize64);

				return sb.ToString();
			}
			catch
			{
				return string.Empty;
			}
		}

		private static readonly PropertyInfo s_hResultPropInfo;
        static Logger()
        {
            const BindingFlags BINDING_FLAGES = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;
            s_hResultPropInfo = typeof(Exception).GetProperty("HResult", BINDING_FLAGES);

            TraceFilter = ETraceLevel.L3;

            var ass = Assembly.GetEntryAssembly();      // Assembly.GetCallingAssembly();
			if (ass != null)
				setConfigFile(string.Format("{0}.config", ass.Location));
        }

        private static int getExceptionHResult(Exception ex)
        {
            if (s_hResultPropInfo == null)
                return 0;

            var hRes = 0;
            var obj = s_hResultPropInfo.GetValue(ex, null);
            if (obj != null)
                int.TryParse(obj.ToString(), out hRes);

            return hRes;
        }

        public static string ExceptionToString(Exception ex, string msg = null)
        {
            msg = flatMessage(msg);

            if (ex == null)
                return msg;

            // Add message(s)
            var sb = new StringBuilder(1024);
	        if (!string.IsNullOrWhiteSpace(msg))
				sb.Append(msg).Append("\n\t");

			addExceptionDesc(ex, sb);
			var innerEx = ex.InnerException;
			while (innerEx != null)
			{
				sb.Append("\n\tInner: ");
				addExceptionDesc(innerEx, sb);
				innerEx = innerEx.InnerException;
			}

            // Add Exception data.
            if (ex.Data.Count > 0)
            {
                sb.Append("\n\tData: ");
                foreach (DictionaryEntry  kv in ex.Data)
                {
                    sb.Append(string.Format("({0}={1}) ", kv.Key, kv.Value));
                }
            }

            return sb.ToString();
        }

        private static string flatMessage(string msg)
        {
            if (string.IsNullOrWhiteSpace(msg))
                return string.Empty;

            return msg.Trim().Replace("\n", "{CR}").Replace("\t", "{TAB}");
        }

        private static void setFileAppendersLoccation(string appenderName = null, string newPath = null, string newName = null)
        {
			if (Log == null)
				return;

			if (string.IsNullOrWhiteSpace(newPath) && string.IsNullOrWhiteSpace(newName))
                return;

			var loggers = Log.Logger.Repository.GetCurrentLoggers();
            foreach (var logger in loggers)
            {
                var hierarchy = logger.Repository as log4net.Repository.Hierarchy.Hierarchy;
                if (hierarchy == null)
                    continue;

                var appenders = hierarchy.Root.Appenders;
                foreach (var appender in appenders)
                {
                    var fileAppender = appender as FileAppender;
                    if (fileAppender == null)
                        continue;

                    if (!string.IsNullOrWhiteSpace(appenderName) && string.Compare(appenderName, fileAppender.Name, StringComparison.OrdinalIgnoreCase) != 0)
                        continue;

                    if (string.IsNullOrWhiteSpace(newPath))
                        newPath = Files.GetFilePath(fileAppender.File);

                    if (string.IsNullOrWhiteSpace(newName))
                        newName = Files.GetFileName(fileAppender.File);

                    fileAppender.File = Path.Combine(newPath, newName);
                    fileAppender.ActivateOptions();
                }
            }
        }

        private static void setConfigFile(string fileName)
        {
            try
            {
                var fileInfo = Files.GetFileInfo(fileName);
                if (fileInfo != null)
                {
                    log4net.Config.DOMConfigurator.ConfigureAndWatch(fileInfo);
                    System.Diagnostics.Trace.WriteLine("log4net configuration file changed: '{0}'." + fileName);
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("log4net configuration error. "+ ex.Message);
            }
        }

		public static string GetFileAppenderLocation(string appenderName)
		{
			if (Log == null || string.IsNullOrWhiteSpace(appenderName))
				return null;

			var loggers = Log.Logger.Repository.GetCurrentLoggers();
			foreach (var logger in loggers)
			{
				var hierarchy = logger.Repository as log4net.Repository.Hierarchy.Hierarchy;
				if (hierarchy == null)
					continue;

				var appenders = hierarchy.Root.Appenders;
				foreach (var appender in appenders)
				{
					var fileAppender = appender as FileAppender;
					if (fileAppender == null)
						continue;

					if (!string.IsNullOrWhiteSpace(appenderName) && string.Compare(appenderName, fileAppender.Name, StringComparison.OrdinalIgnoreCase) != 0)
						continue;

					return fileAppender.File;
				}
			}

			return null;
		}

		public static void Shutdown()
		{
			LogWarning("Logger Shutdown.");

			var prevLogger	= Log;
			s_log			= null;
			prevLogger.Logger.Repository.Shutdown();
		}
    }
}
