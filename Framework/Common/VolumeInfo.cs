using System;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace Rimed.Framework.Common
{
	/// <summary>Represents the different types of drives that may exist in a system.</summary>
	internal enum VolumeTypes
	{
		Unknown,	// The drive type cannot be determined. 
		Invalid,	// The root path is invalid. For example, no volume is mounted at the path. 
		Removable,	// The disk can be removed from the drive. 
		Fixed,		// The disk cannot be removed from the drive. 
		Remote,		// The drive is a remote (network) drive. 
		CDROM,		// The drive is a CD-ROM drive. 
		RAMDisk		// The drive is a RAM disk. 
	};

	/// <summary>Represents the different supporting flags that may be set on a file system.</summary>
	[Flags]
	internal enum VolumeFlags
	{
		Unknown					= 0x0,
		CaseSensitive			= 0x00000001,
		Compressed				= 0x00008000,
		PersistentACLS			= 0x00000008,
		PreservesCase			= 0x00000002,
		ReadOnly				= 0x00080000,
		SupportsEncryption		= 0x00020000,
		SupportsFileCompression	= 0x00000010,
		SupportsNamedStreams	= 0x00040000,
		SupportsObjectIDs		= 0x00010000,
		SupportsQuotas			= 0x00000020,
		SupportsReparsePoints	= 0x00000080,
		SupportsSparseFiles		= 0x00000040,
		SupportsUnicodeOnVolume	= 0x00000004
};

	/// <summary>Presents information about a volume.</summary>
	public class VolumeInfo
	{
        #region Private Constants
		private const int NAMESIZE = 80;
		private const int MAX_PATH = 256;
		private const int FILE_ATTRIBUTE_NORMAL = 128;
		private const int SHGFI_USEFILEATTRIBUTES = 16;
		private const int SHGFI_ICON = 256;
		private const int SHGFI_LARGEICON = 0;
		private const int SHGFI_SMALLICON = 1;
        #endregion

        #region Private Structures
        [StructLayout(LayoutKind.Sequential)]
		private class UniversalNameInfo
		{ 
			public string NetworkPath = null;
		}

		[StructLayout ( LayoutKind.Sequential, CharSet=CharSet.Ansi )]
		public struct SHFILEINFOA
		{ 
			public IntPtr   hIcon; 
			public int      iIcon; 
			public uint     dwAttributes; 
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst=MAX_PATH)]
			public string szDisplayName; 
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst=NAMESIZE)]
			public string szTypeName; 
		}

		[StructLayout ( LayoutKind.Sequential, CharSet=CharSet.Unicode )]
		public struct SHFILEINFO
		{ 
			public IntPtr   hIcon; 
			public int      iIcon; 
			public uint   dwAttributes; 
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst=MAX_PATH)]
			public string szDisplayName; 
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst=NAMESIZE)]
			public string szTypeName; 
		}
        #endregion

        #region Private Enums
		private enum UniInfoLevels
		{
			Universal=1,
			Remote=2
		}
        #endregion

        #region Method Imports
		[DllImport("mpr.dll")]
		private static extern UInt32 WNetGetUniversalName( string driveLetter, UniInfoLevels infoLevel, IntPtr ptr, ref UInt32 uniSize );
		
        [DllImport("kernel32.dll")]
		private static extern long GetDriveType(string driveLetter);
		
        [DllImport("Shell32.dll")]
		private static extern IntPtr SHGetFileInfo( string pszPath,	uint dwFileAttributes, ref SHFILEINFO psfi,	uint cbFileInfo, uint uFlags );
		
        [DllImport("kernel32.dll")]
		private static extern long GetVolumeInformation(string pathName, StringBuilder volumeNameBuffer, UInt32 volumeNameSize, ref UInt32 volumeSerialNumber, ref UInt32 maximumComponentLength, ref UInt32 fileSystemFlags, StringBuilder fileSystemNameBuffer, UInt32 fileSystemNameSize);
        #endregion

        #region Member Variables
		private readonly Uri m_uri;
	    private VolumeTypes m_volType;
		private UInt32 m_serNum;
		private UInt32 m_maxCompLen;
		private VolumeFlags m_volFlags;
		private string m_fileSysName;
        #endregion

        /**********************************************************
		 * Constructors
		 *********************************************************/
		private VolumeInfo(Uri uri)
		{
			// Make sure we were passed something
			if (uri == null)
			{
                Logger.LogError("VolumeInfo(Uri) input parameter is null");
                throw new ArgumentNullException("uri", "Input parameter ca not be null.");
			}

			// Make sure we can handle this type of uri
            if (!uri.IsFile || !uri.LocalPath.EndsWith("\\"))
                throwInvalidVolumeException();

            // Store the Uri
			m_uri = uri;

			// Build information. 
			Refresh();
		}

		/**********************************************************
		 * Utility Methods
		 *********************************************************/
		private bool FlagSet(VolumeFlags Flag)
		{
			return ((m_volFlags & Flag) == Flag);
		}

		/**********************************************************
		 * Methods
		 *********************************************************/
        //public void OldRefresh()
        //{
        //    // Set defaults
        //    largeIcon   = null;
        //    smallIcon   = null;
        //    volLabel    = "";
        //    volType     = VolumeTypes.Invalid;
        //    serNum      = 0;
        //    maxCompLen  = 0;
        //    volFlags    = VolumeFlags.Unknown;
        //    fsName      = "";

        //    // Get the volume type
        //    volType = (VolumeTypes)GetDriveType(uri.LocalPath);

        //    // If not successful, throw an exception
        //    if (volType == VolumeTypes.Invalid)
        //        throwInvalidVolumeException();

        //    // Declare Receiving Variables
        //    var VolLabel = new StringBuilder(256);	// Label
        //    var VolFlags = new UInt32();
        //    var fileSysName = new StringBuilder(256);	// File System Name
			
        //    // Attempt to retreive the information
        //    var Ret = GetVolumeInformation(uri.LocalPath, VolLabel, (UInt32)VolLabel.Capacity, ref serNum, ref maxCompLen, ref VolFlags, fileSysName, (UInt32)fileSysName.Capacity);
        //    // if (Ret != 0) throw new VolumeAccessException();

        //    // Move to regular variables
        //    volLabel = VolLabel.ToString();
        //    volFlags = (VolumeFlags)VolFlags;
        //    fsName = fileSysName.ToString();

        //    // Attempt to get icons
        //    largeIcon = GetIcon(true);
        //    smallIcon = GetIcon(false);
        //}

		/**********************************************************
		 * Methods
		 *********************************************************/
		public void Refresh()
		{
			// Set defaults
			LargeIcon       = null;
			SmallIcon       = null;
			Label           = "";
			m_volType       = VolumeTypes.Invalid;
			m_serNum        = 0;
			m_maxCompLen    = 0;
			m_volFlags      = VolumeFlags.Unknown;
			m_fileSysName   = "";

			// Get the volume type
			m_volType = (VolumeTypes)GetDriveType(m_uri.LocalPath);

			// If not successful, throw an exception
			if (m_volType == VolumeTypes.Invalid)
                throwInvalidVolumeException();

			// Declare Receiving Variables
			var volLabel    = new StringBuilder(256);	    // Label
			var volFlags    = new UInt32();
			var fileSysName = new StringBuilder(256);	// File System Name
			
			// Attempt to retrieve the information
			long Ret = GetVolumeInformation(m_uri.LocalPath, volLabel, (UInt32)volLabel.Capacity, ref m_serNum, ref m_maxCompLen, ref volFlags, fileSysName, (UInt32)fileSysName.Capacity);
			// if (Ret != 0) throw new VolumeAccessException();

			// Move to regular variables
			Label = volLabel.ToString();
			m_volFlags = (VolumeFlags)volFlags;
			m_fileSysName = fileSysName.ToString();

			// Attempt to get icons
			//largeIcon = GetIcon(true);
			//smallIcon = GetIcon(false);
		}


        //private Icon GetIcon(bool Large)
        //{
        //    // Holder
        //    Icon Ret = null;

        //    // Attempt
        //    try
        //    {
        //        // Create structure
        //        SHFILEINFO shfi = new SHFILEINFO();

        //        // Calc Flags
        //        uint flgs = SHGFI_USEFILEATTRIBUTES | SHGFI_ICON;
        //        if (!Large) flgs |= SHGFI_SMALLICON;

        //        // Call method
        //        SHGetFileInfo( uri.LocalPath, FILE_ATTRIBUTE_NORMAL, ref shfi, (uint)Marshal.SizeOf(shfi), flgs );
				
        //        // Return the icon
        //        Ret = Icon.FromHandle(shfi.hIcon);
        //    }
        //    catch{}

        //    // Return icon.
        //    return Ret;
        //}

		/**********************************************************
		 * Properties
		 *********************************************************/
		public Uri Uri
		{
			get
			{
				return m_uri;
			}
		}

        //public VolumeTypes VolumeType
        //{
        //    get
        //    {
        //        return volType;
        //    }
        //}

		public string UncPath
		{
			get
			{
				// Make sure it's the right type
			    if (m_volType != VolumeTypes.Remote)
			        throwInvalidVolumeException();

				// If it is a Unc path, just return the root
				if (m_uri.IsUnc) return m_uri.LocalPath;
				
				// It's a mapped drive letter, we have to perform the lookup
				// Allocate Memory
				uint uniSize    = 255;
				var buff        = Marshal.AllocCoTaskMem((int)uniSize);

				// Call API to perform lookup
				var ret = WNetGetUniversalName(m_uri.LocalPath, UniInfoLevels.Universal, buff, ref uniSize);
				if (ret != 0)
				{
					Marshal.FreeCoTaskMem(buff);

				    var msg = string.Format("The {0} volume could not be accessed and may be offline.", m_uri.LocalPath);
                    Logger.LogError(msg);
					throw new ApplicationException(msg);
				}
				
				// Get the result
				var result = (UniversalNameInfo)Marshal.PtrToStructure(buff,typeof(UniversalNameInfo));

				// Free the memory
				Marshal.FreeCoTaskMem(buff);
				
				// Get result
				var sRes = result.NetworkPath;
				if (!sRes.EndsWith("\\")) sRes += "\\";

				// Return the result
				return sRes;
			}
		}

	    public Icon LargeIcon { get; private set; }

	    public Icon SmallIcon { get; private set; }

	    public string Label { get; private set; }

	    public UInt32 SerialNumber
		{
			get
			{
				return m_serNum;
			}
		}

		public UInt32 MaxComponentLen
		{
			get
			{
				return m_maxCompLen;
			}
		}

        //public VolumeFlags Flags
        //{
        //    get
        //    {
        //        return volFlags;
        //    }
        //}
		
		public bool CaseSensitive
		{
			get
			{
				return FlagSet(VolumeFlags.CaseSensitive);
			}
		}

		public bool Compressed
		{
			get
			{
				return FlagSet(VolumeFlags.Compressed);
			}
		}

        //public bool PersistentACLS
        //{
        //    get
        //    {
        //        return FlagSet(VolumeFlags.PersistentACLS);
        //    }
        //}


		public bool PreservesCase
		{
			get
			{
				return FlagSet(VolumeFlags.PreservesCase);
			}
		}

		public bool ReadOnly
		{
			get
			{
				return FlagSet(VolumeFlags.ReadOnly);
			}
		}

		public bool SupportsEncryption
		{
			get
			{
				return FlagSet(VolumeFlags.SupportsEncryption);
			}
		}

		public bool SupportsFileCompression
		{
			get
			{
				return FlagSet(VolumeFlags.SupportsFileCompression);
			}
		}

		public bool SupportsNamedStreams
		{
			get
			{
				return FlagSet(VolumeFlags.SupportsNamedStreams);
			}
		}
		
		public bool SupportsObjectIDs
		{
			get
			{
				return FlagSet(VolumeFlags.SupportsObjectIDs);
			}
		}
		
		public bool SupportsQuotas
		{
			get
			{
				return FlagSet(VolumeFlags.SupportsQuotas);
			}
		}
		
		public bool SupportsReparsePoints
		{
			get
			{
				return FlagSet(VolumeFlags.SupportsReparsePoints);
			}
		}
		
		public bool SupportsSparseFiles
		{
			get
			{
				return FlagSet(VolumeFlags.SupportsSparseFiles);
			}
		}
		
		public bool SupportsUnicodeOnVolume
		{
			get
			{
				return FlagSet(VolumeFlags.SupportsUnicodeOnVolume);
			}
		}

	    private void throwInvalidVolumeException()
	    {
            var msg = string.Format("Volume information could not be retrieved for the path '{0}'. Verify that the path is valid and ends in a trailing backslash, and try again.", m_uri.LocalPath);
            Logger.LogError(msg);
            throw new ApplicationException(msg);
	    }

		/**********************************************************
		 * Static Creators
		 *********************************************************/
		static public VolumeInfo CurrentVolume ()
		{
            return new VolumeInfo(new Uri(Directory.GetDirectoryRoot(Environment.SystemDirectory)));

			//return new VolumeInfo(new Uri(Directory.GetDirectoryRoot(Directory.GetCurrentDirectory())));
		}

//		static public int GetDiskSerialNumber()
//		{
//			UInt32 serNum = 0;
//			string PathName = "";
//			StringBuilder VolumeNameBuffer = new StringBuilder();
//			UInt32 VolumeNameSize = 0;
//            UInt32 MaximumComponentLength = 0;
//            UInt32 FileSystemFlags = 0;
//            StringBuilder FileSystemNameBuffer = new StringBuilder();
//            UInt32 FileSystemNameSize = 0;
//
//			long Ret = GetVolumeInformation(PathName, 
//				VolumeNameBuffer, 
//				VolumeNameSize, 
//				ref serNum,
//				ref MaximumComponentLength,
//				ref FileSystemFlags, 
//				FileSystemNameBuffer, 
//				FileSystemNameSize);			
//			return (int)serNum;
//		}
	}
}
