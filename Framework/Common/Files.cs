using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text.RegularExpressions;

namespace Rimed.Framework.Common
{
    public static class Files
    {
        private const string    CLASS_NAME                  = "FILES";

        public const int        FILE_SIGNATURE_BYTE_COUNT   = 16;

        private static readonly Regex s_rgxValidPath = new Regex(@"^(([a-zA-Z]\:)|(\\))(\\{1}|((\\{1})[^\\]([^/:*?<>""|]*))+)$");
        //@"
        //(                             # Match the regular expression below and capture its match into backreference number 1
        //   (                             # Match the regular expression below and capture its match into backreference number 2
        //      |                             # Match either the regular expression below (attempting the next alternative only if this one fails)
        //         [a-z]                         # Match a single character in the range between �a� and �z�
        //         :                             # Match the character �:� literally
        //      |                             # Or match regular expression number 2 below (the entire group fails if this one fails to match)
        //         \\                            # Match the character �\� literally
        //         \\                            # Match the character �\� literally
        //         [a-z0-9_.$]                   # Match a single character present in the list below
        //                                          # A character in the range between �a� and �z�
        //                                          # A character in the range between �0� and �9�
        //                                          # One of the characters �_.$�
        //            +                             # Between one and unlimited times, as many times as possible, giving back as needed (greedy)
        //         \\                            # Match the character �\� literally
        //         [a-z0-9_.$]                   # Match a single character present in the list below
        //                                          # A character in the range between �a� and �z�
        //                                          # A character in the range between �0� and �9�
        //                                          # One of the characters �_.$�
        //            +                             # Between one and unlimited times, as many times as possible, giving back as needed (greedy)
        //   )?                            # Between zero and one times, as many times as possible, giving back as needed (greedy)
        //   (                             # Match the regular expression below and capture its match into backreference number 3
        //      \\                            # Match the character �\� literally
        //         ?                             # Between zero and one times, as many times as possible, giving back as needed (greedy)
        //      (?:                           # Match the regular expression below
        //         [^\\/:*?""<>|\r\n]             # Match a single character NOT present in the list below
        //                                          # A \ character
        //                                          # One of the characters �/:*?""<>|�
        //                                          # A carriage return character
        //                                          # A line feed character
        //            +                             # Between one and unlimited times, as many times as possible, giving back as needed (greedy)
        //         \\                            # Match the character �\� literally
        //      )+                            # Between one and unlimited times, as many times as possible, giving back as needed (greedy)
        //   )
        //   [^\\/:*?""<>|\r\n]             # Match a single character NOT present in the list below
        //                                    # A \ character
        //                                    # One of the characters �/:*?""<>|�
        //                                    # A carriage return character
        //                                    # A line feed character
        //      +                             # Between one and unlimited times, as many times as possible, giving back as needed (greedy)
        //)
        //"


        /// <summary>Move / Rename file. Exceptions logged (not thrown)</summary>
        /// <param name="source">The name of the file to move.</param>
        /// <param name="destination">The new path for the file.</param>
        /// <param name="isOverwrite">Flag if destination file can be overwritten</param>
        /// <returns>true: File moved / renamed. False: Move / Rename file fail.</returns>
        /// <remarks>NOTE: In case of failuer, if Replacedestination flag is on (default) destination file, if exists, might be deleted</remarks>
        public static bool Move(string source, string destination, bool isOverwrite = true)
        {
            Logger.LogTrace(Logger.ETraceLevel.L6, CLASS_NAME, "Move(source='{0}', destination='{1}', isOverwrite={2}): ", source, destination, isOverwrite);

            if (string.IsNullOrWhiteSpace(source) || string.IsNullOrWhiteSpace(destination))
                return false;

            if (!File.Exists(source))
                return false;

            if (source.Equals(destination, StringComparison.InvariantCultureIgnoreCase))
                return true;

            if (File.Exists(destination))
            {
                if (!isOverwrite)
                    return false;

                // Delete destination
                try
                {
                    File.Delete(destination);
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex);
                    return false;
                }
            }

            // Move
            try
            {
                File.Move(source, destination);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                return false;
            }

            return true;
        }

        /// <summary>Delete file. Exceptions logged (not thrown)</summary>
        /// <param name="fileName">File to delete</param>
        /// <returns>true: File deleted / not exists. False: Delete file fail.</returns>
        public static bool Delete(string fileName)
        {
            Logger.LogTrace(Logger.ETraceLevel.L6, CLASS_NAME, "Delete(fileName='{0}'): ", fileName);

            if (string.IsNullOrWhiteSpace(fileName))
                return true;

            if (!File.Exists(fileName))
                return true;

            try
            {
                File.Delete(fileName);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                return false;
            }

            return true;
        }

        /// <summary>Copy file. Exceptions logged (not thrown)</summary>
        /// <param name="source">File to copy.</param>
        /// <param name="destination">Name of the destination file (Cannot be a directory).</param>
        /// <param name="isOverwrite">Flag if destination file can be overwritten</param>
        /// <returns>true: File copyed. False: Copy file fail.</returns>
        public static bool Copy(string source, string destination, bool isOverwrite = true)
        {
            Logger.LogTrace(Logger.ETraceLevel.L6, CLASS_NAME, "Copy(source='{0}', destination='{1}', isOverwrite={2}): ", source, destination, isOverwrite);

            if (string.IsNullOrWhiteSpace(source) || string.IsNullOrWhiteSpace(destination))
                return false;

            if (!File.Exists(source))
                return false;

            if (source.Equals(destination, StringComparison.InvariantCultureIgnoreCase))
                return false;

            // Copy
            try
            {
                File.Copy(source, destination, isOverwrite);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                return false;
            }

            return true;
        }

        public static bool DeleteFolderFiles(string path, bool isRecursive = true)
        {
            if (string.IsNullOrWhiteSpace(path) || !Directory.Exists(path))
                return false;

            if (isRecursive)
            {
                var folders = Directory.GetDirectories(path);
                foreach (var folder in folders)
                {
                    DeleteFolderFiles(folder);
                }
            }

            var files = Directory.GetFiles(path);
            foreach (var file in files)
            {
                Delete(file);
            }

            return true;
        }

        public static bool DeleteFolder(string path)
        {
            if (!DeleteFolderFiles(path))
                return false;

            try
            {
                Directory.Delete(path, true);
                return true;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                return false;
            }
        }

		public static bool ImageSave(Image img, string fileName, ImageFormat imgFmt = null)
        {
            if (img == null)
                return false;

            if (string.IsNullOrWhiteSpace(fileName))
                return false;

            if (imgFmt == null)
                imgFmt = ImageFormat.Jpeg;

            try
            {
                using (var bmpCopy = new Bitmap(img))   //Ofer: Workaround for: ExternalException (0x80004005) A generic error occurred in GDI+
                {
                    bmpCopy.Save(fileName, imgFmt);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, string.Format("Image save fail. File: '{0}', save format: {1}", fileName, imgFmt));
                return false;
            }

            return true;
        }

		public static Image ImageLoad(string fileName)
		{
			if (string.IsNullOrWhiteSpace(fileName))
				return null;

			try
			{
				return Image.FromFile(fileName);
			}
			catch (Exception ex)
			{
				Logger.LogError(ex, string.Format("Image load fail. File: '{0}'.", fileName));
			}

			return null;
		}


        public static string GetFileExtention(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
                return string.Empty;

            try
            {
                return Path.GetExtension(path);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }

            return string.Empty;
        }                            

        public static string GetFileName(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
                return string.Empty;

            try
            {
                path = Path.GetFileName(path);
                if (!string.IsNullOrWhiteSpace(path))
                    return path;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }

            return string.Empty;
        }

        public static string GetFilePath(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
                return string.Empty;

            try
            {
                path = Path.GetDirectoryName(path);
                if (!string.IsNullOrWhiteSpace(path))
                    return path;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }

            return string.Empty;
        }
		
        public static string GetFileNameWithoutExtention(string path)
        {
            path = GetFileName(path);
            if (string.IsNullOrWhiteSpace(path))
                return string.Empty;

            try
            {
                path = Path.GetFileNameWithoutExtension(path);
                if (!string.IsNullOrWhiteSpace(path))
                    return path;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }

            return string.Empty;
        }

        public static string GetAbsolutPath(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
                return string.Empty;

            try
            {
                path = Path.GetFullPath(path);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                return string.Empty;
            }

            return path;
        }

        public static bool IsPathValid(string path)
        {
            path = path.Replace(@"\\", @"\");
            if (string.IsNullOrWhiteSpace(path))
                return false;

            return s_rgxValidPath.IsMatch(path);
        }

		public static bool CreatePath(string path)
		{
			if (!IsPathValid(path))
				return false;

			if (Directory.Exists(path))
				return true;

			try
			{
				Directory.CreateDirectory(path);
				return true;
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);
				return false;
			}
		}

        public static FileInfo GetFileInfo(string file, bool checkExsist = true)
        {
            if (!IsPathValid(file))
                return null;

            if (checkExsist && !File.Exists(file))
                return null;

            try
            {
                return new FileInfo(file);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }

            return null;
        }

        public static byte[] GetStreamSinature(Stream stream)
        {
            if (stream == null)
                return null;

            if (!stream.CanRead)
                return null;

            try
            {
                using (var md5 = MD5.Create())
                {
                    return md5.ComputeHash(stream);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }

            return null;
        }

        public static byte[] GetFileSinature(string filePath)
        {
            if (!IsPathValid(filePath))
                return null;

            if (!File.Exists(filePath))
                return null;

            try
            {
                using (var fs = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    return GetStreamSinature(fs);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }

            return null;
        }

        public static string GetFileStringSinature(string filePath)
        {
            return SignatureToString(GetFileSinature(filePath));
        }

        public static string SignatureToString(byte[] signature)
        {
            if (signature == null || signature.Length == 0)
                return string.Empty;

            return BitConverter.ToString(signature).Replace("-", "").ToLower();
        }

		[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 8, CharSet = CharSet.Ansi)]
		public struct Version
		{
			[FieldOffset(0)]public ushort Build;            //0 ... 1
			[FieldOffset(2)]public ushort Reversion;        //2 ... 3
			[FieldOffset(4)]public ushort Minor;            //4 ... 5
			[FieldOffset(6)]public ushort Major;            //6 ... 7

			[FieldOffset(0)]public ulong Value;             //0 ... 7


			public Version(ushort major, ushort minor, ushort reversion = 0, ushort build = 0)
				: this()
			{
				Major = major;
				Minor = minor;
				Reversion = reversion;
				Build = build;
			}

			public bool IsEmpty { get { return Value == 0; } }

			public override string ToString()
			{
				return string.Format("{0}.{1}.{2}.{3}", Major, Minor, Reversion, Build);
			}
		}


		public static string[] GetDirectoryFiles(string dir)
		{
			var res = new string[0];
			if (string.IsNullOrWhiteSpace(dir))
				return res;

			try
			{
				res  = Directory.GetFiles(dir);
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);
				res = new string[0];
			}

			return res;
		}

    }
}
