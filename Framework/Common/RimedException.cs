using System;

namespace Rimed.Framework.Common
{
    public class RimedException: Exception
    {
        public RimedException()
            : base()
        {
        }
        public RimedException(string sMsg)
            : base(sMsg)
        {
        }
        public RimedException(string sMsg, Exception eInner)
            : base(sMsg, eInner)
        {
        }
    }
}
