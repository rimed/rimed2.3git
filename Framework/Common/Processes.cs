﻿using System;
using System.Diagnostics;

namespace Rimed.Framework.Common
{
    public static class Processes
    {
        static Processes()
        {
            Current = Process.GetCurrentProcess();
        }

        public static Process Current { get; private set; }
    }
}
