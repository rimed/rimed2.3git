using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using Rimed.Framework.Common;

namespace Rimed.Framework.Config
{
    public static class AppConfig
    {
		private const string CLASS_NAME = "AppConfig";

		private static bool		s_isLoaded;
        private static string   s_frameworkDefaultConfigurationFile = null;

        public static string	GetConfigValue(string key, string defaultValue = "")
        {
            if (string.IsNullOrWhiteSpace(key))
                return defaultValue;

            if (!s_isLoaded)
				loadAppSettings(); 

            string value;
            if (!s_appSettings.TryGetValue(key, out value))
            {
                Logger.LogWarning("Configuration key ({0}) not found. Using default value ({1}).", key, defaultValue);
                return defaultValue;
            }

            return value;
        }
        public static int		GetConfigValue(string key, int defaultValue)
        {
            if (string.IsNullOrWhiteSpace(key))
                return defaultValue;

            var s = GetConfigValue(key, defaultValue.ToString());
            int value;
            return int.TryParse(s, out value) ? value : defaultValue;
        }
        public static bool		GetConfigValue(string key, bool defaultValue)
        {
            if (string.IsNullOrWhiteSpace(key))
                return defaultValue;

            var s = GetConfigValue(key, defaultValue.ToString()).ToUpper();
            return ("TRUE".Equals(s) || "YES".Equals(s) || "ON".Equals(s) || "OK".Equals(s));
        }
		public static double	GetConfigValue(string key, double defaultValue)
		{
			if (string.IsNullOrWhiteSpace(key))
				return defaultValue;

			var s = GetConfigValue(key, defaultValue.ToString());
			
			double value;
			if (double.TryParse(s, out value))
				return value;

			return defaultValue;
		}
		public static bool		GetConfigValue(string key, string trueValue, bool defaultValue)
		{
			if (string.IsNullOrWhiteSpace(key) || trueValue == null)
				return defaultValue;

			var s = GetConfigValue(key, defaultValue.ToString()).ToUpper();
			return (string.Compare(trueValue, s, StringComparison.InvariantCultureIgnoreCase) == 0);
		}

		public static void		LogConfigurationParameters()
		{
			if (!s_isLoaded)
				loadAppSettings();

			Logger.LogInfo("Loading configuration parameters: ");
			foreach (var item in s_appSettings.OrderBy(kv => kv.Key))
			{
				Logger.LogInfo("\t{0} = {1}", item.Key, item.Value);
			}
		}

        private static readonly Dictionary<string, string> s_appSettings = new Dictionary<string, string>();
        private static void loadAppSettings()
        {
            try
            {
                var set = ConfigurationManager.AppSettings;

                if (s_isLoaded)
                    s_appSettings.Clear();

                for (var i = 0; i < set.Count; i++)
                {
                    var key = set.GetKey(i);
                    var value = set[i];
                    s_appSettings[key] = value;
                }

				s_isLoaded = true;
            }
            catch (Exception ex)
            {
                Logger.LogFatalError(ex, "Configuration file read / parse error.");
                throw;
            }
        }

        internal static string FrameworkDefaultConfigurationFile
        {
            get
            {
                if (string.IsNullOrWhiteSpace(s_frameworkDefaultConfigurationFile))
                {
                    var ass = Assembly.GetExecutingAssembly();
                    s_frameworkDefaultConfigurationFile = string.Format("{0}.config", ass.Location);
                }

                return s_frameworkDefaultConfigurationFile;
            }
        }
    }
}
