﻿using System;

namespace Rimed.Framework.WinAPI
{
    public static class Constants
    {
        public const Int32  ERROR_FILE_NOT_FOUND    = 0x00000002;
        public const UInt32 OPEN_EXISTING           = 0x00000003;
        public const UInt32 ERROR_ACCESS_DENIED     = 0x00000005;

        public const UInt32 MB_ICONHAND             = 0x00000010;
        public const UInt32 MB_ICONQUESTION         = 0x00000020;
        public const UInt32 MB_ICONEXCLAMATION      = 0x00000030;
        public const UInt32 MB_ICONASTERISK         = 0x00000040;

        public const Int32 READ_CONTROL             = 0x00020000;

        public const uint GENERIC_WRITE             = 0x40000000;
        public const uint GENERIC_READ              = 0x80000000;


        public const Int32 INVALID_HANDLE_VALUE	    = -1;
    }
}
