using System;
using System.Runtime.InteropServices;

namespace Rimed.Framework.WinAPI
{
	public static class WinMM
	{
        // extern lib
        private const string DLL_NAME = "WinMM.dll";

        #region consts
        public const int WAVE_MAPPER = -1;
		public const int MMSYSERR_NOERROR = 0; // no error

		public const int MM_WOM_OPEN = 0x3BB;
		public const int MM_WOM_CLOSE = 0x3BC;
		public const int MM_WOM_DONE = 0x3BD;

		public const int MM_WIM_OPEN = 0x3BE;
		public const int MM_WIM_CLOSE = 0x3BF;
		public const int MM_WIM_DATA = 0x3C0;

		public const int CALLBACK_FUNCTION = 0x00030000;    // dwCallback is a FARPROC 

		public const int TIME_MS = 0x0001;  // time in milliseconds 
		public const int TIME_SAMPLES = 0x0002;  // number of wave samples 
		public const int TIME_BYTES = 0x0004;  // current byte offset 


		public const int TIMERR_NOERROR = 0;
		public const int TIMERR_BASE	= 96;
		public const int TIMERR_NOCANDO = TIMERR_BASE + 1;
        #endregion

        #region enums
        public enum EWaveFormat : short
        {
            PCM_UNCOMPRESSED = 1,
            FLOAT = 3
        }
        #endregion

        #region structs
        [StructLayout(LayoutKind.Sequential)]
        public class WaveFormat
        {
            public short wFormatTag;
            public short nChannels;             // Number of channels in the waveform-audio data
            public int nSamplesPerSec;          // Sample rate, in samples per second (hertz)
            public int nAvgBytesPerSec;         // Required average data-transfer rate, in bytes per second. If wFormatTag is WAVE_FORMAT_PCM, nAvgBytesPerSec should be equal to the product of nSamplesPerSec and nBlockAlign.
            public short nBlockAlign;           // Block alignment, in bytes. The block alignment is the minimum atomic unit of data for the wFormatTag format type. If wFormatTag is WAVE_FORMAT_PCM or WAVE_FORMAT_EXTENSIBLE, nBlockAlign must be equal to the product of nChannels and wBitsPerSample divided by 8 (bits per byte)
            public short wBitsPerSample;        // Bits per sample for the wFormatTag format type. If wFormatTag is WAVE_FORMAT_PCM, then wBitsPerSample should be equal to 8 or 16.
            public short cbSize;                // Size, in bytes, of extra format information appended to the end of the WAVEFORMATEX structure

            public WaveFormat(int rate, int bits, int channels)
            {
                wFormatTag = (short)EWaveFormat.PCM_UNCOMPRESSED;
                nChannels = (short)channels;
                nSamplesPerSec = rate;
                wBitsPerSample = (short)bits;
                cbSize = 0;

                nBlockAlign = (short)(channels * (bits / 8));
                nAvgBytesPerSec = nSamplesPerSec * nBlockAlign;
            }
        }

		[StructLayout(LayoutKind.Sequential)] public struct WaveHdr
		{
			public IntPtr lpData; // pointer to locked data buffer
			public int dwBufferLength; // length of data buffer
			public int dwBytesRecorded; // used for input only
			public IntPtr dwUser; // for client's use
			public int dwFlags; // assorted flags (see defines)
			public int dwLoops; // loop control counter
			public IntPtr lpNext; // PWaveHdr, reserved for driver
			public int reserved; // reserved for driver
		}
        #endregion

        #region callback delegates
        public delegate void WaveDelegate(IntPtr hdrvr, int uMsg, int dwUser, ref WaveHdr wavhdr, int dwParam2);
        #endregion

        #region external functions
		/// <summary>Requests a minimum resolution for periodic timers.</summary>
		/// <param name="uPeriod">Minimum timer resolution, in milliseconds, for the application or device driver. A lower value specifies a higher (more accurate) resolution.</param>
		/// <returns>Returns TIMERR_NOERROR if successful or TIMERR_NOCANDO if the resolution specified in uPeriod is out of range.</returns>
		/// <remarks>
		/// Call this function immediately before using timer services, and call the timeEndPeriod function immediately after you are finished using the timer services.
		/// You must match each call to timeBeginPeriod with a call to timeEndPeriod, specifying the same minimum resolution in both calls. An application can make multiple timeBeginPeriod calls as long as each call is matched with a call to timeEndPeriod.
		/// This function affects a global Windows setting. Windows uses the lowest value (that is, highest resolution) requested by any process. Setting a higher resolution can improve the accuracy of time-out intervals in wait functions. However, it can also reduce overall system performance, because the thread scheduler switches tasks more often. High resolutions can also prevent the CPU power management system from entering power-saving modes. Setting a higher resolution does not improve the accuracy of the high-resolution performance counter.
		/// </remarks>
		[DllImport(DLL_NAME, SetLastError = true)]  public static extern int timeBeginPeriod(uint uPeriod);

		/// <summary>Clears a previously set minimum timer resolution.</summary>
		/// <param name="uPeriod">Minimum timer resolution specified in the previous call to the timeBeginPeriod function.</param>
		/// <returns>Returns TIMERR_NOERROR if successful or TIMERR_NOCANDO if the resolution specified in uPeriod is out of range.</returns>
		/// <remarks>
		/// Call this function immediately after you are finished using timer services.
		/// You must match each call to timeBeginPeriod with a call to timeEndPeriod, specifying the same minimum resolution in both calls. An application can make multiple timeBeginPeriod calls as long as each call is matched with a call to timeEndPeriod.
		///</remarks>
		[DllImport(DLL_NAME, SetLastError = true)]	public static extern int timeEndPeriod(uint uPeriod);


        #region WaveOut calls
        [DllImport(DLL_NAME, SetLastError = true)] public static extern int waveOutGetNumDevs();
        [DllImport(DLL_NAME, SetLastError = true)] public static extern int waveOutPrepareHeader(IntPtr hWaveOut, ref WaveHdr lpWaveOutHdr, int uSize);
        [DllImport(DLL_NAME, SetLastError = true)] public static extern int waveOutUnprepareHeader(IntPtr hWaveOut, ref WaveHdr lpWaveOutHdr, int uSize);
        [DllImport(DLL_NAME, SetLastError = true)] public static extern int waveOutWrite(IntPtr hWaveOut, ref WaveHdr lpWaveOutHdr, int uSize);
        [DllImport(DLL_NAME, SetLastError = true)] public static extern int waveOutOpen(out IntPtr hWaveOut, int uDeviceID, WaveFormat lpFormat, WaveDelegate dwCallback, int dwInstance, int dwFlags);
        [DllImport(DLL_NAME, SetLastError = true)] public static extern int waveOutReset(IntPtr hWaveOut);
        [DllImport(DLL_NAME, SetLastError = true)] public static extern int waveOutClose(IntPtr hWaveOut);
        [DllImport(DLL_NAME, SetLastError = true)] public static extern int waveOutPause(IntPtr hWaveOut);
        [DllImport(DLL_NAME, SetLastError = true)] public static extern int waveOutRestart(IntPtr hWaveOut);
        [DllImport(DLL_NAME, SetLastError = true)] public static extern int waveOutGetPosition(IntPtr hWaveOut, out int lpInfo, int uSize);
        [DllImport(DLL_NAME, SetLastError = true)] public static extern int waveOutSetVolume(IntPtr hWaveOut, int dwVolume);
        [DllImport(DLL_NAME, SetLastError = true)] public static extern int waveOutGetVolume(IntPtr hWaveOut, out int dwVolume);
        #endregion

        #region WaveIn calls
        //[DllImport(DLL_NAME, SetLastError = true)] public static extern int waveInGetNumDevs();
        //[DllImport(DLL_NAME, SetLastError = true)] public static extern int waveInAddBuffer(IntPtr hwi, ref WaveHdr pwh, int cbwh);
        //[DllImport(DLL_NAME, SetLastError = true)] public static extern int waveInClose(IntPtr hwi);
        //[DllImport(DLL_NAME, SetLastError = true)] public static extern int waveInOpen(out IntPtr phwi, int uDeviceID, WaveFormat lpFormat, WaveDelegate dwCallback, int dwInstance, int dwFlags);
        //[DllImport(DLL_NAME, SetLastError = true)] public static extern int waveInPrepareHeader(IntPtr hWaveIn, ref WaveHdr lpWaveInHdr, int uSize);
        //[DllImport(DLL_NAME, SetLastError = true)] public static extern int waveInUnprepareHeader(IntPtr hWaveIn, ref WaveHdr lpWaveInHdr, int uSize);
        //[DllImport(DLL_NAME, SetLastError = true)] public static extern int waveInReset(IntPtr hwi);
        //[DllImport(DLL_NAME, SetLastError = true)] public static extern int waveInStart(IntPtr hwi);
        //[DllImport(DLL_NAME, SetLastError = true)] public static extern int waveInStop(IntPtr hwi);
        #endregion
        #endregion
    }
}
