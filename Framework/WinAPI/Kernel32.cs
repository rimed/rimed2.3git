﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace Rimed.Framework.WinAPI
{
    public static class Kernel32
    {
        private const string DLL_NAME = "Kernel32.dll";

        #region Structures
        [StructLayout(LayoutKind.Sequential)]
        public struct COMMTIMEOUTS
        {
            public UInt32 ReadIntervalTimeout;
            public UInt32 ReadTotalTimeoutMultiplier;
            public UInt32 ReadTotalTimeoutConstant;
            public UInt32 WriteTotalTimeoutMultiplier;
            public UInt32 WriteTotalTimeoutConstant;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct DCB
        {
            public Int32 DCBlength;
            public Int32 BaudRate;
            public Int32 PackedValues;
            public Int16 wReserved;
            public Int16 XonLim;
            public Int16 XoffLim;
            public Byte ByteSize;
            public Byte Parity;
            public Byte StopBits;
            public Byte XonChar;
            public Byte XoffChar;
            public Byte ErrorChar;
            public Byte EofChar;
            public Byte EvtChar;
            public Int16 wReserved1;

            //			public void init(bool parity, bool outCTS, bool outDSR, int dtr, bool inDSR, bool txc, bool xOut,
            //				bool xIn, int rts)
            //			{
            //				//This function is not called
            //				DCBlength = 28; 
            //				PackedValues = 0x8001;
            //				if (parity) 
            //					PackedValues |= 0x0002;
            //				if (outCTS) 
            //					PackedValues |= 0x0004;
            //				if (outDSR) 
            //					PackedValues |= 0x0008;
            //				PackedValues |= ((dtr & 0x0003) << 4);
            //
            //				if (inDSR) 
            //					PackedValues |= 0x0040;
            //				if (txc) 
            //					PackedValues |= 0x0080;
            //				if (xOut) 
            //					PackedValues |= 0x0100;
            //				if (xIn) 
            //					PackedValues |= 0x0200;
            //				PackedValues |= ((rts & 0x0003) << 12);
            //			}

            public void InitControl(int rts, int dtr)
            {
                //	RTS (request-to-send) flow control. This member can be one of the following values.
                // 0 = RTS_CONTROL_DISABLE
                // 1 = RTS_CONTROL_ENABLE
                // 2 = RTS_CONTROL_HANDSHAKE
                // 3 = RTS_CONTROL_TOGGLE
                PackedValues |= (((rts & 0x0003) << 12) | ((dtr & 0x0003) << 4));
            }
        }

		[StructLayout(LayoutKind.Sequential)]
		public struct MEMORYSTATUSEX
		{
			/// <summary>Size of the structure, in bytes.</summary>
			/// <remarks>You MUST set this member before calling GlobalMemoryStatusEx.</remarks>
			public int		Length;	

			/// <summary>A number between 0 and 100 that specifies the approximate percentage of physical memory that is in use (0 indicates no memory use and 100 indicates full memory use).</summary>
			public int		MemoryLoad;

			/// <summary>The amount of actual physical memory, in bytes.</summary>
			public ulong	TotalPhysicalMemory;

			/// <summary>The amount of physical memory currently available, in bytes. This is the amount of physical memory that can be immediately reused without having to write its contents to disk first. It is the sum of the size of the standby, free, and zero lists.</summary>
			public ulong	AvailablePhysicalMemory;

			/// <summary>Current committed memory limit for the system or the current process, whichever is smaller, in bytes. To get the system-wide committed memory limit, call <code>GetPerformanceInfo</code>>.</summary>
			public ulong	CommittedMemory;

			/// <summary>The maximum amount of memory the current process can commit, in bytes. This value is equal to or smaller than the system-wide available commit value. To calculate the system-wide available commit value, call GetPerformanceInfo and subtract the value of CommitTotal from the value of CommitLimit.</summary>
			public ulong	AvailableCommittedMemory;

			/// <summary>The size of the user-mode portion of the virtual address space of the calling process, in bytes. This value depends on the type of process, the type of processor, and the configuration of the operating system. For example, this value is approximately 2 GB for most 32-bit processes on an x86 processor and approximately 3 GB for 32-bit processes that are large address aware running on a system with 4-gigabyte tuning enabled.</summary>
			public ulong	TotalVirtual;

			/// <summary>The amount of unreserved and uncommitted memory currently in the user-mode portion of the virtual address space of the calling process, in bytes.</summary>
			public ulong	AvailableVirtual;

			/// <summary>Reserved. This value is always 0.</summary>
			public ulong	Reserved;	//availExtendedVirtual;
		}
        #endregion

        [DllImport(DLL_NAME, CharSet = CharSet.Auto, SetLastError = true)]
        public static extern IntPtr OpenMutex(int desiredAccess, bool inheritHandle, string name);

        [DllImport(DLL_NAME, CharSet = CharSet.Auto, SetLastError = true, ExactSpelling = true)]
        public static extern int CloseHandle(IntPtr hObject);

        [DllImport(DLL_NAME, EntryPoint = "CreateFileW", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = true)]
        public static extern int CreateFile(string filename, uint access, uint sharemode, uint securityAttributes, uint creation, uint flags, uint template);

        [DllImport(DLL_NAME, SetLastError = true)]
        public static extern bool CloseHandle(int handle);

        [DllImport(DLL_NAME, SetLastError = true)]
        public static unsafe extern bool ReadFile(int hFile, byte* lpBuffer, int nNumberOfBytesToRead, int* lpNumberOfBytesRead, NativeOverlapped* lpOverlapped);

        [DllImport(DLL_NAME, SetLastError = true)]
        public static unsafe extern bool WriteFile(int hFile, byte* lpBuffer, int nNumberOfBytesToWrite, int* lpNumberOfBytesWritten, NativeOverlapped* lpOverlapped);

        [DllImport(DLL_NAME, SetLastError = true)]
        public static unsafe extern bool DeviceIoControl(int hDevice, int dwIoControlCode, byte* lpInBuffer, int nInBufferSize, byte* lpOutBuffer, int nOutBufferSize, int* lpBytesReturned, NativeOverlapped* lpOverlapped);

        //[DllImport(DLL_NAME, SetLastError = true)]
        //public static unsafe extern bool GetOverlappedResult(   int                 hDevice,                        // handle to file, pipe, or device
        //                                                        NativeOverlapped*   lpOverlapped,                   // overlapped structure
        //                                                        int*                lpNumberOfBytesTransferred,     // bytes transferred
        //                                                        bool                bWait                          // wait option
        //                                                     );

        [DllImport(DLL_NAME, SetLastError = true)]
        public static extern bool CancelIo(int hDevice);

        [DllImport(DLL_NAME, EntryPoint = "WritePrivateProfileStringW", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern bool WritePrivateProfileString(string lpAppName,  // section name
                                                            string lpKeyName,  // key name
                                                            string lpString,   // string to add
                                                            string lpFileName  // initialization file
                                                           );

        [DllImport(DLL_NAME, EntryPoint = "GetPrivateProfileStringW", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern UInt32 GetPrivateProfileString(string lpAppName,               // section name
                                                            string lpKeyName,               // key name
                                                            string lpDefault,               // default string
                                                            StringBuilder lpReturnedString, // destination buffer
                                                            UInt32 nSize,                   // size of destination buffer
                                                            string lpFileName               // initialization file name
                                                           );
    
        [DllImport(DLL_NAME, SetLastError = true)]
        public static extern Boolean SetupComm(int hFile, uint dwInQueue, uint dwOutQueue);

        [DllImport(DLL_NAME, SetLastError = true)]
        public static extern Boolean SetCommState(int hFile, [In] ref DCB lpDCB);

        [DllImport(DLL_NAME, SetLastError = true)]
        public static extern Boolean SetCommTimeouts(int hFile, [In] ref COMMTIMEOUTS lpCommTimeouts);

		[DllImport(DLL_NAME, SetLastError = true)]
		private static extern bool GlobalMemoryStatusEx(ref MEMORYSTATUSEX buffer);




		#region Wrappers
		public static ulong GetPhysicalMemory()
		{
			var status = GetMemoryStatus();
			return status.TotalPhysicalMemory;
		}

		public static MEMORYSTATUSEX GetMemoryStatus()
		{
			var status = new MEMORYSTATUSEX();
			status.Length = Marshal.SizeOf(status);
			if (!GlobalMemoryStatusEx(ref status))
			{
				var err = Marshal.GetLastWin32Error();
				throw new Win32Exception(err, "GlobalMemoryStatusEx() fail. ");
			}
			return status;
		}
		#endregion
	}
}
