﻿using System;
using System.Runtime.InteropServices;

namespace Rimed.Framework.WinAPI.Struct
{
	[Serializable, StructLayout(LayoutKind.Sequential, Pack = 1, Size = 16)]
	public struct RECT
	{
		public int Left;
		public int Top;
		public int Right;
		public int Bottom;

		public int Width
		{
			get
			{
				return Right - Left;
			}
		}
		public int Height
		{
			get
			{
				return Bottom - Top;
			}
		}
	}
}
