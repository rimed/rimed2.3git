﻿using System;
using System.Runtime.InteropServices;

namespace Rimed.Framework.WinAPI
{
    public static class GDI32
    {
        private const string DLL_NAME = "GDI32.dll";

        public enum ETernaryRasterOperations
        {
            SRCCOPY     = 0x00CC0020,   // dest = source                   
            SRCPAINT    = 0x00EE0086,   // dest = source OR dest           
            SRCAND      = 0x008800C6,   // dest = source AND dest          
            SRCINVERT   = 0x00660046,   // dest = source XOR dest          
            SRCERASE    = 0x00440328,   // dest = source AND (NOT dest )   
            NOTSRCCOPY  = 0x00330008,   // dest = (NOT source)             
            NOTSRCERASE = 0x001100A6,   // dest = (NOT src) AND (NOT dest) 
            MERGECOPY   = 0x00C000CA,   // dest = (source AND pattern)     
            MERGEPAINT  = 0x00BB0226,   // dest = (NOT source) OR dest     
            PATCOPY     = 0x00F00021,   // dest = pattern                  
            PATPAINT    = 0x00FB0A09,   // dest = DPSnoo                   
            PATINVERT   = 0x005A0049,   // dest = pattern XOR dest         
            DSTINVERT   = 0x00550009,   // dest = (NOT dest)               
            BLACKNESS   = 0x00000042,   // dest = BLACK                    
            WHITENESS   = 0x00FF0062    // dest = WHITE                    

        }


        [DllImport(DLL_NAME, SetLastError = true)]
        public static extern bool BitBlt(IntPtr hdcDst, int xDst, int yDst, int cx, int cy, IntPtr hdcSrc, int xSrc, int ySrc, uint ulRop);
    }
}
