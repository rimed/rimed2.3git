﻿using System;
using System.Runtime.InteropServices;

namespace Rimed.Framework.WinAPI
{
    public static class User32
    {
		// A wiki for .NET developers:	http://www.pinvoke.net/

        private const string DLL_NAME					= "USER32.dll";

		private const string WND_CLASS_NAME_TRAY		= "Shell_TrayWnd";
		private const int WND_CLASS_ATOM_START_BUTTON	= 0xC017;
		private const int PROCESS_HANDLE_CURRENT		= -1;


		#region enums
		[Flags]
        public enum EGetGuiResourceType: uint
        {
            GR_GDIOBJECTS = 0,          //Return the count of GDI objects: pens, brushes, fonts, palettes, regions, device contexts, bitmap headers
            GR_USEROBJECTS = 1,         //Return the count of USER objects: accelerator tables, bitmap resources, dialog box templates, font resources, menu resources, raw data resources, string table entries, message table entries, cursors/icons, windows, menus
            GR_GDIOBJECTS_PEAK  = 2,    //Return the peak count of GDI objects. (Win7 and above)
            GR_USEROBJECTS_PEAK = 4     //Return the peak count of USER objects. (Win7 and above)
        }
		#endregion

		#region constants

		//public readonly static IntPtr HWND_BOTTOM		= (IntPtr)1;
		//public readonly static IntPtr HWND_TOP			= (IntPtr)0;
		//public readonly static IntPtr HWND_TOPMOST		= (IntPtr)0xFFFFFFFF;
		//public static readonly IntPtr HWND_NOTOPMOST	= (IntPtr)0xFFFFFFFE;

		public static class WinMessage
		{
			public const int WM_MOVE				= 0x00000003;
			public const int WM_SIZE				= 0x00000005;
			public const int WM_ACTIVATE			= 0x00000006;
			public const int WM_SETFOCUS			= 0x00000007;
	
			public const int WM_ERASEBKGND			= 0x00000014;
			public const int WM_SHOWWINDOW			= 0x00000018;

			public const int WM_WINDOWPOSCHANGING	= 0x00000046;
			public const int WM_WINDOWPOSCHANGED	= 0x00000047;

			/// <summary>Sent to a window in order to determine what part of the window corresponds to a particular screen coordinate.</summary>
			public const int WM_NCHITTEST			= 0x00000084;
			public const int WM_NCPAINT				= 0x00000085;
			public const int WM_NCACTIVATE			= 0x00000086;


			public const int WM_NCLBUTTONDOWN		= 0x000000A1;
			public const int WM_NCLBUTTONDBLCLK		= 0x000000A3;
			public const int WM_NCRBUTTONDOWN		= 0x000000A4;

			public const int WM_LBUTTONDOWN			= 0x00000201;
			public const int WM_LBUTTONUP			= 0x00000202;
			public const int WM_RBUTTONDOWN			= 0x00000204;
			public const int WM_RBUTTONUP			= 0x00000205;

            public const int WM_SYSCOMMAND          = 0x00000112; // added by Alex 26/08/2014 fixed bug #513 v 2.2.0.3
            public const int SC_CLOSE               = 0x0000F060; // added by Alex 26/08/2014 fixed bug #513 v 2.2.0.3

			public static class WMParam
			{
				public const int MK_LBUTTON			= 0x0001;
				public const int HTCAPTION			= 0x0002;

				public const int SW_HIDE			= 0x0000;
				public const int SW_SHOWNORMAL		= 0x0001;
				public const int SW_RESTORE			= 0x0009;
				public const int SW_SHOWMAXIMIZED	= 0x0003;

				/// <summary>Retains the current size (ignores the cx and cy parameters).</summary>
				public const uint SWP_NOSIZE		= 0x0001;

				/// <summary>Retains the current position (ignores X and Y parameters).</summary>
				public const uint SWP_NOMOVE		= 0x0002;

				public const uint SWP_SHOWWINDOW	= 0x0040;
			}
		}


		#region ChangeDisplaySettings
		private const int CDS_NONE				= 0x00000000;
		private const int CDS_UPDATEREGISTRY	= 0x00000001;
		private const int CDS_TEST				= 0x00000002;
		private const int CDS_FULLSCREEN		= 0x00000004;
		private const int CDS_GLOBAL			= 0x00000008;
		private const int CDS_SET_PRIMARY		= 0x00000010;
		private const int CDS_RESET				= 0x40000000;
		private const int CDS_SETRECT			= 0x20000000;
		private const int CDS_NORESET			= 0x10000000;

		private const int DISP_CHANGE_SUCCESSFUL	= 0;	//The settings change was successful.
		private const int DISP_CHANGE_RESTART		= 1;	//The computer must be restarted for the graphics mode to work.
		private const int DISP_CHANGE_FAILED		= -1;	//The display driver failed the specified graphics mode.
		private const int DISP_CHANGE_BADMODE		= -2;	//The graphics mode is not supported.
		private const int DISP_CHANGE_NOTUPDATED	= -3;	//Unable to write settings to the registry.
		private const int DISP_CHANGE_BADFLAGS		= -4;	//An invalid set of flags was passed in.
		private const int DISP_CHANGE_BADPARAM		= -5;	//An invalid parameter was passed in. This can include an invalid flag or combination of flags.
		//DISP_CHANGE_BADDUALVIEW		The settings change was unsuccessful because the system is DualView capable.

		private const int ENUM_CURRENT_SETTINGS		= -1;
		private const int ENUM_REGISTRY_SETTINGS	= -2;
		#endregion


		#region GetWindowLong constants

		[Flags]
	    private enum EGetWindowLongIndexFlags
		{
			/// <summary>Retrieves the user data associated with the window. This data is intended for use by the application that created the window. Its value is initially zero.</summary>
			GWL_USERDATA	= -21,

			/// <summary>Retrieves the extended window styles</summary>
			GWL_EXSTYLE		= -20,

			/// <summary>Retrieves the window styles.</summary>
			GWL_STYLE		= -16,

			/// <summary>Retrieves the identifier of the window.</summary>
			GWL_ID			= -12,

			/// <summary>Retrieves a handle to the parent window, if any.</summary>
			GWLP_HWNDPARENT	=  -8,

			/// <summary>Retrieves a handle to the application instance.</summary>
			GWLP_HINSTANCE	=  -6,

			/// <summary>Retrieves the address of the window procedure, or a handle representing the address of the window procedure. You must use the CallWindowProc function to call the window procedure.</summary>
			GWL_WNDPROC		=  -4,
		}

		private enum EGetWindowLongResult : long
		{
			/// <summary>Retrieves the return value of a message processed in the dialog box procedure.</summary>
			DWLP_MSGRESULT	= 0,

			/// <summary>Retrieves the address of the dialog box procedure, or a handle representing the address of the dialog box procedure. You must use the CallWindowProc function to call the dialog box procedure.</summary>
			DWLP_DLGPROC	= 4,

			/// <summary>Retrieves extra information private to the application, such as handles or pointers.</summary>
			DWLP_USER		= 8
		}

		// Window Styles 	 
		[Flags]
		public enum EWindowStyles: uint
		{
			WS_OVERLAPPED	= 0,
			WS_POPUP		= 0x80000000,
			WS_CHILD		= 0x40000000,
			WS_MINIMIZE		= 0x20000000,
			WS_VISIBLE		= 0x10000000,
			WS_DISABLED		= 0x08000000,
			WS_CLIPSIBLINGS = 0x04000000,
			WS_CLIPCHILDREN = 0x02000000,
			WS_MAXIMIZE		= 0x01000000,
			WS_BORDER		= 0x00800000,
			WS_DLGFRAME		= 0x00400000,
			WS_CAPTION		= WS_BORDER | WS_DLGFRAME,	//0x00C00000
			WS_VSCROLL		= 0x00200000,
			WS_HSCROLL		= 0x00100000,
			WS_SYSMENU		= 0x00080000,
			WS_THICKFRAME	= 0x00040000,
			WS_GROUP		= 0x00020000,
			WS_TABSTOP		= 0x00010000,
			WS_MINIMIZEBOX	= 0x00020000,
			WS_MAXIMIZEBOX	= 0x00010000,
			WS_TILED		= WS_OVERLAPPED,
			WS_ICONIC		= WS_MINIMIZE,
			WS_SIZEBOX		= WS_THICKFRAME,
		}

		// Extended Window Styles 

		[Flags]
		private enum EWindowStylesEx: uint 
		{
			WS_EX_DLGMODALFRAME		= 0x00000001,
			WS_EX_NOPARENTNOTIFY	= 0x00000004,
			WS_EX_TOPMOST			= 0x00000008,
			WS_EX_ACCEPTFILES		= 0x00000010,
			WS_EX_TRANSPARENT		= 0x00000020,
			WS_EX_MDICHILD			= 0x00000040,
			WS_EX_TOOLWINDOW		= 0x00000080,
			WS_EX_WINDOWEDGE		= 0x00000100,
			WS_EX_CLIENTEDGE		= 0x00000200,
			WS_EX_CONTEXTHELP		= 0x00000400,
			WS_EX_RIGHT				= 0x00001000,
			WS_EX_LEFT				= 0x00000000,
			WS_EX_RTLREADING		= 0x00002000,
			WS_EX_LTRREADING		= 0x00000000,
			WS_EX_LEFTSCROLLBAR		= 0x00004000,
			WS_EX_RIGHTSCROLLBAR	= 0x00000000,
			WS_EX_CONTROLPARENT		= 0x00010000,
			WS_EX_STATICEDGE		= 0x00020000,
			WS_EX_APPWINDOW			= 0x00040000,
			WS_EX_OVERLAPPEDWINDOW	= (WS_EX_WINDOWEDGE | WS_EX_CLIENTEDGE),
			WS_EX_PALETTEWINDOW		= (WS_EX_WINDOWEDGE | WS_EX_TOOLWINDOW | WS_EX_TOPMOST),
			WS_EX_LAYERED			= 0x00080000,
			WS_EX_NOINHERITLAYOUT	= 0x00100000, // Disable inheritence of mirroring by children
			WS_EX_LAYOUTRTL			= 0x00400000, // Right to left mirroring
			WS_EX_COMPOSITED		= 0x02000000,
			WS_EX_NOACTIVATE		= 0x08000000,
		}
		#endregion
		#endregion

		#region structures

		/// <summary>The DEVMODE data structure contains information about the initialization and environment of a printer or a display device.</summary>
		[StructLayout(LayoutKind.Sequential)]
		public struct DEVMODE
		{
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]	public string dmDeviceName;
			public short dmSpecVersion;
			public short dmDriverVersion;
			public short dmSize;
			public short dmDriverExtra;
			public int dmFields;
			public short dmOrientation;
			public short dmPaperSize;
			public short dmPaperLength;
			public short dmPaperWidth;
			public short dmScale;
			public short dmCopies;
			public short dmDefaultSource;
			public short dmPrintQuality;
			public short dmColor;
			public short dmDuplex;
			public short dmYResolution;
			public short dmTTOption;
			public short dmCollate;								
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]	public string dmFormName;
			public short dmLogPixels;
			public short dmBitsPerPel;
			public int dmPelsWidth;
			public int dmPelsHeight;
			public int dmDisplayFlags;
			public int dmDisplayFrequency;
			public int dmICMMethod;
			public int dmICMIntent;
			public int dmMediaType;
			public int dmDitherType;
			public int dmReserved1;
			public int dmReserved2;

			public int dmPanningWidth;
			public int dmPanningHeight;
		};
		#endregion

		#region external / import methods
		[DllImport(DLL_NAME)]
        public static extern bool MessageBeep(uint uType);

		[DllImport(DLL_NAME, SetLastError = true, CharSet = CharSet.Auto)]
		public static extern uint RegisterWindowMessage(string lpString);

		[DllImport(DLL_NAME, SetLastError = true, CharSet = CharSet.Auto)]
		public static extern bool SendNotifyMessage(IntPtr hWnd, uint msg, int wParam, int lParam);

		///<summary>Retrieves the count of handles to graphical user interface (GUI) objects in use by the specified process.</summary>
		/// <returns>Count of handles to GUI objects in use by the process. If function fail return value is zero.</returns>
		[DllImport(DLL_NAME, SetLastError = true, EntryPoint = "GetGuiResources")]
		public extern static int GetGuiResources(IntPtr hProcess, EGetGuiResourceType uiFlags);

		[DllImport(DLL_NAME, SetLastError = true)]
        public static extern bool PostMessage(uint hWnd, int msg, uint wParam, uint lParam);

        [DllImport(DLL_NAME, SetLastError = true)]
        public static extern bool SendMessage(uint hWnd, int msg, uint wParam, uint lParam);

        [DllImport(DLL_NAME, SetLastError = true)]
        public static extern uint WindowFromPoint(System.Drawing.Point point);

        [DllImport(DLL_NAME, SetLastError = true)]
        public static extern uint FromHandle(uint hWnd);

        [DllImport(DLL_NAME, SetLastError = true)]
        public static extern uint GetParent();

        [DllImport(DLL_NAME, SetLastError = true)]
        public static extern uint SetCapture(uint hWnd);

        [DllImport(DLL_NAME, SetLastError = true)]
        public static extern uint ReleaseCapture();

        [DllImport(DLL_NAME, SetLastError = true)]
        public static extern bool ScreenToClient(uint hWnd, [In] ref System.Drawing.Point point);

        [DllImport(DLL_NAME, SetLastError = true)]
        public static extern bool ClientToScreen(uint hWnd, [In] ref System.Drawing.Point point);

        [DllImport(DLL_NAME, SetLastError = true)]
        public static extern uint ChildWindowFromPoint(uint hWnd, System.Drawing.Point point);

		[DllImport(DLL_NAME, SetLastError = true)]
		public static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport(DLL_NAME, SetLastError = true)]
        public static extern bool GetWindowRect(IntPtr hwnd, ref Struct.RECT rectangle);

        [DllImport(DLL_NAME, SetLastError = true)]
        public static extern IntPtr GetWindowDC(IntPtr hwnd);

        [DllImport(DLL_NAME, SetLastError = true)]
        public static extern Int32 ReleaseDC(IntPtr hwnd, IntPtr hdc);

        [DllImport(DLL_NAME, SetLastError = true)]
        public static extern IntPtr GetActiveWindow();

		[DllImport(DLL_NAME, SetLastError = true)]
		public static extern IntPtr SetActiveWindow(IntPtr hWnd);

		[DllImport(DLL_NAME, SetLastError = true)]
		public static extern IntPtr GetForegroundWindow();

		[DllImport(DLL_NAME, SetLastError = true)]
		public static extern bool SetForegroundWindow(IntPtr hWnd);

		/// <summary>Changes the parent window of the specified child window.</summary>
		/// <param name="hWndChild">A handle to the child window.</param>
		/// <param name="hWndNewParent">A handle to the new parent window. If this parameter is NULL, the desktop window becomes the new parent window. If this parameter is HWND_MESSAGE, the child window becomes a message-only window.</param>
		/// <returns>If the function succeeds, the return value is a handle to the previous parent window. If the function fails, the return value is NULL. To get extended error information, call GetLastError.</returns>
		[DllImport(DLL_NAME, SetLastError = true)]
		public static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

		/// <summary>Brings the specified window to the top of the Z order. If the window is a top-level window, it is activated. If the window is a child window, the top-level parent window associated with the child window is activated.</summary>
		/// <param name="hWnd">A handle to the window to bring to the top of the Z order.</param>
		/// <returns>f the function succeeds, the return value is nonzero. If the function fails, the return value is zero. To get extended error information, call GetLastError.</returns>
		[DllImport(DLL_NAME, SetLastError = true)]
		public static extern bool BringWindowToTop(IntPtr hWnd);

		[DllImport(DLL_NAME, SetLastError = true)]
		public static extern Int32 GetWindowThreadProcessId(IntPtr hwnd, out int dwProcessId);

		[DllImport(DLL_NAME, SetLastError = true)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

		[DllImport(DLL_NAME, SetLastError = true)]
		public static extern IntPtr FindWindowEx(IntPtr parentHandle, IntPtr childAfter, string className, string windowTitle);

		[DllImport(DLL_NAME, SetLastError = true)]
		private static extern IntPtr FindWindowEx(IntPtr parentHwnd, IntPtr childAfterHwnd, IntPtr className, string windowText);

        [DllImport(DLL_NAME, SetLastError = true)]
        public static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        [DllImport(DLL_NAME, SetLastError = true)]
        public static extern void SwitchToThisWindow(IntPtr hWnd, bool fAltTab);

        [DllImport(DLL_NAME, SetLastError = true)]
        public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int x, int y, int cx, int cy, uint uFlags);

		[DllImport(DLL_NAME, SetLastError = true)]
		public static extern bool IsWindowVisible(IntPtr hWnd);

		[DllImport(DLL_NAME, SetLastError = true)]
		public static extern bool IsIconic(IntPtr hWnd);


		[DllImport(DLL_NAME, SetLastError = true)]
		public static extern int EnumDisplaySettings(string deviceName, int modeNum, ref DEVMODE devMode);

		/// <summary>Changes the settings of the default display device to the specified graphics mode.</summary>
		/// <param name="devMode">Pointer to a DEVMODE structure that describes the new graphics mode. If lpDevMode is NULL, all the values currently in the registry will be used for the display setting.</param>
		/// <param name="flags">DWORD: Indicates how the graphics mode should be changed. This parameter can be one of the following values</param>
		/// <returns></returns>
		/// <remarks>Apps that you design to target Windows 8 and later can no longer query or set display modes that are less than 32 bits per pixel (bpp); these operations will fail.</remarks>
		/// <remarks>Passing NULL for the lpDevMode parameter and 0 for the dwFlags parameter is the easiest way to return to the default mode after a dynamic mode change.</remarks>
		[DllImport(DLL_NAME, SetLastError = true)]
		public static extern int ChangeDisplaySettings(ref DEVMODE devMode, int flags);
		#endregion


		#region method wrappers
		public static int		SetMainScreenResolution(int iWidth, int iHeight)
		{
			var iRet = int.MinValue;

			var dm = new DEVMODE();
			dm.dmDeviceName = new String(new char[32]);
			dm.dmFormName = new String(new char[32]);
			dm.dmSize = (short)Marshal.SizeOf(dm);

			if (0 != EnumDisplaySettings(null, ENUM_REGISTRY_SETTINGS, ref dm))
			{

				dm.dmPelsWidth = iWidth;
				dm.dmPelsHeight = iHeight;

				iRet = ChangeDisplaySettings(ref dm, CDS_TEST);
				if (iRet == DISP_CHANGE_FAILED)
					return iRet;

				iRet = ChangeDisplaySettings(ref dm, CDS_FULLSCREEN);
			}

			return iRet;
		}

		public static bool		IsForegroundProcess(int procId)
		{
			var hWnd = GetForegroundWindow();
			if (hWnd == IntPtr.Zero)
				return false;

			int foregroundPid;
			if (GetWindowThreadProcessId(hWnd, out foregroundPid) == 0)
				return false;

			return (foregroundPid == procId);
		}

		public static bool IsWindowTopMost(IntPtr hWnd)
		{
			if (IntPtr.Zero == hWnd)
				return false;

			var dwExStyle = (uint)GetWindowLong(hWnd, (int) EGetWindowLongIndexFlags.GWL_EXSTYLE);
			return ((dwExStyle & (uint) EWindowStylesEx.WS_EX_TOPMOST) != 0);
		}


		public static IntPtr	GetWindowHandle(string wText)
		{
			return FindWindow(null, wText);
		}

		public static void		WindowRestore(IntPtr hWnd)
		{
			if (hWnd == IntPtr.Zero)
				return;

			ShowWindow(hWnd, WinMessage.WMParam.SW_SHOWNORMAL);
			SwitchToThisWindow(hWnd, true);
		}

		public static void		WindowHide(IntPtr hWnd)
		{
			if (hWnd == IntPtr.Zero)
				return;

			ShowWindow(hWnd, WinMessage.WMParam.SW_HIDE);
		}

		public static void		SetWindowTopMost(IntPtr hWnd, IntPtr hWndParent, bool isVisible = true)
		{
			if (hWnd == IntPtr.Zero)
				return;

			if (hWndParent != IntPtr.Zero)
				SetParent(hWnd, hWndParent);

			var uFlags = WinMessage.WMParam.SWP_NOMOVE | WinMessage.WMParam.SWP_NOSIZE;
			if (isVisible)
				uFlags |= WinMessage.WMParam.SWP_SHOWWINDOW;

			BringWindowToTop(hWnd);
			SetWindowPos(hWnd, (IntPtr)(-1), 0, 0, 0, 0, uFlags);
			SetForegroundWindow(hWnd);
			SetActiveWindow(hWnd);
		}
		public static void		SetWindowTopMost(IntPtr hWnd, bool isVisible = true)
		{
			SetWindowTopMost(hWnd, IntPtr.Zero, isVisible);
		}

		public static void		WindowsTaskBarHide()
		{
			ShowWindow(WND_CLASS_NAME_TRAY, false);

			var hwndOrb = FindWindowEx(IntPtr.Zero, IntPtr.Zero, (IntPtr)WND_CLASS_ATOM_START_BUTTON, null);
			ShowWindow(hwndOrb, WinMessage.WMParam.SW_HIDE);
		}

		public static void		WindowsTaskBarShow()
		{
			ShowWindow(WND_CLASS_NAME_TRAY, true);

			var hwndOrb = FindWindowEx(IntPtr.Zero, IntPtr.Zero, (IntPtr)WND_CLASS_ATOM_START_BUTTON, null);
			ShowWindow(hwndOrb, WinMessage.WMParam.SW_SHOWNORMAL);
		}

		public static void		ShowWindow(string className, bool isShow)
		{
			var hWnd = FindWindow(className, string.Empty);
			if (hWnd != IntPtr.Zero)
				ShowWindow(hWnd, (isShow ? WinMessage.WMParam.SW_SHOWNORMAL : WinMessage.WMParam.SW_HIDE));
		}

		#endregion
	}
}
