using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Rimed.Framework.GUI
{
	public static class GuiExtensionMethods
	{
		/// <summary>Set transparent pictureBox control. Transparent color located at image/BackgroundImage (0,0) coordinates.</summary>
		public static void SetTransparent(this PictureBox picBox)
		{
			if (picBox == null)
				return;

			var img = picBox.Image;
			if (img == null)
			{
				img = picBox.BackgroundImage;
				if (img == null)
					return;
			}

			using (var bmp = new Bitmap(img))
			{
				var gp = new GraphicsPath();

				var mask = bmp.GetPixel(0, 0);
				for (var x = 0; x <= bmp.Width - 1; x++)
				{
					for (var y = 0; y <= bmp.Height - 1; y++)
					{
						if (!bmp.GetPixel(x, y).Equals(mask))
							gp.AddRectangle(new Rectangle(x, y, 1, 1));
					}
				}

				picBox.Region = new Region(gp);
			}
		}

		public static Bitmap CaptureSnapshot(this Control ctrl)
		{
			return HelperMethods.CaptureControlSnapshot(ctrl);
		}


		public static bool SetImage(this PictureBox pictureBox, Image image)
		{
			if (pictureBox == null)
				return false;

			var result = false;
			try
			{
				pictureBox.Image = (image == null) ? new Bitmap(100, 100) : image;

				result = (image != null);
			}
			catch (Exception ex)
			{
				System.Diagnostics.Trace.WriteLine(string.Format("SetImage: An exception was raised when trying to set {0}.Image. {1}", pictureBox.GetType().Name, ex));
			}

			return result;
		}

	}
}
