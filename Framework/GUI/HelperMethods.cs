﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Rimed.Framework.Common;
using Rimed.Framework.WinAPI;
using Rimed.Framework.WinAPI.Struct;

namespace Rimed.Framework.GUI
{
	public static class HelperMethods
	{
		public static Bitmap CaptureControlSnapshot(Control ctrl)
		{
			if (ctrl == null)
				return null;

			var rc = new RECT();
			if (!User32.GetWindowRect(ctrl.Handle, ref rc))
				return null;

			// create a bitmap from the visible clipping bounds of the graphics object from the window
			var bmp = new Bitmap(rc.Width, rc.Height);

			// create a graphics object from the bitmap
			var hdcBitmap = IntPtr.Zero;
			var hdcWindow = IntPtr.Zero;
			using (var grp = Graphics.FromImage(bmp))
			{
				try
				{
					// get a device context for the bitmap
					hdcBitmap = grp.GetHdc();

					// get a device context for the window
					hdcWindow = User32.GetWindowDC(ctrl.Handle);

					// bitblt the window to the bitmap
					GDI32.BitBlt(hdcBitmap, 0, 0, rc.Width, rc.Height, hdcWindow, 0, 0, (int) GDI32.ETernaryRasterOperations.SRCCOPY);
				}
				catch (Exception ex)
				{
					Logger.LogError(ex);
					return null;
				}
				finally
				{
					// release the bitmap's device context
					if (hdcBitmap != IntPtr.Zero)
						grp.ReleaseHdc(hdcBitmap);

					if (hdcWindow != IntPtr.Zero)
						User32.ReleaseDC(ctrl.Handle, hdcWindow);
				}
			}

			// return the bitmap of the window
			return bmp;
		}
	}
}
