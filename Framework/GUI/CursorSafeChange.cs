using System.Windows.Forms;
using Rimed.Framework.Common;

namespace Rimed.Framework.GUI
{
	public sealed class CursorSafeChange : DisposableBase
	{
		private readonly Cursor m_previous = Cursors.Default;
		private readonly Control m_ctrl = null;
		private readonly bool m_isSuspendlayout;
		public CursorSafeChange(Control ctrl, bool isSuspendlayout = false, Cursor csr = null)
		{
			if (csr == null)
				csr = Cursors.WaitCursor;

			m_isSuspendlayout	= isSuspendlayout;
			m_ctrl				= ctrl;
			if (m_ctrl == null)
				return;

			m_previous		= m_ctrl.Cursor;
			m_ctrl.Cursor	= csr;
			if (m_isSuspendlayout)
				m_ctrl.SuspendLayout();
		}
		protected override void Dispose(bool isDisposing)
		{
			if (m_ctrl == null)
				return;

			m_ctrl.Cursor = m_previous;
			if (m_isSuspendlayout)
				m_ctrl.ResumeLayout(true);
		}
	}
}
