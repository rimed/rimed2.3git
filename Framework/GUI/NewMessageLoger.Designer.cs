﻿namespace Rimed.Framework.GUI
{
    partial class NewMessageLoger
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblHeader = new System.Windows.Forms.Label();
            this.lblMessage = new System.Windows.Forms.Label();
            this.imgbtnClose = new Rimed.Framework.GUI.ImageButton();
            this.imgbtnNo = new Rimed.Framework.GUI.ImageButton();
            this.imgbtnYes = new Rimed.Framework.GUI.ImageButton();
            this.imgbtnCancel = new Rimed.Framework.GUI.ImageButton();
            this.imgbtnOk = new Rimed.Framework.GUI.ImageButton();
            ((System.ComponentModel.ISupportInitialize)(this.imgbtnClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgbtnNo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgbtnYes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgbtnCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgbtnOk)).BeginInit();
            this.SuspendLayout();
            // 
            // lblHeader
            // 
            this.lblHeader.AutoSize = true;
            this.lblHeader.BackColor = System.Drawing.Color.Transparent;
            this.lblHeader.Font = new System.Drawing.Font("Tahoma", 9.25F, System.Drawing.FontStyle.Bold);
            this.lblHeader.Location = new System.Drawing.Point(45, 56);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(47, 16);
            this.lblHeader.TabIndex = 6;
            this.lblHeader.Text = "Hader";
            // 
            // lblMessage
            // 
            this.lblMessage.BackColor = System.Drawing.Color.Transparent;
            this.lblMessage.Font = new System.Drawing.Font("Tahoma", 9.25F, System.Drawing.FontStyle.Bold);
            this.lblMessage.Location = new System.Drawing.Point(45, 81);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(278, 34);
            this.lblMessage.TabIndex = 5;
            this.lblMessage.Text = "Message";
            // 
            // imgbtnClose
            // 
            this.imgbtnClose.BackColor = System.Drawing.Color.Transparent;
            this.imgbtnClose.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imgbtnClose.DownImage = null;
            this.imgbtnClose.HoverImage = null;
            this.imgbtnClose.Location = new System.Drawing.Point(328, 10);
            this.imgbtnClose.Name = "imgbtnClose";
            this.imgbtnClose.NormalImage = null;
            this.imgbtnClose.Size = new System.Drawing.Size(29, 30);
            this.imgbtnClose.TabIndex = 5;
            this.imgbtnClose.TabStop = false;
            this.imgbtnClose.Click += new System.EventHandler(this.imgbtnClose_Click);
            // 
            // imgbtnNo
            // 
            this.imgbtnNo.BackColor = System.Drawing.Color.Transparent;
            this.imgbtnNo.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imgbtnNo.DownImage = global::Rimed.Framework.Properties.Resources.Alert_No_03;
            this.imgbtnNo.HoverImage = global::Rimed.Framework.Properties.Resources.Alert_No_02;
            this.imgbtnNo.Location = new System.Drawing.Point(199, 125);
            this.imgbtnNo.Name = "imgbtnNo";
            this.imgbtnNo.NormalImage = global::Rimed.Framework.Properties.Resources.Alert_No_01;
            this.imgbtnNo.Size = new System.Drawing.Size(64, 24);
            this.imgbtnNo.TabIndex = 2;
            this.imgbtnNo.TabStop = false;
            this.imgbtnNo.Click += new System.EventHandler(this.imgbtnNo_Click);
            // 
            // imgbtnYes
            // 
            this.imgbtnYes.BackColor = System.Drawing.Color.Transparent;
            this.imgbtnYes.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imgbtnYes.DownImage = global::Rimed.Framework.Properties.Resources.Alert_Yes_03;
            this.imgbtnYes.HoverImage = global::Rimed.Framework.Properties.Resources.Alert_Yes_02;
            this.imgbtnYes.Location = new System.Drawing.Point(102, 125);
            this.imgbtnYes.Name = "imgbtnYes";
            this.imgbtnYes.NormalImage = global::Rimed.Framework.Properties.Resources.Alert_Yes_01;
            this.imgbtnYes.Size = new System.Drawing.Size(64, 24);
            this.imgbtnYes.TabIndex = 1;
            this.imgbtnYes.TabStop = false;
            this.imgbtnYes.Click += new System.EventHandler(this.imgbtnYes_Click);
            // 
            // imgbtnCancel
            // 
            this.imgbtnCancel.BackColor = System.Drawing.Color.Transparent;
            this.imgbtnCancel.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imgbtnCancel.DownImage = global::Rimed.Framework.Properties.Resources.Alert_Cancel_03;
            this.imgbtnCancel.HoverImage = global::Rimed.Framework.Properties.Resources.Alert_Cancel_02;
            this.imgbtnCancel.Location = new System.Drawing.Point(199, 125);
            this.imgbtnCancel.Name = "imgbtnCancel";
            this.imgbtnCancel.NormalImage = global::Rimed.Framework.Properties.Resources.Alert_Cancel_01;
            this.imgbtnCancel.Size = new System.Drawing.Size(64, 24);
            this.imgbtnCancel.TabIndex = 8;
            this.imgbtnCancel.TabStop = false;
            this.imgbtnCancel.Click += new System.EventHandler(this.imgbtnCancel_Click);
            // 
            // imgbtnOk
            // 
            this.imgbtnOk.BackColor = System.Drawing.Color.Transparent;
            this.imgbtnOk.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imgbtnOk.DownImage = global::Rimed.Framework.Properties.Resources.Alert_OK_03;
            this.imgbtnOk.HoverImage = global::Rimed.Framework.Properties.Resources.Alert_OK_02;
            this.imgbtnOk.Location = new System.Drawing.Point(102, 125);
            this.imgbtnOk.Name = "imgbtnOk";
            this.imgbtnOk.NormalImage = global::Rimed.Framework.Properties.Resources.Alert_OK_01;
            this.imgbtnOk.Size = new System.Drawing.Size(64, 24);
            this.imgbtnOk.TabIndex = 7;
            this.imgbtnOk.TabStop = false;
            this.imgbtnOk.Click += new System.EventHandler(this.imgbtnOk_Click);
            // 
            // NewMessageLoger
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImage = global::Rimed.Framework.Properties.Resources.Alert_02;
            this.ClientSize = new System.Drawing.Size(373, 173);
            this.Controls.Add(this.imgbtnClose);
            this.Controls.Add(this.lblHeader);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.imgbtnNo);
            this.Controls.Add(this.imgbtnYes);
            this.Controls.Add(this.imgbtnCancel);
            this.Controls.Add(this.imgbtnOk);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "NewMessageLoger";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NewMessage";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.imgbtnClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgbtnNo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgbtnYes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgbtnCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgbtnOk)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblHeader;
        private System.Windows.Forms.Label lblMessage;
        private ImageButton imgbtnYes;
        private ImageButton imgbtnNo;
        private ImageButton imgbtnClose;
        private ImageButton imgbtnCancel;
        private ImageButton imgbtnOk;

    }
}