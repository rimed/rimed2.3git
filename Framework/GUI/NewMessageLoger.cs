﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using Rimed.Framework.Common;

namespace Rimed.Framework.GUI
{
    // Added by Alex 01/05/2016 bug #849 v 2.2.3.31
    public partial class NewMessageLoger : Form
    {
       public DialogResult result;

        public NewMessageLoger()
        {
            InitializeComponent();
        }
        
        public NewMessageLoger(string header, string message, MessageBoxButtons buttons)
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.None;
            this.AllowTransparency = true;
            this.BackColor = Color.AliceBlue;
            this.TransparencyKey = this.BackColor;
            lblHeader.Text = header;
            lblMessage.Text = message;

            imgbtnYes.Visible = false;
            imgbtnNo.Visible = false;
            imgbtnCancel.Visible = false;
            imgbtnOk.Visible = false;
            switch (buttons)
            {
                case MessageBoxButtons.YesNo:
                    imgbtnYes.Visible = true;
                    imgbtnNo.Visible = true;
                    break;
                case MessageBoxButtons.OKCancel:
                    imgbtnOk.Visible = true;
                    imgbtnCancel.Visible = true;
                    break;
                case MessageBoxButtons.OK:
                    imgbtnOk.Visible = true;
                    imgbtnOk.Left = Width / 2 - imgbtnOk.Width / 2;
                    break;
                default:
                    Logger.LogInfo("The buttons set don't supported. The default set for OK button will used.");
                    imgbtnOk.Visible = true;
                    imgbtnOk.Left = Width / 2 - imgbtnOk.Width / 2;
                    break;
            }
        }

        private void imgbtnYes_Click(object sender, EventArgs e)
        {
            result = DialogResult.Yes;
            Logger.LogInfo("NewMessageLoger('{0}', '{1}'). User action: {2}.", lblHeader.Text.Replace("\n", "<CR>"), lblMessage.Text.Replace("\n", "<CR>"), result);
            Close();
        }

        private void imgbtnNo_Click(object sender, EventArgs e)
        {
            result = DialogResult.No;
            Logger.LogInfo("NewMessageLoger('{0}', '{1}'). User action: {2}.", lblHeader.Text.Replace("\n", "<CR>"), lblMessage.Text.Replace("\n", "<CR>"), result);
            Close();
        }

        private void imgbtnClose_Click(object sender, EventArgs e)
        {
            result = DialogResult.Cancel;
            Logger.LogInfo("NewMessageLoger('{0}', '{1}'). User action: {2}.", lblHeader.Text.Replace("\n", "<CR>"), lblMessage.Text.Replace("\n", "<CR>"), result);
            Close();
        }

        private void imgbtnOk_Click(object sender, EventArgs e)
        {
            result = DialogResult.OK;
            Logger.LogInfo("NewMessageLoger('{0}', '{1}'). User action: {2}.", lblHeader.Text.Replace("\n", "<CR>"), lblMessage.Text.Replace("\n", "<CR>"), result);
            Close();
        }

        private void imgbtnCancel_Click(object sender, EventArgs e)
        {
            result = DialogResult.Cancel;
            Logger.LogInfo("NewMessageLoger('{0}', '{1}'). User action: {2}.", lblHeader.Text.Replace("\n", "<CR>"), lblMessage.Text.Replace("\n", "<CR>"), result);
            Close();
        }
    }
}
