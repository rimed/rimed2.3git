﻿using System;
using System.Windows.Forms;

namespace Rimed.Framework.GUI
{
	public partial class InputDialog : Form
	{

		private InputDialog()
		{
			InitializeComponent();
		}


		public static string GetUserInput(Form owner, string caption, string label)
		{
			string result;

			if (owner == null)
			{
				using (var topmostForm = new Form { Size = new System.Drawing.Size(1, 1), StartPosition = FormStartPosition.CenterScreen })
				{
					WinAPI.User32.SetWindowTopMost(topmostForm.Handle, false);
					result = getUserInput(topmostForm, caption, label);
				}
			}
			else
			{
				result = getUserInput(owner, caption, label);
			}

			return result;
		}

		private static string getUserInput(Form owner, string caption, string label)
		{
			var result = string.Empty;

			if (owner == null)
				return result;

			if (owner.InvokeRequired)
			{
				owner.Invoke((Action)delegate() { result = getUserInput(owner, caption, label); });
				return result;
			}

			using (var dlg = new InputDialog())
			{
				dlg.Text = caption;
				dlg.labelLabel.Text = label;
				if (dlg.ShowDialog(owner) == DialogResult.OK)
					result = dlg.textBox.Text;
			}

			return result;
		}


		private void buttonOK_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.OK;
		}

		private void buttonCancel_Click(object sender, EventArgs e)
		{
			DialogResult = DialogResult.Cancel;
		}

		private void InputDialog_Load(object sender, EventArgs e)
		{
			textBox.Focus();
		}
	}
}
