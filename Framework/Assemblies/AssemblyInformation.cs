﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace Rimed.Framework.Assemblies
{
    public class AssemblyInformation
	{
        private readonly Assembly m_assembly = null;

        #region - Public members - 
        #region   - Public constructors - 
        public AssemblyInformation()
            : this(null)
        {
        }

        public AssemblyInformation(Assembly asm)
        {
	        if (asm == null)
				asm = Assembly.GetEntryAssembly();	// Entry assembly can be null if it's an unmanaged code

            if (asm == null)
                m_assembly = Assembly.GetCallingAssembly();
            else
                m_assembly = asm;
        }
        #endregion

        public Assembly Assembly		{ get { return m_assembly; } }

        public string	Name			{ get { return GetName(m_assembly); }}

		public string	Version			
		{
			get 
			{
                var ver = GetVersion(m_assembly);
                return (ver != null) ? ver.ToString() : string.Empty;
			}
		}

		public FileVersionInfo FileVersion { get { return GetFileVersionInfo(m_assembly); } }

		public string	Title			{ get { return GetTitle(m_assembly); } }

    	public string	Description		{ get { return GetDescription(m_assembly); }}

		public string	Product			{ get { return GetProduct(m_assembly); }}

		public string	Copyright		{ get { return GetCopyright(m_assembly); }}

		public string	Company			{ get {return GetCompany(m_assembly); }}

        public string   Location		{ get { return GetLocation(m_assembly); } }

		public string	Directory		{ get { return GetDirectory(m_assembly); } }

		public DateTime BuildTimestamp { get { return GetBuildTimestamp(m_assembly); } }

		public string	QualifiedName	{ get { return GetQualifiedName(m_assembly); } }


		public override string ToString()
		{
			return string.Format("{0} {1}. {2} {3} v{4} {5}. (c){6}", Company, Product, Name, Title, Version, BuildTimestamp.ToString("yyyy.MM.dd HH:mm:ss"), Copyright);
		}
        #endregion

        #region - Static members - 
        public static string	            GetName(Assembly asm = null)
		{
	        if (asm == null)
		        asm = Assembly.GetCallingAssembly();
            
			var asmName = asm.GetName();
			return asmName.Name;
		}

		public static string	            GetLocation(Assembly asm = null)
		{
			if (asm == null)
				asm = Assembly.GetCallingAssembly();
            
			return asm.Location;
		}

		public static string				GetCodeBase(Assembly asm = null)
		{
			if (asm == null)
				asm = Assembly.GetCallingAssembly();

			return asm.CodeBase;
		}

		public static string				GetDirectory(Assembly asm = null)
		{
			var codeBase	= GetCodeBase(asm);
			var uri			= new UriBuilder(codeBase);
			var path		= Uri.UnescapeDataString(uri.Path);

			return Path.GetDirectoryName(path);
		}

		public static string				GetQualifiedName(Assembly asm = null)
		{
			if (asm == null)
				asm = Assembly.GetCallingAssembly();

			return asm.ManifestModule.FullyQualifiedName;
		}

		public static Version               GetVersion(Assembly asm = null)
		{
			if (asm == null)
				asm = Assembly.GetCallingAssembly();

			return asm.GetName().Version;
		}

        public static string                GetTitle(Assembly asm = null)
		{
			if (asm == null)
				asm = Assembly.GetCallingAssembly();

			// Get assembly on process executable in the default application domain 
			var attributes = asm.GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
			
            // If there is at least one Title attribute
            if (attributes.Length > 0)
			{
				// Select the first one
				var titleAttribute = attributes[0] as AssemblyTitleAttribute;
				
				// If it is not an empty string, return it
				if (titleAttribute != null && !string.IsNullOrEmpty(titleAttribute.Title))
					return titleAttribute.Title;
			}

			// If there was no Title attribute, or Title attribute is empty, return empty string
	        return string.Empty;
		}

		public static string	            GetDescription(Assembly asm = null)
		{
			if (asm == null)
				asm = Assembly.GetCallingAssembly();

			var attributes = asm.GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
			
			// If there aren't any Description attributes, return an empty string
            if (attributes.Length == 0)
				return string.Empty;

			// Select the first one
			var attr = attributes[0] as AssemblyDescriptionAttribute;

			if (attr == null)
				return string.Empty;

			// If there is a Description attribute, return its value
			return attr.Description;
		}

		public static string	            GetProduct(Assembly asm = null)
		{
			if (asm == null)
				asm = Assembly.GetCallingAssembly();

			var attributes = asm.GetCustomAttributes(typeof(AssemblyProductAttribute), false);
			
			// If there aren't any Product attributes, return an empty string
            if (attributes.Length == 0)
				return string.Empty;

			// Select the first one
			var attr = attributes[0] as AssemblyProductAttribute;

			if (attr == null)
				return string.Empty;

			// If there is a Product attribute, return its value
			return attr.Product;
		}

		public static string	            GetCopyright(Assembly asm = null)
		{
			if (asm == null)
				asm = Assembly.GetCallingAssembly();

			// Get all Copyright attributes on this assembly
			// Get assembly on process executable in the default application domain 
			var attributes = asm.GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
			
			// If there aren't any Copyright attributes, return an empty string
            if (attributes.Length == 0)
				return string.Empty;

			// Select the first one
			var attr = attributes[0] as AssemblyCopyrightAttribute;

			if (attr == null)
				return string.Empty;

			// If there is a Copyright attribute, return its value
			return attr.Copyright;
		}

		public static string	            GetCompany(Assembly asm = null)
		{
			if (asm == null)
				asm = Assembly.GetCallingAssembly();

			var attributes = asm.GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
			
			// If there aren't any Company attributes, return an empty string
            if (attributes.Length == 0)
				return string.Empty;

			// Select the first one
			var attr = attributes[0] as AssemblyCompanyAttribute;

			if (attr == null)
				return string.Empty;

			// If there is a Company attribute, return its value
			return attr.Company;
		}

        public static DateTime              GetBuildTimestamp(Assembly asm = null)
        {
			if (asm == null)
				asm = Assembly.GetCallingAssembly();

			var dt = DateTime.MinValue;
            var ver	= GetVersion(asm);
            if (ver != null)
            {
                // v.Build		= days since Jan. 1, 2000
                // v.Revision*2 = Seconds since local midnight
                // (NO daylight saving time)
				dt = (new DateTime(2000, 01, 01, 0, 0, 0, DateTimeKind.Utc)).AddDays(ver.Build).AddSeconds(ver.Revision * 2);
				//dt = new DateTime(ver.Build * TimeSpan.TicksPerDay + ver.Revision * TimeSpan.TicksPerSecond * 2).AddYears(1999);
			}

            return dt;
        }

	    public static FileVersionInfo		GetFileVersionInfo(Assembly asm = null)
	    {
			if (asm == null)
				asm = Assembly.GetCallingAssembly();

			return FileVersionInfo.GetVersionInfo(asm.Location);	
	    }

		public static FileVersionInfo		GetFileVersionInfo(string fileLocation)
		{
			if (string.IsNullOrWhiteSpace(fileLocation))
				return null;

			try
			{
				return FileVersionInfo.GetVersionInfo(fileLocation);
			}
			catch (Exception)
			{
				return null;
			}
		}
		#endregion

		#region - EntryAssembly singletone -
		private static AssemblyInformation s_entryAssemblyInfo = null;
	    public static AssemblyInformation	EntryAssembly
	    {
		    get
		    {
			    if (s_entryAssemblyInfo == null)
					s_entryAssemblyInfo = new AssemblyInformation(Assembly.GetEntryAssembly());

			    return s_entryAssemblyInfo;
		    }
		}
		#endregion
	}
}
