﻿using System.Collections.Generic;
using Rimed.Framework.Common;
using Rimed.Framework.WinAPI;

namespace Rimed.Framework.Threads
{
	public sealed class OsTimerResolution: Disposable
	{
		#region Static members
		private const uint	MIN_RESOLUTION = 1;
		private const string CLASS_NAME = "OsTimerResolution";

		private static readonly OsTimerResolution s_osTimerResolution = new OsTimerResolution();

		public static bool Set(uint ms = MIN_RESOLUTION)
		{
			return s_osTimerResolution.set(ms);
		}

		public static void Reset(uint ms)
		{
			s_osTimerResolution.reset(ms);
		}

		public static void ResetLast()
		{
			s_osTimerResolution.reset();
		}

		public static void Clear()
		{
			s_osTimerResolution.clear();
		}
		#endregion


		#region Instance members
		private readonly List<uint> m_resolutions = new List<uint>();

		private bool set(uint ms)
		{
			if (ms < MIN_RESOLUTION)
				return false;

			Logger.LogDebug("{0}.set(ms={1})", CLASS_NAME, ms);

			lock (m_resolutions)
			{
				var res = WinMM.timeBeginPeriod(ms);
				if (res != WinMM.TIMERR_NOERROR)
					return false;

				m_resolutions.Add(ms);
			}

			return true;
		}

		private void reset(uint ms = MIN_RESOLUTION -1)
		{
			if (m_resolutions.Count == 0)
				return;

			Logger.LogDebug("{0}.reset(ms={1})", CLASS_NAME, ms);

			lock (m_resolutions)
			{
				if (ms < MIN_RESOLUTION)
					ms = m_resolutions[m_resolutions.Count - 1];

				if (m_resolutions.Remove(ms))
					WinAPI.WinMM.timeEndPeriod(ms);
			}
		}

		private void clear()
		{
			Logger.LogDebug("{0}.clear()", CLASS_NAME);

			while (m_resolutions.Count > 0)
			{
				reset();
			}
		}

		protected override void DisposeUnmanagedResources()
		{
			clear();
			base.DisposeUnmanagedResources();
		}
		#endregion
	}
}
