﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading;
using Rimed.Framework.Common;
using ThreadState = System.Threading.ThreadState;

namespace Rimed.Framework.Threads
{
    /// <summary>Thread class extension, Add inter thread communication mechanism.</summary>
    /// <remarks>
    /// Events invoked on this thread context:
    ///     MainLoopStarting()
    ///     Event: OnStart
    ///     While Running
    ///         Event: OnPause 
    ///         Event: OnContinue
    ///         Event: OnTimer
    /// 
    ///         WORK: LoopIteration(ref obj)  
    /// 
    ///         Event: OnEmpty
    ///         Event: OnWaitTimeout
    ///     End While
    ///     Event: OnEnd
    ///     MainLoopEnding()
    /// On exception:   Event: OnException(ex)
    /// 
    /// NOTE: All events and methods executed in sequence on this thread context
    /// </remarks>
    public abstract class QThreadBase : DisposableBase
    {
        #region Public ------------------------------------------------------------------------
        #region - Public structures 
        public struct LoopbackAck
        {
            public QThreadBase AckThread { get; set; }
            public DateTime AckTime { get; set; }
            public DateTime SendTime { get; set; }

            public int      RoundTripDuration
            {
                get
                {
                    return (int)AckTime.Subtract(SendTime).TotalMilliseconds;                    
                }
            }
        }
        #endregion

        #region - Public.Constants
        /// <summary>= System.Threading.Timeout.Infinite.</summary>
        public const int WAIT_TIMEOUT_INFINITE = Timeout.Infinite;

        /// <summary>Flag = 0.</summary>
        public const int WAIT_TIMEOUT_TIMESLICE = 0;

        /// <summary>Flag = int.MinValue.</summary>
        public const int WAIT_TIMEOUT_NONE = int.MinValue;
        #endregion  // Public.Constants

        #region - Public.Delegates
        public delegate bool DelegateOnEvent();
        public delegate bool DelegateOnParamEvent(object obj);
        public delegate bool DelegateOnException(QThreadBase qTrd, Exception e);
        public delegate bool DelegateOnQueueEvent(ref object obj);
        public delegate bool DelegateOnLoopbackAckEvent(LoopbackAck ack);
        #endregion  // Public.Delegates

        #region - Public.Events
        public event DelegateOnException        OnException;
            
        public event DelegateOnEvent            OnWaitTimeout;
        public event DelegateOnEvent            OnTimer;
        public event DelegateOnEvent            OnEmpty;

        public event DelegateOnEvent            OnPause;
        public event DelegateOnEvent            OnContinue;

        public event DelegateOnEvent            OnStart;
        public event DelegateOnEvent            OnEnd;

        public event DelegateOnLoopbackAckEvent OnLoopbackAck;
        #endregion	//Public.Events

        #region - Public.Properties
        /// <summary>
        /// QThread internal loop Sleep time out.
        /// Use SleepTimeout for Common used sleep timeout values
        /// </summary>
        public int  WaitTimeout { get; set; }

        /// <summary>Timer pulse interval in mSec</summary>
        /// <remarks>Min value TIMER_MIN_INTERVAL_MSEC (25) mSec.</remarks>
        public int  TimerInterval
        {
            get
            {
                return m_iTimerIntervalMSec;
            }
            set
            {
                if (value <= 0)
                    TimerIsEnabled = false;
                else
                {
                    if (value < TIMER_MIN_INTERVAL_MSEC)
                        throw new ArgumentException(string.Format("{0}: TimerInterval property cannt be smaller than {1} mSec, {2} is an invalid value.", Name, TIMER_MIN_INTERVAL_MSEC, value), "TimerInterval");

                    m_iTimerIntervalMSec = value;

                    TimerIsEnabled = true;
                }
            }
        }

        public bool TimerIsEnabled
        {
            get
            {
                if (TimerInterval <= 0)
                    m_bTimerIsEnabled = false;

                return m_bTimerIsEnabled;
            }
            set
            {
                m_bTimerIsEnabled = value;

                m_swTimeFromLastEvent.Reset();

                if (m_bTimerIsEnabled)
                    m_swTimeFromLastEvent.Start();
                else
                    m_swTimeFromLastEvent.Stop();
            }
        }

        /// <summary>Class instance Name, default value: ClassType#Id.</summary>
        public string Name
        {
            get
            {
                return m_thread.Name;
            }
        }

        /// <summary>Indicate the instance thread execution status.</summary>
        public bool IsAlive
        {
            get
            {
                    return m_thread.IsAlive;
            }
        }

        public bool IsUnstarted
        {
            get
            {
                return isThreadStateOn(ThreadState.Unstarted);

            }
        }

        public bool IsStopped
        {
            get { return isThreadStateOn(ThreadState.Stopped); }
        }

        public bool IsRunning
        {
            get { return IsAlive && (!IsPaused) && isThreadStateOn(ThreadState.Running); }
        }

        /// <summary>Return # of objects in all thread queues.</summary>
        public int Count
        {
            get
            {
                return m_qIterationQueue.Count;
            }
        }

        /// <summary>Indicate if thread all queues are empty or not.</summary>
        public bool IsEmpty
        {
            get { return m_qIterationQueue.IsEmpty; }
        }

        /// <summary>Indicate if iteration loop is paused.</summary>
        public bool IsPaused { get; protected set; }

        /// <summary>QThread instance unique runtime id.</summary>
        public int  ManagedThreadId
        {
            get { return m_thread.ManagedThreadId; }
        }

        public int Id { get; private set; }
        #endregion // Public.Properties

        #region - Public.Methods
        #region - Public.Methods.Synchronus
        #region Override object methods  
        public override bool Equals(object obj)
        {
            var other = obj as QThreadBase;
            if (other == null)
                return false;

            return m_qIterationQueue.Equals(other.m_qIterationQueue) && m_thread.Equals(other.m_thread);
        }

        public override int GetHashCode()
        {
            return m_thread.GetHashCode() ^ m_qIterationQueue.GetHashCode();
        }

        public override string ToString()
        {
            string s = string.Format(FMT_TOSTRING, Name, ManagedThreadId, IsAlive, IsPaused, WaitTimeout, Count, State, Priority, EnqueueCount, DequeueCount, TimerIsEnabled, TimerInterval);
            return s;
        }
        #endregion

        /// <summary>Start the instance main loop.</summary>
        /// <returns>True: QThread started.</returns>
        /// <remarks>If thread already stoped new thread is created, initiated and started.</remarks>
        public bool Start()
        {
            lock (m_thread)
            {
                if (IsUnstarted)
                    m_thread.Start();
                else
                    return false;
            }

            return true;
        }

        /// <summary>Set Main loop exit flag to true - Exit thread on next iteration.</summary>
        public void Stop()
        {
            if (IsStopped || IsUnstarted)
                return;

            Trace.WriteLine(string.Format(FMT_MODULE_METHOD, Name, "Stop", string.Empty, this));

            if (!IsContinueMainLoop)
                return;

            // Disable QThread object deposit
            IsEnqueueEnabled = false;

            // Disable QThread object processing withdraw
            IsDequeueEnabled = false;

            // Set main loop exit flag
            IsContinueMainLoop = false;

            // Release pause wait
            Continue();

            WaitTimeout = WAIT_TIMEOUT_NONE;

            // Release loopWait (if on wait)
            signalLoopWait();
        }

        /// <summary>Blocks the calling thread until a thread terminates or timeout.</summary>
        /// <param name="iTimeoutMSec">Block timeout in mSec.</param>
        /// <returns>true: thread has terminated; false: timeout has elapsed.</returns>
        public bool Join(int iTimeoutMSec = WAIT_TIMEOUT_INFINITE)
        {
            if (IsUnstarted)
                throw new QThreadException(string.Format("{0}: Thread was not started.", Name));

            // Check self-join. 
            verifyNoSelfJoin();

            Trace.WriteLine(string.Format(FMT_MODULE_METHOD, Name, "Join", iTimeoutMSec, this));

            // Release loopWait (if on wait)
            signalLoopWait();
            bool bRet = m_thread.Join(iTimeoutMSec);

            return bRet;
        }

        /// <summary>Set Main loop exit flag to true and wait for thread termination with timeout.</summary>
        public bool StopJoin(int iTimeoutMSec = WAIT_TIMEOUT_INFINITE)
        {
            // Check self-join. 
            verifyNoSelfJoin();

            Trace.WriteLine(string.Format(FMT_MODULE_METHOD, Name, "StopJoin", iTimeoutMSec, this));

            Stop();
            bool bRet = Join(iTimeoutMSec);

            return bRet;
        }

        /// <summary>Raise iteration pause flag, with timeout in mSecs.</summary>
        /// <param name="iTimeoutMSec">Pause timeout in mSec. Default = Infinit.</param>
        /// <remarks>To release invoke <code>Continue()</code> or wait for timeout to elaps.</remarks>
        public void Pause(int iTimeoutMSec = WAIT_TIMEOUT_INFINITE)
        {
            m_iPauseTimeoutMSec = iTimeoutMSec;
            m_whPauseIteration.Reset();
            IsPaused = true;

            // Release loopWait (if on wait)
            signalLoopWait();

        }

        /// <summary>Release iteration paused by <code>Pause()</code> method.</summary>
        public void Continue()
        {
            resetPauseAttributes();
            m_whPauseIteration.Set();
        }

        public void TimerReset()
        {
            m_swTimeFromLastEvent.Reset();
            m_swTimeFromLastEvent.Start();
        }

        public bool PostObjectJoin(object obj, int timeout = WAIT_TIMEOUT_INFINITE, int checkInterval = WAIT_TIMEOUT_TIMESLICE)
        {
            //Verify call made from another thread else throw exception
            verifyNoSelfJoin();

            var msg = new BlockingMessage(obj);
            if (!enqueueObject(msg))
                return false;

            var expired = DateTime.UtcNow.AddMilliseconds(timeout);

            while (!msg.Processed && DateTime.UtcNow > expired)
            {
                Thread.Sleep(checkInterval);
            }

            return msg.Processed;
        }

        #endregion	//Public.Methods.Synchronus

        #region - Public.Methods.ASynchronus
        public bool PostObject(object obj)
        {
            if (obj == null)
                obj = new NullMessage();

            return enqueueObject(obj);
        }

        /// <summary>Place a LOOPBACK message in instance message queue.</summary>
        /// <param name="qTrdAckDest">Ack. message to destination QThread.</param>
        /// <returns>true: Message posted successfully; false: error.</returns>
        public bool PostLoopback(QThreadBase qTrdAckDest)
        {
            var b = false;

            if (qTrdAckDest != null)
                b = postQThreadMessage(QThreadMessage.MSGID_LOOPBACK, qTrdAckDest);

            return b;
        }

        /// <summary>Place a MSGID_WAKEABLE_WAIT message in instance message queue.</summary>
        /// <param name="iTimeoutMSec">Wait timeout in mSec. Default = Infinit.</param>
        /// <returns>true: Message posted successfully; false: error.</returns>
        public bool PostWait(int iTimeoutMSec = WAIT_TIMEOUT_INFINITE)
        {
            var b = false;

            if (iTimeoutMSec >= WAIT_TIMEOUT_TIMESLICE || iTimeoutMSec == WAIT_TIMEOUT_INFINITE)
                b = postQThreadMessage(QThreadMessage.MSGID_WAKEABLE_WAIT, iTimeoutMSec);

            return b;
        }

        /// <summary>Place a PAUSE message in instance message queue.</summary>
        /// <param name="iTimeoutMSec">Pause timeout in mSec. Default = Infinit.</param>
        /// <returns>true: Message posted successfully; false: error.</returns>
        public bool PostPause(int iTimeoutMSec = WAIT_TIMEOUT_INFINITE)
        {
            var bRet = false;

            if (iTimeoutMSec >= WAIT_TIMEOUT_TIMESLICE || iTimeoutMSec == WAIT_TIMEOUT_INFINITE)
                bRet = postQThreadMessage(QThreadMessage.MSGID_PAUSE, iTimeoutMSec);

            return bRet;
        }

        /// <summary>Place a STOP message in instance message queue.</summary>
        /// <returns>true: Message posted successfully; false: error.</returns>
        public bool PostStop()
        {
            Trace.WriteLine(string.Format(FMT_MODULE_METHOD, Name, "PostStop", string.Empty, this));
            
            return postQThreadMessage(QThreadMessage.MSGID_STOP);
        }

        /// <summary>Place a STOP message in instance message queue.</summary>
        /// <param name="iTimeoutMSec">Join timeout in mSec. Default = Infinit.</param>
        /// <returns>true: Message posted successfully; false: error.</returns>
        public bool PostStopJoin(int iTimeoutMSec = WAIT_TIMEOUT_INFINITE)
        {
            // Check self-join. 
            verifyNoSelfJoin();

            Trace.WriteLine(string.Format(FMT_MODULE_METHOD, Name, "PostStopJoin", iTimeoutMSec, this));

            PostStop();
            return Join(iTimeoutMSec);
        }

        #endregion	//Public.Methods.ASynchronus
        #endregion	//Public.Methods
        #endregion  //Public

        #region Internal ----------------------------------------------------------------------
         /// <summary>QThread termination handle event.</summary>
        internal ManualResetEvent TerminateHandle
        {
            get { return m_whThreadExit; }
        }
        #endregion

        #region Protected ---------------------------------------------------------------------
        #region - Protected.CONSTANTS
        protected const string FMT_TOSTRING = "Name={0}, Id={1}, IsAlive={2}, IsPaused={3}, WaitTimeout={4}, Count={5}, State={6}, Priority={7}, EnqueueCount={8}, DequeueCount={9}, TimerIsEnabled={10}, TimerInterval={11}.";

        /// <summary>OnTimer invoke min interval in mSec = 5</summary>
        protected const int TIMER_MIN_INTERVAL_MSEC = 5;
        #endregion //Protected.CONSTANTs


        #region - Protected.Constructors
		protected QThreadBase(string name = null, bool bAutoStart = false, int waitTimeout = 1000)
        {
            Id = Interlocked.Increment(ref s_instancCounter);

            // Enable QThread object processing withdraw
            IsDequeueEnabled = true;

            // Enable QThread object deposit
            IsEnqueueEnabled = true;

            // init operation flags
            IsContinueMainLoop = true;
            IsPaused = false;

            m_thread                = new Thread(threadStart);
            m_thread.IsBackground   = true;
            //m_thread.Name           = Id.ToString("D3") + "." + (string.IsNullOrEmpty(name) ? GetType().Name : name);
            m_thread.SetName((string.IsNullOrEmpty(name) ? GetType().Name : name));
            
            // Set default wait timeout
            WaitTimeout         = waitTimeout;


            if (bAutoStart)
                Start();
        }
        #endregion

        #region - Protected.Abstract.Methods
        /// <summary>Thread loop iteration method, invoked on every iteration where a valid object dequeued.</summary>
        abstract protected void LoopIteration(object obj);
        protected abstract void HandleException(Exception ex);
        #endregion	//Protected.Abstract.Methods

        #region - Protected.Virtual.Methods
        virtual protected void MainLoopStarting()
        {
            Trace.WriteLine(string.Format(FMT_MAIN_LOOP_MSG, Name, "START"));
        }

        virtual protected void MainLoopEnding()
        {
            Trace.WriteLine(string.Format(FMT_MAIN_LOOP_MSG, Name, "END"));

            // Disable QThread object processing withdraw
            IsDequeueEnabled = false;

            // Disable QThread object deposit
            IsEnqueueEnabled = false;
        }
        #endregion


        #region - Protected.Properties
        /// <summary>Containing the instance states.</summary>
        protected ThreadState       State
        {
            get { return m_thread.ThreadState; }
        }

        /// <summary>Specifies the instance scheduling priority.</summary>	
        protected ThreadPriority    Priority
        {
            get
            {
                if ((m_thread == null) || (!IsAlive))
                    return ThreadPriority.Lowest;
                return m_thread.Priority;
            }
            set
            {
                m_thread.Priority = value;
            }
        }

        /// <summary>Flag: Enable Dequeue action.</summary>
        protected bool              IsDequeueEnabled
        {
            get { return m_bIsDequeueEnabled; }
            set
            {
                if (m_bIsDequeueEnabled == value) 
                    return;

                m_bIsDequeueEnabled = value;
                if (m_bIsDequeueEnabled)
                    signalLoopWait();
            }
        }

        /// <summary>Flad: Enable Enqueue action.</summary>
        protected bool              IsEnqueueEnabled { get; set; }

        protected bool              IsSuspended
        {
            get { return isThreadStateOn(ThreadState.Suspended); }
        }


        protected bool              IsContinueMainLoop { get; private set; }


        /// <summary>Set Enqueue counter value</summary>
        /// <returns>Counter value prior to the reset.</returns>
        protected long EnqueueCount { get; private set; }

        /// <summary>Set Enqueue counter value</summary>
        /// <returns>Counter value prior to the reset.</returns>
        protected long DequeueCount { get; private set; }
        #endregion	//Protected.Properties

        #region - Protected.Events
        ///<summary>On thread execption event shell</summary>
        protected void DoOnException(Exception ex)
        {
            var tmpEvent = OnException;

			if (tmpEvent != null)
                tmpEvent(this, ex);
            else
				Trace.WriteLine(string.Format("An unhandled exception has been thrown on QThread '{0}', and there were no subscribers to the OnException event: {1}", Name, ex));
        }

        ///<summary>On loop iteration wait timeout event shell</summary>
        protected void DoOnWaitTimeout()
        {
            var tmpEvent = OnWaitTimeout;
            if (tmpEvent != null)
                tmpEvent();
        }

        /// <summary>On loop iteration timer event shell</summary>
        protected void DoOnTimer()
        {
            var tmpEvent = OnTimer;
            if (tmpEvent == null)
                TimerIsEnabled = false;
            else
                tmpEvent();

            TimerReset();
        }

        /// <summary>Evoked when iteration queue is empty.</summary>
        protected void DoOnEmpty()
        {
            var tmpEvent = OnEmpty;
            if (tmpEvent != null)
                tmpEvent();
        }

        /// <summary>On loop iteration PAUSE</summary>
        protected void DoOnPause()
        {
            var tmpEvent = OnPause;
            if (tmpEvent != null)
                tmpEvent();
        }

        /// <summary>On loop iteration pause CONTINUE</summary>
        protected void DoOnContinue()
        {
            var tmpEvent = OnContinue;
            if (tmpEvent != null)
                tmpEvent();
        }

        /// <summary>On thread main loop start event shell</summary>
        protected void DoOnStart()
        {
            var tmpEvent = OnStart;
            if (tmpEvent != null)
                tmpEvent();
        }

        /// <summary>On thread main loop end event shell</summary>
        protected void DoOnEnd()
        {
            var tmpEvent = OnEnd;
            if (tmpEvent != null)
                tmpEvent();
        }

        /// <summary>On thread queue pop event shell</summary>
        protected void DoOnLoopbackAck(LoopbackAck ack)
        {
            var tmpEvent = OnLoopbackAck;
            if (tmpEvent != null)
                tmpEvent(ack);
        }
        #endregion	//Protected.Events

        #region - Protected.Methods
        /// <summary>Retrieve first object from queue.</summary>
        /// <returns>Pulled object, null if queue empty or dequeue is desabled.</returns>
        protected bool WakeableWait(int iSleepTimeoutMSec)
        {
            var b = false;

            if (iSleepTimeoutMSec > WAIT_TIMEOUT_NONE)
                b = m_whIterationWait.WaitOne(iSleepTimeoutMSec, false);

            return b;
        }

        protected void Purge()
        {
            if (m_qIterationQueue.IsEmpty)
                return;

            m_qIterationQueue = new ConcurrentQueue<object>();
        }

        #region base class methods override
        /// <summary>Releases resources allocated by the object.</summary>
        /// <param name="isDisposing">Indicate if method invoked manualy (true) or by the .NET RT (false).</param>
        /// <remarks>
        /// Dispose(bool disposing) executes in two distinct scenarios: 
        /// 1. IsDisposing == true: Method invoked by a user's code => Managed and unmanaged resources need to be disposed.
        /// 2. IsDisposing == false: Method invoked by the runtime from inside the finalizer => 
        ///     You should not reference other objects. 
        ///     Only unmanaged resources can be disposed.
        /// </remarks>
        protected override void Dispose(bool isDisposing)
        {
            if (!isDisposing) 
                return;

            Stop();

            m_whPauseIteration.Set();
            m_whPauseIteration.Close();
            m_whPauseIteration.Dispose();

            m_whIterationWait.Set();
            m_whIterationWait.Close();
            m_whIterationWait.Dispose();

            m_whThreadExit.Set();
            m_whThreadExit.Close();
            m_whThreadExit.Dispose();
        }
        #endregion

        #endregion	//Protected.Methods
        #endregion	//Protected

        #region Private -----------------------------------------------------------------------
        #region - Private.Constants
        private const string FMT_MAIN_LOOP_MSG  = "Rimed: {0}.MainLoop: {1}.";
        private const string FMT_MODULE_METHOD = "{0}.{1}({2}): {3}";
        #endregion	//Private.Constants

        #region - Private.Fields
        /// <summary>Encapsulated Thread object</summary>
        private readonly Thread                     m_thread;

        /// <summary>Thread safe queues</summary>
        private ConcurrentQueue<object>             m_qIterationQueue       = new ConcurrentQueue<object>();

        /// <summary>Thread iteration wait object.</summary>
        private readonly AutoResetEvent             m_whIterationWait       = new AutoResetEvent(false);

        private readonly AutoResetEvent             m_whPauseIteration      = new AutoResetEvent(false);

        private readonly ManualResetEvent           m_whThreadExit          = new ManualResetEvent(false);

        private readonly Stopwatch                  m_swTimeFromLastEvent   = new Stopwatch();

        /// <summary>Flad: Enable GetNextObject action - queue object pulling.</summary>
        private bool m_bIsDequeueEnabled                        = true;

        private int m_iPauseTimeoutMSec                         = WAIT_TIMEOUT_INFINITE;

        private bool m_bTimerIsEnabled;

        private int m_iTimerIntervalMSec                        = -1;

        private static int s_instancCounter                     = 0;

        #endregion	//Private.Fields

        #region - Private.Methods
        /// <summary>ThreadStart delegate assigned to class thread member - Main thread iteration loop.</summary>
        /// <remarks>
        ///	Infinit loop until <c>End</c> method is called.
        ///	On each iteration the <c>ProcessQueueObject</c> abstruct method is called
        /// </remarks>
        private void threadStart()
        {
            try
            {
                // Reset thread exit wait handle
                m_whThreadExit.Reset();

                MainLoopStarting();

                //  Invoke OnStart event
                DoOnStart();

                //
                // Enter thread main loop 
                //
                while (IsContinueMainLoop)
                {
                    try
                    {
                        threadMainLoop();
                    }
                    catch (ThreadAbortException ex)
                    {
                        HandleException(ex);
                        Stop();
                    }
                    catch (Exception ex)
                    {
                        HandleException(ex);
                        DoOnException(ex);
                    }// try...catch
                }

                // Invoke OnEnd event
                DoOnEnd();
            }
            finally
            {
                MainLoopEnding();

                // Set QThread terminated waitable handle
                m_whThreadExit.Set();
            }
        }

        private void threadMainLoop()
        {
            while (IsContinueMainLoop)
            {
                // Check if Timer event required
                if (isIterationTimer())
                    DoOnTimer();

                // Check pause flag
                if (IsPaused)
                    iterationPause(m_iPauseTimeoutMSec);
                else
                {
                    processObject();

                    if (IsContinueMainLoop && m_qIterationQueue.IsEmpty)
                    {
                        DoOnEmpty();

                        // Iteration wait
                        iterationWait(WaitTimeout);
                    }
                }
            }// while
        }

        private bool isIterationTimer()
        {
            if (!TimerIsEnabled)
                return false;

            return (m_swTimeFromLastEvent.ElapsedMilliseconds > TimerInterval);
        }


        /// <summary>Wait for post object, post command, timer event or timeout.</summary>
        /// <returns>True: Wait Timeout, False: Signaled or Timer</returns>
        private void iterationWait(int iTimeoutMSec = WAIT_TIMEOUT_INFINITE)
        {
            if (IsDequeueEnabled || WaitTimeout != WAIT_TIMEOUT_NONE) 
                return;

            var bIsWaitTimeout = true;

            #region Calculate timer interval and select wait interval 
            // If timer enabled: calculate wait interval
            if (TimerIsEnabled)
            {
                // Calc. mSec to next Timer event
                var iTimerInterval = TimerInterval - (int)m_swTimeFromLastEvent.ElapsedMilliseconds;

                // verify min. wait interval.
                if (iTimerInterval < TIMER_MIN_INTERVAL_MSEC)
                    iTimerInterval = TIMER_MIN_INTERVAL_MSEC;

                // select smallest wait interval
                if (iTimeoutMSec > iTimerInterval || iTimeoutMSec == WAIT_TIMEOUT_INFINITE)
                {
                    iTimeoutMSec = iTimerInterval;
                    bIsWaitTimeout = false;
                } // if
            } // if (Timer enabled)
            #endregion

            // Verify no messages added
            if (m_qIterationQueue.Count > 0)
                return;

            if (iTimeoutMSec < -1)
                iTimeoutMSec = WAIT_TIMEOUT_TIMESLICE;

            // Wait for object push / timeout
            var bIsSignaled = m_whIterationWait.WaitOne(iTimeoutMSec);

            // if timeout evoke OnWaitTimeOut event
            if (bIsWaitTimeout && (!bIsSignaled))
                DoOnWaitTimeout();
        }

        private bool iterationPause(int iTimeoutMSec = WAIT_TIMEOUT_INFINITE)
        {
            if (iTimeoutMSec <= WAIT_TIMEOUT_NONE)
                return false;

            DoOnPause();

            Trace.WriteLine(string.Format(FMT_MAIN_LOOP_MSG, Name, "PAUSE"));

            bool bRet = m_whPauseIteration.WaitOne(iTimeoutMSec, false);
            resetPauseAttributes();

            Trace.WriteLine(string.Format(FMT_MAIN_LOOP_MSG, Name, "CONTINUE"));

            DoOnContinue();

            return bRet;
        }

        private void resetPauseAttributes()
        {
            IsPaused = false;
            m_iPauseTimeoutMSec = WAIT_TIMEOUT_INFINITE;
            //TODO: reset manual event
        }


        private bool postQThreadMessage(int iMsgId, object oA = null, object oB = null)
        {
            IQThreadMessage qtMsg = new QThreadMessage(iMsgId, oA, oB);
            return PostObject(qtMsg);
        }

        private bool processQThreadMessage(object obj)
        {
            var qtrdMsg = obj as IQThreadMessage;
            if (qtrdMsg == null)
                return false;

            var bRv = false;
            QThreadBase qtTrd;

            switch (qtrdMsg.Id)
            {
                case QThreadMessage.MSGID_STOP:
                    Stop();
                    bRv = true;
                    break;
                case QThreadMessage.MSGID_LOOPBACK:
                    qtTrd = qtrdMsg.ObjA as QThreadBase;
                    if (qtTrd != null)
                    {
                        qtrdMsg.Id = QThreadMessage.MSGID_LOOPBACK_ACK;
                        qtrdMsg.ObjA = this;
                        qtrdMsg.ObjB = DateTime.UtcNow;
                        qtTrd.PostObject(qtrdMsg);

                        bRv = true;
                    }
                    break;
                case QThreadMessage.MSGID_LOOPBACK_ACK:
                    qtTrd = qtrdMsg.ObjA as QThreadBase;
                    if (qtTrd != null)
                    {
                        var ack = new LoopbackAck
                        {
                            AckThread = qtTrd,
                            AckTime = (DateTime)qtrdMsg.ObjB,
                            SendTime = qtrdMsg.InitTime
                        };
                        DoOnLoopbackAck(ack);
                    }

                    bRv = true;
                    break;
                case QThreadMessage.MSGID_WAKEABLE_WAIT:
                    if (qtrdMsg.ObjA is int)
                    {
                        var iMSec = (int)qtrdMsg.ObjA;
                        m_whIterationWait.Reset();
                        iterationWait(iMSec);
                        bRv = true;
                    }
                    break;
                case QThreadMessage.MSGID_PAUSE:
                    if (qtrdMsg.ObjA is int)
                    {
                        var iMSec = (int)qtrdMsg.ObjA;
                        m_whPauseIteration.Reset();
                        iterationPause(iMSec);
                        bRv = true;
                    }
                    break;
                default:
                    break;
            }// switch

            return bRv;
        }


        private bool enqueueObject(object obj)
        {
            var b = false;

            if (IsAlive && IsEnqueueEnabled)
            {
                m_qIterationQueue.Enqueue(obj);

                if (!(obj is QThreadMessage))
                    EnqueueCount++;

                if (IsDequeueEnabled)
                    signalLoopWait();

                b = true;
            }// if 

            return b;
        }

        /// <summary>Pop object from thread queue</summary>
        /// <returns>Poped object or null if queue is empty</returns>
        private object dequeueObject()
        {
            if (!IsDequeueEnabled)
                return null;

            if (m_qIterationQueue.IsEmpty)
                return null;

            object oRet;
            if (m_qIterationQueue.TryDequeue(out oRet))
            {
                // Incriment dequeue counter
                if (!(oRet is QThreadMessage))
                    DequeueCount++;
            }
            else
                oRet = null;

            return oRet;
        }

        private bool processObject()
        {
            var obj = dequeueObject();  // Pop object from iteration queues
            
            // 
            if (obj == null)
                return false;

            var bIsHandled = processQThreadMessage(obj);
            if (bIsHandled)
                return true;

            var blkMsg = obj as BlockingMessage;
            if (blkMsg == null)
            {
                if (obj is NullMessage)
                    obj = null;

                LoopIteration(obj);
            }
            else
            {
                LoopIteration(blkMsg.Message);
                blkMsg.Processed = true;
            }

            return true;
        }


        ///<summary>Release iteration loop wait (if on wait)</summary> 
        private bool signalLoopWait()
        {
            return m_whIterationWait.Set();
        }

        /// <summary>Indicate whether the thread is in the input ThreadState or not.</summary>
        /// <param name="trdState">ThreadState value to check.</param>
        /// <returns>true: Thread is in the input ThreadState.</returns>
        private bool isThreadStateOn(ThreadState trdState)
        {
            var b = false;

            if (m_thread != null)
                b = ((m_thread.ThreadState & trdState) == trdState);

            return (b);
        }

        /// <summary>
        /// Check if curent thread is the calling thread. If true throws a QThreadException 'self join' exception
        /// </summary>
        private void verifyNoSelfJoin()
        {
            // Check self-join. 
            if (Thread.CurrentThread.ManagedThreadId == ManagedThreadId)
                throw new QThreadException(string.Format("{0}: Self Join.", Name));
        }
        #endregion	//Private.Methods

        #region - Private.Helper classes
        private class BlockingMessage
        {
            public BlockingMessage(object msg)
            {
                Message = msg;
            }

            public readonly object Message;

            public bool Processed { get; set; }
        }

        private class NullMessage
        {
        }


        #endregion

        #endregion	//Private
    }
}
