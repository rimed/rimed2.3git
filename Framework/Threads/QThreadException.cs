using System;
using Rimed.Framework.Common;

namespace Rimed.Framework.Threads
{
    public class QThreadException : RimedException
    {
        public QThreadException(string sMsg)
            : base(sMsg)
        {
        }
        public QThreadException(string sMsg, Exception eInner)
            : base(sMsg, eInner)
        {
        }
    }
}
