﻿using System;

namespace Rimed.Framework.Threads
{
    internal interface IQThreadMessage
    {
		int			Id          {get; set;}
		object		ObjA        {get; set;}
		object		ObjB        {get; set;}
		DateTime	InitTime    {get; }
    }
}
