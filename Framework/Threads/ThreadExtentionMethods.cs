using System.Threading;

namespace Rimed.Framework.Threads
{
    public static class ThreadExtentionMethods
    {
        private static long s_threadNameCounter;
        public static long SetName(this Thread thread, string name)
		{
            if (thread == null || thread.Name != null)
                return -1;

            if (string.IsNullOrWhiteSpace(name))
                name = "MngTrd";

            var id      = Interlocked.Increment(ref s_threadNameCounter);
            thread.Name = string.Format("{0:X4}.{1}", id, name);

            return id;
        }

    }
}
