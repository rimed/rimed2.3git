﻿using System;

namespace Rimed.Framework.Threads
{
    [Serializable]
    internal class QThreadMessage : IQThreadMessage
    {
        #region Public ------------------------------------------ 
		#region - Public.Constants 

        public const int	MSGID_NONE					=   0;
			
        /// <summary>Request thread main loop termination.</summary>
		public const int	MSGID_STOP					=   1;

        /// <summary> 
        /// Request thread Pause; ObjA [int]: Timeout in mSec.
        /// </summary>
        public const int    MSGID_PAUSE                 =   2;

        /// <summary> 
        /// Request thread wait; ObjA [int]: Timeout in mSec.
        /// Wait mode aborted whan object is posted to the thread queue.
        /// </summary>
		public const int	MSGID_WAKEABLE_WAIT			=   3;

        /// <summary>Request MSGID_LOOPBACK_ACK message.</summary>
        /// <remarks>ObjA: Sender ref</remarks>
        /// <remarks>ObjB: QThread, Ack. dest.</remarks>
		public const int	MSGID_LOOPBACK				= 1001;
		
        /// <summary>Respond message to MSGID_LOOPBACK message.</summary>
        /// <remarks>
        ///     ObjA: Sending thread
        ///     ObjB: Loopback_Ack send time.
        /// </remarks>
		public const int	MSGID_LOOPBACK_ACK			= 1002;

        /// <summary>App. defined MsgIds must be between QThreadMessage.MSGID_USER_MIN and QThreadMessage.MSGID_USER_MAX.</summary>
		public const int	MSGID_USER_MIN  			=    1024;	
		public const int	MSGID_USER_MAX  			= 1000000;	
		#endregion

        #region - IQThreadMessage implementation 

        public int Id           { get; set; }

        public object ObjA      { get; set; }

        public object ObjB      { get; set; }

        public DateTime InitTime
        {
            get {return m_dtInitTime;}
        }
		#endregion

        public	QThreadMessage(int iMsgId, object oA = null, object oB = null)
		{
			Id	    = iMsgId;
			ObjA	= oA;
			ObjB	= oB;
		}

        #endregion

		#region Private ----------------------------------------- 

        /// <summary>Message structure create time.</summary>
		private readonly DateTime	m_dtInitTime    = DateTime.UtcNow;

        #endregion
    }
}
