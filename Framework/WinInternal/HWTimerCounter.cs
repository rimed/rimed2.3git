//
// Source: http://msdn.microsoft.com/en-us/library/ms979201.aspx
//

using System;
using System.Runtime.InteropServices;

namespace Rimed.Framework.WinInternal
{
	/// <summary>Measure hardware time up to elapsed nanosecond (ns or nsec = 10E-9 of a second).</summary>
    public class HWTimerCounter
    {
        /// <summary>Returns the counter in number of physical clock ticks</summary>
        /// <param name="lpPerformanceCount">Variable in which to place the result</param>
        /// <returns>true is succeeded, false otherwise</returns>
		[DllImport("Kernel32.dll")]	private static extern bool QueryPerformanceCounter(out long lpPerformanceCount);

		[DllImport("Kernel32.dll")]	private static extern bool QueryPerformanceFrequency(out long lpFrequency);

		private static readonly long s_frequency = 0;

		static HWTimerCounter()
		{
			Multiplier = 0;

			IsFrequencySupported = QueryPerformanceFrequency(out s_frequency);
			if (IsFrequencySupported && s_frequency != 0)
				Multiplier = (double)(new Decimal(1.0e9)) / s_frequency;
		}

		public static long Frequency			{ get { return s_frequency; } }
		public static bool IsFrequencySupported { get; private set; }
		public static double Multiplier			{ get; private set; }

		/// <summary>Returns the raw value of the HW perf counter in ticks.</summary>
		public static long GetTimestamp()
		{
			long rawVal;
			QueryPerformanceCounter(out rawVal);

			return rawVal;
		}


		public HWTimerCounter()
		{
			Start();
		}

        private long m_start;
        private long m_stop;

        /// <summary>Save start timestamp</summary>
        public void		Start()
        {
            QueryPerformanceCounter(out m_start);
        }

		public long		GetElapsedTicks()
		{
			return GetTimestamp() - m_start; 
		}

		public double	GetElapsedNS()
		{
			return GetElapsedTicks() * Multiplier;
		}

		/// <summary>Save stop timestamp</summary>
        public void		Stop()
        {
            QueryPerformanceCounter(out m_stop);
        }

		/// <summary>Returns elapsed hardware ticks between start and stop timestamps.</summary>
        /// <returns>Elapsed HW ticks</returns>
        public long		ElapsedTicks
        {
			get { return m_stop - m_start; } 
        }

		/// <summary>Returns elapsed nanosecond start and stop timestamps.</summary>
		/// <returns>Elapsed nanosecond</returns>
		public double	ElapsedNS
		{
			get { return ElapsedTicks * Multiplier; }
		}
    }
}