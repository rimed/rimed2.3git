using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Rimed.Framework.WinInternal
{
	public class WindowsSpecialKeysWatcher: Common.Disposable
	{
		private const int WH_KEYBOARD_LL = 13;

		[StructLayout(LayoutKind.Sequential, Pack = 1)]	
		private struct KBDLLHOOKSTRUCT
		{
			//public Keys key;
			public int		vkCode;
			public int		scanCode;
			public int		flags;
			public int		time;
			public IntPtr	extra;
		}

		//System level functions to be used for hook and unhook keyboard input
		private delegate IntPtr LowLevelKeyboardProc(int nCode, IntPtr wParam, IntPtr lParam);

		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]		private static extern IntPtr SetWindowsHookEx(int id, LowLevelKeyboardProc callback, IntPtr hMod, uint dwThreadId);
		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]		private static extern bool UnhookWindowsHookEx(IntPtr hook);
		[DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]		private static extern IntPtr CallNextHookEx(IntPtr hook, int nCode, IntPtr wp, IntPtr lp);
		[DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]	private static extern IntPtr GetModuleHandle(string name);

		private static readonly object s_locker = new object();
		private static IntPtr s_ptrHook = IntPtr.Zero;
		private static bool s_isEnableKeys = false;

		// use static variable for your delegate, this will prevent the delegate from being garbage collected.
		private static readonly LowLevelKeyboardProc s_callBackDelegate = new LowLevelKeyboardProc(captureKey);

		private static IntPtr captureKey(int nCode, IntPtr wp, IntPtr lp)
		{
			//Common.Logger.LogDebug("captureKey(....), s_isEnableKeys={0})", s_isEnableKeys);

			if (nCode >= 0)
			{
				var objKeyInfo = (KBDLLHOOKSTRUCT)Marshal.PtrToStructure(lp, typeof(KBDLLHOOKSTRUCT));

				if (objKeyInfo.vkCode == (int)Keys.RWin || objKeyInfo.vkCode == (int)Keys.LWin			//WinKey
						|| (objKeyInfo.vkCode == (int)Keys.Tab && hasAltModifier(objKeyInfo.flags))		//Alt+Tab
						|| (objKeyInfo.vkCode == (int)Keys.Escape && (objKeyInfo.flags == 0))			//Ctrl+Esc
					)
					return (IntPtr)(s_isEnableKeys ? 0 : 1);	
			}

			return CallNextHookEx(s_ptrHook, nCode, wp, lp);
		}

		private static bool hasAltModifier(int flags)
		{
			return (flags & 0x20) == 0x20;
		}

		protected override void DisposeUnmanagedResources()
		{
			End();

			base.DisposeUnmanagedResources();
		}


		public bool EnableKeys(bool value)
		{
			Common.Logger.LogDebug("EnableKeys(value={0})", value);
			var prevValue = s_isEnableKeys;
			s_isEnableKeys = value;

			return prevValue;
		}


		public void Start(bool isEnableKeys = false)
		{
			s_isEnableKeys = isEnableKeys;

			lock (s_locker)
			{
				if (s_ptrHook != IntPtr.Zero)
					return;

				Common.Logger.LogDebug("Start Windows special-keys monitoring");
				try
				{
					var objCurrentModule = Process.GetCurrentProcess().MainModule;
					s_ptrHook = SetWindowsHookEx(WH_KEYBOARD_LL, s_callBackDelegate, GetModuleHandle(objCurrentModule.ModuleName), 0); //Set Hook of Keyboard Process for current module
				}
				catch (Exception ex)
				{
					Common.Logger.LogError(ex);
				}
			}
		}

		public void End()
		{
			if (s_ptrHook == IntPtr.Zero)
				return;

			try
			{
				UnhookWindowsHookEx(s_ptrHook);
				s_ptrHook = IntPtr.Zero;
			}
			catch (Exception ex)
			{
				Common.Logger.LogError(ex);
			}

			Common.Logger.LogDebug("End Windows special-keys monitoring");
		}
	}
}
