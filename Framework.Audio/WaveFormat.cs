﻿namespace Rimed.Framework.Audio
{
    /// <summary>Describes a specific wave format.</summary>
    public class WaveFormat
    {
        public WaveFormat(EWaveFormatTag formatTag, short channels, int samplesPerSecond, short bitsPerSample)
        {
            FormatTag           = formatTag;
            Channels            = channels;
            SamplesPerSecond    = samplesPerSecond;
            BitsPerSample       = bitsPerSample;
        }

	    private WaveFormat()
        {
        }

        /// <summary>Gets or sets the format of the wave samples.</summary>
        public EWaveFormatTag FormatTag { get; private set; }

        /// <summary>Gets or sets the number of channels.</summary>
        public short Channels { get; private set; }

        /// <summary>Gets or sets the sampling frequency.</summary>
        public int SamplesPerSecond { get; private set; }

        /// <summary>Gets or sets the number of bits per sample.</summary>
        public short BitsPerSample { get; private set; }

        /// <summary>Gets the smallest atomic unit of data, in bytes.</summary>
        public short BlockAlign
        {
            get
            {
                return (short)(Channels * BitsPerSample / 8);
            }
        }

        /// <summary>Gets the average number of bytes per second.</summary>
        public int AverageBytesPerSecond
        {
            get
            {
                return SamplesPerSecond * BlockAlign;
            }
        }

		public override string ToString()
		{
			return string.Format("Tag={0}, Channels={1}, SamplesPerSecond={2}, BitsPerSample={3}, BlockAlign={4}, AvgBytePerSec={5})", FormatTag, Channels, SamplesPerSecond, BitsPerSample, BlockAlign, AverageBytesPerSecond);
		}

        /// <summary>Creates a duplicate copy of this object.</summary>
        /// <returns>The requested copy</returns>
        internal WaveFormat Clone()
        {
            var clone = new WaveFormat();
            clone.BitsPerSample = BitsPerSample;
            clone.Channels = Channels;
            clone.FormatTag = FormatTag;
            clone.SamplesPerSecond = SamplesPerSecond;
            return clone;
        }


        #region Factory methods
        /// <summary>Gets the preset WaveFormat: PCM, 44.100 kHz, 16 bit, stereo</summary>
        public static WaveFormat Pcm44Khz16BitStereo
        {
            get
            {
                var wf = new WaveFormat();
                wf.FormatTag = EWaveFormatTag.PCM;
                wf.SamplesPerSecond = 44100;
                wf.BitsPerSample = 16;
                wf.Channels = 2;
                return wf;
            }
        }

        /// <summary>Gets the preset WaveFormat: PCM, 44.100 kHz, 16 bit, monaural</summary>
        public static WaveFormat Pcm44Khz16BitMono
        {
            get
            {
                var wf = new WaveFormat();
                wf.FormatTag = EWaveFormatTag.PCM;
                wf.SamplesPerSecond = 44100;
                wf.BitsPerSample = 16;
                wf.Channels = 1;
                return wf;
            }
        }

        /// <summary>Gets the preset WaveFormat: PCM, 44.100 kHz, 8 bit, stereo</summary>
        public static WaveFormat Pcm44Khz8BitStereo
        {
            get
            {
                WaveFormat wf = new WaveFormat {FormatTag = EWaveFormatTag.PCM};
                wf.SamplesPerSecond = 44100;
                wf.BitsPerSample = 8;
                wf.Channels = 2;
                return wf;
            }
        }

        /// <summary>Gets the preset WaveFormat: PCM, 44.100 kHz, 8 bit, monaural</summary>
        public static WaveFormat Pcm44Khz8BitMono
        {
            get
            {
                var wf = new WaveFormat();
                wf.FormatTag = EWaveFormatTag.PCM;
                wf.SamplesPerSecond = 44100;
                wf.BitsPerSample = 8;
                wf.Channels = 1;
                return wf;
            }
        }

        /// <summary>Gets the preset WaveFormat: PCM, 32.000 kHz, 16 bit, stereo</summary>
        public static WaveFormat Pcm32Khz16BitStereo
        {
            get
            {
                WaveFormat wf = new WaveFormat();
                wf.FormatTag = EWaveFormatTag.PCM;
                wf.SamplesPerSecond = 32000;
                wf.BitsPerSample = 16;
                wf.Channels = 2;
                return wf;
            }
        }

        /// <summary>Gets the preset WaveFormat: PCM, 32.000 kHz, 16 bit, monaural</summary>
        public static WaveFormat Pcm32Khz16BitMono
        {
            get
            {
                WaveFormat wf = new WaveFormat();
                wf.FormatTag = EWaveFormatTag.PCM;
                wf.SamplesPerSecond = 32000;
                wf.BitsPerSample = 16;
                wf.Channels = 1;
                return wf;
            }
        }

        /// <summary>Gets the preset WaveFormat: PCM, 32.000 kHz, 8 bit, stereo</summary>
        public static WaveFormat Pcm32Khz8BitStereo
        {
            get
            {
                WaveFormat wf = new WaveFormat();
                wf.FormatTag = EWaveFormatTag.PCM;
                wf.SamplesPerSecond = 32000;
                wf.BitsPerSample = 8;
                wf.Channels = 2;
                return wf;
            }
        }

        /// <summary>Gets the preset WaveFormat: PCM, 32.000 kHz, 8 bit, monaural</summary>
        public static WaveFormat Pcm32Khz8BitMono
        {
            get
            {
                WaveFormat wf = new WaveFormat();
                wf.FormatTag = EWaveFormatTag.PCM;
                wf.SamplesPerSecond = 32000;
                wf.BitsPerSample = 8;
                wf.Channels = 1;
                return wf;
            }
        }

        /// <summary>Gets the preset WaveFormat: PCM, 24.000 kHz, 16 bit, stereo</summary>
        public static WaveFormat Pcm24Khz16BitStereo
        {
            get
            {
                WaveFormat wf = new WaveFormat();
                wf.FormatTag = EWaveFormatTag.PCM;
                wf.SamplesPerSecond = 24000;
                wf.BitsPerSample = 16;
                wf.Channels = 2;
                return wf;
            }
        }

        /// <summary>Gets the preset WaveFormat: PCM, 24.000 kHz, 16 bit, monaural</summary>
        public static WaveFormat Pcm24Khz16BitMono
        {
            get
            {
                WaveFormat wf = new WaveFormat();
                wf.FormatTag = EWaveFormatTag.PCM;
                wf.SamplesPerSecond = 24000;
                wf.BitsPerSample = 16;
                wf.Channels = 1;
                return wf;
            }
        }

        /// <summary>Gets the preset WaveFormat: PCM, 24.000 kHz, 8 bit, stereo</summary>
        public static WaveFormat Pcm24Khz8BitStereo
        {
            get
            {
                WaveFormat wf = new WaveFormat();
                wf.FormatTag = EWaveFormatTag.PCM;
                wf.SamplesPerSecond = 24000;
                wf.BitsPerSample = 8;
                wf.Channels = 2;
                return wf;
            }
        }

        /// <summary>Gets the preset WaveFormat: PCM, 24.000 kHz, 8 bit, monaural</summary>
        public static WaveFormat Pcm24Khz8BitMono
        {
            get
            {
                WaveFormat wf = new WaveFormat();
                wf.FormatTag = EWaveFormatTag.PCM;
                wf.SamplesPerSecond = 24000;
                wf.BitsPerSample = 8;
                wf.Channels = 1;
                return wf;
            }
        }

        /// <summary>Gets the preset WaveFormat: PCM, 22.050 kHz, 16 bit, stereo</summary>
        public static WaveFormat Pcm22Khz16BitStereo
        {
            get
            {
                WaveFormat wf = new WaveFormat();
                wf.FormatTag = EWaveFormatTag.PCM;
                wf.SamplesPerSecond = 22050;
                wf.BitsPerSample = 16;
                wf.Channels = 2;
                return wf;
            }
        }

        /// <summary>Gets the preset WaveFormat: PCM, 22.050 kHz, 16 bit, monaural</summary>
        public static WaveFormat Pcm22Khz16BitMono
        {
            get
            {
                WaveFormat wf = new WaveFormat();
                wf.FormatTag = EWaveFormatTag.PCM;
                wf.SamplesPerSecond = 22050;
                wf.BitsPerSample = 16;
                wf.Channels = 1;
                return wf;
            }
        }

        /// <summary>Gets the preset WaveFormat: PCM, 22.050 kHz, 8 bit, stereo</summary>
        public static WaveFormat Pcm22Khz8BitStereo
        {
            get
            {
                WaveFormat wf = new WaveFormat();
                wf.FormatTag = EWaveFormatTag.PCM;
                wf.SamplesPerSecond = 22050;
                wf.BitsPerSample = 8;
                wf.Channels = 2;
                return wf;
            }
        }

        /// <summary>Gets the preset WaveFormat: PCM, 22.050 kHz, 8 bit, monaural</summary>
        public static WaveFormat Pcm22Khz8BitMono
        {
            get
            {
                WaveFormat wf = new WaveFormat();
                wf.FormatTag = EWaveFormatTag.PCM;
                wf.SamplesPerSecond = 22050;
                wf.BitsPerSample = 8;
                wf.Channels = 1;
                return wf;
            }
        }

        /// <summary>Gets the preset WaveFormat: PCM, 16.000 kHz, 16 bit, stereo</summary>
        public static WaveFormat Pcm16Khz16BitStereo
        {
            get
            {
                WaveFormat wf = new WaveFormat();
                wf.FormatTag = EWaveFormatTag.PCM;
                wf.SamplesPerSecond = 16000;
                wf.BitsPerSample = 16;
                wf.Channels = 2;
                return wf;
            }
        }

        /// <summary>Gets the preset WaveFormat: PCM, 16.000 kHz, 16 bit, monaural</summary>
        public static WaveFormat Pcm16Khz16BitMono
        {
            get
            {
                WaveFormat wf = new WaveFormat();
                wf.FormatTag = EWaveFormatTag.PCM;
                wf.SamplesPerSecond = 16000;
                wf.BitsPerSample = 16;
                wf.Channels = 1;
                return wf;
            }
        }

        /// <summary>Gets the preset WaveFormat: PCM, 16.000 kHz, 8 bit, stereo</summary>
        public static WaveFormat Pcm16Khz8BitStereo
        {
            get
            {
                WaveFormat wf = new WaveFormat();
                wf.FormatTag = EWaveFormatTag.PCM;
                wf.SamplesPerSecond = 16000;
                wf.BitsPerSample = 8;
                wf.Channels = 2;
                return wf;
            }
        }

        /// <summary>Gets the preset WaveFormat: PCM, 16.000 kHz, 8 bit, monaural</summary>
        public static WaveFormat Pcm16Khz8BitMono
        {
            get
            {
                WaveFormat wf = new WaveFormat();
                wf.FormatTag = EWaveFormatTag.PCM;
                wf.SamplesPerSecond = 16000;
                wf.BitsPerSample = 8;
                wf.Channels = 1;
                return wf;
            }
        }

        /// <summary>Gets the preset WaveFormat: PCM, 12.000 kHz, 16 bit, stereo</summary>
        public static WaveFormat Pcm12Khz16BitStereo
        {
            get
            {
                WaveFormat wf = new WaveFormat();
                wf.FormatTag = EWaveFormatTag.PCM;
                wf.SamplesPerSecond = 12000;
                wf.BitsPerSample = 16;
                wf.Channels = 2;
                return wf;
            }
        }

        /// <summary>Gets the preset WaveFormat: PCM, 12.000 kHz, 16 bit, monaural</summary>
        public static WaveFormat Pcm12Khz16BitMono
        {
            get
            {
                WaveFormat wf = new WaveFormat();
                wf.FormatTag = EWaveFormatTag.PCM;
                wf.SamplesPerSecond = 12000;
                wf.BitsPerSample = 16;
                wf.Channels = 1;
                return wf;
            }
        }

        /// <summary>Gets the preset WaveFormat: PCM, 12.000 kHz, 8 bit, stereo</summary>
        public static WaveFormat Pcm12Khz8BitStereo
        {
            get
            {
                WaveFormat wf = new WaveFormat();
                wf.FormatTag = EWaveFormatTag.PCM;
                wf.SamplesPerSecond = 12000;
                wf.BitsPerSample = 8;
                wf.Channels = 2;
                return wf;
            }
        }

        /// <summary>Gets the preset WaveFormat: PCM, 12.000 kHz, 8 bit, monaural</summary>
        public static WaveFormat Pcm12Khz8BitMono
        {
            get
            {
                WaveFormat wf = new WaveFormat();
                wf.FormatTag = EWaveFormatTag.PCM;
                wf.SamplesPerSecond = 12000;
                wf.BitsPerSample = 8;
                wf.Channels = 1;
                return wf;
            }
        }

        /// <summary>Gets the preset WaveFormat: PCM, 11.025 kHz, 16 bit, stereo</summary>
        public static WaveFormat Pcm11Khz16BitStereo
        {
            get
            {
                WaveFormat wf = new WaveFormat();
                wf.FormatTag = EWaveFormatTag.PCM;
                wf.SamplesPerSecond = 11025;
                wf.BitsPerSample = 16;
                wf.Channels = 2;
                return wf;
            }
        }

        /// <summary>Gets the preset WaveFormat: PCM, 11.025 kHz, 16 bit, monaural</summary>
        public static WaveFormat Pcm11Khz16BitMono
        {
            get
            {
                WaveFormat wf = new WaveFormat();
                wf.FormatTag = EWaveFormatTag.PCM;
                wf.SamplesPerSecond = 11025;
                wf.BitsPerSample = 16;
                wf.Channels = 1;
                return wf;
            }
        }

        /// <summary>Gets the preset WaveFormat: PCM, 11.025 kHz, 8 bit, stereo</summary>
        public static WaveFormat Pcm11Khz8BitStereo
        {
            get
            {
                WaveFormat wf = new WaveFormat();
                wf.FormatTag = EWaveFormatTag.PCM;
                wf.SamplesPerSecond = 11025;
                wf.BitsPerSample = 8;
                wf.Channels = 2;
                return wf;
            }
        }

        /// <summary>Gets the preset WaveFormat: PCM, 11.025 kHz, 8 bit, monaural</summary>
        public static WaveFormat Pcm11Khz8BitMono
        {
            get
            {
                WaveFormat wf = new WaveFormat();
                wf.FormatTag = EWaveFormatTag.PCM;
                wf.SamplesPerSecond = 11025;
                wf.BitsPerSample = 8;
                wf.Channels = 1;
                return wf;
            }
        }

        /// <summary>Gets the preset WaveFormat: PCM, 8.000 kHz, 16 bit, stereo</summary>
        public static WaveFormat Pcm8Khz16BitStereo
        {
            get
            {
                WaveFormat wf = new WaveFormat();
                wf.FormatTag = EWaveFormatTag.PCM;
                wf.SamplesPerSecond = 8000;
                wf.BitsPerSample = 16;
                wf.Channels = 2;
                return wf;
            }
        }

        /// <summary>Gets the preset WaveFormat: PCM, 8.000 kHz, 16 bit, monaural</summary>
        public static WaveFormat Pcm8Khz16BitMono
        {
            get
            {
                WaveFormat wf = new WaveFormat();
                wf.FormatTag = EWaveFormatTag.PCM;
                wf.SamplesPerSecond = 8000;
                wf.BitsPerSample = 16;
                wf.Channels = 1;
                return wf;
            }
        }

        /// <summary>Gets the preset WaveFormat: PCM, 8.000 kHz, 8 bit, stereo</summary>
        public static WaveFormat Pcm8Khz8BitStereo
        {
            get
            {
                WaveFormat wf = new WaveFormat();
                wf.FormatTag = EWaveFormatTag.PCM;
                wf.SamplesPerSecond = 8000;
                wf.BitsPerSample = 8;
                wf.Channels = 2;
                return wf;
            }
        }

        /// <summary>Gets the preset WaveFormat: PCM, 8.000 kHz, 8 bit, monaural</summary>
        public static WaveFormat Pcm8Khz8BitMono
        {
            get
            {
                WaveFormat wf = new WaveFormat();
                wf.FormatTag = EWaveFormatTag.PCM;
                wf.SamplesPerSecond = 8000;
                wf.BitsPerSample = 8;
                wf.Channels = 1;
                return wf;
            }
        }
        #endregion
    }
}
