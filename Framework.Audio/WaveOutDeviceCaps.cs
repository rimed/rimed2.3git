﻿namespace Rimed.Framework.Audio
{
    /// <summary>Enumerates the capabilities of a WaveOut device.</summary>
    public class WaveOutDeviceCaps
    {
        /// <summary>Gets or sets the system specific identifier of the device.</summary>
        public int DeviceId { get; set; }

        /// <summary>Gets or sets the device's manufacturer name.</summary>
        public string Manufacturer { get; set; }

        /// <summary>Gets or sets the device's product id.</summary>
        public int ProductId { get; set; }

        /// <summary>Gets or sets the device's driver version.</summary>
        public int DriverVersion { get; set; }

        /// <summary>Gets or sets the name of the device.</summary>
        public string Name { get; set; }

        /// <summary>Gets or sets the number of channels the device is capable of playing.</summary>
        public int Channels { get; set; }

        /// <summary>Gets a value indicating whether a device supports pitch modulation.</summary>
        public bool SupportsPitch
        {
            get
            {
                return (Capabilities & WinMM.EWaveCaps.WAVECAPS_PITCH) != 0;
            }
        }

        /// <summary>Gets a value indicating whether a device supports playback rate modification.</summary>
        public bool SupportsPlaybackRate
        {
            get
            {
                return (Capabilities & WinMM.EWaveCaps.WAVECAPS_PLAYBACKRATE) != 0;
            }
        }

        /// <summary>Gets a value indicating whether a device supports volume changing.</summary>
        public bool SupportsVolume
        {
            get
            {
                return (Capabilities & WinMM.EWaveCaps.WAVECAPS_VOLUME) != 0;
            }
        }

        /// <summary>Gets a value indicating whether a device supports stereo volume changing.</summary>
        public bool SupportsStereoVolume
        {
            get
            {
                return (Capabilities & WinMM.EWaveCaps.WAVECAPS_LRVOLUME) != 0;
            }
        }

        /// <summary>Gets or sets the capabilities of the device.</summary>
        internal WinMM.EWaveCaps Capabilities { get; set; }
    }
}
