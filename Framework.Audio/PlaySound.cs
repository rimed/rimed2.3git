﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace Rimed.Framework.Audio
{
    /// <summary>Provides a familiar interface to the functions made available by winmm.dll</summary>
    public static class PlaySound
    {
        /// <summary>Asynchronously plays a system sound.</summary>
        /// <param name="systemSoundName">The name of the system sound to play.</param>
        /// <remarks>
        /// If the specified sound cannot be found, the call is ignored.
        /// If the sound can be found in the registry, the sound buffer is purged before playing.
        /// </remarks>
        public static void PlaySystemSound(string systemSoundName)
        {
            var res = WinMM.PlaySound(systemSoundName, (IntPtr)0, WinMM.EPlaysoundflags.SND_ALIAS | WinMM.EPlaysoundflags.SND_PURGE | WinMM.EPlaysoundflags.SND_NODEFAULT | WinMM.EPlaysoundflags.SND_ASYNC);
            if (res == 0)
                throw new Win32Exception(Marshal.GetLastWin32Error());
        }

        /// <summary>Purges the sound buffer of playing sounds.</summary>
        public static void StopAllSounds()
        {
            var res = WinMM.PlaySound(null, (IntPtr)0, WinMM.EPlaysoundflags.SND_PURGE);
            if (res == 0)
                throw new Win32Exception(Marshal.GetLastWin32Error());
        }

        /// <summary>Asynchronously plays a sound specified by filename.</summary>
        /// <param name="soundFileName">The filename of the file to play.</param>
        public static void PlaySoundFile(string soundFileName)
        {
            var res = WinMM.PlaySound(soundFileName, (IntPtr)0, WinMM.EPlaysoundflags.SND_FILENAME | WinMM.EPlaysoundflags.SND_PURGE | WinMM.EPlaysoundflags.SND_NODEFAULT | WinMM.EPlaysoundflags.SND_ASYNC);
            if (res == 0)
                throw new Win32Exception(Marshal.GetLastWin32Error());
        }
    }
}
