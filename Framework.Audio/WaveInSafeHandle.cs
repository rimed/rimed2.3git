﻿using System;
using Microsoft.Win32.SafeHandles;

namespace Rimed.Framework.Audio
{
    /// <summary>Encapsulates a handle to a waveIn device.</summary>
    internal sealed class WaveInSafeHandle : SafeHandleZeroOrMinusOneIsInvalid
    {
        /// <summary>Initializes a new instance of the WaveInSafeHandle class.</summary>
        public WaveInSafeHandle()
            : base(true)
        {
        }

        /// <summary>Initializes a new instance of the WaveInSafeHandle class.</summary>
        /// <param name="tempHandle">A temporary handle from which to initialize.  The temporart handle MUST NOT be released after this instance has been created.</param>
        public WaveInSafeHandle(IntPtr tempHandle)
            : base(true)
        {
            handle = tempHandle;
        }

        /// <summary>Releases the resuorces used by this handle.</summary>
        /// <returns>true, if disposing of the handle succeeded; false, otherwise.</returns>
        protected override bool ReleaseHandle()
        {
            if (!IsClosed)
            {
                var ret = WinMM.waveInClose(this);
                return (ret == WinMM.EMMSysError.MMSYSERR_NOERROR);
            }

            return true;
        }
    }
}
