﻿namespace Rimed.Framework.Audio
{
    /// <summary>Indicates a WaveOut message.</summary>
    public enum EWaveOutMessage
    {
        /// <summary>Not Used.  Indicates that there is no message.</summary>
        NONE = 0x000,

        /// <summary>Indicates that the device has been opened.</summary>
        DEVICE_OPENED = 0x3BB,

        /// <summary>Indicates that the device has been closed.</summary>
        DEVICE_CLOSED = 0x3BC,

        /// <summary>Indicates that playback of a write operation has been completed.</summary>
        WRITE_DONE = 0x3BD
    }

    /// <summary>Indicates a WaveIn message.</summary>
    public enum EWaveInMessage
    {
        /// <summary>Not Used.  Indicates that there is no message.</summary>
        NONE = 0x000,

        /// <summary>Indicates that the device has been opened.</summary>
        DEVICE_OPENED = 0x3BE,

        /// <summary>Indicates that the device has been closed.</summary>
        DEVICE_CLOSED = 0x3BF,

        /// <summary>Indicates that playback of a write operation has been completed.</summary>
        DATA_READY = 0x3C0
    }

    /// <summary>Indicates a wave data sample format.</summary>
    public enum EWaveFormatTag
    {
        /// <summary>Indicates an invalid sample format.</summary>
        INVALID = 0x00,

        /// <summary>Indicates raw Pulse Code Modulation data.</summary>
        PCM = 0x01,

        /// <summary>Indicates Adaptive Differential Pulse Code Modulation data.</summary>
        ADPCM = 0x02,

        /// <summary>Indicates IEEE-Float data.</summary>
        FLOAT = 0x03,

        /// <summary>Indicates a-law companded data.</summary>
        A_LAW = 0x06,

        /// <summary>Indicates μ-law  companded data.</summary>
        MU_LAW = 0x07,
    }

    ///// <summary>Describes a variety of channels, frequencies, and bit-depths by which a wave signal may be expressed.</summary>
    //[Flags]
    //public enum EWaveFormats
    //{
    //    /// <summary>
    //    /// Monaural, 8-bit, 11025 Hz
    //    /// </summary>
    //    Mono8Bit11Khz = 1,

    //    /// <summary>
    //    /// Stereo, 8-bit, 11025 Hz
    //    /// </summary>
    //    Stereo8Bit11Khz = 2,

    //    /// <summary>
    //    /// Monaural, 16-bit, 11025 Hz
    //    /// </summary>
    //    Mono16Bit11Khz = 4,

    //    /// <summary>
    //    /// Stereo, 16-bit, 11025 Hz
    //    /// </summary>
    //    Stereo16Bit11Khz = 8,

    //    /// <summary>
    //    /// Monaural, 8-bit, 22050 Hz
    //    /// </summary>
    //    Mono8Bit22Khz = 16,

    //    /// <summary>
    //    /// Stereo, 8-bit, 22050 Hz
    //    /// </summary>
    //    Stereo8Bit22Khz = 32,

    //    /// <summary>
    //    /// Monaural, 16-bit, 22050 Hz
    //    /// </summary>
    //    Mono16Bit22Khz = 64,

    //    /// <summary>
    //    /// Stereo, 16-bit, 22050 Hz
    //    /// </summary>
    //    Stereo16Bit22Khz = 128,

    //    /// <summary>
    //    /// Monaural, 8-bit, 44100 Hz
    //    /// </summary>
    //    Mono8Bit44Khz = 256,

    //    /// <summary>
    //    /// Stereo, 8-bit, 44100 Hz
    //    /// </summary>
    //    Stereo8Bit44Khz = 512,

    //    /// <summary>
    //    /// Monaural, 16-bit, 44100 Hz
    //    /// </summary>
    //    Mono16Bit44Khz = 1024,

    //    /// <summary>
    //    /// Stereo, 16-bit, 44100 Hz
    //    /// </summary>
    //    Stereo16Bit44Khz = 2048,

    //    /// <summary>
    //    /// Monaural, 8-bit, 48000 Hz
    //    /// </summary>
    //    Mono8Bit48Khz = 4096,

    //    /// <summary>
    //    /// Stereo, 8-bit, 48000 Hz
    //    /// </summary>
    //    Stereo8Bit48Khz = 8192,

    //    /// <summary>
    //    /// Monaural, 16-bit, 48000 Hz
    //    /// </summary>
    //    Mono16Bit48Khz = 16384,

    //    /// <summary>
    //    /// Stereo, 16-bit, 48000 Hz
    //    /// </summary>
    //    Stereo16Bit48Khz = 32768,

    //    /// <summary>
    //    /// Monaural, 8-bit, 96000 Hz
    //    /// </summary>
    //    Mono8Bit96Khz = 65536,

    //    /// <summary>
    //    /// Stereo, 8-bit, 96000 Hz
    //    /// </summary>
    //    Stereo8Bit96Khz = 131072,

    //    /// <summary>
    //    /// Monaural, 16-bit, 96000 Hz
    //    /// </summary>
    //    Mono16Bit96Khz = 262144,

    //    /// <summary>
    //    /// Stereo, 16-bit, 96000 Hz
    //    /// </summary>
    //    Stereo16Bit96Khz = 524288,
    //}
}
