﻿namespace Rimed.Framework.Audio
{
    /// <summary>Enumerates the capabilities of a WaveIn device.</summary>
    public class WaveInDeviceCaps
    {
        /// <summary>Gets or sets the system specific identifier of the device.</summary>
        public int DeviceId { get; set; }

        /// <summary>Gets or sets the device's manufacturer name.</summary>
        public string Manufacturer { get; set; }

        /// <summary>Gets or sets the device's product id.</summary>
        public int ProductId { get; set; }

        /// <summary>Gets or sets the device's driver version.</summary>
        public int DriverVersion { get; set; }

        /// <summary>Gets or sets the name of the device.</summary>
        public string Name { get; set; }

        /// <summary>Gets or sets the number of channels the device is capable of playing.</summary>
        public int Channels { get; set; }
    }
}
