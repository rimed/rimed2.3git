﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Threading;
using System.Xml;

using Rimed.Framework.Audio.Events;
using Rimed.Framework.Common;
using Rimed.Framework.Threads;

namespace Rimed.Framework.Audio
{
    /// <summary>Encapsulates the waveOut commands in the <see cref="WinMM"/> class (from winmm.dll).  This provides a familiar format for using the WaveOut tools.</summary>
    public sealed class WaveOut : IDisposable
    {
        /// <summary>Indicates the DeviceID of the Microsoft Wave Mapper device.</summary>
        public const int WAVE_OUT_MAPPER_DEVICE_ID = -1;

	    private static readonly DateTime s_refTime = DateTime.UtcNow;

        /// <summary>Holds a list of manufactureres, read lazily from the assembly's resources.</summary>
        private static XmlDocument s_manufacturers;

        /// <summary>Holds this device's DeviceID.</summary>
        private readonly int m_deviceId;

        /// <summary>Holds the device's capabilities.</summary>
        private WaveOutDeviceCaps m_capabilities;

        /// <summary>Hold a locking object for start/stop synchronization.</summary>
        private readonly object m_startStopLock = new object();

        /// <summary>Hold a locking object for buffer synchronization.</summary>
        private readonly object m_bufferingLock = new object();

        /// <summary>Holds a flag indicating whether or not we are currently buffering.</summary>
        private bool m_buffering;

        /// <summary>Holds the number of buffers currently in the queue.</summary>
        private int m_bufferQueueCount;

        /// <summary>Holds a list of buffers to be released to the operating system.</summary>
        private readonly Queue<IntPtr> m_bufferReleaseQueue = new Queue<IntPtr>();

        /// <summary>Holds the thread used to release completed buffers and add new buffers to the queue.</summary>
        private Thread m_bufferMaintainerThread;

        /// <summary>Holds the handle to the device.</summary>
        private WaveOutSafeHandle m_deviceHandle;

        /// <summary>Holds a reference to our our own callback.</summary>
        /// <remarks>
        /// We assign this a value in the constructor, and maintain it until at lease after either
        /// Dispose or the Finalizer is called to prevent the garbage collector from finalizing
        /// the instance we pass to the <see cref="WinMM.waveOutOpen"/> method.
        /// </remarks>
        private readonly WinMM.WaveOutDelegate m_callback;

        /// <summary>Initializes a new instance of the WaveOut class, based on an available Device Id.</summary>
        /// <param name="deviceId">The device identifier to obtain.</param>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="deviceId"/> is not in the valid range.</exception>
        public WaveOut(int deviceId)
        {
            if (deviceId >= DeviceCount && deviceId != WAVE_OUT_MAPPER_DEVICE_ID)
                throw new ArgumentOutOfRangeException("deviceId", "The Device ID specified was not within the valid range.");

            m_callback  = new WinMM.WaveOutDelegate(deviceMessageCallback);
            m_deviceId  = deviceId;
        }

        /// <summary>Finalizes an instance of the WaveOut class and disposes of the native resources used by the instance.</summary>
        ~WaveOut()
        {
            dispose(false);
        }

        /// <summary>Called when the device recieves a message from the system.</summary>
        public event EventHandler<WaveOutMessageReceivedEventArgs> MessageReceived;

        /// <summary>Gets the devices offered by the system.</summary>
        public static ReadOnlyCollection<WaveOutDeviceCaps> Devices
        {
            get { return getAllDeviceCaps().AsReadOnly(); }
        }

        /// <summary>Gets this device's capabilities.</summary>
        public WaveOutDeviceCaps Capabilities
        {
            get
            {
                if (m_capabilities == null)
                    m_capabilities = getDeviceCaps(m_deviceId);

                return m_capabilities;
            }
        }

        /// <summary>Gets or sets the stereo volume of the device.</summary>
        /// <remarks>
        /// If the device does not support sereo volume, the left channel will be used for the mono volume.
        /// </remarks>
        public Volume Volume
        {
            get
            {
                uint volume = 0;
                if (IsOpened)//(m_handle != null && !m_handle.IsInvalid && !m_handle.IsClosed)
                        WinMM.CheckReturnCode(WinMM.waveOutGetVolume(m_deviceHandle, ref volume), WinMM.ERrorSource.WaveOut);
                else
                    WinMM.CheckReturnCode( WinMM.waveOutGetVolume((UIntPtr)(uint)this.m_deviceId, ref volume), WinMM.ERrorSource.WaveOut);

                var left    = volume & (uint)0xFFFF;
                var right   = volume >> 16;
                var ret     = new Volume();
                ret.Left    = (float)left / UInt16.MaxValue;
                ret.Right   = (float)right / UInt16.MaxValue;

                return ret;
            }

            set
            {
                var leftVolume  = Math.Min(Math.Max(value.Left, 0.0f), 1.0f);
                var rightVolume = Math.Min(Math.Max(value.Right, 0.0f), 1.0f);
                var left        = (uint)(UInt16.MaxValue * leftVolume);
                var right       = (uint)(UInt16.MaxValue * rightVolume);
                var volume      = left | (right << 16);

				//Logger.LogDebug("VOLUME: leftVolume={0}, rightVolume={1}, left={2:X4}, right={3:X4}, volume]{4:X8}", leftVolume, rightVolume, left, right, volume);
                if (IsOpened)//(m_handle != null && !m_handle.IsInvalid && !m_handle.IsClosed)
                    WinMM.CheckReturnCode( WinMM.waveOutSetVolume(m_deviceHandle, volume), WinMM.ERrorSource.WaveOut);
                else
                    WinMM.CheckReturnCode( WinMM.waveOutSetVolume((UIntPtr)(uint)m_deviceId, volume), WinMM.ERrorSource.WaveOut);
            }
        }

        /// <summary>Gets or sets the pitch modifier ratio.  This is not supported on all devices.</summary>
        public float Pitch
        {
            get
            {
                uint pitch = 0;
                WinMM.CheckReturnCode( WinMM.waveOutGetPitch(m_deviceHandle, ref pitch), WinMM.ERrorSource.WaveOut);

                return fixedToFloat(pitch);
            }

            set
            {
                WinMM.CheckReturnCode( WinMM.waveOutSetPitch(m_deviceHandle, floatToFixed(value)), WinMM.ERrorSource.WaveOut);
            }
        }

        /// <summary>Gets or sets the playback rate modifier ratio.  This is not supported on all devices.</summary>
        public float PlaybackRate
        {
            get
            {
                uint rate = 0;
                WinMM.CheckReturnCode( WinMM.waveOutGetPlaybackRate(m_deviceHandle, ref rate), WinMM.ERrorSource.WaveOut);

                return fixedToFloat(rate);
            }

            set
            {
                WinMM.CheckReturnCode( WinMM.waveOutSetPlaybackRate(m_deviceHandle, floatToFixed(value)), WinMM.ERrorSource.WaveOut);
            }
        }

        public bool IsOpened
        {
            get { return (m_deviceHandle != null && !m_deviceHandle.IsInvalid && !m_deviceHandle.IsClosed); }
        }

        /// <summary>Gets the number of devices available on the system.</summary>
        private static int DeviceCount
        {
            get
            {
                return (int)WinMM.waveOutGetNumDevs();
            }
        }

        /// <summary>Gets a document containing the names of all of the device manufactureres.</summary>
        private static XmlDocument Manufacturers
        {
            get
            {
                if (s_manufacturers == null)
                {
                    var doc = new XmlDocument();
                    doc.LoadXml(Properties.Resources.Devices);
                    s_manufacturers = doc;
                }

                return s_manufacturers;
            }
        }

        /// <summary>Opens the device for writing with the specified format.</summary>
        /// <param name="waveFormat">The format of the device to open.</param>
        public void Open(WaveFormat waveFormat)
        {
            lock (m_startStopLock)
            {
                if (m_deviceHandle != null)
                    throw new InvalidOperationException("The device is already open.");

                WavFormat   = waveFormat;

                var wfx     = new WinMM.WAVEFORMATEX();
                wfx.nAvgBytesPerSec = WavFormat.AverageBytesPerSecond;
                wfx.wBitsPerSample  = WavFormat.BitsPerSample;
                wfx.nBlockAlign     = WavFormat.BlockAlign;
                wfx.nChannels       = WavFormat.Channels;
                wfx.wFormatTag      = (short)(int)WavFormat.FormatTag;
                wfx.nSamplesPerSec  = WavFormat.SamplesPerSecond;
                wfx.cbSize          = 0;

                var tempHandle = new IntPtr();
                WinMM.CheckReturnCode( WinMM.waveOutOpen( ref tempHandle, (uint)m_deviceId, ref wfx, m_callback, (IntPtr)0, WinMM.EWaveOpenFlags.CALLBACK_FUNCTION | WinMM.EWaveOpenFlags.WAVE_FORMAT_DIRECT), WinMM.ERrorSource.WaveOut);
                m_deviceHandle = new WaveOutSafeHandle(tempHandle);

                lock (m_bufferingLock)
                {
                    m_buffering = true;
                    Monitor.Pulse(m_bufferingLock);
                }

                m_bufferMaintainerThread = new Thread(maintainBuffersThreadDelegate);
                m_bufferMaintainerThread.IsBackground = true;
				m_bufferMaintainerThread.Priority = ThreadPriority.BelowNormal;
                m_bufferMaintainerThread.SetName("WaveOUT.Cleanup");
                m_bufferMaintainerThread.Start();
            }
        }

        /// <summary>Closes the device.  If the device is playing, playback is stopped.</summary>
        private void close()
        {
            lock (m_startStopLock)
            {
                if (m_deviceHandle == null)
                    return;

                if (m_deviceHandle.IsClosed || m_deviceHandle.IsInvalid)
                    return;

                Stop();

                lock (m_bufferingLock)
                {
                    m_buffering = false;
                    Monitor.Pulse(m_bufferingLock);
                }

                m_bufferMaintainerThread.Join();

                m_deviceHandle.Close();
                m_deviceHandle = null;
            }
        }

	    /// <summary>Writes a block of data (in the current forma, set during Open) to the device.</summary>
	    /// <param name="bufferData">The data to send to the device.</param>
	    public void Write(byte[] bufferData)
        {
            lock (m_startStopLock)
            {
                var mem = Marshal.AllocHGlobal(bufferData.Length);
                Marshal.Copy(bufferData, 0, mem, bufferData.Length);

				var pwh				= new WinMM.WAVEHDR();
                pwh.dwBufferLength	= (uint)bufferData.Length;
                pwh.dwFlags			= 0;
                pwh.lpData			= mem;
				pwh.dwUser			= new IntPtr(58007576);
	            
                var header = Marshal.AllocHGlobal((int)WinMM.SIZE_OF_WAVEHDR);
                Marshal.StructureToPtr(pwh, header, false);

                WinMM.CheckReturnCode(WinMM.waveOutPrepareHeader(m_deviceHandle, header, WinMM.SIZE_OF_WAVEHDR), WinMM.ERrorSource.WaveOut);
                WinMM.CheckReturnCode(WinMM.waveOutWrite(m_deviceHandle, header, WinMM.SIZE_OF_WAVEHDR), WinMM.ERrorSource.WaveOut);

                lock (m_bufferingLock)
                {
                    m_bufferQueueCount++;
                    Monitor.Pulse(m_bufferingLock);
                }
            }
        }

        /// <summary>Pauses playback.  If playback is already paused, this does nothing.</summary>
        public void Pause()
        {
            lock (m_startStopLock)
            {
                WinMM.CheckReturnCode( WinMM.waveOutPause(m_deviceHandle), WinMM.ERrorSource.WaveOut);
            }
        }

        /// <summary>Resumes playback.  If playback is already in progress, this does nothing.</summary>
        public void Resume()
        {
            lock (m_startStopLock)
            {
                WinMM.CheckReturnCode( WinMM.waveOutRestart(m_deviceHandle), WinMM.ERrorSource.WaveOut);
            }
        }

        /// <summary>Stop current playback, clear pending bufferes.</summary>
        public void Stop()
        {
            lock (m_startStopLock)
            {
                WinMM.CheckReturnCode(WinMM.waveOutReset(m_deviceHandle), WinMM.ERrorSource.WaveOut);
            }
        }

        /// <summary>Determines whether or not the device supports a given format.</summary>
        /// <param name="waveFormat">The format to check.</param>
        /// <returns>true, if the format is supported; false, otherwise.</returns>
        public bool SupportsFormat(WaveFormat waveFormat)
        {
            var wfx = new WinMM.WAVEFORMATEX();
            wfx.nAvgBytesPerSec = waveFormat.AverageBytesPerSecond;
            wfx.wBitsPerSample = waveFormat.BitsPerSample;
            wfx.nBlockAlign = waveFormat.BlockAlign;
            wfx.nChannels = waveFormat.Channels;
            wfx.wFormatTag = (short)(int)waveFormat.FormatTag;
            wfx.nSamplesPerSec = waveFormat.SamplesPerSecond;
            wfx.cbSize = 0;

            var dummy = new IntPtr(0);
            var ret = WinMM.waveOutOpen( ref dummy, (uint)m_deviceId, ref wfx, null, (IntPtr)0, WinMM.EWaveOpenFlags.WAVE_FORMAT_QUERY);

            if (ret == WinMM.EMMSysError.MMSYSERR_NOERROR)
                return true;
            
            if (ret == WinMM.EMMSysError.WAVERR_BADFORMAT)
                return false;

            WinMM.CheckReturnCode(ret, WinMM.ERrorSource.WaveOut);
                
            return false;
        }

        
        public WaveFormat   WavFormat { get; private set; }

        /// <summary>Disposes of the managed and native resources used by this instance.</summary>
        public void Dispose()
        {
            dispose(true);
            GC.SuppressFinalize(this);
        }



        /// <summary>Retrieves the capabilities of a device.</summary>
        /// <param name="deviceId">The DeviceID for which to retrieve the capabilities.</param>
        /// <returns>The capabilities of the device.</returns>
        private static WaveOutDeviceCaps getDeviceCaps(int deviceId)
        {
            var wocaps = new WinMM.WAVEOUTCAPS();
            WinMM.waveOutGetDevCaps(new UIntPtr((uint)deviceId), ref wocaps, (uint)Marshal.SizeOf(wocaps.GetType()));

            var caps            = new WaveOutDeviceCaps();
            caps.DeviceId       = (int)deviceId;
            caps.Channels       = wocaps.wChannels;
            caps.DriverVersion  = (int)wocaps.vDriverVersion;
            caps.Manufacturer   = getManufacturer(wocaps.wMid);
            caps.Name           = wocaps.szPname;
            caps.ProductId      = wocaps.wPid;
            caps.Capabilities   = wocaps.dwSupport;

            return caps;
        }

        /// <summary>Retreives a manufacturer's name from the manufacturer registry resource.</summary>
        /// <param name="manufacturerId">The ManufacturerID for which to search.</param>
        /// <returns>The specified manufacturer's name.</returns>
        private static string getManufacturer(ushort manufacturerId)
        {
            var manufacturers = Manufacturers;
            XmlElement man = null;

            if (manufacturers != null)
                man = (XmlElement)manufacturers.SelectSingleNode("/devices/manufacturer[@id='" + manufacturerId.ToString(CultureInfo.InvariantCulture) + "']");
            
            if (man == null)
                return "Unknown [" + manufacturerId + "]";

            return man.GetAttribute("name");
        }

        /// <summary>Converts a floating point number to a 32-bit fixed point number.</summary>
        /// <param name="value">The floating point number to convert.</param>
        /// <returns>A 32-bit fixed point number.</returns>
        private static uint floatToFixed(float value)
        {
            var whole = (short)value;
            var fraction = (ushort)((value - whole) * ushort.MaxValue);

            return (((uint)whole) << 8) | (((uint)fraction) >> 8);
        }

        /// <summary>Converts a 32-bit fixed point number number to a floating point.</summary>
        /// <param name="value">The 32-bit fixed point number to convert.</param>
        /// <returns>A floating point number.</returns>
        private static float fixedToFloat(uint value)
        {
            var whole = (short)(value >> 8);
            var fraction = (ushort)value;

            return (float)whole + (((float)fraction) / ushort.MaxValue);
        }

        /// <summary>Retrieves a list of the capabilities of all of the devices registered on the system.</summary>
        /// <returns>A list of the capabilities of all of the devices registered on the system.</returns>
        private static List<WaveOutDeviceCaps> getAllDeviceCaps()
        {
            var devices = new List<WaveOutDeviceCaps>();
            var count = DeviceCount;
            
            for (var i = 0; i < count; i++)
            {
                devices.Add(getDeviceCaps(i));
            }

            devices.Add(getDeviceCaps(WAVE_OUT_MAPPER_DEVICE_ID));

            return devices;
        }

        /// <summary>Fires when the operating system has a message about a device.</summary>
        /// <param name="waveOutHandle">A handle to the device on which the message has been fired.</param>
        /// <param name="message">The message to be processed.</param>
        /// <param name="instance">A user instance value.</param>
        /// <param name="param1">Message parameter one.</param>
        /// <param name="param2">Message parameter two.</param>
        private void deviceMessageCallback(IntPtr waveOutHandle, WinMM.EWaveOutMessage message, IntPtr instance, IntPtr param1, IntPtr param2)
        {
            if (message == WinMM.EWaveOutMessage.WOM_DONE)
            {
                lock (m_bufferingLock)
                {
					m_bufferReleaseQueue.Enqueue(param1);
                    m_bufferQueueCount--;
                    Monitor.Pulse(m_bufferingLock);
                }
            }

            if (MessageReceived != null)
                MessageReceived(this, new WaveOutMessageReceivedEventArgs((EWaveOutMessage)message));
        }

        /// <summary>Adds buffers to the device and cleans up completed buffers.</summary>
        private void maintainBuffersThreadDelegate()
        {
            try
            {
                while (m_buffering || m_bufferQueueCount > 0 || m_bufferReleaseQueue.Count > 0)
                {
                    lock (m_bufferingLock)
                    {
                        while (m_bufferReleaseQueue.Count == 0 && (m_bufferQueueCount > 0 || m_buffering))
                        {
                            Monitor.Wait(m_bufferingLock, 250);
                        }
                    }

                    while (m_bufferReleaseQueue.Count > 0)
                    {
                        processDone();
                    }
                }
            }
            catch (ThreadAbortException)
            {
            }
        }

        /// <summary>Frees buffers that have been used by the application.</summary>
        private void processDone()
        {
            if ((m_bufferReleaseQueue.Count == 0))
                return;

            IntPtr header;

            // Pull the header data back out of unmanaged memory
            lock (m_bufferingLock)
            {
                header = m_bufferReleaseQueue.Dequeue();
                Monitor.Pulse(m_bufferingLock);
            }

            var pwh = (WinMM.WAVEHDR)Marshal.PtrToStructure(header, typeof(WinMM.WAVEHDR));
            var data = pwh.lpData;

            WinMM.CheckReturnCode(WinMM.waveOutUnprepareHeader(m_deviceHandle, header, WinMM.SIZE_OF_WAVEHDR), WinMM.ERrorSource.WaveOut);

            Marshal.FreeHGlobal(data);
            Marshal.FreeHGlobal(header);
        }

        /// <summary>Disposes of the managed and native resources used by this instance.</summary>
        /// <param name="disposing">true to dispose all resources, false to relase native resources only.</param>
        private void dispose(bool disposing)
        {
            if (disposing)
            {
                // Free managed resources.
            }

            if (m_deviceHandle != null)
            {
                close();
	            m_deviceHandle = null;
            }
        }
    }
}
