﻿using System;

namespace Rimed.Framework.Audio.Events
{
    /// <summary>Describes an event when a WaveOutMessage is recieved.</summary>
    public sealed class WaveOutMessageReceivedEventArgs : EventArgs
    {
        /// <summary>Initializes a new instance of the WaveOutMessageReceivedEventArgs class with the specified message.</summary>
        /// <param name="message">The message the new instance will describe.</param>
        public WaveOutMessageReceivedEventArgs(EWaveOutMessage message)
        {
            Message = message;
        }

        /// <summary>Gets the message recieved.</summary>
        public EWaveOutMessage Message { get; private set; }
    }
}
