﻿using System;

namespace Rimed.Framework.Audio.Events
{
    /// <summary>Describes an DataReady event.</summary>
    public class DataReadyEventArgs : EventArgs
    {
        /// <summary>Initializes a new instance of the DataReadyEventArgs class with the specified data.</summary>
        /// <param name="data">The data specific to this event.</param>
        public DataReadyEventArgs(byte[] data)
        {
            Data = data;
        }

        /// <summary>Gets the value of the data, specific to this event.</summary>
        public byte[] Data { get; private set; }
    }
}
