﻿using System;
using Microsoft.Win32.SafeHandles;

namespace Rimed.Framework.Audio
{
    /// <summary>Encapsulates a handle to a waveOut device.</summary>
    internal sealed class WaveOutSafeHandle : SafeHandleZeroOrMinusOneIsInvalid
    {
        /// <summary>Initializes a new instance of the WaveOutSafeHandle class.</summary>
        public WaveOutSafeHandle()
            : base(true)
        {
        }

        /// <summary>Initializes a new instance of the WaveOutSafeHandle class.</summary>
        /// <param name="tempHandle">A temporary handle from which to initialize.  The temporart handle MUST NOT be released after this instance has been created.</param>
        public WaveOutSafeHandle(IntPtr tempHandle)
            : base(true)
        {
            handle = tempHandle;
        }

        /// <summary>Releases the resuorces used by this handle.</summary>
        /// <returns>true, if disposing of the handle succeeded; false, otherwise.</returns>
        protected override bool ReleaseHandle()
        {
            if (!IsClosed)
            {
                var ret = WinMM.waveOutClose(this);
                return (ret == WinMM.EMMSysError.MMSYSERR_NOERROR);
            }

            return true;
        }
    }
}
