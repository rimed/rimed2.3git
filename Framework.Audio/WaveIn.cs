﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Threading;
using System.Xml;

using Rimed.Framework.Audio.Events;
using Rimed.Framework.Common;
using Rimed.Framework.Threads;

namespace Rimed.Framework.Audio
{
    /// <summary>Encapsulates the waveIn commands in the <see cref="WinMM"/> class (from winmm.dll).  This provides a familiar format for using the WaveIn tools.</summary>
    public sealed class WaveIn : IDisposable
    {
        /// <summary>Indicates the DeviceID of the Microsoft Wave Mapper device.</summary>
        public const int WAVE_IN_MAPPER_DEVICE_ID = -1;

        /// <summary>Holds a list of manufactureres, read lazily from the assembly's resources.</summary>
        private static XmlDocument s_manufacturers;

        /// <summary>Hold a locking object for start/stop synchronization.</summary>
        private readonly object m_startStopLock = new object();

        /// <summary>Hold a locking object for buffer synchronization.</summary>
        private readonly object m_bufferingLock = new object();

        /// <summary>Holds the current recording format.</summary>
        private WaveFormat m_recordingFormat;

        /// <summary>Holds a flag indicating whether or not we are currently buffering.</summary>
        private bool m_buffering;

        /// <summary>Holds the number of samples to hold in each buffer in the queue.</summary>
        private int m_bufferSize = 200;

        /// <summary>Holds the size of the buffer queue size in buffers.</summary>
        private int m_bufferQueueSize = 30;

        /// <summary>Holds the number of buffers currently in the queue.</summary>
        private int m_bufferQueueCount;

        /// <summary>Holds a list of buffers to be released to the operating system.</summary>
        private readonly Queue<IntPtr> m_bufferReleaseQueue = new Queue<IntPtr>();

        /// <summary>Holds the thread used to release completed buffers and add new buffers to the queue.</summary>
        private Thread m_bufferMaintainerThread;

        /// <summary>Holds this device's DeviceID.</summary>
        private readonly int m_deviceId;

        /// <summary>Holds the handle to the device.</summary>
        private WaveInSafeHandle m_handle;

        /// <summary>Holds a reference to our our own callback.</summary>
        /// <remarks>
        /// We assign this a value in the constructor, and maintain it until at lease after either
        /// Dispose or the Finalizer is called to prevent the garbage collector from finalizing
        /// the instance we pass to the <see cref="WinMM.waveInOpen"/> method.
        /// </remarks>
        private readonly WinMM.WaveInDelegate m_callback;

        /// <summary>Initializes a new instance of the WaveIn class, based on an available Device Id.</summary>
        /// <param name="deviceId">The device identifier to obtain.</param>
        /// <exception cref="ArgumentOutOfRangeException">If <paramref name="deviceId"/> is not in the valid range.</exception>
        public WaveIn(int deviceId)
        {
            if (deviceId >= DeviceCount && deviceId != WAVE_IN_MAPPER_DEVICE_ID)
                throw new ArgumentOutOfRangeException("deviceId", "The Device ID specified was not within the valid range.");

            m_callback  = new WinMM.WaveInDelegate(this.deviceMessageCallback);
            m_deviceId  = deviceId;
        }

        /// <summary>Finalizes an instance of the WaveIn class and disposes of the native resources used by the instance.</summary>
        ~WaveIn()
        {
            Dispose(false);
        }

        /// <summary>Called when the system returns from the device.</summary>
        public event EventHandler<DataReadyEventArgs> DataReady;

        /// <summary>Gets the devices offered by the system.</summary>
        public static ReadOnlyCollection<WaveInDeviceCaps> Devices
        {
            get { return getAllDeviceCaps().AsReadOnly(); }
        }

        /// <summary>Gets or sets the size of the internal input buffers, in samples.</summary>
        public int BufferSize
        {
            get
            {
                return m_bufferSize;
            }

            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException("value", "The value you specified is too small");

                m_bufferSize = value;
            }
        }

        /// <summary>Gets or sets the number of internal input buffers to enqueue at the device level.</summary>
        public int BufferQueueSize
        {
            get
            {
                return m_bufferQueueSize;
            }

            set
            {
                if (m_bufferQueueSize <= 0)
                    throw new ArgumentOutOfRangeException("value", "The value you specified is too small");

                m_bufferQueueSize = value;
            }
        }

        /// <summary>Gets the number of devices available on the system.</summary>
        private static int DeviceCount
        {
            get
            {
                return (int)WinMM.waveInGetNumDevs();
            }
        }

        /// <summary>Gets a document containing the names of all of the device manufactureres.</summary>
        private static XmlDocument Manufacturers
        {
            get
            {
                if (s_manufacturers == null)
                {
                    var doc = new XmlDocument();
                    doc.LoadXml(Properties.Resources.Devices);
                    s_manufacturers = doc;
                }

                return s_manufacturers;
            }
        }

        /// <summary>Opens the device for writing with the specified format.</summary>
        /// <param name="waveFormat">The format of the device to open.</param>
        public void Open(WaveFormat waveFormat)
        {
            lock (m_startStopLock)
            {
                if (m_handle != null)
                    throw new InvalidOperationException("The device is already open.");

                var wfx = new WinMM.WAVEFORMATEX();
                wfx.nAvgBytesPerSec = waveFormat.AverageBytesPerSecond;
                wfx.wBitsPerSample = waveFormat.BitsPerSample;
                wfx.nBlockAlign = waveFormat.BlockAlign;
                wfx.nChannels = waveFormat.Channels;
                wfx.wFormatTag = (short)(int)waveFormat.FormatTag;
                wfx.nSamplesPerSec = waveFormat.SamplesPerSecond;
                wfx.cbSize = 0;

                m_recordingFormat = waveFormat.Clone();

                var tempHandle = new IntPtr();
                WinMM.CheckReturnCode( WinMM.waveInOpen( ref tempHandle, (uint)m_deviceId, ref wfx, m_callback, (IntPtr)0, WinMM.EWaveOpenFlags.CALLBACK_FUNCTION | WinMM.EWaveOpenFlags.WAVE_FORMAT_DIRECT), WinMM.ERrorSource.WaveOut);
                m_handle = new WaveInSafeHandle(tempHandle);
            }
        }

        /// <summary>Closes the device.  If the device is playing, playback is stopped.</summary>
        /// <remarks>
        /// If the device is not currently open, this function does nothing.
        /// </remarks>
        public void Close()
        {
            lock (m_startStopLock)
            {
                if (m_handle == null)
                    return;

                if (!m_handle.IsClosed && !m_handle.IsInvalid)
                {
                    Stop();
                    m_handle.Close();
                }

                m_handle = null;
            }
        }

        /// <summary>Begins recording.</summary>
        public void Start()
        {
            lock (m_startStopLock)
            {
                if (m_bufferMaintainerThread != null)
                    throw new InvalidOperationException("The device has already been started.");

                lock (m_bufferingLock)
                {
                    m_buffering = true;
                    Monitor.Pulse(m_bufferingLock);
                }

                m_bufferMaintainerThread = new Thread(new ThreadStart(this.maintainBuffersThreadDelegate));
                m_bufferMaintainerThread.IsBackground = true;
                m_bufferMaintainerThread.SetName("WaveIN.Cleanup");
                m_bufferMaintainerThread.Start();

                WinMM.CheckReturnCode( WinMM.waveInStart(m_handle), WinMM.ERrorSource.WaveIn);
            }
        }

        /// <summary>Stops recording.</summary>
        /// <remarks>
        /// If the device is not currently started, this call does nothing.
        /// </remarks>
        public void Stop()
        {
            lock (m_startStopLock)
            {
                if (m_bufferMaintainerThread == null)
                    return;

                lock (m_bufferingLock)
                {
                    m_buffering = false;
                    Monitor.Pulse(m_bufferingLock);
                }

                WinMM.CheckReturnCode( WinMM.waveInReset(m_handle), WinMM.ERrorSource.WaveIn);

                m_bufferMaintainerThread.Join();
                m_bufferMaintainerThread = null;
            }
        }

        /// <summary>Determines whether or not the device supports a given format.</summary>
        /// <param name="waveFormat">The format to check.</param>
        /// <returns>true, if the format is supported; false, otherwise.</returns>
        public bool SupportsFormat(WaveFormat waveFormat)
        {
            var wfx = new WinMM.WAVEFORMATEX();
            wfx.nAvgBytesPerSec = waveFormat.AverageBytesPerSecond;
            wfx.wBitsPerSample = waveFormat.BitsPerSample;
            wfx.nBlockAlign = waveFormat.BlockAlign;
            wfx.nChannels = waveFormat.Channels;
            wfx.wFormatTag = (short)(int)waveFormat.FormatTag;
            wfx.nSamplesPerSec = waveFormat.SamplesPerSecond;
            wfx.cbSize = 0;

            var dummy = new IntPtr(0);
            var ret = WinMM.waveInOpen( ref dummy, (uint)m_deviceId, ref wfx, null, (IntPtr)0, WinMM.EWaveOpenFlags.WAVE_FORMAT_QUERY);

            if (ret == WinMM.EMMSysError.MMSYSERR_NOERROR)
                return true;

            if (ret == WinMM.EMMSysError.WAVERR_BADFORMAT)
                return false;

            WinMM.CheckReturnCode(ret, WinMM.ERrorSource.WaveIn);

            return false;
        }

        /// <summary>Disposes of the managed and native resources used by this instance.</summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>Retrieves the capabilities of a device.</summary>
        /// <param name="deviceId">The DeviceID for which to retrieve the capabilities.</param>
        /// <returns>The capabilities of the device.</returns>
        private static WaveInDeviceCaps getDeviceCaps(int deviceId)
        {
            var wicaps = new WinMM.WAVEINCAPS();
            WinMM.waveInGetDevCaps(new UIntPtr((uint)deviceId), ref wicaps, (uint)Marshal.SizeOf(wicaps.GetType()));
            var caps = new WaveInDeviceCaps();
            caps.DeviceId = (int)deviceId;
            caps.Channels = wicaps.wChannels;
            caps.DriverVersion = (int)wicaps.vDriverVersion;
            caps.Manufacturer = getManufacturer(wicaps.wMid);
            caps.Name = wicaps.szPname;
            caps.ProductId = wicaps.wPid;

            return caps;
        }

        /// <summary>Retreives a manufacturer's name from the manufacturer registry resource.</summary>
        /// <param name="manufacturerId">The ManufacturerID for which to search.</param>
        /// <returns>The specified manufacturer's name.</returns>
        private static string getManufacturer(ushort manufacturerId)
        {
            XmlDocument manufacturers = Manufacturers;
            XmlElement man = null;

            if (manufacturers != null)
                man = (XmlElement)manufacturers.SelectSingleNode("/devices/manufacturer[@id='" + manufacturerId.ToString(CultureInfo.InvariantCulture) + "']");

            if (man == null)
                return "Unknown [" + manufacturerId + "]";

            return man.GetAttribute("name");
        }

        /// <summary>Retrieves a list of the capabilities of all of the devices registered on the system.</summary>
        /// <returns>A list of the capabilities of all of the devices registered on the system.</returns>
        private static List<WaveInDeviceCaps> getAllDeviceCaps()
        {
            var devices = new List<WaveInDeviceCaps>();
            int count = DeviceCount;

            for (var i = 0; i < count; i++)
            {
                devices.Add(getDeviceCaps(i));
            }

            devices.Add(getDeviceCaps(WAVE_IN_MAPPER_DEVICE_ID));

            return devices;
        }

        /// <summary>Adds buffers to the device and cleans up completed buffers.</summary>
        private void maintainBuffersThreadDelegate()
        {
            try
            {
                while (m_buffering)
                {
                    lock (m_bufferingLock)
                    {
                        while ((m_bufferQueueCount >= m_bufferQueueSize && m_bufferReleaseQueue.Count == 0) && m_buffering)
                        {
                            Monitor.Wait(m_bufferingLock);
                        }
                    }

                    while (m_bufferQueueCount < m_bufferQueueSize && m_buffering)
                    {
                        addBuffer();
                    }

                    while (m_bufferReleaseQueue.Count > 0)
                    {
                        processDone();
                    }
                }

                while (m_bufferReleaseQueue.Count > 0 || m_bufferQueueCount > 0)
                {
                    lock (m_bufferingLock)
                    {
                        while (m_bufferReleaseQueue.Count == 0)
                        {
                            Monitor.Wait(m_bufferingLock, 1000);
                        }
                    }

                    while (m_bufferReleaseQueue.Count > 0)
                    {
                        processDone();
                    }
                }
            }
            catch (ThreadAbortException)
            {
            }
        }

        /// <summary>Adds a buffer to the queue.</summary>
        private void addBuffer()
        {
            // Allocate unmanaged memory for the buffer
            var bufferLength = m_bufferSize * m_recordingFormat.BlockAlign;
            var mem = Marshal.AllocHGlobal(bufferLength);

            // Initialize the buffer header, including a reference to the buffer memory
            var pwh = new WinMM.WAVEHDR();
            pwh.dwBufferLength = (uint)bufferLength;
            pwh.dwFlags = 0;
            pwh.lpData = mem;
            pwh.dwUser = new IntPtr(12345);

            // Copy the header into unmanaged memory
            var header = Marshal.AllocHGlobal((int)WinMM.SIZE_OF_WAVEHDR);
            //var header = Marshal.AllocHGlobal(Marshal.SizeOf(pwh));
            Marshal.StructureToPtr(pwh, header, false);
            
            // Prepare the header
            WinMM.CheckReturnCode(WinMM.waveInPrepareHeader(m_handle, header, WinMM.SIZE_OF_WAVEHDR), WinMM.ERrorSource.WaveOut);
//            WinMM.CheckReturnCode( WinMM.waveInPrepareHeader(m_handle, header, (uint)Marshal.SizeOf(typeof(WinMM.WAVEHDR))), WinMM.ERrorSource.WaveOut);

            // Add the buffer to the device
            WinMM.CheckReturnCode(WinMM.waveInAddBuffer(m_handle, header, WinMM.SIZE_OF_WAVEHDR), WinMM.ERrorSource.WaveOut);
//            WinMM.CheckReturnCode( WinMM.waveInAddBuffer(m_handle, header, (uint)Marshal.SizeOf(typeof(WinMM.WAVEHDR))), WinMM.ERrorSource.WaveOut);

            lock (m_bufferingLock)
            {
                m_bufferQueueCount++;
                Monitor.Pulse(m_bufferingLock);
            }
        }

        /// <summary>Processes data and frees buffers that have been used by the application.</summary>
        private void processDone()
        {
            IntPtr header;

            // Pull the header data back out of unmanaged memory
            lock (m_bufferingLock)
            {
                header = m_bufferReleaseQueue.Dequeue();
                Monitor.Pulse(m_bufferingLock);
            }

            var pwh = (WinMM.WAVEHDR)Marshal.PtrToStructure(header, typeof(WinMM.WAVEHDR));

            // Find and copy the buffer data
            var data = pwh.lpData;

            // Copy the data and fire the DataReady event if necessary
            if (pwh.dwBytesRecorded > 0 && DataReady != null)
            {
                var newData = new byte[pwh.dwBytesRecorded];
                Marshal.Copy(data, newData, 0, (int)pwh.dwBytesRecorded);
                DataReady(this, new DataReadyEventArgs(newData));
            }

            // Unprepare the header
            WinMM.CheckReturnCode(WinMM.waveInUnprepareHeader(m_handle, header, WinMM.SIZE_OF_WAVEHDR), WinMM.ERrorSource.WaveIn);
//            WinMM.CheckReturnCode(WinMM.waveInUnprepareHeader(m_handle, header, (uint)Marshal.SizeOf(typeof(WinMM.WAVEHDR))), WinMM.ERrorSource.WaveIn);

            // Free the unmanaged memory
            Marshal.FreeHGlobal(data);
            Marshal.FreeHGlobal(header);
        }

        /// <summary>Fires when the operating system has a message about a device.</summary>
        /// <param name="waveInHandle">A handle to the device on which the message has been fired.</param>
        /// <param name="message">The message to be processed.</param>
        /// <param name="instance">A user instance value.</param>
        /// <param name="param1">Message parameter one.</param>
        /// <param name="param2">Message parameter two.</param>
        private void deviceMessageCallback(IntPtr waveInHandle, WinMM.EWaveInMmessage message, IntPtr instance, IntPtr param1, IntPtr param2)
        {
            if (message == WinMM.EWaveInMmessage.MM_WIM_DATA)
            {
                lock (m_bufferingLock)
                {
                    m_bufferReleaseQueue.Enqueue(param1);
                    m_bufferQueueCount--;
                    Monitor.Pulse(m_bufferingLock);
                }
            }
        }

        /// <summary>Disposes of the managed and native resources used by this instance.</summary>
        /// <param name="disposing">true to dispose all resources, false to relase native resources only.</param>
        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                // Free managed resources.
            }

            if (m_handle != null)
            {
                Close();
            }
        }
    }
}
