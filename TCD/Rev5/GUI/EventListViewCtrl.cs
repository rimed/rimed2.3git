using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Timers;
using System.Windows.Forms;

using TCD2003.DB;
using TCD2003.DSP;
using TCD2003.Utils;

namespace TCD2003.GUI
{
    /// <summary>
    /// Summary description for BVListViewCtrl.
    /// The event list control manages the event functionality and UI
    /// The user can insert events during monitoring examination using this control
    /// He can also "Jump" to events using the bottom list at LODING mode
    /// </summary>
    public partial class EventListViewCtrl : UserControl
    {
        #region Private Fields

        private readonly List<SingleEvent> m_predefinedEvents = new List<SingleEvent>();
        private readonly List<SingleHistoryEvent> m_eventsHistory = new List<SingleHistoryEvent>();
        private readonly List<SingleHistoryEvent> m_addedEventsHistory = new List<SingleHistoryEvent>();
        private readonly List<SingleHistoryEvent> m_deletedEventsHistory = new List<SingleHistoryEvent>();
        private bool m_eventListSaved = true;

        private Double m_baselineVelocityPrimary = 0;
        private Double m_baselineVelocitySecondary = 0;
        private Double m_VMRPrimary = 0;
        private Double m_VMRSecondary = 0;

        private volatile Stack<SingleHistoryEvent> m_captureEvents = new Stack<SingleHistoryEvent>();
        private volatile bool m_waitBeforeCapture = false;
        private Thread m_captureEventsThread = null;
        private object m_lockCaptureEvents = new object();
        private SingleHistoryEvent m_selectedEvent = null;

        private Guid m_examIndex = Guid.Empty;

        #endregion Private Fields

        #region Public Properties

        [Browsable(false)]
        public Double VMRPrimary
        {
            get
            {
                return this.m_VMRPrimary;
            }
        }

        [Browsable(false)]
        public Double VMRSecondary
        {
            get
            {
                return this.m_VMRSecondary;
            }
        }

        [Browsable(false)]
        public SingleHistoryEvent LastStopStimulationEvent
        {
            get
            {
                for (int i = m_eventsHistory.Count - 1; i > -1; i--)
                {
                    if (m_eventsHistory[i].EventName.Equals("Stop Stimulation"))
                        return m_eventsHistory[i];
                }

                return null;
            }
        }

        public static EventListViewCtrl Instance { get; private set; }  //Ofer


        [Browsable(false)]
        public List<SingleEvent> PredefinedEvents
        {
            get
            {
                return this.m_predefinedEvents;
            }
        }

        [Browsable(false)]
        public List<SingleHistoryEvent> HistoryEvents
        {
            get
            {
                return this.m_eventsHistory;
            }
        }

        [Browsable(false)]
        public bool EventListSaved
        {
            get
            {
                return m_eventListSaved;
            }
            set
            {
                m_eventListSaved = value;
                
                if (MainForm.Instance != null)  //Ofer
                    MainForm.Instance.ToolBarPanel1.ButtonSaveCtrl.Enabled = !value;

                if (value)
                {
                    m_addedEventsHistory.Clear();
                    m_deletedEventsHistory.Clear();
                }
            }
        }

        /// <summary>
        /// Returns selected event, or null if there is no selected events 
        /// or event list is empty.
        /// </summary>
        /// <returns>Selected event</returns>
        [Browsable(false)]
        internal SingleHistoryEvent SelectedEvent
        {
            get
            {
                return this.m_selectedEvent;
            }
            set
            {
                if (value == null)
                    this.m_selectedEvent = value;
            }
        }

        [Browsable(false)]
        internal DateTime ExaminationDate { get; set; }

        #endregion Public Properties

        #region Public Methods

        internal delegate void AddHistoryDelegate(string eventName, GlobalTypes.EventType eventType, bool postEvent);
        internal void AddHistoryEvent(string eventName, GlobalTypes.EventType eventType, bool postEvent)
        {
            if (InvokeRequired)
            {
                BeginInvoke((AddHistoryDelegate)AddHistoryEvent, eventName, eventType, postEvent);
                return;
            }

            if (!MainForm.Instance.IsRecordMode && LayoutManager.SystemLayoutMode == GlobalTypes.TSystemLayoutMode.Monitoring)
            {
                MessageBox.Show(MainForm.ResourceManager.GetString("Adding events is enabled just in record mode please press the record button to use this feature"),
                    MainForm.ResourceManager.GetString("Adding Events"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            if (!MainForm.Instance.IsRecordMode && LayoutManager.SystemLayoutMode != GlobalTypes.TSystemLayoutMode.MonitoringLoad)
            {
                return;
            }

            DateTime eventTime;
            double diffTimeInSec = 0;
            double newLastLeftPositionPainted = LayoutManager.theLayoutManager.Trends[0].GetTrendGraph().LastLeftPositionPainted;
            if (LayoutManager.SystemLayoutMode != GlobalTypes.TSystemLayoutMode.MonitoringLoad)
            {
                eventTime = DateTime.Now;

                if (eventName.CompareTo("BaseLine Velocity") == 0)
                    this.SetBaselineVelocity();
                if (eventName.CompareTo("Test Velocity") == 0)
                    this.SetVMRValue();

                if (eventName.CompareTo("Start Stimulation") == 0)
                    LayoutManager.theLayoutManager.SetStartPeakValue();
                if (eventName.CompareTo("Stop Stimulation") == 0)
                    LayoutManager.theLayoutManager.SetStopPeakValue();
            }
            else
            {
                //Get new Time (from start position of Spectrum Chart screen)
                diffTimeInSec = LayoutManager.theLayoutManager.GateViews[0].Spectrum.Graph.GetDistanceToStartScreenPosition();

                //Calculate ExtraInfo3 (LastLeftPositionPainted)
                eventTime = MainForm.Instance.MonitoringReplayTimeCounter.AddSeconds(diffTimeInSec * -1);
                //newLastLeftPositionPainted *=  eventTime.Ticks / MainForm.Instance.MonitoringReplayTimeCounter.Ticks;   
            }

            double ms = eventTime.Subtract(this.ExaminationDate).TotalMilliseconds;
            long fullIndexInFile = (long)(Dsps.Instance[0].LastBufferNum - Dsps.Instance[0].RecordingStartBufferNum);
            //Calculate new IndexInFile
            long indexInFile = (long)(ms / 24);

            if (LayoutManager.SystemLayoutMode == GlobalTypes.TSystemLayoutMode.MonitoringLoad)
            {
                //check it ?
                newLastLeftPositionPainted *= fullIndexInFile != 0 ? indexInFile / fullIndexInFile : 0;
            }

            //Calculate new monitoringFileNum
            int monitoringFileNum;
            if (LayoutManager.SystemLayoutMode == GlobalTypes.TSystemLayoutMode.MonitoringLoad)
                monitoringFileNum = Convert.ToInt32(indexInFile / 500); //1000
            else
                monitoringFileNum = Dsps.Instance[0].MonitoringFileCounter;

            GateView currentGV = LayoutManager.theLayoutManager.SelectedGv;
            GlobalTypes.THitType hitType = GlobalTypes.THitType.None;
            if (eventType == GlobalTypes.EventType.HitEvent)
            {
                if (currentGV != null && currentGV.GateSide == GlobalTypes.TGateSide.eRight)
                    hitType |= GlobalTypes.THitType.RightProbe;
                if (postEvent)
                    hitType |= GlobalTypes.THitType.Manual;
            }

            SingleHistoryEvent historyEvent;
            if (MainForm.Instance.CurrentStudyName == "Evoked Flow Unilateral")
            {
                historyEvent = new SingleHistoryEvent(Guid.NewGuid(), (Guid)RimedDal.Instance.s_dsExamination.tb_Examination.Rows[0].ItemArray[0], monitoringFileNum
                , indexInFile, eventTime, false, eventType, eventName, m_VMRPrimary, m_VMRSecondary, newLastLeftPositionPainted,
                LayoutManager.theLayoutManager.Trends[0].EvokedFlowPeak, 0, true, hitType);
            }
            else if (MainForm.Instance.CurrentStudyName == "Evoked Flow Bilateral")
            {
                historyEvent = new SingleHistoryEvent(Guid.NewGuid(), (Guid)RimedDal.Instance.s_dsExamination.tb_Examination.Rows[0].ItemArray[0], monitoringFileNum
                , indexInFile, eventTime, false, eventType, eventName, m_VMRPrimary, m_VMRSecondary, newLastLeftPositionPainted,
                LayoutManager.theLayoutManager.Trends[0].EvokedFlowPeak, LayoutManager.theLayoutManager.Trends[1].EvokedFlowPeak, true, hitType);
            }
            else
            {
                historyEvent = new SingleHistoryEvent(Guid.NewGuid(), (Guid)RimedDal.Instance.s_dsExamination.tb_Examination.Rows[0].ItemArray[0], monitoringFileNum
                , indexInFile, eventTime, false, eventType, eventName, m_VMRPrimary, m_VMRSecondary, newLastLeftPositionPainted,
                0, 0, false, hitType);
            }

            InsertHistoryEvent(historyEvent);

            if (!MainForm.Instance.CurrentStudyName.StartsWith("Monitoring Intracranial"))
                return;

            // Copy clinical bar values to Summary View
            foreach (GateView gv in LayoutManager.theLayoutManager.GateViews.Where(gv => gv.IsMainSpectrum))
            {
                foreach (DB.dsGateExamination.tb_GateExaminationRow gateRow in RimedDal.Instance.s_dsGateExamination.tb_GateExamination.Rows)
                {
                    if (gateRow.Gate_Index == gv.Gate_Index)
                    {
                        SummaryScreen.Instance.UpdateSummaryScreenWithNewValues(gateRow, true);
                        break;
                    }
                }
            }

            this.m_waitBeforeCapture = (LayoutManager.SystemLayoutMode == GlobalTypes.TSystemLayoutMode.Monitoring && LayoutManager.SystemMode == GlobalTypes.TSystemModes.Online);
            if (this.m_waitBeforeCapture)
            {
                lock (this.m_lockCaptureEvents)
                    this.m_captureEvents.Push(historyEvent);

                if (this.m_captureEventsThread == null || !this.m_captureEventsThread.IsAlive)
                {
                    this.m_captureEventsThread = new Thread(this.SaveEventProcess);
                    m_captureEventsThread.IsBackground = true;
                    General.SetThreadName(m_captureEventsThread, "SaveEventProcessThread");
                    this.m_captureEventsThread.Priority = ThreadPriority.Normal;
                    this.m_captureEventsThread.Start();
                }
            }
            else
                this.CaptureEvent(historyEvent);
        }

        private void InsertHistoryEvent(SingleHistoryEvent historyEvent)
        {
            ListViewItem item = new ListViewItem();
            item.Text = historyEvent.EventTime.ToLongTimeString();
            item.Font = GlobalSettings.F14;
            item.SubItems.Add(historyEvent.EventName);
            int eventPosition = 0;
            while (eventPosition < this.m_eventsHistory.Count && this.m_eventsHistory[eventPosition].EventTime < historyEvent.EventTime)
                eventPosition++;
            this.listView2.Items.Insert(eventPosition, item);
            this.listView2.SelectedIndices.Clear();
            this.m_selectedEvent = null;
            this.m_eventsHistory.Insert(eventPosition, historyEvent);
            this.m_addedEventsHistory.Add(historyEvent);
            this.EventListSaved = false;
        }

        internal SingleHistoryEvent GetHitEventByTime(DateTime time, GlobalTypes.TGateSide gateSide)
        {
            bool hasRightSideFlag = gateSide == GlobalTypes.TGateSide.eRight;
            return m_eventsHistory.FirstOrDefault(item =>
                item.EventType == GlobalTypes.EventType.HitEvent &&
                (item.HitType.HasFlag(GlobalTypes.THitType.RightProbe) == hasRightSideFlag) &&
                Math.Abs(item.EventTime.Subtract(time).TotalMilliseconds) < 1000);
        }

        internal void LoadHistoryEvents(Guid examinationID)
        {
            dsEventExamination ds = RimedDal.Instance.GetEventHistoryByExaminationID(examinationID);
            foreach (DB.dsEventExamination.tb_ExaminationEventRow row in ds.tb_ExaminationEvent.Rows)
            {
                SingleHistoryEvent s;
                if (row["EventName"].ToString().EndsWith("Stimulation"))
                {
                    s = new SingleHistoryEvent((Guid)row["Event_Index"], (Guid)row["Examination_Index"], (System.Int16)row["FileIndex"]
                            , (System.Int32)row["IndexInFile"], (DateTime)row["EventTime"], (bool)row["Deleted"], (GlobalTypes.EventType)(System.Int16)row["EventType"], (string)row["EventName"], (System.Double)row["ExtraInfo1"], (System.Double)row["ExtraInfo2"], (System.Double)row["ExtraInfo3"]
                            , Convert.ToInt16(row["ExtraInfo4"]), Convert.ToInt16(row["ExtraInfo5"]), true, (GlobalTypes.THitType)Convert.ToInt16(row["HitType"]));
                }
                else
                {
                    s = new SingleHistoryEvent((Guid)row["Event_Index"], (Guid)row["Examination_Index"], (System.Int16)row["FileIndex"]
                        , (System.Int32)row["IndexInFile"], (DateTime)row["EventTime"], (bool)row["Deleted"], (GlobalTypes.EventType)(System.Int16)row["EventType"], (string)row["EventName"], (System.Double)row["ExtraInfo1"], (System.Double)row["ExtraInfo2"], (System.Double)row["ExtraInfo3"]
                        , 0, 0, false, (GlobalTypes.THitType)Convert.ToInt16(row["HitType"]));
                }
                this.AddHistoryEvent(s);
            }
            this.UpdateHistoryEvents();
        }

        internal void LoadPredefinedEvents()
        {
            DataView dv = RimedDal.Instance.GetEventsBySetupStudyIndex(MainForm.Instance.CurrentStudyId);
            this.ClearPredefinedEventsList();
            foreach (DataRowView row in dv)
            {
                SingleEvent s = new SingleEvent((Guid)row["Event_Index"], (string)row["Name"]);
                this.AddPredefinedEvent(s);
            }
            this.UpdatePredefinedEvents();
            lock(this.m_lockCaptureEvents)
                this.m_captureEvents.Clear();
        }

        private void SaveEventProcess()
        {
            ExceptionPublisherLog4Net.TraceLog("SaveEventProcess(): START", Name);

            while (m_captureEvents.Count > 0)
            {
                try
                {
                    SingleHistoryEvent captureEvent = null;
                    lock (this.m_lockCaptureEvents)
                    {
                        if (this.m_captureEvents.Count > 0)
                            captureEvent = m_captureEvents.Pop();
                        else
                        {
                            ExceptionPublisherLog4Net.TraceLog("SaveEventProcess(): END.1", Name);
                            return;
                        }
                    }
                    if (m_waitBeforeCapture)
                    {
                        TimeSpan timeToWait = DateTime.Now - captureEvent.EventTime;
                        double difference = MainForm.Instance.GateTimeInterval * 1000 - timeToWait.TotalMilliseconds;
                        if (difference > 0)
                            Thread.Sleep(Convert.ToInt16(difference));
                    }
                    
                    // Save screenshot
                    this.CaptureEvent(captureEvent);

                }
                catch (ThreadAbortException)
                {
                    ExceptionPublisherLog4Net.TraceLog("SaveEventProcess(): ABORT", Name);
                    break;
                }
                catch (Exception ex)
                {
                    ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
                }
            }

            ExceptionPublisherLog4Net.TraceLog("SaveEventProcess(): END", Name);
        }

        private void CaptureEvent(SingleHistoryEvent historyEvent)
        {
            var gates = LayoutManager.theLayoutManager.GateViews.ToArray(); //Ofer
            foreach (GateView gv in gates.Where(gv => gv.IsMainSpectrum))
            {
                gv.MonitoringCopyTo();
                SummaryScreen.Instance.SaveImage2HD(gv, historyEvent.Event_Index);
            }
        }

        private const int CAPTURE_EVENT_THREAD_JOIN_TIMEOUT_MSEC = 30*1000;

        internal void SaveEventHistory()
        {
            if (LayoutManager.SystemLayoutMode != GlobalTypes.TSystemLayoutMode.MonitoringLoad && LayoutManager.SystemLayoutMode != GlobalTypes.TSystemLayoutMode.Monitoring)
                return;

            if (EventListSaved)
                return;

            RimedDal.Instance.GetEventHistoryByExaminationID((Guid)RimedDal.Instance.s_dsExamination.tb_Examination.Rows[0]["Examination_Index"]);

            for (int i = 0; i < m_eventsHistory.Count; i++)
                DB.RimedDal.Instance.AddHistoryEvent2Study(m_eventsHistory[i].Event_Index, m_eventsHistory[i].Examination_Index, m_eventsHistory[i].Gate_Index,
                    m_eventsHistory[i].File_Index, m_eventsHistory[i].IndexInFile, m_eventsHistory[i].EventTime,
                    m_eventsHistory[i].Deleted, (int)m_eventsHistory[i].EventType, m_eventsHistory[i].EventName, (int)m_eventsHistory[i].HitType, m_eventsHistory[i].HitVelocity,
                    m_eventsHistory[i].HitEnergy, m_eventsHistory[i].AlarmValue, m_eventsHistory[i].VMRPrimary, m_eventsHistory[i].VMRSecondary, m_eventsHistory[i].TrendSiganlIndexAtEventTime,
                    m_eventsHistory[i].FlowChangePrimary, m_eventsHistory[i].FlowChangeSecondary, m_eventsHistory[i].EvokedFlowEvent);
        
            for (int i = 0; i < m_deletedEventsHistory.Count; i++)
                DB.RimedDal.Instance.DeleteHistoryEvent(m_deletedEventsHistory[i].Event_Index);

            RimedDal.Instance.SaveEventHistory();
            this.EventListSaved = true;

            if (!MainForm.Instance.CurrentStudyName.StartsWith("Monitoring Intracranial"))
                return;

            if (LayoutManager.SystemLayoutMode == GlobalTypes.TSystemLayoutMode.MonitoringLoad)
                RimedDal.Instance.UpdateCurExamModified();

            m_waitBeforeCapture = false;

            if (m_captureEventsThread == null || !m_captureEventsThread.IsAlive)
            {
                m_captureEventsThread = new Thread(SaveEventProcess);
                m_captureEventsThread.IsBackground = true;
                General.SetThreadName(m_captureEventsThread, "SaveEventProcess");
                m_captureEventsThread.Priority = ThreadPriority.Normal;
                m_captureEventsThread.Start();
            }


            ExceptionPublisherLog4Net.TraceLog(string.Format("SaveEventHistory().Join({0}).ENTER", CAPTURE_EVENT_THREAD_JOIN_TIMEOUT_MSEC), Name);
            var isTerminated = this.m_captureEventsThread.Join(CAPTURE_EVENT_THREAD_JOIN_TIMEOUT_MSEC);
            ExceptionPublisherLog4Net.TraceLog(string.Format("SaveEventHistory().Join({0}).LEAVE: IsTimeOut={1}", CAPTURE_EVENT_THREAD_JOIN_TIMEOUT_MSEC, !isTerminated), Name);

            while (m_captureEvents.Count > 0)
                CaptureEvent(m_captureEvents.Pop());
        }

        internal void DeleteCurEventFiles()
        {
            foreach (var addedEvent in this.m_addedEventsHistory)
            {
                RimedDal.DeleteHistoryEventFiles(addedEvent.Event_Index);
            }
            this.EventListSaved = true;
        }

        internal void RewriteVMR(DateTime? currentReplayTime)
        {
            SingleHistoryEvent historyEvent = null;
            if (currentReplayTime.HasValue)
                historyEvent = m_eventsHistory.LastOrDefault(item =>
                    item.EventType != GlobalTypes.EventType.HitEvent &&
                    item.EventName == "Test Velocity" &&
                    item.EventTime <= currentReplayTime);
            if (historyEvent == null)
            {
                m_VMRPrimary = 0.0;
                m_VMRSecondary = 0.0;
            }
            else
            {
                m_VMRPrimary = historyEvent.VMRPrimary;
                m_VMRSecondary = historyEvent.VMRSecondary;
            }
        }

        internal void ClearEventsHistoryList()
        {
            this.m_eventsHistory.Clear();
            this.m_selectedEvent = null;

            if (this.InvokeRequired)
                this.listView2.BeginInvoke((Action)delegate()
                {
                    this.listView2.Items.Clear();
                });
            else
            {
                this.listView2.Items.Clear();
            }

            this.EventListSaved = true;
        }

        internal void GetHitCountByTime(DateTime? dateTime, out int leftProbe, out int rightProbe)
        {
            leftProbe = 0;
            rightProbe = 0;
            if (dateTime.HasValue)
                foreach (var historyEvent in this.m_eventsHistory)
                {
                    if (historyEvent.EventType == GlobalTypes.EventType.HitEvent &&
                        historyEvent.EventTime <= dateTime)
                        if (historyEvent.HitType.HasFlag(GlobalTypes.THitType.RightProbe))
                            rightProbe++;
                        else
                            leftProbe++;
                }
        }

        internal void DeleteHistoryEvent()
        {
            if (this.m_selectedEvent != null)
            {
                int index = this.m_eventsHistory.IndexOf(this.m_selectedEvent);
                this.m_eventsHistory.RemoveAt(index);
                this.listView2.Items.RemoveAt(index);
                this.m_deletedEventsHistory.Add(this.m_selectedEvent);
                this.EventListSaved = false;
                this.m_selectedEvent = null;
            }
        }

        internal void DeleteHistoryEvent(SingleHistoryEvent deleteEvent)
        {
            if (deleteEvent != null)
            {
                int index = this.m_eventsHistory.IndexOf(deleteEvent);
                this.m_eventsHistory.RemoveAt(index);
                this.listView2.Items.RemoveAt(index);
                this.m_deletedEventsHistory.Add(deleteEvent);
                this.EventListSaved = false;
                this.m_selectedEvent = null;
            }
        }

        internal void LoadExamination(dsExamination.tb_ExaminationRow examRow)
        {
            this.ClearEventsHistoryList();
            if (LayoutManager.SystemLayoutMode == GlobalTypes.TSystemLayoutMode.Monitoring ||
                LayoutManager.SystemLayoutMode == GlobalTypes.TSystemLayoutMode.MonitoringLoad)
            {
                this.ExaminationDate = examRow.Date;
                this.LoadPredefinedEvents();
                this.LoadHistoryEvents(examRow.Examination_Index);
                this.m_examIndex = examRow.Examination_Index;
            }
        }

        #endregion Public Methods

        /// <summary>Constructor</summary>
        public EventListViewCtrl()
        {
            this.InitializeComponent();
            var strRes = ResourceManagerWrapper.GetWrapper(MainForm.ResourceManager); 
            this.EventName.Text = strRes.GetString("Event");
            this.EventTime.Text = strRes.GetString("Time");
            this.EventType.Text = strRes.GetString("Event");

            Instance = this;
        }

        #region Private Methods

        private void ClearPredefinedEventsList()
        {
            this.m_predefinedEvents.Clear();
            if (this.InvokeRequired)
                this.listView1.BeginInvoke((Action)delegate()
                {
                    this.listView1.Items.Clear();
                });
            else
            {
                this.listView1.Items.Clear();
            }
        }

        /// <summary>
        /// This function is called when the user load new study.
        /// </summary>
        private void AddPredefinedEvent(SingleEvent se)
        {
            this.m_predefinedEvents.Add(se);
        }

        /// <summary>
        /// This function fill listview1 control with the BVs items. 
        /// </summary>
        private void UpdatePredefinedEvents()
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((Action)UpdatePredefinedEvents);
                return;
            }

            this.listView1.Items.Clear();
            foreach (SingleEvent se in this.m_predefinedEvents)
            {
                ListViewItem item = new ListViewItem();
                item.Text = se.Name;
                item.Font = GlobalSettings.F21;
                item.SubItems.Add(se.Index.ToString());
                this.listView1.Items.Add(item);
            }
        }

        private void AddHistoryEvent(SingleHistoryEvent se)
        {
            this.m_eventsHistory.Add(se);
            if (se.EventName.Equals("Test Velocity"))
            {
                m_VMRPrimary = se.VMRPrimary;
                m_VMRSecondary = se.VMRSecondary;
            }
        }

        private void UpdateHistoryEvents()
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((Action)UpdateHistoryEvents);
                return;
            }

            this.listView2.Items.Clear();
            foreach (SingleHistoryEvent se in this.m_eventsHistory)
            {
                ListViewItem item = new ListViewItem();
                item.Text = se.EventTime.ToLongTimeString();
                item.Font = GlobalSettings.F14;
                item.SubItems.Add(se.EventName);
                this.listView2.Items.Add(item);
            }
        }

        private void SetBaselineVelocity()
        {
            //Peak -> Mean on customer's request
            if (LayoutManager.theLayoutManager.GateViews[0].Spectrum.clinicalBar1.ClinicalParamLabelMeanUpper > 0)
                this.m_baselineVelocityPrimary = LayoutManager.theLayoutManager.GateViews[0].Spectrum.clinicalBar1.ClinicalParamLabelMeanUpper;
            else
                this.m_baselineVelocityPrimary = LayoutManager.theLayoutManager.GateViews[0].Spectrum.clinicalBar1.ClinicalParamLabelMeanLower;
            if (LayoutManager.theLayoutManager.StudyType != GlobalTypes.TStudyType.eUnilateral)
            {
                if (LayoutManager.theLayoutManager.GateViews[0].Spectrum.clinicalBar1.ClinicalParamLabelMeanUpper > 0)
                    this.m_baselineVelocitySecondary = LayoutManager.theLayoutManager.GateViews[1].Spectrum.clinicalBar1.ClinicalParamLabelMeanUpper;
                else
                    this.m_baselineVelocitySecondary = LayoutManager.theLayoutManager.GateViews[1].Spectrum.clinicalBar1.ClinicalParamLabelMeanLower;
            }
        }

        private void SetVMRValue()
        {
            //RIMD-439: Change VMR formula
            if (LayoutManager.theLayoutManager.GateViews[0].Spectrum.clinicalBar1.ClinicalParamLabelMeanUpper > 0)
                this.m_VMRPrimary = (int)System.Math.Round((LayoutManager.theLayoutManager.GateViews[0].Spectrum.clinicalBar1.ClinicalParamLabelMeanUpper - m_baselineVelocityPrimary) / m_baselineVelocityPrimary * 100);
            else if (m_baselineVelocityPrimary != 0)
                this.m_VMRPrimary = (int)System.Math.Round((System.Math.Abs(LayoutManager.theLayoutManager.GateViews[0].Spectrum.clinicalBar1.ClinicalParamLabelMeanLower) - System.Math.Abs(m_baselineVelocityPrimary)) / System.Math.Abs(m_baselineVelocityPrimary) * 100);
            else
                this.m_VMRPrimary = 0;
            LayoutManager.theLayoutManager.GateViews[0].Spectrum.clinicalBar1.ClinicalParamLabelSWLower = m_VMRPrimary;
            LayoutManager.theLayoutManager.GateViews[0].Spectrum.clinicalBar1.ClinicalParamLabelSWUpper = m_VMRPrimary;

            if (LayoutManager.theLayoutManager.StudyType != GlobalTypes.TStudyType.eUnilateral)
            {
                if (LayoutManager.theLayoutManager.GateViews[0].Spectrum.clinicalBar1.ClinicalParamLabelMeanUpper > 0)
                    this.m_VMRSecondary = (int)System.Math.Round((LayoutManager.theLayoutManager.GateViews[1].Spectrum.clinicalBar1.ClinicalParamLabelMeanUpper - m_baselineVelocitySecondary) / m_baselineVelocitySecondary * 100);
                else if (m_baselineVelocitySecondary != 0)
                    this.m_VMRSecondary = (int)System.Math.Round((System.Math.Abs(LayoutManager.theLayoutManager.GateViews[1].Spectrum.clinicalBar1.ClinicalParamLabelMeanLower) - System.Math.Abs(m_baselineVelocitySecondary)) / System.Math.Abs(m_baselineVelocitySecondary) * 100);
                else
                    this.m_VMRSecondary = 0;
                LayoutManager.theLayoutManager.GateViews[1].Spectrum.clinicalBar1.ClinicalParamLabelSWLower = m_VMRSecondary;
                LayoutManager.theLayoutManager.GateViews[1].Spectrum.clinicalBar1.ClinicalParamLabelSWUpper = m_VMRSecondary;
            }
        }

        private long IndexInFileToStartDrawing(long indexInFile, GlobalTypes.TStudyType studyType)
        {
            if (studyType == GlobalTypes.TStudyType.eBilateral)
            {
                if (indexInFile % 500 < 45)
                {
                    LayoutManager.theLayoutManager.xLocationForEventLine = (int)indexInFile % 500;
                    return (indexInFile - indexInFile % 500);
                }
                else
                {
                    LayoutManager.theLayoutManager.xLocationForEventLine = (int)indexInFile % 500;
                    return indexInFile - 45;
                }
            }
            else
            {
                if (indexInFile % 500 < 116)
                {
                    LayoutManager.theLayoutManager.xLocationForEventLine = (int)indexInFile % 500;
                    return (indexInFile - indexInFile % 500);
                }
                else
                {
                    LayoutManager.theLayoutManager.xLocationForEventLine = (int)indexInFile % 500;
                    return indexInFile - 116;
                }
            }
        }

        private void MoveToCurrentHistoryEvent(int fileIndex, long indexInFile, DateTime eventTime, double lastLeftPositionPainted)
        {
            int numberOfColumnToDraw;

            long indexInFileToStartDrawing = this.IndexInFileToStartDrawing(indexInFile, LayoutManager.theLayoutManager.StudyType);
            LayoutManager.theLayoutManager.Jump2FileInMonitoringLoadExamination(fileIndex, eventTime);
            LayoutManager.theLayoutManager.IsNewDataFromEventHistory = true;
            System.Threading.Thread.Sleep(1000);
            MainForm.Instance.ToolBarPanel1.ButtonPlayCtrl.Enabled = false;

            if (LayoutManager.theLayoutManager.StudyType == GlobalTypes.TStudyType.eBilateral)
                numberOfColumnToDraw = 90;
            else
                numberOfColumnToDraw = 232;
            Dsps.Instance[0].LastBufferNum = Convert.ToInt32(indexInFileToStartDrawing);
            Dsps.Instance[0].EventBufferStartPoint = Convert.ToInt32(indexInFileToStartDrawing);
            for (int i = 0; i < numberOfColumnToDraw; i++)
                Dsps.Instance[0].DspInterruptHandlerFromEventHistory();

            Dsps.Instance[0].LastBufferNum = Convert.ToInt32(indexInFileToStartDrawing);
            Dsps.Instance[0].EventBufferStartPoint = 0;

            //Assaff : need to rewrite the next line for fine tuning of thr events
            //Dsps.Instance[0].LastReplayBufferNum=(int)lIndexInFileToStratDrawing;
            foreach (AutoScanChart AutoScanC in LayoutManager.theLayoutManager.Autoscans)
            {
                AutoScanC.Graph.UpdateDepthLine();
            }

            foreach (TrendChart tc in LayoutManager.theLayoutManager.Trends)
            {
                tc.GetTrendGraph().LastLeftPositionPainted = lastLeftPositionPainted;
                tc.GetTrendGraph().CopyFromFullResolutionToCurrentResolution();
            }
            if (this.m_selectedEvent.VMRPrimary > 0)
            {
                LayoutManager.theLayoutManager.GateViews[0].Spectrum.clinicalBar1.ClinicalParamLabelSWLower = this.m_selectedEvent.VMRPrimary;
                LayoutManager.theLayoutManager.GateViews[0].Spectrum.clinicalBar1.ClinicalParamLabelSWUpper = this.m_selectedEvent.VMRPrimary;
            }
            if (m_eventsHistory[listView2.SelectedItems[0].Index].VMRSecondary > 0)
            {
                LayoutManager.theLayoutManager.GateViews[1].Spectrum.clinicalBar1.ClinicalParamLabelSWLower = this.m_selectedEvent.VMRSecondary;
                LayoutManager.theLayoutManager.GateViews[1].Spectrum.clinicalBar1.ClinicalParamLabelSWUpper = this.m_selectedEvent.VMRSecondary;
            }
            LayoutManager.theLayoutManager.IsNewDataFromEventHistory = false;
            //Natalie: check for Pause button's pushed position was added
            //In the other case the application stops responding
            //when the pause button is pushed and we push the play button also
            if (!LayoutManager.theLayoutManager.IsPaused)
                MainForm.Instance.ToolBarPanel1.ButtonPlayCtrl.Enabled = true;
        }

        private void MoveToCurrentHistoryEvent()
        {
            MoveToCurrentHistoryEvent(this.m_selectedEvent.File_Index, this.m_selectedEvent.IndexInFile,
                this.m_selectedEvent.EventTime, this.m_selectedEvent.TrendSiganlIndexAtEventTime);
        }

        private void MoveToCurrentEventWithPlayInterval()
        {
            foreach (GateView item in LayoutManager.theLayoutManager.GateViews)
            {
                item.Spectrum.Graph.ResetLastHitEvent();
            }

            //get data of selected event
            int fileIndex = this.m_selectedEvent.File_Index;
            long indexInFile = this.m_selectedEvent.IndexInFile;
            DateTime eventTime = this.m_selectedEvent.EventTime;

            //read event replay duration from the registry
            Microsoft.Win32.RegistryKey myKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Rimed", true);
            uint startPlayEventTime = 2;
            if (myKey != null)
            {
                if (myKey.GetValue("StartPlayEvent") != null)
                {
                    startPlayEventTime = Convert.ToUInt16(myKey.GetValue("StartPlayEvent"));
                    if (startPlayEventTime > 0)
                    {
                        //calculate new eventTime
                        eventTime = eventTime.AddSeconds(-1 * startPlayEventTime);
                        if (eventTime < this.ExaminationDate)
                            eventTime = this.ExaminationDate;

                        double ms = eventTime.Subtract(this.ExaminationDate).TotalMilliseconds;
                        indexInFile = (long)(ms / 24);

                        //calculate new fileIndex (check situation with 700, for example)
                        fileIndex = Convert.ToInt32(indexInFile / 500);
                    }
                }
            }

            //calculate new LastLeftPositionPainted 
            double lastLeftPositionPainted = this.m_selectedEvent.TrendSiganlIndexAtEventTime;
            if (startPlayEventTime > 0)
                lastLeftPositionPainted = indexInFile != 0 ? lastLeftPositionPainted * indexInFile / this.m_selectedEvent.IndexInFile : 0;

            MoveToCurrentHistoryEvent(fileIndex, indexInFile, eventTime, lastLeftPositionPainted);
        }

        #endregion Private Methods

        #region Event Handlers

        private void listView1_SizeChanged(object sender, EventArgs e)
        {
            int widthCtrl = this.listView1.Width;
            this.listView1.Columns[0].Width = (int)(widthCtrl / 3) * 2 - 1;
        }

        private void listView2_SizeChanged(object sender, EventArgs e)
        {
            int widthCtrl = this.listView1.Width;
            this.listView2.Columns[0].Width = (int)(widthCtrl / 2);
            this.listView2.Columns[1].Width = (int)(widthCtrl / 2);
        }

        private void EventListViewCtrl_SizeChanged(object sender, EventArgs e)
        {
            if (MainForm.Instance == null)
                return;

            if (this.Width < 50)
                this.Width = 50;

            MainForm.Instance.ResizePanelsSize();

            int hight = this.listView1.Parent.Height;
            this.listView1.Height = hight / 2;
            this.listView2.Height = hight / 2;

            int widthCtrl = this.listView1.Width;
            this.listView1.Columns[0].Width = widthCtrl;
            this.listView2.Columns[0].Width = (int)(widthCtrl / 2);
            this.listView2.Columns[1].Width = (int)(widthCtrl / 2);
        }

        private void listView2_DoubleClick(object sender, EventArgs e)
        {
            if (listView2.SelectedIndices.Count == 0 ||
                this.m_eventsHistory.Count == 0 ||
                listView2.SelectedIndices[0] >= this.m_eventsHistory.Count)
                return;

            this.m_selectedEvent = this.m_eventsHistory[listView2.SelectedIndices[0]];

            //RIMD-552: Trend window is not updated (event jumping works incorrect)
            LayoutManager.theLayoutManager.CursorsMode = false;

            if (LayoutManager.SystemMode != GlobalTypes.TSystemModes.Online)
                if (MainForm.Instance.CurrentStudyName.StartsWith("Monitoring Intracranial"))
                    this.MoveToCurrentEventWithPlayInterval();
                else
                    this.MoveToCurrentHistoryEvent();
        }

        private void listView1_MouseUp(object sender, MouseEventArgs e)
        {
            if (!MainForm.Instance.IsRecordMode)
                return;

            if (!(Dsps.Instance[0].LastBufferNum == -1) && this.listView1.SelectedItems.Count > 0)
                this.AddHistoryEvent(this.listView1.SelectedItems[0].Text, GlobalTypes.EventType.PreDefinedEvent, false);
        }

        #endregion Event Handlers
    }
}
