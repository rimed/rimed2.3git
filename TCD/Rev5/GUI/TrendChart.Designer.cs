namespace TCD2003.GUI
{
    partial class TrendChart
    {
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(TrendChart));
            this.contextMenuTrend = new System.Windows.Forms.ContextMenu();
            this.menuItemChangeTimeDisplay = new System.Windows.Forms.MenuItem();
            this.menuItem4Min = new System.Windows.Forms.MenuItem();
            this.menuItem30Min = new System.Windows.Forms.MenuItem();
            this.menuItem60Min = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.menuItem16 = new System.Windows.Forms.MenuItem();
            this.menuItemClose = new System.Windows.Forms.MenuItem();
            this.trendGraph1 = new TCD2003.GUI.TrendGraph();
            this.spectrumYAxis1 = new TCD2003.GUI.SpectrumYAxis();
            this.trendMCLabel = new System.Windows.Forms.Label();
            this.verticalTextCtrl1 = new TCD2003.GUI.OrientedTextLabel();
            this.CaptionLabel = new System.Windows.Forms.Label();
            this.trendChannelsList1 = new TCD2003.GUI.TrendChannelsList();
            this.pictureBoxTriangle = new System.Windows.Forms.PictureBox();
            this.SuspendLayout();
            // 
            // contextMenuTrend
            // 
            this.contextMenuTrend.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																							 this.menuItemChangeTimeDisplay,
																							 this.menuItem4,
																							 this.menuItemClose});
            // 
            // menuItemChangeTimeDisplay
            // 
            this.menuItemChangeTimeDisplay.Index = 0;
            this.menuItemChangeTimeDisplay.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																									  this.menuItem4Min,
																									  this.menuItem30Min,
																									  this.menuItem60Min});
            // 
            // menuItem4Min
            // 
            this.menuItem4Min.Checked = true;
            this.menuItem4Min.RadioCheck = true;
            this.menuItem4Min.Index = 0;
            this.menuItem4Min.RadioCheck = true;
            this.menuItem4Min.Click += new System.EventHandler(this.menuItemTimeDisplay_Click);
            // 
            // menuItem30Sec
            // 
            this.menuItem30Min.Index = 1;
            this.menuItem30Min.Click += new System.EventHandler(this.menuItemTimeDisplay_Click);
            // 
            // menuItem60Sec
            // 
            this.menuItem60Min.Index = 2;
            this.menuItem60Min.RadioCheck = true;
            this.menuItem60Min.Click += new System.EventHandler(this.menuItemTimeDisplay_Click);
            // 
            // menuItem4
            // 
            this.menuItem4.Index = 1;
            this.menuItem4.Text = "-";
            // 
            // menuItemClose
            // 
            this.menuItemClose.Index = 2;
            this.menuItemClose.Click += new System.EventHandler(this.menuItemClose_Click);
            // 
            // trendGraph1
            // 
            this.trendGraph1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.trendGraph1.ContextMenu = this.contextMenuTrend;
            this.trendGraph1.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.trendGraph1.LastLeftPositionPainted = 0;
            this.trendGraph1.Location = new System.Drawing.Point(72, 40);
            this.trendGraph1.Name = "trendGraph1";
            this.trendGraph1.Size = new System.Drawing.Size(600, 336);
            this.trendGraph1.TabIndex = 1;
            this.trendGraph1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // spectrumYAxis1
            // 
            this.spectrumYAxis1.BackColor = System.Drawing.SystemColors.Control;
            this.spectrumYAxis1.ContextMenu = this.contextMenuTrend;
            this.spectrumYAxis1.Dock = System.Windows.Forms.DockStyle.Left;
            this.spectrumYAxis1.Location = new System.Drawing.Point(0, 24);
            this.spectrumYAxis1.MaxValue = 250;
            this.spectrumYAxis1.MinValue = 20;
            this.spectrumYAxis1.Name = "spectrumYAxis1";
            this.spectrumYAxis1.Size = new System.Drawing.Size(40, 360);
            this.spectrumYAxis1.TabIndex = 2;
            // 
            // trendMCLabel
            // 
            this.trendMCLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((System.Byte)(177)));
            this.trendMCLabel.Location = new System.Drawing.Point(330, 3);
            this.trendMCLabel.Name = "trendMCLabel";
            this.trendMCLabel.Size = new System.Drawing.Size(130, 23);
            this.trendMCLabel.TabIndex = 19;
            this.trendMCLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // verticalTextCtrl1
            // 
            this.verticalTextCtrl1.RotationAngle = 270F;
            this.verticalTextCtrl1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(170)), ((System.Byte)(190)), ((System.Byte)(230)));
            this.verticalTextCtrl1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
            this.verticalTextCtrl1.ForeColor = System.Drawing.Color.Red;
            this.verticalTextCtrl1.Location = new System.Drawing.Point(4, 136);
            this.verticalTextCtrl1.Name = "verticalTextCtrl1";
            this.verticalTextCtrl1.Size = new System.Drawing.Size(16, 56);
            this.verticalTextCtrl1.TabIndex = 4;
            this.verticalTextCtrl1.Text = "";
            this.verticalTextCtrl1.Visible = false;
            // 
            // CaptionLabel
            // 
            this.CaptionLabel.ContextMenu = this.contextMenuTrend;
            this.CaptionLabel.Dock = System.Windows.Forms.DockStyle.Top;
            this.CaptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.CaptionLabel.Location = new System.Drawing.Point(0, 0);
            this.CaptionLabel.Name = "CaptionLabel";
            this.CaptionLabel.Size = new System.Drawing.Size(784, 24);
            this.CaptionLabel.TabIndex = 5;
            this.CaptionLabel.Text = "label1";
            this.CaptionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // trendChannelsList1
            // 
            this.trendChannelsList1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.trendChannelsList1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.trendChannelsList1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((System.Byte)(177)));
            this.trendChannelsList1.ItemHeight = 50;
            this.trendChannelsList1.Location = new System.Drawing.Point(672, 40);
            this.trendChannelsList1.Name = "trendChannelsList1";
            this.trendChannelsList1.Size = new System.Drawing.Size(112, 304);
            this.trendChannelsList1.TabIndex = 6;
            // 
            // pictureBoxTriangle
            // 
            this.pictureBoxTriangle.BackColor = System.Drawing.Color.Black;
            this.pictureBoxTriangle.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxTriangle.Image")));
            this.pictureBoxTriangle.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxTriangle.Name = "pictureBoxTriangle";
            this.pictureBoxTriangle.Size = new System.Drawing.Size(16, 16);
            this.pictureBoxTriangle.TabIndex = 7;
            this.pictureBoxTriangle.TabStop = false;
            // 
            // TrendChart
            // 
            this.ContextMenu = this.contextMenuTrend;
            this.Controls.Add(this.trendMCLabel);
            this.Controls.Add(this.pictureBoxTriangle);
            this.Controls.Add(this.trendGraph1);
            this.Controls.Add(this.trendChannelsList1);
            this.Controls.Add(this.verticalTextCtrl1);
            this.Controls.Add(this.spectrumYAxis1);
            this.Controls.Add(this.CaptionLabel);
            this.Name = "TrendChart";
            this.Size = new System.Drawing.Size(784, 384);
            this.SizeChanged += new System.EventHandler(this.TrendChart_SizeChanged);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenu contextMenuTrend;
        private System.Windows.Forms.MenuItem menuItem16;
        private System.Windows.Forms.Label CaptionLabel;
        private System.ComponentModel.Container components = null;
        private TCD2003.GUI.TrendGraph trendGraph1;
        private TCD2003.GUI.SpectrumYAxis spectrumYAxis1;
        private TCD2003.GUI.OrientedTextLabel verticalTextCtrl1;
        private TCD2003.GUI.TrendChannelsList trendChannelsList1;
        private System.Windows.Forms.MenuItem menuItem4;
        private System.Windows.Forms.MenuItem menuItemChangeTimeDisplay;
        private System.Windows.Forms.MenuItem menuItemClose;
        private System.Windows.Forms.MenuItem menuItem4Min;
        private System.Windows.Forms.MenuItem menuItem30Min;
        private System.Windows.Forms.MenuItem menuItem60Min;
        private System.Windows.Forms.PictureBox pictureBoxTriangle;
        private System.Windows.Forms.Label trendMCLabel;
    }
}