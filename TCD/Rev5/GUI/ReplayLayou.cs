using System;
using System.Windows.Forms;
using System.Collections;
using System.Drawing;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using Microsoft.ApplicationBlocks.ExceptionManagement;
using log4net;
using mPrest.Utils;
using TCD2003.DB;


using TCD2003.DSP;

namespace TCD2003.GUI
{
	/// <summary>
	/// ReplayLayout - SingleTone implementation.
	/// </summary>
	public sealed class ReplayLayout
	{
		private static ILog log = LogManager.GetLogger(typeof(ReplayLayout));
		
		private MainForm m_MainForm;
		private ReplayGate m_ReplayForm;
		private GateView m_GateView; // must be initial.
		private SummaryView m_SummaryView;

		/// <summary>
		/// private constructor for Singleton implementation.
		/// </summary>
		public ReplayLayout()
		{
		}

//		static private readonly ReplayLayout m_ReplayLayoutInstance = new ReplayLayout();
//		
//		static public ReplayLayout theReplayLayout
//		{ 
//			get { return m_ReplayLayoutInstance; } 
//		}

		public void Init(ReplayGate replayGate)
		{
			m_ReplayForm = replayGate;
			m_MainForm = MainForm.Instance;
//			m_MainForm.SystemModeChangedEvent += new SystemModeChangedDelegate(this.ModeChanged);

			for ( int i = 0 ; i < Dsps.Instance.m_NumOfDsps; i++ )
			{
				Dsps.Instance[ i ].DspNewData  += new TCD2003.DSP.Dsp.DspNewDataDelegate(OnNewDataFromDsp);
				Dsps.Instance[ i ].DspEndPlayback  += new TCD2003.DSP.Dsp.DspEndPlaybackDelegate(OnDspEndPlayback);
			}
		}

		#region Child controls' event handlers
		/// <summary>
		/// 
		/// </summary>
		/// <param name="sender"></param>
//		private void ChartVisibilityChangedHandler(object sender, GateView.ChartVisibilityEventArgs e)
//		{
//			GateView gv = sender as GateView;
//			if ( gv == null )
//				return;
//			if ( ( e.PrevState == GateView.ChartVisibilityState.Spectrum ) &&
//				( e.CurState == GateView.ChartVisibilityState.SpectrumAutoscan ) )
//			{
//				// Adding an autoscan to the sender gv
//				// Mark probe as Autoscan = true
//				gv.probe.AutoscanGv = gv;
//				// Show the autoscan on the selected gv
//				if ( m_GateView != null )
//				{
//					m_GateView.chartVisibilityState = GateView.ChartVisibilityState.SpectrumAutoscan;
//					m_SelectedGv.chartVisibilityState = GateView.ChartVisibilityState.Spectrum;
//				}
//				else
//					m_SelectedGv.chartVisibilityState = GateView.ChartVisibilityState.SpectrumAutoscan;
//			}
//			else if ( ( e.PrevState == GateView.ChartVisibilityState.SpectrumAutoscan ) &&
//				( e.CurState == GateView.ChartVisibilityState.Spectrum ) )
//			{
//				// removing an Autoscan from the sender gv
//				// Mark probe as Autoscan = false
//				gv.probe.AutoscanGv = null;
//				// stop showing the autoscan (always on the current)
//				if ( m_GateView != null )
//				{
//					m_GateView.chartVisibilityState = GateView.ChartVisibilityState.Spectrum;
//					m_SelectedGv.chartVisibilityState = GateView.ChartVisibilityState.Spectrum;
//				}
//				else
//					m_SelectedGv.chartVisibilityState = GateView.ChartVisibilityState.Spectrum;
//			}
//
//			RecalcLayout();
//		}

		#endregion Child controls' event handlers



		public SummaryView SV
		{
			get { return m_SummaryView; }
			set { m_SummaryView = value; }
		}
		public void OnNewSpectrum( SummaryView sv )
		{
			Probe probe = sv.probe;
			int depth = int.Parse(sv.DepthVal.Text);
			int Id = sv.IndexInProbe;



			// !!! make some order in this function calling to DSP function, some code and again calling to DSP function.

			// Create a new gate view
			m_GateView = new GateView( probe );
			m_GateView.dopplerBar.DepthVar = depth;

//			SetSelectGv(m_GateView);
//			{
				Dsps.Instance[m_GateView.DspId].WriteGate2Hear2Dsp(m_GateView.probe.DspId * 8 + m_GateView.Id, true);
				m_GateView.Selected = true;
//			}
//			AddEventsToGV( m_GateView );

			RegisterClickEventHandlerRecursive( m_GateView.Controls, 
				new EventHandler( m_GateView.GateView_Click ) );
			

			// Make the new gv the sleected gv (also recalculates layout)
//			RecalcLayout();
			
			// Write the new spectrum parameters to the dsp
			// TODO: we need an index for each probe. i.e., probe 2MHz PW is always
			// DSP-A[0]. How can we set this?
			// Currently we always use 0.
			int ProbeIndex = 0;
			// TODO: We need to assign an id in the probe to each gv 
			int GvIndexInProbe = m_GateView.Id;

			Dsps.Instance[ m_GateView.DspId ].WriteDepth2Dsp( ProbeIndex, GvIndexInProbe, depth, true );
		}

		// %%% check it out again
		private void RemoveEventsFromGV (GateView gv)
		{
//			m_MainForm.SystemModeChangedEvent -= new SystemModeChangedDelegate(gv.Spectrum.Graph.ModeChnged);
//			m_MainForm.SystemModeChangedEvent -= new SystemModeChangedDelegate(gv.Spectrum.TimeDomain.ModeChanged);
		}

		private void AddEventsToGV( GateView gv )
		{
//			m_MainForm.SystemModeChangedEvent += new SystemModeChangedDelegate(gv.Spectrum.Graph.ModeChnged);
//			m_MainForm.SystemModeChangedEvent += new SystemModeChangedDelegate(gv.Spectrum.TimeDomain.ModeChanged);

			MainForm.Instance.VolumeChangedEvent += new MainForm.VolumeChangedHandler(gv.VolumeChangedHandler);

		}

		private void RegisterClickEventHandlerRecursive( 
			Control.ControlCollection Controls, EventHandler ClickHandler )
		{
			foreach ( Control c in Controls )
			{
				if ( c is ComboBox )
					continue;
				c.Click += new EventHandler( ClickHandler );
				RegisterClickEventHandlerRecursive( c.Controls, ClickHandler );
			}
		}

		public void RecalcLayout()
		{
			m_ReplayForm.SuspendLayout();

			PanelRecalcLayout( m_GateView );

			// Now the gv needs to recalculate internal layout
			m_GateView.CalculateLayout();
			m_ReplayForm.ResumeLayout();
		}

		// Recalculate the layout of Gateviews in the main panel. I leav this function as it is for future usage,
		// when Rimed will want that the Replay from will replay more then one spectrum.
		private void PanelRecalcLayout( GateView gv )
		{
			m_ReplayForm.Panel.SuspendLayout();

			System.Windows.Forms.Control.ControlCollection Controls = m_ReplayForm.Panel.Controls;

			int	Height = m_ReplayForm.Panel.Height;

//			MergeChartsIntoControls( gv, Controls, Height );
//			{
			m_ReplayForm.Panel.Controls.Add(m_GateView);
			m_GateView.Dock = DockStyle.Fill;
//			}

			m_GateView.dopplerBar.m_HeaderPanel = null;
			m_GateView.ChartMode = GlobalTypes.TSizeMode.Large;
			m_GateView.Height = Height;
			// Set speed factor for each gv
			int [] Speeds = m_ReplayForm.ToolBarPanel1.ComboBoxTime.Tag as int[];
			if ( Speeds != null )
					m_GateView.SpeedFactor = Speeds[ m_ReplayForm.ToolBarPanel1.ComboBoxTime.SelectedIndex ];

			m_ReplayForm.Panel.ResumeLayout();
		}


		#region Unuse code
//		private void MergeChartsIntoControls( GateView gv, 
//			Control.ControlCollection Controls, int Height )
//		{
//			int LastChartInControls = 0;
//
//			// check if this is the Chart we want to add from Charts
//			//GateView gv = Gvs[ i ] as GateView;
//			UserControl Chart = gv as UserControl;
//
//			// add Chart to displayed controls
//			Chart.Dock = DockStyle.Top;
//			Chart.Height = Height;
//			Controls.Add( Chart );
//				
//			Controls.SetChildIndex( Chart, LastChartInControls);
//		}
		#endregion
		public GateView gateView
		{
			get { return m_GateView; }
		}

		DateTime m_LastPaintTime = DateTime.MinValue;
		static int m_nLoopNumber = 0;
		static int [] m_TimeFromLastPaint = new int[100];
		bool bBenchmark = false;

		public void OnDspEndPlayback(Object state)
		{
			if ( m_ReplayForm.InvokeRequired )
			{
				m_ReplayForm.BeginInvoke( 
					new DSP.Dsp.DspEndPlaybackDelegate( OnDspEndPlayback ),
					new object [] { state } );
				return;
			}
			MainForm.SystemMode = GlobalTypes.TSystemModes.ReplayOffline;
		}

		//static private System.Threading.Timer RefreshTimer ;
		//static private System.Windows.Forms.Timer RefreshTimer;
		//		public void OnRefreshTimer(Object state,EventArgs e)
		/// <summary>
		/// 
		/// </summary>
		/// <param name="state">The Dsp with new data</param>
		public void ModeChanged()
		{
			// if it's the system is not in replay mode -> do nothings.
			if(MainForm.SystemMode != GlobalTypes.TSystemModes.ReplayOnline &&
				MainForm.SystemMode != GlobalTypes.TSystemModes.ReplayOffline)
				return;


			// change the "global variable" in the main Form that hold the system mode.
			if (MainForm.SystemMode == GlobalTypes.TSystemModes.ReplayOnline)
			{
				MainForm.SystemMode = GlobalTypes.TSystemModes.ReplayOffline;
			}
			else
			{
				MainForm.SystemMode = GlobalTypes.TSystemModes.ReplayOnline;
				CursorsMode = false;
			}
			gateView.Spectrum.Graph.ModeChnged();
			gateView.Spectrum.TimeDomain.ModeChanged();

//			// Set the correct toolbar
//			m_ReplayForm.ToolBarPanel1.ShowToolbar( GlobalTypes.TooolBarType.TB6_Replay);


			for ( int i = 0; i < Dsps.Instance.m_NumOfDsps; i++ )
			{
				if (MainForm.SystemMode == GlobalTypes.TSystemModes.ReplayOnline)
					Dsps.Instance[ i ].WriteSysMode2Dsp( (ushort)GlobalTypes.TSystemModes.ReplayOnline, true );
				else if (MainForm.SystemMode == GlobalTypes.TSystemModes.ReplayOffline)
					Dsps.Instance[ i ].WriteSysMode2Dsp( (ushort)GlobalTypes.TSystemModes.ReplayOffline, true );
			}
		}
		
		// Called whenever the time base of the spectrum is changed due to
		// ChangeSize event.
		// c is the control that has cahnged the time base (the calling control)
		// NewTimeBase_s - then ew time base in seconds
		public void SpectrumChangeTimeBase( Control c, double NewTimeBase_s )
		{
			// We need to find the panel that hosts the gate view
			// Note: if the spectrum is in a summary screen, we might not
			// have such a panel.
			bool Cont = true;
			Control parent = c;
			ComboBox RelatedCb = null;
			while ( Cont )
			{
				parent = parent.Parent;
				RelatedCb = m_ReplayForm.GetTimeComboboxForPanel();
				if ( RelatedCb != null )
					Cont = false;
				else
				{
					if ( parent == null )
						Cont = false;
				}
			}
			if ( RelatedCb == null )
				return; 

			// We have the combo - now change the values
			int [] Speeds = RelatedCb.Tag as int[] ;
			if ( Speeds == null )
				return;
			
			// We make sure the combo has all the values (even if before
			// they were removed because they were over 30 secs)
			for( int i = RelatedCb.Items.Count; i < Speeds.Length; i++ )
				RelatedCb.Items.Add( Speeds[ i ] );

			int CurrentSpeed = Speeds[ RelatedCb.SelectedIndex ];
			for( int i = Speeds.Length - 1; i >= 0 ; i-- )
			{	
				double CalcSpeed = NewTimeBase_s / CurrentSpeed * Speeds[ i ];
				if ( CalcSpeed <= Dsp.DSP2PC_BUFFER_LEN_SEC)
					RelatedCb.Items[ i ] = Math.Round(CalcSpeed);		//chenged to make the number in the combo look nice
				else
				{
					RelatedCb.Items.RemoveAt( i );
				}
			}
			// If we removed the selected item (because now it is too
			// big), set the selection to the biggest entry
			if ( RelatedCb.SelectedIndex == -1 )
				RelatedCb.SelectedIndex = RelatedCb.Items.Count-1;
		}

		private bool m_TimeDomainMode;
		public bool TimeDomainMode
		{
			get {return m_TimeDomainMode;}
			set 
			{
				m_TimeDomainMode = value;
				m_GateView.TimeDomainMode = m_TimeDomainMode;
		
				if (m_GateView !=null)
					m_GateView.TimeDomainMode=m_TimeDomainMode;
				RecalcLayout();
			}
		}


		private bool m_CursorsMode = false;

		public bool CursorsMode
		{
			set 
			{
				m_CursorsMode = value;

				m_GateView.DrawCursors(m_CursorsMode);

				if (m_GateView !=null)
				{
					m_GateView.DrawCursors(m_CursorsMode);
					if (value)
						m_GateView.InitCursorsTime();
				}
				else if (value)
					m_GateView.InitCursorsTime();
			}
			get {return m_CursorsMode;}
		}
		
		private void SetSVFromRam()
		{


		}
		public bool ReplaySV()
		{
//			//check if there is a valid row in the SV
//			if (m_SummaryView.Row == null && m_SummaryView.FileName==null)
//			{
//				SetSVFromRam();
//
////				System.Resources.ResourceManager strRes = new System.Resources.ResourceManager(				
////					GetType().Namespace + ".Resource1" , GetType().Assembly);
////
////				MessageBox.Show(strRes.GetString("Can not replay unloaded examination"),
////					strRes.GetString("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error );
////				return false;
//			}
//
//			string FileName;
//			 if (m_SummaryView.Row != null)
//				FileName=m_SummaryView.Row.RawDataPath;
//			else
//				FileName=m_SummaryView.FileName;
//			//check that the gate can be replied
//			if (!System.IO.File.Exists(FileName))
//			{
//				System.Resources.ResourceManager strRes = new System.Resources.ResourceManager(				
//					GetType().Namespace + ".Resource1" , GetType().Assembly);
//
//				MessageBox.Show(strRes.GetString("File not found"),
//					strRes.GetString("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error );
//				return false;
//			}
//			if(m_GateView != null)
//				m_GateView.Spectrum.Graph.ResetGraph();
//			OnNewSpectrum(m_SummaryView);
//			Dsps.Instance[0].LoadFromFile(FileName);

			return true;
		}

		public void OnNewDataFromDsp(Object state)
		{

			// Marshal to GUI thread context
			if ( m_ReplayForm.InvokeRequired )
			{
				m_ReplayForm.BeginInvoke( 
					new DSP.Dsp.DspNewDataDelegate( OnNewDataFromDsp ),
					new object [] { state } );
				return;
			}

			if (bBenchmark)
			{
				if(m_LastPaintTime != DateTime.MinValue)
				{
					if(m_nLoopNumber++ > 0)
					{					
						double elapsed = (DateTime.Now - m_LastPaintTime).TotalMilliseconds;
						elapsed = Math.Min(elapsed,m_TimeFromLastPaint.Length - 1);
					
						m_TimeFromLastPaint[(int)Math.Round(elapsed / 10)]++;					

						if( (m_nLoopNumber % (1000)) == 0)
						{
							System.IO.StreamWriter tw = System.IO.File.CreateText("TimerVal"+m_nLoopNumber.ToString()+".txt");
							for(int i = 0; i < m_TimeFromLastPaint.Length; i++)
							{
								tw.WriteLine("{0} {1}",i,m_TimeFromLastPaint[i]);
							}
					
							tw.Close();
						}					
					}
				}

				m_LastPaintTime = DateTime.Now;			
			}



			//TickDelegate td = new TickDelegate(((GateView )m_LayoutManagerInstance.GateViews[i]).TimerTick);
			//td.BeginInvoke(null,null);
			if ( Dsps.Instance[ gateView.DspId ] == state )
				gateView.TimerTick();
	

			if ( (gateView != null) && ( Dsps.Instance[ gateView.DspId ] == state ) )
				gateView.TimerTick();


			// HITS
			//			m_MainForm.HitsHistogram.TimerTick();

		}

		public void DisplayBmp()
		{
//			// get the Bitmap from the database.
//			Byte[] byteData =  new Byte[0];
//			byteData = (Byte[])(m_SummaryView.Row.Picture);
//			MemoryStream ms = new MemoryStream(byteData);
//			Deserialize(ms);
		}
		public void Deserialize (Stream ms)
		{
			SpectrumYAxis tmp1 = new SpectrumYAxis();
			tmp1.Deserialize(ms);
			SummaryClinicalBar tmp2 = new SummaryClinicalBar();
			tmp2.Deserialize(ms);

			BinaryFormatter formatter = new BinaryFormatter();
			Image img = (Image)formatter.Deserialize(ms);
			m_GateView.DisplayBmp(img);
		}
	}
}

