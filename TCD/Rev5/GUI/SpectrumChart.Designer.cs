namespace TCD2003.GUI
{
    partial class SpectrumChart
    {
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            this.spectrumBackground.Dispose();
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SpectrumChart));
            this.gradientPanelCM = new Syncfusion.Windows.Forms.Tools.GradientPanel();
            this.bUnits = new System.Windows.Forms.Button();
            this.probImage = new System.Windows.Forms.Label();
            this.spectrumYAxis1 = new TCD2003.GUI.SpectrumYAxis();
            this.imageListProbLarge = new System.Windows.Forms.ImageList(this.components);
            this.imageListProbSmall = new System.Windows.Forms.ImageList(this.components);
            this.BackGroundPanel = new Syncfusion.Windows.Forms.Tools.GradientPanel();
            this.pSpectrumCursors = new System.Windows.Forms.Panel();
            this.lbDelta = new System.Windows.Forms.Label();
            this.lbt2 = new System.Windows.Forms.Label();
            this.lbt1 = new System.Windows.Forms.Label();
            this.spectrumBackground = new System.Windows.Forms.PictureBox();
            this.SpectrumGradient = new System.Windows.Forms.PictureBox();
            this.spectrumGraph1 = new TCD2003.GUI.SpectrumGraph();
            this.clinicalBar1 = new TCD2003.GUI.ClinicalBar();
            this.gradientPanelCM.SuspendLayout();
            this.BackGroundPanel.SuspendLayout();
            this.pSpectrumCursors.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spectrumBackground)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpectrumGradient)).BeginInit();
            this.SuspendLayout();
            // 
            // gradientPanelCM
            // 
            this.gradientPanelCM.BackColor = System.Drawing.Color.Transparent;
            this.gradientPanelCM.Border3DStyle = System.Windows.Forms.Border3DStyle.Flat;
            this.gradientPanelCM.BorderColor = System.Drawing.Color.Black;
            this.gradientPanelCM.BorderSides = System.Windows.Forms.Border3DSide.Middle;
            this.gradientPanelCM.BorderSingle = System.Windows.Forms.ButtonBorderStyle.None;
            this.gradientPanelCM.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gradientPanelCM.Controls.Add(this.bUnits);
            this.gradientPanelCM.Controls.Add(this.probImage);
            this.gradientPanelCM.Controls.Add(this.spectrumYAxis1);
            this.gradientPanelCM.Dock = System.Windows.Forms.DockStyle.Left;
            this.gradientPanelCM.Location = new System.Drawing.Point(0, 0);
            this.gradientPanelCM.Name = "gradientPanelCM";
            this.gradientPanelCM.Size = new System.Drawing.Size(50, 432);
            this.gradientPanelCM.TabIndex = 2;
            // 
            // bUnits
            // 
            this.bUnits.BackColor = System.Drawing.Color.Transparent;
            this.bUnits.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.bUnits.Image = ((System.Drawing.Image)(resources.GetObject("bUnits.Image")));
            this.bUnits.Location = new System.Drawing.Point(0, 343);
            this.bUnits.Name = "bUnits";
            this.bUnits.Size = new System.Drawing.Size(44, 25);
            this.bUnits.TabIndex = 0;
            this.bUnits.Text = "CM/S";
            this.bUnits.UseVisualStyleBackColor = false;
            this.bUnits.Click += new System.EventHandler(this.bUnits_Click);
            // 
            // probImage
            // 
            this.probImage.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.probImage.Location = new System.Drawing.Point(6, 190);
            this.probImage.Name = "probImage";
            this.probImage.Size = new System.Drawing.Size(16, 69);
            this.probImage.TabIndex = 1;
            this.probImage.MouseUp += new System.Windows.Forms.MouseEventHandler(this.probImage_MouseUp);
            // 
            // spectrumYAxis1
            // 
            this.spectrumYAxis1.BackColor = System.Drawing.SystemColors.Control;
            this.spectrumYAxis1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.spectrumYAxis1.Location = new System.Drawing.Point(0, 0);
            this.spectrumYAxis1.MaxValue = 32768D;
            this.spectrumYAxis1.MinValue = -32768D;
            this.spectrumYAxis1.Name = "spectrumYAxis1";
            this.spectrumYAxis1.Size = new System.Drawing.Size(30, 240);
            this.spectrumYAxis1.TabIndex = 0;
            // 
            // imageListProbLarge
            // 
            this.imageListProbLarge.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListProbLarge.ImageStream")));
            this.imageListProbLarge.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListProbLarge.Images.SetKeyName(0, "");
            this.imageListProbLarge.Images.SetKeyName(1, "");
            this.imageListProbLarge.Images.SetKeyName(2, "");
            this.imageListProbLarge.Images.SetKeyName(3, "");
            // 
            // imageListProbSmall
            // 
            this.imageListProbSmall.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListProbSmall.ImageStream")));
            this.imageListProbSmall.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListProbSmall.Images.SetKeyName(0, "");
            this.imageListProbSmall.Images.SetKeyName(1, "");
            // 
            // BackGroundPanel
            // 
            this.BackGroundPanel.BackColor = System.Drawing.Color.White;
            this.BackGroundPanel.BorderColor = System.Drawing.Color.Black;
            this.BackGroundPanel.BorderSides = System.Windows.Forms.Border3DSide.Middle;
            this.BackGroundPanel.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.BackGroundPanel.Controls.Add(this.pSpectrumCursors);
            this.BackGroundPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BackGroundPanel.Location = new System.Drawing.Point(0, 0);
            this.BackGroundPanel.Name = "BackGroundPanel";
            this.BackGroundPanel.Size = new System.Drawing.Size(808, 432);
            this.BackGroundPanel.TabIndex = 5;
            // 
            // pSpectrumCursors
            // 
            this.pSpectrumCursors.BackColor = System.Drawing.Color.Transparent;
            this.pSpectrumCursors.Controls.Add(this.lbDelta);
            this.pSpectrumCursors.Controls.Add(this.lbt2);
            this.pSpectrumCursors.Controls.Add(this.lbt1);
            this.pSpectrumCursors.Controls.Add(this.spectrumBackground);
            this.pSpectrumCursors.Location = new System.Drawing.Point(88, 382);
            this.pSpectrumCursors.Name = "pSpectrumCursors";
            this.pSpectrumCursors.Size = new System.Drawing.Size(383, 25);
            this.pSpectrumCursors.TabIndex = 3;
            // 
            // lbDelta
            // 
            this.lbDelta.AutoSize = true;
            this.lbDelta.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbDelta.Location = new System.Drawing.Point(245, 4);
            this.lbDelta.Name = "lbDelta";
            this.lbDelta.Size = new System.Drawing.Size(38, 16);
            this.lbDelta.TabIndex = 4;
            this.lbDelta.Text = "Delta";
            // 
            // lbt2
            // 
            this.lbt2.AutoSize = true;
            this.lbt2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbt2.Location = new System.Drawing.Point(123, 4);
            this.lbt2.Name = "lbt2";
            this.lbt2.Size = new System.Drawing.Size(19, 16);
            this.lbt2.TabIndex = 6;
            this.lbt2.Text = "t2";
            // 
            // lbt1
            // 
            this.lbt1.AutoSize = true;
            this.lbt1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbt1.Location = new System.Drawing.Point(34, 4);
            this.lbt1.Name = "lbt1";
            this.lbt1.Size = new System.Drawing.Size(19, 16);
            this.lbt1.TabIndex = 5;
            this.lbt1.Text = "t1";
            // 
            // SpectrumBackground
            // 
            this.spectrumBackground.Location = new System.Drawing.Point(0, 0);
            this.spectrumBackground.Name = "SpectrumBackground";
            this.spectrumBackground.Size = new System.Drawing.Size(280, 25);
            this.spectrumBackground.TabIndex = 3;
            this.spectrumBackground.TabStop = false;
            // 
            // SpectrumGradient
            // 
            this.SpectrumGradient.BackColor = System.Drawing.Color.White;
            this.SpectrumGradient.Image = ((System.Drawing.Image)(resources.GetObject("SpectrumGradient.Image")));
            this.SpectrumGradient.Location = new System.Drawing.Point(664, 32);
            this.SpectrumGradient.Name = "SpectrumGradient";
            this.SpectrumGradient.Size = new System.Drawing.Size(24, 360);
            this.SpectrumGradient.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.SpectrumGradient.TabIndex = 0;
            this.SpectrumGradient.TabStop = false;
            // 
            // spectrumGraph1
            // 
            this.spectrumGraph1.BackColor = System.Drawing.SystemColors.Desktop;
            this.spectrumGraph1.CursorsMode = TCD2003.GUI.GlobalTypes.TCursorsMode.None;
            this.spectrumGraph1.DrawModeBckEnvelope = false;
            this.spectrumGraph1.DrawModeFwdEnvelope = false;
            this.spectrumGraph1.DrawPeakBckEnvelope = false;
            this.spectrumGraph1.DrawPeakFwdEnvelope = true;
            this.spectrumGraph1.DrawSpectrum = true;
            this.spectrumGraph1.JustGotFocus = false;
            this.spectrumGraph1.LastDrawTime = System.TimeSpan.Parse("00:00:00");
            this.spectrumGraph1.Location = new System.Drawing.Point(88, 72);
            this.spectrumGraph1.MasterYValue = 0D;
            this.spectrumGraph1.Name = "spectrumGraph1";
            this.spectrumGraph1.OverridenSpectrumImage = null;
            this.spectrumGraph1.SecondYValue = 0D;
            this.spectrumGraph1.Size = new System.Drawing.Size(536, 304);
            this.spectrumGraph1.SpeedFactor = 1;
            this.spectrumGraph1.TabIndex = 0;
            this.spectrumGraph1.TimeInGraph = 0.0041875D;
            this.spectrumGraph1.Units = TCD2003.GUI.GlobalTypes.TSpectrumUnits.KHz;
            this.spectrumGraph1.YAxis = this.spectrumYAxis1;
            this.spectrumGraph1.ZeroLinePercent = 50;
            // 
            // clinicalBar1
            // 
            this.clinicalBar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(235)))), ((int)(((byte)(250)))));
            this.clinicalBar1.ClinicalParamLabelAverageLower = 0D;
            this.clinicalBar1.ClinicalParamLabelAverageUpper = 0D;
            this.clinicalBar1.ClinicalParamLabelDVLower = 0D;
            this.clinicalBar1.ClinicalParamLabelDVUpper = 0D;
            this.clinicalBar1.ClinicalParamLabelExt1Lower = 0D;
            this.clinicalBar1.ClinicalParamLabelExt1Upper = 0D;
            this.clinicalBar1.ClinicalParamLabelExt2Lower = 0D;
            this.clinicalBar1.ClinicalParamLabelExt2Upper = 0D;
            this.clinicalBar1.ClinicalParamLabelExt3Lower = 0D;
            this.clinicalBar1.ClinicalParamLabelExt3Upper = 0D;
            this.clinicalBar1.ClinicalParamLabelExt4Lower = 0D;
            this.clinicalBar1.ClinicalParamLabelExt4Upper = 0D;
            this.clinicalBar1.ClinicalParamLabelExt5Lower = 0D;
            this.clinicalBar1.ClinicalParamLabelExt5Upper = 0D;
            this.clinicalBar1.ClinicalParamLabelExt6Lower = 0D;
            this.clinicalBar1.ClinicalParamLabelExt6Upper = 0D;
            this.clinicalBar1.ClinicalParamLabelExt7Lower = 0D;
            this.clinicalBar1.ClinicalParamLabelExt7Upper = 0D;
            this.clinicalBar1.ClinicalParamLabelExt8Lower = 0D;
            this.clinicalBar1.ClinicalParamLabelExt8Upper = 0D;
            this.clinicalBar1.ClinicalParamLabelHitsLower = 0D;
            this.clinicalBar1.ClinicalParamLabelHitsRateLower = 0D;
            this.clinicalBar1.ClinicalParamLabelHitsRateUpper = 0D;
            this.clinicalBar1.ClinicalParamLabelHitsUpper = 0D;
            this.clinicalBar1.ClinicalParamLabelMeanLower = 0D;
            this.clinicalBar1.ClinicalParamLabelMeanUpper = 0D;
            this.clinicalBar1.ClinicalParamLabelModeLower = 0D;
            this.clinicalBar1.ClinicalParamLabelModeUpper = 0D;
            this.clinicalBar1.ClinicalParamLabelPeakLower = 0D;
            this.clinicalBar1.ClinicalParamLabelPeakUpper = 0D;
            this.clinicalBar1.ClinicalParamLabelPILower = 0D;
            this.clinicalBar1.ClinicalParamLabelPIUpper = 0D;
            this.clinicalBar1.ClinicalParamLabelRILower = 0D;
            this.clinicalBar1.ClinicalParamLabelRIUpper = 0D;
            this.clinicalBar1.ClinicalParamLabelSDLower = 0D;
            this.clinicalBar1.ClinicalParamLabelSDUpper = 0D;
            this.clinicalBar1.ClinicalParamLabelSWLower = 0D;
            this.clinicalBar1.ClinicalParamLabelSWUpper = 0D;
            this.clinicalBar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.clinicalBar1.Location = new System.Drawing.Point(704, 0);
            this.clinicalBar1.Name = "clinicalBar1";
            this.clinicalBar1.SetupFlowToShow = ((TCD2003.GUI.GlobalTypes.FlowToShow)((TCD2003.GUI.GlobalTypes.FlowToShow.ShowTop | TCD2003.GUI.GlobalTypes.FlowToShow.ShowBottom)));
            this.clinicalBar1.Size = new System.Drawing.Size(104, 432);
            this.clinicalBar1.TabIndex = 8;
            this.clinicalBar1.UseEnvelopesForRCPDisplay = false;
            // 
            // SpectrumChart
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.Controls.Add(this.spectrumGraph1);
            this.Controls.Add(this.SpectrumGradient);
            this.Controls.Add(this.clinicalBar1);
            this.Controls.Add(this.gradientPanelCM);
            this.Controls.Add(this.BackGroundPanel);
            this.Name = "SpectrumChart";
            this.Size = new System.Drawing.Size(808, 432);
            this.SizeChanged += new System.EventHandler(this.SpectrumChart_SizeChanged);
            this.gradientPanelCM.ResumeLayout(false);
            this.BackGroundPanel.ResumeLayout(false);
            this.pSpectrumCursors.ResumeLayout(false);
            this.pSpectrumCursors.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spectrumBackground)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpectrumGradient)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Syncfusion.Windows.Forms.Tools.GradientPanel gradientPanelCM;
        private System.Windows.Forms.Button bUnits;
        private System.ComponentModel.IContainer components;
        private System.Windows.Forms.Label probImage;
        private System.Windows.Forms.ImageList imageListProbLarge;
        private System.Windows.Forms.ImageList imageListProbSmall;
        private Syncfusion.Windows.Forms.Tools.GradientPanel BackGroundPanel;
        private System.Windows.Forms.PictureBox SpectrumGradient;
        public TCD2003.GUI.ClinicalBar clinicalBar1;
        private TCD2003.GUI.SpectrumGraph spectrumGraph1;
        public TCD2003.GUI.SpectrumYAxis spectrumYAxis1;
        private System.Windows.Forms.Panel pSpectrumCursors;
        private System.Windows.Forms.Label lbDelta;
        private System.Windows.Forms.Label lbt2;
        private System.Windows.Forms.Label lbt1;
        private System.Windows.Forms.PictureBox spectrumBackground;
    }
}