namespace TCD2003.GUI
{
    partial class TrendGraph
    {
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            this.ReleaseResources();
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        private void InitializeComponent()
        {
            this.PictureBoxToShow = new System.Windows.Forms.PictureBox();
            this.SuspendLayout();
            // 
            // PictureBoxToShow
            // 
            this.PictureBoxToShow.BackColor = System.Drawing.Color.Black;
            this.PictureBoxToShow.Location = new System.Drawing.Point(0, 0);
            this.PictureBoxToShow.Name = "PictureBoxToShow";
            this.PictureBoxToShow.Size = new System.Drawing.Size(104, 56);
            this.PictureBoxToShow.TabIndex = 1;
            this.PictureBoxToShow.TabStop = false;
            this.PictureBoxToShow.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            // 
            // TrendGraph
            // 
            this.BackColor = System.Drawing.Color.Black;
            this.Controls.Add(this.PictureBoxToShow);
            this.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.Name = "TrendGraph";
            this.Size = new System.Drawing.Size(584, 232);
            //			this.SizeChanged += new System.EventHandler(this.TrendGraph_SizeChanged);

            this.MouseUp += new System.Windows.Forms.MouseEventHandler(PictureBoxToShow_MouseUp);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(PictureBoxToShow_MouseMove);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(PictureBoxToShow_MouseDown);
            PictureBoxToShow.MouseDown += new System.Windows.Forms.MouseEventHandler(PictureBoxToShow_MouseDown);
            PictureBoxToShow.MouseMove += new System.Windows.Forms.MouseEventHandler(PictureBoxToShow_MouseMove);
            PictureBoxToShow.MouseUp += new System.Windows.Forms.MouseEventHandler(PictureBoxToShow_MouseUp);

            this.ResumeLayout(false);

        }

        #endregion

        private System.ComponentModel.Container components = null;
        /// <summary>
        /// PictureBox for trends imaging 
        /// </summary>
        private System.Windows.Forms.PictureBox PictureBoxToShow;
    }
}