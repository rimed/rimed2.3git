﻿namespace TCD2003.GUI
{
    partial class SpectrumGraph
    {
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            // 
            // SpectrumGraph
            // 
            this.BackColor = System.Drawing.SystemColors.Desktop;
            this.Name = "SpectrumGraph";
            this.Size = new System.Drawing.Size(542, 286);
            this.VisibleChanged += new System.EventHandler(this.SpectrumGraph_VisibleChanged);
            this.SizeChanged += new System.EventHandler(this.SpectrumGraph_SizeChanged);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.SpectrumGraph_MouseUp);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.SpectrumGraph_MouseMove);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.SpectrumGraph_MouseDown);

        }

        #endregion

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
    }
}