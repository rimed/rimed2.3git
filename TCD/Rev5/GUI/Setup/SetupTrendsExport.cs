using System;
using TCD2003.Utils;

namespace TCD2003.GUI.Setup
{
	public partial class SetupTrendsExport : SetUpBase
	{
        private int m_exportInterval;
        private string m_exportSeparator = string.Empty;

        public SetupTrendsExport()
		{
			this.InitializeComponent();
		}

		public int ExportInterval
		{
			get
			{
                return this.m_exportInterval;
			}
		}

		public string ExportSeparator
		{
			get
			{
                return this.m_exportSeparator;
			}
		}

		private void cbExportInterval_SelectedIndexChanged(object sender, EventArgs e)
		{
            if (this.cbExportInterval.SelectedIndex == 1)
                this.m_exportInterval = GlobalSettings.LongExportInterval;
			else
                this.m_exportInterval = GlobalSettings.ShortExportInterval;
		}

		private void SetupTrendsExport_Load(object sender, EventArgs e)
		{
            var strRes = ResourceManagerWrapper.GetWrapper(MainForm.ResourceManager); 
            
            this.lbHeader.Text = strRes.GetString("Export trends to Excel");
			this.lbExport.Text = strRes.GetString("Export Clinical Parameters to Excel every:");
			this.cbExportInterval.Items.Add("30 " + strRes.GetString("Sec"));
			this.cbExportInterval.Items.Add("1 " + strRes.GetString("minutes"));

            switch (this.m_exportInterval)
			{
				case 0:
					Microsoft.Win32.RegistryKey myKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Rimed", true);
					if (myKey != null)
					{
						if (myKey.GetValue("ExportInterval") != null)
						{
                            this.m_exportInterval = Convert.ToInt32(myKey.GetValue("ExportInterval"));
						}
						else
						{
							//Default time interval for export = 30 sec
                            this.m_exportInterval = GlobalSettings.ShortExportInterval;
							myKey.SetValue("ExportInterval", GlobalSettings.ShortExportInterval);
						}
					}
					else
                        this.m_exportInterval = GlobalSettings.ShortExportInterval;
                    if (this.m_exportInterval == GlobalSettings.ShortExportInterval)
                        this.cbExportInterval.SelectedIndex = 0;
					else
                        this.cbExportInterval.SelectedIndex = 1;
					break;
				case GlobalSettings.LongExportInterval:
                    this.cbExportInterval.SelectedIndex = 1;
					break;
				default:
                    this.cbExportInterval.SelectedIndex = 0;
					break;
			}

			this.lbSeparator.Text = strRes.GetString("Field separator:");
			this.cbSeparator.Items.Add(GlobalSettings.CommaSeparator);
			this.cbSeparator.Items.Add(GlobalSettings.SemicolumnSeparator);

			switch (m_exportSeparator)
			{
				case "":
					Microsoft.Win32.RegistryKey myKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Rimed", true);
					if (myKey != null)
					{
						if (myKey.GetValue("ExportSeparator") != null)
						{
							m_exportSeparator = Convert.ToString(myKey.GetValue("ExportSeparator"));
						}
						else
						{
							//Default separator for the export file is comma
                            this.m_exportSeparator = GlobalSettings.CommaSeparator;
							myKey.SetValue("ExportSeparator", GlobalSettings.CommaSeparator);
						}
					}
					else
                        this.m_exportSeparator = GlobalSettings.CommaSeparator;
                    if (this.m_exportSeparator == GlobalSettings.CommaSeparator)
                        this.cbSeparator.SelectedIndex = 0;
					else
                        this.cbSeparator.SelectedIndex = 1;
					break;
				case GlobalSettings.SemicolumnSeparator:
                    this.cbSeparator.SelectedIndex = 1;
					break;
				default:
                    this.cbSeparator.SelectedIndex = 0;
					break;
			}
		}

		private void cbSeparator_SelectedIndexChanged(object sender, EventArgs e)
		{
            if (this.cbSeparator.SelectedIndex == 1)
                this.m_exportSeparator = GlobalSettings.SemicolumnSeparator;
			else
                this.m_exportSeparator = GlobalSettings.CommaSeparator;
		}
	}
}

