namespace TCD2003.GUI.Setup
{
    partial class SetupListViewCtrl
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbCaption = new System.Windows.Forms.Label();
            this.labelTextWnd = new System.Windows.Forms.Label();
            this.chkBoxRestoreManufacturerDefaults = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // lbCaption
            // 
            this.lbCaption.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lbCaption.Location = new System.Drawing.Point(24, 40);
            this.lbCaption.Name = "lbCaption";
            this.lbCaption.Size = new System.Drawing.Size(400, 23);
            this.lbCaption.TabIndex = 1;
            this.lbCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelTextWnd
            // 
            this.labelTextWnd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelTextWnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.labelTextWnd.Location = new System.Drawing.Point(24, 280);
            this.labelTextWnd.Name = "labelTextWnd";
            this.labelTextWnd.Size = new System.Drawing.Size(400, 56);
            this.labelTextWnd.TabIndex = 1;
            this.labelTextWnd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelTextWnd.Visible = false;
            // 
            // chkBoxRestoreManufacturerDefaults
            // 
            this.chkBoxRestoreManufacturerDefaults.Location = new System.Drawing.Point(24, 72);
            this.chkBoxRestoreManufacturerDefaults.Name = "chkBoxRestoreManufacturerDefaults";
            this.chkBoxRestoreManufacturerDefaults.Size = new System.Drawing.Size(400, 40);
            this.chkBoxRestoreManufacturerDefaults.TabIndex = 2;
            this.chkBoxRestoreManufacturerDefaults.CheckedChanged += new System.EventHandler(this.chkBoxRestoreManufacturerDefaults_CheckedChanged);
            // 
            // SetupListViewCtrl
            // 
            this.Controls.Add(this.chkBoxRestoreManufacturerDefaults);
            this.Controls.Add(this.lbCaption);
            this.Controls.Add(this.labelTextWnd);
            this.Name = "SetupListViewCtrl";
            this.ResumeLayout(false);

        }
        #endregion

        private System.Windows.Forms.Label lbCaption;
        private System.Windows.Forms.CheckBox chkBoxRestoreManufacturerDefaults;
        private System.Windows.Forms.Label labelTextWnd;
        private System.ComponentModel.IContainer components = null;
    }
}