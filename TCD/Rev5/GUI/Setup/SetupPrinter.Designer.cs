namespace TCD2003.GUI.Setup
{
    partial class SetupPrinter
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetupPrinter));
            this.checkBoxPLprintPreview = new System.Windows.Forms.CheckBox();
            this.dsConfiguation1 = new TCD2003.DB.dsConfiguation();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox1Chart = new System.Windows.Forms.PictureBox();
            this.pictureBox2Chart = new System.Windows.Forms.PictureBox();
            this.pictureBox4Chart = new System.Windows.Forms.PictureBox();
            this.pictureBox6Chart = new System.Windows.Forms.PictureBox();
            this.checkBoxNoBackground = new System.Windows.Forms.CheckBox();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.btnPrinterConfiguration = new System.Windows.Forms.Button();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.pageSetupDialog1 = new System.Windows.Forms.PageSetupDialog();
            this.lblHeader = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dsConfiguation1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1Chart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2Chart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4Chart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6Chart)).BeginInit();
            this.SuspendLayout();
            // 
            // checkBoxPLprintPreview
            // 
            this.checkBoxPLprintPreview.AutoSize = true;
            this.checkBoxPLprintPreview.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.dsConfiguation1, "tb_BasicConfiguration.LayoutRepPreview", true));
            this.checkBoxPLprintPreview.Location = new System.Drawing.Point(40, 179);
            this.checkBoxPLprintPreview.Name = "checkBoxPLprintPreview";
            this.checkBoxPLprintPreview.Size = new System.Drawing.Size(117, 17);
            this.checkBoxPLprintPreview.TabIndex = 4;
            this.checkBoxPLprintPreview.Text = "Show Print preview";
            this.checkBoxPLprintPreview.Visible = false;
            // 
            // dsConfiguation1
            // 
            this.dsConfiguation1.DataSetName = "dsConfiguation";
            this.dsConfiguation1.Locale = new System.Globalization.CultureInfo("en-US");
            this.dsConfiguation1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pictureBox1Chart);
            this.groupBox1.Controls.Add(this.pictureBox2Chart);
            this.groupBox1.Controls.Add(this.pictureBox4Chart);
            this.groupBox1.Controls.Add(this.pictureBox6Chart);
            this.groupBox1.Location = new System.Drawing.Point(240, 65);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(80, 226);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Layout";
            // 
            // pictureBox1Chart
            // 
            this.pictureBox1Chart.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox1Chart.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1Chart.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1Chart.Image")));
            this.pictureBox1Chart.Location = new System.Drawing.Point(16, 24);
            this.pictureBox1Chart.Name = "pictureBox1Chart";
            this.pictureBox1Chart.Size = new System.Drawing.Size(48, 48);
            this.pictureBox1Chart.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1Chart.TabIndex = 0;
            this.pictureBox1Chart.TabStop = false;
            this.pictureBox1Chart.Click += new System.EventHandler(this.pictureBox1Chart_Click);
            // 
            // pictureBox2Chart
            // 
            this.pictureBox2Chart.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2Chart.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2Chart.Image")));
            this.pictureBox2Chart.Location = new System.Drawing.Point(16, 72);
            this.pictureBox2Chart.Name = "pictureBox2Chart";
            this.pictureBox2Chart.Size = new System.Drawing.Size(48, 48);
            this.pictureBox2Chart.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2Chart.TabIndex = 0;
            this.pictureBox2Chart.TabStop = false;
            this.pictureBox2Chart.Click += new System.EventHandler(this.pictureBox2Chart_Click);
            // 
            // pictureBox4Chart
            // 
            this.pictureBox4Chart.BackColor = System.Drawing.SystemColors.Control;
            this.pictureBox4Chart.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox4Chart.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4Chart.Image")));
            this.pictureBox4Chart.Location = new System.Drawing.Point(16, 120);
            this.pictureBox4Chart.Name = "pictureBox4Chart";
            this.pictureBox4Chart.Size = new System.Drawing.Size(48, 48);
            this.pictureBox4Chart.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox4Chart.TabIndex = 0;
            this.pictureBox4Chart.TabStop = false;
            this.pictureBox4Chart.Click += new System.EventHandler(this.pictureBox4Chart_Click);
            // 
            // pictureBox6Chart
            // 
            this.pictureBox6Chart.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox6Chart.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6Chart.Image")));
            this.pictureBox6Chart.Location = new System.Drawing.Point(16, 168);
            this.pictureBox6Chart.Name = "pictureBox6Chart";
            this.pictureBox6Chart.Size = new System.Drawing.Size(48, 48);
            this.pictureBox6Chart.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox6Chart.TabIndex = 0;
            this.pictureBox6Chart.TabStop = false;
            this.pictureBox6Chart.Click += new System.EventHandler(this.pictureBox6Chart_Click);
            // 
            // checkBoxNoBackground
            // 
            this.checkBoxNoBackground.AutoSize = true;
            this.checkBoxNoBackground.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.dsConfiguation1, "tb_BasicConfiguration.LayoutRepWhiteBg", true));
            this.checkBoxNoBackground.Location = new System.Drawing.Point(40, 75);
            this.checkBoxNoBackground.Name = "checkBoxNoBackground";
            this.checkBoxNoBackground.Size = new System.Drawing.Size(101, 17);
            this.checkBoxNoBackground.TabIndex = 4;
            this.checkBoxNoBackground.Text = "No Background";
            // 
            // radioButton2
            // 
            this.radioButton2.Location = new System.Drawing.Point(0, 0);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(104, 24);
            this.radioButton2.TabIndex = 0;
            // 
            // radioButton3
            // 
            this.radioButton3.Location = new System.Drawing.Point(0, 0);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(104, 24);
            this.radioButton3.TabIndex = 0;
            // 
            // radioButton4
            // 
            this.radioButton4.Location = new System.Drawing.Point(0, 0);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(104, 24);
            this.radioButton4.TabIndex = 0;
            // 
            // btnPrinterConfiguration
            // 
            this.btnPrinterConfiguration.AutoSize = true;
            this.btnPrinterConfiguration.Location = new System.Drawing.Point(150, 308);
            this.btnPrinterConfiguration.Name = "btnPrinterConfiguration";
            this.btnPrinterConfiguration.Size = new System.Drawing.Size(172, 24);
            this.btnPrinterConfiguration.TabIndex = 1;
            this.btnPrinterConfiguration.Text = "Printer Configuration";
            this.btnPrinterConfiguration.Click += new System.EventHandler(this.btnPrinterConfiguration_Click);
            // 
            // lblHeader
            // 
            this.lblHeader.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblHeader.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.lblHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lblHeader.Location = new System.Drawing.Point(40, 19);
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Size = new System.Drawing.Size(376, 23);
            this.lblHeader.TabIndex = 16;
            this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SetupPrinter
            // 
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.lblHeader);
            this.Controls.Add(this.btnPrinterConfiguration);
            this.Controls.Add(this.checkBoxPLprintPreview);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.checkBoxNoBackground);
            this.Name = "SetupPrinter";
            this.Size = new System.Drawing.Size(472, 352);
            this.Load += new System.EventHandler(this.SetupPrinter_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dsConfiguation1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1Chart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2Chart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4Chart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6Chart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnPrinterConfiguration;
        private System.Windows.Forms.PrintDialog printDialog1;
        private System.Windows.Forms.PageSetupDialog pageSetupDialog1;
        private System.Windows.Forms.CheckBox checkBoxPLprintPreview;
        private System.Windows.Forms.PictureBox pictureBox1Chart;
        private System.Windows.Forms.PictureBox pictureBox2Chart;
        private System.Windows.Forms.PictureBox pictureBox4Chart;
        private System.Windows.Forms.PictureBox pictureBox6Chart;
        private TCD2003.DB.dsConfiguation dsConfiguation1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.ComponentModel.Container components = null;
        private System.Windows.Forms.CheckBox checkBoxNoBackground;
        private System.Windows.Forms.Label lblHeader;
    }
}