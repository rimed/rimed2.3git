using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using TCD2003.DB;
using TCD2003.Utils;

namespace TCD2003.GUI.Setup
{
    // TODO: [uuu] Need to change the BV's list box of syncfusion to regular list box, it will make things more easily.
    /// <summary>
    /// Summary description for UserControl1.
    /// </summary>
    public partial class SetUpBloodVessels : SetUpBase
    {
        private int m_selectedBVIndex = -1;
        private List<ProbeComboBox> m_probeComboBoxes = new List<ProbeComboBox>();

        private static int[] m_ranges =
        {
			GlobalSettings.RangeStep0,
			GlobalSettings.RangeStep1,
			GlobalSettings.RangeStep2,
			GlobalSettings.RangeStep3,
			GlobalSettings.RangeStep4,
			GlobalSettings.RangeStep5,
			GlobalSettings.RangeStep6,
			GlobalSettings.RangeStep7,
			GlobalSettings.RangeStep8
		};

        private int[] m_powers =
            { 
	            GlobalSettings.PowerStep0,
	            GlobalSettings.PowerStep1,
	            GlobalSettings.PowerStep2,
	            GlobalSettings.PowerStep3,
	            GlobalSettings.PowerStep4,
	            GlobalSettings.PowerStep5,
	            GlobalSettings.PowerStep6,
	            GlobalSettings.PowerStep7,
	            GlobalSettings.PowerStep8,
	            GlobalSettings.PowerStep9,
	            GlobalSettings.PowerStep10,
	            GlobalSettings.PowerStep11,
	            GlobalSettings.PowerStep12,
	            GlobalSettings.PowerStep13,
	            GlobalSettings.PowerStep14,
	            GlobalSettings.PowerStep15
            };

        private int[] m_thumps = 
            {
				GlobalSettings.ThumpStep0,
				GlobalSettings.ThumpStep1,
				GlobalSettings.ThumpStep2,
				GlobalSettings.ThumpStep3,
				GlobalSettings.ThumpStep4,
				GlobalSettings.ThumpStep5,
				GlobalSettings.ThumpStep6
			};

        /// <summary>
        /// Constructor
        /// </summary>
        public SetUpBloodVessels()
        {
            // This call is required by the Windows.Forms Form Designer.
            this.InitializeComponent();
            this.ReloadControlLabels();
            this.dsBVSetup2.Merge(RimedDal.Instance.s_dsBVSetup);

            // fill dsSetup1
            bool bvExist = false;
            this.dsBVSetup1.Clear();
            for (int i = 0; i < this.dsBVSetup2.tb_BloodVesselSetup.Rows.Count; i++)
            {
                DB.dsBVSetup.tb_BloodVesselSetupRow row2 = (DB.dsBVSetup.tb_BloodVesselSetupRow)this.dsBVSetup2.tb_BloodVesselSetup.Rows[i];
                for (int q = 0; q < this.dsBVSetup1.tb_BloodVesselSetup.Rows.Count; q++)
                {
                    DB.dsBVSetup.tb_BloodVesselSetupRow row1 = (DB.dsBVSetup.tb_BloodVesselSetupRow)this.dsBVSetup1.tb_BloodVesselSetup.Rows[q];

                    string name1 = row1["Name"].ToString();
                    string name2 = row2["Name"].ToString();
                    if ((name1.Equals("") || name2.Equals("")))
                        continue;
                    if (name1.Equals(name2))
                    {
                        bvExist = true;
                        if ((((GlobalTypes.ESide)row1["Side"]) == GlobalTypes.ESide.Left && ((GlobalTypes.ESide)row2["Side"]) == GlobalTypes.ESide.Right)
                            || (((GlobalTypes.ESide)row2["Side"]) == GlobalTypes.ESide.Left && ((GlobalTypes.ESide)row1["Side"]) == GlobalTypes.ESide.Right))
                        {
                            row1["Side"] = (int)GlobalTypes.ESide.BothSides;
                        }
                        break;
                    }
                }
                if (!bvExist)
                {
                    DB.dsBVSetup.tb_BloodVesselSetupRow row = (DB.dsBVSetup.tb_BloodVesselSetupRow)dsBVSetup1.tb_BloodVesselSetup.NewRow();
                    row["Blood_Vessel_Index"] = row2["Blood_Vessel_Index"];
                    row["Name"] = row2["Name"];
                    row["Probe"] = row2["Probe"];
                    row["Depth"] = row2["Depth"];
                    row["Gain"] = row2["Gain"];
                    row["Thump"] = row2["Thump"];
                    row["Width"] = row2["Width"];
                    row["Power"] = row2["Power"];
                    row["MinFrequency"] = row2["MinFrequency"];
                    row["MaxFrequency"] = row2["MaxFrequency"];
                    row["Units"] = row2["Units"];
                    row["ZeroLinePosition"] = row2["ZeroLinePosition"];
                    row["DirectionToProbe"] = row2["DirectionToProbe"];
                    row["Comments"] = row2["Comments"];
                    row["DisplayPeakFwdEnvelope"] = row2["DisplayPeakFwdEnvelope"];
                    row["DisplayPeakBckEnvelope"] = row2["DisplayPeakBckEnvelope"];
                    row["DisplayMeanFwdEnvelope"] = row2["DisplayMeanFwdEnvelope"];
                    row["DisplayMeanBckEnvelope"] = row2["DisplayMeanBckEnvelope"];
                    row["UseEnvelopesForRCPDisplay"] = row2["UseEnvelopesForRCPDisplay"];
                    row["DisplaySecptrum"] = row2["DisplaySecptrum"];
                    row["HitsThreshold"] = row2["HitsThreshold"];
                    row["Angle"] = row2["Angle"];
                    row["FreqRang"] = row2["FreqRang"];
                    row["FreqRang"] = row2["FreqRang"];
                    row["Side"] = row2["Side"];
                    row["Location"] = row2["Location"];
                    this.dsBVSetup1.tb_BloodVesselSetup.Addtb_BloodVesselSetupRow(row);
                }
                bvExist = false;
            }

            #region Init ComboBox

            this.comboBoxUnits.DataBindings.Add(new Binding("SelectedIndex", this.dsBVSetup1, "tb_BloodVesselSetup.Units"));

            // Fill Power ComboBox.
            this.comboBoxPower.Items.Clear();
            this.comboBoxPower.DataSource = m_powers;
            this.comboBoxPower.DataBindings.Add(new Binding("SelectedItem", this.dsBVSetup1, "tb_BloodVesselSetup.Power"));

            this.textBoxHitsThreshold.DataBindings.Add("Text", this.dsBVSetup1, "tb_BloodVesselSetup.HitsThreshold");

            // Fill thump ComboBox.
            this.comboBoxThump.Items.Clear();
            this.comboBoxThump.DataSource = m_thumps;
            this.comboBoxThump.DataBindings.Add(new Binding("SelectedItem", this.dsBVSetup1, "tb_BloodVesselSetup.Thump"));

            this.numericUpDownZeroLine.DataBindings.Add(new Binding("Value", this.dsBVSetup1, "tb_BloodVesselSetup.ZeroLinePosition", true));

            #endregion

            for (int i = 0; i < this.gridDataBoundGrid1.Model.ColCount + 1; i++)
                this.gridDataBoundGrid1.Model.Cols.Hidden[i] = true;

            this.gridDataBoundGrid1.Model.Cols.Hidden[0] = false;
            this.gridDataBoundGrid1.Model.Cols.Size[0] = 15;
            this.gridDataBoundGrid1.Model.Cols.Hidden["Name"] = false;
            this.gridDataBoundGrid1.Model.Cols.Size["Name"] = gridDataBoundGrid1.Width - 15;
            this.gridDataBoundGrid1.Model.Rows.Hidden[0] = true;

            this.UpdateProbesComboBox();

            // initial the probe comboBox
            this.comboBoxProbe.DisplayMember = "Name";
            this.comboBoxProbe.ValueMember = "ProbeIndex";

            this.BackColor = GlobalSettings.BackgroundDlg;

            // Fill Range ComboBox.
            this.UpdateRangeComboBox(-1);
        }

        private void ReloadControlLabels()
        {
            var strRes = ResourceManagerWrapper.GetWrapper(MainForm.ResourceManager); 

            this.chkDisplaySpectrum.Text = strRes.GetString("Display Spectrum");
            this.gbDisplayEnvelopes.Text = strRes.GetString("Display Envelopes");
            this.checkBoxReverseMode.Text = strRes.GetString("Reverse Mode");
            this.checkBoxForwardMode.Text = strRes.GetString("Forward Mode");
            this.checkBoxReversePeak.Text = strRes.GetString("Reverse Peak");
            this.checkBoxForwardPeak.Text = strRes.GetString("Forward Peak");
            this.lbPower.Text = strRes.GetString("Power") + " [%]:";
            this.lbSampleVolume.Text = strRes.GetString("Sample Volume [mm]:");
            this.lbDepth.Text = strRes.GetString("Depth [mm]:");
            this.lbFilter.Text = strRes.GetString("Filter [Hz]:");
            this.lbGain.Text = strRes.GetString("Gain [db]:");
            this.albDirection.Text = strRes.GetString("Direction") + ":";
            this.lbUnits.Text = strRes.GetString("Units") + ":";
            this.lbZeroLine.Text = strRes.GetString("Zero Line [%]:");
            this.lbScale.Text = strRes.GetString("Scale [KHz]:");
            this.lbProbe.Text = strRes.GetString("Probe") + ":";
            this.lbCaption.Text = strRes.GetString("Default Parameters For Selected Blood Vessel");
            this.barItemAdd.ToolTipText = strRes.GetString("Add");
            this.barItemDelete.ToolTipText = strRes.GetString("Delete");
            this.gbHitsDetection.Text = strRes.GetString("Hits Detection");
            this.autoLabel1.Text = strRes.GetString("Threshold [db]:");
            this.lbAngle.Text = strRes.GetString("Angle") + ":";
            this.chkUseEnvelope.Text = strRes.GetString("Use Envelopes For Clinical Parameters Display");
        }

        /// <summary>
        /// This function copy dataSet1 [that refers to the dataGrid] to dataSet2 [that refers to the database].
        /// </summary>
        /// <returns></returns>
        public override bool Verify()
        {
            this.BindingContext[dsBVSetup1, "tb_BloodVesselSetup"].EndCurrentEdit();

            if (this.dsBVSetup1.HasChanges())
            {
                bool bvExist = false;
                for (int i = 0; i < dsBVSetup1.tb_BloodVesselSetup.Rows.Count; i++)
                {
                    DB.dsBVSetup.tb_BloodVesselSetupRow row1 = (DB.dsBVSetup.tb_BloodVesselSetupRow)this.dsBVSetup1.tb_BloodVesselSetup.Rows[i];

                    for (int q = 0; q < dsBVSetup2.tb_BloodVesselSetup.Rows.Count; q++)
                    {
                        DB.dsBVSetup.tb_BloodVesselSetupRow row2 = (DB.dsBVSetup.tb_BloodVesselSetupRow)this.dsBVSetup2.tb_BloodVesselSetup.Rows[q];

                        if (((String)row2["Name"]) == ((String)row1["Name"]) &&
                            ((int)row2["Side"] == (int)row1["Side"] || (int)row1["Side"] == (int)GlobalTypes.ESide.BothSides))
                        {
                            bvExist = true;

                            row2["Probe"] = row1["Probe"];
                            row2["Depth"] = row1["Depth"];
                            row2["Gain"] = row1["Gain"];
                            row2["Thump"] = row1["Thump"];
                            row2["Width"] = row1["Width"];
                            row2["Power"] = row1["Power"];
                            row2["MinFrequency"] = row1["MinFrequency"];
                            row2["MaxFrequency"] = row1["MaxFrequency"];
                            row2["Units"] = row1["Units"];
                            row2["ZeroLinePosition"] = row1["ZeroLinePosition"];
                            row2["DirectionToProbe"] = row1["DirectionToProbe"];
                            row2["Comments"] = row1["Comments"];
                            row2["DisplayPeakFwdEnvelope"] = row1["DisplayPeakFwdEnvelope"];
                            row2["DisplayPeakBckEnvelope"] = row1["DisplayPeakBckEnvelope"];
                            row2["DisplayMeanFwdEnvelope"] = row1["DisplayMeanFwdEnvelope"];
                            row2["DisplayMeanBckEnvelope"] = row1["DisplayMeanBckEnvelope"];
                            row2["UseEnvelopesForRCPDisplay"] = row1["UseEnvelopesForRCPDisplay"];
                            row2["DisplaySecptrum"] = row1["DisplaySecptrum"];
                            row2["HitsThreshold"] = row1["HitsThreshold"];
                            row2["Angle"] = row1["Angle"];
                            row2["FreqRang"] = row1["FreqRang"];
                            row2["FreqRang"] = row1["FreqRang"];
                            //row2["Side"] = row1["Side"];
                            row2["Location"] = row1["Location"];
                        }
                        if (((String)row2["Name"]) == ((String)row1["Name"]) &&
                            (row2["Side"].ToString() == "2"))
                        {
                            bvExist = true;

                            row2["Probe"] = row1["Probe"];
                            row2["Depth"] = row1["Depth"];
                            row2["Gain"] = row1["Gain"];
                            row2["Thump"] = row1["Thump"];
                            row2["Width"] = row1["Width"];
                            row2["Power"] = row1["Power"];
                            row2["MinFrequency"] = row1["MinFrequency"];
                            row2["MaxFrequency"] = row1["MaxFrequency"];
                            row2["Units"] = row1["Units"];
                            row2["ZeroLinePosition"] = row1["ZeroLinePosition"];
                            row2["DirectionToProbe"] = row1["DirectionToProbe"];
                            row2["Comments"] = row1["Comments"];
                            row2["DisplayPeakFwdEnvelope"] = row1["DisplayPeakFwdEnvelope"];
                            row2["DisplayPeakBckEnvelope"] = row1["DisplayPeakBckEnvelope"];
                            row2["DisplayMeanFwdEnvelope"] = row1["DisplayMeanFwdEnvelope"];
                            row2["DisplayMeanBckEnvelope"] = row1["DisplayMeanBckEnvelope"];
                            row2["UseEnvelopesForRCPDisplay"] = row1["UseEnvelopesForRCPDisplay"];
                            row2["DisplaySecptrum"] = row1["DisplaySecptrum"];
                            row2["HitsThreshold"] = row1["HitsThreshold"];
                            row2["Angle"] = row1["Angle"];
                            row2["FreqRang"] = row1["FreqRang"];
                            row2["FreqRang"] = row1["FreqRang"];
                            //row2["Side"] = row1["Side"];
                            row2["Location"] = row1["Location"];
                        }
                    }

                    // the bv in dsBVSetup1 is not exist in dsBVSetup2 -> so, create it,
                    // if it is bothside create 2 BV.
                    if (!bvExist)
                    {
                        bool createAnotherOne = false;

                        DB.dsBVSetup.tb_BloodVesselSetupRow row2 = (DB.dsBVSetup.tb_BloodVesselSetupRow)dsBVSetup2.tb_BloodVesselSetup.NewRow();
                        row2["Blood_Vessel_Index"] = System.Guid.NewGuid();
                        row2["Name"] = row1["Name"];
                        row2["Probe"] = row1["Probe"];
                        row2["Depth"] = row1["Depth"];
                        row2["Gain"] = row1["Gain"];
                        row2["Thump"] = row1["Thump"];
                        row2["Width"] = row1["Width"];
                        row2["Power"] = row1["Power"];
                        row2["MinFrequency"] = row1["MinFrequency"];
                        row2["MaxFrequency"] = row1["MaxFrequency"];
                        row2["Units"] = row1["Units"];
                        row2["ZeroLinePosition"] = row1["ZeroLinePosition"];
                        row2["DirectionToProbe"] = row1["DirectionToProbe"];
                        row2["Comments"] = row1["Comments"];
                        row2["DisplayPeakFwdEnvelope"] = row1["DisplayPeakFwdEnvelope"];
                        row2["DisplayPeakBckEnvelope"] = row1["DisplayPeakBckEnvelope"];
                        row2["DisplayMeanFwdEnvelope"] = row1["DisplayMeanFwdEnvelope"];
                        row2["DisplayMeanBckEnvelope"] = row1["DisplayMeanBckEnvelope"];
                        row2["UseEnvelopesForRCPDisplay"] = row1["UseEnvelopesForRCPDisplay"];
                        row2["DisplaySecptrum"] = row1["DisplaySecptrum"];
                        row2["HitsThreshold"] = row1["HitsThreshold"];
                        row2["Angle"] = row1["Angle"];
                        row2["FreqRang"] = row1["FreqRang"];
                        row2["FreqRang"] = row1["FreqRang"];
                        row2["Location"] = row1["Location"];

                        if (((GlobalTypes.ESide)row1["Side"]) == GlobalTypes.ESide.BothSides)
                        {
                            row2["Side"] = (int)GlobalTypes.ESide.Left;
                            createAnotherOne = true;
                        }
                        else
                        {
                            row2["Side"] = row1["Side"];
                        }

                        dsBVSetup2.tb_BloodVesselSetup.Addtb_BloodVesselSetupRow(row2);
                        //							//Assaf : Automatically adding the 2mhz prob to the new BV
                        //							RimedDal.Instance.AddProbe2BV ((System.Guid)row2["Blood_Vessel_Index"], (System.Guid)row2["Probe"],0);

                        if (createAnotherOne)
                        {
                            row2 = (DB.dsBVSetup.tb_BloodVesselSetupRow)dsBVSetup2.tb_BloodVesselSetup.NewRow();
                            row2["Blood_Vessel_Index"] = System.Guid.NewGuid();
                            row2["Name"] = row1["Name"];
                            row2["Probe"] = row1["Probe"];
                            row2["Depth"] = row1["Depth"];
                            row2["Gain"] = row1["Gain"];
                            row2["Thump"] = row1["Thump"];
                            row2["Width"] = row1["Width"];
                            row2["Power"] = row1["Power"];
                            row2["MinFrequency"] = row1["MinFrequency"];
                            row2["MaxFrequency"] = row1["MaxFrequency"];
                            row2["Units"] = row1["Units"];
                            row2["ZeroLinePosition"] = row1["ZeroLinePosition"];
                            row2["DirectionToProbe"] = row1["DirectionToProbe"];
                            row2["Comments"] = row1["Comments"];
                            row2["DisplayPeakFwdEnvelope"] = row1["DisplayPeakFwdEnvelope"];
                            row2["DisplayPeakBckEnvelope"] = row1["DisplayPeakBckEnvelope"];
                            row2["DisplayMeanFwdEnvelope"] = row1["DisplayMeanFwdEnvelope"];
                            row2["DisplayMeanBckEnvelope"] = row1["DisplayMeanBckEnvelope"];
                            row2["UseEnvelopesForRCPDisplay"] = row1["UseEnvelopesForRCPDisplay"];
                            row2["DisplaySecptrum"] = row1["DisplaySecptrum"];
                            row2["HitsThreshold"] = row1["HitsThreshold"];
                            row2["Angle"] = row1["Angle"];
                            row2["FreqRang"] = row1["FreqRang"];
                            row2["FreqRang"] = row1["FreqRang"];
                            row2["Location"] = row1["Location"];
                            row2["Side"] = (int)GlobalTypes.ESide.Right;
                            //if(((int)row2["Side"] != (int)GlobalTypes.TSide.eMidle))
                            dsBVSetup2.tb_BloodVesselSetup.Addtb_BloodVesselSetupRow(row2);
                            createAnotherOne = false;
                        }
                    }
                    bvExist = false;
                }

                RimedDal.Instance.s_dsBVSetup.Merge(dsBVSetup2);
            }

            return true;
        }

        private void VerifyIfThereAreRowsToDelete()
        {
            // TODO: implement the case the user delete BV from dsSetup1.
            //Check for Delete rows				
            bool bvErased = true;
            for (int i = 0; i < dsBVSetup2.tb_BloodVesselSetup.Rows.Count; i++)
            {
                bvErased = true;
                DB.dsBVSetup.tb_BloodVesselSetupRow row2 = (DB.dsBVSetup.tb_BloodVesselSetupRow)this.dsBVSetup2.tb_BloodVesselSetup.Rows[i];
                // go thrigh all the rows in dsBVSetup:
                //	if the bv exist copy from dsBVSetup1 row1 to  dsBVSetup2 row2
                // continue search after anothe bv with the same name and other side.
                for (int q = 0; q < this.dsBVSetup1.tb_BloodVesselSetup.Rows.Count; q++)
                {
                    DB.dsBVSetup.tb_BloodVesselSetupRow row1 = (DB.dsBVSetup.tb_BloodVesselSetupRow)this.dsBVSetup1.tb_BloodVesselSetup.Rows[q];

                    if ((String)row2["Name"] == (String)row1["Name"])
                    {
                        if ((int)row2["Side"] == (int)row1["Side"] || (int)row1["Side"] == (int)GlobalTypes.ESide.BothSides)
                        {
                            bvErased = false;
                            break;
                        }
                    }
                }

                if (bvErased)
                {
                    bvErased = false;
                    this.dsBVSetup2.tb_BloodVesselSetup.Removetb_BloodVesselSetupRow(row2);
                }
            }
        }

        private void UpdateProbesComboBox()
        {
            int pos = this.gridDataBoundGrid1.BindingContext[this.gridDataBoundGrid1.DataSource, this.gridDataBoundGrid1.DataMember].Position;
            this.m_selectedBVIndex = pos;

            this.m_probeComboBoxes.Clear();
            this.comboBoxProbe.Items.Clear();

            // get the row of the selected BV.
            DB.dsBVSetup.tb_BloodVesselSetupRow row = (DB.dsBVSetup.tb_BloodVesselSetupRow)this.dsBVSetup1.tb_BloodVesselSetup.Rows[m_selectedBVIndex];

            Guid BVGuid = (Guid)row["Blood_Vessel_Index"];

            DataView dv = RimedDal.Instance.GetProbesByBVIndex(BVGuid);
            if (dv.Count == 0)
            {
                this.comboBoxProbe.Items.Add("Add...");
                return;
            }

            foreach (DataRowView r in dv)
            {
                String name = RimedDal.Instance.GetProbeName((Guid)r["Probe_Index"]);
                ProbeComboBox tmp = new ProbeComboBox((Guid)r["Probe_Index"], name);
                this.m_probeComboBoxes.Add(tmp);
            }

            if (this.m_probeComboBoxes.Count == 0)
                return;

            int selectedItem = 0;
            for (int i = 0; i < this.m_probeComboBoxes.Count; i++)
            {
                this.comboBoxProbe.Items.Add(this.m_probeComboBoxes[i]);
                if (this.m_probeComboBoxes[i].ProbeIndex == (Guid)row["Probe"])
                    selectedItem = i;
            }
            // add "Add probe" item.
            this.comboBoxProbe.Items.Add("Add...");

            this.comboBoxProbe.SelectedIndex = selectedItem;
        }

        private void EnablePWCtrls(bool enable)
        {
            this.numericUpDownExtDepth.Enabled = enable;
            this.lbDepth.Enabled = enable;

            this.comboBoxPower.Enabled = enable;
            this.lbPower.Enabled = enable;

            this.numericUpDownExtWidth.Enabled = enable;
            this.lbSampleVolume.Enabled = enable;
        }

        /// <summary>this function gets the max range with dependency on the depth value.</summary>
        /// <returns></returns>
        private int GetMaxRange()
        {
            var selIndex = this.comboBoxProbe.SelectedIndex;

			//Ofer: Exception: System.ArgumentOutOfRangeException: InvalidArgument=Value of '-1' is not valid for 'index'. Parameter name: index
            if (selIndex < 0) //Non selected 
                return 32;

            try
            {
                var probeId = ((ProbeComboBox)this.comboBoxProbe.Items[selIndex]).ProbeIndex;
                Probe p = ProbesCollection.Instance.GetProbeById(probeId);
                if (p.Freq_MHz == 16 || p.Freq_MHz == 20)
                    return 32;

                // probes 1, 2. 4, 8.
                if (numericUpDownExtDepth.Value > 128)
                    return (4);
               
                if (numericUpDownExtDepth.Value > 95)
                    return (6);
                
                if (numericUpDownExtDepth.Value > 64)
                    return (8);
                
                if (numericUpDownExtDepth.Value > 32)
                    return (12);
                
                if (numericUpDownExtDepth.Value > 23)
                    return (24);
               
                return (32);
            }
            catch (Exception ex)
            {
                ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
                return (32);
            }
        }

        private void UpdateRangeComboBox(int maxRange)
        {
            int curSel = 0;
            int curSelIndex = 0;
            if (maxRange == -1)
                maxRange = GetMaxRange();
            else
            {
                // get the currunt selection.
                curSel = (int)this.comboBoxFreqRange.SelectedItem;
                curSelIndex = this.comboBoxFreqRange.SelectedIndex;
            }

            this.comboBoxFreqRange.Items.Clear();
            try
            {
                int i = 0;
                for (; i < m_ranges.Length && m_ranges[i] <= maxRange; i++)
                    this.comboBoxFreqRange.Items.Add(m_ranges[i]);

                if (m_ranges[i - 1] < curSel)
                    this.comboBoxFreqRange.SelectedItem = m_ranges[i - 1];
                else
                    this.comboBoxFreqRange.SelectedItem = curSel;
            }
            catch (System.Exception ex)
            {
                ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Add BV to BV list
        /// </summary>
        private void AddBV()
        {
            ExceptionPublisherLog4Net.FormActionLog("barItemAdd", this.Name, null);
            // Get Bv name
            BvName dlg = new BvName();
            if (dlg.ShowDialog() != DialogResult.OK)
                return;

            List<LogField> fields = new List<LogField>();
            fields.Add(new LogField("textBoxBvName", dlg.NameOfBV));
            fields.Add(new LogField("radioButtonExtracranial/radioButtonIntracranial/radioButtonPeriphral", dlg.BVLocation.ToString()));
            fields.Add(new LogField("radioButtonMidle/radioButtonBothSides/radioButtonRight/radioButtonLeft", dlg.BVSide.ToString()));
            ExceptionPublisherLog4Net.FormActionLog("buttonOK", "BvName", fields);

            DB.dsBVSetup.tb_BloodVesselSetupRow row = (DB.dsBVSetup.tb_BloodVesselSetupRow)this.dsBVSetup1.tb_BloodVesselSetup.NewRow();

            // Copy the other fields from the prev BV selection.
            row.ItemArray = dsBVSetup1.tb_BloodVesselSetup[0].ItemArray;

            row["Blood_Vessel_Index"] = Guid.NewGuid();
            row["Name"] = (String)dlg.NameOfBV;
            row["Side"] = (int)dlg.BVSide;
            row["Location"] = (int)dlg.BVLocation;
            this.dsBVSetup1.tb_BloodVesselSetup.Addtb_BloodVesselSetupRow(row);
        }

        /// <summary>
        /// Delete BV from list
        /// </summary>
        private void RemoveBV()
        {
            if (MessageBox.Show(MainForm.ResourceManager.GetString("Are you sure you want to delete this blood vessel?"), "",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                return;

            int position = this.gridDataBoundGrid1.BindingContext[this.gridDataBoundGrid1.DataSource, this.gridDataBoundGrid1.DataMember].Position;

            int pos = this.dsBVSetup2.tb_BloodVesselSetup.Rows.Count;
            DB.dsBVSetup.tb_BloodVesselSetupRow row1 = (DB.dsBVSetup.tb_BloodVesselSetupRow)this.dsBVSetup1.tb_BloodVesselSetup.Rows[position];
            String eraseStr = (String)row1["Name"];

            for (int i = 0; i < this.dsBVSetup2.tb_BloodVesselSetup.Rows.Count; i++)
            {
                DB.dsBVSetup.tb_BloodVesselSetupRow row2 = (DB.dsBVSetup.tb_BloodVesselSetupRow)this.dsBVSetup2.tb_BloodVesselSetup.Rows[i];
                if ((String)row2["Name"] == eraseStr)
                    this.dsBVSetup2.tb_BloodVesselSetup.Removetb_BloodVesselSetupRow(row2);
            }

            for (int i = 0; i < this.dsBVSetup1.tb_BloodVesselSetup.Rows.Count; i++)
            {
                DB.dsBVSetup.tb_BloodVesselSetupRow row = (DB.dsBVSetup.tb_BloodVesselSetupRow)this.dsBVSetup1.tb_BloodVesselSetup.Rows[i];
                if ((String)row["Name"] == eraseStr)
                    this.dsBVSetup1.tb_BloodVesselSetup.Removetb_BloodVesselSetupRow(row);
            }

            pos = this.dsBVSetup2.tb_BloodVesselSetup.Rows.Count;

            RimedDal.Instance.UpdateDBWithDS();
            MainForm.Instance.CurrentBVNotification();
        }

        private void ValidateThresholdValue(bool isSetFocus)
        {
            //Check value of Threashould
            int val = 0;
            int.TryParse(this.textBoxHitsThreshold.Text, out val);
            if (val < 5)
            {
                this.textBoxHitsThreshold.Text = "5";
                MessageBox.Show(MainForm.ResourceManager.GetString("Valid values for threshold is between 5 and 15."));
                if (isSetFocus)
                    this.textBoxHitsThreshold.Focus();
            }
            else if (val > 15)
            {
                this.textBoxHitsThreshold.Text = "15";
                MessageBox.Show(MainForm.ResourceManager.GetString("Valid values for threshold is between 5 and 15."));
                if (isSetFocus)
                    this.textBoxHitsThreshold.Focus();
            }
        }

        #region Event Handlers

        void xpToolBar1_ButtonClick(object sender, ToolBarButtonClickEventArgs e)
        {
            switch (this.xpToolBar1.Buttons.IndexOf(e.Button))
            {
                case 0:
                    this.AddBV();
                    break;
                case 1:
                    this.RemoveBV();
                    break;
            }
            e.Button.Pushed = false;
        }

        private void pictureBoxFromProbe_Click(object sender, EventArgs e)
        {
            this.pictureBoxFromProbe.BackColor = SystemColors.InactiveCaption;
            this.pictureBoxToProbe.BackColor = GlobalSettings.BackgroundDlg;

            // This is work around in order to bind the directionToProbe column to the GUI.
            this.tmpDirection.Checked = false;
        }

        private void pictureBoxToProbe_Click(object sender, EventArgs e)
        {
            this.pictureBoxToProbe.BackColor = SystemColors.InactiveCaption;
            this.pictureBoxFromProbe.BackColor = GlobalSettings.BackgroundDlg;

            // this is work around in order to bind the directionToProbe column to the GUI.
            this.tmpDirection.Checked = true;
        }

        /// <summary>
        /// Update the probe direction pictures - depend on the check pox.
        /// </summary>
        private void tmpDirection_CheckedChanged(object sender, EventArgs e)
        {
            if (this.tmpDirection.Checked)
            {
                this.pictureBoxToProbe.BackColor = SystemColors.InactiveCaption;
                this.pictureBoxFromProbe.BackColor = GlobalSettings.BackgroundDlg;
            }
            else
            {
                this.pictureBoxFromProbe.BackColor = SystemColors.InactiveCaption;
                this.pictureBoxToProbe.BackColor = GlobalSettings.BackgroundDlg;
            }
        }

        private void gridDataBoundGrid1_Click(object sender, EventArgs e)
        {
            this.UpdateProbesComboBox();
        }

        private void numericUpDownExtDepth_Leave(object sender, EventArgs e)
        {
            // Enable the user to insert depth value from keyboard.
            int maxRange = this.GetMaxRange();
            this.UpdateRangeComboBox(maxRange);
        }

        private void comboBoxProbe_SelectedIndexChanged(object sender, EventArgs e)
        {
            int newSelIndex = this.comboBoxProbe.SelectedIndex;

            if (newSelIndex == this.comboBoxProbe.Items.Count - 1)
            {
                // enable the user to add probes to the avaliable probes list.
                ProbeListDlg dlg = new ProbeListDlg();
                DB.dsBVSetup.tb_BloodVesselSetupRow row = (DB.dsBVSetup.tb_BloodVesselSetupRow)this.dsBVSetup1.tb_BloodVesselSetup.Rows[m_selectedBVIndex];
                Guid bvGuid = (Guid)row["Blood_Vessel_Index"];

                dlg.BVIndex = bvGuid;
                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    this.UpdateProbesComboBox();
                }
            }
            else
            {
                // Get the row of the selected BV.
                DB.dsBVSetup.tb_BloodVesselSetupRow row = (DB.dsBVSetup.tb_BloodVesselSetupRow)this.dsBVSetup1.tb_BloodVesselSetup.Rows[m_selectedBVIndex];
                row["Probe"] = m_probeComboBoxes[newSelIndex].ProbeIndex;
            }

            // Disable power, width, power controls in case of CW probe.
            // get the selected Item.
            newSelIndex = this.comboBoxProbe.SelectedIndex;
            if (newSelIndex == this.comboBoxProbe.Items.Count - 1)
            {
                this.EnablePWCtrls(true);
            }
            else
            {
                Guid probeID = ((ProbeComboBox)this.comboBoxProbe.Items[newSelIndex]).ProbeIndex;
                if (ProbesCollection.Instance.IsCWProbe(probeID))
                    this.EnablePWCtrls(false);
                else
                {
                    this.EnablePWCtrls(true);
                    // Set the depth to minimum depth of the probe.
                    String filter = "ProbeIndex = '" + probeID.ToString() + "'";
                    // WWWWW TTTTT
                    DataView dv = new DataView(RimedDal.Instance.s_dsProbe.tb_Probe, filter, null, DataViewRowState.CurrentRows);
                }
            }
        }

        private void comboBoxFreqRange_Leave(object sender, EventArgs e)
        {
            //update the the dataSet concern to the range selection.
            int selBVIndex = this.gridDataBoundGrid1.BindingContext[this.gridDataBoundGrid1.DataSource, this.gridDataBoundGrid1.DataMember].Position;
            DB.dsBVSetup.tb_BloodVesselSetupRow row = (DB.dsBVSetup.tb_BloodVesselSetupRow)this.dsBVSetup1.tb_BloodVesselSetup.Rows[selBVIndex];

            row["FreqRang"] = (int)this.comboBoxFreqRange.SelectedItem;
        }

        private void textBoxHitsThreshold_MouseLeave(object sender, EventArgs e)
        {
            ValidateThresholdValue(false);
        }

        private void textBoxHitsThreshold_Leave(object sender, EventArgs e)
        {
            ValidateThresholdValue(true);
        }

        #endregion Event Handlers

    }

    /// <summary>
    /// Probe combobox
    /// </summary>
    public class ProbeComboBox
    {
        private Guid m_probeIndex;
        private String m_name;
        
        public ProbeComboBox(Guid probeIndex, String name)
        {
            this.m_probeIndex = probeIndex;
            this.m_name = name;
        }
        
        /// <summary>
        /// Probe GUID identifier
        /// </summary>
        public Guid ProbeIndex
        {
            get
            {
                return m_probeIndex;
            }
        }

        /// <summary>
        /// Probe name
        /// </summary>
        public String Name
        {
            get
            {
                return m_name;
            }
        }
    }
}
