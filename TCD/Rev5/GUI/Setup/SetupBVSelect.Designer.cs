namespace TCD2003.GUI.Setup
{
    partial class SetupBVSelect
    {
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetupBVSelect));
            this.bMoveDown = new System.Windows.Forms.Button();
            this.bMoveUp = new System.Windows.Forms.Button();
            this.bRemove = new System.Windows.Forms.Button();
            this.bAdd = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.listBoxBVOrder = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.listBoxBV = new System.Windows.Forms.ListBox();
            this.radioButtonExtracranial = new System.Windows.Forms.RadioButton();
            this.radioButtonIntracranial = new System.Windows.Forms.RadioButton();
            this.radioButtonPeriphral = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // bMoveDown
            // 
            this.bMoveDown.Enabled = false;
            this.bMoveDown.Image = ((System.Drawing.Image)(resources.GetObject("bMoveDown.Image")));
            this.bMoveDown.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.bMoveDown.Location = new System.Drawing.Point(159, 224);
            this.bMoveDown.Name = "bMoveDown";
            this.bMoveDown.Size = new System.Drawing.Size(85, 50);
            this.bMoveDown.TabIndex = 33;
            this.bMoveDown.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.bMoveDown.Click += new System.EventHandler(this.bMoveDown_Click);
            // 
            // bMoveUp
            // 
            this.bMoveUp.Enabled = false;
            this.bMoveUp.Image = ((System.Drawing.Image)(resources.GetObject("bMoveUp.Image")));
            this.bMoveUp.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.bMoveUp.Location = new System.Drawing.Point(159, 168);
            this.bMoveUp.Name = "bMoveUp";
            this.bMoveUp.Size = new System.Drawing.Size(85, 50);
            this.bMoveUp.TabIndex = 32;
            this.bMoveUp.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.bMoveUp.Click += new System.EventHandler(this.bMoveUp_Click);
            // 
            // bRemove
            // 
            this.bRemove.Enabled = false;
            this.bRemove.Image = ((System.Drawing.Image)(resources.GetObject("bRemove.Image")));
            this.bRemove.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.bRemove.Location = new System.Drawing.Point(159, 112);
            this.bRemove.Name = "bRemove";
            this.bRemove.Size = new System.Drawing.Size(85, 50);
            this.bRemove.TabIndex = 31;
            this.bRemove.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.bRemove.Click += new System.EventHandler(this.bRemove_Click);
            // 
            // bAdd
            // 
            this.bAdd.Enabled = false;
            this.bAdd.Image = ((System.Drawing.Image)(resources.GetObject("bAdd.Image")));
            this.bAdd.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.bAdd.Location = new System.Drawing.Point(159, 56);
            this.bAdd.Name = "bAdd";
            this.bAdd.Size = new System.Drawing.Size(85, 50);
            this.bAdd.TabIndex = 30;
            this.bAdd.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.bAdd.Click += new System.EventHandler(this.bAdd_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(260, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 24);
            this.label2.TabIndex = 29;
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listBoxBVOrder
            // 
            this.listBoxBVOrder.ColumnWidth = 10;
            this.listBoxBVOrder.Location = new System.Drawing.Point(260, 56);
            this.listBoxBVOrder.Name = "listBoxBVOrder";
            this.listBoxBVOrder.Size = new System.Drawing.Size(120, 225);
            this.listBoxBVOrder.TabIndex = 28;
            this.listBoxBVOrder.SelectedIndexChanged += new System.EventHandler(this.listBoxBVOrder_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(24, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(120, 21);
            this.label1.TabIndex = 27;
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listBoxBV
            // 
            this.listBoxBV.Location = new System.Drawing.Point(24, 56);
            this.listBoxBV.Name = "listBoxBV";
            this.listBoxBV.Size = new System.Drawing.Size(120, 225);
            this.listBoxBV.TabIndex = 26;
            this.listBoxBV.SelectedIndexChanged += new System.EventHandler(this.listBoxBV_SelectedIndexChanged);
            // 
            // radioButtonExtracranial
            // 
            this.radioButtonExtracranial.Location = new System.Drawing.Point(8, 8);
            this.radioButtonExtracranial.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.radioButtonExtracranial.Name = "radioButtonExtracranial";
            this.radioButtonExtracranial.Size = new System.Drawing.Size(136, 24);
            this.radioButtonExtracranial.TabIndex = 34;
            this.radioButtonExtracranial.Text = "Extracranial";
            this.radioButtonExtracranial.Click += new System.EventHandler(this.radioButtonExtracranial_Click);
            // 
            // radioButtonIntracranial
            // 
            this.radioButtonIntracranial.Location = new System.Drawing.Point(144, 8);
            this.radioButtonIntracranial.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.radioButtonIntracranial.Name = "radioButtonIntracranial";
            this.radioButtonIntracranial.Size = new System.Drawing.Size(127, 24);
            this.radioButtonIntracranial.TabIndex = 34;
            this.radioButtonIntracranial.Text = "Intracranial";
            this.radioButtonIntracranial.Click += new System.EventHandler(this.radioButtonIntracranial_Click);
            // 
            // radioButtonPeriphral
            // 
            this.radioButtonPeriphral.Location = new System.Drawing.Point(272, 8);
            this.radioButtonPeriphral.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.radioButtonPeriphral.Name = "radioButtonPeriphral";
            this.radioButtonPeriphral.Size = new System.Drawing.Size(138, 24);
            this.radioButtonPeriphral.TabIndex = 34;
            this.radioButtonPeriphral.Text = "Periphral";
            this.radioButtonPeriphral.Click += new System.EventHandler(this.radioButtonPeriphral_Click);
            // 
            // SetupBVSelect
            // 
            this.Controls.Add(this.radioButtonExtracranial);
            this.Controls.Add(this.bMoveDown);
            this.Controls.Add(this.bMoveUp);
            this.Controls.Add(this.bRemove);
            this.Controls.Add(this.bAdd);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.listBoxBVOrder);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listBoxBV);
            this.Controls.Add(this.radioButtonIntracranial);
            this.Controls.Add(this.radioButtonPeriphral);
            this.Name = "SetupBVSelect";
            this.Size = new System.Drawing.Size(424, 296);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bMoveDown;
        private System.Windows.Forms.Button bMoveUp;
        private System.Windows.Forms.Button bRemove;
        private System.Windows.Forms.Button bAdd;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.ListBox listBoxBVOrder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listBoxBV;
        private System.Windows.Forms.RadioButton radioButtonExtracranial;
        private System.Windows.Forms.RadioButton radioButtonIntracranial;
        private System.Windows.Forms.RadioButton radioButtonPeriphral;
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
    }
}