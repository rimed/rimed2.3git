using System;
using System.ComponentModel;
using System.Windows.Forms;
using TCD2003.DB;
using TCD2003.Utils;

namespace TCD2003.GUI.Setup
{
    /// <summary>
    /// Summary description for SystemSetUp.
    /// </summary>
    public partial class SetUpHits : SetUpBase
    {
        public SetUpHits()
        {
            this.InitializeComponent();
            var strRes = ResourceManagerWrapper.GetWrapper(MainForm.ResourceManager);   

            this.albHitsRate.Text = strRes.GetString("Hits Rate (per minute)");
            this.albHistogramSteps.Text = strRes.GetString("Histogram Steps (dB)");
            this.lbCaption.Text = strRes.GetString("Hits Setup");

            this.BackColor = GlobalSettings.BackgroundDlg;
        }

        public override bool Verify()
        {
            this.SetHitsProperties();

            // ... TODO
            this.BindingContext[this.dsHits1.tb_Hits].EndCurrentEdit();
            if (this.dsHits1.HasChanges())
                RimedDal.Instance.s_dsHits.Merge(dsHits1);

            return true;
        }

        private void SetUpHits_Load(object sender, EventArgs e)
        {
            this.dsHits1.Merge(RimedDal.Instance.s_dsHits);

            // 2Mhz
            this.comboBoxHitsRate.Items.Clear();
            int[] hitsRateValues =
            {
                GlobalSettings.HitsRate_1,
				GlobalSettings.HitsRate_2,
				GlobalSettings.HitsRate_3,
				GlobalSettings.HitsRate_4
            };
            this.comboBoxHitsRate.DataSource = hitsRateValues;


            int ii = this.HitsRate;
            int index = 0;
            switch (ii)
            {
                case GlobalSettings.HitsRate_1:
                    index = 0;
                    break;
                case GlobalSettings.HitsRate_2:
                    index = 1;
                    break;
                case GlobalSettings.HitsRate_3:
                    index = 2;
                    break;
                case GlobalSettings.HitsRate_4:
                    index = 3;
                    break;
                default:
                    index = 0;
                    break;
            }

            this.comboBoxHitsRate.SelectedIndex = index;
            this.textBoxStep.Text = HitsSteps.ToString();
        }

        private void SetHitsProperties()
        {
            int r = (int)this.comboBoxHitsRate.SelectedValue;
            this.HitsRate = r;

            String str = this.textBoxStep.Text;
            int s = Convert.ToInt32(str);
            this.HitsSteps = s;

            MainForm.Instance.HitsHistogram.SetHitsParameters(new HitsParameters(s, r, MainForm.Instance.GetBVThreshold()));
        }

        private void textBoxStep_Validating(object sender, CancelEventArgs e)
        {
            int i;
            try
            {
                String str = ((TextBox)sender).Text;
                i = Convert.ToInt32(str);
            }
            catch
            {
                e.Cancel = true;
                return;
            }
        }

        private int HitsRate
        {
            get
            {
                DB.dsHits.tb_HitsRow[] rows = GetRows();
                return ((DB.dsHits.tb_HitsRow)rows[0]).Rate;
            }
            set
            {
                DB.dsHits.tb_HitsRow[] rows = GetRows();
                ((DB.dsHits.tb_HitsRow)rows[0]).Rate = value;
            }
        }
        private int HitsSteps
        {
            get
            {
                DB.dsHits.tb_HitsRow[] rows = GetRows();
                return ((DB.dsHits.tb_HitsRow)rows[0]).Step;
            }
            set
            {
                DB.dsHits.tb_HitsRow[] rows = GetRows();
                ((DB.dsHits.tb_HitsRow)rows[0]).Step = value;
            }
        }

        private DB.dsHits.tb_HitsRow[] GetRows()
        {
            DB.dsHits.tb_HitsRow[] rows = (DB.dsHits.tb_HitsRow[])dsHits1.tb_Hits.Select("ID = '1'");

            if (rows.Length > 0)
                return rows;
            else
            {
                MessageBox.Show(MainForm.ResourceManager.GetString("Error in database:table HITS is missing"));
                return null;
            }
        }
    }
}
