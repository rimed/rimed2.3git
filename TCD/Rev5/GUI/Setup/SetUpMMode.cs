using TCD2003.DB;
using TCD2003.Utils;

namespace TCD2003.GUI.Setup
{
    /// <summary>
    /// Summary description for SystemSetUp.
    /// </summary>
    public partial class SetUpMMode : SetUpBase
    {
        public SetUpMMode()
        {
            this.InitializeComponent();
            this.ReloadControlLabels();
            this.BackColor = GlobalSettings.BackgroundDlg;
            this.dsConfiguation1.Merge(RimedDal.Instance.s_DS);
        }

        private void ReloadControlLabels()
        {
            var strRes = ResourceManagerWrapper.GetWrapper(MainForm.ResourceManager);   

            this.albThreshold1.Text = strRes.GetString("Threshold") + " 1";
            this.lbCaption.Text = strRes.GetString("M-mode Setup");
            this.albThreshold2.Text = strRes.GetString("Threshold") + " 2";
        }

        public override bool Verify()
        {
            this.SetMModeProperties();

            // ... TODO
            this.BindingContext[this.dsConfiguation1.tb_BasicConfiguration].EndCurrentEdit();
            if (this.dsConfiguation1.HasChanges())
            {
                RimedDal.Instance.s_DS.Merge(this.dsConfiguation1);

                DSP.Dsps.Instance[0].WriteMModeThreshold2DSP
                        (this.dsConfiguation1.tb_BasicConfiguration[0].M_ModeThreshould1,
                        this.dsConfiguation1.tb_BasicConfiguration[0].M_ModeThreshould2,
                        true);
            }
            return true;
        }

        private void SetUpMMode_Load(object sender, System.EventArgs e)
        {
            this.numericUpDownMModeThres1.Value = this.dsConfiguation1.tb_BasicConfiguration[0].M_ModeThreshould1;
            this.numericUpDownMModeThres2.Value = this.dsConfiguation1.tb_BasicConfiguration[0].M_ModeThreshould2;
        }

        private void SetMModeProperties()
        {
            this.dsConfiguation1.tb_BasicConfiguration[0].M_ModeThreshould1 = (int)this.numericUpDownMModeThres1.Value;
            this.dsConfiguation1.tb_BasicConfiguration[0].M_ModeThreshould2 = (int)this.numericUpDownMModeThres2.Value;
        }
    }
}
