namespace TCD2003.GUI.Setup
{
    partial class SetUpSystem
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);

            if ((this.m_tmpLogoFile != null) && (System.IO.File.Exists(this.m_tmpLogoFile)))
            {
                try { System.IO.File.Delete(m_tmpLogoFile); }
                catch { }
            }
        }

        #region Component Designer generated code
        
        /// <summary> 
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonBrowseLogo = new System.Windows.Forms.Button();
            this.albLogoFile = new Syncfusion.Windows.Forms.Tools.AutoLabel();
            this.pictureBoxLogo = new System.Windows.Forms.PictureBox();
            this.albNotes = new Syncfusion.Windows.Forms.Tools.AutoLabel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.dsConfiguation1 = new TCD2003.DB.dsConfiguation();
            this.albDoctorName = new Syncfusion.Windows.Forms.Tools.AutoLabel();
            this.textBoxDocName = new System.Windows.Forms.TextBox();
            this.albOperatorName = new Syncfusion.Windows.Forms.Tools.AutoLabel();
            this.textBoxOperatorName = new System.Windows.Forms.TextBox();
            this.albDepartmentName = new Syncfusion.Windows.Forms.Tools.AutoLabel();
            this.textBoxDepartmentName = new System.Windows.Forms.TextBox();
            this.albHospitalName = new Syncfusion.Windows.Forms.Tools.AutoLabel();
            this.textBoxHospName = new System.Windows.Forms.TextBox();
            this.lbCaption = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupDICOM = new System.Windows.Forms.GroupBox();
            this.btnEcho = new System.Windows.Forms.Button();
            this.ipAddressControl1 = new IPAddressControlLib.IPAddressControl();
            this.txtAETitle = new System.Windows.Forms.TextBox();
            this.txtPortNumber = new System.Windows.Forms.TextBox();
            this.lblAE_Address = new System.Windows.Forms.Label();
            this.lblPortNumber = new System.Windows.Forms.Label();
            this.lblIpAddress = new System.Windows.Forms.Label();
            this.lblLocalAE_Title = new System.Windows.Forms.Label();
            this.txtLocalAETitle = new System.Windows.Forms.TextBox();
            this.txtAe_address = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsConfiguation1)).BeginInit();
            this.groupDICOM.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonBrowseLogo
            // 
            this.buttonBrowseLogo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.buttonBrowseLogo.Location = new System.Drawing.Point(160, 256);
            this.buttonBrowseLogo.Name = "buttonBrowseLogo";
            this.buttonBrowseLogo.Size = new System.Drawing.Size(32, 23);
            this.buttonBrowseLogo.TabIndex = 28;
            this.buttonBrowseLogo.Text = "...";
            this.buttonBrowseLogo.Click += new System.EventHandler(this.buttonBrowseLogo_Click);
            // 
            // albLogoFile
            // 
            this.albLogoFile.DX = -68;
            this.albLogoFile.DY = 11;
            this.albLogoFile.LabeledControl = this.pictureBoxLogo;
            this.albLogoFile.Location = new System.Drawing.Point(28, 259);
            this.albLogoFile.Name = "albLogoFile";
            this.albLogoFile.Size = new System.Drawing.Size(64, 28);
            this.albLogoFile.TabIndex = 27;
            this.albLogoFile.Text = "Logo File";
            this.albLogoFile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBoxLogo
            // 
            this.pictureBoxLogo.Location = new System.Drawing.Point(96, 248);
            this.pictureBoxLogo.Name = "pictureBoxLogo";
            this.pictureBoxLogo.Size = new System.Drawing.Size(50, 50);
            this.pictureBoxLogo.TabIndex = 26;
            this.pictureBoxLogo.TabStop = false;
            // 
            // albNotes
            // 
            this.albNotes.DX = -132;
            this.albNotes.DY = 16;
            this.albNotes.LabeledControl = this.textBox1;
            this.albNotes.Location = new System.Drawing.Point(27, 192);
            this.albNotes.Name = "albNotes";
            this.albNotes.Size = new System.Drawing.Size(128, 16);
            this.albNotes.TabIndex = 25;
            this.albNotes.Text = "Notes";
            this.albNotes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox1
            // 
            this.textBox1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsConfiguation1, "tb_HospitalDetails.Notes", true));
            this.textBox1.Location = new System.Drawing.Point(159, 176);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(272, 48);
            this.textBox1.TabIndex = 24;
            // 
            // dsConfiguation1
            // 
            this.dsConfiguation1.DataSetName = "dsConfiguation";
            this.dsConfiguation1.Locale = new System.Globalization.CultureInfo("en-US");
            this.dsConfiguation1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // albDoctorName
            // 
            this.albDoctorName.DX = -132;
            this.albDoctorName.DY = 2;
            this.albDoctorName.LabeledControl = this.textBoxDocName;
            this.albDoctorName.Location = new System.Drawing.Point(28, 106);
            this.albDoctorName.Name = "albDoctorName";
            this.albDoctorName.Size = new System.Drawing.Size(128, 16);
            this.albDoctorName.TabIndex = 23;
            this.albDoctorName.Text = "Doctor Name";
            this.albDoctorName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxDocName
            // 
            this.textBoxDocName.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsConfiguation1, "tb_HospitalDetails.Doctor_Name", true));
            this.textBoxDocName.Location = new System.Drawing.Point(160, 104);
            this.textBoxDocName.Name = "textBoxDocName";
            this.textBoxDocName.Size = new System.Drawing.Size(272, 20);
            this.textBoxDocName.TabIndex = 22;
            // 
            // albOperatorName
            // 
            this.albOperatorName.DX = -132;
            this.albOperatorName.DY = 2;
            this.albOperatorName.LabeledControl = this.textBoxOperatorName;
            this.albOperatorName.Location = new System.Drawing.Point(28, 146);
            this.albOperatorName.Name = "albOperatorName";
            this.albOperatorName.Size = new System.Drawing.Size(128, 16);
            this.albOperatorName.TabIndex = 21;
            this.albOperatorName.Text = "Operator Name";
            this.albOperatorName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxOperatorName
            // 
            this.textBoxOperatorName.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsConfiguation1, "tb_HospitalDetails.Operator_Name", true));
            this.textBoxOperatorName.Location = new System.Drawing.Point(160, 144);
            this.textBoxOperatorName.Name = "textBoxOperatorName";
            this.textBoxOperatorName.Size = new System.Drawing.Size(272, 20);
            this.textBoxOperatorName.TabIndex = 20;
            // 
            // albDepartmentName
            // 
            this.albDepartmentName.DX = -132;
            this.albDepartmentName.DY = 2;
            this.albDepartmentName.LabeledControl = this.textBoxDepartmentName;
            this.albDepartmentName.Location = new System.Drawing.Point(28, 74);
            this.albDepartmentName.Name = "albDepartmentName";
            this.albDepartmentName.Size = new System.Drawing.Size(128, 16);
            this.albDepartmentName.TabIndex = 19;
            this.albDepartmentName.Text = "Department Name";
            this.albDepartmentName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxDepartmentName
            // 
            this.textBoxDepartmentName.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsConfiguation1, "tb_HospitalDetails.Department_Name", true));
            this.textBoxDepartmentName.Location = new System.Drawing.Point(160, 72);
            this.textBoxDepartmentName.Name = "textBoxDepartmentName";
            this.textBoxDepartmentName.Size = new System.Drawing.Size(272, 20);
            this.textBoxDepartmentName.TabIndex = 18;
            // 
            // albHospitalName
            // 
            this.albHospitalName.DX = -132;
            this.albHospitalName.DY = -8;
            this.albHospitalName.LabeledControl = this.textBoxHospName;
            this.albHospitalName.Location = new System.Drawing.Point(28, 27);
            this.albHospitalName.Name = "albHospitalName";
            this.albHospitalName.Size = new System.Drawing.Size(128, 37);
            this.albHospitalName.TabIndex = 17;
            this.albHospitalName.Text = "Hospital/Institute Name";
            this.albHospitalName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxHospName
            // 
            this.textBoxHospName.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsConfiguation1, "tb_HospitalDetails.Hospital_Name", true));
            this.textBoxHospName.Location = new System.Drawing.Point(160, 35);
            this.textBoxHospName.Name = "textBoxHospName";
            this.textBoxHospName.Size = new System.Drawing.Size(272, 20);
            this.textBoxHospName.TabIndex = 16;
            // 
            // lbCaption
            // 
            this.lbCaption.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbCaption.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.lbCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lbCaption.Location = new System.Drawing.Point(40, 5);
            this.lbCaption.Name = "lbCaption";
            this.lbCaption.Size = new System.Drawing.Size(376, 19);
            this.lbCaption.TabIndex = 15;
            this.lbCaption.Text = "Hospital Details";
            this.lbCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "Image Files|*.jpg";
            // 
            // groupDICOM
            // 
            this.groupDICOM.Controls.Add(this.btnEcho);
            this.groupDICOM.Controls.Add(this.ipAddressControl1);
            this.groupDICOM.Controls.Add(this.txtAETitle);
            this.groupDICOM.Controls.Add(this.txtPortNumber);
            this.groupDICOM.Controls.Add(this.lblAE_Address);
            this.groupDICOM.Controls.Add(this.lblPortNumber);
            this.groupDICOM.Controls.Add(this.lblIpAddress);
            this.groupDICOM.Controls.Add(this.lblLocalAE_Title);
            this.groupDICOM.Controls.Add(this.txtLocalAETitle);
            this.groupDICOM.Location = new System.Drawing.Point(206, 229);
            this.groupDICOM.Name = "groupDICOM";
            this.groupDICOM.Size = new System.Drawing.Size(224, 110);
            this.groupDICOM.TabIndex = 29;
            this.groupDICOM.TabStop = false;
            // 
            // btnEcho
            // 
            this.btnEcho.Location = new System.Drawing.Point(160, 88);
            this.btnEcho.Name = "btnEcho";
            this.btnEcho.Size = new System.Drawing.Size(56, 20);
            this.btnEcho.TabIndex = 9;
            this.btnEcho.Text = "ECHO";
            this.btnEcho.Click += new System.EventHandler(this.btnEcho_Click);
            // 
            // ipAddressControl1
            // 
            this.ipAddressControl1.AutoHeight = true;
            this.ipAddressControl1.BackColor = System.Drawing.SystemColors.Window;
            this.ipAddressControl1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ipAddressControl1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsConfiguation1, "tb_HospitalDetails.IP_Address", true));
            this.ipAddressControl1.Location = new System.Drawing.Point(72, 16);
            this.ipAddressControl1.Name = "ipAddressControl1";
            this.ipAddressControl1.ReadOnly = false;
            this.ipAddressControl1.Size = new System.Drawing.Size(144, 20);
            this.ipAddressControl1.TabIndex = 6;
            this.ipAddressControl1.Text = "...";
            // 
            // txtAETitle
            // 
            this.txtAETitle.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsConfiguation1, "tb_HospitalDetails.AE_Title", true));
            this.txtAETitle.Location = new System.Drawing.Point(72, 64);
            this.txtAETitle.Name = "txtAETitle";
            this.txtAETitle.Size = new System.Drawing.Size(144, 20);
            this.txtAETitle.TabIndex = 5;
            // 
            // txtPortNumber
            // 
            this.txtPortNumber.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsConfiguation1, "tb_HospitalDetails.Port_Number", true));
            this.txtPortNumber.Location = new System.Drawing.Point(72, 40);
            this.txtPortNumber.Name = "txtPortNumber";
            this.txtPortNumber.Size = new System.Drawing.Size(144, 20);
            this.txtPortNumber.TabIndex = 4;
            // 
            // lblAE_Address
            // 
            this.lblAE_Address.Location = new System.Drawing.Point(2, 69);
            this.lblAE_Address.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.lblAE_Address.Name = "lblAE_Address";
            this.lblAE_Address.Size = new System.Drawing.Size(74, 17);
            this.lblAE_Address.TabIndex = 2;
            this.lblAE_Address.Text = "AE Title";
            // 
            // lblPortNumber
            // 
            this.lblPortNumber.Location = new System.Drawing.Point(2, 45);
            this.lblPortNumber.Name = "lblPortNumber";
            this.lblPortNumber.Size = new System.Drawing.Size(72, 24);
            this.lblPortNumber.TabIndex = 1;
            this.lblPortNumber.Text = "Port Number";
            // 
            // lblIpAddress
            // 
            this.lblIpAddress.Location = new System.Drawing.Point(2, 20);
            this.lblIpAddress.Name = "lblIpAddress";
            this.lblIpAddress.Size = new System.Drawing.Size(64, 16);
            this.lblIpAddress.TabIndex = 0;
            this.lblIpAddress.Text = "IP Address";
            // 
            // lblLocalAE_Title
            // 
            this.lblLocalAE_Title.Location = new System.Drawing.Point(2, 91);
            this.lblLocalAE_Title.Name = "lblLocalAE_Title";
            this.lblLocalAE_Title.Size = new System.Drawing.Size(64, 16);
            this.lblLocalAE_Title.TabIndex = 8;
            this.lblLocalAE_Title.Text = "Local AE";
            // 
            // txtLocalAETitle
            // 
            this.txtLocalAETitle.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsConfiguation1, "tb_HospitalDetails.localAE_Title", true));
            this.txtLocalAETitle.Location = new System.Drawing.Point(72, 88);
            this.txtLocalAETitle.Name = "txtLocalAETitle";
            this.txtLocalAETitle.Size = new System.Drawing.Size(80, 20);
            this.txtLocalAETitle.TabIndex = 7;
            // 
            // txtAe_address
            // 
            this.txtAe_address.Location = new System.Drawing.Point(0, 0);
            this.txtAe_address.Name = "txtAe_address";
            this.txtAe_address.Size = new System.Drawing.Size(100, 20);
            this.txtAe_address.TabIndex = 0;
            // 
            // SetUpSystem
            // 
            this.Controls.Add(this.groupDICOM);
            this.Controls.Add(this.buttonBrowseLogo);
            this.Controls.Add(this.albLogoFile);
            this.Controls.Add(this.pictureBoxLogo);
            this.Controls.Add(this.albNotes);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.albDoctorName);
            this.Controls.Add(this.textBoxDocName);
            this.Controls.Add(this.albOperatorName);
            this.Controls.Add(this.textBoxOperatorName);
            this.Controls.Add(this.albDepartmentName);
            this.Controls.Add(this.textBoxDepartmentName);
            this.Controls.Add(this.albHospitalName);
            this.Controls.Add(this.textBoxHospName);
            this.Controls.Add(this.lbCaption);
            this.Name = "SetUpSystem";
            this.Load += new System.EventHandler(this.SetUpSystem_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsConfiguation1)).EndInit();
            this.groupDICOM.ResumeLayout(false);
            this.groupDICOM.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        
        #endregion

        private System.Windows.Forms.Button buttonBrowseLogo;
        private Syncfusion.Windows.Forms.Tools.AutoLabel albLogoFile;
        private System.Windows.Forms.PictureBox pictureBoxLogo;
        private Syncfusion.Windows.Forms.Tools.AutoLabel albNotes;
        private System.Windows.Forms.TextBox textBox1;
        private Syncfusion.Windows.Forms.Tools.AutoLabel albDoctorName;
        private System.Windows.Forms.TextBox textBoxDocName;
        private Syncfusion.Windows.Forms.Tools.AutoLabel albOperatorName;
        private System.Windows.Forms.TextBox textBoxOperatorName;
        private Syncfusion.Windows.Forms.Tools.AutoLabel albDepartmentName;
        private System.Windows.Forms.TextBox textBoxDepartmentName;
        private Syncfusion.Windows.Forms.Tools.AutoLabel albHospitalName;
        private System.Windows.Forms.TextBox textBoxHospName;
        private System.Windows.Forms.Label lbCaption;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private TCD2003.DB.dsConfiguation dsConfiguation1;
        private System.Windows.Forms.GroupBox groupDICOM;
        private System.Windows.Forms.Label lblIpAddress;
        private System.Windows.Forms.Label lblPortNumber;
        private System.Windows.Forms.Label lblAE_Address;
        private System.Windows.Forms.TextBox txtPortNumber;
        private System.Windows.Forms.TextBox txtAe_address;
        private System.Windows.Forms.TextBox txtAETitle;
        private System.Windows.Forms.TextBox txtLocalAETitle;
        private IPAddressControlLib.IPAddressControl ipAddressControl1;
        private System.Windows.Forms.Label lblLocalAE_Title;
        private System.Windows.Forms.Button btnEcho;
        private System.ComponentModel.Container components = null;
    }
}