namespace TCD2003.GUI.Setup
{
    partial class SetupTrendsExport
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbExport = new System.Windows.Forms.Label();
            this.cbExportInterval = new System.Windows.Forms.ComboBox();
            this.lbHeader = new System.Windows.Forms.Label();
            this.lbSeparator = new System.Windows.Forms.Label();
            this.cbSeparator = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // lbExport
            // 
            this.lbExport.Location = new System.Drawing.Point(36, 83);
            this.lbExport.Name = "lbExport";
            this.lbExport.Size = new System.Drawing.Size(273, 23);
            this.lbExport.TabIndex = 0;
            this.lbExport.Text = "Export Clinical Parameters to Excel every:";
            // 
            // cbExportInterval
            // 
            this.cbExportInterval.Location = new System.Drawing.Point(299, 80);
            this.cbExportInterval.Name = "cbExportInterval";
            this.cbExportInterval.Size = new System.Drawing.Size(117, 21);
            this.cbExportInterval.TabIndex = 1;
            this.cbExportInterval.SelectedIndexChanged += new System.EventHandler(this.cbExportInterval_SelectedIndexChanged);
            // 
            // lbHeader
            // 
            this.lbHeader.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbHeader.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.lbHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lbHeader.Location = new System.Drawing.Point(40, 16);
            this.lbHeader.Name = "lbHeader";
            this.lbHeader.Size = new System.Drawing.Size(376, 24);
            this.lbHeader.TabIndex = 2;
            this.lbHeader.Text = "Export trends to Excel";
            this.lbHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbSeparator
            // 
            this.lbSeparator.Location = new System.Drawing.Point(32, 155);
            this.lbSeparator.Name = "lbSeparator";
            this.lbSeparator.Size = new System.Drawing.Size(240, 23);
            this.lbSeparator.TabIndex = 3;
            this.lbSeparator.Text = "Field separator:";
            // 
            // cbSeparator
            // 
            this.cbSeparator.Location = new System.Drawing.Point(299, 152);
            this.cbSeparator.Name = "cbSeparator";
            this.cbSeparator.Size = new System.Drawing.Size(117, 21);
            this.cbSeparator.TabIndex = 4;
            this.cbSeparator.SelectedIndexChanged += new System.EventHandler(this.cbSeparator_SelectedIndexChanged);
            // 
            // SetupTrendsExport
            // 
            this.Controls.Add(this.cbSeparator);
            this.Controls.Add(this.lbSeparator);
            this.Controls.Add(this.lbHeader);
            this.Controls.Add(this.cbExportInterval);
            this.Controls.Add(this.lbExport);
            this.Name = "SetupTrendsExport";
            this.Load += new System.EventHandler(this.SetupTrendsExport_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbExportInterval;
        private System.Windows.Forms.Label lbExport;
        private System.Windows.Forms.Label lbHeader;
        private System.Windows.Forms.Label lbSeparator;
        private System.Windows.Forms.ComboBox cbSeparator;
        private System.ComponentModel.IContainer components = null;
    }
}