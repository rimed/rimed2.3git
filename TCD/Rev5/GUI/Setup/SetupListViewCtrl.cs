
using TCD2003.Utils;

namespace TCD2003.GUI.Setup
{
    public partial class SetupListViewCtrl : SetUpBase
    {
        private bool m_restoreManufacturerDefaults = false;

        public SetupListViewCtrl()
        {
            this.InitializeComponent();
            this.ReloadControlLabels();
            this.BackColor = GlobalSettings.BackgroundDlg;
        }

        private void ReloadControlLabels()
        {
            var strRes = ResourceManagerWrapper.GetWrapper(MainForm.ResourceManager);   

            this.lbCaption.Text = strRes.GetString("Empty Form");
            this.labelTextWnd.Text = strRes.GetString("In the selected item there is nothing to do, please select sub item in order to change the configuration of the system.");
            this.chkBoxRestoreManufacturerDefaults.Text = strRes.GetString("Restore Manufacturer Defaults");
        }

        private void chkBoxRestoreManufacturerDefaults_CheckedChanged(object sender, System.EventArgs e)
        {
            this.m_restoreManufacturerDefaults = chkBoxRestoreManufacturerDefaults.Checked;
        }

        public bool RestoreManufacturerDefaults()
        {
            return this.m_restoreManufacturerDefaults;
        }

        public string Caption
        {
            set
            {
                this.lbCaption.Text = value;
            }
        }
    }
}

