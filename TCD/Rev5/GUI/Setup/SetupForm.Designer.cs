namespace TCD2003.GUI.Setup
{
    partial class SetupForm
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(SetupForm));
            this.treeViewPages = new System.Windows.Forms.TreeView();
            this.imageListTree = new System.Windows.Forms.ImageList(this.components);
            this.barItemAdd = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemDelete = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.cardLayout1 = new Syncfusion.Windows.Forms.Tools.CardLayout(this.components);
            this.panelCardsContainer = new System.Windows.Forms.Panel();
            this.panelStudySetup = new System.Windows.Forms.Panel();
            this.setupStudies1 = new SetupStudies();
            this.panelEvents = new System.Windows.Forms.Panel();
            this.setUpEvents1 = new SetUpEvents();
            this.panelRecording = new System.Windows.Forms.Panel();
            this.setUpRecording1 = new SetUpRecording();
            this.panelSummaryScreen = new System.Windows.Forms.Panel();
            this.setupSummaryScreen = new SetupSummaryScreen();
            this.panelBasicConfiguration = new System.Windows.Forms.Panel();
            this.setUpBasicConfiguration1 = new SetUpBasicConfiguration();
            this.panelNextFunction = new System.Windows.Forms.Panel();
            this.setUpNextFunction1 = new SetUpNextFunction();
            this.panelBVList = new System.Windows.Forms.Panel();
            this.panelSysSetup = new System.Windows.Forms.Panel();
            this.setUpSystem1 = new SetUpSystem();
            this.panelHitsSetup = new System.Windows.Forms.Panel();
            this.setUpHits1 = new SetUpHits();
            this.panelMModeSetup = new System.Windows.Forms.Panel();
            this.setUpMMode1 = new SetUpMMode();
            this.panelExtChannelConfig = new System.Windows.Forms.Panel();
            this.setUpExternChannelConfig = new SetUpExternChannelConfig();
            this.panelPrinterSetup = new System.Windows.Forms.Panel();
            this.setupPrinter1 = new SetupPrinter();
            this.panelTrendsExportSetup = new System.Windows.Forms.Panel();
            this.setupTrendsExport = new SetupTrendsExport();
            this.panelListViewCtrl = new System.Windows.Forms.Panel();
            this.setupListViewCtrl1 = new SetupListViewCtrl();
            this.setUpBloodVessels1 = new SetUpBloodVessels();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.cardLayout1)).BeginInit();
            this.panelCardsContainer.SuspendLayout();
            this.panelStudySetup.SuspendLayout();
            this.panelEvents.SuspendLayout();
            this.panelRecording.SuspendLayout();
            this.panelSummaryScreen.SuspendLayout();
            this.panelHitsSetup.SuspendLayout();
            this.panelMModeSetup.SuspendLayout();
            this.panelBasicConfiguration.SuspendLayout();
            this.panelNextFunction.SuspendLayout();
            this.panelSysSetup.SuspendLayout();
            this.panelExtChannelConfig.SuspendLayout();
            this.panelPrinterSetup.SuspendLayout();
            this.panelListViewCtrl.SuspendLayout();
            this.panelTrendsExportSetup.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeViewPages
            // 
            this.treeViewPages.Dock = System.Windows.Forms.DockStyle.Left;
            this.treeViewPages.ImageList = this.imageListTree;
            this.treeViewPages.Location = new System.Drawing.Point(0, 0);
            this.treeViewPages.Name = "treeViewPages";
            this.treeViewPages.Size = new System.Drawing.Size(168, 384);
            this.treeViewPages.TabIndex = 0;
            this.treeViewPages.AfterExpand += new System.Windows.Forms.TreeViewEventHandler(this.treeViewPages_AfterExpand);
            this.treeViewPages.AfterCollapse += new System.Windows.Forms.TreeViewEventHandler(this.treeViewPages_AfterCollapse);
            this.treeViewPages.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeViewPages_AfterSelect);
            this.treeViewPages.Enter += new System.EventHandler(this.treeViewPages_Enter);
            // 
            // imageListTree
            // 
            this.imageListTree.ImageSize = new System.Drawing.Size(16, 16);
            this.imageListTree.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListTree.ImageStream")));
            this.imageListTree.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // barItemAdd
            // 
            this.barItemAdd.CategoryIndex = -1;
            this.barItemAdd.ID = "";
            // 
            // barItemDelete
            // 
            this.barItemDelete.CategoryIndex = -1;
            this.barItemDelete.ID = "";
            // 
            // cardLayout1
            // 
            this.cardLayout1.ContainerControl = this.panelCardsContainer;
            this.cardLayout1.LayoutMode = Syncfusion.Windows.Forms.Tools.CardLayoutMode.Fill;
            // 
            // panelCardsContainer
            // 
            this.panelCardsContainer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panelCardsContainer.Controls.Add(this.panelStudySetup);
            this.panelCardsContainer.Controls.Add(this.panelEvents);
            this.panelCardsContainer.Controls.Add(this.panelRecording);
            this.panelCardsContainer.Controls.Add(this.panelSummaryScreen);
            this.panelCardsContainer.Controls.Add(this.panelHitsSetup);
            this.panelCardsContainer.Controls.Add(this.panelMModeSetup);
            this.panelCardsContainer.Controls.Add(this.panelBasicConfiguration);
            this.panelCardsContainer.Controls.Add(this.panelNextFunction);
            this.panelCardsContainer.Controls.Add(this.panelBVList);
            this.panelCardsContainer.Controls.Add(this.panelSysSetup);
            this.panelCardsContainer.Controls.Add(this.panelExtChannelConfig);
            this.panelCardsContainer.Controls.Add(this.panelPrinterSetup);
            this.panelCardsContainer.Controls.Add(this.panelTrendsExportSetup);
            this.panelCardsContainer.Controls.Add(this.panelListViewCtrl);
            this.panelCardsContainer.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelCardsContainer.Location = new System.Drawing.Point(168, 0);
            this.panelCardsContainer.Name = "panelCardsContainer";
            this.panelCardsContainer.Size = new System.Drawing.Size(458, 344);
            this.panelCardsContainer.TabIndex = 1;
            // 
            // panelStudySetup
            // 
            this.cardLayout1.SetCardName(this.panelStudySetup, "");
            this.panelStudySetup.Controls.Add(this.setupStudies1);
            this.panelStudySetup.Location = new System.Drawing.Point(0, 0);
            this.panelStudySetup.Name = "panelStudySetup";
            this.panelStudySetup.Size = new System.Drawing.Size(454, 340);
            this.panelStudySetup.TabIndex = 8;
            // 
            // setupStudies1
            // 
            this.setupStudies1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(216)), ((System.Byte)(228)), ((System.Byte)(248)));
            this.setupStudies1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.setupStudies1.Location = new System.Drawing.Point(0, 0);
            this.setupStudies1.Name = "setupStudies1";
            this.setupStudies1.SelectedStudy = new System.Guid("00000000-0000-0000-0000-000000000000");
            this.setupStudies1.Size = new System.Drawing.Size(454, 340);
            this.setupStudies1.TabIndex = 0;
            // 
            // panelEvents
            // 
            this.cardLayout1.SetCardName(this.panelEvents, "");
            this.panelEvents.Controls.Add(this.setUpEvents1);
            this.panelEvents.Location = new System.Drawing.Point(0, 0);
            this.panelEvents.Name = "panelEvents";
            this.panelEvents.Size = new System.Drawing.Size(454, 340);
            this.panelEvents.TabIndex = 5;
            // 
            // setUpEvents1
            // 
            this.setUpEvents1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(216)), ((System.Byte)(228)), ((System.Byte)(248)));
            this.setUpEvents1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.setUpEvents1.Location = new System.Drawing.Point(0, 0);
            this.setUpEvents1.Name = "setUpEvents1";
            this.setUpEvents1.Size = new System.Drawing.Size(454, 340);
            this.setUpEvents1.TabIndex = 0;
            // 
            // panelRecording
            // 
            this.cardLayout1.SetCardName(this.panelRecording, "");
            this.panelRecording.Controls.Add(this.setUpRecording1);
            this.panelRecording.Location = new System.Drawing.Point(0, 0);
            this.panelRecording.Name = "panelRecording";
            this.panelRecording.Size = new System.Drawing.Size(454, 340);
            this.panelRecording.TabIndex = 6;
            // 
            // setUpRecording1
            // 
            this.setUpRecording1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(216)), ((System.Byte)(228)), ((System.Byte)(248)));
            this.setUpRecording1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.setUpRecording1.Location = new System.Drawing.Point(0, 0);
            this.setUpRecording1.Name = "setUpRecording1";
            this.setUpRecording1.Size = new System.Drawing.Size(454, 340);
            this.setUpRecording1.TabIndex = 0;
            // 
            // panelSummaryScreen
            // 
            this.cardLayout1.SetCardName(this.panelSummaryScreen, "");
            this.panelSummaryScreen.Controls.Add(this.setupSummaryScreen);
            this.panelSummaryScreen.Location = new System.Drawing.Point(0, 0);
            this.panelSummaryScreen.Name = "panelSummaryScreen";
            this.panelSummaryScreen.Size = new System.Drawing.Size(454, 340);
            this.panelSummaryScreen.TabIndex = 6;
            // 
            // setupSummaryScreen
            // 
            this.setupSummaryScreen.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(216)), ((System.Byte)(228)), ((System.Byte)(248)));
            this.setupSummaryScreen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.setupSummaryScreen.Location = new System.Drawing.Point(0, 0);
            this.setupSummaryScreen.Name = "setupSummaryScreen";
            this.setupSummaryScreen.Size = new System.Drawing.Size(454, 340);
            this.setupSummaryScreen.TabIndex = 0;
            // 
            // panelBasicConfiguration
            // 
            this.cardLayout1.SetCardName(this.panelBasicConfiguration, "");
            this.panelBasicConfiguration.Controls.Add(this.setUpBasicConfiguration1);
            this.panelBasicConfiguration.Location = new System.Drawing.Point(0, 0);
            this.panelBasicConfiguration.Name = "panelBasicConfiguration";
            this.panelBasicConfiguration.Size = new System.Drawing.Size(454, 340);
            this.panelBasicConfiguration.TabIndex = 4;
            // 
            // setUpBasicConfiguration1
            // 
            this.setUpBasicConfiguration1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(216)), ((System.Byte)(228)), ((System.Byte)(248)));
            this.setUpBasicConfiguration1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.setUpBasicConfiguration1.Location = new System.Drawing.Point(0, 0);
            this.setUpBasicConfiguration1.Name = "setUpBasicConfiguration1";
            this.setUpBasicConfiguration1.Size = new System.Drawing.Size(454, 340);
            this.setUpBasicConfiguration1.TabIndex = 0;
            // 
            // panelNextFunction
            // 
            this.cardLayout1.SetCardName(this.panelNextFunction, "");
            this.panelNextFunction.Controls.Add(this.setUpNextFunction1);
            this.panelNextFunction.Location = new System.Drawing.Point(0, 0);
            this.panelNextFunction.Name = "panelNextFunction";
            this.panelNextFunction.Size = new System.Drawing.Size(454, 340);
            this.panelNextFunction.TabIndex = 3;
            // 
            // setUpNextFunction1
            // 
            this.setUpNextFunction1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(216)), ((System.Byte)(228)), ((System.Byte)(248)));
            this.setUpNextFunction1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.setUpNextFunction1.Location = new System.Drawing.Point(0, 0);
            this.setUpNextFunction1.Name = "setUpNextFunction1";
            this.setUpNextFunction1.Size = new System.Drawing.Size(454, 340);
            this.setUpNextFunction1.TabIndex = 0;
            // 
            // panelBVList
            // 
            this.cardLayout1.SetCardName(this.panelBVList, "");
            this.panelBVList.Controls.Add(this.setUpBloodVessels1);
            this.panelBVList.Location = new System.Drawing.Point(0, 0);
            this.panelBVList.Name = "panelBVList";
            this.panelBVList.Size = new System.Drawing.Size(454, 340);
            this.panelBVList.TabIndex = 2;
            // 
            // panelSysSetup
            // 
            this.cardLayout1.SetCardName(this.panelSysSetup, "");
            this.panelSysSetup.Controls.Add(this.setUpSystem1);
            this.panelSysSetup.Location = new System.Drawing.Point(0, 0);
            this.cardLayout1.SetMinimumSize(this.panelSysSetup, new System.Drawing.Size(454, 340));
            this.panelSysSetup.Name = "panelSysSetup";
            this.cardLayout1.SetPreferredSize(this.panelSysSetup, new System.Drawing.Size(454, 340));
            this.panelSysSetup.Size = new System.Drawing.Size(454, 340);
            this.panelSysSetup.TabIndex = 7;
            // 
            // setUpSystem1
            // 
            this.setUpSystem1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(216)), ((System.Byte)(228)), ((System.Byte)(248)));
            this.setUpSystem1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.setUpSystem1.Location = new System.Drawing.Point(0, 0);
            this.setUpSystem1.Name = "setUpSystem1";
            this.setUpSystem1.Size = new System.Drawing.Size(454, 340);
            this.setUpSystem1.TabIndex = 0;
            // 
            // panelHitsSetup
            // 
            this.cardLayout1.SetCardName(this.panelHitsSetup, "");
            this.panelHitsSetup.Controls.Add(this.setUpHits1);
            this.panelHitsSetup.Location = new System.Drawing.Point(0, 0);
            this.cardLayout1.SetMinimumSize(this.panelHitsSetup, new System.Drawing.Size(454, 340));
            this.panelHitsSetup.Name = "panelHitsSetup";
            this.cardLayout1.SetPreferredSize(this.panelHitsSetup, new System.Drawing.Size(454, 340));
            this.panelHitsSetup.Size = new System.Drawing.Size(454, 340);
            this.panelHitsSetup.TabIndex = 27;
            // 
            // setUpHits1
            // 
            this.setUpHits1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(216)), ((System.Byte)(228)), ((System.Byte)(248)));
            this.setUpHits1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.setUpHits1.Location = new System.Drawing.Point(0, 0);
            this.setUpHits1.Name = "setUpHits1";
            this.setUpHits1.Size = new System.Drawing.Size(454, 340);
            this.setUpHits1.TabIndex = 28;
            // 
            // panelMModeSetup
            // 
            this.cardLayout1.SetCardName(this.panelMModeSetup, "");
            this.panelMModeSetup.Controls.Add(this.setUpMMode1);
            this.panelMModeSetup.Location = new System.Drawing.Point(0, 0);
            this.cardLayout1.SetMinimumSize(this.panelMModeSetup, new System.Drawing.Size(454, 340));
            this.panelMModeSetup.Name = "panelMModeSetup";
            this.cardLayout1.SetPreferredSize(this.panelMModeSetup, new System.Drawing.Size(454, 340));
            this.panelMModeSetup.Size = new System.Drawing.Size(454, 340);
            this.panelMModeSetup.TabIndex = 127;
            // 
            // setUpMMode1
            // 
            this.setUpMMode1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(216)), ((System.Byte)(228)), ((System.Byte)(248)));
            this.setUpMMode1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.setUpMMode1.Location = new System.Drawing.Point(0, 0);
            this.setUpMMode1.Name = "setUpMMode1";
            this.setUpMMode1.Size = new System.Drawing.Size(454, 340);
            this.setUpMMode1.TabIndex = 28;

            // 
            // panelExtChannelConfig
            // 
            this.cardLayout1.SetCardName(this.panelExtChannelConfig, "");
            this.panelExtChannelConfig.Controls.Add(this.setUpExternChannelConfig);
            this.panelExtChannelConfig.Location = new System.Drawing.Point(0, 0);
            this.panelExtChannelConfig.Name = "panelExtChannelConfig";
            this.panelExtChannelConfig.Size = new System.Drawing.Size(454, 340);
            this.panelExtChannelConfig.TabIndex = 7;
            // 
            // setUpExternChannelConfig
            // 
            this.setUpExternChannelConfig.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(216)), ((System.Byte)(228)), ((System.Byte)(248)));
            this.setUpExternChannelConfig.Dock = System.Windows.Forms.DockStyle.Fill;
            this.setUpExternChannelConfig.Location = new System.Drawing.Point(0, 0);
            this.setUpExternChannelConfig.Name = "setUpExternChannelConfig";
            this.setUpExternChannelConfig.Size = new System.Drawing.Size(454, 340);
            this.setUpExternChannelConfig.TabIndex = 0;
            // 
            // panelPrinterSetup
            // 
            this.cardLayout1.SetCardName(this.panelPrinterSetup, "");
            this.panelPrinterSetup.Controls.Add(this.setupPrinter1);
            this.panelPrinterSetup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelPrinterSetup.Location = new System.Drawing.Point(0, 0);
            this.cardLayout1.SetMinimumSize(this.panelPrinterSetup, new System.Drawing.Size(454, 340));
            this.panelPrinterSetup.Name = "panelPrinterSetup";
            this.cardLayout1.SetPreferredSize(this.panelPrinterSetup, new System.Drawing.Size(454, 340));
            this.panelPrinterSetup.Size = new System.Drawing.Size(454, 340);
            this.panelPrinterSetup.TabIndex = 1;
            // 
            // setupPrinter1
            // 
            this.setupPrinter1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(216)), ((System.Byte)(228)), ((System.Byte)(248)));
            this.setupPrinter1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.setupPrinter1.Location = new System.Drawing.Point(0, 0);
            this.setupPrinter1.Name = "setupPrinter1";
            this.setupPrinter1.Size = new System.Drawing.Size(454, 340);
            this.setupPrinter1.TabIndex = 0;
            // 
            // panelListViewCtrl
            // 
            this.cardLayout1.SetCardName(this.panelListViewCtrl, "");
            this.panelListViewCtrl.Controls.Add(this.setupListViewCtrl1);
            this.panelListViewCtrl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelListViewCtrl.Location = new System.Drawing.Point(0, 0);
            this.cardLayout1.SetMinimumSize(this.panelListViewCtrl, new System.Drawing.Size(454, 340));
            this.panelListViewCtrl.Name = "panelListViewCtrl";
            this.cardLayout1.SetPreferredSize(this.panelListViewCtrl, new System.Drawing.Size(454, 340));
            this.panelListViewCtrl.Size = new System.Drawing.Size(454, 340);
            this.panelListViewCtrl.TabIndex = 1;
            // 
            // panelTrendsExportSetup
            // 
            this.cardLayout1.SetCardName(this.panelTrendsExportSetup, "");
            this.panelTrendsExportSetup.Controls.Add(this.setupTrendsExport);
            this.panelTrendsExportSetup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelTrendsExportSetup.Location = new System.Drawing.Point(0, 0);
            this.cardLayout1.SetMinimumSize(this.panelTrendsExportSetup, new System.Drawing.Size(454, 340));
            this.panelTrendsExportSetup.Name = "panelTrendsExportSetup";
            this.cardLayout1.SetPreferredSize(this.panelTrendsExportSetup, new System.Drawing.Size(454, 340));
            this.panelTrendsExportSetup.Size = new System.Drawing.Size(454, 340);
            this.panelTrendsExportSetup.TabIndex = 1;
            // 
            // setupListViewCtrl1
            // 
            this.setupListViewCtrl1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(216)), ((System.Byte)(228)), ((System.Byte)(248)));
            this.setupListViewCtrl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.setupListViewCtrl1.Location = new System.Drawing.Point(0, 0);
            this.setupListViewCtrl1.Name = "setupListViewCtrl1";
            this.setupListViewCtrl1.Size = new System.Drawing.Size(454, 340);
            this.setupListViewCtrl1.TabIndex = 0;
            // 
            // setupTrendsExport
            // 
            this.setupTrendsExport.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(216)), ((System.Byte)(228)), ((System.Byte)(248)));
            this.setupTrendsExport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.setupTrendsExport.Location = new System.Drawing.Point(0, 0);
            this.setupTrendsExport.Name = "setupTrendsExport";
            this.setupTrendsExport.Size = new System.Drawing.Size(454, 340);
            this.setupTrendsExport.TabIndex = 0;
            // 
            // setUpBloodVessels1
            // 
            this.setUpBloodVessels1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(216)), ((System.Byte)(228)), ((System.Byte)(248)));
            this.setUpBloodVessels1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.setUpBloodVessels1.Location = new System.Drawing.Point(0, 0);
            this.setUpBloodVessels1.Name = "setUpBloodVessels1";
            this.setUpBloodVessels1.Size = new System.Drawing.Size(454, 340);
            this.setUpBloodVessels1.TabIndex = 1;
            // 
            // buttonOK
            // 
            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOK.Location = new System.Drawing.Point(320, 352);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.TabIndex = 2;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(416, 352);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // SetupForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(626, 384);
            this.Controls.Add(this.panelCardsContainer);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.treeViewPages);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "SetupForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.cardLayout1)).EndInit();
            this.panelCardsContainer.ResumeLayout(false);
            this.panelStudySetup.ResumeLayout(false);
            this.panelEvents.ResumeLayout(false);
            this.panelRecording.ResumeLayout(false);
            this.panelSummaryScreen.ResumeLayout(false);
            this.panelHitsSetup.ResumeLayout(false);
            this.panelMModeSetup.ResumeLayout(false);
            this.panelBasicConfiguration.ResumeLayout(false);
            this.panelNextFunction.ResumeLayout(false);
            this.panelSysSetup.ResumeLayout(false);
            this.panelExtChannelConfig.ResumeLayout(false);
            this.panelPrinterSetup.ResumeLayout(false);
            this.panelListViewCtrl.ResumeLayout(false);
            this.panelTrendsExportSetup.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        
        #endregion

        private System.Windows.Forms.TreeView treeViewPages;
        private System.Windows.Forms.ImageList imageListTree;
        private Syncfusion.Windows.Forms.Tools.CardLayout cardLayout1;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemDelete;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemAdd;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.ComponentModel.IContainer components;
        private System.Windows.Forms.Panel panelEvents;
        private SetUpEvents setUpEvents1;
        private System.Windows.Forms.Panel panelRecording;
        private SetUpRecording setUpRecording1;
        private System.Windows.Forms.Panel panelSummaryScreen;
        private SetupSummaryScreen setupSummaryScreen;
        private System.Windows.Forms.Panel panelBasicConfiguration;
        private SetUpBasicConfiguration setUpBasicConfiguration1;
        private System.Windows.Forms.Panel panelNextFunction;
        private SetUpNextFunction setUpNextFunction1;
        private System.Windows.Forms.Panel panelBVList;
        private SetUpBloodVessels setUpBloodVessels1;
        private System.Windows.Forms.Panel panelSysSetup;
        private SetUpSystem setUpSystem1;
        private System.Windows.Forms.Panel panelHitsSetup;
        private SetUpHits setUpHits1;
        private System.Windows.Forms.Panel panelMModeSetup;
        private SetUpMMode setUpMMode1;
        private System.Windows.Forms.Panel panelExtChannelConfig;
        private SetUpExternChannelConfig setUpExternChannelConfig;
        private System.Windows.Forms.Panel panelCardsContainer;
        private System.Windows.Forms.Panel panelStudySetup;
        private SetupStudies setupStudies1;
        private System.Windows.Forms.Panel panelPrinterSetup;
        private SetupPrinter setupPrinter1;
        private System.Windows.Forms.Panel panelTrendsExportSetup;
        private SetupTrendsExport setupTrendsExport;
        private System.Windows.Forms.Panel panelListViewCtrl;
        private SetupListViewCtrl setupListViewCtrl1;
    }
}