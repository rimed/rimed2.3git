namespace TCD2003.GUI
{
    partial class ClinicalBar
    {
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gradientPanel1 = new Syncfusion.Windows.Forms.Tools.GradientPanel();
            this.clinicalParamLabelRI = new TCD2003.GUI.ClinicalParamLabel();
            this.clinicalParamLabelSW = new TCD2003.GUI.ClinicalParamLabel();
            this.clinicalParamLabelSD = new TCD2003.GUI.ClinicalParamLabel();
            this.clinicalParamLabelPI = new TCD2003.GUI.ClinicalParamLabel();
            this.clinicalParamLabelDV = new TCD2003.GUI.ClinicalParamLabel();
            this.clinicalParamLabelMean = new TCD2003.GUI.ClinicalParamLabel();
            this.clinicalParamLabelPeak = new TCD2003.GUI.ClinicalParamLabel();
            this.clinicalParamLabelMode = new TCD2003.GUI.ClinicalParamLabel();
            this.clinicalParamLabelAverage = new TCD2003.GUI.ClinicalParamLabel();
            this.clinicalParamLabelHits = new TCD2003.GUI.ClinicalParamLabel();
            this.clinicalParamLabelHitsRate = new TCD2003.GUI.ClinicalParamLabel();
            this.clinicalParamLabelExt1 = new TCD2003.GUI.ClinicalParamLabel();
            this.clinicalParamLabelExt2 = new TCD2003.GUI.ClinicalParamLabel();
            this.clinicalParamLabelExt3 = new TCD2003.GUI.ClinicalParamLabel();
            this.clinicalParamLabelExt4 = new TCD2003.GUI.ClinicalParamLabel();
            this.clinicalParamLabelExt5 = new TCD2003.GUI.ClinicalParamLabel();
            this.clinicalParamLabelExt6 = new TCD2003.GUI.ClinicalParamLabel();
            this.clinicalParamLabelExt7 = new TCD2003.GUI.ClinicalParamLabel();
            this.clinicalParamLabelExt8 = new TCD2003.GUI.ClinicalParamLabel();
            this.BackgroundPanel = new Syncfusion.Windows.Forms.Tools.GradientPanel();
            this.dsClinicalParamSetup1 = new TCD2003.DB.dsClinicalParamSetup();
            this.gradientPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dsClinicalParamSetup1)).BeginInit();
            this.SuspendLayout();
            // 
            // gradientPanel1
            // 
            this.gradientPanel1.BackColor = System.Drawing.Color.Transparent;
            this.gradientPanel1.BackgroundColor = new Syncfusion.Drawing.BrushInfo(Syncfusion.Drawing.GradientStyle.Vertical, new Syncfusion.Drawing.BrushInfoColorArrayList(new System.Drawing.Color[] {
                System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255))))),
                System.Drawing.Color.Green}));
            this.gradientPanel1.Border3DStyle = System.Windows.Forms.Border3DStyle.Flat;
            this.gradientPanel1.BorderColor = System.Drawing.Color.Transparent;
            this.gradientPanel1.BorderSides = System.Windows.Forms.Border3DSide.Middle;
            this.gradientPanel1.BorderSingle = System.Windows.Forms.ButtonBorderStyle.None;
            this.gradientPanel1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gradientPanel1.Controls.Add(this.clinicalParamLabelRI);
            this.gradientPanel1.Controls.Add(this.clinicalParamLabelSW);
            this.gradientPanel1.Controls.Add(this.clinicalParamLabelSD);
            this.gradientPanel1.Controls.Add(this.clinicalParamLabelPI);
            this.gradientPanel1.Controls.Add(this.clinicalParamLabelDV);
            this.gradientPanel1.Controls.Add(this.clinicalParamLabelMean);
            this.gradientPanel1.Controls.Add(this.clinicalParamLabelPeak);
            this.gradientPanel1.Controls.Add(this.clinicalParamLabelMode);
            this.gradientPanel1.Controls.Add(this.clinicalParamLabelAverage);
            this.gradientPanel1.Controls.Add(this.clinicalParamLabelHits);
            this.gradientPanel1.Controls.Add(this.clinicalParamLabelHitsRate);
            this.gradientPanel1.Controls.Add(this.clinicalParamLabelExt1);
            this.gradientPanel1.Controls.Add(this.clinicalParamLabelExt2);
            this.gradientPanel1.Controls.Add(this.clinicalParamLabelExt3);
            this.gradientPanel1.Controls.Add(this.clinicalParamLabelExt4);
            this.gradientPanel1.Controls.Add(this.clinicalParamLabelExt5);
            this.gradientPanel1.Controls.Add(this.clinicalParamLabelExt6);
            this.gradientPanel1.Controls.Add(this.clinicalParamLabelExt7);
            this.gradientPanel1.Controls.Add(this.clinicalParamLabelExt8);
            this.gradientPanel1.Controls.Add(this.BackgroundPanel);
            this.gradientPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gradientPanel1.Location = new System.Drawing.Point(0, 0);
            this.gradientPanel1.Name = "gradientPanel1";
            this.gradientPanel1.Padding = new System.Windows.Forms.Padding(2);
            this.gradientPanel1.Size = new System.Drawing.Size(104, 699);
            this.gradientPanel1.TabIndex = 0;
            // 
            // clinicalParamLabelRI
            // 
            this.clinicalParamLabelRI.Id = "G";
            this.clinicalParamLabelRI.Location = new System.Drawing.Point(0, 480);
            this.clinicalParamLabelRI.Name = "clinicalParamLabelRI";
            this.clinicalParamLabelRI.ParamName = "RI";
            this.clinicalParamLabelRI.Size = new System.Drawing.Size(96, 76);
            this.clinicalParamLabelRI.TabIndex = 15;
            // 
            // clinicalParamLabelSW
            // 
            this.clinicalParamLabelSW.BackColor = System.Drawing.SystemColors.Control;
            this.clinicalParamLabelSW.DisplayOnlyUpperValue = true;
            this.clinicalParamLabelSW.Id = "F";
            this.clinicalParamLabelSW.Location = new System.Drawing.Point(2, 400);
            this.clinicalParamLabelSW.Name = "clinicalParamLabelSW";
            this.clinicalParamLabelSW.ParamName = "VMR";
            this.clinicalParamLabelSW.Size = new System.Drawing.Size(96, 76);
            this.clinicalParamLabelSW.TabIndex = 13;
            // 
            // clinicalParamLabelSD
            // 
            this.clinicalParamLabelSD.BackColor = System.Drawing.SystemColors.Control;
            this.clinicalParamLabelSD.Id = "E";
            this.clinicalParamLabelSD.Location = new System.Drawing.Point(2, 320);
            this.clinicalParamLabelSD.Name = "clinicalParamLabelSD";
            this.clinicalParamLabelSD.ParamName = "S/D";
            this.clinicalParamLabelSD.Size = new System.Drawing.Size(96, 76);
            this.clinicalParamLabelSD.TabIndex = 12;
            // 
            // clinicalParamLabelPI
            // 
            this.clinicalParamLabelPI.BackColor = System.Drawing.SystemColors.Control;
            this.clinicalParamLabelPI.Id = "D";
            this.clinicalParamLabelPI.Location = new System.Drawing.Point(2, 240);
            this.clinicalParamLabelPI.Name = "clinicalParamLabelPI";
            this.clinicalParamLabelPI.ParamName = "P.I.";
            this.clinicalParamLabelPI.Size = new System.Drawing.Size(96, 76);
            this.clinicalParamLabelPI.TabIndex = 11;
            // 
            // clinicalParamLabelDV
            // 
            this.clinicalParamLabelDV.BackColor = System.Drawing.SystemColors.Control;
            this.clinicalParamLabelDV.Id = "C";
            this.clinicalParamLabelDV.Location = new System.Drawing.Point(0, 160);
            this.clinicalParamLabelDV.Name = "clinicalParamLabelDV";
            this.clinicalParamLabelDV.ParamName = "DV";
            this.clinicalParamLabelDV.Size = new System.Drawing.Size(96, 76);
            this.clinicalParamLabelDV.TabIndex = 10;
            // 
            // clinicalParamLabelMean
            // 
            this.clinicalParamLabelMean.BackColor = System.Drawing.SystemColors.Control;
            this.clinicalParamLabelMean.Id = "B";
            this.clinicalParamLabelMean.Location = new System.Drawing.Point(2, 80);
            this.clinicalParamLabelMean.Name = "clinicalParamLabelMean";
            this.clinicalParamLabelMean.ParamName = "Mean";
            this.clinicalParamLabelMean.Size = new System.Drawing.Size(96, 76);
            this.clinicalParamLabelMean.TabIndex = 9;
            // 
            // clinicalParamLabelPeak
            // 
            this.clinicalParamLabelPeak.BackColor = System.Drawing.SystemColors.Control;
            this.clinicalParamLabelPeak.Id = "A";
            this.clinicalParamLabelPeak.Location = new System.Drawing.Point(2, 2);
            this.clinicalParamLabelPeak.Name = "clinicalParamLabelPeak";
            this.clinicalParamLabelPeak.ParamName = "Peak";
            this.clinicalParamLabelPeak.Size = new System.Drawing.Size(96, 76);
            this.clinicalParamLabelPeak.TabIndex = 8;
            // 
            // clinicalParamLabelMode
            // 
            this.clinicalParamLabelMode.Id = "H";
            this.clinicalParamLabelMode.Location = new System.Drawing.Point(0, 528);
            this.clinicalParamLabelMode.Name = "clinicalParamLabelMode";
            this.clinicalParamLabelMode.ParamName = "Mode";
            this.clinicalParamLabelMode.Size = new System.Drawing.Size(96, 76);
            this.clinicalParamLabelMode.TabIndex = 15;
            // 
            // clinicalParamLabelAverage
            // 
            this.clinicalParamLabelAverage.BackColor = System.Drawing.SystemColors.Control;
            this.clinicalParamLabelAverage.Id = "I";
            this.clinicalParamLabelAverage.Location = new System.Drawing.Point(0, 592);
            this.clinicalParamLabelAverage.Name = "clinicalParamLabelAverage";
            this.clinicalParamLabelAverage.ParamName = "Avrg";
            this.clinicalParamLabelAverage.Size = new System.Drawing.Size(96, 76);
            this.clinicalParamLabelAverage.TabIndex = 10;
            // 
            // clinicalParamLabelHits
            // 
            this.clinicalParamLabelHits.BackColor = System.Drawing.SystemColors.Control;
            this.clinicalParamLabelHits.DisplayOnlyUpperValue = true;
            this.clinicalParamLabelHits.Id = "J";
            this.clinicalParamLabelHits.Location = new System.Drawing.Point(0, 640);
            this.clinicalParamLabelHits.Name = "clinicalParamLabelHits";
            this.clinicalParamLabelHits.ParamName = "HITS";
            this.clinicalParamLabelHits.Size = new System.Drawing.Size(96, 76);
            this.clinicalParamLabelHits.TabIndex = 10;
            // 
            // clinicalParamLabelHitsRate
            // 
            this.clinicalParamLabelHitsRate.BackColor = System.Drawing.SystemColors.Control;
            this.clinicalParamLabelHitsRate.DisplayOnlyUpperValue = true;
            this.clinicalParamLabelHitsRate.Id = "K";
            this.clinicalParamLabelHitsRate.Location = new System.Drawing.Point(0, 640);
            this.clinicalParamLabelHitsRate.Name = "clinicalParamLabelHitsRate";
            this.clinicalParamLabelHitsRate.ParamName = "HITr";
            this.clinicalParamLabelHitsRate.Size = new System.Drawing.Size(96, 76);
            this.clinicalParamLabelHitsRate.TabIndex = 10;
            // 
            // clinicalParamLabelExt1
            // 
            this.clinicalParamLabelExt1.BackColor = System.Drawing.SystemColors.Control;
            this.clinicalParamLabelExt1.Id = "L";
            this.clinicalParamLabelExt1.Location = new System.Drawing.Point(0, 720);
            this.clinicalParamLabelExt1.LowerVar = 0D;
            this.clinicalParamLabelExt1.Name = "clinicalParamLabelExt1";
            this.clinicalParamLabelExt1.ParamName = "Ext 1";
            this.clinicalParamLabelExt1.Size = new System.Drawing.Size(96, 76);
            this.clinicalParamLabelExt1.TabIndex = 11;
            this.clinicalParamLabelExt1.UpperVar = 0D;
            // 
            // clinicalParamLabelExt2
            // 
            this.clinicalParamLabelExt2.BackColor = System.Drawing.SystemColors.Control;
            this.clinicalParamLabelExt2.Id = "M";
            this.clinicalParamLabelExt2.Location = new System.Drawing.Point(0, 720);
            this.clinicalParamLabelExt2.Name = "clinicalParamLabelExt2";
            this.clinicalParamLabelExt2.ParamName = "Ext 2";
            this.clinicalParamLabelExt2.Size = new System.Drawing.Size(96, 76);
            this.clinicalParamLabelExt2.TabIndex = 11;
            // 
            // clinicalParamLabelExt3
            // 
            this.clinicalParamLabelExt3.BackColor = System.Drawing.SystemColors.Control;
            this.clinicalParamLabelExt3.Id = "N";
            this.clinicalParamLabelExt3.Location = new System.Drawing.Point(0, 720);
            this.clinicalParamLabelExt3.Name = "clinicalParamLabelExt3";
            this.clinicalParamLabelExt3.ParamName = "Ext 3";
            this.clinicalParamLabelExt3.Size = new System.Drawing.Size(96, 76);
            this.clinicalParamLabelExt3.TabIndex = 11;
            // 
            // clinicalParamLabelExt4
            // 
            this.clinicalParamLabelExt4.BackColor = System.Drawing.SystemColors.Control;
            this.clinicalParamLabelExt4.Id = "O";
            this.clinicalParamLabelExt4.Location = new System.Drawing.Point(0, 720);
            this.clinicalParamLabelExt4.Name = "clinicalParamLabelExt4";
            this.clinicalParamLabelExt4.ParamName = "Ext 4";
            this.clinicalParamLabelExt4.Size = new System.Drawing.Size(96, 76);
            this.clinicalParamLabelExt4.TabIndex = 11;
            // 
            // clinicalParamLabelExt5
            // 
            this.clinicalParamLabelExt5.BackColor = System.Drawing.SystemColors.Control;
            this.clinicalParamLabelExt5.Id = "P";
            this.clinicalParamLabelExt5.Location = new System.Drawing.Point(0, 720);
            this.clinicalParamLabelExt5.Name = "clinicalParamLabelExt5";
            this.clinicalParamLabelExt5.ParamName = "Ext 5";
            this.clinicalParamLabelExt5.Size = new System.Drawing.Size(96, 76);
            this.clinicalParamLabelExt5.TabIndex = 11;
            // 
            // clinicalParamLabelExt6
            // 
            this.clinicalParamLabelExt6.BackColor = System.Drawing.SystemColors.Control;
            this.clinicalParamLabelExt6.Id = "Q";
            this.clinicalParamLabelExt6.Location = new System.Drawing.Point(0, 720);
            this.clinicalParamLabelExt6.Name = "clinicalParamLabelExt6";
            this.clinicalParamLabelExt6.ParamName = "Ext 6";
            this.clinicalParamLabelExt6.Size = new System.Drawing.Size(96, 76);
            this.clinicalParamLabelExt6.TabIndex = 11;
            // 
            // clinicalParamLabelExt7
            // 
            this.clinicalParamLabelExt7.BackColor = System.Drawing.SystemColors.Control;
            this.clinicalParamLabelExt7.Id = "R";
            this.clinicalParamLabelExt7.Location = new System.Drawing.Point(0, 720);
            this.clinicalParamLabelExt7.Name = "clinicalParamLabelExt7";
            this.clinicalParamLabelExt7.ParamName = "Ext 7";
            this.clinicalParamLabelExt7.Size = new System.Drawing.Size(96, 76);
            this.clinicalParamLabelExt7.TabIndex = 11;
            // 
            // clinicalParamLabelExt8
            // 
            this.clinicalParamLabelExt8.BackColor = System.Drawing.SystemColors.Control;
            this.clinicalParamLabelExt8.Id = "S";
            this.clinicalParamLabelExt8.Location = new System.Drawing.Point(0, 720);
            this.clinicalParamLabelExt8.Name = "clinicalParamLabelExt8";
            this.clinicalParamLabelExt8.ParamName = "Ext 8";
            this.clinicalParamLabelExt8.Size = new System.Drawing.Size(96, 76);
            this.clinicalParamLabelExt8.TabIndex = 11;
            // 
            // BackgroundPanel
            // 
            this.BackgroundPanel.Border3DStyle = System.Windows.Forms.Border3DStyle.Flat;
            this.BackgroundPanel.BorderColor = System.Drawing.Color.Transparent;
            this.BackgroundPanel.BorderSides = System.Windows.Forms.Border3DSide.Top;
            this.BackgroundPanel.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.BackgroundPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BackgroundPanel.Location = new System.Drawing.Point(2, 2);
            this.BackgroundPanel.Name = "BackgroundPanel";
            this.BackgroundPanel.Size = new System.Drawing.Size(100, 695);
            this.BackgroundPanel.TabIndex = 14;
            // 
            // dsClinicalParamSetup1
            // 
            this.dsClinicalParamSetup1.DataSetName = "dsClinicalParamSetup";
            this.dsClinicalParamSetup1.Locale = new System.Globalization.CultureInfo("en-US");
            this.dsClinicalParamSetup1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // ClinicalBar
            // 
            this.BackColor = System.Drawing.Color.Brown;
            this.Controls.Add(this.gradientPanel1);
            this.Name = "ClinicalBar";
            this.Size = new System.Drawing.Size(104, 699);
            this.Load += new System.EventHandler(this.ClinicalBar_Load);
            this.gradientPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dsClinicalParamSetup1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Syncfusion.Windows.Forms.Tools.GradientPanel gradientPanel1;
        private System.ComponentModel.Container components = null;
        private TCD2003.DB.dsClinicalParamSetup dsClinicalParamSetup1;
        public TCD2003.GUI.ClinicalParamLabel clinicalParamLabelRI;
        public TCD2003.GUI.ClinicalParamLabel clinicalParamLabelMode;
        public TCD2003.GUI.ClinicalParamLabel clinicalParamLabelAverage;
        public TCD2003.GUI.ClinicalParamLabel clinicalParamLabelHits;
        public TCD2003.GUI.ClinicalParamLabel clinicalParamLabelSD;
        public TCD2003.GUI.ClinicalParamLabel clinicalParamLabelDV;
        public TCD2003.GUI.ClinicalParamLabel clinicalParamLabelMean;
        public TCD2003.GUI.ClinicalParamLabel clinicalParamLabelPeak;
        public TCD2003.GUI.ClinicalParamLabel clinicalParamLabelSW;
        public TCD2003.GUI.ClinicalParamLabel clinicalParamLabelPI;
        public TCD2003.GUI.ClinicalParamLabel clinicalParamLabelHitsRate;
        public TCD2003.GUI.ClinicalParamLabel clinicalParamLabelExt1;
        public TCD2003.GUI.ClinicalParamLabel clinicalParamLabelExt2;
        public TCD2003.GUI.ClinicalParamLabel clinicalParamLabelExt3;
        public TCD2003.GUI.ClinicalParamLabel clinicalParamLabelExt4;
        public TCD2003.GUI.ClinicalParamLabel clinicalParamLabelExt5;
        public TCD2003.GUI.ClinicalParamLabel clinicalParamLabelExt6;
        public TCD2003.GUI.ClinicalParamLabel clinicalParamLabelExt7;
        public TCD2003.GUI.ClinicalParamLabel clinicalParamLabelExt8;
        private Syncfusion.Windows.Forms.Tools.GradientPanel BackgroundPanel;
    }
}