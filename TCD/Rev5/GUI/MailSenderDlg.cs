using System.Windows.Forms;

using TCD2003.Utils;

namespace TCD2003.GUI
{
	/// <summary>
	/// Summary description for MailSenderDlg.
	/// </summary>
	public partial class MailSenderDlg : Form
	{
		public MailSenderDlg()
		{
			this.InitializeComponent();
            var strRes = ResourceManagerWrapper.GetWrapper(MainForm.ResourceManager); 
            this.btnSend.Text = strRes.GetString("Send");
            this.btnCancal.Text = strRes.GetString("Cancel");
            this.lblTo.Text = strRes.GetString("To...");
            this.lblSubject.Text = strRes.GetString("Subject") + ":";
            this.lblCC.Text = strRes.GetString("Cc...");
            this.label4.Text = strRes.GetString("Message") + ":";
            this.buttonHelp.Text = strRes.GetString("Help");
            this.Text = strRes.GetString("Mail Sender Dialog");

			this.BackColor = GlobalSettings.BackgroundDlg;
		}

        private string m_to;
        private string m_from = "ronen@rimed.com";
        private string m_subject;
        private string m_body;
		private void btnSend_Click(object sender, System.EventArgs e)
		{
			ExceptionPublisherLog4Net.FormActionLog("btnSend", this.Name, null);
			this.m_to = this.textBoxTo.Text;
            this.m_subject = this.textBoxSubject.Text;
            this.m_body = this.textBoxBody.Text;		
		}
	}
}
