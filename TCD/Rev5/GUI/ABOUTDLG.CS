using System.Text;
using System.Windows.Forms;
using TCD2003.DSP;
using TCD2003.Utils;

namespace TCD2003.GUI
{
    public partial class AboutDlg : Form
    {
        public const string VERSION_NUMBER = "1.18.3.00";
        public const string VERSION_DATE   = "2014.05.29";

        public AboutDlg()
        {
            InitializeComponent();

            var strRes = ResourceManagerWrapper.GetWrapper(MainForm.ResourceManager); 

            labelVersion.Text = strRes.GetString("Version:");
            Text = strRes.GetString("About Digi-Lite");

            BackColor = GlobalSettings.BackgroundDlg;
        }

        private void AboutDlg_Load(object sender, System.EventArgs e)
        {
            refreshVersionLabels();
        }

        private void refreshVersionLabels()
        {
            var version = new StringBuilder("  Versions:\n  - SW:" + VERSION_NUMBER);
            uint cpldLowVersion = 0;
            uint cpldHighVersion = 0;

            //sergey
            if (Dsps.Instance != null && Dsps.Instance.NumOfDsps > 0 && Dsps.Instance[0] != null && Dsps.Instance[0].DspExists)
            {
                uint hwLowVersion = 0;
                uint hwHighVersion = 0;
                int dspVersion = 0;
                if ((int)Dsps.Instance[0].ReadDsp2PcMbx(15) == 1)
                {
                    hwLowVersion = Dsps.Instance[0].ReadDsp2PcMbx(12);
                    hwHighVersion = Dsps.Instance[0].ReadDsp2PcMbx(13);

                    if (Dsps.Instance[0].FPGACardREV == "5")
                    {
                        cpldLowVersion = Dsps.Instance[0].ReadDsp2PcMbx(19);
                        cpldHighVersion = Dsps.Instance[0].ReadDsp2PcMbx(18);
                    }

                    dspVersion = (int)Dsps.Instance[0].ReadDsp2PcMbx(14);

                    int dsp1 = dspVersion / 1000;
                    dspVersion = dspVersion % 1000;
                    int dsp2 = dspVersion / 100;
                    int dsp3 = dspVersion % 100;

                    version.Append("\n  - DSP: " + dsp1 + "." + dsp2 + "." + dsp3);
                    version.Append("\n  - HW: " + hwHighVersion + "." + hwLowVersion);
                    if (Dsps.Instance[0].FPGACardREV == "5")
                        version.Append("\n  - CPLD: " + cpldHighVersion + "." + cpldLowVersion);
                }
                else if ((int)Dsps.Instance[0].ReadDsp2PcMbx(15) == 2)
                {
                    hwLowVersion = Dsps.Instance[0].ReadDsp2PcMbx(12);
                    hwHighVersion = Dsps.Instance[0].ReadDsp2PcMbx(13);
                    if (Dsps.Instance[0].FPGACardREV == "5")
                    {
                        cpldLowVersion = Dsps.Instance[0].ReadDsp2PcMbx(19);
                        cpldHighVersion = Dsps.Instance[0].ReadDsp2PcMbx(18);
                    }
                    dspVersion = (int)Dsps.Instance[0].ReadDsp2PcMbx(14);

                    int dsp1 = dspVersion / 1000000;
                    dspVersion = dspVersion % 1000000;
                    int dsp2 = dspVersion / 10000;
                    dspVersion = dspVersion % 10000;
                    int dsp3 = dspVersion / 100;
                    int dsp4 = dspVersion % 100;

                    version.Append(value: "\n  - DSP: " + dsp1 + "." + dsp2 + "." + dsp3 + "." + dsp4);
                    version.Append("\n  - HW: " + hwHighVersion + "." + hwLowVersion);
                    if (Dsps.Instance[0].FPGACardREV == "5")
                        version.Append("\n  - CPLD: " + cpldHighVersion + "." + cpldLowVersion);
                }
                else
                {
                    hwLowVersion = Dsps.Instance[0].ReadDsp2PcMbx(12);
                    hwHighVersion = Dsps.Instance[0].ReadDsp2PcMbx(13);
                    if (Dsps.Instance[0].FPGACardREV == "5")
                    {
                        cpldLowVersion = Dsps.Instance[0].ReadDsp2PcMbx(19);
                        cpldHighVersion = Dsps.Instance[0].ReadDsp2PcMbx(18);
                    }
                    dspVersion = (int)Dsps.Instance[0].ReadDsp2PcMbx(14);
                    if (hwLowVersion < 16)
                    {
                        version.Append("\n  - DSP:" + dspVersion + "\n  - HW: " + hwHighVersion + "." + hwLowVersion);
                        if (Dsps.Instance[0].FPGACardREV == "5")
                            version.Append("\n  - CPLD: " + cpldHighVersion + "." + cpldLowVersion);
                    }
                    else
                    {
                        version.Append("\n  - DSP: ??? \n  - HW: ???");
                        if (Dsps.Instance[0].FPGACardREV == "5")
                            version.Append("\n  - CPLD: ???");
                    }
                }
            }
            else
            {
                version.Append("\n  - DSP: Offline\n  - HW: Offline ");
            }

            labelVersion.Text = version.ToString();
        }


        private static AboutDlg s_splash;
        public static void SplashOn()
        {
            if (s_splash != null)
            {
                s_splash.Show();
                s_splash.BringToFront();

                return;
            }

            s_splash = new AboutDlg();
            s_splash.TopMost = true;
            s_splash.StartPosition = FormStartPosition.CenterParent;
            s_splash.WindowState = FormWindowState.Normal;
            s_splash.ControlBox = false;
            s_splash.FormBorderStyle = FormBorderStyle.None;
            //s_splash.pictureBox1.BorderStyle = BorderStyle.FixedSingle;

            s_splash.Show();
            s_splash.Focus();
            s_splash.Refresh();
        }

        public static void SplashOff()
        {
            if (s_splash != null)
            {
                s_splash.Close();
                s_splash.Dispose();
                s_splash = null;
            }
        }

        public static void SplashRefresh()
        {
            if (s_splash != null)
            {
                s_splash.refreshVersionLabels();
                s_splash.Refresh();
            }
        }
    }
}
