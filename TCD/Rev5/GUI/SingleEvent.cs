using System;

namespace TCD2003.GUI
{
	public class SingleEvent
	{
		private Guid m_Index;
		private String m_Name;
    
		public  SingleEvent(Guid Index, String Name)
		{
			this.m_Index = Index;
			this.m_Name = Name;
		}

		public String Name
		{
			get
			{
				return m_Name;
			}
		}

		public Guid Index
		{
			get
			{
				return m_Index;
			}
		}
	}
}
