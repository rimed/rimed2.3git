﻿namespace TCD2003.GUI
{
    partial class BVRenameDlg
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dsBVSetup1 = new TCD2003.DB.dsBVSetup();
            this.lblBVName = new System.Windows.Forms.Label();
            this.radioButtonLeft = new System.Windows.Forms.RadioButton();
            this.radioButtonRight = new System.Windows.Forms.RadioButton();
            this.radioButtonMidle = new System.Windows.Forms.RadioButton();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCncel = new System.Windows.Forms.Button();
            this.dsBVSetup2 = new TCD2003.DB.dsBVSetup();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.buttonHelp = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dsBVSetup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsBVSetup2)).BeginInit();
            this.SuspendLayout();
            // 
            // dsBVSetup1
            // 
            this.dsBVSetup1.DataSetName = "dsBVSetup";
            this.dsBVSetup1.Locale = new System.Globalization.CultureInfo("he-IL");
            this.dsBVSetup1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lblBVName
            // 
            this.lblBVName.Location = new System.Drawing.Point(3, 16);
            this.lblBVName.Name = "lblBVName";
            this.lblBVName.Size = new System.Drawing.Size(139, 23);
            this.lblBVName.TabIndex = 2;
            this.lblBVName.Text = "Blood Vessel Name:";
            this.lblBVName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // radioButtonLeft
            // 
            this.radioButtonLeft.AutoSize = true;
            this.radioButtonLeft.Location = new System.Drawing.Point(30, 52);
            this.radioButtonLeft.Name = "radioButtonLeft";
            this.radioButtonLeft.Size = new System.Drawing.Size(43, 17);
            this.radioButtonLeft.TabIndex = 3;
            this.radioButtonLeft.Text = "Left";
            this.radioButtonLeft.Visible = false;
            this.radioButtonLeft.CheckedChanged += new System.EventHandler(this.radioButtonLeft_CheckedChanged);
            // 
            // radioButtonRight
            // 
            this.radioButtonRight.AutoSize = true;
            this.radioButtonRight.Location = new System.Drawing.Point(118, 52);
            this.radioButtonRight.Name = "radioButtonRight";
            this.radioButtonRight.Size = new System.Drawing.Size(50, 17);
            this.radioButtonRight.TabIndex = 3;
            this.radioButtonRight.Text = "Right";
            this.radioButtonRight.Visible = false;
            this.radioButtonRight.CheckedChanged += new System.EventHandler(this.radioButtonRight_CheckedChanged);
            // 
            // radioButtonMidle
            // 
            this.radioButtonMidle.AutoSize = true;
            this.radioButtonMidle.Location = new System.Drawing.Point(214, 52);
            this.radioButtonMidle.Name = "radioButtonMidle";
            this.radioButtonMidle.Size = new System.Drawing.Size(56, 17);
            this.radioButtonMidle.TabIndex = 3;
            this.radioButtonMidle.Text = "Middle";
            this.radioButtonMidle.Visible = false;
            this.radioButtonMidle.CheckedChanged += new System.EventHandler(this.radioButtonMidle_CheckedChanged);
            // 
            // buttonOK
            // 
            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOK.Enabled = false;
            this.buttonOK.Location = new System.Drawing.Point(24, 88);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 4;
            this.buttonOK.Text = "OK";
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCncel
            // 
            this.buttonCncel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCncel.Location = new System.Drawing.Point(112, 88);
            this.buttonCncel.Name = "buttonCncel";
            this.buttonCncel.Size = new System.Drawing.Size(75, 23);
            this.buttonCncel.TabIndex = 4;
            this.buttonCncel.Text = "Cancel";
            // 
            // dsBVSetup2
            // 
            this.dsBVSetup2.DataSetName = "dsBVSetup";
            this.dsBVSetup2.Locale = new System.Globalization.CultureInfo("he-IL");
            this.dsBVSetup2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.Location = new System.Drawing.Point(148, 16);
            this.comboBox1.MaxDropDownItems = 10;
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(132, 21);
            this.comboBox1.TabIndex = 5;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // buttonHelp
            // 
            this.buttonHelp.Location = new System.Drawing.Point(200, 88);
            this.buttonHelp.Name = "buttonHelp";
            this.buttonHelp.Size = new System.Drawing.Size(75, 23);
            this.buttonHelp.TabIndex = 4;
            this.buttonHelp.Text = "Help";
            // 
            // BVRenameDlg
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(296, 125);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.radioButtonLeft);
            this.Controls.Add(this.lblBVName);
            this.Controls.Add(this.radioButtonRight);
            this.Controls.Add(this.radioButtonMidle);
            this.Controls.Add(this.buttonCncel);
            this.Controls.Add(this.buttonHelp);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BVRenameDlg";
            this.ShowInTaskbar = false;
            this.Text = "Rename BV";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.BVRenameDlg_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dsBVSetup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsBVSetup2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TCD2003.DB.dsBVSetup dsBVSetup1;
        private TCD2003.DB.dsBVSetup dsBVSetup2;
        private System.Windows.Forms.Label lblBVName;
        private System.Windows.Forms.RadioButton radioButtonLeft;
        private System.Windows.Forms.RadioButton radioButtonRight;
        private System.Windows.Forms.RadioButton radioButtonMidle;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCncel;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button buttonHelp;
        private System.ComponentModel.Container components = null;
    }
}