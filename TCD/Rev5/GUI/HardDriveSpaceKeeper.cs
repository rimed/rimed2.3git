using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using TCD2003.DB;
using TCD2003.Utils;

namespace TCD2003.GUI
{
    /// <summary>
    /// Summary description for HardDriveSpaceKeeper.
    /// This class was wrote by Dima from MWDN and holds the logics for the option to 
    /// consume less HD space, by saving just partial data of the examination (Snapshots for the report and the spectum outlook
    /// and not the replay/rawdata).
    /// the designations are taking according to the setup and the user choice on the fly.
    /// </summary>
    public class HardDriveSpaceKeeper
    {
        public event RowDataModeChangedHandler RowDataModeChanged;

        private bool m_fakeDataRowReady = true;
        public static readonly HardDriveSpaceKeeper Instance = new HardDriveSpaceKeeper();

        public bool IsFakeDataRowReady
        {
            get
            {
                return m_fakeDataRowReady;
            }
        }

        /// <summary>
        /// Keeper should subscribe by LayoutManager events the fist then all
        /// </summary>
        public void PrepareForWork()
        {
            LayoutManager.LoadBVExamEvent += new LoadBVExamDelegate(LayoutManager_LoadBVExamEvent);
            LayoutManager.LoadExamEvent += new LoadExamDelegate(LayoutManager_LoadExamEvent);
            LayoutManager.LoadGateExamEvent += new LoadGateExamDelegate(LayoutManager_LoadGateExamEvent);
            LayoutManager.theLayoutManager.SystemModeChangedEvent += new Action(theLayoutManager_SystemModeChangedEvent);
        }

        private Image GetChartImage(string path)
        {
            if (!IsFakeDataRowReady || !File.Exists(path))
                return null;

            try
            {
                //ExceptionPublisherLog4Net.TraceLog(string.Format("GetChartImage('{0}')   <--- LOAD FILE.", path), "HardDriveSpaceKeeper");
                return Image.FromFile(path);
            }
            catch (Exception ex)
            {
                ExceptionPublisherLog4Net.ExceptionErrorLog(ex);

                MessageBox.Show(MainForm.ResourceManager.GetString("Failed to load the chart from an image file.") + "\n" + ex.Message, MainForm.ResourceManager.GetString("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        public Image GetAutoscanImage(Guid subExaminationIndex, int index)
        {
            string path = GetAutoscanSnapshotPath(subExaminationIndex, index);
            return GetChartImage(path);
        }

        public Image GetSpectrumImage(GateView gate)
        {
            string path;

            if (LayoutManager.theLayoutManager.CurrentGateView != null && LayoutManager.theLayoutManager.CurrentGateView.Gate_Index == gate.Gate_Index)
                path = GetGateSnapshotPath(gate.Gate_Index, true);
            else
                path = GetGateSnapshotPath(gate.Gate_Index, false);

            return GetChartImage(path);
        }

        public bool IsSaveOnlySummaryImages(Guid subExamIndex)
        {
            foreach (dsBVExamination.tb_BVExaminationRow bvRow in RimedDal.Instance.s_dsBVExamination.tb_BVExamination.Rows)
            {
                if (bvRow.SubExamIndex == subExamIndex && bvRow.SaveOnlySummaryImages)
                    return true;
            }

            return false;
        }

        public bool IsReplayRowDataFake(Guid subExamIndex)
        {
            if (!IsFakeDataRowReady)
                return false;

            foreach (dsBVExamination.tb_BVExaminationRow bvRow in RimedDal.Instance.s_dsBVExamination.tb_BVExamination.Rows)
            {
                if (bvRow.SubExamIndex == subExamIndex && bvRow.SaveOnlySummaryImages)
                    return true;
            }

            return false;
        }
        /// <summary>
        /// Save the  snapshots of the rows.
        /// A snapshot will use in the replay mode if a row data will be removed
        /// </summary>
        public void SaveReplayShaphots()
        {
            m_fakeDataRowReady = false;

            if (!Directory.Exists(GlobalSettings.ReplayImgDir))
            {
                Directory.CreateDirectory(GlobalSettings.ReplayImgDir);
            }

            var gateIndex = Guid.Empty;

            foreach (GateView view in LayoutManager.theLayoutManager.GateViews)
            {
                string path = GetGateSnapshotPath(view.Gate_Index, false);
                view.Spectrum.Graph.SaveSnapshot(path);

                // for gate center view mode
                if (LayoutManager.theLayoutManager.CurrentGateView != null)
                {
                    path = GetGateSnapshotPath(view.Gate_Index, true);
                    var size = LayoutManager.theLayoutManager.CurrentGateView.Spectrum.Graph.Size;

                    view.Spectrum.Graph.SaveSnapshot(path, size);
                }

                if (gateIndex == Guid.Empty)
                {
                    gateIndex = view.Gate_Index;
                }
            }

            var examinationRow = (dsExamination.tb_ExaminationRow)RimedDal.Instance.s_dsExamination.tb_Examination.Rows[0];
            var examinationIndex = examinationRow.Examination_Index;

            var gateRow = RimedDal.Instance.s_dsGateExamination.tb_GateExamination.FindByGate_IndexExaminationIndex(gateIndex, examinationIndex);
            var subExaminationIndex = gateRow.SubExaminationIndex;

            for (int i = 0; i < LayoutManager.theLayoutManager.Autoscans.Length; i++)
            {
                if (LayoutManager.theLayoutManager.Autoscans[i].Visible)
                {
                    var path = GetAutoscanSnapshotPath(subExaminationIndex, i);
                    LayoutManager.theLayoutManager.Autoscans[i].Graph.SaveSnapshot(path);
                }
            }
        }

        public void DeleteUnusedExamData()
        {
            foreach (dsBVExamination.tb_BVExaminationRow bvRow in RimedDal.Instance.s_dsBVExamination.tb_BVExamination.Rows)
            {
                string path;

                if (bvRow.SaveOnlySummaryImages) // remove row data fiels
                {
                    path = Path.Combine(GlobalSettings.RawDataDir, bvRow.SubExamIndex.ToString()) + ".dat";
                    DeleteFile(path);

                    path = Path.Combine(GlobalSettings.RawDataDir, bvRow.SubExamIndex.ToString()) + ".dat.tmp";
                    DeleteFile(path);
                }
                else //remove replay images
                {
                    foreach (dsGateExamination.tb_GateExaminationRow gateRow in RimedDal.Instance.s_dsGateExamination.tb_GateExamination.Rows)
                    {
                        if (gateRow.SubExaminationIndex == bvRow.SubExamIndex)
                        {
                            path = GetGateSnapshotPath(gateRow.Gate_Index, true);
                            DeleteFile(path);

                            path = GetGateSnapshotPath(gateRow.Gate_Index, false);
                            DeleteFile(path);
                        }
                    }

                    for (int i = 0; i < LayoutManager.theLayoutManager.Autoscans.Length; i++)
                    {
                        path = GetAutoscanSnapshotPath(bvRow.SubExamIndex, i);
                        DeleteFile(path);
                    }
                }
                foreach (dsGateExamination.tb_GateExaminationRow gateRow in RimedDal.Instance.s_dsGateExamination.tb_GateExamination.Rows)
                {
                    var fileName = gateRow.Gate_Index.ToString();
                    if (gateRow.Name.EndsWith("-L"))
                        fileName += "_L.jpg";
                    else if (gateRow.Name.EndsWith("-R"))
                        fileName += "_R.jpg";
                    else
                        fileName += "_M.jpg";

                    var filePath = Path.Combine(GlobalSettings.TmpImgDir, fileName);
                    DeleteFile(filePath);
                    DeleteFile(filePath.Insert(filePath.IndexOf('.'), "_MC"));
                }
            }
        }

        static string GetGateSnapshotPath(Guid gateIndex, bool inCenterView)
        {
            if (inCenterView)
                return Path.Combine(GlobalSettings.ReplayImgDir, gateIndex + "_CenterView" + ".jpg");

            return Path.Combine(GlobalSettings.ReplayImgDir, gateIndex + ".jpg");
        }

        static string GetAutoscanSnapshotPath(Guid subExaminationIndex, int index)
        {
            return Path.Combine(GlobalSettings.ReplayImgDir, subExaminationIndex + "_" + index + ".jpg");
        }

        static void DeleteFile(string path)
        {
            try
            {
                if (File.Exists(path))
                {
                    ExceptionPublisherLog4Net.TraceLog(string.Format("DeleteFile(...): File.Delete('{0}')   <--- FILE.DELETE", path), "HardDriveSpaceKeeper");  
                    File.Delete(path);
                }
            }
            catch (Exception ex)
            {
                ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
            }
        }

        public void SetSavingRowDataMode(Guid subExaminationIndex, bool saveOnlySummaryImages)
        {
            if (this.IsFakeDataRowReady)
                return;

            bool changed = false;

            foreach (dsBVExamination.tb_BVExaminationRow row in RimedDal.Instance.s_dsBVExamination.tb_BVExamination.Rows)
            {
                if (row.SubExamIndex == subExaminationIndex)
                {
                    if (row.SaveOnlySummaryImages != saveOnlySummaryImages)
                    {
                        row.SaveOnlySummaryImages = saveOnlySummaryImages;
                        changed = true;
                    }
                }
            }

            if (this.RowDataModeChanged != null && changed)
            {
                this.RowDataModeChanged(subExaminationIndex, saveOnlySummaryImages);
            }
        }

        public void ToggleSavingRowDataMode(Guid subExaminationIndex)
        {
            if (this.IsFakeDataRowReady)
            {
                return;
            }

            bool changed = false;
            bool saveOnlySummaryImages = false;

            foreach (dsBVExamination.tb_BVExaminationRow row in RimedDal.Instance.s_dsBVExamination.tb_BVExamination.Rows)
            {
                if (row.SubExamIndex == subExaminationIndex)
                {
                    row.SaveOnlySummaryImages = !row.SaveOnlySummaryImages;
                    saveOnlySummaryImages = row.SaveOnlySummaryImages;
                    changed = true;
                }
            }

            if (this.RowDataModeChanged != null && changed)
            {
                this.RowDataModeChanged(subExaminationIndex, saveOnlySummaryImages);
            }
        }

        private void LayoutManager_LoadBVExamEvent(Guid examIndex, Guid bvExamIndex)
        {
            m_fakeDataRowReady = !RimedDal.Instance.IsTemporaryExamination(examIndex);
        }

        private void LayoutManager_LoadExamEvent(Guid examIndex)
        {
            m_fakeDataRowReady = !RimedDal.Instance.IsTemporaryExamination(examIndex);
        }

        private void LayoutManager_LoadGateExamEvent(Guid examIndex, GateIndexTagData gitd)
        {
            m_fakeDataRowReady = !RimedDal.Instance.IsTemporaryExamination(examIndex);
        }

        private void theLayoutManager_SystemModeChangedEvent()
        {
            if (LayoutManager.SystemLayoutMode != GlobalTypes.TSystemLayoutMode.DiagnosticLoad)
                m_fakeDataRowReady = false;
        }
    }

    public delegate void RowDataModeChangedHandler(Guid subExaminationIndex, bool saveOnlySummaryImages);
}
