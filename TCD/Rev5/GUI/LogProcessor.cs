using System;
using System.Globalization;
using System.IO;

namespace TCD2003.GUI
{
	/// <summary>
	/// Summary description for LogProcessor.
	/// </summary>
	public class LogProcessor: IDisposable
	{
		private FileStream _stream = null;
		private StreamWriter writer = null;
		private object _syncWriterLock;

		private static LogProcessor instance;

		private LogProcessor()
		{
			//
			// TODO: Add constructor logic here
			//
			_syncWriterLock = new object();
			string directory = string.Format("{0}Logs", AppDomain.CurrentDomain.BaseDirectory);

			if(!Directory.Exists(directory))
				Directory.CreateDirectory(directory);
			string _fileName = string.Format("\\{0}.txt", System.DateTime.Now.ToString(CultureInfo.InvariantCulture).Replace(" ", "_").Replace(":", "-").Replace("/", ".").Replace(".", ""));				
			_fileName = string.Concat(directory, _fileName);

			_stream = new FileStream(_fileName, FileMode.Create, FileAccess.Write, FileShare.Read);
			writer = new StreamWriter(_stream);
		}
		
		public void WriteToFile(string info)
		{
			lock(_syncWriterLock)
			{
				writer.WriteLine(info);
				writer.Flush();
				_stream.Flush();	
			}
		}

		public void Dispose()
		{
			writer.WriteLine("\r\n----next examination----\r\n");
			writer.Close();
			_stream.Close();
		}

		public static LogProcessor Instance
		{
			get
			{
				if(instance == null)
				{
					instance = new LogProcessor();
				}
				return instance;
			}
		}

	}
}
