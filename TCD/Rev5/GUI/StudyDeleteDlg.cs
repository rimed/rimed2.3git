using System;
using System.Drawing;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

using Syncfusion.Windows.Forms.Tools.XPMenus;

using TCD2003.Utils;

namespace TCD2003.GUI
{
    /// <summary>
    /// Summary description for StudyDeleteDlg.
    /// </summary>
    public partial class StudyDeleteDlg : Form
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public StudyDeleteDlg()
        {
            this.InitializeComponent();

            var strRes = ResourceManagerWrapper.GetWrapper(MainForm.ResourceManager); 
            this.buttonDelete.Text = strRes.GetString("Delete");
            this.buttonCancel.Text = strRes.GetString("Close");
            this.Text = strRes.GetString("Delete Study");

            this.BackColor = GlobalSettings.BackgroundDlg;
        }

        public void InitData(BarItems items)
        {
            this.treeViewStudies.Nodes.Clear();
            foreach (ParentBarItem ib in items)
            {
                TreeNode n = new TreeNode(ib.Text);
                n.Tag = ib.Tag;
                this.treeViewStudies.Nodes.Add(n);
                foreach (BarItem bi in ib.Items)
                {
                    TreeNode nn = new TreeNode(bi.Text);
                    nn.Tag = bi.Tag;
                    n.Nodes.Add(nn);
                }
            }
        }

        /// <summary>
        /// Method for selected study deleting 
        /// </summary>
        private void buttonDelete_Click(object sender, EventArgs e)
        {
            Guid selectedNodeTag = (Guid)treeViewStudies.SelectedNode.Tag;
            if (treeViewStudies.SelectedNode != null
                && treeViewStudies.SelectedNode.Nodes.Count == 0
                && treeViewStudies.SelectedNode.Text != "Default")
            {
                var strRes = ResourceManagerWrapper.GetWrapper(MainForm.ResourceManager); 

                if (MessageBox.Show(strRes.GetString("Are you sure you want to delete the selected study?"),
                    strRes.GetString("Warning"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
                {
                    List<LogField> fields = new List<LogField>();
                    fields.Add(new LogField("treeViewStudies", treeViewStudies.SelectedNode.Text));
                    ExceptionPublisherLog4Net.FormActionLog("buttonDelete", this.Name, fields);

                    if (DB.RimedDal.Instance.DeleteStudy(selectedNodeTag))
                    {
                        // delete the selected study from the tree.
                        this.treeViewStudies.Nodes.Remove(treeViewStudies.SelectedNode);
                    }
                }
            }
        }

        private void treeViewStudies_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (this.treeViewStudies.SelectedNode != null
                && this.treeViewStudies.SelectedNode.Index != 0
                && this.treeViewStudies.SelectedNode.FirstNode == null
                && (Guid)this.treeViewStudies.SelectedNode.Tag != MainForm.Instance.CurrentStudyId)
            {
                this.buttonDelete.Enabled = true;
            }
            else
            {
                this.buttonDelete.Enabled = false;
            }
        }
    }
}
