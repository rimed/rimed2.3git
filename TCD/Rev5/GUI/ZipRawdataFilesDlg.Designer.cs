﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TCD2003.GUI
{
    partial class ZipRawdataFilesDlg
    {
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.Label lbInfo;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Button buttonStart;
        public Syncfusion.Windows.Forms.Tools.ProgressBarAdv progressBarAdv1;
        private System.Windows.Forms.Button buttonHelp;
        private System.ComponentModel.IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonClose = new System.Windows.Forms.Button();
            this.lbInfo = new System.Windows.Forms.Label();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.buttonStart = new System.Windows.Forms.Button();
            this.progressBarAdv1 = new Syncfusion.Windows.Forms.Tools.ProgressBarAdv();
            this.buttonHelp = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.progressBarAdv1)).BeginInit();
            this.SuspendLayout();
            // 
            // buttonClose
            // 
            this.buttonClose.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonClose.Enabled = false;
            this.buttonClose.Location = new System.Drawing.Point(176, 112);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(72, 24);
            this.buttonClose.TabIndex = 0;
            this.buttonClose.Text = "Close";
            this.buttonClose.Visible = false;
            // 
            // lbInfo
            // 
            this.lbInfo.Location = new System.Drawing.Point(16, 16);
            this.lbInfo.Name = "lbInfo";
            this.lbInfo.Size = new System.Drawing.Size(312, 40);
            this.lbInfo.TabIndex = 2;
            this.lbInfo.Text = "The system is Zipping the raw data files, it might take a few seconds.";
            this.lbInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(16, 112);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(72, 24);
            this.buttonStart.TabIndex = 0;
            this.buttonStart.Text = "Start";
            this.buttonStart.Visible = false;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // progressBarAdv1
            // 
            this.progressBarAdv1.BackColor = System.Drawing.SystemColors.Control;
            this.progressBarAdv1.BackGradientEndColor = System.Drawing.SystemColors.ControlLightLight;
            this.progressBarAdv1.BackGradientStartColor = System.Drawing.SystemColors.ControlDark;
            this.progressBarAdv1.BackgroundStyle = Syncfusion.Windows.Forms.Tools.ProgressBarBackgroundStyles.VerticalGradient;
            this.progressBarAdv1.BackMultipleColors = new System.Drawing.Color[] {
        System.Drawing.SystemColors.ControlDark,
        System.Drawing.SystemColors.ControlLightLight,
        System.Drawing.SystemColors.Control};
            this.progressBarAdv1.BackSegments = false;
            this.progressBarAdv1.BackTubeEndColor = System.Drawing.SystemColors.ControlLight;
            this.progressBarAdv1.BackTubeStartColor = System.Drawing.SystemColors.ControlDark;
            this.progressBarAdv1.Border3DStyle = System.Windows.Forms.Border3DStyle.RaisedOuter;
            this.progressBarAdv1.BorderColor = System.Drawing.Color.Black;
            this.progressBarAdv1.FontColor = System.Drawing.SystemColors.HighlightText;
            this.progressBarAdv1.ForeColor = System.Drawing.Color.MediumBlue;
            this.progressBarAdv1.ForegroundImage = null;
            this.progressBarAdv1.GradientEndColor = System.Drawing.Color.Lime;
            this.progressBarAdv1.GradientStartColor = System.Drawing.Color.Red;
            this.progressBarAdv1.Location = new System.Drawing.Point(16, 64);
            this.progressBarAdv1.MultipleColors = new System.Drawing.Color[] {
        System.Drawing.Color.DarkRed,
        System.Drawing.Color.Red,
        System.Drawing.Color.Black};
            this.progressBarAdv1.Name = "progressBarAdv1";
            this.progressBarAdv1.ProgressStyle = Syncfusion.Windows.Forms.Tools.ProgressBarStyles.Tube;
            this.progressBarAdv1.SegmentWidth = 20;
            this.progressBarAdv1.Size = new System.Drawing.Size(320, 23);
            this.progressBarAdv1.StretchImage = false;
            this.progressBarAdv1.StretchMultGrad = false;
            this.progressBarAdv1.TabIndex = 4;
            this.progressBarAdv1.TextShadow = false;
            this.progressBarAdv1.ThemesEnabled = false;
            this.progressBarAdv1.TubeEndColor = System.Drawing.SystemColors.Control;
            this.progressBarAdv1.TubeStartColor = System.Drawing.SystemColors.ControlDark;
            this.progressBarAdv1.Value = 0;
            this.progressBarAdv1.WaitingGradientWidth = 400;
            // 
            // buttonHelp
            // 
            this.buttonHelp.Enabled = false;
            this.buttonHelp.Location = new System.Drawing.Point(264, 112);
            this.buttonHelp.Name = "buttonHelp";
            this.buttonHelp.Size = new System.Drawing.Size(72, 24);
            this.buttonHelp.TabIndex = 0;
            this.buttonHelp.Text = "Help";
            this.buttonHelp.Visible = false;
            // 
            // ZipRawdataFilesDlg
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(354, 160);
            this.ControlBox = false;
            this.Controls.Add(this.progressBarAdv1);
            this.Controls.Add(this.lbInfo);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.buttonHelp);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ZipRawdataFilesDlg";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Zip Raw-Data Files";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.ZipRawdataFilesDlg_Load);
            ((System.ComponentModel.ISupportInitialize)(this.progressBarAdv1)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

    }
}
