﻿namespace TCD2003.GUI
{
    partial class DopplerVarLabel
    {
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DopplerVarLabel));
            this.labelHeader = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labelVar = new System.Windows.Forms.Label();
            this.imageListLarge = new System.Windows.Forms.ImageList(this.components);
            this.imageListSmall = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pictureBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelHeader
            // 
            this.labelHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelHeader.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.labelHeader.Location = new System.Drawing.Point(0, 0);
            this.labelHeader.Name = "labelHeader";
            this.labelHeader.Size = new System.Drawing.Size(58, 23);
            this.labelHeader.TabIndex = 0;
            this.labelHeader.Text = "Gain";
            this.labelHeader.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.labelHeader.Click += new System.EventHandler(this.labelHeader_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Controls.Add(this.labelVar);
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(0, 23);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(58, 35);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // labelVar
            // 
            this.labelVar.BackColor = System.Drawing.Color.Transparent;
            this.labelVar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelVar.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.labelVar.Location = new System.Drawing.Point(0, 0);
            this.labelVar.Name = "labelVar";
            this.labelVar.Size = new System.Drawing.Size(58, 35);
            this.labelVar.TabIndex = 2;
            this.labelVar.Text = "5";
            this.labelVar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelVar.Click += new System.EventHandler(this.labelVar_Click);
            // 
            // imageListLarge
            // 
            this.imageListLarge.ColorDepth = System.Windows.Forms.ColorDepth.Depth24Bit;
            this.imageListLarge.ImageSize = new System.Drawing.Size(83, 52);
            this.imageListLarge.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListLarge.ImageStream")));
            this.imageListLarge.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // imageListSmall
            // 
            this.imageListSmall.ColorDepth = System.Windows.Forms.ColorDepth.Depth24Bit;
            this.imageListSmall.ImageSize = new System.Drawing.Size(72, 27);
            this.imageListSmall.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListSmall.ImageStream")));
            this.imageListSmall.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // DopplerVarLabel
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(235)))), ((int)(((byte)(250)))));
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.labelHeader);
            this.Name = "DopplerVarLabel";
            this.Size = new System.Drawing.Size(58, 58);
            this.Load += new System.EventHandler(this.DopplerVarLabel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pictureBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.Label labelHeader;
        private System.ComponentModel.IContainer components;

        protected System.Windows.Forms.Label labelVar;
        protected System.Windows.Forms.ImageList imageListLarge;
        protected System.Windows.Forms.ImageList imageListSmall;

        protected System.Windows.Forms.PictureBox pictureBox1;
    }
}