using System;
using System.Collections.Generic;
using Microsoft.Win32;

namespace TCD2003.GUI
{
	/// <summary>
	/// Contains clinical parameters data for one TrendChart
	/// </summary>
	public class ExportPackage
	{
		private DateTime m_startTime;
		private int m_interval;

		/// <summary>
		/// Time of start recording (and clinical parameters saving)
		/// </summary>
		public DateTime StartTime
		{
			set 
			{
				m_startTime = value;
			}

			get
			{
				return m_startTime;
			}
		}

		/// <summary>
		/// Time interval for clinical parameters recording (at seconds)
		/// </summary>
		public int Interval
		{
			get
			{
				return m_interval;
			}
		}

        public List<ExportElement> Log { get; private set; }
        public List<string> Header { get; private set; }

		public ExportPackage()
		{
            this.Log = new List<ExportElement>();
            this.Header = new List<string>();
			RegistryKey myKey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Rimed", true);
			if (myKey != null)
			{
				if (myKey.GetValue("ExportInterval") != null)
				{
                    this.m_interval = Convert.ToInt32(myKey.GetValue("ExportInterval"));
				}
				else
				{
					//Default time interval for export = 30 sec
                    this.m_interval = GlobalSettings.ShortExportInterval;
					myKey.SetValue("ExportInterval", GlobalSettings.ShortExportInterval);
				}
			}
			else
				m_interval = GlobalSettings.ShortExportInterval;
		}
	}
}
