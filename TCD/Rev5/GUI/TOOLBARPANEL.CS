using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

using TCD2003.Utils;

namespace TCD2003.GUI
{
    /// <summary>
    /// The main tool bar of the application
    /// Holds the most important buttons/functions for the user.
    /// The buttons on the bar will change according to the study type
    /// There are several other elements on the bar, like the Probs combobox and the
    /// Analog Gain combobox
    /// </summary>
    public partial class ToolBarPanel : UserControl
    {
        #region Private Fields

        /// <summary>
        /// A Jagged-array of ToolBarButtonDef.
        /// It's an array of "arrays of toolbar buttons".
        /// The first indexer is the toolbar index, the 2nd is the 
        /// index of the button in the toolbar.
        /// </summary>
        private ToolBarBtn[][] m_toolBarBtnArr;

        #endregion Private Fields

        #region Control Properties

        public PictureBox ButtonPlay { get { return buttonPlay.Button; } }
        public TCD2003.GUI.ToolBarBtn ButtonPause { get { return buttonPause; } }
        public PictureBox ButtonStopRecording { get { return buttonStopRecording.Button; } }
        public TCD2003.GUI.ToolBarBtn ButtonStartRecording { get { return buttonStartRecording; } }
        public PictureBox ButtonAddTrend { get { return buttonAddTrend.Button; } }
        public PictureBox ButtonSendTo { get { return buttonSendTo.Button; } }
        public PictureBox ButtonSave { get { return buttonSave.Button; } }
        public TCD2003.GUI.ToolBarBtn ButtonAutoscan { get { return buttonAutoscan; } }
        public PictureBox ButtonAddSpectrum { get { return buttonAddSpectrum.Button; } }
        public TCD2003.GUI.ToolBarBtn ButtonHitsDetection { get { return buttonHitsDetection; } }
        public PictureBox ButtonNextFunction { get { return buttonNextFunction.Button; } }
        public PictureBox ButtonSummary { get { return buttonSummary.Button; } }
        public PictureBox ButtonNotes { get { return buttonNotes.Button; } }
        public PictureBox ButtonScrollForward { get { return buttonScrollForward.Button; } }
        public PictureBox ButtonScrollBackward { get { return buttonScrollBackWard.Button; } }
        public TCD2003.GUI.ToolBarBtn ButtonCursors { get { return buttonCursors; } }
        public PictureBox ButtonPrint { get { return buttonPrint.Button; } }
        public PictureBox ButtonLoad { get { return buttonLoad.Button; } }
        public PictureBox ButtonNewPatient { get { return buttonNewPatient.Button; } }
        public PictureBox ButtonStudies { get { return buttonStudies.Button; } }
        public PictureBox ButtonClinicalParameters { get { return buttonClinicalParameters.Button; } }
        public PictureBox ButtonPatientRep { get { return buttonPatientRep.Button; } }
        public TCD2003.GUI.ToolBarBtn ButtonFreeze { get { return buttonFreeze; } }

        public TCD2003.GUI.ToolBarBtn ButtonReturn { get { return buttonReturn; } }
        public TCD2003.GUI.ToolBarBtn ButtonStopRecordingCtrl { get { return buttonStopRecording; } }
        public TCD2003.GUI.ToolBarBtn ButtonAddTrendCtrl { get { return buttonAddTrend; } }
        public TCD2003.GUI.ToolBarBtn ButtonSendToCtrl { get { return buttonSendTo; } }
        public TCD2003.GUI.ToolBarBtn ButtonSaveCtrl { get { return buttonSave; } }
        public TCD2003.GUI.ToolBarBtn ButtonAddSpectrumCtrl { get { return buttonAddSpectrum; } }
        public TCD2003.GUI.ToolBarBtn ButtonNextFunctionCtrl { get { return buttonNextFunction; } }
        public TCD2003.GUI.ToolBarBtn ButtonSummaryCtrl { get { return buttonSummary; } }
        public TCD2003.GUI.ToolBarBtn ButtonNotesCtrl { get { return buttonNotes; } }

        public TCD2003.GUI.ToolBarBtn ButtonPrintCtrl { get { return buttonPrint; } }
        public TCD2003.GUI.ToolBarBtn ButtonLoadCtrl { get { return buttonLoad; } }
        public TCD2003.GUI.ToolBarBtn ButtonNewPAtientCtrl { get { return buttonNewPatient; } }
        public TCD2003.GUI.ToolBarBtn ButtonStudiesCtrl { get { return buttonStudies; } }
        public TCD2003.GUI.ToolBarBtn ButtonClinicalParametersCtrl { get { return buttonClinicalParameters; } }
        public TCD2003.GUI.ToolBarBtn ButtonPlayCtrl { get { return buttonPlay; } }
        public TCD2003.GUI.ToolBarBtn ButtonPatientRepCtrl { get { return buttonPatientRep; } }
        public TCD2003.GUI.ToolBarBtn ButtonScrollBackWardCtrl { get { return buttonScrollBackWard; } }
        public TCD2003.GUI.ToolBarBtn ButtonScrollForwardCtrl { get { return buttonScrollForward; } }

        public ComboBox ComboBoxTime { get { return comboBoxTime; } }
        public TCD2003.GUI.ProbesCombobox ComboBoxProbes { get { return this.comboBoxProbes; } }
        public System.Windows.Forms.Label TimeLabel { get { return this.timeLabel; } }
        public string TimeLabelText
        {
            set
            {
                if (this.InvokeRequired)
                    this.BeginInvoke((Action)delegate()
                    {
                        this.timeLabel.Text = value;
                    });
                else
                    this.timeLabel.Text = value;
            }
        }
        //sergey - all this file
        public System.Windows.Forms.ComboBox ComboBoxSensitivity { get { return this.comboBoxSensitivity; } }
        public System.Windows.Forms.ComboBox ComboBoxSensitivitySecondary { get { return this.comboBoxSensitivitySecondary; } }

        #endregion Control Properties


        /// <summary>Constructor</summary>
        public ToolBarPanel()
        {
            CurrentToolBarType = (GlobalTypes.TooolBarType)(-1);
            ProbeLocation16Mhz = 0;
            InitializeComponent();

            initToolBarButtonsArray();

            ///////////////////////////////////////////////////////////////
            //// Initialization Of toolbar buttons
            //////////////////////////////////////////////////////////////
            //s_toolBarBtnArr = new ToolBarBtn[][]
            //    {
            //        // TB1_DiagnosticUnfreeze
            //        new ToolBarBtn [] 
            //        { 
            //            buttonFreeze,
            //            buttonPrint,
            //            buttonHitsDetection,
            //            buttonAddSpectrum,
            //            buttonAutoscan,
            //            buttonNextFunction
            //        },

            //        // TB2_DiagnosticFreeze
            //        new ToolBarBtn [] 
            //        { 
            //            buttonFreeze,
            //            buttonStudies,
            //            buttonNewPatient,
            //            buttonLoad,
            //            buttonPrint,
            //            buttonCursors,
            //            buttonNotes,
            //            buttonSummary,
            //            buttonNextFunction,
            //            buttonHitsDetection, 
            //            buttonAddSpectrum,
            //            buttonAutoscan, 
            //            buttonScrollBackWard,
            //            buttonScrollForward
            //        },

            //        // TB3_Summary
            //        new ToolBarBtn [] 
            //        { 
            //            buttonStudies,
            //            buttonNewPatient,
            //            buttonLoad,
            //            buttonPrint,
            //            buttonNotes,
            //            buttonClinicalParameters,
            //            buttonReturn,
            //            buttonSave,
            //            buttonPatientRep
            //        },

            //        // TB4_MonitoringUnfreeze
            //        new ToolBarBtn [] 
            //        { 
            //            buttonFreeze,
            //            buttonPrint,
            //            buttonHitsDetection, 
            //            buttonAddSpectrum,
            //            buttonAddTrend,
            //            buttonAutoscan,
            //            buttonStartRecording,
            //            buttonStopRecording
            //        },

            //        // TB5_MonitoringFreeze
            //        new ToolBarBtn [] 
            //        { 
            //            buttonFreeze,
            //            buttonStudies,
            //            buttonNewPatient,
            //            buttonLoad,
            //            buttonCursors,
            //            buttonNotes,
            //            buttonHitsDetection, 
            //            buttonAddSpectrum,
            //            buttonAddTrend,
            //            buttonAutoscan,
            //            buttonStartRecording,
            //            buttonStopRecording
            //        },

            //        // TB6_ReplayFreeze
            //        new ToolBarBtn [] 
            //        { 
            //            buttonPlay,
            //            buttonPause,
            //            buttonStopRecording,
            //            buttonStudies,
            //            buttonNewPatient,
            //            buttonLoad,
            //            buttonPrint,

            //            buttonCursors,
            //            buttonNotes,
            //            buttonSummary,
            //            buttonAutoscan,
            //        },
            //        // TB7_Monitoring_Replay
            //        new ToolBarBtn [] 
            //        { 
            //            buttonPlay,
            //            buttonScrollBackWard,
            //            buttonPause,
            //            buttonScrollForward,
            //            buttonStudies,
            //            buttonNewPatient,
            //            buttonLoad,
            //            buttonPrint,
            //            buttonCursors,
            //            buttonNotes, 
            //            buttonAddTrend,
            //            buttonAutoscan
            //        },

            //        // TB8_MonitoringPause
            //        new ToolBarBtn [] 
            //        { 
            //            buttonFreeze,
            //            buttonPrint,
            //            buttonHitsDetection, 
            //            buttonAddSpectrum,
            //            buttonAddTrend,
            //            buttonAutoscan,
            //            buttonCursors,
            //            buttonStartRecording,
            //            buttonStopRecording,
            //            buttonPause,
            //        },

            //        // TB9_ReplayUnfreeze
            //        new ToolBarBtn [] 
            //        { 
            //            buttonPlay,
            //            buttonPause,
            //            buttonStopRecording,
            //        },

            //        // TB10_MonitoringIntracranialFreeze
            //        new ToolBarBtn [] 
            //        { 
            //            buttonFreeze,
            //            buttonStudies,
            //            buttonNewPatient,
            //            buttonLoad,
            //            buttonCursors,
            //            buttonNotes,
            //            buttonHitsDetection, 
            //            buttonAddSpectrum,
            //            buttonAddTrend,
            //            buttonAutoscan,
            //            buttonStartRecording,
            //            buttonStopRecording,
            //            buttonPatientRep
            //        },
            //        // TB11_MonitoringIntracranialReplay
            //        new ToolBarBtn [] 
            //        { 
            //            buttonPlay,
            //            buttonScrollBackWard,
            //            buttonPause,
            //            buttonScrollForward,
            //            buttonStudies,
            //            buttonNewPatient,
            //            buttonSave,
            //            buttonLoad,
            //            buttonPrint,
            //            buttonCursors,
            //            buttonNotes, 
            //            buttonAddTrend,
            //            buttonAutoscan,
            //            buttonPatientRep
            //        },
            //    };

            //// Each button has an enable/disable state that is based on the 
            //// current toolbar, and if there is any unsaved data (dirty-falg)
            //// This state data is stored as a multi domentional array, at the
            //// Tag field of the button.
            //// 
            //// Note:
            //// Buttons that are always enabled don't have this state-data at all!
            //// 

            //// Print button
            //buttonPrint.Tag = new bool[][] {
            //    // TB1_DiagnosticUnfreeze
            //    new bool [] { true, true },
            //    // TB2_DiagnosticFreeze
            //    new bool [] { false, true },
            //    // TB3_Summary
            //    new bool [] { true, true },
            //    // TB4_MonitoringUnfreeze
            //    new bool [] { true, true },
            //    // TB5_MonitoringFreeze
            //    new bool [] { false, true },
            //    // TB6_Replay
            //    new bool [] { true, true },
            //    //monmon
            //    // TB7_MonitoringReplay
            //    new bool [] { true, true },
            //    // TB8_MonitoringPause
            //    new bool [] { false, true }
            //                                };

            //// Cursors button
            //buttonCursors.Tag = new bool[][] {
            //    // TB1_DiagnosticUnfreeze
            //    new bool [] { true, true },
            //    // TB2_DiagnosticFreeze
            //    new bool [] { false, true },
            //    // TB3_Summary
            //    new bool [] { true, true },
            //    // TB4_MonitoringUnfreeze
            //    new bool [] { true, true },
            //    // TB5_MonitoringFreeze
            //    new bool [] { false, true },
            //    // TB6_Replay
            //    new bool [] { true, true },
            //    // TB7_MonitoringReplay
            //    new bool [] { true, true },
            //    // TB8_MonitoringPause
            //    new bool [] { false, true }
            //                                };

            //// Notes button
            //buttonNotes.Tag = new bool[][] {
            //    // TB1_DiagnosticUnfreeze
            //    new bool [] { true, true },
            //    // TB2_DiagnosticFreeze
            //    new bool [] { true, true },
            //    // TB3_Summary
            //    new bool [] { true, true },
            //    // TB4_MonitoringUnfreeze
            //    new bool [] { true, true },
            //    // TB5_MonitoringFreeze
            //    new bool [] { true, true },
            //    // TB6_Replay
            //    new bool [] { true, true },
            //    // TB7_MonitoringReplay
            //    new bool [] { true, true },
            //    // TB8_MonitoringPause
            //    new bool [] { true, true }
            //                                  };

            //// Send-to button
            //buttonSendTo.Tag = new bool[][] {
            //    // TB1_DiagnosticUnfreeze
            //    new bool [] { true, true },
            //    // TB2_DiagnosticFreeze
            //    new bool [] { true, true },
            //    // TB3_Summary
            //    new bool [] { false, true },
            //    // TB4_MonitoringUnfreeze
            //    new bool [] { true, true },
            //    // TB5_MonitoringFreeze
            //    new bool [] { false, true },
            //    // TB6_Replay
            //    new bool [] { true, true },
            //    // TB7_MonitoringReplay
            //    new bool [] { true, true },
            //    // TB8_MonitoringPause
            //    new bool [] { true, true }
            //                                };

            //// Save button
            //buttonSave.Tag = new bool[][] {
            //    // TB1_DiagnosticUnfreeze
            //    new bool [] { true, true },
            //    // TB2_DiagnosticFreeze
            //    new bool [] { false, true },
            //    // TB3_Summary
            //    new bool [] { true, true },
            //    // TB4_MonitoringUnfreeze
            //    new bool [] { true, true },
            //    // TB5_MonitoringFreeze
            //    new bool [] { true, true },
            //    // TB6_Replay
            //    new bool [] { true, true },
            //    // TB7_MonitoringReplay
            //    new bool [] { true, true },
            //    // TB8_MonitoringPause
            //    new bool [] { true, true }
            //};
        }

        /// <summary>Constructor</summary>
        private void initToolBarButtonsArray()
        {
            m_toolBarBtnArr = new ToolBarBtn[][]
			{
				// TB1_DiagnosticUnfreeze
				new ToolBarBtn [] 
				{ 
					buttonFreeze,
					buttonPrint,
					buttonHitsDetection,
					buttonAddSpectrum,
					buttonAutoscan,
					buttonNextFunction
				},

				// TB2_DiagnosticFreeze
				new ToolBarBtn [] 
				{ 
					buttonFreeze,
					buttonStudies,
					buttonNewPatient,
					buttonLoad,
					buttonPrint,
					buttonCursors,
					buttonNotes,
					buttonSummary,
					buttonNextFunction,
					buttonHitsDetection, 
					buttonAddSpectrum,
					buttonAutoscan, 
					buttonScrollBackWard,
					buttonScrollForward
				},

				// TB3_Summary
				new ToolBarBtn [] 
				{ 
					buttonStudies,
					buttonNewPatient,
					buttonLoad,
					buttonPrint,
					buttonNotes,
					buttonClinicalParameters,
					buttonReturn,
					buttonSave,
					buttonPatientRep
				},

				// TB4_MonitoringUnfreeze
				new ToolBarBtn [] 
				{ 
					buttonFreeze,
					buttonPrint,
					buttonHitsDetection, 
					buttonAddSpectrum,
					buttonAddTrend,
					buttonAutoscan,
					buttonStartRecording,
					buttonStopRecording
				},

				// TB5_MonitoringFreeze
				new ToolBarBtn [] 
				{ 
					buttonFreeze,
					buttonStudies,
					buttonNewPatient,
					buttonLoad,
					buttonCursors,
					buttonNotes,
					buttonHitsDetection, 
					buttonAddSpectrum,
					buttonAddTrend,
					buttonAutoscan,
					buttonStartRecording,
					buttonStopRecording
				},

				// TB6_ReplayFreeze
				new ToolBarBtn [] 
				{ 
					buttonPlay,
					buttonPause,
					buttonStopRecording,
					buttonStudies,
					buttonNewPatient,
					buttonLoad,
					buttonPrint,

					buttonCursors,
					buttonNotes,
					buttonSummary,
					buttonAutoscan,
				},
				// TB7_Monitoring_Replay
				new ToolBarBtn [] 
				{ 
					buttonPlay,
					buttonScrollBackWard,
					buttonPause,
					buttonScrollForward,
					buttonStudies,
					buttonNewPatient,
					buttonLoad,
					buttonPrint,
					buttonCursors,
					buttonNotes, 
					buttonAddTrend,
					buttonAutoscan
    			},

				// TB8_MonitoringPause
				new ToolBarBtn [] 
				{ 
					buttonFreeze,
					buttonPrint,
					buttonHitsDetection, 
					buttonAddSpectrum,
					buttonAddTrend,
					buttonAutoscan,
					buttonCursors,
					buttonStartRecording,
					buttonStopRecording,
					buttonPause,
				},

				// TB9_ReplayUnfreeze
				new ToolBarBtn [] 
				{ 
					buttonPlay,
					buttonPause,
					buttonStopRecording,
				},

				// TB10_MonitoringIntracranialFreeze
				new ToolBarBtn [] 
				{ 
					buttonFreeze,
					buttonStudies,
					buttonNewPatient,
					buttonLoad,
					buttonCursors,
					buttonNotes,
					buttonHitsDetection, 
					buttonAddSpectrum,
					buttonAddTrend,
					buttonAutoscan,
					buttonStartRecording,
					buttonStopRecording,
                    buttonPatientRep
				},
				// TB11_MonitoringIntracranialReplay
				new ToolBarBtn [] 
				{ 
					buttonPlay,
					buttonScrollBackWard,
					buttonPause,
					buttonScrollForward,
					buttonStudies,
					buttonNewPatient,
                    buttonSave,
					buttonLoad,
					buttonPrint,
					buttonCursors,
					buttonNotes, 
					buttonAddTrend,
					buttonAutoscan,
                    buttonPatientRep
    			},
            };

            // Each button has an enable/disable state that is based on the 
            // current toolbar, and if there is any unsaved data (dirty-falg)
            // This state data is stored as a multi domentional array, at the
            // Tag field of the button.
            // 
            // Note: Buttons that are always enabled don't have this state-data at all!

            // Print button
            buttonPrint.Tag = new bool[][] 
            {
				// TB1_DiagnosticUnfreeze
				new bool [] { true, true },
				// TB2_DiagnosticFreeze
				new bool [] { false, true },
				// TB3_Summary
				new bool [] { true, true },
				// TB4_MonitoringUnfreeze
				new bool [] { true, true },
				// TB5_MonitoringFreeze
				new bool [] { false, true },
				// TB6_Replay
				new bool [] { true, true },
				//monmon
				// TB7_MonitoringReplay
				new bool [] { true, true },
				// TB8_MonitoringPause
				new bool [] { false, true }
            };

            // Cursors button
            buttonCursors.Tag = new bool[][] 
            {
				// TB1_DiagnosticUnfreeze
				new bool [] { true, true },
				// TB2_DiagnosticFreeze
				new bool [] { false, true },
				// TB3_Summary
				new bool [] { true, true },
				// TB4_MonitoringUnfreeze
				new bool [] { true, true },
				// TB5_MonitoringFreeze
				new bool [] { false, true },
				// TB6_Replay
				new bool [] { true, true },
				// TB7_MonitoringReplay
			    new bool [] { true, true },
				// TB8_MonitoringPause
				new bool [] { false, true }
            };

            // Notes button
            buttonNotes.Tag = new bool[][] 
            {
				// TB1_DiagnosticUnfreeze
				new bool [] { true, true },
				// TB2_DiagnosticFreeze
				new bool [] { true, true },
				// TB3_Summary
				new bool [] { true, true },
				// TB4_MonitoringUnfreeze
				new bool [] { true, true },
				// TB5_MonitoringFreeze
				new bool [] { true, true },
				// TB6_Replay
				new bool [] { true, true },
				// TB7_MonitoringReplay
				new bool [] { true, true },
				// TB8_MonitoringPause
				new bool [] { true, true }
            };

            // Send-to button
            buttonSendTo.Tag = new bool[][] 
            {
				// TB1_DiagnosticUnfreeze
				new bool [] { true, true },
				// TB2_DiagnosticFreeze
				new bool [] { true, true },
				// TB3_Summary
				new bool [] { false, true },
				// TB4_MonitoringUnfreeze
				new bool [] { true, true },
				// TB5_MonitoringFreeze
				new bool [] { false, true },
				// TB6_Replay
				new bool [] { true, true },
				// TB7_MonitoringReplay
				new bool [] { true, true },
				// TB8_MonitoringPause
				new bool [] { true, true }
            };

            // Save button
            buttonSave.Tag = new bool[][] 
            {
				// TB1_DiagnosticUnfreeze
				new bool [] { true, true },
				// TB2_DiagnosticFreeze
				new bool [] { false, true },
				// TB3_Summary
				new bool [] { true, true },
				// TB4_MonitoringUnfreeze
				new bool [] { true, true },
				// TB5_MonitoringFreeze
				new bool [] { true, true },
				// TB6_Replay
				new bool [] { true, true },
				// TB7_MonitoringReplay
				new bool [] { true, true },
				// TB8_MonitoringPause
				new bool [] { true, true }
            };
        }

        private GlobalTypes.TooolBarType CurrentToolBarType { get; set; }

        public int ProbeLocation16Mhz { get; private set; }

        /// <summary>Show toolbar</summary>
        /// <param name="type"></param>
        public void ShowToolbar(GlobalTypes.TooolBarType type)
        {
            if (MainForm.Instance.CurrentStudyName.StartsWith("Monitoring Intracranial"))
            {
                if (type == GlobalTypes.TooolBarType.TB5_MonitoringFreeze)
                    type = GlobalTypes.TooolBarType.TB10_MonitoringIntracranialFreeze;
                else if (type == GlobalTypes.TooolBarType.TB7_MonitoringReplay)
                    type = GlobalTypes.TooolBarType.TB11_MonitoringIntracranialReplay;
            }

            if (this.CurrentToolBarType == type)
                return;

            this.CurrentToolBarType = type;

            this.SuspendLayout();
            this.panel2.SuspendLayout();

            for (int j = panel2.Controls.Count - 1; j >= 0; j--)
                if (panel2.Controls[j] is ToolBarBtn)
                    panel2.Controls.RemoveAt(j);

            for (int i = 0; i < this.m_toolBarBtnArr[(int)type].Length; i++)
            {
                // We need to revert the adding order
                int j = m_toolBarBtnArr[(int)type].Length - i - 1;

                //Set features invisible refers to the authorization manager.
                if ((!GlobalSettings.AMhits && this.m_toolBarBtnArr[(int)type][j].ToolTip == "Hits Detection")
                    || (!GlobalSettings.AMautoscan && this.m_toolBarBtnArr[(int)type][j].ToolTip == "M-Mode"))
                    continue;

                this.panel2.Controls.Add(this.m_toolBarBtnArr[(int)type][j]);
                if (this.m_toolBarBtnArr[(int)type][j].LeftDoc != -1)
                {
                    this.m_toolBarBtnArr[(int)type][j].LeftDoc = this.m_toolBarBtnArr[(int)type][j].LeftDoc;
                    this.m_toolBarBtnArr[(int)type][j].Dock = DockStyle.None;
                }
                else
                    this.m_toolBarBtnArr[(int)type][j].Dock = DockStyle.Left;
            }

            this.EnableButtons();

            this.ResumeLayout();
            this.panel2.ResumeLayout();
        }

        /// <summary>Enable / disable buttons</summary>
        public void EnableButtons()
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((Action)EnableButtons);
                return;
            }

            int ii = (int)CurrentToolBarType;
            if (CurrentToolBarType == GlobalTypes.TooolBarType.TB10_MonitoringIntracranialFreeze)
                ii = (int)GlobalTypes.TooolBarType.TB5_MonitoringFreeze;
            else if (CurrentToolBarType == GlobalTypes.TooolBarType.TB11_MonitoringIntracranialReplay)
                ii = (int)GlobalTypes.TooolBarType.TB7_MonitoringReplay;

            foreach (Control c in this.panel2.Controls)
            {
                if ((c is ToolBarBtn) && (c.Tag != null))
                {
                    bool[][] StateData = (bool[][])c.Tag;
                    bool b =
                        StateData[ii]
                        [Convert.ToInt32(LayoutManager.theLayoutManager.ExaminationDirtyFlag)];
                    c.Enabled = b;
                }
            }

            if (this.Parent.Name != "SummaryScreen")
                if (LayoutManager.theLayoutManager.StudyType != GlobalTypes.TStudyType.eBilateral)
                {
                    if (LayoutManager.SystemMode == GlobalTypes.TSystemModes.Replay)
                    {
                        this.comboBoxProbes.ProbesCombobox_TurnActivativation(false);
                    }
                    else
                    {
                        //REMOVE COMBO WHEN in ONLINE
                        this.comboBoxProbes.ProbesCombobox_TurnActivativation(
                            LayoutManager.SystemMode == GlobalTypes.TSystemModes.Offline);
                    }
                }
            if (MainForm.Instance.CurrentStudyName == "Intraoperative")
                this.comboBoxProbes.Enabled = false;
            if (LayoutManager.SystemLayoutMode == GlobalTypes.TSystemLayoutMode.Monitoring)
                this.comboBoxProbes.Visible = false;
        }

        public void Init()
        {
            int i = 0;
            foreach (Probe p in ProbesCollection.Instance.Probes)
            {
                this.comboBoxProbes.AddItem(p.Color, p.Name, p.ID);
                if (p.Name == "16Mhz PW")
                    ProbeLocation16Mhz = i;
                i++;
            }
            ReInit();
        }

        public void DisableButtonsInMonitoringOnlineMode()
        {
            EnableButtonsInMonitoringOnlineMode(false);
        }

        public void EnableButtonsInMonitoringOnlineMode()
        {
            EnableButtonsInMonitoringOnlineMode(true);
        }

        public delegate void EnableButtonsDelegate(bool enabled);
        public void EnableButtonsInMonitoringOnlineMode(bool enabled)
        {
            if (InvokeRequired)
            {
                this.BeginInvoke((EnableButtonsDelegate)EnableButtonsInMonitoringOnlineMode, enabled);
                return;
            }

            buttonStudies.Enabled = enabled;
            buttonNewPatient.Enabled = enabled;
            buttonLoad.Enabled = enabled;
        }

        public void ReInit()
        {
            if (InvokeRequired)
            {
                this.BeginInvoke((Action)ReInit);
                return;
            }

            switch (LayoutManager.theLayoutManager.StudyType)
            {
                case GlobalTypes.TStudyType.eBilateral:
                    this.comboBoxProbes.Visible = false;
                    break;

                case GlobalTypes.TStudyType.eUnilateral:
                    //sergey
                    if (LayoutManager.theLayoutManager.ReplayMode)
                        this.comboBoxProbes.Visible = false;
                    else
                    {
                        if (LayoutManager.SystemLayoutMode != GlobalTypes.TSystemLayoutMode.MonitoringLoad && LayoutManager.SystemLayoutMode != GlobalTypes.TSystemLayoutMode.Monitoring)
                            this.comboBoxProbes.Visible = true;
                    }
                    break;
                case GlobalTypes.TStudyType.eMultifrequency:
                    this.comboBoxProbes.Visible = true;
                    break;
            }
        }

        #region Event Handlers

        private void comboBoxSensitivity_SelectionChangeCommitted(object sender, System.EventArgs e)
        {
            ComboBox combo = sender as ComboBox;
            ExceptionPublisherLog4Net.ChangeComboValueLog(combo.Name, combo.Parent.Name, combo.Text);
        }

        private void comboBoxSensitivity_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (MainForm.Instance != null)              //Ofer, 1.18.2.10
                MainForm.Instance.SensitivityChanged(0);
        }

        private void comboBoxSensitivitySecondary_SelectionChangeCommitted(object sender, System.EventArgs e)
        {
            ComboBox combo = sender as ComboBox;
            ExceptionPublisherLog4Net.ChangeComboValueLog(combo.Name, combo.Parent.Name, combo.Text);
        }

        private void comboBoxSensitivitySecondary_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (MainForm.Instance != null)              //Ofer, 1.18.2.10
                MainForm.Instance.SensitivityChanged(1);
        }

        #endregion Event Handlers
    }
}
