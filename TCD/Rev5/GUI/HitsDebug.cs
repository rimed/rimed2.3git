using System;

using TCD2003.DSP;
using TCD2003.Utils;

namespace TCD2003.GUI
{
    /// <summary>
    /// Summary description for HitsDebug.
    /// </summary>
    public partial class HitsDebug : System.Windows.Forms.Form
    {
        private double m_lastIndex;

        /// <summary>
        /// Constructor
        /// </summary>
        public HitsDebug()
        {
            this.InitializeComponent();
            var strRes = ResourceManagerWrapper.GetWrapper(MainForm.ResourceManager); 
            this.label1.Text = strRes.GetString("Index  Type  Energy  Speed  Duration");
            this.label2.Text = strRes.GetString("Index  Type  Energy  Speed  Duration");
            this.lblHistory.Text = strRes.GetString("History");
            this.Text = strRes.GetString("Hits detection debug");

            this.m_lastIndex = -1;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.listBoxIndex.Items.Clear();
            String str = "";
            uint index, type, energy, speed, duration;

            //See if there is new Data in this case update form
            if (Dsps.Instance[0].DspExists)
            {
                for (uint i = 0; i < 5; i++)
                {
                    index = Dsps.Instance[0].ReadDsp2PcMbx(20 + i * 5);
                    type = Dsps.Instance[0].ReadDsp2PcMbx(20 + i * 5 + 1);
                    energy = Dsps.Instance[0].ReadDsp2PcMbx(20 + i * 5 + 2);
                    speed = Dsps.Instance[0].ReadDsp2PcMbx(20 + i * 5 + 3);
                    duration = Dsps.Instance[0].ReadDsp2PcMbx(20 + i * 5 + 4);
                    str = index.ToString() + "\t" +
                        type.ToString() + "\t" +
                        energy.ToString() + "\t" +
                        speed.ToString() + "\t" +
                        duration.ToString();
                    this.listBoxIndex.Items.Add(str);

                    if ((index > this.m_lastIndex) && (type != 0))
                    {
                        this.m_lastIndex++;
                        this.listBoxHistory.Items.Add(str);
                    }
                }
            }
            else
            {
                this.timer1.Interval = 2000;
                for (uint i = 0; i < 5; i++)
                {
                    Random rnd = new Random();
                    index = i;
                    str = index.ToString() + "\t" +
                        (rnd.Next(0, 2)).ToString() + "\t" +
                        (rnd.Next(0, 255)).ToString() + "\t" +
                        (rnd.Next(0, 255)).ToString() + "\t" +
                        (rnd.Next(0, 255)).ToString();
                    this.listBoxIndex.Items.Add(str);

                    if (index > this.m_lastIndex)
                    {
                        this.m_lastIndex = (double)index;
                        this.listBoxHistory.Items.Insert(0, str);
                    }
                }
            }
        }

        private void HitsDebug_Load(object sender, EventArgs e)
        {
            this.m_lastIndex = -1;
            this.listBoxHistory.Items.Clear();
        }
    }
}
