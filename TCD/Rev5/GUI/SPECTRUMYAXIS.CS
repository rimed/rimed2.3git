using System;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace TCD2003.GUI
{
    /// <summary>
    /// Summary description for SpectrumYAxis.
    /// </summary>
    public partial class SpectrumYAxis : UserControl
    {
        #region Private Fields

        #endregion Private Fields

        public SpectrumYAxis()
        {
            InitializeComponent();

            gradientPanel1.BackgroundColor = new Syncfusion.Drawing.BrushInfo(Syncfusion.Drawing.PatternStyle.None, GlobalSettings.P2, GlobalSettings.P3);
            gradientPanel1.BackColor = GlobalSettings.P10;

            foreach (Control c in gradientPanel1.Controls)
            {
                c.Font = GlobalSettings.F2;
            }
        }

        public double MinValue { get; set; }

        public double MaxValue { get; set; }

        public Color BkColor
        {
            set
            {
                gradientPanel1.BackColor = value;
            }
        }

        public void UpdateAxis()
        {
            if (gradientPanel1.Controls.Count == 0)
                return;

            if (InvokeRequired)
            {
                BeginInvoke((Action)UpdateAxis);
                return;
            }

            int y = this.Height;
            foreach (Control c in this.gradientPanel1.Controls)
                c.Visible = false;

            double pixelFact = y / (this.MaxValue - this.MinValue);
            int yLoc = (int)((this.MaxValue - 0) * pixelFact);
            
            double diff;
            switch (y)
            {
                case 512:
                    diff = (this.MaxValue - this.MinValue) / 7.0;
                    break;
                case 256:
                    diff = (this.MaxValue - this.MinValue) / 6.0;
                    break;
                case 128:
                    diff = (this.MaxValue - this.MinValue) / 5.0;
                    break;
                case 64:
                    diff = (this.MaxValue - this.MinValue) / 3.0;
                    break;
                default:
                    if (y < 100)
                        diff = (MaxValue - MinValue) / 3.0;
                    else
                        diff = (MaxValue - MinValue) / 5.0;
                    break;
            }
            System.Globalization.NumberFormatInfo nfi = new System.Globalization.CultureInfo("en-US", false).NumberFormat;
            nfi.NumberGroupSeparator = string.Empty;

            if (diff > 1000)
            {
                diff = diff - diff % 1000;  //Ofer, BugFix: mod div by 1000 (insted of 10000)
                nfi.NumberDecimalDigits = 0;
            }
            else if (diff > 5)
            {
                diff = diff - diff % 5;
                nfi.NumberDecimalDigits = 0;
            }
            else
            {
                if ((diff * 10 - (diff * 10) % 5) / 10 == 0.0)
                {
                    if ((diff * 10 - (diff * 10) % 3) / 10 == 0.0)
                        diff = (diff * 10 - (diff * 10) % 1) / 10;
                    else
                        diff = (diff * 10 - (diff * 10) % 3) / 10;
                }
                else
                    diff = (diff * 10 - (diff * 10) % 5) / 10;
                nfi.NumberDecimalDigits = 1;
            }
            
            int count = 1;
            double value = 0;

            // when this function is calling and the window isnot display the application throw an exception. this is the reason for the following query.
            gradientPanel1.Controls[0].Location = new Point(label1.Location.X, yLoc - label1.Height / 2);
            gradientPanel1.Controls[0].Text = value.ToString("n", nfi) + "-";
            if (yLoc < Height - label1.Height / 2 && yLoc > label1.Height / 2)
            {
                gradientPanel1.Controls[0].Visible = true;
            }

            while (value < MaxValue && diff > 0.0) //Ofer: posible infinit loop BugFix. While condition changed to: ....diff > 0.0, insted of: ....(value != 0.0 || diff != 0.0)
            {
                value += diff;

                yLoc = (int)((MaxValue - value) * pixelFact);
                if (yLoc < label1.Height || yLoc > Height - label1.Height)
                    continue;

                gradientPanel1.Controls[count].Location = new Point(label1.Location.X, yLoc - label1.Height / 2);
                gradientPanel1.Controls[count].Text = value.ToString("n", nfi) + "-";
                gradientPanel1.Controls[count].Visible = true;
                count++;

                if (count >= gradientPanel1.Controls.Count)
                    gradientPanel1.Controls.Add(new TextBox());
            }

            value = 0;
            while (value > MinValue && diff > 0.0) //Ofer: posible infinit loop BugFix. While condition changed to: ....diff > 0.0, insted of: ....(value != 0.0 || diff != 0.0)
            {
                value -= diff;

                yLoc = (int)((MaxValue - value) * pixelFact);
                if (yLoc > Height - label1.Height)
                    continue;

                gradientPanel1.Controls[count].Location = new Point(label1.Location.X, yLoc - this.label1.Height / 2);
                gradientPanel1.Controls[count].Text = value.ToString("n", nfi) + "-";
                gradientPanel1.Controls[count].Visible = true;
                count++;

                if (count >= gradientPanel1.Controls.Count)
                    gradientPanel1.Controls.Add(new TextBox());
            }
        }

        public void CopyFrom(SpectrumYAxis src)
        {
            this.MinValue = src.MinValue;
            this.MaxValue = src.MaxValue;
            this.UpdateAxis();
        }

        private void SpectrumYAxis_SizeChanged(object sender, System.EventArgs e)
        {
            foreach (Control c in this.gradientPanel1.Controls)
            {
                c.Location = new Point(0, 0);
                c.Width = Width;
            }
            this.UpdateAxis();
        }

        private void PaintGraphics(Graphics g, Rectangle paintRect, int height)
        {
            using (Brush b = new SolidBrush(GlobalSettings.P2))
            {
                g.FillRectangle(b, paintRect);
            }

            int y = height;
            foreach (Control c in gradientPanel1.Controls)
            {
                if ((c.Top - c.Height) > -5)
                {
                    System.Drawing.Rectangle r = new Rectangle(c.Left, c.Top, c.Width, c.Height);
                    g.DrawString(c.Text, this.Font, Brushes.Black, r, System.Drawing.StringFormat.GenericDefault);
                }
            }
        }

        public void Serialize(Stream ms)
        {
            var formatter = new BinaryFormatter();
            formatter.Serialize(ms, MinValue);
            formatter.Serialize(ms, MaxValue);
        }
        public void Deserialize(Stream ms)
        {
            var formatter = new BinaryFormatter();
            MinValue = (double)formatter.Deserialize(ms);
            MaxValue = (double)formatter.Deserialize(ms);
            UpdateAxis();
        }
        
        public Image Convert2Image()
        {
            using (var ms = new MemoryStream())
            {
                using (var b = new Bitmap(this.Width, this.Height))
                {
                    using (var g = Graphics.FromImage(b))
                    {
                        PaintGraphics(g, new Rectangle(0, 0, Width, Height), Height);
                        b.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                }
                Image image = Image.FromStream(ms);

                return image;
            }
        }
    }
}
