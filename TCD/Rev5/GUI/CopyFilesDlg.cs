using System;
using System.Collections.Generic;

using TCD2003.Utils;

namespace TCD2003.GUI
{
    /// <summary>
    /// Summary description for CopyFilesDlg.
    /// </summary>
    public class CopyFilesDlg
    {
        public enum TFileOperation { eMove, eCopy }

        private List<string> m_source = null;
        private List<string> m_dest = null;
        private IntPtr m_parentHandle = IntPtr.Zero;

        public void AddFile(String src, String dest)
        {
            m_source.Add(src);
            m_dest.Add(dest);
        }

        public CopyFilesDlg(IntPtr pHandle)
        {
            m_parentHandle = pHandle;
            m_source = new List<string>();
            m_dest = new List<string>();
        }

        public bool DoOperation(TFileOperation operation)
        {
            ShellFileOperation fo = new ShellFileOperation();

            //Operation does Copy for Copy and Copy+Delete for Move
            fo.Operation = ShellFileOperation.FileOperations.FO_COPY;
            fo.OwnerWindow = m_parentHandle;

            fo.SourceFiles = m_source;
            fo.DestFiles = m_dest;

            bool res = fo.DoOperation();

            if (res && operation == TFileOperation.eMove)
            {
                fo.Operation = Utils.ShellFileOperation.FileOperations.FO_DELETE;
                res = res && fo.DoOperation();
            }

            return res;
        }
    }
}
