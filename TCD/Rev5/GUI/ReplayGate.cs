using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

using TCD2003.DSP;

namespace TCD2003.GUI
{
	/// <summary>
	/// Summary description for ReplayGate.
	/// </summary>
	public class ReplayGate : System.Windows.Forms.Form
	{
		private MainForm m_MainForm = MainForm.Instance;
		private ReplayLayout m_ReplayLayout = new ReplayLayout();

//		static private readonly ReplayGate m_sReplayGate = new ReplayGate();

		private System.Windows.Forms.MenuItem menuItem2;
		private TCD2003.GUI.ToolBarPanel toolBarPanel1;
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.Panel panel1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
//		static public MainForm mainForm
//		{
//			get { return m_MainForm; }
//			set {m_MainForm = value; }
//		}
		
//		static public ReplayGate Instance 
//		{
//			get
//			{ 
//				return m_sReplayGate;
//			}
//		}
		public ReplayGate()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			toolBarPanel1.ButtonFreeze.Click += new EventHandler(buttonFreezeToggle_Click);
			toolBarPanel1.ButtonPrint.Click += new EventHandler(buttonPrint_Click);
			toolBarPanel1.ButtonCursors.Click += new EventHandler(buttonCurrsor_Click);
			toolBarPanel1.ButtonNotes.Click += new EventHandler(buttonNotes_Click);
			toolBarPanel1.ButtonSummery.Click += new EventHandler(buttonSummary_Click);
			toolBarPanel1.ButtonTD.Click += new EventHandler(buttonTD_Click);
			toolBarPanel1.ButtonPlay.Click += new EventHandler(buttonPlay_Click);
			toolBarPanel1.ButtonStopRecording.Click += new EventHandler(buttonStop_Click);
			toolBarPanel1.ButtonPause.Click += new EventHandler(buttonPause_Click);
			toolBarPanel1.ButtonExit.Click += new EventHandler(buttonExit_Click);


			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ReplayGate));
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.toolBarPanel1 = new TCD2003.GUI.ToolBarPanel();
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.panel1 = new System.Windows.Forms.Panel();
			this.SuspendLayout();
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 1;
			this.menuItem2.Text = "Exit";
			// 
			// toolBarPanel1
			// 
			this.toolBarPanel1.BackColor = System.Drawing.SystemColors.Control;
			this.toolBarPanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("toolBarPanel1.BackgroundImage")));
			this.toolBarPanel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.toolBarPanel1.Location = new System.Drawing.Point(0, 0);
			this.toolBarPanel1.Name = "toolBarPanel1";
			this.toolBarPanel1.Size = new System.Drawing.Size(1018, 36);
			this.toolBarPanel1.TabIndex = 1;
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem1,
																					  this.menuItem2});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.Text = "Print";
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(255)), ((System.Byte)(255)), ((System.Byte)(192)));
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(0, 36);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(1018, 679);
			this.panel1.TabIndex = 2;
			// 
			// ReplayGate
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(1018, 715);
			this.ControlBox = false;
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.toolBarPanel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Menu = this.mainMenu1;
			this.MinimizeBox = false;
			this.Name = "ReplayGate";
			this.ShowInTaskbar = false;
			this.Text = "ReplayGate";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ReplayGate_KeyDown);
			this.Load += new System.EventHandler(this.ReplayGate_Load);
			this.ResumeLayout(false);

		}
		#endregion

		#region Button Events Handle
		private void buttonFreezeToggle_Click(object sender, System.EventArgs e)
		{
			ToggleFreezeMode();
		}

		private void buttonPrint_Click(object sender, System.EventArgs e)
		{
			// TODO: Activate the report generator.
		}
		private void buttonCurrsor_Click(object sender, System.EventArgs e)
		{
			// TODO: Will not be implemented for the Beta site.
		}
		private void buttonNotes_Click(object sender, System.EventArgs e)
		{	
			// TODO: Will not be implemented for the Beta site.
		}
		private void buttonSummary_Click(object sender, System.EventArgs e)
		{	
			Exit();
		}
		private void buttonTD_Click(object sender, System.EventArgs e)
		{	
			// TODO:
		}
		private void buttonPlay_Click(object sender, System.EventArgs e)
		{	
			// TODO:
		}
		private void buttonStop_Click(object sender, System.EventArgs e)
		{	
			// TODO:
		}
		private void buttonPause_Click(object sender, System.EventArgs e)
		{	
			// TODO:
		}
		private void buttonExit_Click(object sender, System.EventArgs e)
		{
			Exit();
		}

		#endregion

		public bool Init(SummaryView sv)
		{
			m_ReplayLayout.Init(this);
			m_ReplayLayout.SV = sv;
			return true;
		}
		private void ReplayGate_Load(object sender, System.EventArgs e)
		{
			this.SetDesktopLocation(0,0);
			this.WindowState = FormWindowState.Normal;
			Rectangle ScreenRect = Screen.PrimaryScreen.Bounds;
			this.Left = ScreenRect.Left;
			this.Top = ScreenRect.Top;
			this.Width = ScreenRect.Width;
			this.Height = ScreenRect.Height;
			this.toolBarPanel1.ShowToolbar(GlobalTypes.TooolBarType.TB6_Replay);
			if(m_ReplayLayout.ReplaySV() == true)
			{
				m_ReplayLayout.RecalcLayout();
				m_ReplayLayout.DisplayBmp();
			}
			else
			{
				Exit();
			}
		}
		public System.Windows.Forms.Panel Panel
		{
			get { return panel1; }
		}
		public TCD2003.GUI.ToolBarPanel ToolBarPanel1
		{
			get { return toolBarPanel1; }
		}
		public ComboBox GetTimeComboboxForPanel()
		{
			return toolBarPanel1.ComboBoxTime;
		}

		// volume event [this code is copy from the main form class.
//		public event VolumeChangedHandler VolumeChangedEvent;
//		public delegate void VolumeChangedHandler( object sender );
//		protected void FireVolumeChanged()
//		{
//			if ( VolumeChangedEvent != null )
//				VolumeChangedEvent(this);
//		}
		private void Exit()
		{
			//add this line to fix bug in .NET
			//if this line is missing the second time you load a patient the summary screen hungs
			//on WindowsFormsParkingWindow.
			MainForm.SystemMode = GlobalTypes.TSystemModes.Offline;
			this.toolBarPanel1.Focus();
			this.Close();
		}
		private void ToggleFreezeMode()
		{
			//wait for dsps while they save their data, and only after they finish the system
			//can chage its mode (probaly from offline to online)

			for ( int i = 0; i < Dsps.Instance.m_NumOfDsps; i++ )
			{
				if (Dsps.Instance[i].SaveThread != null)
					Dsps.Instance[i].SaveThread.Join();

			}
			m_ReplayLayout.ModeChanged();
			// Enable/dsiable all features regarding system mode like Setup menu
//			if (MainForm.SystemMode == GlobalTypes.TSystemModes.ReplayOnline)
//				this.parentBarItemSetup.Enabled = false;
//			else 
//				if(MainForm.SystemMode == GlobalTypes.TSystemModes.Offline)
//				this.parentBarItemSetup.Enabled = true;
				
		}

		protected override void WndProc(ref Message m)
		{
			if (m.Msg == 0x0084)
				return;
			base.WndProc (ref m);
		}

		private void ReplayGate_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			System.Diagnostics.Trace.WriteLine("Key Down");
			e.Handled=true;
			switch (e.KeyCode)
			{
				case System.Windows.Forms.Keys.D1:
					ZeroLineDown();
					break;
				case System.Windows.Forms.Keys.D2:
					ZeroLineUp();
					break;
				case System.Windows.Forms.Keys.Space:
					ToggleFreezeMode();
					break;
				case System.Windows.Forms.Keys.Z:
					if(e.Control)
						ToggleBckPeakEnvelope();
					else if (!e.Alt && !e.Shift)
						ToggleFwdPeakEnvelope();
					break;
				case System.Windows.Forms.Keys.X:
					if(e.Control)
						ToggleBckModeEnvelope();
					else if (!e.Alt && !e.Shift)
						ToggleFwdModeEnvelope();
					break;
				case System.Windows.Forms.Keys.C:
					if (e.Control)
						ToggleSpectrumDisplay();
					break;
				case System.Windows.Forms.Keys.OemMinus:
					MainForm.Instance.VolumeDown();
					break;
				case System.Windows.Forms.Keys.Oemplus:
					if (e.Shift)
						MainForm.Instance.VolumeUp();
					break;

				case System.Windows.Forms.Keys.Left:
					if (toolBarPanel1.ComboBoxTime.SelectedIndex < toolBarPanel1.ComboBoxTime.Items.Count)
						toolBarPanel1.ComboBoxTime.SelectedIndex=toolBarPanel1.ComboBoxTime.SelectedIndex+1;
					break;
					//case System.Windows.Forms.Keys.Down:
					//		break;
			}
		}
		private void ZeroLineUp()
		{
			m_ReplayLayout.gateView.Spectrum.Graph.ZeroLinePercent += GlobalSettings.ZeroLineStep;
			if (m_ReplayLayout.gateView !=null)
				m_ReplayLayout.gateView.Spectrum.Graph.ZeroLinePercent += GlobalSettings.ZeroLineStep;
		}
		private void ZeroLineDown()
		{
			m_ReplayLayout.gateView.Spectrum.Graph.ZeroLinePercent -= GlobalSettings.ZeroLineStep;
			if (m_ReplayLayout.gateView !=null)
				m_ReplayLayout.gateView.Spectrum.Graph.ZeroLinePercent -= GlobalSettings.ZeroLineStep;
		}
		private void ToggleSpectrumDisplay()
		{
			m_ReplayLayout.gateView.Spectrum.Graph.DrawSpectrum=!m_ReplayLayout.gateView.Spectrum.Graph.DrawSpectrum;
			if (m_ReplayLayout.gateView !=null)
				m_ReplayLayout.gateView.Spectrum.Graph.DrawSpectrum=!m_ReplayLayout.gateView.Spectrum.Graph.DrawSpectrum;
		}

		private void ToggleFwdPeakEnvelope()
		{
			m_ReplayLayout.gateView.Spectrum.Graph.DrawPeakFwdEnvelope=!m_ReplayLayout.gateView.Spectrum.Graph.DrawPeakFwdEnvelope;
			if (m_ReplayLayout.gateView !=null)
				m_ReplayLayout.gateView.Spectrum.Graph.DrawPeakFwdEnvelope=!m_ReplayLayout.gateView.Spectrum.Graph.DrawPeakFwdEnvelope;
		}
		private void ToggleBckPeakEnvelope()
		{
			m_ReplayLayout.gateView.Spectrum.Graph.DrawPeakBckEnvelope=!m_ReplayLayout.gateView.Spectrum.Graph.DrawPeakBckEnvelope;
			if (m_ReplayLayout.gateView !=null)
				m_ReplayLayout.gateView.Spectrum.Graph.DrawPeakBckEnvelope=!m_ReplayLayout.gateView.Spectrum.Graph.DrawPeakBckEnvelope;
		}
		private void ToggleFwdModeEnvelope()
		{
			m_ReplayLayout.gateView.Spectrum.Graph.DrawModeFwdEnvelope=!m_ReplayLayout.gateView.Spectrum.Graph.DrawModeFwdEnvelope;
			if (m_ReplayLayout.gateView !=null)
				m_ReplayLayout.gateView.Spectrum.Graph.DrawModeFwdEnvelope=!m_ReplayLayout.gateView.Spectrum.Graph.DrawModeFwdEnvelope;
		}
		private void ToggleBckModeEnvelope()
		{
			m_ReplayLayout.gateView.Spectrum.Graph.DrawModeBckEnvelope=!m_ReplayLayout.gateView.Spectrum.Graph.DrawModeBckEnvelope;
			if (m_ReplayLayout.gateView !=null)
				m_ReplayLayout.gateView.Spectrum.Graph.DrawModeBckEnvelope=!m_ReplayLayout.gateView.Spectrum.Graph.DrawModeBckEnvelope;
		}
//		public event volumereplaychangedhandler volumereplaychangedevent;
//		public delegate void volumereplaychangedhandler( object sender );
//		protected void firevolumereplaychanged()
//		{
//			if ( volumereplaychangedevent != null )
//				volumereplaychangedevent(this);
//		}
	}
}
