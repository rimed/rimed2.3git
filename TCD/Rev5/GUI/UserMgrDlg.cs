using System;
using System.Drawing;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

using TCD2003.Utils;

namespace TCD2003.GUI
{
    /// <summary>
    /// Summary description for UserMgrDlg.
    /// </summary>
    public partial class UserMgrDlg : Form
    {
        private const String m_opratorName = "Administrator";
        private const String m_operatorPasswd = "gfsoft";

        public UserMgrDlg()
        {
            InitializeComponent();
            this.BackColor = GlobalSettings.BackgroundDlg;
        }

        private void buttonOK_Click(object sender, System.EventArgs e)
        {
            List<LogField> fields = new List<LogField>();
            fields.Add(new LogField("textBoxUserName", textBoxUserName.Text));
            fields.Add(new LogField("textBoxPassword", textBoxPassword.Text));
            ExceptionPublisherLog4Net.FormActionLog("buttonOK", this.Name, fields);

            if (this.textBoxUserName.Text == m_opratorName && this.textBoxPassword.Text == m_operatorPasswd)
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            else
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}
