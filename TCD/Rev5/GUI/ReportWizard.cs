using System;
using System.Windows.Forms;
using TCD2003.Utils;

namespace TCD2003.GUI
{
    /// <summary>Summary description for ReportWizard. The wizard is a UI for configuration of the patient report From here you can Add/Remove elements from the report</summary>
    public partial class ReportWizard : Form
    {
        public ReportWizard()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            this.BackColor = GlobalSettings.BackgroundDlg;

            dsPatientRepLayout1.Merge(DB.RimedDal.Instance.s_dsPatientRepLayout);

            //Added by Natalie for Simple Report
            rBtn1.Checked = true;

            Microsoft.Win32.RegistryKey myKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Rimed", true);
            if (myKey != null)
            {
                if (myKey.GetValue("OnePageReport") != null)
                {
                    if (Convert.ToBoolean(myKey.GetValue("OnePageReport")))
                    {
                        checkBoxOnePageReport.Checked = true;
                    }
                }

                //RIMD-321: United report for DigiLite and IP
                rBtnNo.Checked = true;
                if (myKey.GetValue("IpImagesPerPage") != null)
                {
                    switch (Convert.ToInt16(myKey.GetValue("IpImagesPerPage")))
                    {
                        case 1:
                            rBtn1.Checked = true;
                            break;
                        case 2:
                            rBtn2.Checked = true;
                            break;
                        case 4:
                            rBtn4.Checked = true;
                            break;
                        default:
                            rBtnNo.Checked = true;
                            break;
                    }
                }
            }
        }

        private void wizardControl1_CancelButton_Click(object sender, System.EventArgs e)
        {
            ExceptionPublisherLog4Net.FormActionLog("wizardControl1_CancelButton", this.Name, null);

            if (MessageBox.Show(MainForm.ResourceManager.GetString("Are you sure you want to cancel the wizard?"), MainForm.ResourceManager.GetString("Confirm"), MessageBoxButtons.YesNo) == DialogResult.Yes)
                Close();

        }

        private void wizardControl1_FinishButton_Click(object sender, System.EventArgs e)
        {
            ExceptionPublisherLog4Net.FormActionLog("wizardControl1_FinishButton", this.Name, null);

            var res = MessageBox.Show(MainForm.ResourceManager.GetString("Are you sure you finished to customize your patient report?"), MainForm.ResourceManager.GetString("Confirmation"), MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

            if (res == DialogResult.Yes)    
            {
                // update the data set for the controls that r not binding.
                DB.RimedDal.Instance.s_dsPatientRepLayout.tb_PatientRepLayout[0].PatientDetails     = radioButtonFullPatientDetails.Checked ? 1 : 0;    

                if (checkBoxSummaryScreen.Checked)
                    DB.RimedDal.Instance.s_dsPatientRepLayout.tb_PatientRepLayout[0].Layout         = (byte)(checkBoxTable.Checked ? 2 : 0);           
                else
                    DB.RimedDal.Instance.s_dsPatientRepLayout.tb_PatientRepLayout[0].Layout         = (byte) (checkBoxTable.Checked ? 1 : 3);

                DB.RimedDal.Instance.s_dsPatientRepLayout.tb_PatientRepLayout[0].DateDisplay        = checkBoxDate.Checked;

                DB.RimedDal.Instance.s_dsPatientRepLayout.tb_PatientRepLayout[0].HospitalDetails    = checkBoxHospitalDetails.Checked;

                DB.RimedDal.Instance.s_dsPatientRepLayout.tb_PatientRepLayout[0].HospitalLogo       = checkBoxHospitalLogo.Checked;

                DB.RimedDal.Instance.s_dsPatientRepLayout.tb_PatientRepLayout[0].TitleDisplay       = checkBoxTitle.Checked;

                DB.RimedDal.Instance.s_dsPatientRepLayout.tb_PatientRepLayout[0].PatientHistory     = checkBoxHistory.Checked;

                DB.RimedDal.Instance.s_dsPatientRepLayout.tb_PatientRepLayout[0].ExaminationHistory = checkBoxExaminationHistory.Checked;

                DB.RimedDal.Instance.s_dsPatientRepLayout.tb_PatientRepLayout[0].Indication         = checkBoxIndication.Checked;

                DB.RimedDal.Instance.s_dsPatientRepLayout.tb_PatientRepLayout[0].Interpretation     = checkBoxInterpretation.Checked;

                DB.RimedDal.Instance.s_dsPatientRepLayout.tb_PatientRepLayout[0].Signature          = checkBoxSignature.Checked;

                //Added by Natalie - writing key for One Page Report to reg 
                //TODO: � ���� ��� ����� ��� �������� ��� winAPI
                Microsoft.Win32.RegistryKey myKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Rimed", true);
                if (myKey == null)
                    myKey = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(@"SOFTWARE\Rimed");

                myKey.SetValue("OnePageReport", checkBoxOnePageReport.Checked);

                //RIMD-321: United report for DigiLite and IP
                //Ofer, v1.18.2.17: default value = rBtnNo.Checked = NO Images
                if (rBtn1.Checked)
                    myKey.SetValue("IpImagesPerPage", 1);
                else if (rBtn2.Checked)
                    myKey.SetValue("IpImagesPerPage", 2);
                else if (rBtn4.Checked)
                    myKey.SetValue("IpImagesPerPage", 4);
                else
                {
                    rBtnNo.Checked = true;
                    myKey.SetValue("IpImagesPerPage", 0);
                }

                Close();
            }
            else if (res == DialogResult.Cancel)
            {
                // close the wizard without saving any change
                Close();
            }
        }


        private void LoadData(object sender, System.EventArgs e)
        {
            // update controls that r not bind to the data Set.
            if (dsPatientRepLayout1.tb_PatientRepLayout[0].PatientDetails == 0)
            {
                radioButtonFullPatientDetails.Checked = false;
                radioButtonMinimalPatientDetails.Checked = true;
            }
            else
            {
                radioButtonFullPatientDetails.Checked = true;
                radioButtonMinimalPatientDetails.Checked = false;
            }

            this.checkBoxSummaryScreen.Checked = false;
            this.checkBoxTable.Checked = false;

            if (dsPatientRepLayout1.tb_PatientRepLayout[0].Layout == 0)
                this.checkBoxSummaryScreen.Checked = true;
            else if (dsPatientRepLayout1.tb_PatientRepLayout[0].Layout == 1)
                this.checkBoxTable.Checked = true;
            else if (dsPatientRepLayout1.tb_PatientRepLayout[0].Layout == 2)
            {
                this.checkBoxSummaryScreen.Checked = true;
                this.checkBoxTable.Checked = true;
            }
            checkBoxIndication.Checked = dsPatientRepLayout1.tb_PatientRepLayout[0].Indication;
            checkBoxInterpretation.Checked = dsPatientRepLayout1.tb_PatientRepLayout[0].Interpretation;
        }

        private void CheckVisibility()
        {
            this.pictureBoxLogo.Visible = checkBoxHospitalLogo.Checked;
            this.labelHospitalDetails.Visible = checkBoxHospitalDetails.Checked;
            this.labelDate.Visible = checkBoxDate.Checked;
            this.labelTitle.Visible = checkBoxTitle.Checked;

            this.labelMinimalPatientDetails.Visible = this.radioButtonMinimalPatientDetails.Checked;
            this.labelFullPatientDeails.Visible = this.radioButtonFullPatientDetails.Checked;

            labelPatientHistory.Visible = checkBoxHistory.Checked;
            labelExaminationHistory.Visible = checkBoxExaminationHistory.Checked;
        }


        private void checkBoxHospitalLogo_CheckedChanged(object sender, System.EventArgs e)
        {
            ExceptionPublisherLog4Net.FormActionLog(string.Format("checkBoxHospitalLogo_CheckedChanged({0})", ((CheckBox)sender).Checked), Name, null); 

            this.pictureBoxLogo.Visible = ((CheckBox)sender).Checked;
        }

        private void checkBoxHospitalDetails_CheckedChanged(object sender, System.EventArgs e)
        {
            ExceptionPublisherLog4Net.FormActionLog(string.Format("checkBoxHospitalDetails_CheckedChanged({0})", ((CheckBox)sender).Checked), Name, null); 
            this.labelHospitalDetails.Visible = ((CheckBox)sender).Checked;
        }

        private void checkBoxDate_CheckedChanged(object sender, System.EventArgs e)
        {
            ExceptionPublisherLog4Net.FormActionLog(string.Format("checkBoxDate_CheckedChanged({0})", ((CheckBox)sender).Checked), Name, null);
            this.labelDate.Visible = ((CheckBox)sender).Checked;
        }

        private void wizardControlPage2_VisibleChanged(object sender, System.EventArgs e)
        {
            CheckVisibility();
        }

        private void checkBoxTitle_CheckedChanged(object sender, System.EventArgs e)
        {
            ExceptionPublisherLog4Net.FormActionLog(string.Format("checkBoxTitle_CheckedChanged({0})", ((CheckBox)sender).Checked), Name, null);
            this.labelTitle.Visible = ((CheckBox)sender).Checked;
        }

        private void radioButtonFullPatientDetails_CheckedChanged(object sender, System.EventArgs e)
        {
            ExceptionPublisherLog4Net.FormActionLog(string.Format("radioButtonFullPatientDetails_CheckedChanged({0})", ((RadioButton)sender).Checked), Name, null); 

            this.labelFullPatientDeails.Visible = ((RadioButton)sender).Checked;
            this.labelMinimalPatientDetails.Visible = !((RadioButton)sender).Checked;
        }

        private void radioButtonMinimalPatientDetails_CheckedChanged(object sender, System.EventArgs e)
        {
            ExceptionPublisherLog4Net.FormActionLog(string.Format("radioButtonMinimalPatientDetails_CheckedChanged({0})", ((RadioButton)sender).Checked), Name, null); 

            this.labelFullPatientDeails.Visible = !((RadioButton)sender).Checked;
            this.labelMinimalPatientDetails.Visible = ((RadioButton)sender).Checked;
        }

        private void checkBoxHistory_CheckedChanged(object sender, System.EventArgs e)
        {
            ExceptionPublisherLog4Net.FormActionLog(string.Format("checkBoxHistory_CheckedChanged({0})", ((CheckBox)sender).Checked), Name, null);

            labelPatientHistory.Visible = ((CheckBox)sender).Checked;
        }

        private void checkBoxExaminationHistory_CheckedChanged(object sender, System.EventArgs e)
        {
            ExceptionPublisherLog4Net.FormActionLog(string.Format("checkBoxExaminationHistory_CheckedChanged({0})", ((CheckBox)sender).Checked), Name, null); 

            labelExaminationHistory.Visible = ((CheckBox)sender).Checked;
        }

        private void checkBoxIndication_CheckedChanged(object sender, System.EventArgs e)
        {
            ExceptionPublisherLog4Net.FormActionLog(string.Format("checkBoxIndication_CheckedChanged({0})", ((CheckBox)sender).Checked), Name, null); 

            labelIndication.Visible = ((CheckBox)sender).Checked;
            EnableIndicationInterpretation();
        }

        private void checkBoxInterpretation_CheckedChanged(object sender, System.EventArgs e)
        {
            ExceptionPublisherLog4Net.FormActionLog(string.Format("checkBoxInterpretation_CheckedChanged({0})", ((CheckBox)sender).Checked), Name, null); 

            labelInterpretation.Visible = ((CheckBox)sender).Checked;
            EnableIndicationInterpretation();
        }

        private void EnableIndicationInterpretation()
        {
            if (checkBoxOnePageReport.Checked)
            {
                if (checkBoxIndication.Checked && checkBoxInterpretation.Checked)
                    checkBoxInterpretation.Checked = false;
                else
                {
                    checkBoxIndication.Enabled = !checkBoxInterpretation.Checked;
                    checkBoxInterpretation.Enabled = !checkBoxIndication.Checked;
                }
            }
        }

        private void checkBoxSignature_CheckedChanged(object sender, System.EventArgs e)
        {
            ExceptionPublisherLog4Net.FormActionLog(string.Format("checkBoxSignature_CheckedChanged({0})", ((CheckBox)sender).Checked), Name, null); 

            labelSignature.Visible = ((CheckBox)sender).Checked;
        }

        private void checkBoxSummaryScreen_CheckedChanged(object sender, System.EventArgs e)
        {
            ExceptionPublisherLog4Net.FormActionLog("checkBoxSummaryScreen_CheckedChanged", Name, null); 

            UpdateImage();
        }

        private void checkBoxTable_CheckedChanged(object sender, System.EventArgs e)
        {
            ExceptionPublisherLog4Net.FormActionLog("checkBoxTable_CheckedChanged", Name, null); 

            UpdateImage();
        }

        private void checkBoxOnePageReport_CheckedChanged(object sender, System.EventArgs e)
        {
            ExceptionPublisherLog4Net.FormActionLog(string.Format("checkBoxOnePageReport_CheckedChanged({0})", ((CheckBox)sender).Checked), Name, null); 

            bool isNotOnePageReport = !((CheckBox)sender).Checked;
            checkBoxHistory.Checked = isNotOnePageReport;
            this.dsPatientRepLayout1.tb_PatientRepLayout[0].PatientHistory = checkBoxHistory.Checked;

            checkBoxExaminationHistory.Checked = isNotOnePageReport;
            this.dsPatientRepLayout1.tb_PatientRepLayout[0].ExaminationHistory = checkBoxExaminationHistory.Checked;

            checkBoxSummaryScreen.Checked = isNotOnePageReport;

            checkBoxTable.Checked = true;
            checkBoxSignature.Checked = true;

            if (!isNotOnePageReport)
                radioButtonMinimalPatientDetails.Checked = true;

            checkBoxHistory.Enabled = isNotOnePageReport;
            checkBoxExaminationHistory.Enabled = isNotOnePageReport;
            checkBoxSummaryScreen.Enabled = isNotOnePageReport;
            if (isNotOnePageReport)
            {
                checkBoxIndication.Enabled = true;
                checkBoxInterpretation.Enabled = true;
            }
            else
                EnableIndicationInterpretation();

            checkBoxTable.Enabled = isNotOnePageReport;
            checkBoxSignature.Enabled = isNotOnePageReport;
            panelIpImages.Enabled = isNotOnePageReport;
        }

        private void UpdateImage()
        {
            if (!checkBoxTable.Checked)
            {
                if (!checkBoxSummaryScreen.Checked)
                    this.labelExaminationDisplay.Image = imageList1.Images[3];	// none.
                else
                    this.labelExaminationDisplay.Image = imageList1.Images[0];	// summary.
            }
            else
            {
                if (!checkBoxSummaryScreen.Checked)
                    this.labelExaminationDisplay.Image = imageList1.Images[1];
                else
                    this.labelExaminationDisplay.Image = imageList1.Images[2];
            }
        }

        private void ReportWizard_Load(object sender, System.EventArgs e)
        {
            var strRes = ResourceManagerWrapper.GetWrapper(MainForm.ResourceManager); 

            lIpImages.Text = strRes.GetString("Manage DigiLite IP Images");
            rBtnNo.Text = strRes.GetString("0 images per page");
            rBtn1.Text = strRes.GetString("1 images per page");
            rBtn2.Text = strRes.GetString("2 images per page");
            rBtn4.Text = strRes.GetString("4 images per page");
            this.wizardControl1.BackButton.Text = "<< " + strRes.GetString("Back");
            this.wizardControl1.CancelButton.Text = strRes.GetString("Cancel");
            this.wizardControl1.FinishButton.Text = strRes.GetString("Finish");
            this.wizardControl1.HelpButton.Text = strRes.GetString("Help");
            this.wizardControl1.NextButton.Text = strRes.GetString("Next") + " >>";
            this.label1.Text = strRes.GetString("Examination Display") + " ";
            this.wizardControlPage1.Description = strRes.GetString("This is the description of the Wizard Page");
            this.wizardControlPage1.Title = strRes.GetString("Page Title");
            this.label7.Text = strRes.GetString("To continue, click Next");
            this.label5.Text = strRes.GetString("Welcome to the Report Generator Wizard.");
            this.wizardControlPage2.Title = strRes.GetString("Report Header");
            this.groupBox1.Text = strRes.GetString("Patient Details");
            this.radioButtonFullPatientDetails.Text = strRes.GetString("Full");
            this.radioButtonMinimalPatientDetails.Text = strRes.GetString("Minimal");
            this.labelTitle.Text = strRes.GetString("Rimed TCD Patient Report");
            this.checkBoxHospitalLogo.Text = strRes.GetString("Hospital Logo");
            this.checkBoxHospitalDetails.Text = strRes.GetString("Hospital Details");
            this.checkBoxDate.Text = strRes.GetString("Date Display");
            this.checkBoxTitle.Text = strRes.GetString("Title Display");
            this.checkBoxHistory.Text = strRes.GetString("Patient History");
            this.checkBoxExaminationHistory.Text = strRes.GetString("Examinations History");
            this.checkBoxOnePageReport.Text = strRes.GetString("One Page Report");
            this.wizardControlPage3.Description = strRes.GetString("This is the description of the Wizard Page");
            this.wizardControlPage3.Title = strRes.GetString("Examination Display") + " ";
            this.checkBoxSummaryScreen.Text = strRes.GetString("Summary Screen");
            this.checkBoxIncludsNormalValues.Text = strRes.GetString("Includes Normal Values");
            this.checkBoxTable.Text = strRes.GetString("Table Report");
            this.wizardControlPage2.Description = this.wizardControlPage4.Description = strRes.GetString("Here you can determine which details will be display in the report.");
            this.wizardControlPage4.Title = strRes.GetString("Report Footer");
            this.checkBoxIndication.Text = strRes.GetString("Indication");
            this.labelIndication.Text = strRes.GetString("Indication") + ":______________________________________________________________________" +
                "___________________________________________________________________";
            this.labelInterpretation.Text = strRes.GetString("Interpretation") + ":__________________________________________________________________" +
                "_______________________________________________________________________";
            this.labelSignature.Text = strRes.GetString("Technologist") + ": ______________" + strRes.GetString("Interpreted by") + ": ____________";
            this.checkBoxInterpretation.Text = strRes.GetString("Interpretation");
            this.checkBoxSignature.Text = strRes.GetString("Signature");
            this.Text = strRes.GetString("Report Generator Wizard");
            LoadData(null, null);
        }

        private void wizardControl1_NextButton_Click(object sender, EventArgs e)
        {
            ExceptionPublisherLog4Net.FormActionLog("wizardControl1_NextButton_Click", Name, null); 
        }

        private void wizardControl1_BackButton_Click(object sender, EventArgs e)
        {
            ExceptionPublisherLog4Net.FormActionLog("wizardControl1_BackButton_Click", Name, null); 
        }

        private void wizardControl1_HelpButton_Click(object sender, EventArgs e)
        {
            ExceptionPublisherLog4Net.FormActionLog("wizardControl1_HelpButton_Click", Name, null); 
        }
    }
}
