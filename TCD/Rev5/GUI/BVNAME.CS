using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using TCD2003.Utils;

namespace TCD2003.GUI
{
    /// <summary>
    /// Summary description for BvName.
    /// </summary>
    public partial class BvName : Form
    {
        #region Private Fields

        private bool m_sideSelected = false;
        private bool m_locationSelected = false;
        private GlobalTypes.ESide m_side;
        private GlobalTypes.TLocation m_location = GlobalTypes.TLocation.eExtracranial;

        #endregion Private Fields

        /// <summary>
        /// Constructor
        /// </summary>
        public BvName()
        {
            InitializeComponent();

            var strRes = ResourceManagerWrapper.GetWrapper(MainForm.ResourceManager); 
            this.lblBVName.Text = strRes.GetString("Blood Vessel Name");
            this.buttonOK.Text = strRes.GetString("OK");
            this.bCancel.Text = strRes.GetString("Cancel");
            this.gbSide.Text = strRes.GetString("Side");
            this.radioButtonLeft.Text = strRes.GetString("Left");
            this.radioButtonRight.Text = strRes.GetString("Right");
            this.radioButtonBothSides.Text = strRes.GetString("Both Sides");
            this.radioButtonMidle.Text = strRes.GetString("Middle");
            this.groupBox1.Text = strRes.GetString("Location");
            this.radioButtonExtracranial.Text = strRes.GetString("Extracranial");
            this.radioButtonPeriphral.Text = strRes.GetString("Periphral");
            this.radioButtonIntracranial.Text = strRes.GetString("Intracranial");
            this.Text = strRes.GetString("Blood Vessel Name");

            this.BackColor = GlobalSettings.BackgroundDlg;
        }

        public String NameOfBV
        {
            get
            {
                return this.textBoxBvName.Text;
            }
        }

        /// <summary>
        /// Blood Vessel Side
        /// </summary>
        public GlobalTypes.ESide BVSide
        {
            get
            {
                return this.m_side;
            }
        }

        /// <summary>
        /// Blood Vessel Location
        /// </summary>
        public GlobalTypes.TLocation BVLocation
        {
            get
            {
                return this.m_location;
            }
        }

        private void location_CheckedChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                if (sender == this.radioButtonExtracranial)
                    this.m_location = GlobalTypes.TLocation.eExtracranial;
                else if (sender == this.radioButtonIntracranial)
                    this.m_location = GlobalTypes.TLocation.eIntracranial;
                else if (sender == this.radioButtonPeriphral)
                    this.m_location = GlobalTypes.TLocation.eIntracranial;
                
                this.m_locationSelected = true;
                
                if (this.m_sideSelected)
                    this.buttonOK.Enabled = true;
            }
        }

        private void side_CheckedChanged(object sender, EventArgs e)
        {
            if (((RadioButton)sender).Checked)
            {
                if (sender == this.radioButtonLeft)
                    this.m_side = GlobalTypes.ESide.Left;
                else if (sender == this.radioButtonRight)
                    this.m_side = GlobalTypes.ESide.Right;
                else if (sender == this.radioButtonBothSides)
                    this.m_side = GlobalTypes.ESide.BothSides;
                else if (sender == this.radioButtonMidle)
                    this.m_side = GlobalTypes.ESide.Middle;

                this.m_sideSelected = true;

                if (this.m_locationSelected)
                    this.buttonOK.Enabled = true;
            }
        }
    }
}
