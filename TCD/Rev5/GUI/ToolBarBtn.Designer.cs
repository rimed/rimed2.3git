﻿namespace TCD2003.GUI
{
    partial class ToolBarBtn
    {
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.PictureBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.button1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(96, 104);
            this.button1.TabIndex = 0;
            this.button1.TabStop = false;
            this.button1.EnabledChanged += new System.EventHandler(this.button1_EnabledChanged);
            this.button1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button1_MouseDown);
            this.button1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button1_MouseUp);
            // 
            // ToolBarBtn
            // 
            this.Controls.Add(this.button1);
            this.Name = "ToolBarBtn";
            this.Size = new System.Drawing.Size(96, 104);
            this.Load += new System.EventHandler(this.ToolBarBtn_Load);
            this.EnabledChanged += new System.EventHandler(ToolBarBtn_EnabledChanged);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button1_MouseDown);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button1_MouseUp);
            ((System.ComponentModel.ISupportInitialize)(this.button1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox button1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.ComponentModel.IContainer components = null;
    }
}