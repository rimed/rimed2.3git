using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Syncfusion.Windows.Forms.Tools;
using System.IO;
using System.Runtime.InteropServices;
using TCD2003.DB;
using ICDBurnInterop;
using System.Text;


namespace TCD2003.GUI
{
	/// <summary>
	/// Summary description for Restore.
	/// </summary>
	/// 

	public class Restore : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label1;
		private System.ComponentModel.IContainer components;
		private System.Windows.Forms.CheckedListBox patientListBox;

		private ArrayList m_SelectedPatientList = null;
		private ArrayList m_SelectedExminations = null;
		private ArrayList m_Patients = null;
		private ArrayList m_PatientsNames = null;
		private ArrayList m_PatientsIDs = null;
		private ArrayList m_BackupSubExamIndexes = null;
		private double m_FilesSize = 0.0;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.ListView listViewExamination;
		private System.Windows.Forms.ColumnHeader columnHeader6;
		private System.Windows.Forms.ColumnHeader columnHeader1;
		private System.Windows.Forms.ColumnHeader columnHeader2;
		private System.Windows.Forms.ColumnHeader columnHeader3;
		private System.Windows.Forms.ColumnHeader columnHeader4;
		private System.Windows.Forms.ColumnHeader columnHeader5;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Button bDetails;
		private System.Windows.Forms.RadioButton rbUserDefined;
		private System.Windows.Forms.RadioButton rbIncrement;
		private System.Windows.Forms.RadioButton rbAll;
		private ArrayList m_SelectedExaminationRows = null;
		private Syncfusion.Windows.Forms.FolderBrowser folderBrowser1;
		private System.Windows.Forms.Button bBrowseFolder;
		private System.Windows.Forms.TextBox location;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label labelSize;
		private System.Windows.Forms.Button bHelp;
		private TCD2003.GUI.AutoCompleteComboBox autoCompleteComboBoxName;
		private TCD2003.GUI.AutoCompleteComboBox autoCompleteComboBoxID;
		private System.Windows.Forms.Button bRestore;
		private System.Windows.Forms.GroupBox groupBoxSearchPatient;
		private System.Windows.Forms.GroupBox groupBoxMode;

		private string m_RestoreLocation = null;

		public Restore()
		{
			InitializeComponent();

			m_SelectedPatientList = new ArrayList();
			m_SelectedExminations = new ArrayList();
			m_SelectedExaminationRows = new ArrayList();
			m_PatientsNames = new ArrayList();
			m_PatientsIDs = new ArrayList();
			m_BackupSubExamIndexes = new ArrayList();
			listViewExamination.Items.Clear();
			this.rbUserDefined_Click(this,null);

			this.BackColor = GlobalSettings.BackgroundDlg;
		}


		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.patientListBox = new System.Windows.Forms.CheckedListBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.bDetails = new System.Windows.Forms.Button();
			this.panel2 = new System.Windows.Forms.Panel();
			this.listViewExamination = new System.Windows.Forms.ListView();
			this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
			this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
			this.label7 = new System.Windows.Forms.Label();
			this.panel3 = new System.Windows.Forms.Panel();
			this.panel1 = new System.Windows.Forms.Panel();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label8 = new System.Windows.Forms.Label();
			this.bBrowseFolder = new System.Windows.Forms.Button();
			this.location = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.labelSize = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.bRestore = new System.Windows.Forms.Button();
			this.bHelp = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.groupBoxSearchPatient = new System.Windows.Forms.GroupBox();
			this.autoCompleteComboBoxID = new TCD2003.GUI.AutoCompleteComboBox();
			this.autoCompleteComboBoxName = new TCD2003.GUI.AutoCompleteComboBox();
			this.label9 = new System.Windows.Forms.Label();
			this.groupBoxMode = new System.Windows.Forms.GroupBox();
			this.rbUserDefined = new System.Windows.Forms.RadioButton();
			this.rbIncrement = new System.Windows.Forms.RadioButton();
			this.rbAll = new System.Windows.Forms.RadioButton();
			this.folderBrowser1 = new Syncfusion.Windows.Forms.FolderBrowser(this.components);
			this.panel1.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.groupBoxSearchPatient.SuspendLayout();
			this.groupBoxMode.SuspendLayout();
			this.SuspendLayout();
			// 
			// patientListBox
			// 
			this.patientListBox.Location = new System.Drawing.Point(16, 192);
			this.patientListBox.Name = "patientListBox";
			this.patientListBox.Size = new System.Drawing.Size(200, 214);
			this.patientListBox.TabIndex = 0;
			this.patientListBox.SelectedIndexChanged += new System.EventHandler(this.patientListBox_SelectedIndexChanged);
			this.patientListBox.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.patientListBox_ItemCheck);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(16, 168);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(200, 16);
			this.label1.TabIndex = 2;
			this.label1.Text = "Patient List";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(32, 40);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(40, 20);
			this.label2.TabIndex = 11;
			this.label2.Text = "Name:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// bDetails
			// 
			this.bDetails.Enabled = false;
			this.bDetails.Location = new System.Drawing.Point(272, 48);
			this.bDetails.Name = "bDetails";
			this.bDetails.Size = new System.Drawing.Size(96, 32);
			this.bDetails.TabIndex = 12;
			this.bDetails.Text = "&Details";
			this.bDetails.Click += new System.EventHandler(this.bDetails_Click);
			// 
			// panel2
			// 
			this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(216, 248);
			this.panel2.TabIndex = 5;
			// 
			// listViewExamination
			// 
			this.listViewExamination.CheckBoxes = true;
			this.listViewExamination.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
																								  this.columnHeader6,
																								  this.columnHeader1,
																								  this.columnHeader2,
																								  this.columnHeader3,
																								  this.columnHeader4,
																								  this.columnHeader5});
			this.listViewExamination.FullRowSelect = true;
			this.listViewExamination.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
			this.listViewExamination.HideSelection = false;
			this.listViewExamination.Location = new System.Drawing.Point(224, 32);
			this.listViewExamination.Name = "listViewExamination";
			this.listViewExamination.Size = new System.Drawing.Size(432, 214);
			this.listViewExamination.Sorting = System.Windows.Forms.SortOrder.Ascending;
			this.listViewExamination.TabIndex = 4;
			this.listViewExamination.View = System.Windows.Forms.View.Details;
			this.listViewExamination.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.listViewExamination_ItemCheck);
			// 
			// columnHeader6
			// 
			this.columnHeader6.Text = "";
			this.columnHeader6.Width = 20;
			// 
			// columnHeader1
			// 
			this.columnHeader1.Text = "Date";
			this.columnHeader1.Width = 75;
			// 
			// columnHeader2
			// 
			this.columnHeader2.Text = "Time";
			this.columnHeader2.Width = 50;
			// 
			// columnHeader3
			// 
			this.columnHeader3.Text = "Type";
			this.columnHeader3.Width = 100;
			// 
			// columnHeader4
			// 
			this.columnHeader4.Text = "Size,K";
			// 
			// columnHeader5
			// 
			this.columnHeader5.Text = "Comments";
			this.columnHeader5.Width = 151;
			// 
			// label7
			// 
			this.label7.BackColor = System.Drawing.SystemColors.Control;
			this.label7.Location = new System.Drawing.Point(248, 8);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(368, 16);
			this.label7.TabIndex = 3;
			this.label7.Text = "Examination List";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// panel3
			// 
			this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel3.Location = new System.Drawing.Point(216, 0);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(440, 248);
			this.panel3.TabIndex = 6;
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.SystemColors.Control;
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel1.Controls.Add(this.panel2);
			this.panel1.Controls.Add(this.listViewExamination);
			this.panel1.Controls.Add(this.label7);
			this.panel1.Controls.Add(this.panel3);
			this.panel1.Location = new System.Drawing.Point(8, 160);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(664, 256);
			this.panel1.TabIndex = 9;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label8);
			this.groupBox1.Controls.Add(this.bBrowseFolder);
			this.groupBox1.Controls.Add(this.location);
			this.groupBox1.Controls.Add(this.label6);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.labelSize);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Location = new System.Drawing.Point(8, 424);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(664, 112);
			this.groupBox1.TabIndex = 13;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Restore Information";
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(16, 48);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(64, 16);
			this.label8.TabIndex = 8;
			this.label8.Text = "Location:";
			this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// bBrowseFolder
			// 
			this.bBrowseFolder.Location = new System.Drawing.Point(264, 48);
			this.bBrowseFolder.Name = "bBrowseFolder";
			this.bBrowseFolder.Size = new System.Drawing.Size(24, 24);
			this.bBrowseFolder.TabIndex = 7;
			this.bBrowseFolder.Text = "...";
			this.bBrowseFolder.Click += new System.EventHandler(this.bBrowseFolder_Click);
			// 
			// location
			// 
			this.location.Location = new System.Drawing.Point(96, 48);
			this.location.Name = "location";
			this.location.Size = new System.Drawing.Size(160, 20);
			this.location.TabIndex = 6;
			this.location.Text = "";
			// 
			// label6
			// 
			this.label6.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.label6.Location = new System.Drawing.Point(568, 72);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(64, 16);
			this.label6.TabIndex = 3;
			this.label6.Text = "00:00:00";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(448, 72);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(96, 16);
			this.label5.TabIndex = 2;
			this.label5.Text = "Estimated Time:";
			// 
			// labelSize
			// 
			this.labelSize.FlatStyle = System.Windows.Forms.FlatStyle.System;
			this.labelSize.Location = new System.Drawing.Point(568, 32);
			this.labelSize.Name = "labelSize";
			this.labelSize.Size = new System.Drawing.Size(72, 16);
			this.labelSize.TabIndex = 1;
			this.labelSize.Text = "0 K";
			this.labelSize.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(448, 32);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(120, 16);
			this.label3.TabIndex = 0;
			this.label3.Text = "Size of selected files:";
			// 
			// bRestore
			// 
			this.bRestore.Enabled = false;
			this.bRestore.Location = new System.Drawing.Point(16, 552);
			this.bRestore.Name = "bRestore";
			this.bRestore.TabIndex = 14;
			this.bRestore.Text = "Restore";
			this.bRestore.Click += new System.EventHandler(this.bRestore_Click);
			// 
			// bHelp
			// 
			this.bHelp.Location = new System.Drawing.Point(496, 552);
			this.bHelp.Name = "bHelp";
			this.bHelp.TabIndex = 16;
			this.bHelp.Text = "Help";
			// 
			// button5
			// 
			this.button5.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.button5.Location = new System.Drawing.Point(592, 552);
			this.button5.Name = "button5";
			this.button5.TabIndex = 17;
			this.button5.Text = "Close";
			// 
			// groupBoxSearchPatient
			// 
			this.groupBoxSearchPatient.Controls.Add(this.autoCompleteComboBoxID);
			this.groupBoxSearchPatient.Controls.Add(this.autoCompleteComboBoxName);
			this.groupBoxSearchPatient.Controls.Add(this.bDetails);
			this.groupBoxSearchPatient.Controls.Add(this.label9);
			this.groupBoxSearchPatient.Controls.Add(this.label2);
			this.groupBoxSearchPatient.Location = new System.Drawing.Point(232, 24);
			this.groupBoxSearchPatient.Name = "groupBoxSearchPatient";
			this.groupBoxSearchPatient.Size = new System.Drawing.Size(440, 120);
			this.groupBoxSearchPatient.TabIndex = 18;
			this.groupBoxSearchPatient.TabStop = false;
			this.groupBoxSearchPatient.Text = "Search Patient";
			// 
			// autoCompleteComboBoxID
			// 
			this.autoCompleteComboBoxID.LimitToList = true;
			this.autoCompleteComboBoxID.Location = new System.Drawing.Point(104, 72);
			this.autoCompleteComboBoxID.Name = "autoCompleteComboBoxID";
			this.autoCompleteComboBoxID.Size = new System.Drawing.Size(121, 21);
			this.autoCompleteComboBoxID.TabIndex = 22;
			this.autoCompleteComboBoxID.SelectedIndexChanged += new System.EventHandler(this.autoCompleteComboBoxID_SelectedIndexChanged);
			// 
			// autoCompleteComboBoxName
			// 
			this.autoCompleteComboBoxName.LimitToList = true;
			this.autoCompleteComboBoxName.Location = new System.Drawing.Point(104, 40);
			this.autoCompleteComboBoxName.Name = "autoCompleteComboBoxName";
			this.autoCompleteComboBoxName.Size = new System.Drawing.Size(121, 21);
			this.autoCompleteComboBoxName.TabIndex = 21;
			this.autoCompleteComboBoxName.SelectedIndexChanged += new System.EventHandler(this.autoCompleteComboBoxName_SelectedIndexChanged);
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(32, 80);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(40, 20);
			this.label9.TabIndex = 19;
			this.label9.Text = "ID #:";
			this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// groupBoxMode
			// 
			this.groupBoxMode.Controls.Add(this.rbUserDefined);
			this.groupBoxMode.Controls.Add(this.rbIncrement);
			this.groupBoxMode.Controls.Add(this.rbAll);
			this.groupBoxMode.Enabled = false;
			this.groupBoxMode.Location = new System.Drawing.Point(8, 24);
			this.groupBoxMode.Name = "groupBoxMode";
			this.groupBoxMode.Size = new System.Drawing.Size(216, 120);
			this.groupBoxMode.TabIndex = 19;
			this.groupBoxMode.TabStop = false;
			this.groupBoxMode.Text = "Mode For Restore";
			// 
			// rbUserDefined
			// 
			this.rbUserDefined.Checked = true;
			this.rbUserDefined.Location = new System.Drawing.Point(16, 80);
			this.rbUserDefined.Name = "rbUserDefined";
			this.rbUserDefined.Size = new System.Drawing.Size(96, 16);
			this.rbUserDefined.TabIndex = 2;
			this.rbUserDefined.TabStop = true;
			this.rbUserDefined.Text = "&User Defined";
			this.rbUserDefined.Click += new System.EventHandler(this.rbUserDefined_Click);
			// 
			// rbIncrement
			// 
			this.rbIncrement.Location = new System.Drawing.Point(16, 56);
			this.rbIncrement.Name = "rbIncrement";
			this.rbIncrement.Size = new System.Drawing.Size(96, 24);
			this.rbIncrement.TabIndex = 1;
			this.rbIncrement.Text = "Incremen&t";
			this.rbIncrement.Visible = false;
			this.rbIncrement.Click += new System.EventHandler(this.rbIncrement_Click);
			// 
			// rbAll
			// 
			this.rbAll.Location = new System.Drawing.Point(16, 24);
			this.rbAll.Name = "rbAll";
			this.rbAll.Size = new System.Drawing.Size(96, 48);
			this.rbAll.TabIndex = 0;
			this.rbAll.Text = "&All";
			this.rbAll.Click += new System.EventHandler(this.rbAll_Click);
			// 
			// folderBrowser1
			// 
			this.folderBrowser1.Description = "Restore Loaction";
			this.folderBrowser1.StartLocation = Syncfusion.Windows.Forms.FolderBrowserFolder.MyComputer;
			this.folderBrowser1.Style = Syncfusion.Windows.Forms.FolderBrowserStyles.NewDialogStyle;
			// 
			// Restore
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(680, 584);
			this.Controls.Add(this.groupBoxMode);
			this.Controls.Add(this.button5);
			this.Controls.Add(this.bHelp);
			this.Controls.Add(this.bRestore);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.patientListBox);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.groupBoxSearchPatient);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "Restore";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Restore Patient Data";
			this.panel1.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.groupBoxSearchPatient.ResumeLayout(false);
			this.groupBoxMode.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void patientListBox_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			listViewExamination.Items.Clear();
			m_SelectedExaminationRows.Clear();
			string selectCommand = "Patient_Index = '"+((Patient)(patientListBox.SelectedItem)).Guid.ToString()+"'";
			m_SelectedExaminationRows.AddRange(RimedDal.Instance.s_dsBurnExamination.tb_Examination.Select(selectCommand));
			foreach (DB.dsExamination.tb_ExaminationRow row in m_SelectedExaminationRows)
			{
				if ((rbAll.Checked)&&(!( m_SelectedExminations.Contains(row.Examination_Index))))
				{
						
					m_SelectedExminations.Add(row.Examination_Index);
						
				}
				ListViewItem item = new ListViewItem();
				item.SubItems.Add(row.Date.ToShortDateString());
				// examination time.
				string time = 
					((row.TimeHours < 10) ? "0" : "") + row.TimeHours.ToString() 
					+ ":" + 
					((row.TimeMinutes < 10) ? "0" : "") + row.TimeMinutes.ToString();

				item.SubItems.Add(time);

				// examination type.
				DB.dsStudy ds = RimedDal.Instance.GetStudyByGuid((Guid)row.Study_Index);
				item.SubItems.Add((string)ds.tb_Study.Rows[0]["Name"]);

				// size
				double size = this.GetExaminationSize((Guid)row.Examination_Index);
				item.SubItems.Add(size.ToString());

				item.SubItems.Add(row.Notes);

				listViewExamination.Items.Add(item);
				if (m_SelectedExminations.Contains(row.Examination_Index))
				{
					m_ViewOnly = true;
					listViewExamination.Items[listViewExamination.Items.Count-1].Checked = true;
					m_ViewOnly = false;
				}
			}
			if ((rbAll.Checked)&& m_SelectedExminations.Count>=1)
				bRestore.Enabled=true;
		}


		private bool m_ViewOnly = false;
		private void listViewExamination_ItemCheck(object sender, System.Windows.Forms.ItemCheckEventArgs e)
		{
			if(m_ViewOnly == true)
				return;
			int checkedItems = listViewExamination.CheckedItems.Count;
			DB.dsExamination.tb_ExaminationRow row = m_SelectedExaminationRows[e.Index] as DB.dsExamination.tb_ExaminationRow;
			if (e.NewValue == CheckState.Checked)
			{
				m_SelectedExminations.Add(row.Examination_Index);
				checkedItems++;
				string filter = "ExaminationIndex = '" + row.Examination_Index + "'";
				System.Data.DataView dv = RimedDal.Instance.GetBurnSubExaminationView(filter);
				foreach(System.Data.DataRowView r in dv)
				{
					if(!m_BackupSubExamIndexes.Contains((Guid)r["SubExaminationIndex"]))
					{
						try
						{
							m_FilesSize += GetFileSize((Guid)r["SubExaminationIndex"]);
							m_BackupSubExamIndexes.Add((Guid)r["SubExaminationIndex"]);
						}
						catch(ApplicationException ex)
						{
							// TODO: give notification to the user that the file does not exist.
							System.Diagnostics.Trace.WriteLine(ex.Message.ToString());
						}
					}						
				}
			}
			else
			{
				m_SelectedExminations.Remove(row.Examination_Index);
				checkedItems--;
				string filter = "ExaminationIndex = '" + row.Examination_Index + "'";
				System.Data.DataView dv = RimedDal.Instance.GetBurnSubExaminationView(filter);
				foreach(System.Data.DataRowView r in dv)
				{
					if(!m_BackupSubExamIndexes.Contains((Guid)r["SubExaminationIndex"]))
					{
						try
						{
							m_FilesSize -= GetFileSize((Guid)r["SubExaminationIndex"]);
							m_BackupSubExamIndexes.Remove((Guid)r["SubExaminationIndex"]);
						}
						catch(ApplicationException ex)
						{
							// TODO: give notification to the user that the file does not exist.
							System.Diagnostics.Trace.WriteLine(ex.Message.ToString());
						}
					}
				}
			}
			
			if (m_SelectedExminations.Count==1)
				bRestore.Enabled =true;
			
			if (m_SelectedExminations.Count==0)
				bRestore.Enabled =false;
				
			this.labelSize.Text = m_FilesSize.ToString();
			
			if (checkedItems>0)
			{
				patientListBox.SetItemChecked(patientListBox.SelectedIndex,true);
				if (!m_SelectedPatientList.Contains(((Patient)(patientListBox.SelectedItem)).Guid))
					m_SelectedPatientList.Add(((Patient)(patientListBox.SelectedItem)).Guid);
			}
			else
			{
				patientListBox.SetItemChecked(patientListBox.SelectedIndex,false);
				m_SelectedPatientList.Remove(((Patient)(patientListBox.SelectedItem)).Guid);
			}
		}

		private void bDetails_Click(object sender, System.EventArgs e)
		{
			string filter = "Select * FROM tb_Patient WHERE (Patient_Index='" + ((Patient)(patientListBox.SelectedItem)).Guid + "' AND Deleted=false)";
			PatientDetails dlg = new PatientDetails(RimedDal.Instance.GetPatients(filter));

			dlg.ShowDialog();
		}

		private void rbAll_Click(object sender, System.EventArgs e)
		{
			patientListBox.Items.Clear();
			listViewExamination.Items.Clear();
			m_SelectedExminations.Clear();
			m_PatientsNames.Clear();
			m_PatientsIDs.Clear();
			m_FilesSize = 0.0;
			m_SelectedPatientList.Clear();
			m_BackupSubExamIndexes.Clear();
			foreach (Patient p in m_Patients)
			{
				patientListBox.Items.Add(p,true);
				m_PatientsNames.Add(p.Name);
				m_PatientsIDs.Add(p.ID);
				if (!m_SelectedPatientList.Contains(p.Guid))
					m_SelectedPatientList.Add(p.Guid);
			}
										
//			foreach (Patient p in m_Patients)
//			{
//				string selectCommand = "Patient_Index = '"+p.Guid.ToString() + "'";// AND BackupPath = '"+ m_RestoreLocation+"'";
//				FillExaminationsList(p,selectCommand);
//				m_SelectedPatientList.Add(p.Guid);
//			}
			this.FillComboBoxCtrl(this.autoCompleteComboBoxName, m_PatientsNames);
			this.FillComboBoxCtrl(this.autoCompleteComboBoxID, m_PatientsIDs);
			try
			{
				for (int i=m_Patients.Count-1;i>=0;i--)
				{
					autoCompleteComboBoxName.SelectedIndex = i;
					autoCompleteComboBoxID.SelectedIndex = i;
				}
			}
			catch(Exception ex)
			{
				System.Diagnostics.Trace.WriteLine(ex.Message.ToString());
			}
			m_FilesSize = CalcFilesSize(m_BackupSubExamIndexes);
			this.labelSize.Text = m_FilesSize.ToString();
		}

		private void FillExaminationsList(Patient p,string selectCommand)
		{
			m_SelectedExaminationRows.Clear();
			m_SelectedExaminationRows.AddRange(RimedDal.Instance.s_dsExamination.tb_Examination.Select(selectCommand));
			foreach (DB.dsExamination.tb_ExaminationRow row in m_SelectedExaminationRows)
			{
				m_SelectedExminations.Add(row.Examination_Index);
				string filter = "ExaminationIndex = '" + row.Examination_Index + "'";
				System.Data.DataView dv = RimedDal.Instance.GetBurnSubExaminationView(filter);
				foreach(System.Data.DataRowView r in dv)
				{
					if(!m_BackupSubExamIndexes.Contains((Guid)r["SubExaminationIndex"]))
						m_BackupSubExamIndexes.Add((Guid)r["SubExaminationIndex"]);
				}
			}
			if (m_SelectedExaminationRows.Count>0)
			{
				patientListBox.Items.Add(p,true);	
				m_PatientsNames.Add(p.Name);
				m_PatientsIDs.Add(p.ID);
			}
		}

		private void rbUserDefined_Click(object sender, System.EventArgs e)
		{
			patientListBox.Items.Clear();
			listViewExamination.Items.Clear();	
			m_SelectedExminations.Clear();
			m_PatientsNames.Clear();
			m_PatientsIDs.Clear();
			m_FilesSize = 0.0;
			//			m_BackupFileNames.Clear();
			m_BackupSubExamIndexes.Clear();
			bRestore.Enabled =false;

			if(m_RestoreLocation == null)
				return;

			DisplayBurnPackage();

		}
		private void rbIncrement_Click(object sender, System.EventArgs e)
		{
			patientListBox.Items.Clear();
			listViewExamination.Items.Clear();
			m_SelectedExminations.Clear();
			m_PatientsNames.Clear();
			m_PatientsIDs.Clear();
			m_FilesSize = 0.0;
			//			m_BackupFileNames.Clear();
			m_BackupSubExamIndexes.Clear();

			foreach (Patient p in m_Patients)
			{
				string selectCommand = "Patient_Index = '"+p.Guid.ToString()+"' AND (BackedUp='false')";
				FillExaminationsList(p,selectCommand);
			}
			this.FillComboBoxCtrl(this.autoCompleteComboBoxName, m_PatientsNames);
			this.FillComboBoxCtrl(this.autoCompleteComboBoxID, m_PatientsIDs);
			try
			{
				autoCompleteComboBoxName.SelectedIndex = 0;
				autoCompleteComboBoxID.SelectedIndex = 0;
			}
			catch(Exception ex)
			{
				System.Diagnostics.Trace.WriteLine(ex.Message.ToString());
			}

			m_FilesSize = CalcFilesSize(m_BackupSubExamIndexes);
			this.labelSize.Text = m_FilesSize.ToString();
			this.labelSize.Text = "0k";
		}


		private void bBrowseFolder_Click(object sender, System.EventArgs e)
		{
			if (this.folderBrowser1.ShowDialog() == DialogResult.OK)
			{
				try
				{
					location.Text = this.folderBrowser1.DirectoryPath;
					m_RestoreLocation = location.Text;
					RimedDal.Instance.ReadBurnDS(m_RestoreLocation);
					m_Patients = new ArrayList(RimedDal.Instance.GetBurnPatientsList());
					m_Patients.Sort();

					DisplayBurnPackage();
					bRestore.Enabled = true;
					this.groupBoxMode.Enabled = true;
					this.groupBoxSearchPatient.Enabled = true;
				}
				catch(Exception ex)
				{
					System.Diagnostics.Trace.WriteLine(ex.Message.ToString());

					System.Resources.ResourceManager strRes = new System.Resources.ResourceManager(				
						GetType().Namespace + ".Resource1" , GetType().Assembly);

					MessageBox.Show(strRes.GetString("The selected location does not contain valid backup information."),
						strRes.GetString("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error );
				}
			}
		}

		private double CalcFilesSize(ArrayList guidArr)
		{
			double size = 0;
			foreach (Guid g in guidArr)
				size += GetFileSize(g);
			return size;
		}


		private double CalcFileSize(string file)
		{
			if (File.Exists(file))
			{
				FileInfo f = new FileInfo(file);
				return (f.Length/1000.0);  // in k
			}
			return 0;
		}

		private void FillComboBoxCtrl(ComboBox aComboBox, ArrayList aArrayList)
		{
			if(aComboBox.Items.Count != 0)
			{
				aComboBox.Items.Clear();
				aComboBox.Text = "";
			}

			foreach(object obj in aArrayList)
			{
				aComboBox.Items.Add(obj);
			}
			// Enable/Disable the details button refers to the selection in the comboBoxes.
			if(aComboBox.Items.Count == 0)
			{
				bDetails.Enabled = false;
			}
			else
			{
				bDetails.Enabled = true;
			}
		}

		private void autoCompleteComboBoxName_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int index = this.autoCompleteComboBoxName.SelectedIndex;
			this.autoCompleteComboBoxID.SelectedIndex = index;
			patientListBox.SelectedIndex = index;
		}

		private void autoCompleteComboBoxID_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int index = this.autoCompleteComboBoxID.SelectedIndex;
			this.autoCompleteComboBoxName.SelectedIndex = index;
			patientListBox.SelectedIndex = index;
		}

		private bool CheckFreeSpace()
		{
			return true;

		}

		private double GetFileSize(Guid fileGuid)
		{
			string filePath = m_RestoreLocation + @"\RawData\" + fileGuid.ToString() + ".dat";
			string zipfile = m_RestoreLocation + @"\RawData\" + fileGuid.ToString() + ".dat.tmp";
			// check which file exist on the HD.
			if(System.IO.File.Exists(filePath))
				return CalcFileSize(filePath);
			else
				if(System.IO.File.Exists(zipfile))
				return CalcFileSize(zipfile);

			System.Resources.ResourceManager strRes = new System.Resources.ResourceManager(				
				GetType().Namespace + ".Resource1" , GetType().Assembly);
			throw new ApplicationException( 
				strRes.GetString("The file of the selected examination does not exist!"));

			//TODO: Give notification to the user that the file does not exist.
		}


		private double GetExaminationSize(Guid examIndex)
		{
			double size = 0;
			string filter = "ExaminationIndex = '" + examIndex.ToString() + "'";
			System.Data.DataView dv = RimedDal.Instance.GetBurnSubExaminationView(filter);
			foreach(System.Data.DataRowView row in dv)
				size += GetFileSize((Guid)row["SubExaminationIndex"]);

			return size;
		}

		private void bRestore_Click(object sender, System.EventArgs e)
		{
			try
			{
				RestorePackage();

				// Give notification to the user.
				System.Resources.ResourceManager strRes = new System.Resources.ResourceManager(				
					GetType().Namespace + ".Resource1" , GetType().Assembly);

				MessageBox.Show(strRes.GetString("Restore operation completed successfully."),
					strRes.GetString("Information"), MessageBoxButtons.OK, MessageBoxIcon.Information );
			}
			catch(ApplicationException ex)
			{
				System.Diagnostics.Trace.WriteLine(ex.Message.ToString());
			}
				
		}

		private void RestorePackage()
		{
			string target = string.Empty;
			string source = string.Empty;
			string sourceZipFile = string.Empty;

			CopyFilesDlg cf = new CopyFilesDlg(this.Handle);

			// patient table.
			foreach(Guid g in this.m_SelectedPatientList)
			{
				dsPatient.tb_PatientRow burnPatientRow = RimedDal.Instance.s_dsBurnPatient.tb_Patient.FindByPatient_Index(g);
				dsPatient.tb_PatientRow patientRow = RimedDal.Instance.s_dsPatient.tb_Patient.FindByPatient_Index(g);
				if(patientRow != null)
					patientRow.ItemArray = burnPatientRow.ItemArray;
				else
				{
					DB.dsPatient.tb_PatientRow newPatientRow = RimedDal.Instance.s_dsPatient.tb_Patient.Newtb_PatientRow();
					newPatientRow.ItemArray = burnPatientRow.ItemArray;
					RimedDal.Instance.s_dsPatient.tb_Patient.Rows.Add(newPatientRow);
				}
			}

			dsExamination dsExam = new dsExamination();
			dsGateExamination dsGateExam = new dsGateExamination();
			dsSubExamination dsSubExam = new dsSubExamination();
			dsBVExamination dsBVExam = new dsBVExamination();

			foreach(Guid examIndex in m_SelectedExminations)
			{
				// examination table.
				dsExamination.tb_ExaminationRow burnExamRow = RimedDal.Instance.s_dsBurnExamination.tb_Examination.FindByExamination_Index(examIndex);

				DB.dsExamination.tb_ExaminationRow examRow = dsExam.tb_Examination.Newtb_ExaminationRow();
				examRow.ItemArray = burnExamRow.ItemArray;
				dsExam.tb_Examination.Rows.Add(examRow);

				// subExamination table & RawData files.
				string filter = "ExaminationIndex = '" + examIndex.ToString() + "'";
				System.Data.DataRow [] burnSubExamRows = RimedDal.Instance.s_dsBurnSubExamination.tb_SubExamination.Select(filter);
				foreach(System.Data.DataRow burnSubExamRow in burnSubExamRows)
				{
					DB.dsSubExamination.tb_SubExaminationRow subExamRow = dsSubExam.tb_SubExamination.Newtb_SubExaminationRow();
					subExamRow.ItemArray = burnSubExamRow.ItemArray;
					dsSubExam.tb_SubExamination.Rows.Add(subExamRow);

					// add raw data files to the copy buffer.
					source = subExamRow.SubExaminationIndex.ToString() + ".dat";
					sourceZipFile = source + ".tmp";
					target = GlobalSettings.RawDataDir + @"\";

					if(System.IO.File.Exists(m_RestoreLocation + @"\RawData\" + source))
						cf.AddFile(m_RestoreLocation + @"\RawData\" + source,
							target + source);
					else if(System.IO.File.Exists(m_RestoreLocation + @"\RawData\" + sourceZipFile))
						cf.AddFile(m_RestoreLocation + @"\RawData\" + sourceZipFile,
							target + sourceZipFile);
				}

				// bvExamination table.
				filter = "ExaminationIndex = '" + examIndex.ToString() + "'";
				System.Data.DataRow [] burnBVExamRows = RimedDal.Instance.s_dsBurnBVExamination.tb_BVExamination.Select(filter);
				foreach(System.Data.DataRow burnBVExamRow in burnBVExamRows)
				{
					DB.dsBVExamination.tb_BVExaminationRow newBVExamRow = dsBVExam.tb_BVExamination.Newtb_BVExaminationRow();
					newBVExamRow.ItemArray = burnBVExamRow.ItemArray;
					dsBVExam.tb_BVExamination.Rows.Add(newBVExamRow);
				}

				// gateExamination table & GateImg files. 
				filter = "ExaminationIndex = '" + examIndex.ToString() + "'";
				System.Data.DataRow [] burnGateExamRows = RimedDal.Instance.s_dsBurnGateExamination.tb_GateExamination.Select(filter);
				foreach(System.Data.DataRow burnGateExamRow in burnGateExamRows)
				{
					DB.dsGateExamination.tb_GateExaminationRow gateExamRow = dsGateExam.tb_GateExamination.Newtb_GateExaminationRow();
					gateExamRow.ItemArray = burnGateExamRow.ItemArray;
					dsGateExam.tb_GateExamination.Rows.Add(gateExamRow);

					// add Gates Img file to copy buffer.
					string suffix = "_M";
					if(gateExamRow.Name.EndsWith("-L") == true)
						suffix = "_L";
					else if(gateExamRow.Name.EndsWith("-R") == true)
						suffix = "_R";

					target = GlobalSettings.GateImgDir + @"\" + gateExamRow.Gate_Index.ToString() + suffix + ".jpg";
					source = m_RestoreLocation + @"\GateImg\" + gateExamRow.Gate_Index.ToString() + suffix + ".jpg";
					if(System.IO.File.Exists(source))
						cf.AddFile(source, target);

					if(System.IO.File.Exists(m_RestoreLocation + @"\SummaryImg\" + gateExamRow.Gate_Index.ToString() + suffix + ".jpg"))
						cf.AddFile(m_RestoreLocation + @"\SummaryImg\" + gateExamRow.Gate_Index.ToString() + suffix + ".jpg",
							GlobalSettings.SummaryImgDir + @"\" + gateExamRow.Gate_Index.ToString() + suffix + ".jpg");
				}
			}
			cf.DoOperation(CopyFilesDlg.TFileOperation.eCopy);
			RimedDal.Instance.UpdateRestoredData(dsExam, dsSubExam, dsBVExam, dsGateExam);	
		}

		private void DisplayBurnPackage()
		{
			patientListBox.Enabled = true;
			listViewExamination.Enabled = true;
			foreach (Patient p in m_Patients)
			{
				patientListBox.Items.Add(p,false);
				m_PatientsNames.Add(p.Name);
				m_PatientsIDs.Add(p.ID);
			}

			this.FillComboBoxCtrl(this.autoCompleteComboBoxName, m_PatientsNames);
			this.FillComboBoxCtrl(this.autoCompleteComboBoxID, m_PatientsIDs);
			try
			{
				autoCompleteComboBoxName.SelectedIndex = 0;
				autoCompleteComboBoxID.SelectedIndex = 0;
			}
			catch(Exception ex)
			{
				System.Diagnostics.Trace.WriteLine(ex.Message.ToString());
			}
		}

		private void patientListBox_ItemCheck(object sender, System.Windows.Forms.ItemCheckEventArgs e)
		{
			if(m_ViewOnly == true)
				return;
			if (e.NewValue == CheckState.Checked)
				m_SelectedPatientList.Add(((Patient)((ListBox)sender).Items[e.Index]).Guid);
			else
				m_SelectedPatientList.Remove(((Patient)((ListBox)sender).Items[e.Index]).Guid);
		}	
	}
}
