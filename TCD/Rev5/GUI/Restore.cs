using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

using TCD2003.DB;
using TCD2003.Utils;

namespace TCD2003.GUI
{
    /// <summary>
    /// Summary description for Restore.
    /// The UI to restore data from CD/HD/Network
    /// </summary>
    public partial class Restore : Form
    {
        #region Private Fields

        private List<Guid> m_selectedPatients = new List<Guid>();
        private List<Guid> m_selectedExaminations = new List<Guid>();
        private List<Patient> m_patients = new List<Patient>();
        private List<string> m_patientsNames = new List<string>();
        private List<string> m_patientsIDs = new List<string>();
        private List<Guid> m_backupSubExamIndexes = new List<Guid>();
        private double m_filesSize = 0.0;
        private List<DataRow> m_selectedExaminationRows = new List<DataRow>();

        private String m_restoreLocation = null;
        private bool m_viewOnly = false;

        #endregion Private Fields

        /// <summary>
        /// Constructor
        /// </summary>
        public Restore()
        {
            InitializeComponent();
            var strRes = ResourceManagerWrapper.GetWrapper(MainForm.ResourceManager); 

            this.lblPatientList.Text = strRes.GetString("Patient List");
            this.lblExaminationList.Text = strRes.GetString("Examination List");
            this.label2.Text = strRes.GetString("Name") + ":";
            this.bDetails.Text = strRes.GetString("Details");
            this.columnHeader1.Text = strRes.GetString("Date");
            this.columnHeader2.Text = strRes.GetString("Time");
            this.columnHeader3.Text = strRes.GetString("Type");
            this.columnHeader4.Text = strRes.GetString("Size") + ",K";
            this.columnHeader5.Text = strRes.GetString("Comments");
            this.gbRestoreInformation.Text = strRes.GetString("Restore Information");
            this.lbLocation.Text = strRes.GetString("Location") + ":";
            this.lbEstimatedTime.Text = strRes.GetString("Estimated Time") + ":";
            this.labelSize.Text = strRes.GetString("0K");
            this.lbSize.Text = strRes.GetString("Size of selected files") + ":";
            this.bRestore.Text = strRes.GetString("Restore");
            this.bHelp.Text = strRes.GetString("Help");
            this.bClose.Text = strRes.GetString("Close");
            this.gbSearchPatient.Text = strRes.GetString("Search Patient");
            this.label9.Text = strRes.GetString("ID") + "#:";
            this.gbMode.Text = strRes.GetString("Mode For Restore");
            this.rbUserDefined.Text = strRes.GetString("User Defined");
            this.rbIncrement.Text = strRes.GetString("Increment");
            this.rbAll.Text = strRes.GetString("All");
            this.folderBrowser1.Description = strRes.GetString("Restore Location");
            this.Text = strRes.GetString("Restore Patient Data");

            RimedDal.Instance.LoadAllSubExaminations();
            this.listViewExamination.Items.Clear();
            this.rbUserDefined_Click(this, null);

            this.BackColor = GlobalSettings.BackgroundDlg;
        }

        #region Event Handlers

        private void patientListBox_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            double size = 0;
            this.listViewExamination.Items.Clear();
            this.m_selectedExaminationRows.Clear();
            String selectCommand = "Patient_Index = '" + ((Patient)(patientListBox.SelectedItem)).Guid.ToString() + "'";
            this.m_selectedExaminationRows.AddRange(RimedDal.Instance.s_dsBurnExamination.tb_Examination.Select(selectCommand));
            foreach (DB.dsExamination.tb_ExaminationRow row in this.m_selectedExaminationRows)
            {
                if ((rbAll.Checked) && (!(this.m_selectedExaminations.Contains(row.Examination_Index))))
                {

                    this.m_selectedExaminations.Add(row.Examination_Index);

                }
                ListViewItem item = new ListViewItem();
                item.SubItems.Add(row.Date.ToShortDateString());
                // examination time.
                String time =
                    ((row.TimeHours < 10) ? "0" : "") + row.TimeHours.ToString()
                    + ":" +
                    ((row.TimeMinutes < 10) ? "0" : "") + row.TimeMinutes.ToString();

                item.SubItems.Add(time);

                // examination type.
                DB.dsStudy ds = RimedDal.Instance.GetStudyByGuid((Guid)row.Study_Index);
                item.SubItems.Add((String)ds.tb_Study.Rows[0]["Name"]);

                // size
                //if this is a monitoring study
                if ((bool)ds.tb_Study.Rows[0].ItemArray[5] == true)
                    size = this.GetExaminationSize((Guid)row.Examination_Index, true);
                else
                    size = this.GetExaminationSize((Guid)row.Examination_Index, false);
                item.SubItems.Add(size.ToString());

                item.SubItems.Add(row.Notes);

                this.listViewExamination.Items.Add(item);
                if (this.m_selectedExaminations.Contains(row.Examination_Index))
                {
                    this.m_viewOnly = true;
                    this.listViewExamination.Items[this.listViewExamination.Items.Count - 1].Checked = true;
                    this.m_viewOnly = false;
                }
            }
            if ((this.rbAll.Checked) && this.m_selectedExaminations.Count >= 1)
                this.bRestore.Enabled = true;
        }

        private void listViewExamination_ItemCheck(object sender, System.Windows.Forms.ItemCheckEventArgs e)
        {
            if (this.m_viewOnly)
                return;

            int checkedItems = this.listViewExamination.CheckedItems.Count;
            String examSize = this.listViewExamination.Items[e.Index].SubItems[4].Text;
            DB.dsExamination.tb_ExaminationRow row = m_selectedExaminationRows[e.Index] as DB.dsExamination.tb_ExaminationRow;
            if (e.NewValue == CheckState.Checked)
            {
                this.m_selectedExaminations.Add(row.Examination_Index);
                checkedItems++;
                String filter = "ExaminationIndex = '" + row.Examination_Index + "'";
                DataView dv = RimedDal.Instance.GetBurnSubExaminationView(filter);
                this.m_filesSize += Convert.ToDouble(double.Parse(examSize));
            }
            else
            {
                this.m_selectedExaminations.Remove(row.Examination_Index);
                checkedItems--;
                String filter = "ExaminationIndex = '" + row.Examination_Index + "'";
                DataView dv = RimedDal.Instance.GetBurnSubExaminationView(filter);
                this.m_filesSize -= Convert.ToDouble(double.Parse(examSize));
            }

            if (this.m_selectedExaminations.Count == 1)
                this.bRestore.Enabled = true;

            if (this.m_selectedExaminations.Count == 0)
                this.bRestore.Enabled = false;

            this.labelSize.Text = m_filesSize.ToString();

            if (checkedItems > 0)
            {
                this.patientListBox.SetItemChecked(patientListBox.SelectedIndex, true);
                if (!this.m_selectedPatients.Contains(((Patient)(this.patientListBox.SelectedItem)).Guid))
                    this.m_selectedPatients.Add(((Patient)(this.patientListBox.SelectedItem)).Guid);
            }
            else
            {
                this.patientListBox.SetItemChecked(patientListBox.SelectedIndex, false);
                this.m_selectedPatients.Remove(((Patient)(patientListBox.SelectedItem)).Guid);
            }
        }

        private void bDetails_Click(object sender, System.EventArgs e)
        {
            List<LogField> fields = new List<LogField>();
            fields.Add(new LogField("patientListBox", ((Patient)(patientListBox.SelectedItem)).Name));
            ExceptionPublisherLog4Net.FormActionLog("bDetails", this.Name, fields);
            String filter = "Select * FROM tb_Patient WHERE (Patient_Index='" + ((Patient)(patientListBox.SelectedItem)).Guid + "' AND Deleted=false)";
            PatientDetails dlg = new PatientDetails(RimedDal.Instance.GetPatients(filter));

            dlg.ShowDialog();
        }

        private void rbAll_Click(object sender, System.EventArgs e)
        {
            this.patientListBox.Items.Clear();
            this.listViewExamination.Items.Clear();
            this.m_selectedExaminations.Clear();
            this.m_patientsNames.Clear();
            this.m_patientsIDs.Clear();
            this.m_filesSize = 0.0;
            this.m_selectedPatients.Clear();
            this.m_backupSubExamIndexes.Clear();
            foreach (Patient p in this.m_patients)
            {
                this.patientListBox.Items.Add(p, true);
                this.m_patientsNames.Add(p.Name);
                this.m_patientsIDs.Add(p.ID);
                if (!this.m_selectedPatients.Contains(p.Guid))
                    this.m_selectedPatients.Add(p.Guid);
            }

            this.FillComboBoxCtrl(this.autoCompleteComboBoxName, this.m_patientsNames);
            this.FillComboBoxCtrl(this.autoCompleteComboBoxID, this.m_patientsIDs);
            try
            {
                for (int i = this.m_patients.Count - 1; i >= 0; i--)
                {
                    this.autoCompleteComboBoxName.SelectedIndex = i;
                    this.autoCompleteComboBoxID.SelectedIndex = i;
                }
            }
            catch (Exception ex)
            {
                ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
            }
            this.m_filesSize = this.CalcFilesSize(this.m_backupSubExamIndexes);
            this.labelSize.Text = this.m_filesSize.ToString();
        }

        private void rbUserDefined_Click(object sender, System.EventArgs e)
        {
            this.listViewExamination.Items.Clear();
            this.m_selectedExaminations.Clear();
            this.m_filesSize = 0.0;
            this.m_backupSubExamIndexes.Clear();
            this.bRestore.Enabled = false;

            if (this.m_restoreLocation == null)
                return;

            this.DisplayBurnPackage();
        }
        
        private void rbIncrement_Click(object sender, System.EventArgs e)
        {
            this.patientListBox.Items.Clear();
            this.listViewExamination.Items.Clear();
            this.m_selectedExaminations.Clear();
            this.m_patientsNames.Clear();
            this.m_patientsIDs.Clear();
            this.m_filesSize = 0.0;
            this.m_backupSubExamIndexes.Clear();

            foreach (Patient p in this.m_patients)
            {
                String selectCommand = "Patient_Index = '" + p.Guid.ToString() + "' AND (BackedUp='false')";
                this.FillExaminationsList(p, selectCommand);
            }
            this.FillComboBoxCtrl(this.autoCompleteComboBoxName, this.m_patientsNames);
            this.FillComboBoxCtrl(this.autoCompleteComboBoxID, this.m_patientsIDs);
            try
            {
                this.autoCompleteComboBoxName.SelectedIndex = 0;
                this.autoCompleteComboBoxID.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
            }

            this.m_filesSize = CalcFilesSize(m_backupSubExamIndexes);
            this.labelSize.Text = m_filesSize.ToString();
            this.labelSize.Text = "0k";
        }

        private void bBrowseFolder_Click(object sender, System.EventArgs e)
        {
            ExceptionPublisherLog4Net.FormActionLog("bBrowseFolder", this.Name, null);
            if (this.folderBrowser1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    this.location.Text = this.folderBrowser1.SelectedPath;
                    this.m_restoreLocation = this.location.Text;
                    RimedDal.Instance.ReadBurnDS(this.m_restoreLocation);
                    this.m_patients = new List<Patient>(RimedDal.Instance.GetBurnPatientsList());
                    this.m_patients.Sort();

                    this.DisplayBurnPackage();
                    this.gbMode.Enabled = true;
                    this.gbSearchPatient.Enabled = true;
                }
                catch (Exception ex)
                {
                    ExceptionPublisherLog4Net.ExceptionErrorLog(ex, "The selected location does not contain valid backup information");

                    MessageBox.Show(MainForm.ResourceManager.GetString("The selected location does not contain valid backup information."),
                        MainForm.ResourceManager.GetString("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void autoCompleteComboBoxName_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            int index = this.autoCompleteComboBoxName.SelectedIndex;
            this.autoCompleteComboBoxID.SelectedIndex = index;
            this.patientListBox.SelectedIndex = index;
        }

        private void autoCompleteComboBoxID_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            int index = this.autoCompleteComboBoxID.SelectedIndex;
            this.autoCompleteComboBoxName.SelectedIndex = index;
            this.patientListBox.SelectedIndex = index;
        }

        private void bRestore_Click(object sender, System.EventArgs e)
        {
            List<LogField> fields = new List<LogField>();
            fields.Add(new LogField("rbUserDefined", rbUserDefined.Checked ? "true" : "false"));
            fields.Add(new LogField("location", location.Text));
            string selectedPatients = string.Empty;
            for (int i = 0; i < this.patientListBox.CheckedItems.Count; i++)
            {
                selectedPatients += ((Patient)(this.patientListBox.CheckedItems[i])).Name;
                if (i != this.patientListBox.CheckedItems.Count - 1)
                    selectedPatients += "; ";
            }
            fields.Add(new LogField("patientListBox", selectedPatients));
            ExceptionPublisherLog4Net.FormActionLog("bRestore", this.Name, fields);

            try
            {
                bool res = RestorePackage();

                // Give notification to the user.
                if (res)
                    MessageBox.Show(MainForm.ResourceManager.GetString("Restore operation completed successfully."),
                        MainForm.ResourceManager.GetString("Information"), MessageBoxButtons.OK, MessageBoxIcon.Information);
                else
                    MessageBox.Show(MainForm.ResourceManager.GetString("Restore operation failed."),
                        MainForm.ResourceManager.GetString("Information"), MessageBoxButtons.OK, MessageBoxIcon.Information);

                //RIMD-319: 12/01/2010
                this.listViewExamination.Items.Clear();
                this.patientListBox.Items.Clear();

                this.m_selectedExaminations.Clear();
                this.m_selectedPatients.Clear();

                this.location.Text = "";

                this.autoCompleteComboBoxName.Items.Clear();
                this.autoCompleteComboBoxName.Text = "";
                this.autoCompleteComboBoxID.Items.Clear();
                this.autoCompleteComboBoxID.Text = "";
                this.bDetails.Enabled = false;
                this.bRestore.Enabled = false;
                this.rbUserDefined.Checked = true;
                this.gbMode.Enabled = false;
                this.gbSearchPatient.Enabled = false;
            }
            catch (ApplicationException ex)
            {
                ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
            }
        }

        private void patientListBox_ItemCheck(object sender, System.Windows.Forms.ItemCheckEventArgs e)
        {
            if (this.m_viewOnly)
                return;

            if (e.NewValue == CheckState.Checked)
                this.m_selectedPatients.Add(((Patient)((ListBox)sender).Items[e.Index]).Guid);
            else
                this.m_selectedPatients.Remove(((Patient)((ListBox)sender).Items[e.Index]).Guid);
        }

        private void patientListBox_MouseUp(object sender, MouseEventArgs e)
        {
            //RIMD-316: Change the logic of check-boxes on Backup screen
            if (this.patientListBox.SelectedIndex >= 0 && this.patientListBox.GetItemChecked(this.patientListBox.SelectedIndex))
            {
                for (int i = 0; i < this.listViewExamination.Items.Count; i++)
                    this.listViewExamination.Items[i].Checked = true;
            }
            else
            {
                for (int i = 0; i < this.listViewExamination.Items.Count; i++)
                    this.listViewExamination.Items[i].Checked = false;
            }
        }

        #endregion Event Handlers

        #region Private Methods

        private void FillExaminationsList(Patient p, String selectCommand)
        {
            this.m_selectedExaminationRows.Clear();
            this.m_selectedExaminationRows.AddRange(RimedDal.Instance.s_dsExamination.tb_Examination.Select(selectCommand));
            foreach (DB.dsExamination.tb_ExaminationRow row in this.m_selectedExaminationRows)
            {
                this.m_selectedExaminations.Add(row.Examination_Index);
                String filter = "ExaminationIndex = '" + row.Examination_Index + "'";
                DataView dv = RimedDal.Instance.GetBurnSubExaminationView(filter);
                foreach (DataRowView r in dv)
                {
                    if (!this.m_backupSubExamIndexes.Contains((Guid)r["SubExaminationIndex"]))
                        this.m_backupSubExamIndexes.Add((Guid)r["SubExaminationIndex"]);
                }
            }
            if (this.m_selectedExaminationRows.Count > 0)
            {
                this.patientListBox.Items.Add(p, true);
                this.m_patientsNames.Add(p.Name);
                this.m_patientsIDs.Add(p.ID);
            }
        }

        private double CalcFilesSize(List<Guid> guidArr)
        {
            double size = 0;
            foreach (Guid g in guidArr)
                size += this.GetFileSize(g);
            return size;
        }

        private double CalcFileSize(String file)
        {
            if (File.Exists(file))
            {
                FileInfo f = new FileInfo(file);
                return (f.Length / 1000.0);  // in k
            }
            return 0;
        }

        private void FillComboBoxCtrl(ComboBox comboBox, List<string> list)
        {
            if (comboBox.Items.Count != 0)
            {
                comboBox.Items.Clear();
                comboBox.Text = string.Empty;
            }

            foreach (object obj in list)
                comboBox.Items.Add(obj);

            // Enable/Disable the details button refers to the selection in the comboBoxes.
            bDetails.Enabled = comboBox.Items.Count != 0;
        }

        private double GetFileSize(Guid fileGuid)
        {
            String filePath = this.m_restoreLocation + @"\RawData\" + fileGuid.ToString() + ".dat";
            String zipfile = this.m_restoreLocation + @"\RawData\" + fileGuid.ToString() + ".dat.tmp";
            // check which file exist on the HD.
            double fileSize = 0;
            if (File.Exists(filePath))
                fileSize = this.CalcFileSize(filePath);
            else if (File.Exists(zipfile))
                fileSize = this.CalcFileSize(zipfile);
            
            return fileSize;
        }

        private double GetExaminationSize(Guid examIndex, bool isMonitoringExam)
        {
            double size = 0;
            String[] smallBackupFilesArray;
            String filter = "ExaminationIndex = '" + examIndex.ToString() + "'";
            if (isMonitoringExam)
            {
                smallBackupFilesArray = Directory.GetFiles(this.m_restoreLocation + @"\RawData\" + examIndex);
                for (int i = 0; i < smallBackupFilesArray.Length; i++)
                    size += this.CalcFileSize(smallBackupFilesArray[i]);

            }
            else
            {
                DataView dv = RimedDal.Instance.GetSubExaminationView(filter);
                foreach (DataRowView row in dv)
                    size += this.GetFileSize((Guid)row["SubExaminationIndex"]);
            }

            return size;
        }
        
        private bool RestorePackage()
        {
            String target = String.Empty;
            String source = String.Empty;
            String sourceZipFile = String.Empty;
            string sourceReplayDir = Path.Combine(m_restoreLocation, "ReplayImg");
            CopyFilesDlg cf = new CopyFilesDlg(this.Handle);

            // Patient table.
            foreach (Guid g in this.m_selectedPatients)
            {
                dsPatient.tb_PatientRow burnPatientRow = RimedDal.Instance.s_dsBurnPatient.tb_Patient.FindByPatient_Index(g);
                dsPatient.tb_PatientRow patientRow = RimedDal.Instance.s_dsPatient.tb_Patient.FindByPatient_Index(g);
                if (patientRow != null)
                    patientRow.ItemArray = burnPatientRow.ItemArray;
                else
                {
                    DB.dsPatient.tb_PatientRow newPatientRow = RimedDal.Instance.s_dsPatient.tb_Patient.Newtb_PatientRow();
                    newPatientRow.ItemArray = burnPatientRow.ItemArray;
                    RimedDal.Instance.s_dsPatient.tb_Patient.Rows.Add(newPatientRow);
                }
            }

            dsExamination dsExam = new dsExamination();
            dsGateExamination dsGateExam = new dsGateExamination();
            dsSubExamination dsSubExam = new dsSubExamination();
            dsBVExamination dsBVExam = new dsBVExamination();
            dsEventExamination dsEventExam = new dsEventExamination();

            foreach (Guid examIndex in m_selectedExaminations)
            {
                // examination table.
                dsExamination.tb_ExaminationRow burnExamRow = RimedDal.Instance.s_dsBurnExamination.tb_Examination.FindByExamination_Index(examIndex);

                DB.dsExamination.tb_ExaminationRow examRow = dsExam.tb_Examination.Newtb_ExaminationRow();
                examRow.ItemArray = burnExamRow.ItemArray;
                dsExam.tb_Examination.Rows.Add(examRow);

                // subExamination table & RawData files.
                String filter = "ExaminationIndex = '" + examIndex.ToString() + "'";
                DataRow[] burnSubExamRows = RimedDal.Instance.s_dsBurnSubExamination.tb_SubExamination.Select(filter);
                foreach (DataRow burnSubExamRow in burnSubExamRows)
                {
                    DB.dsSubExamination.tb_SubExaminationRow subExamRow = dsSubExam.tb_SubExamination.Newtb_SubExaminationRow();
                    subExamRow.ItemArray = burnSubExamRow.ItemArray;
                    dsSubExam.tb_SubExamination.Rows.Add(subExamRow);

                    // Add raw data files to the copy buffer.
                    source = subExamRow.SubExaminationIndex.ToString() + ".dat";
                    sourceZipFile = source + ".tmp";
                    target = GlobalSettings.RawDataDir;

                    string sourceDir = Path.Combine(this.m_restoreLocation, "RawData");
                    if (File.Exists(Path.Combine(sourceDir, source)))
                        cf.AddFile(Path.Combine(sourceDir, source), Path.Combine(target, source));
                    else if (File.Exists(Path.Combine(sourceDir, sourceZipFile)))
                        cf.AddFile(Path.Combine(sourceDir, sourceZipFile), Path.Combine(target, sourceZipFile));
                    if (Directory.Exists(Path.Combine(sourceDir, examIndex.ToString())))
                        cf.AddFile(Path.Combine(sourceDir, examIndex.ToString()), Path.Combine(target, examIndex.ToString()));

                    // fake autoscan images (left and right)
                    string[] tmp = new string[]
					{
						subExamRow.SubExaminationIndex.ToString() + "_0.jpg",
						subExamRow.SubExaminationIndex.ToString() + "_1.jpg",
					};
                    foreach (string name in tmp)
                    {
                        target = Path.Combine(GlobalSettings.ReplayImgDir, name);
                        source = Path.Combine(sourceReplayDir, name);
                        if (File.Exists(source))
                        {
                            cf.AddFile(source, target);
                        }
                    }
                }

                // bvExamination table.
                filter = "ExaminationIndex = '" + examIndex.ToString() + "'";
                DataRow[] burnBVExamRows = RimedDal.Instance.s_dsBurnBVExamination.tb_BVExamination.Select(filter);
                foreach (DataRow burnBVExamRow in burnBVExamRows)
                {
                    DB.dsBVExamination.tb_BVExaminationRow newBVExamRow = dsBVExam.tb_BVExamination.Newtb_BVExaminationRow();
                    newBVExamRow.ItemArray = burnBVExamRow.ItemArray;
                    dsBVExam.tb_BVExamination.Rows.Add(newBVExamRow);
                }

                // ExaminationEvent table.
                filter = "Examination_Index = '" + examIndex.ToString() + "'";
                DataRow[] burnEventExaminationRows = RimedDal.Instance.s_dsBurnExaminationEvent.tb_ExaminationEvent.Select(filter);
                foreach (DataRow burnEventExamRow in burnEventExaminationRows)
                {
                    DB.dsEventExamination.tb_ExaminationEventRow newEventExamRow = dsEventExam.tb_ExaminationEvent.Newtb_ExaminationEventRow();
                    newEventExamRow.ItemArray = burnEventExamRow.ItemArray;
                    dsEventExam.tb_ExaminationEvent.Rows.Add(newEventExamRow);

                    string[] eventGates = Directory.GetFiles(Path.Combine(m_restoreLocation, "GateImg"), newEventExamRow.Event_Index + "*.jpg", SearchOption.TopDirectoryOnly);
                    if (eventGates.Length > 0)
                    {
                        foreach (string eventGate in eventGates)
                        {
                            target = Path.Combine(GlobalSettings.GateImgDir, Path.GetFileName(eventGate));
                            source = eventGate;
                            cf.AddFile(source, target);
                        }
                    }
                }

                // gateExamination table & GateImg files. 
                filter = "ExaminationIndex = '" + examIndex.ToString() + "'";
                DataRow[] burnGateExamRows = RimedDal.Instance.s_dsBurnGateExamination.tb_GateExamination.Select(filter);
                foreach (DataRow burnGateExamRow in burnGateExamRows)
                {
                    DB.dsGateExamination.tb_GateExaminationRow gateExamRow = dsGateExam.tb_GateExamination.Newtb_GateExaminationRow();
                    gateExamRow.ItemArray = burnGateExamRow.ItemArray;
                    dsGateExam.tb_GateExamination.Rows.Add(gateExamRow);

                    // add Gates Img file to copy buffer.
                    var suffix = "_M";
                    if (gateExamRow.Name.EndsWith("-L"))
                        suffix = "_L";
                    else if (gateExamRow.Name.EndsWith("-R"))
                        suffix = "_R";

                    target = Path.Combine(GlobalSettings.GateImgDir, gateExamRow.Gate_Index + suffix + ".jpg");
                    source = Path.Combine(m_restoreLocation, "GateImg", gateExamRow.Gate_Index + suffix + ".jpg");
                    if (File.Exists(source))
                        cf.AddFile(source, target);

                    target = Path.Combine(GlobalSettings.SummaryImgDir, gateExamRow.Gate_Index + suffix + ".jpg");
                    source = Path.Combine(m_restoreLocation, "SummaryImg", gateExamRow.Gate_Index + suffix + ".jpg");
                    if (File.Exists(source))
                        cf.AddFile(source, target);

                    target = Path.Combine(GlobalSettings.TrendsDataDir, gateExamRow.ExaminationIndex + "_" + gateExamRow.ID + ".bmp");
                    source = Path.Combine(m_restoreLocation, "Trends", gateExamRow.ExaminationIndex + "_" + gateExamRow.ID + ".bmp");
                    if (File.Exists(source))
                        cf.AddFile(source, target);

                    //fake gate images
                    string[] tmp = new string[]
					{
						gateExamRow.Gate_Index.ToString() + ".jpg",
						gateExamRow.Gate_Index.ToString() + "_CenterView.jpg",
					};
                    foreach (string name in tmp)
                    {
                        target = Path.Combine(GlobalSettings.ReplayImgDir, name);
                        source = Path.Combine(sourceReplayDir, name);
                        if (File.Exists(source))
                        {
                            cf.AddFile(source, target);
                        }
                    }
                }

                //IP Images Restore
                target = Path.Combine(GlobalSettings.IpImagesDataDir, examIndex.ToString());
                source = Path.Combine(m_restoreLocation, "IpImages", examIndex.ToString());
                if (Directory.Exists(source))
                {
                    if (!Directory.Exists(target))
                        Directory.CreateDirectory(target);
                    string[] files = Directory.GetFiles(source);
                    foreach (string file in files)
                        cf.AddFile(file, Path.Combine(target, file.Substring(file.LastIndexOf('\\') + 1)));
                }
            }
            bool res = cf.DoOperation(CopyFilesDlg.TFileOperation.eCopy);
            RimedDal.Instance.UpdateRestoredData(dsExam, dsSubExam, dsBVExam, dsGateExam, dsEventExam);
            return res;
        }

        private void DisplayBurnPackage()
        {
            //RIMD-290: Backup 1 patient and when makes restore you see many patients in the list (fix comment)
            this.patientListBox.Items.Clear();
            this.m_patientsNames.Clear();
            this.m_patientsIDs.Clear();

            this.patientListBox.Enabled = true;
            this.listViewExamination.Enabled = true;
            foreach (Patient p in this.m_patients)
            {
                this.patientListBox.Items.Add(p, false);
                this.m_patientsNames.Add(p.Name);
                this.m_patientsIDs.Add(p.ID);
            }

            this.FillComboBoxCtrl(this.autoCompleteComboBoxName, this.m_patientsNames);
            this.FillComboBoxCtrl(this.autoCompleteComboBoxID, this.m_patientsIDs);
            try
            {
                this.autoCompleteComboBoxName.SelectedIndex = 0;
                this.autoCompleteComboBoxID.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
            }
        }

        #endregion Private Methods
    }
}
