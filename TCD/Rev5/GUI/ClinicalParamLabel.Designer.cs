namespace TCD2003.GUI
{
    partial class ClinicalParamLabel
    {
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ClinicalParamLabel));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.clinicalParamPanel = new Syncfusion.Windows.Forms.Tools.GradientPanel();
            this.lowerVar = new System.Windows.Forms.Label();
            this.upperVar = new System.Windows.Forms.Label();
            this.paramName = new System.Windows.Forms.Label();
            this.clinicalParamPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "");
            // 
            // clinicalParamPanel
            // 
            this.clinicalParamPanel.BackColor = System.Drawing.Color.Transparent;
            this.clinicalParamPanel.BackgroundColor = new Syncfusion.Drawing.BrushInfo(Syncfusion.Drawing.GradientStyle.Vertical, new Syncfusion.Drawing.BrushInfoColorArrayList(new System.Drawing.Color[] {
                System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192))))),
                System.Drawing.Color.White}));
            this.clinicalParamPanel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("clinicalParamPanel.BackgroundImage")));
            this.clinicalParamPanel.Border3DStyle = System.Windows.Forms.Border3DStyle.Flat;
            this.clinicalParamPanel.BorderColor = System.Drawing.Color.Transparent;
            this.clinicalParamPanel.BorderSingle = System.Windows.Forms.ButtonBorderStyle.None;
            this.clinicalParamPanel.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.clinicalParamPanel.Controls.Add(this.lowerVar);
            this.clinicalParamPanel.Controls.Add(this.upperVar);
            this.clinicalParamPanel.Controls.Add(this.paramName);
            this.clinicalParamPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.clinicalParamPanel.Location = new System.Drawing.Point(0, 0);
            this.clinicalParamPanel.Name = "clinicalParamPanel";
            this.clinicalParamPanel.Size = new System.Drawing.Size(96, 56);
            this.clinicalParamPanel.TabIndex = 0;
            // 
            // lowerVar
            // 
            this.lowerVar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lowerVar.BackColor = System.Drawing.Color.Transparent;
            this.lowerVar.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lowerVar.Location = new System.Drawing.Point(34, 32);
            this.lowerVar.Name = "lowerVar";
            this.lowerVar.Size = new System.Drawing.Size(59, 24);
            this.lowerVar.TabIndex = 2;
            this.lowerVar.Text = "-9.09";
            this.lowerVar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // upperVar
            // 
            this.upperVar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.upperVar.BackColor = System.Drawing.Color.Transparent;
            this.upperVar.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upperVar.Location = new System.Drawing.Point(43, 0);
            this.upperVar.Name = "upperVar";
            this.upperVar.Size = new System.Drawing.Size(50, 24);
            this.upperVar.TabIndex = 1;
            this.upperVar.Text = "1.02";
            this.upperVar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // paramName
            // 
            this.paramName.AutoSize = true;
            this.paramName.BackColor = System.Drawing.Color.Transparent;
            this.paramName.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.paramName.Location = new System.Drawing.Point(0, 19);
            this.paramName.Name = "paramName";
            this.paramName.Size = new System.Drawing.Size(41, 18);
            this.paramName.TabIndex = 0;
            this.paramName.Text = "Gain";
            this.paramName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ClinicalParamLabel
            // 
            this.Controls.Add(this.clinicalParamPanel);
            this.Name = "ClinicalParamLabel";
            this.Size = new System.Drawing.Size(96, 56);
            this.clinicalParamPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label paramName;
        private Syncfusion.Windows.Forms.Tools.GradientPanel clinicalParamPanel;
        private System.Windows.Forms.Label lowerVar;
        private System.Windows.Forms.Label upperVar;
        private System.ComponentModel.IContainer components;
    }
}