﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TCD2003.Utils;

namespace TCD2003.GUI
{
    public partial class AskRecordDlg : Form
    {
        public AskRecordDlg()
        {
            InitializeComponent();

            var strRes = ResourceManagerWrapper.GetWrapper(MainForm.ResourceManager); 
            this.lblMessage.Text = strRes.GetString("Would you like to save the examination?");
            this.btnSave.Text = strRes.GetString("Save");
            this.btnNotSave.Text = strRes.GetString("Don't Save");
            this.btnContinue.Text = strRes.GetString("Continue Recording");

            this.BackColor = GlobalSettings.BackgroundDlg;
            
        }
    }
}
