using System.Collections.Generic;
using System.Windows.Forms;

using TCD2003.Utils;

namespace TCD2003.GUI
{
	/// <summary>
	/// Summary description for AboutDlg.
	/// </summary>
	public partial class AskDlg : Form
	{
		public AskDlg()
		{
			this.InitializeComponent();

            var strRes = ResourceManagerWrapper.GetWrapper(MainForm.ResourceManager); 
            this.label1.Text = strRes.GetString("Cannot delete one side spectrum. Delete both sides\' spectrums?");
            this.Text = strRes.GetString("About Digi-Lite");
            
            this.BackColor = GlobalSettings.BackgroundDlg;
		}

		private void buttonNo_Click(object sender, System.EventArgs e)
		{
			ExceptionPublisherLog4Net.FormActionLog("buttonNo", this.Name, null);
            this.DialogResult = System.Windows.Forms.DialogResult.No;
            this.Close();
        }

		private void buttonYes_Click(object sender, System.EventArgs e)
		{
            List<LogField> fields = new List<LogField>();
			fields.Add(new LogField("label1", label1.Text));
			ExceptionPublisherLog4Net.FormActionLog("buttonYes", this.Name, fields);
            this.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.Close();
        }
	}
}
