using System;
using System.Collections.Generic;
using System.IO;

using Microsoft.Win32;

namespace TCD2003.GUI
{
    /// <summary>
    /// Summary description for ExportManager.
    /// </summary>
    public static class ExportManager
    {
        public static void AddRecord(ExportPackage package, DateTime currentTime, List<double> parameters)
        {
            ExportElement next = new ExportElement(parameters.Count);
            next.currentTime = currentTime;

            for (int i = 0; i < parameters.Count; i++)
                next.parameters[i] = (double)parameters[i];

            package.Log.Add(next);
        }

        public static void AddHeader(ExportPackage package, List<string> captions)
        {
            package.Header.Add("Trends");
            package.Header.AddRange(captions);
        }

        /// <summary>
        /// Save log file for the Unilateral mode
        /// </summary>
        /// <param name="package">data from TrendChart</param>
        /// <param name="fileName">name of csv-file</param>
        public static void SaveToFile(ExportPackage package, string fileName)
        {
            string separator = GetSeparator();
            using (StreamWriter sw = File.CreateText(fileName))
            {
                string fileHeader = String.Empty;
                for (int i = 0; i < package.Header.Count; i++)
                {
                    fileHeader += package.Header[i];
                    if (i == package.Header.Count - 1)
                        fileHeader += "\n";
                    else
                        fileHeader += separator;
                }
                sw.Write(fileHeader);

                foreach (ExportElement item in package.Log)
                {
                    string data = item.currentTime.ToLongTimeString() + separator;
                    for (int i = 0; i < item.parameters.Length; i++)
                    {
                        data += item.parameters[i].ToString();
                        if (i == item.parameters.Length - 1)
                            data += "\n";
                        else
                            data += separator;
                    }
                    sw.Write(data);
                }
            }
        }

        /// <summary>
        /// Save log file for the Bilateral mode
        /// </summary>
        /// <param name="package">data from TrendChart</param>
        /// <param name="package">data from TrendChart</param>
        /// <param name="fileName">name of csv-file</param>
        public static void SaveToFile(ExportPackage package1, ExportPackage package2, string fileName)
        {
            string separator = GetSeparator();
            using (StreamWriter sw = File.CreateText(fileName))
            {
                int header1Length = package1.Header.Count;
                int header2Length = package2.Header.Count;

                string preHeader = separator + "Probe_1";
                for (int i = 0; i < header1Length - 1; i++)
                {
                    preHeader += separator;
                }
                preHeader += "Probe_2";
                preHeader += "\n";
                sw.Write(preHeader);

                string fileHeader = String.Empty;
                for (int i = 0; i < header1Length; i++)
                {
                    fileHeader += package1.Header[i];
                    fileHeader += separator;
                }
                for (int i = 1; i < header2Length; i++)
                {
                    fileHeader += package2.Header[i];
                    if (i == header2Length - 1)
                        fileHeader += "\n";
                    else
                        fileHeader += separator;
                }
                sw.Write(fileHeader);

                for (int j = 0; j < package1.Log.Count; j++)
                {
                    ExportElement item = package1.Log[j];
                    string data = item.currentTime.ToLongTimeString() + separator;
                    for (int i = 0; i < item.parameters.Length; i++)
                    {
                        data += item.parameters[i].ToString();
                        data += separator;
                    }
                    item = package2.Log[j];
                    for (int i = 0; i < item.parameters.Length; i++)
                    {
                        data += item.parameters[i].ToString();
                        if (i == item.parameters.Length - 1)
                            data += "\n";
                        else
                            data += separator;
                    }
                    sw.Write(data);
                }
            }
        }

        private static string GetSeparator()
        {
            string separator = String.Empty;
            RegistryKey myKey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Rimed", true);
            if (myKey != null)
            {
                if (myKey.GetValue("ExportSeparator") != null)
                {
                    separator = Convert.ToString(myKey.GetValue("ExportSeparator"));
                }
                else
                {
                    separator = GlobalSettings.CommaSeparator;
                    myKey.SetValue("ExportSeparator", GlobalSettings.CommaSeparator);
                }
            }
            else
                separator = GlobalSettings.CommaSeparator;
            return separator;
        }
    }
}
