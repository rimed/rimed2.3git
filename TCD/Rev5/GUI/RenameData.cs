using System;

namespace TCD2003.GUI
{
	/// <summary>
	/// Summary description for RenameData.
	/// </summary>
	public class RenameData
	{
		public String name;
		public String name2=String.Empty;
		public Guid bvIndex1 = Guid.Empty;
		public Guid bvIndex2 = Guid.Empty;

		public GlobalTypes.ESide side;
		public bool renamed;
		public RenameData()
		{
			//
			// TODO: Add constructor logic here
			//
			name = "";
			side = GlobalTypes.ESide.Left;
			renamed = false;
		}

		public RenameData(String name, GlobalTypes.ESide side,bool renamed,Guid guid)
		{
			//
			// TODO: Add constructor logic here
			//

			this.name = name;
			this.side = side;
			this.renamed = renamed;
			this.bvIndex1 = guid;
		}

		public RenameData(String name,String name2 ,bool renamed, Guid guid1, Guid guid2)
		{
			//
			// TODO: Add constructor logic here
			//

			this.name = name;
			this.name2 = name2;
			this.side = GlobalTypes.ESide.BothSides;
			this.renamed = renamed;
			this.bvIndex1 = guid1;
			this.bvIndex2 = guid2;
		}
	}
}
