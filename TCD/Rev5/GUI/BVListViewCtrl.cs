using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Windows.Forms;

using TCD2003.DB;
using TCD2003.DSP;
using TCD2003.GUI.Images;
using TCD2003.Utils;

namespace TCD2003.GUI
{
    /// <summary>
    /// Summary description for BVListViewCtrl.
    /// The blood vessel control Allow the user to navigate through saved examinations
    /// to choose blood vessels to examine. To delete/return BVs in examination.
    /// The saving of the data is also from this control which probably the most significant in the
    /// application
    /// </summary>
    public partial class BVListViewCtrl : UserControl
    {
        private enum EListType { SingleList, DualList };

        public readonly ObjectArr ListViewArr;
        
        #region Private Fields

        private static readonly Color EXAMINED_BV_COLOR = Color.Blue;
        private static readonly Color UNEXAMINED_BV_COLOR = Color.Black;

        static private BVListViewCtrl s_sInstance;
        private readonly List<BV> m_bvCollection = new List<BV>();
        private bool m_addGates2SavedExam = false;
        private double m_tmpDepth = -1;
        private bool m_probeChanged = false;
        private Probe m_tmpProbe = null;
        private bool m_enabledChangeSelection = true;
        private bool m_examinationWasSaved = true;
        private String m_subExamNotes = String.Empty;
        private int m_currentSummaryScreenIndex = 0;

        #endregion Private Fields

        public event BVChangedDelegate BVChangedEvent;
        private delegate void UpdateBVListDelegate(Guid slectedBVId, bool aDisplayOnlyExamineBV);
        private delegate void ProbeChangedDelegate(Probe newProbe);
        private delegate void DeleteGateDataDelegate(dsGateExamination.tb_GateExaminationRow row);
        
        static public BVListViewCtrl sInstance
        {
            get
            {
                return s_sInstance;
            }
        }

        public Guid RecordingExaminationId { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public BVListViewCtrl()
        {
            InitializeComponent();

            ListType = EListType.SingleList;
            InSummaryStatus = GlobalTypes.TAdd2SummaryScreen.SelectedSpectrum;

            HardDriveSpaceKeeper.Instance.RowDataModeChanged += new RowDataModeChangedHandler(HardDriveSpaceKeeper_RowDataModeChanged);
            s_sInstance = this;

            imageList1.Images.Add(ImageManager.Instance.GetImage(ImageKey.DeleteRowData));
            imageList1.Images.Add(ImageManager.Instance.GetImage(ImageKey.SaveRowData));

            imageList2.Images.Add(ImageManager.Instance.GetImage(ImageKey.DeletedRowData));
            imageList2.Images.Add(ImageManager.Instance.GetImage(ImageKey.SavedRowData));

            ListViewArr = new ObjectArr(listView1, listView2); //Two lists
        }

        #region Public Properties

        private EListType ListType { get; set; }

        public GlobalTypes.TAdd2SummaryScreen InSummaryStatus { get; set; }

        public bool ExaminationWasSaved
        {
            get
            {
                return this.m_examinationWasSaved && !RimedDal.Instance.IsTemporaryLoadedExamination;
            }
            set
            {
                this.m_examinationWasSaved = value;
            }
        }

        public string SubExamNotes
        {
            get
            {
                return m_subExamNotes;
            }
            set
            {
                m_subExamNotes = value;

                //Ofer
                //var sc = listView1.SelectedIndices;

                //if (sc.Count == 0)
                //    return;

                //var bv = listView1.Items[sc[0]].Tag as BV;
                //if (bv == null)
                //    return;

                //Ofer
                var bv = SelectedBV;
                if (bv == null)
                    return;

                //RIMD-549: Notes are not saved for Monitoring examination
                if (LayoutManager.SystemLayoutMode == GlobalTypes.TSystemLayoutMode.Monitoring)
                {
                    if (!ExaminationWasSaved)
                        return;
                }
                else if (bv.BVStatus == BV.TBVStatus.eNotExaminatedYet)
                     return;

                var filter = "SubExaminationIndex = '" + bv.SubExamIndex + "'";
                var rows = RimedDal.Instance.s_dsSubExamination.tb_SubExamination.Select(filter);
                if (rows.Length != 0)
                {
                    rows[0]["Notes"] = m_subExamNotes;
                    if (ExaminationWasSaved)
                        RimedDal.Instance.UpdateCurSubExamination();
                }
            }
        }

        public string ExamNotes
        {
            get
            {
                if (RimedDal.Instance.s_dsExamination.tb_Examination.Rows.Count == 0)
                    return string.Empty;
                
                return (string)RimedDal.Instance.s_dsExamination.tb_Examination.Rows[0]["Notes"];
            }
            set
            {
                if (RimedDal.Instance.s_dsExamination.tb_Examination.Rows.Count == 0) 
                    return;
                
                try
                {
                    var str = (string)RimedDal.Instance.s_dsExamination.tb_Examination.Rows[0]["Notes"];
                    str = value.Substring(str.Length, value.Length - str.Length);

                    RimedDal.Instance.UpdateExamination(RimedDal.Instance.s_dsExamination.tb_Examination[0], "Notes", str);
                }
                catch (Exception ex)
                {
                    // do not update the String.
                    ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
                }
            }
        }

        #endregion Public Properties

        private void RemoveEvents()
        {
            if (MainForm.Instance != null)//Ofer
            {
                MainForm.Instance.NextBVEvent -= new NextBVDelegate(NextBVNotification);
                MainForm.Instance.CurrentBVEvent -= new Action(CurrentBVNotification);
                MainForm.Instance.PrevBVEvent -= new Action(PrevBVNotification);
            }

            GateView.GateViewDepthChanged -= new DepthChangedDelegate(DisplayDepth);
            LayoutManager.LoadExamEvent -= new LoadExamDelegate(LayoutManager_LoadExamEvent);
            LayoutManager.LoadBVExamEvent -= new LoadBVExamDelegate(LayoutManager_LoadBVExamEvent);
            LayoutManager.LoadGateExamEvent -= new LoadGateExamDelegate(LayoutManager_LoadGateExamEvent);
        }

        public void EventRegistry()
        {
            InSummaryStatus = (GlobalTypes.TAdd2SummaryScreen)DB.RimedDal.Instance.s_DS.tb_BasicConfiguration[0].Add2SummaryScreen;

            if (MainForm.Instance != null) //Ofer
            {
                MainForm.Instance.NextBVEvent += new NextBVDelegate(NextBVNotification);
                MainForm.Instance.CurrentBVEvent += new Action(CurrentBVNotification);
                MainForm.Instance.PrevBVEvent += new Action(PrevBVNotification);
            }

            GateView.GateViewDepthChanged += new DepthChangedDelegate(DisplayDepth);
            LayoutManager.LoadExamEvent += new LoadExamDelegate(LayoutManager_LoadExamEvent);
            LayoutManager.LoadBVExamEvent += new LoadBVExamDelegate(LayoutManager_LoadBVExamEvent);
            LayoutManager.LoadGateExamEvent += new LoadGateExamDelegate(LayoutManager_LoadGateExamEvent);
        }

        private dsExamination.tb_ExaminationRow CreateNewExam(Guid studyIndex, Guid patientIndex)
        {
            // create new row exam.
            dsExamination.tb_ExaminationRow examRow = RimedDal.Instance.s_dsExamination.tb_Examination.Newtb_ExaminationRow();
            examRow.Examination_Index = Guid.NewGuid();

            // the next variables can be changed, we set those value in case the user want to generate patient Report of unsaved examination.
            examRow.Patient_Index = patientIndex;
            examRow.BackupPath = String.Empty;
            examRow.Deleted = false;
            DateTime t = DateTime.Now;
            examRow.Date = t;
            examRow.Study_Index = studyIndex;
            examRow.TimeHours = t.Hour;
            examRow.TimeMinutes = t.Minute;
            examRow.Indication = "";
            examRow.Interpretation = "";
            examRow.Notes = "";
            examRow.IsTemporary = true;

            RimedDal.Instance.s_dsExamination.tb_Examination.Rows.Add(examRow);
            EventListViewCtrl.Instance.ExaminationDate = t;

            this.m_subExamNotes = String.Empty;
            this.ExaminationWasSaved = false;
            this.m_addGates2SavedExam = false;
            return examRow;
        }

        /// <summary>Assaf (08/01/2007) : to prevent saving the study in replay mode (creates huge problems)</summary>
        private void Init(GlobalTypes.TStudyType type)
        {
            if (type == GlobalTypes.TStudyType.eUnilateral)
            {
                this.ListType = EListType.SingleList;
            }
            else
            {
                this.ListType = EListType.DualList;
            }
            this.InitDisplay();
        }

        private void ReloadControlLabels()
        {
            var strRes = ResourceManagerWrapper.GetWrapper(MainForm.ResourceManager); 
            this.BVName.Text = strRes.GetString("BV Name");
            this.GateDepth.Text = strRes.GetString("Depth");
            this.menuItemReplayBV.Text = strRes.GetString("Replay BV");
            this.menuItemRename.Text = strRes.GetString("Rename BV");
            this.menuItemReturn.Text = strRes.GetString("Return BV");
            this.BVName2.Text = strRes.GetString("BV Name");
            this.GateDepth2.Text = strRes.GetString("Depth");
        }

        private void InitDisplay()
        {
            if (InvokeRequired)
            {
                BeginInvoke((Action)InitDisplay);
                return;
            }

            if (ListType == EListType.DualList)
            {
                listView1.Height = Height / 2;
                listView2.Height = Height / 2;
                listView1.Dock = DockStyle.Top;
                listView2.Dock = DockStyle.Bottom;
                listView1.Visible = true;
                listView2.Visible = true;
            }
            else
            {
                listView2.Visible = false;
                listView1.Dock = DockStyle.Fill;
            }
            ReloadControlLabels();
        }

        //private void CheckVisible()
        //{
        //    bool b1 = this.listView1.Visible;
        //    bool b2 = this.listView2.Visible;
        //    this.InitDisplay();
        //}

        private void VerifyIfItIsCleardataStructure()
        {
            if (!this.ExaminationWasSaved && this.m_addGates2SavedExam)
            {
                try
                {
                    throw new Exception("Data structure is not clear");
                }
                catch (Exception ex)
                {
                    ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
                    throw;
                }
            }
        }

        private void ClearDataStructure()
        {
            // Reset also the Summary Screen.
            SummaryScreen.Instance.ClearAll();
            if (!this.ExaminationWasSaved)
                this.ExaminationWasSaved = true;
            this.m_addGates2SavedExam = false;

            GC.Collect(0);
            GC.Collect(1);
            GC.Collect(2);
            GC.Collect();
        }

        //public void MarkSaved()
        //{
        //    if (!this.ExaminationWasSaved)
        //        this.ExaminationWasSaved = true;
        //    this.m_addGates2SavedExam = false;
        //}

        private void AddBV(BV bv)
        {
            m_bvCollection.Add(bv);
        }

        /// <summary>
        /// This function fill listview1 control with the BVs items. 
        /// </summary>
        /// <param name="selectedBVId">the guid id of the selected BV</param>
        /// <param name="displayOnlyExamineBV">flag</param>
        public void UpdateBVList(Guid selectedBVId, bool displayOnlyExamineBV)
        {
            if (InvokeRequired)
            {
                BeginInvoke((UpdateBVListDelegate)UpdateBVList, selectedBVId, displayOnlyExamineBV);
                return;
            }

            this.listView1.Items.Clear();
            this.listView2.Items.Clear();
            int selectedIndex = 0;
            int counter = 0;

            if (LayoutManager.SystemLayoutMode == GlobalTypes.TSystemLayoutMode.DiagnosticLoad ||
                LayoutManager.SystemLayoutMode == GlobalTypes.TSystemLayoutMode.MonitoringLoad)
            {
                this.listView1.StateImageList = this.imageList2;
                this.listView2.StateImageList = this.imageList2;
            }
            else
            {
                this.listView1.StateImageList = this.imageList1;
                this.listView2.StateImageList = this.imageList1;
            }

            foreach (BV bv in m_bvCollection)
            {
                if (displayOnlyExamineBV && bv.BVStatus != BV.TBVStatus.eExaminated)
                    continue;

                // adding BV to the list view.
                var item = new ListViewItem();
                item.Text = bv.FullName;
                item.Font = GlobalSettings.F21;

                item.Tag = bv;
                // mark exmined BV in blue color text.
                if (bv.BVStatus == BV.TBVStatus.eExaminated)
                {
                    item.ForeColor = EXAMINED_BV_COLOR;
                    ((BV)item.Tag).SaveOnlySummaryImages = RimedDal.Instance.SaveOnlySummaryImages;
                    item.Checked = true; // only this way list starts show state icons
                    item.Checked = !RimedDal.Instance.SaveOnlySummaryImages;
                }
                else
                    item.ForeColor = UNEXAMINED_BV_COLOR;

                if (bv.BVExaminationIndex == selectedBVId)
                    selectedIndex = counter;

                // in case of CW probe the depth and the width will be 0 and there is no need to display the depth of the BV.
                AddItemDepth(bv, item);

                this.AddItem(item, counter);
                counter++;
            }

            if (this.listView1.Items.Count != 0 && selectedBVId.CompareTo(Guid.Empty) != 0)
            {
                if (ListType == EListType.DualList)
                {
                    selectedIndex /= 2; // The counter was * 2 because it is Bilateral and we has two list
                    this.Visible = true;
                    this.Enabled = true;
                    this.listView2.Items[selectedIndex].Selected = false;
                    this.listView2.Items[selectedIndex].Selected = true;
                    this.listView2.Items[selectedIndex].Focused = true;
                }

                this.listView1.Items[selectedIndex].Selected = true;
                this.listView1.Items[selectedIndex].Focused = true;
            }
        }

        private static void AddItemDepth(BV bv, ListViewItem item)
        {
            if (bv.BVProbe.IsCW)
                item.SubItems.Add(String.Empty);
            else
                item.SubItems.Add(bv.DefaultDepthString);

            item.SubItems.Add(bv.DefaultDepthString);
        }

        private static void SetItemDepth(BV bv, ListViewItem item)
        {
            if (bv.BVProbe.IsCW)
                item.SubItems[1].Text = String.Empty;
            else
                item.SubItems[1].Text = bv.DefaultDepthString;

            item.SubItems[2].Text = bv.DefaultDepthString;
        }

        private void AddItem(ListViewItem item, int counter)
        {
            if (this.ListType == EListType.SingleList)
                this.AddItem(listView1, this.listView1.Items.Count, item);
            else
            {
                if ((counter % 2) == 0)
                    AddItem(this.listView1, this.listView1.Items.Count, item);
                else
                    AddItem(this.listView2, this.listView2.Items.Count, item);
            }
        }

        private void AddItem(ListView list, int position, ListViewItem item)
        {
            if (LayoutManager.SystemLayoutMode == GlobalTypes.TSystemLayoutMode.DiagnosticLoad ||
                LayoutManager.SystemLayoutMode == GlobalTypes.TSystemLayoutMode.MonitoringLoad)
            {
                item.Checked = true; // only this way list starts show state icons
                item.Checked = !((BV)item.Tag).SaveOnlySummaryImages;
            }

            list.Items.Insert(position, item);
        }

        private void FireBVChangedEvent(BV[] bvArr, object sender)
        {
            if (this.BVChangedEvent != null)
                this.BVChangedEvent(bvArr, sender);
        }

        /// <summary>
        /// This method select BV in list using guid of the SubExamination
        /// </summary>
        /// <param name="subExaminationIndex">SubExamination's guid; is equal for both of right and left BVs</param>
        public void SelectBVbyIndex(string subExaminationIndex)
        {
            for (int i = 0; i < listView1.Items.Count; i++)
            {
                var bv = listView1.Items[i].Tag as BV;
                if (bv != null && bv.SubExamIndex.ToString() == subExaminationIndex)
                {
                    listView1.Items[i].Selected = true;
                    break;
                }
            }
        }

        private int LastListItem()
        {
            int index = listView1.Items.Count;
            return index;
        }

        private bool NextBVNotification()
        {
            //Type of return value was changed to Boolean for last vessel marking
            //changed by Natalie - 11/4/2008
            if (this.ListType == EListType.DualList)
            {
                return DualNextBVNotification();
            }

            ListView.SelectedIndexCollection sc = this.listView1.SelectedIndices;

            if (sc.Count != 0)
            {
                int bvIndex = sc[0];
                if ((bvIndex + 1) >= this.LastListItem()) // get to the end of the list view.
                {
                    //Changed by Natalie - 11/4/2008
                    MessageBox.Show(MainForm.ResourceManager.GetString("Blood vessel list is finished"));
                    return false;
                }

                this.listView1.Items[bvIndex + 1].Selected = true;
                this.listView1.Items[bvIndex + 1].Focused = true;
            }
            else
            {
                this.listView1.Items[0].Selected = true;
                this.listView1.Items[0].Focused = true;
            }
            this.listView1.Focus();
            return true;
        }

        private bool DualNextBVNotification()
        {
            //Type of return value was changed to Boolean for last vessel marking changed by Natalie - 11/4/2008
            var sc = listView1.SelectedIndices;

            if (sc.Count != 0)
            {
                int bvIndex = sc[0];
                if ((bvIndex + 1) >= LastListItem()) // get to the end of the list view.
                {
                    MessageBox.Show(MainForm.ResourceManager.GetString("Blood vessel list is finished"));
                    return false;
                }

                this.listView1.Items[bvIndex + 1].Selected = true;
                this.listView1.Items[bvIndex + 1].Focused = true;
                this.listView2.Items[bvIndex + 1].Selected = true;
                this.listView2.Items[bvIndex + 1].Focused = true;
            }
            else
            {
                this.listView1.Items[0].Selected = true;
                this.listView1.Items[0].Focused = true;
                this.listView2.Items[0].Selected = true;
                this.listView2.Items[0].Focused = true;
            }
            this.listView1.Focus();
            this.listView2.Focus();
            return true;
        }

        private void CurrentBVNotification()
        {
            if (this.ListType == EListType.DualList)
            {
                this.DualCurrentBVNotification();
                return;
            }

            ListView.SelectedIndexCollection sc = this.listView1.SelectedIndices;

            if (sc.Count != 0)
            {
                int bvIndex = sc[0];
                if (bvIndex >= this.LastListItem()) // get to the end of the list view.
                    return;

                this.m_enabledChangeSelection = false;
                this.listView1.Items[bvIndex].Selected = false;
                this.m_enabledChangeSelection = true;
                this.listView1.Items[bvIndex].Selected = true;
                this.listView1.Items[bvIndex].Focused = true;
            }
            else
            {
                this.m_enabledChangeSelection = false;
                this.listView1.Items[0].Selected = false;
                this.m_enabledChangeSelection = true;
                this.listView1.Items[0].Selected = true;
                this.listView1.Items[0].Focused = true;
            }
            this.listView1.Focus();
        }

        private void DualCurrentBVNotification()
        {
            ListView.SelectedIndexCollection sc = this.listView1.SelectedIndices;

            if (sc.Count != 0)
            {
                int bvIndex = sc[0];
                if ((bvIndex) >= this.LastListItem()) // get to the end of the list view.
                    return;

                this.m_enabledChangeSelection = false;
                this.listView1.Items[bvIndex].Selected = false;
                this.m_enabledChangeSelection = true;
                this.listView1.Items[bvIndex].Selected = true;
                this.listView1.Items[bvIndex].Focused = true;

                this.listView2.Items[bvIndex].Selected = true;
                this.listView2.Items[bvIndex].Focused = true;
            }
            else
            {
                this.m_enabledChangeSelection = false;
                this.listView1.Items[0].Selected = false;
                this.m_enabledChangeSelection = true;

                this.listView1.Items[0].Selected = true;
                this.listView1.Items[0].Focused = true;
                this.listView2.Items[0].Selected = true;
                this.listView2.Items[0].Focused = true;
            }
            this.listView1.Focus();
            this.listView2.Focus();
        }

        private void PrevBVNotification()
        {
            if (this.ListType == EListType.DualList)
            {
                this.DualPrevBVNotification();
                return;
            }

            ListView.SelectedIndexCollection sc = this.listView1.SelectedIndices;

            if (sc.Count != 0)
            {
                if ((sc[0] - 1) == -1) // get to the start of the list view.
                    return;

                this.listView1.Items[sc[0] - 1].Selected = true;
                this.listView1.Items[sc[0]].Focused = true;
            }
            else
            {
                this.listView1.Items[0].Selected = true;
                this.listView1.Items[0].Focused = true;
            }
            this.listView1.Focus();
        }

        private void DualPrevBVNotification()
        {
            ListView.SelectedIndexCollection sc = this.listView1.SelectedIndices;

            if (sc.Count != 0)
            {
                if ((sc[0] - 1) == -1) // get to the start of the list view.
                    return;

                this.listView2.Items[sc[0] - 1].Selected = true;
                this.listView2.Items[sc[0]].Focused = true;
                this.listView1.Items[sc[0] - 1].Selected = true;
                this.listView1.Items[sc[0]].Focused = true;
            }
            else
            {
                this.listView2.Items[0].Selected = true;
                this.listView2.Items[0].Focused = true;
                this.listView1.Items[0].Selected = true;
                this.listView1.Items[0].Focused = true;
            }
            this.listView1.Focus();
        }

        /// <summary>
        /// this function mark the current BV with blue color.
        /// and add bv after the current BV [the same as the current].
        /// </summary>
        private void DuplicateBV()
        {
            if (ListType == EListType.DualList)
            {
                DualDuplicateBV();
                return;
            }

            // mark the current selected BV with blue color.
            ListView.SelectedIndexCollection sc = listView1.SelectedIndices;
            if (sc.Count == 0)
                return;

            int index = sc[0];
            listView1.Items[index].ForeColor = EXAMINED_BV_COLOR;

            // add new item after the selected BV.
            BV bv = new BV((BV)listView1.Items[index].Tag);

            if (!m_probeChanged)
                bv.BVProbe = ((BV)listView1.Items[index].Tag).BVProbe;
            else
                bv.BVProbe = m_tmpProbe;

            bv.DuplicateFlag = true;
            m_bvCollection.Insert(index + 1, bv);

            ListViewItem item = new ListViewItem();
            item.Text = bv.FullName;
            item.Font = GlobalSettings.F21;

            if (bv.BVStatus == BV.TBVStatus.eNotExaminatedYet)
                item.ForeColor = UNEXAMINED_BV_COLOR;
            else
                item.ForeColor = EXAMINED_BV_COLOR;

            item.Tag = bv;

            //RIMD-331: Need to keep values for Depth Gain, Power etc when making Freeze and then Unfreeze
            //RIMD-399: Parameter values on screen not change for 4 Mhz CW probe (when using Remote or not)
            //Commented by Natalie: 01/28/2011
            //There was a lot of strange code here, but we deleted it and now Depth changes correctly ^^

            if (/*!bv.BVProbe.IsCW &&*/ ((BV)listView1.Items[index].Tag).BVProbe.IsCW)
            {
                dsBVSetup.tb_BloodVesselSetupRow row = RimedDal.Instance.GetBVProperties(bv.BVIndex);
                bv.DefaultDepth = (double)row["Depth"];
            }

            AddItemDepth(bv, item);

            //Insert in a position
            AddItem(listView1, index + 1, item); //this.listView1.Items.Insert(index + 1, item1);

            // set the new item selected.
            listView1.Items[index].ForeColor = EXAMINED_BV_COLOR;
            listView1.Items[index + 1].Selected = true;
            listView1.Items[index + 1].Focused = true; // this is work around so the previose item will be refresh and will be colored with blue color.
            listView1.Focus();
        }

        private void DualDuplicateBV()
        {
            // mark the current selected BV with blue color.
            System.Windows.Forms.ListView.SelectedIndexCollection sc = this.listView1.SelectedIndices;
            if (sc.Count == 0)
                return;

            int index = sc[0];

            this.listView1.Items[index].ForeColor = EXAMINED_BV_COLOR;
            this.listView2.Items[index].ForeColor = EXAMINED_BV_COLOR;

            // add new item after the selected BV.
            BV bv1 = new BV((BV)listView1.Items[index].Tag);
            BV bv2 = new BV((BV)listView2.Items[index].Tag);

            if (!m_probeChanged)
            {
                bv1.BVProbe = ((BV)listView1.Items[index].Tag).BVProbe;
                bv2.BVProbe = ((BV)listView2.Items[index].Tag).BVProbe;
            }
            else
            {
                bv1.BVProbe = m_tmpProbe;
                bv2.BVProbe = m_tmpProbe;
            }

            bv1.DuplicateFlag = true;
            bv2.DuplicateFlag = true;

            this.m_bvCollection.Insert(index + 1, bv1);
            this.m_bvCollection.Insert(index + 1 + 1, bv2);

            ListViewItem item1 = new ListViewItem();
            ListViewItem item2 = new ListViewItem();

            item1.Text = bv1.FullName;
            item1.Font = GlobalSettings.F21;

            item2.Text = bv2.FullName;
            item2.Font = GlobalSettings.F21;

            if (bv1.BVStatus == BV.TBVStatus.eNotExaminatedYet)
            {
                item1.ForeColor = UNEXAMINED_BV_COLOR;
                item2.ForeColor = UNEXAMINED_BV_COLOR;
            }
            else
            {
                item1.ForeColor = EXAMINED_BV_COLOR;
                item2.ForeColor = EXAMINED_BV_COLOR;
            }

            item1.Tag = bv1;
            item2.Tag = bv2;

            if (m_tmpDepth != -1)
            {
                bv1.DefaultDepth = m_tmpDepth;
                bv2.DefaultDepth = m_tmpDepth;
                m_tmpDepth = -1;
            }

            SetItemDepth(bv1, item1);
            SetItemDepth(bv2, item2);

            //Insert in a position
            this.AddItem(listView1, index + 1, item1); //	this.listView1.Items.Insert(index + 1, item1);
            this.AddItem(listView2, index + 1, item2); // this.listView2.Items.Insert(index + 1, item2);

            // set the new item selected.
            this.listView1.Items[index].ForeColor = EXAMINED_BV_COLOR;
            this.listView1.Items[index + 1].Selected = true;
            this.listView1.Items[index + 1].Focused = true; // this is work around so the previose item will be refresh and will be colored with blue color.
            this.listView1.Focus();


            this.listView2.Items[index].ForeColor = EXAMINED_BV_COLOR;
            this.listView2.Items[index + 1].Selected = true;
            this.listView2.Items[index + 1].Focused = true; // this is work around so the previose item will be refresh and will be colored with blue color.
            this.listView2.Focus();
        }

        /// <summary>
        /// Rename function
        /// </summary>
        /// <returns></returns>
        private RenameData Rename()
        {
            if (listView1 == null)
                return null;

            // this will rename the BV in the bv list and in all the gates that refers to that BV.
            var sc = listView1.SelectedIndices;
            if (sc.Count == 0)
                return null;

            RenameData renameData;

            var index = sc[0];

            var bv = listView1.Items[index].Tag as BV;
            if (bv == null)
                return null;

            if (bv.BVStatus != BV.TBVStatus.eExaminated)
            {
                MessageBox.Show(MainForm.ResourceManager.GetString("Only examinated tests can be renamed !"));
                return null;
            }

            var dlg = new BVRenameDlg(ListType == EListType.SingleList, (int)bv.BVSide, bv.BVProbe.Name, bv.BVName);
            if (dlg.ShowDialog() != DialogResult.OK)
                return null;

            BV bv2 = null;
            if (ListType == EListType.DualList)
            {
                if (listView2 == null)
                    return null;

                bv2 = listView2.Items[index].Tag as BV;
                if (bv2 == null)
                    return null;
            }

            DuplicateBV();

            var oldSide = GlobalTypes.ESide.BothSides;
            if (ListType == EListType.SingleList)
            {
                bv.BVIndex  = dlg.Blood_Vessel_Index;
                bv.BVName   = dlg.BVName;
                oldSide     = bv.BVSide;
                bv.BVSide   = dlg.BVSide;

                renameData = new RenameData(bv.FullName, bv.BVSide, true, bv.BVIndex);
            }
            else
            {
                bv.BVIndex  = dlg.Blood_Vessel_Index;
                bv.BVName   = dlg.BVName;
                bv2.BVIndex = dlg.Blood_Vessel_Index2;
                bv2.BVName  = dlg.BVName;

                renameData = new RenameData(bv.FullName, bv2.FullName, true, bv.BVIndex, bv2.BVIndex);
            }

            bv.IsRenamed = true;
            listView1.Items[index].Text = bv.FullName;

            if (ListType == EListType.DualList)
            {
                bv2.IsRenamed = true;
                listView2.Items[index].Text = bv2.FullName;
                listView1.Items[index + 1].Selected = true;
                listView1.Items[index + 1].ForeColor = EXAMINED_BV_COLOR;
            }

            var gatesRows   = (DB.dsGateExamination.tb_GateExaminationRow[])RenameGetGates(bv.SubExamIndex);
            var bvRows      = (DB.dsBVExamination.tb_BVExaminationRow[])RenameGetBVExaminationRows(bv.BVExaminationIndex);

            //Update table tb_BVExamination - The DATABASE is not  ������
            //BVIndex and Name is found in both tables: GateExamination and BVExamination				
            if (ListType == EListType.SingleList)
            {
                bvRows[0]["Name"] = bv.BVName;
                bvRows[0]["BV_Index"] = bv.BVIndex;
                bvRows[0]["Side"] = bv.BVSide;
            }
            else
            {
                var bvRows2 = (DB.dsBVExamination.tb_BVExaminationRow[])RenameGetBVExaminationRows(bv2.BVExaminationIndex);
                bvRows[0]["Name"] = bv.BVName;
                bvRows[0]["BV_Index"] = bv.BVIndex;
                bvRows2[0]["Name"] = bv2.BVName;
                bvRows2[0]["BV_Index"] = bv2.BVIndex;
            }

            foreach (DataRow gateRow in gatesRows)
            {
                if (ListType == EListType.SingleList)
                {
                    gateRow["Name"] = bv.FullName;
                    gateRow["Side"] = bv.BVSide;
                    gateRow["BVIndex"] = bv.BVIndex;

                    SummaryScreen.Instance.RenameGate(gateRow, oldSide);

                    foreach (GateView gv in LayoutManager.theLayoutManager.GateViews)
                    {
                        var oldside = ".jpg";
                        if (oldSide == GlobalTypes.ESide.Left)
                            oldside = "_L.jpg";
                        if (oldSide == GlobalTypes.ESide.Right)
                            oldside = "_R.jpg";
                        if (oldSide == GlobalTypes.ESide.Middle)
                            oldside = "_M.jpg";

                        var sourceFileName = Path.Combine(GlobalSettings.SummaryImgDir, gv.Gate_Index + oldside);

                        var newside = ".jpg";
                        if (bv.BVSide == GlobalTypes.ESide.Left)
                            newside = "_L.jpg";
                        if (bv.BVSide == GlobalTypes.ESide.Right)
                            newside = "_R.jpg";
                        if (bv.BVSide == GlobalTypes.ESide.Middle)
                            newside = "_M.jpg";

                        var destFileName = Path.Combine(GlobalSettings.SummaryImgDir, gv.Gate_Index + newside);

                        //Ofer
                        if (! sourceFileName.Equals(destFileName, StringComparison.InvariantCultureIgnoreCase))
                        {
                            if (File.Exists(sourceFileName))
                            {
                                try
                                {
                                    if (File.Exists(destFileName))
                                    {
                                        ExceptionPublisherLog4Net.TraceLog(string.Format("Rename(...): File.Delete('{0}')   <--- FILE.DELETE", destFileName), Name);
                                        File.Delete(destFileName);
                                    }

                                    ExceptionPublisherLog4Net.TraceLog(string.Format("Rename(...): File.Move('{0}', '{1}')   <--- FILE.MOVE", sourceFileName, destFileName), Name);
                                    File.Move(sourceFileName, destFileName);
                                }
                                catch (Exception ex)
                                {
                                    ExceptionPublisherLog4Net.ExceptionErrorLog(ex, string.Format("Fail to Rename / Move file. Rename '{0}' as '{1}'.", sourceFileName, destFileName));
                                }
                            }
                        }
                    }
                }
                else
                {
                    Byte b = (Byte)gateRow["Side"];
                    if (b == 0)
                    {
                        gateRow["Name"] = bv.FullName;
                        gateRow["BVIndex"] = bv.BVIndex;
                        oldSide = GlobalTypes.ESide.Left;
                    }
                    else
                    {
                        gateRow["Name"] = bv2.FullName;
                        gateRow["BVIndex"] = bv2.BVIndex;
                        oldSide = GlobalTypes.ESide.Right;
                    }

                    SummaryScreen.Instance.RenameGate(gateRow, oldSide);
                }
            }

            return renameData;
        }

        public void ModeChanged()
        {
            if (InvokeRequired)
            {
                BeginInvoke((Action)ModeChanged);
                return;
            }

            if (ListType == EListType.DualList)
            {
                DualModeChanged();
                return;
            }

            ListView.SelectedIndexCollection sc;
            if (LayoutManager.SystemMode == GlobalTypes.TSystemModes.Offline)
            {
                // the system move to FREEZE mode.
                sc = listView1.SelectedIndices;
                if (sc.Count == 0)
                    return;

                ((BV)listView1.Items[sc[0]].Tag).BVStatus = BV.TBVStatus.eExaminated;
                //TODO: save BV to previous BV variable +_+ 
                listView1.Items[sc[0]].ForeColor = EXAMINED_BV_COLOR;
                listView1.Items[sc[0]].Focused = true;
            }
            else
            {
                if (ExaminationWasSaved)
                    m_addGates2SavedExam = true;

                // the system move to UNFREEZE mode.
                sc = listView1.SelectedIndices;
                if (sc.Count == 0)
                    return;

                if (((BV)listView1.Items[sc[0]].Tag).BVStatus == BV.TBVStatus.eExaminated)
                {
                    // TODO: check if we should duplicate the BV refers to user setting in the setup.

                    // this function display the new BV from the data Structure and set it as selected BV.
                    this.DuplicateBV();

                    // update the gateViews in case the probe change.
                    if (m_probeChanged)
                        m_probeChanged = false;
                }
                else
                {
                    m_tmpDepth = -1;
                    ((BV)listView1.Items[sc[0]].Tag).UpdateIndex();
                }

                sc = listView1.SelectedIndices;
                if (sc.Count == 0)
                    return;

                ((BV)listView1.Items[sc[0]].Tag).BVStatus = BV.TBVStatus.eExaminateNow;

                if (LayoutManager.SystemMode == GlobalTypes.TSystemModes.Online)
                {
                    ListViewItem item1 = listView1.Items[sc[0]];

                    ((BV)item1.Tag).SaveOnlySummaryImages = RimedDal.Instance.SaveOnlySummaryImages;
                    item1.Checked = true; // only this way list starts show state icons
                    item1.Checked = !RimedDal.Instance.SaveOnlySummaryImages;
                }
            }
        }

        private void DualModeChanged()
        {
            ListView.SelectedIndexCollection sc;
            if (LayoutManager.SystemMode == GlobalTypes.TSystemModes.Offline)
            {
                // the system move to FREEZE mode.
                sc = listView1.SelectedIndices;
                if (sc.Count == 0)
                    return;

                ((BV)listView1.Items[sc[0]].Tag).BVStatus = BV.TBVStatus.eExaminated;
                listView1.Items[sc[0]].ForeColor = EXAMINED_BV_COLOR;
                listView1.Items[sc[0]].Focused = true;

                ((BV)listView2.Items[sc[0]].Tag).BVStatus = BV.TBVStatus.eExaminated;
                listView2.Items[sc[0]].ForeColor = EXAMINED_BV_COLOR;
                listView2.Refresh();
            }
            else
            {
                if (ExaminationWasSaved)
                    m_addGates2SavedExam = true;

                // the system move to UNFREEZE mode.
                sc = listView1.SelectedIndices;
                if (sc.Count == 0)
                    return;

                if (((BV)listView1.Items[sc[0]].Tag).BVStatus == BV.TBVStatus.eExaminated)
                {
                    // TODO: check if we should duplicate the BV refers to user setting in the setup.

                    // this function display the new BV from the data Structure and set it as selected BV.
                    DualDuplicateBV();

                    // update the gateViews in case the probe change.
                    if (m_probeChanged)
                        m_probeChanged = false;
                }
                else
                {
                    m_tmpDepth = -1;
                    ((BV)listView1.Items[sc[0]].Tag).UpdateIndex();
                    ((BV)listView2.Items[sc[0]].Tag).UpdateIndex();
                }
                sc = listView1.SelectedIndices;
                if (sc.Count == 0)
                    return;

                ((BV)listView1.Items[sc[0]].Tag).BVStatus = BV.TBVStatus.eExaminateNow;
                ((BV)listView2.Items[sc[0]].Tag).BVStatus = BV.TBVStatus.eExaminateNow;

                if (LayoutManager.SystemMode == GlobalTypes.TSystemModes.Online)
                {
                    ListViewItem item1 = listView1.Items[sc[0]];
                    ListViewItem item2 = listView2.Items[sc[0]];

                    ((BV)item1.Tag).SaveOnlySummaryImages = RimedDal.Instance.SaveOnlySummaryImages;
                    item1.Checked = true; // only this way list starts show state icons
                    item1.Checked = !RimedDal.Instance.SaveOnlySummaryImages;

                    ((BV)item2.Tag).SaveOnlySummaryImages = RimedDal.Instance.SaveOnlySummaryImages;
                    item2.Checked = true; // only this way list starts show state icons
                    item2.Checked = !RimedDal.Instance.SaveOnlySummaryImages;
                }
            }
        }
        
        private void DeleteFromSummaryScreen(DB.dsGateExamination.tb_GateExaminationRow row)
        {
            String filter = "Gate_Index <> '" + row.Gate_Index.ToString() + "' AND Depth = " + row.Depth + " AND Name = '" + row.Name + "' AND InSummaryScreen = true";
            DataRow[] rows = RimedDal.Instance.s_dsGateExamination.tb_GateExamination.Select(filter, null, DataViewRowState.CurrentRows);
            if (rows.Length != 0)
            {
                SummaryScreen.Instance.RemoveGate((DB.dsGateExamination.tb_GateExaminationRow)rows[0]);
                this.DeleteGateData((dsGateExamination.tb_GateExaminationRow)rows[0]);
            }

            foreach (DataRow subRow in rows)
            {
                subRow.Delete();
            }

            RimedDal.Instance.UpdateCurExam2DB(RimedDal.Instance.IsTemporaryLoadedExamination);
        }

        public void DeleteFromSummaryScreen(string subExaminationIndex)
        {
            String filter = string.Format("SubExaminationIndex = '{0}'", subExaminationIndex);
            DataRow[] rows = RimedDal.Instance.s_dsGateExamination.tb_GateExamination.Select(filter, null, DataViewRowState.CurrentRows);
            foreach (DataRow row in rows)
            {
                this.DeleteGateData((dsGateExamination.tb_GateExaminationRow)row);
            }

            rows = RimedDal.Instance.s_dsSubExamination.tb_SubExamination.Select(filter);
            foreach (DataRow row in rows)
            {
                row.Delete();
            }
            
            RimedDal.Instance.UpdateCurExam2DB(RimedDal.Instance.IsTemporaryLoadedExamination);
        }
        
        public bool CanDeleteFromSummaryScreen(string subExaminationIndex, string bvName)
        {
            String filter = string.Format("SubExaminationIndex = '{0}' AND Name = '{1}'", subExaminationIndex, bvName);
            DataRow[] rows = RimedDal.Instance.s_dsGateExamination.tb_GateExamination.Select(filter);
            bool result = true;
            if (rows.Length == 0)
                result = false;
            else if (rows.Length > 1)
            {
                var strRes = ResourceManagerWrapper.GetWrapper(MainForm.ResourceManager); 
                string title = string.Format(strRes.GetString("DeleteMultipleSpectrumsTitle"), rows.Length);
                string msg = string.Format(strRes.GetString("DeleteMultipleSpectrumsMessage"), rows.Length);

                if (MessageBox.Show(msg, title, MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                    result = false;
            }
            return result;
        }

        /// <summary>
        /// Deletes GateExamination and BVExamination records for removed gate from DB
        /// Deletes all linked data and image files
        /// RIMD-343: If delete the BV from Summary screen - it is still saved in the list in Main Screen
        /// </summary>
        /// <param name="row"></param>
        private void DeleteGateData(dsGateExamination.tb_GateExaminationRow row)
        {
            if (InvokeRequired)
            {
                BeginInvoke((DeleteGateDataDelegate)DeleteGateData, row);
                return;
            }

            string gateIndex = row.Gate_Index.ToString();
            string subexaminationIndex = row.SubExaminationIndex.ToString();
            string bvexaminationIndex = row.BVExaminationIndex.ToString();
            int itemsCount = this.listView1.Items.Count;

            for (int i = 0; i < itemsCount; i++)
            {
                if (((BV)(this.listView1.Items[i].Tag)).BVExaminationIndex == row.BVExaminationIndex)
                {
                    int count = 0;
                    for (int j = 0; j < itemsCount; j++)
                        if (((BV)this.listView1.Items[i].Tag).BVIndex == ((BV)this.listView1.Items[j].Tag).BVIndex)
                            count++;
                    if (count > 1)
                    {
                        this.listView1.Items.RemoveAt(i);
                        this.m_bvCollection.RemoveAt(i);
                        itemsCount--;
                    }
                    else
                    {
                        m_bvCollection[i].BVStatus = BV.TBVStatus.eNotExaminatedYet;
                        this.UpdateBVList(((BV)listView1.Items[i].Tag).BVIndex, false);
                    }
                    if (i < itemsCount)
                    {
                        this.listView1.Items[i].Selected = true;
                        this.listView1.Items[i].Focused = true;
                    }
                    else
                    {
                        this.listView1.Items[itemsCount - 1].Selected = true;
                        this.listView1.Items[itemsCount - 1].Focused = true;
                    }
                    break;
                }
            }

            row.Delete();
            String filterBVExamination = "BVExaminationIndex = '" + bvexaminationIndex + "'";
            DataRow[] rows = RimedDal.Instance.s_dsBVExamination.tb_BVExamination.Select(filterBVExamination, null, DataViewRowState.CurrentRows);
            if (rows.Length != 0)
                rows[0].Delete();

            //remove all Gate files
            DeleteFileByIndex(GlobalSettings.TmpImgDir, gateIndex);
            DeleteFileByIndex(GlobalSettings.ReplayImgDir, gateIndex);
            DeleteFileByIndex(GlobalSettings.SummaryImgDir, gateIndex);
            DeleteFileByIndex(GlobalSettings.RawDataDir, subexaminationIndex);
        }

        private void DeleteFileByIndex(string directory, string index)
        {
            string[] filelist = Directory.GetFiles(directory);
            foreach (string filename in filelist)
            {
                if (filename.IndexOf(index) != -1)
                {
                    ExceptionPublisherLog4Net.TraceLog(string.Format("DeleteFileByIndex(...): File.Delete('{0}')   <--- FILE.DELETE", filename), Name);
                    File.Delete(filename);
                    break;
                }
            }
        }

        private DataRow[] RenameGetBVExaminationRows(Guid index)
        {
            String filter = "BVExaminationIndex = '" + index.ToString() + "'";
            DataRow[] rows = RimedDal.Instance.s_dsBVExamination.tb_BVExamination.Select(filter);
            return rows;
        }

        private DataRow[] RenameGetGates(Guid subExamIndex)
        {
            String filter = "SubExaminationIndex = '" + subExamIndex.ToString() + "'";
            DataRow[] rows = RimedDal.Instance.s_dsGateExamination.tb_GateExamination.Select(filter);
            return rows;
        }

        private bool IsGateExistInSummary(DB.dsGateExamination.tb_GateExaminationRow row)
        {
            String filter = "Gate_Index <> '" + row.Gate_Index.ToString() + "' AND Depth = " + row.Depth.ToString() + " AND Name = '" + row.Name + "' AND InSummaryScreen = true";
            DataRow[] rows = RimedDal.Instance.s_dsGateExamination.tb_GateExamination.Select(filter, null, System.Data.DataViewRowState.CurrentRows);
            return rows.Length != 0;
        }

        private GlobalTypes.TArrayAction InSummaryScreen(DB.dsGateExamination.tb_GateExaminationRow row)
        {
            if (this.InSummaryStatus == GlobalTypes.TAdd2SummaryScreen.OpenSpectrum)
                return GlobalTypes.TArrayAction.eAdd;

            if (this.InSummaryStatus == GlobalTypes.TAdd2SummaryScreen.SelectedSpectrum)
            {
                if ((bool)row["InSummaryScreen"])
                    return GlobalTypes.TArrayAction.eAdd;
            }
            else if (this.IsGateExistInSummary(row))
            {
                switch (this.InSummaryStatus)
                {
                    case GlobalTypes.TAdd2SummaryScreen.CustomizeReplaceOld:
                        this.DeleteFromSummaryScreen(row);
                        return GlobalTypes.TArrayAction.eAdd;
                    //						break;
                    case GlobalTypes.TAdd2SummaryScreen.CustomizeAddNew:
                        return GlobalTypes.TArrayAction.eAdd;
                    //						break;
                    case GlobalTypes.TAdd2SummaryScreen.CustomizeAsk:
                        {
                            AddGV2SummaryDlg dlg = new AddGV2SummaryDlg();
                            if (dlg.ShowDialog() == DialogResult.OK)
                            {
                                if (dlg.Add2SummaryScreen == GlobalTypes.TAdd2SummaryScreen2.AddNew)
                                    return GlobalTypes.TArrayAction.eAdd;
                                else if (dlg.Add2SummaryScreen == GlobalTypes.TAdd2SummaryScreen2.ReplaceOld)
                                {
                                    this.DeleteFromSummaryScreen(row);
                                    return GlobalTypes.TArrayAction.eAdd;
                                }
                            }
                        }
                        break;
                }
            }
            else
                return GlobalTypes.TArrayAction.eAdd;

            return GlobalTypes.TArrayAction.eDoNothing;
        }

        private delegate void SaveLayoutDelegate(Guid index, List<GateView> gatesViews, bool displayAutoscan0, bool displayAutoscan1);

        /// <summary>Activate by the LayoutManager, when the system move to offline (Freeze mode). function save the current layout to the local dataset's.</summary>
        /// <param name="subExamId"></param>
        /// <param name="gatesViews"></param>
        /// <param name="displayAutoscan0"></param>
        /// <param name="displayAutoscan1"></param>
        public void SaveLayout(Guid subExamId, List<GateView> gatesViews, bool displayAutoscan0, bool displayAutoscan1)
        {
            ExceptionPublisherLog4Net.TraceLog(string.Format("SaveLayout(subExamIdex={0}, gatesViews, displayAutoscan0={1}, displayAutoscan1={2})", subExamId, displayAutoscan0, displayAutoscan1), Name);

            if (LayoutManager.SystemLayoutMode == GlobalTypes.TSystemLayoutMode.Monitoring && MainForm.Instance.IsMonitoringSaved)
                return;

            //Ofer
            if (listView1.SelectedIndices.Count == 0)
                return;

            //RIMD-562: Digi-Lite is stuck after MC or Summary Screen when using Remote Control
            if (InvokeRequired)
            {
                //Ofer: Unhandled exception bugFix: System.InvalidOperationException: Collection was modified; enumeration operation may not execute (ErrorCode: 0x80131509). 
                Invoke(new SaveLayoutDelegate(SaveLayout), subExamId, gatesViews, displayAutoscan0, displayAutoscan1); //Ofer: Replace BeginInvoke with a blocking Invoke call
                return;
            }

            if (LayoutManager.theLayoutManager.StudyType != GlobalTypes.TStudyType.eUnilateral)
            {
                bilateralSaveLayout(subExamId, gatesViews, displayAutoscan0, displayAutoscan1);
                return;
            }

            //Ofer
            var selectedIndices = listView1.SelectedIndices[0];
            var bv              = listView1.Items[selectedIndices].Tag as BV;
            if (bv == null)
            {
                ExceptionPublisherLog4Net.ExceptionErrorLog(null, string.Format("{0}.SaveLayout(subExamIdex={1},...): NO BV Selected", Name, subExamId));
                return;
            }

            var errTrace = string.Empty;
            var examIndex = Guid.Empty;
            dsBVExamination.tb_BVExaminationRow     bvExamRow   = null;
            try     //Ofer
            {
                examIndex       = (Guid)RimedDal.Instance.s_dsExamination.tb_Examination.Rows[0]["Examination_Index"];

                var subExamRow  = RimedDal.Instance.s_dsSubExamination.tb_SubExamination.Newtb_SubExaminationRow();
                subExamRow.SubExaminationIndex = subExamId;
                subExamRow.ExaminationIndex = examIndex;
                subExamRow.BackupPath = "";
                subExamRow.Notes = SubExamNotes;
                subExamRow.Deleted = false;

                subExamRow.TotalMilliseconds = gatesViews[0].Spectrum.Graph.LastDrawTime.TotalMilliseconds;

                // save the selected BV. Is's important to save at the first an examination and only then start initialize summary view controls
                bvExamRow = RimedDal.Instance.s_dsBVExamination.tb_BVExamination.Newtb_BVExaminationRow();
                bvExamRow.ExaminationIndex = examIndex;
                bv.SubExamIndex = subExamId;
                bv.CopyTo(bvExamRow);
                bvExamRow.Deleted = false;

                errTrace = string.Format("BVExamination.Add(bvExamRow.BVExamIndex={0})", bvExamRow.BVExaminationIndex);
                RimedDal.Instance.s_dsBVExamination.tb_BVExamination.Rows.Add(bvExamRow);
                errTrace = string.Empty;

                subExamRow.DisplayAutoscan0 = displayAutoscan0;
                subExamRow.DisplayAutoscan1 = displayAutoscan1;

                errTrace = string.Format("SubExamination.Add(subExamRow.Index={0})", subExamRow.ExaminationIndex);
                RimedDal.Instance.s_dsSubExamination.tb_SubExamination.Rows.Add(subExamRow);
                errTrace = string.Empty;
            }
            catch (Exception ex)
            {
                var examName    = bvExamRow == null ? "bvExamRow is NULL" : bvExamRow.Name;
                var msg         = string.Format("Examination save FAIL. SaveLayout(subExamIdex={0}, gatesViews, displayAutoscan0={1}, displayAutoscan1={2}): examIndex={3}, bvExamRow.Name={4}. {5}",
                                                    subExamId, displayAutoscan0, displayAutoscan1, examIndex, examName, errTrace);

                //ExceptionPublisherLog4Net.ExceptionErrorLog(ex, msg);

                throw new ApplicationException(msg, ex);
            }

            var gates = gatesViews.ToArray();   //Ofer: Avoid - collection modified during iteration exception
            foreach (var gv in gates)
            {
                if (gv == null || !gv.Visible)  //Ofer 
                    continue;

                dsGateExamination.tb_GateExaminationRow gateExamRow = null;

                //Ofer: System.Data.ConstraintException Message: Column 'BVExaminationIndex' is constrained to be unique.  Value '' is already present.
                try
                {
                    gateExamRow = RimedDal.Instance.s_dsGateExamination.tb_GateExamination.Newtb_GateExaminationRow();
                    gateExamRow.Gate_Index  = Guid.NewGuid();
                    
                    // TODO: need to check if the new Guid already exist. - Gedalia.
                    gv.CopyTo(gateExamRow);
                    gateExamRow.ExaminationIndex = examIndex;

                    gateExamRow.BVExaminationIndex = bv.BVExaminationIndex;
                    gateExamRow.BVIndex = bv.BVIndex;
                    gateExamRow.SubExaminationIndex = subExamId;
                    gateExamRow.Deleted = false;

                    gateExamRow.OnSummaryScreenIndex = m_currentSummaryScreenIndex;
                    m_currentSummaryScreenIndex++;

                    //RIMD-399: Parameter values on screen not change for 4 Mhz CW probe (when using Remote or not)
                    if (gateExamRow.InSummaryScreen)
                    {
                        SummaryScreen.Instance.AddGate(gateExamRow, false);
                    }
                    else if (InSummaryScreen(gateExamRow) == GlobalTypes.TArrayAction.eAdd)
                    {
                        gateExamRow.InSummaryScreen = true;
                        SummaryScreen.Instance.AddGate(gateExamRow, false);
                    }

                    RimedDal.Instance.s_dsGateExamination.tb_GateExamination.Rows.Add(gateExamRow);
                }
                catch (Exception ex)
                {
                    var examName    = gateExamRow == null ? "gateExamRow is NULL" : gateExamRow.Name;       
                    var bvName      = gateExamRow == null ? "gateExamRow is NULL" : gateExamRow.BVName;     
                    var msg = string.Format("Gate save error. SaveLayout(subExamIdex={0}, gatesViews, displayAutoscan0={1}, displayAutoscan1={2}): gateExamRow.Name={3}, gateExamRow.BVName={4}. GateExamination.Add(gateExamRow)",    //Ofer
                                                        subExamId, displayAutoscan0, displayAutoscan1, examName, bvName);

                    ExceptionPublisherLog4Net.ExceptionErrorLog(ex, msg);
                }
            }
        }

        //private BV getBVByBVIndex(Guid bvIndex)
        //{
        //    if (bvIndex == Guid.Empty)
        //        return null;

        //    foreach (var bv in m_bvCollection)
        //    {
        //        if (bv.BVIndex.Equals(bvIndex))
        //            return bv;
        //    }

        //    return null;
        //}



        /// <summary>activate by the LayoutManager, when the system move to offline (Freeze mode). function save the current layout to the local dataset's.</summary>
        /// <param name="index"></param>
        /// <param name="gatesViews"></param>
        /// <param name="displayAutoscan0"></param>
        /// <param name="displayAutoscan1"></param>
        private void bilateralSaveLayout(Guid index, List<GateView> gatesViews, bool displayAutoscan0, bool displayAutoscan1)
        {
            if (listView1.SelectedIndices.Count == 0 || listView2.SelectedIndices.Count == 0)
                return;

            //Ofer
            var selectedIndices = listView1.SelectedIndices[0];
            var bv = listView1.Items[selectedIndices].Tag as BV;
            if (bv == null)
            {
                ExceptionPublisherLog4Net.ExceptionErrorLog(null, string.Format("{0}.SaveLayout(subExamIdex={1},...): NO BV Selected", Name, index));
                return;
            }

            //Ofer
            selectedIndices = listView2.SelectedIndices[0];
            var bv2 = listView2.Items[selectedIndices].Tag as BV;
            if (bv2 == null)
            {
                ExceptionPublisherLog4Net.ExceptionErrorLog(null, string.Format("{0}.SaveLayout(subExamIdex={1},...): NO BV2 Selected", Name, index));
                return;
            }

            var errTrace = string.Empty;
            var examIndex = Guid.Empty;
            dsBVExamination.tb_BVExaminationRow bvExamRow = null;
            try //Ofer
            {
                examIndex = (Guid)RimedDal.Instance.s_dsExamination.tb_Examination.Rows[0]["Examination_Index"];

                var subExamRow = RimedDal.Instance.s_dsSubExamination.tb_SubExamination.Newtb_SubExaminationRow();
                subExamRow.SubExaminationIndex = index;
                subExamRow.ExaminationIndex = examIndex;
                subExamRow.BackupPath = "";
                subExamRow.Notes = SubExamNotes;
                subExamRow.Deleted = false;
                subExamRow.TotalMilliseconds = gatesViews[0].Spectrum.Graph.LastDrawTime.TotalMilliseconds;

                bvExamRow = RimedDal.Instance.s_dsBVExamination.tb_BVExamination.Newtb_BVExaminationRow();
                bvExamRow.ExaminationIndex = examIndex;

                //BV bv = (BV)this.listView1.Items[sc1[0]].Tag;
                bv.SubExamIndex = index;
                bv.CopyTo(bvExamRow);
                //bvExamRow.Side = (int)GlobalTypes.TSide.eBothSides;
                bvExamRow.Deleted = false;

                errTrace = string.Format("BVExamination.Add(bvExamRow.BVExamIndex={0})", bvExamRow.BVExaminationIndex);
                RimedDal.Instance.s_dsBVExamination.tb_BVExamination.Rows.Add(bvExamRow);
                errTrace = string.Empty;

                // Port 2
                var bvExamRow2 = RimedDal.Instance.s_dsBVExamination.tb_BVExamination.Newtb_BVExaminationRow();
                bvExamRow2.ExaminationIndex = examIndex;

                //BV bv2 = (BV)listView2.Items[sc2[0]].Tag;
                bv2.SubExamIndex = index;
                bv2.CopyTo(bvExamRow2);
                bvExamRow2.Deleted = false;

                errTrace = string.Format("BVExamination.Add(bvExamRow2.BVExamIndex={0})", bvExamRow2.BVExaminationIndex);
                RimedDal.Instance.s_dsBVExamination.tb_BVExamination.Rows.Add(bvExamRow2);
                errTrace = string.Empty;

                subExamRow.DisplayAutoscan0 = displayAutoscan0;
                subExamRow.DisplayAutoscan1 = displayAutoscan1;

                errTrace = string.Format("SubExamination.Add(subExamRow.Index={0})", subExamRow.ExaminationIndex);
                RimedDal.Instance.s_dsSubExamination.tb_SubExamination.Rows.Add(subExamRow);
                errTrace = string.Empty;
            }
            catch (Exception ex)
            {
                var examName = bvExamRow == null ? "bvExamRow is NULL" : bvExamRow.Name;
                var msg = string.Format("Examination save FAIL. bilateralSaveLayout(subExamIdex={0}, gatesViews, displayAutoscan0={1}, displayAutoscan1={2}): examIndex={3}, bvExamRow.Name={4}. {5}",
                                            index, displayAutoscan0, displayAutoscan1, examIndex, examName, errTrace);

                //ExceptionPublisherLog4Net.ExceptionErrorLog(ex, msg);

                throw new ApplicationException(msg, ex);
            }


            var gates = gatesViews.ToArray();   //Ofer: Overcome collection modified during iteration exception
            foreach (var gv in gates)
            {
                if (gv == null || !gv.Visible)  //Ofer  
                    continue;

                //Ofer: System.Data.ConstraintException Message: Column 'BVExaminationIndex' is constrained to be unique.  Value '' is already present.
                dsGateExamination.tb_GateExaminationRow gateExamRow = null;
                try
                {
                    gateExamRow = RimedDal.Instance.s_dsGateExamination.tb_GateExamination.Newtb_GateExaminationRow();
                    gateExamRow.Gate_Index = Guid.NewGuid();
                
                    // TODO: need to check if the new Guid already exist. - Gedalia.
                    gv.CopyTo(gateExamRow);
                    gateExamRow.ExaminationIndex = examIndex;
                    gateExamRow.OnSummaryScreenIndex = m_currentSummaryScreenIndex;
                    m_currentSummaryScreenIndex++;

                    if (gv.Id < 8)
                    {
                        gateExamRow.BVExaminationIndex = bv.BVExaminationIndex;
                        gateExamRow.BVIndex = bv.BVIndex;
                    }
                    else
                    {
                        gateExamRow.BVExaminationIndex = bv2.BVExaminationIndex;
                        gateExamRow.BVIndex = bv2.BVIndex;
                    }

                    gateExamRow.SubExaminationIndex = index;
                    gateExamRow.Deleted             = false;


                    if (gateExamRow.InSummaryScreen)
                    {
                        SummaryScreen.Instance.AddGate(gateExamRow, false);
                    }
                    else if (InSummaryScreen(gateExamRow) == GlobalTypes.TArrayAction.eAdd)
                    {
                        gateExamRow.InSummaryScreen = true;
                        SummaryScreen.Instance.AddGate(gateExamRow, false);
                    }

                        RimedDal.Instance.s_dsGateExamination.tb_GateExamination.Rows.Add(gateExamRow);
                }
                catch (Exception ex)
                {
                    var examName    = gateExamRow == null ? "gateExamRow is NULL" : gateExamRow.Name;       //Ofer
                    var bvName      = gateExamRow == null ? "gateExamRow is NULL" : gateExamRow.BVName;     //Ofer
                    var msg = string.Format("Gate save error. bilateralSaveLayout(subExamIdex={0}, gatesViews, displayAutoscan0={1}, displayAutoscan1={2}): gateExamRow.Name={3}, gateExamRow.BVName={4}. GateExamination.Add(gateExamRow)",    //Ofer 
                                                        index, displayAutoscan0, displayAutoscan1, examName, bvName);

                    ExceptionPublisherLog4Net.ExceptionErrorLog(ex, msg);
                }
            }
        }

        private void DisplayDepth(object obj, double depth)
        {
            if (InvokeRequired)
            {
                BeginInvoke((DepthChangedDelegate)DisplayDepth, obj, depth);
                return;
            }

            //Ofer
            if (obj is GateView) 
                setSelectedIndexDepth((GateView.SideSelected == 0 ? listView1 : listView2), depth);
        }

        private void setSelectedIndexDepth(ListView listView, double depth)
        {
            if (listView == null)
                return;

            var sc = listView.SelectedIndices;
            if (sc.Count == 0)
                return;

            var bv = listView.Items[sc[0]].Tag as BV;
            if (bv == null)
                return;

            if (bv.BVStatus == BV.TBVStatus.eExaminated)
            {
                m_tmpDepth = depth;
            }
            else
            {
                bv.DefaultDepth = depth;
                SetItemDepth(bv, listView.Items[sc[0]]);
            }
        }

        public StudyLayout GetLayout(BV[] bvArr)
        {
            int[] gates = new int[bvArr.Length];
            bool[] displayAutoscan = new bool[bvArr.Length];
            StudyLayout studyLayout = new StudyLayout();

            for (int i = 0; i < bvArr.Length; ++i)
            {
                var filter = "BVExaminationIndex = '" + bvArr[i].BVExaminationIndex + "' ";
                DataRow[] gateExamRows = RimedDal.Instance.s_dsGateExamination.tb_GateExamination.Select(filter);

                //int fwwef = gateExamRows.Length;
                gates[i] = gateExamRows.Length;

                if (gateExamRows.Length == 0)
                    continue;

                filter = "SubExaminationIndex = '" + ((Guid)gateExamRows[0]["SubExaminationIndex"]) + "'";
                DataRow[] subExamRows = RimedDal.Instance.s_dsSubExamination.tb_SubExamination.Select(filter);

                int fw = subExamRows.Length;
                if (fw > 0)
                    displayAutoscan[i] = (bool)subExamRows[0]["DisplayAutoscan" + i.ToString()];
                else
                    displayAutoscan[i] = true;
            }

            studyLayout.m_GateNumLeft = gates[0];
            studyLayout.m_DispAutoscan0 = displayAutoscan[0];

            if (bvArr.Length > 1)
            {
                studyLayout.m_GateNumRight = gates[1];
                studyLayout.m_DispAutoscan1 = displayAutoscan[1];
            }
            else
            {
                studyLayout.m_GateNumRight = 0;
                studyLayout.m_DispAutoscan1 = false;
            }
            return studyLayout;
        }

        public void ProbeChanged(Probe newProbe)
        {
            if (InvokeRequired)
            {
                BeginInvoke((ProbeChangedDelegate)ProbeChanged, newProbe);
                return;
            }

            int index = newProbe.ProbeIndex;

            ListView listView = (ListView)ListViewArr[index];

            ListView.SelectedIndexCollection sc = listView.SelectedIndices;

            BV bv = (BV)listView.Items[sc[0]].Tag;
            if (LayoutManager.SystemMode == GlobalTypes.TSystemModes.Online)
            {
                if (sc.Count == 0)
                    return;

                bv.BVProbe = newProbe;
                //RIMD-399: Parameter values on screen not change for 4 Mhz CW probe (when using Remote or not)
                //if(newProbe.IsCW)
                //	listView.Items[sc[0]].SubItems[1].Text = String.Empty;
                //else
                SetItemDepth((BV)listView.Items[sc[0]].Tag, listView.Items[sc[0]]);
            }
            else
            {
                // check if the selected BV was examinated.
                if (sc.Count != 0)
                {
                    if (bv.BVStatus == BV.TBVStatus.eNotExaminatedYet)
                    {
                        bv.BVProbe = newProbe;
                        //RIMD-399: Parameter values on screen not change for 4 Mhz CW probe (when using Remote or not)
                        //if(newProbe.IsCW)
                        //	listView.Items[sc[0]].SubItems[1].Text = String.Empty;
                        //else
                        SetItemDepth(bv, listView.Items[sc[0]]);
                    }
                    else
                    {
                        m_tmpProbe = newProbe;
                        m_probeChanged = true;
                    }
                }
            }
        }

        public DataView GetGateExam(String filter)
        {
            DataView dv = new DataView(RimedDal.Instance.s_dsGateExamination.tb_GateExamination, filter, null, System.Data.DataViewRowState.CurrentRows);
            return dv;
        }

        /// <summary>save the DATA STRUCTURE to the DATA BASE. called when the user press on the save button of the summary screen.</summary>
        public bool Save(bool directSaving)
        {
            ExceptionPublisherLog4Net.TraceLog(string.Format("Save(directSaving={0})", directSaving), Name);

            var strRes = ResourceManagerWrapper.GetWrapper(MainForm.ResourceManager); //Ofer

            if (!directSaving)
            {
                //to discuss (monitoring: this.ExaminationWasSaved was always true)
                if ((!ExaminationWasSaved && RimedDal.Instance.s_dsGateExamination.tb_GateExamination.Rows.Count != 0 && LayoutManager.SystemLayoutMode != GlobalTypes.TSystemLayoutMode.DiagnosticLoad && LayoutManager.SystemLayoutMode != GlobalTypes.TSystemLayoutMode.Monitoring) 
                    || m_addGates2SavedExam || !EventListViewCtrl.Instance.EventListSaved)
                {
                    var res = MessageBox.Show(strRes.GetString("You have unsaved tests. Do you want to save them?"), strRes.GetString("Warning"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (res == DialogResult.No)
                    {
                        if (!EventListViewCtrl.Instance.EventListSaved)
                            EventListViewCtrl.Instance.DeleteCurEventFiles();

                        if (RimedDal.Instance.IsTemporaryLoadedExamination)
                            RimedDal.Instance.DeleteCurrentExamination();

                        return false; // examination was not saved.
                    }
                }
                else
                {
                    return false; // examination was not saved.
                }
            }

            EventListViewCtrl.Instance.SaveEventHistory();
            if (LayoutManager.SystemLayoutMode == GlobalTypes.TSystemLayoutMode.MonitoringLoad)
                return true;

            var examRow = RimedDal.Instance.s_dsExamination.tb_Examination.Rows[0] as dsExamination.tb_ExaminationRow;
            var patientIndex = examRow.Patient_Index;
            // check if the current user is undefined user.
            if (patientIndex == Guid.Empty && !m_addGates2SavedExam)
            {
                // open patient list.
                using (var dlg = new PatientListDlg())
                {
                    var res = dlg.ShowDialog();

                    if (res == DialogResult.OK)
                    {
                        patientIndex = dlg.SelPatientIndex;
                        ExaminationWasSaved = true; // this flag is set to true in order that the next time this function will be called, it will return amidiatly.
                        if (!(LayoutManager.SystemLayoutMode == GlobalTypes.TSystemLayoutMode.Monitoring && !MainForm.Instance.IsMonitoringSaved))
                            MainForm.Instance.SelectedPatientIndex = patientIndex;
                    }
                    else if (res == DialogResult.Cancel)
                    {
                        if (!directSaving)
                            RimedDal.Instance.DeleteCurrentExamination();

                        MessageBox.Show(strRes.GetString("Examination was not saved."), strRes.GetString("Notification"), MessageBoxButtons.OK, MessageBoxIcon.Information);

                        return false;
                    }
                }
            }

            if (LayoutManager.SystemLayoutMode == GlobalTypes.TSystemLayoutMode.Monitoring && !MainForm.Instance.IsMonitoringSaved)
            {
                Guid g = Guid.NewGuid();
                SaveLayout(g, LayoutManager.theLayoutManager.GateViews, LayoutManager.theLayoutManager.Autoscans[0].Display, LayoutManager.theLayoutManager.Autoscans[1].Display);
                MainForm.Instance.IsMonitoringSaved = true;
            }

            if (LayoutManager.SystemLayoutMode == GlobalTypes.TSystemLayoutMode.Diagnostic || LayoutManager.SystemLayoutMode == GlobalTypes.TSystemLayoutMode.DiagnosticLoad)
                SummaryScreen.Instance.SaveImage2HD();

            examRow.Patient_Index = patientIndex;
            examRow.Date = DateTime.Now;
            examRow.Study_Index = MainForm.Instance.CurrentStudyId;

            if (LayoutManager.SystemLayoutMode == GlobalTypes.TSystemLayoutMode.Diagnostic)
                LayoutManager.SystemLayoutMode = GlobalTypes.TSystemLayoutMode.DiagnosticLoad;

            //RimedDal.Instance.SaveGates(); - rimd-180 fail
            RimedDal.Instance.UpdateCurExam2DB();
            ExaminationWasSaved = true;
            RecordingExaminationId = Guid.Empty;
            LayoutManager.theLayoutManager.WorkingMode = GlobalTypes.TWorkingMode.eLoadMode;

            // Notification to the user.
            var filter = "Select * FROM tb_Patient WHERE (Patient_Index = '" + patientIndex.ToString() + "')";
            var ds = RimedDal.Instance.GetPatients(filter);
            MessageBox.Show(strRes.GetString("Examination was saved under patient: ") + ((ds.tb_Patient[0].First_Name == "Undefined") ? "" : ds.tb_Patient[0].Last_Name) + " " + ds.tb_Patient[0].First_Name + " ID: " + ds.tb_Patient[0].ID, strRes.GetString("Notification"), MessageBoxButtons.OK, MessageBoxIcon.Information);

            HardDriveSpaceKeeper.Instance.DeleteUnusedExamData();

            return true;
        }
        
        //Assaf : The saving mechanism of monitoring exam is a little bit different 
        public bool SaveMonitoringExam()
        {
            Guid patientIndex = MainForm.Instance.SelectedPatientIndex;
            Guid g = Guid.NewGuid();
            g = Guid.NewGuid();
            SaveLayout(g, LayoutManager.theLayoutManager.GateViews, LayoutManager.theLayoutManager.Autoscans[0].Display, LayoutManager.theLayoutManager.Autoscans[1].Display);

            MainForm.Instance.IsMonitoringSaved = true;
            int i = RimedDal.Instance.s_dsExamination.tb_Examination.Rows.Count;

            RimedDal.Instance.s_dsExamination.tb_Examination.Rows[0]["Patient_Index"] = patientIndex;
            RimedDal.Instance.s_dsExamination.tb_Examination.Rows[0]["Date"] = DateTime.Now;
            RimedDal.Instance.s_dsExamination.tb_Examination.Rows[0]["Study_Index"] = MainForm.Instance.CurrentStudyId;

            if (LayoutManager.SystemLayoutMode == GlobalTypes.TSystemLayoutMode.Monitoring && !MainForm.Instance.IsMonitoringSaved)
                MainForm.Instance.IsMonitoringSaved = true;
            
            ExaminationWasSaved = true;
            
            return true;
        }

        //public void SetBVSelect(Guid bvExaminationIndex)
        //{
        //    ListView.ListViewItemCollection items = this.listView1.Items;

        //    for (int i = 0; i < 2; ++i)
        //    {
        //        foreach (ListViewItem item in items)
        //        {
        //            if (((BV)item.Tag).BVExaminationIndex == bvExaminationIndex)
        //            {
        //                item.Selected = true;
        //                item.Focused = true;
        //                return;
        //            }
        //        }

        //        if (this.ListType == EListType.DualList)
        //            items = this.listView2.Items;
        //        else
        //            break;
        //    }
        //}
        
        public Guid GetSubExaminationID(BV bv)
        {
            String filter = "BVExaminationIndex = '" + bv.BVExaminationIndex.ToString() + "'";
            DataRow[] gateExamRows = RimedDal.Instance.s_dsGateExamination.tb_GateExamination.Select(filter);

            return (Guid)gateExamRows[0]["SubExaminationIndex"];
        }

        //Ofer
        public BV SelectedBV
        {
            get { return getSelectedBV(listView1); }
        }

        private ListViewItem getSelectedItem(ListView lstView)
        {
            if (lstView == null)
                return null;

            return (lstView.SelectedItems.Count == 0) ? null : lstView.SelectedItems[0];
        }

        //Ofer
        private BV getSelectedBV(ListView lstView)
        {
            var item = getSelectedItem(lstView);
            if (item == null)
                return null;

            return item.Tag as BV;;
        }

        /// <summary>
        /// This function is called when the user load new study.
        /// </summary>
        private void ClearBVList()
        {
            m_bvCollection.Clear();
        }

        private bool IsExamineBVExist()
        {
            foreach (BV bv in m_bvCollection)
            {
                if (bv.BVStatus == BV.TBVStatus.eExaminated)
                    return true;
            }

            return false;
        }
        
        public void GVNotes(int gvId, String notes)
        {
            ListView.SelectedIndexCollection sc;
            ListView.ListViewItemCollection items;

            if (gvId % 2 == 0)
            {
                sc = this.listView1.SelectedIndices;
                items = this.listView1.Items;
            }
            else
            {
                sc = this.listView2.SelectedIndices;
                items = this.listView2.Items;
            }

            if (sc.Count == 0)
                return;

            BV bv = (BV)items[sc[0]].Tag;

            if (LayoutManager.SystemLayoutMode == GlobalTypes.TSystemLayoutMode.Monitoring)
            {
                if (!this.ExaminationWasSaved)
                    return;
            }
            else
                if (bv.BVStatus == BV.TBVStatus.eNotExaminatedYet)
                    return;

            String filter = "ID = '" + gvId.ToString() + "' AND BVExaminationIndex = '" + bv.BVExaminationIndex.ToString() + "'";
            DataRow[] rows = RimedDal.Instance.s_dsGateExamination.tb_GateExamination.Select(filter);
            if (rows.Length != 0)
            {
                rows[0]["Notes"] = notes;
                //RIMD-549: Notes are not saved for Monitoring examination
                if ((LayoutManager.SystemLayoutMode == GlobalTypes.TSystemLayoutMode.Monitoring || LayoutManager.SystemLayoutMode == GlobalTypes.TSystemLayoutMode.MonitoringLoad) && this.ExaminationWasSaved)
                    RimedDal.Instance.UpdateGateExamination();
            }
        }
        
        public void AddGv2SummaryScreen(int gvId)
        {
            ListView.SelectedIndexCollection sc;
            ListView.ListViewItemCollection items;

            if (gvId % 2 == 0)
            {
                sc = this.listView1.SelectedIndices;
                items = this.listView1.Items;
            }
            else
            {
                sc = this.listView2.SelectedIndices;
                items = this.listView2.Items;
            }

            if (sc.Count == 0)
                return;

            BV bv = (BV)items[sc[0]].Tag;
            if (bv.BVStatus == BV.TBVStatus.eNotExaminatedYet)
                return;

            String filter = "BVExaminationIndex = '" + bv.BVExaminationIndex + "' AND ID = " + gvId.ToString() + " AND InSummaryScreen=false";
            DataRow[] rows = RimedDal.Instance.s_dsGateExamination.tb_GateExamination.Select(filter, null, DataViewRowState.CurrentRows);
            if (rows.Length == 1)
                rows[0]["InSummaryScreen"] = true;
        }

        internal bool LoadStudy(Guid studyIndex, Guid patientIndex) //Ofer 
        {
            ExceptionPublisherLog4Net.TraceLog(string.Format("LoadStudy(studyIndex={0}, patientIndex={1})", studyIndex, patientIndex), Name);

            m_selectedBVId = Guid.NewGuid();
            return loadExamination(studyIndex, patientIndex, true, false);
        }

        private bool loadExamination(Guid examIndex, Guid patientIndex, bool createNew, bool showSummaryScreen) //Ofer  
        {
            ExceptionPublisherLog4Net.TraceLog(string.Format("loadExamination(examIndex={0}, patientIndex={1}, showSummaryScreen={2})", examIndex, patientIndex, showSummaryScreen), Name);

            try
            {
                //In case that a monitoring replay exam was terminated before it's end
                if (LayoutManager.theLayoutManager.IsPaused)
                {
                    //RIMD-465: Monitoring study load works incorrect (Pause problem)
                    Dsps.Instance[0].IsOddMonitoringIndexfileNumber = false;
                    LayoutManager.theLayoutManager.IsPaused = false;
                    LayoutManager.theLayoutManager.CancelWait12Thread = false;
                    MainForm.Instance.toolBarPanel1.ButtonPause.IsPressed = false;
                }

                Save(false);

                dsExamination.tb_ExaminationRow examRow;
                if (createNew)
                {
                    RimedDal.Instance.ClearCurExam();
                    examRow = CreateNewExam(examIndex, patientIndex);
                    examIndex = examRow.Examination_Index;
                    RecordingExaminationId = examIndex;
                    m_currentSummaryScreenIndex = 0;
                }
                else
                {
                    examRow = RimedDal.Instance.LoadCurExam(examIndex);
                    if (examRow == null)
                    {
                        MessageBox.Show("Examination is already removed");
                        createNew = true;
                        RimedDal.Instance.ClearCurExam();
                        examRow = CreateNewExam(RimedDal.Instance.s_DS.tb_BasicConfiguration[0].LastStudy, patientIndex);
                        examIndex = examRow.Examination_Index;
                        RecordingExaminationId = examIndex;
                        m_currentSummaryScreenIndex = 0;
                    }
                    else
                    {
                        patientIndex = examRow.Patient_Index;
                        RecordingExaminationId = Guid.Empty;
                        m_currentSummaryScreenIndex = -1;
                        foreach (var row in RimedDal.Instance.s_dsGateExamination.tb_GateExamination.Rows)
                        {
                            if (((dsGateExamination.tb_GateExaminationRow)row).OnSummaryScreenIndex > m_currentSummaryScreenIndex)
                                m_currentSummaryScreenIndex = ((dsGateExamination.tb_GateExaminationRow)row).OnSummaryScreenIndex;
                        }
                        m_currentSummaryScreenIndex++;
                    }
                }

                var studyRow = RimedDal.Instance.GetStudy(examRow.Study_Index);

                LayoutManager.theLayoutManager.LoadExamination(createNew, examRow.IsTemporary, studyRow.Monitoring);
                
                MainForm.Instance.LoadStudy(studyRow, patientIndex, examRow.IsTemporary);

                ClearDataStructure();
                VerifyIfItIsCleardataStructure();
                Init(LayoutManager.theLayoutManager.StudyType);

                // fill the BV List view Ctrl.
                LoadBVList(!examRow.IsTemporary);

                foreach (AutoScanChart autoscan in LayoutManager.theLayoutManager.Autoscans)
                {
                    autoscan.SetItemCloseVisibility(!studyRow.Monitoring);
                    autoscan.Graph.OverridenAutoScanImage = null;
                }

                EventListViewCtrl.Instance.LoadExamination(examRow);
                SummaryScreen.Instance.LoadExamination(studyRow.Monitoring, examRow.IsTemporary, showSummaryScreen);
            }
            catch (Exception ex)
            {
                ExceptionPublisherLog4Net.ExceptionErrorLog(ex, "Study loading   Failed."); //Ofer
                MessageBox.Show(MainForm.ResourceManager.GetString("Study loading Failed."), MainForm.ResourceManager.GetString("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);  //Ofer

                return false;   //Ofer 
            }

            return true;        //Ofer
        }

        // Ofer: BugFix #0092
        // Unhandled exception: InvalidArgument = Value of '-1' is not valid for 'index'. Parameter name: index (ErrorCode: 0x80131502)
        // Fix: Added object (bv) null referance and listView1.Items[] range check 
        public bool IsCurrentBVHasOnlySummaryImages()   
        {
            var isCurrentBVHasOnlySummaryImages = false;

            if (listView1 == null || listView1.Items.Count == 0)    //Ofer
                return isCurrentBVHasOnlySummaryImages;

            var sc = listView1.SelectedIndices;
            if (sc.Count != 0)
            {
                var index = sc[0];
                if (index >= 0 && index < listView1.Items.Count)    //Ofer
                {
                    var bv = listView1.Items[index].Tag as BV;      //Ofer
                    if (bv != null)                                 //Ofer
                        isCurrentBVHasOnlySummaryImages = bv.SaveOnlySummaryImages;
                }
            }

            return isCurrentBVHasOnlySummaryImages;
        }

        public bool IsContained(Guid bvIndex)
        {
            foreach (BV bv in m_bvCollection)
            {
                if (bv.BVIndex == bvIndex)
                    return true;
            }
            return false;
        }

        public void LoadBVList(bool displayOnlyExamineBV)
        {
            this.ClearBVList();

            List<Guid> examIndexes = new List<Guid>();
            foreach (dsBVExamination.tb_BVExaminationRow examBV in RimedDal.Instance.s_dsBVExamination.tb_BVExamination.Rows)
            {
                BV bv = new BV(examBV);
                this.m_bvCollection.Add(bv);
                if (!examIndexes.Contains(bv.BVIndex))
                    examIndexes.Add(bv.BVIndex);
            }

            if (!displayOnlyExamineBV)
            {
                DataView dv = RimedDal.Instance.GetBVByStudyIndex(MainForm.Instance.CurrentStudyId);
                foreach (DataRowView row in dv)
                {
                    Guid bvIndex = (Guid)row["Blood_Vessel_Index"];
                    if (!examIndexes.Contains(bvIndex))
                    {
                        DB.dsBVSetup.tb_BloodVesselSetupRow rowBV = RimedDal.Instance.GetBVProperties((Guid)row["Blood_Vessel_Index"]);
                        BV bv = new BV(rowBV);
                        this.AddBV(bv);
                    }
                }

                // the parameter of the following function was set to newGuid, so, the selected BV will be '0'.
                TemporarySolutionToTheTracktorBug();
            }
            UpdateBVList(m_selectedBVId, displayOnlyExamineBV);
        }

        private void TemporarySolutionToTheTracktorBug()
        {
            //Assaf (31/12/12) - this function was made as a stupid, and hopefully temporary solution to the
            //Famous "Tractor" bug (No signal after renaming BV and moving from Unilateral to Bilateral or via versa)
            MainForm.Instance.PowerDown();
            MainForm.Instance.PowerUp();
            GateView gv = LayoutManager.theLayoutManager.SelectedGv;
            gv.DopplerBar.DopplerVarLabelPower.Active = false;
        }

        #region Event Handlers

        private void listView1_MouseDown(object sender, MouseEventArgs e)
        {
            if (HardDriveSpaceKeeper.Instance.IsFakeDataRowReady)
                return;

            if (e.Button == MouseButtons.Left && e.Clicks == 1)
            {
                Point pt = new Point(e.X, e.Y);

                for (int i = 0; i < this.listView1.Items.Count; i++)
                {
                    Rectangle rect = this.listView1.GetItemRect(i);

                    rect = new Rectangle(rect.Location, this.imageList1.ImageSize);

                    if (rect.Contains(pt))
                    {
                        HardDriveSpaceKeeper.Instance.ToggleSavingRowDataMode(((BV)this.listView1.Items[i].Tag).SubExamIndex);
                        break;
                    }
                }
            }
        }

        private void HardDriveSpaceKeeper_RowDataModeChanged(Guid subExaminationIndex, bool saveOnlySummaryImages)
        {
            foreach (ListViewItem item in this.listView1.Items)
            {
                if (((BV)item.Tag).SubExamIndex == subExaminationIndex)
                {
                    item.Checked = !saveOnlySummaryImages;
                    ((BV)item.Tag).SaveOnlySummaryImages = saveOnlySummaryImages;
                    break;
                }
            }

            foreach (ListViewItem item in this.listView2.Items)
            {
                if (((BV)item.Tag).SubExamIndex == subExaminationIndex)
                {
                    item.Checked = !saveOnlySummaryImages;
                    ((BV)item.Tag).SaveOnlySummaryImages = saveOnlySummaryImages;
                    break;
                }
            }
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!m_enabledChangeSelection)
                return;

            if (ListType == EListType.DualList)
            {
                listView1_BilateralSelectedIndexChanged(sender, e);
                return;
            }

            //Solve problem of Exception where selected side is 1 when returning from Bilateral test
            GateView.SideSelected = 0;
            var sc = listView1.SelectedIndices;

            if (sc.Count == 0) 
                return;

            var bv = listView1.Items[sc[0]].Tag as BV;  //Ofer
            if (bv == null)
                return;

            ExceptionPublisherLog4Net.SelectBVLog(bv.FullName);

            //				RIMD-399: Parameter values on screen not change for 4 Mhz CW probe (when using Remote or not)
            double depth = 0;
            Double.TryParse(listView1.Items[sc[0]].SubItems[2].Text, out depth);
            bv.DefaultDepth = depth;

            if (bv.BVStatus == BV.TBVStatus.eExaminated)
            {
                String filter = "SubExaminationIndex = '" + bv.SubExamIndex + "'";
                System.Data.DataRow[] rows = RimedDal.Instance.s_dsSubExamination.tb_SubExamination.Select(filter);
                if (rows.Length != 0)
                    this.SubExamNotes = (String)rows[0]["Notes"];
            }
            else
                this.SubExamNotes = String.Empty;

            BV[] bvArr = new BV[1];
            bvArr[0] = bv;
            this.FireBVChangedEvent(bvArr, sender);
            // This call is not necessary now but I didn't delete it
            if (MainForm.Instance.CurrentStudyName == "Intraoperative")
            {
                MainForm.Instance.ToolBarPanel1.BeginInvoke((Action)delegate()
                    {
                        MainForm.Instance.ToolBarPanel1.ComboBoxProbes.SelectedIndex = MainForm.Instance.ToolBarPanel1.ProbeLocation16Mhz;
                        MainForm.Instance.ToolBarPanel1.ComboBoxProbes.Enabled = false;
                    });
            }
            
            //Was added by Natalia for RIMD-89
            LayoutManager.theLayoutManager.CursorsMode = false;
        }

        private void listView1_BilateralSelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (!m_enabledChangeSelection)
                return;

            var selectedIndices1 = listView1.SelectedIndices;
            var selectedIndices2 = listView2.SelectedIndices;
            if (selectedIndices1.Count == 0 || selectedIndices2.Count == 0)
                return;

            if (LayoutManager.theLayoutManager.StudyType == GlobalTypes.TStudyType.eBilateral && selectedIndices1[0] != selectedIndices2[0])
                listView2.Items[selectedIndices1[0]].Selected = true;

            var bvArr = new BV[GlobalSettings.AProbesNum];
            for (int i = 0; i < GlobalSettings.AProbesNum; ++i)
            {
                var listView = (ListView)ListViewArr[i];
                var sc = listView.SelectedIndices;
                bvArr[i] = (BV)listView.Items[sc[0]].Tag;

                ExceptionPublisherLog4Net.SelectBVLog(bvArr[i].FullName);

                //RIMD-399: Parameter values on screen not change for 4 Mhz CW probe (when using Remote or not)
                double depth = 0;
                Double.TryParse(listView.Items[sc[0]].SubItems[2].Text, out depth);
                bvArr[i].DefaultDepth = depth;

                if (bvArr[0].BVStatus == BV.TBVStatus.eExaminated)
                {
                    var filter = "SubExaminationIndex = '" + bvArr[i].SubExamIndex + "'";
                    var rows = RimedDal.Instance.s_dsSubExamination.tb_SubExamination.Select(filter);
                    if (rows.Length != 0)
                        SubExamNotes = (string)rows[0]["Notes"];
                }
                else
                    SubExamNotes = string.Empty;
            }

            FireBVChangedEvent(bvArr, sender);

            //Was added by Natalia for RIMD-89
            LayoutManager.theLayoutManager.CursorsMode = false;
        }

        private void menuRename_Click(object sender, System.EventArgs e)
        {
            ExceptionPublisherLog4Net.ContextMenuClickLog("menuRename", this.Name);
            if (LayoutManager.theLayoutManager.WorkingMode == GlobalTypes.TWorkingMode.eLoadMode)
            {
                MessageBox.Show(MainForm.ResourceManager.GetString("Cannot rename BV for a loaded examination"));
                return;
            }

            RenameData renameData = null;

            try
            {
                Rename();
            }
            catch (Exception ex)
            {
                ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
                MessageBox.Show(MainForm.ResourceManager.GetString("This BV name can not be used."));
                return;
            }
            if (renameData == null)
                return;

            if (!renameData.renamed)
                return;

            foreach (GateView g in LayoutManager.theLayoutManager.GateViews)
            {
                //TODO : Multifrequency
                if (this.ListType == EListType.SingleList)
                {
                    g.BVName = renameData.name;
                }
                else
                {
                    if (g.GateSide == GlobalTypes.TGateSide.eLeft)
                    {
                        g.BVName = renameData.name;
                    }
                    else
                    {
                        g.BVName = renameData.name2;
                    }
                }
            }

            if (LayoutManager.theLayoutManager.CurrentGateView != null)
            {
                LayoutManager.theLayoutManager.CurrentGateView.BVName = renameData.name;
            }
        }

        private void menuItemReturn_Click(object sender, System.EventArgs e)
        {
            ExceptionPublisherLog4Net.ContextMenuClickLog("menuItemReturn", this.Name);
            if (LayoutManager.theLayoutManager.WorkingMode == GlobalTypes.TWorkingMode.eLoadMode)
            {
                MessageBox.Show(MainForm.ResourceManager.GetString("Cannot perform new test for loaded examination!"));
                return;
            }

            // this will rename the BV in the bv list and in all the gates that refers to that BV.
            ListView.SelectedIndexCollection sc = this.listView1.SelectedIndices;

            if (sc.Count != 0)
            {
                int index = sc[0];

                BV bv = (BV)this.listView1.Items[index].Tag;

                if (bv.BVStatus != BV.TBVStatus.eExaminated)
                    return;

                int position = index;

                this.DuplicateBV();

                if ((index + 1) < this.listView1.Items.Count)
                    ++position;

                if (this.ListType == EListType.DualList)
                {
                    this.listView2.Items[position - 1].ForeColor = EXAMINED_BV_COLOR;
                    this.listView2.Items[position].Selected = true;
                    this.listView2.Items[position].Focused = true;
                }

                this.listView1.Items[position - 1].ForeColor = EXAMINED_BV_COLOR;
                this.listView1.Items[position].Selected = true;
                this.listView1.Items[position].Focused = true;
                foreach (var gateView in LayoutManager.theLayoutManager.GateViews)
                    gateView.Probe.AddGv(gateView.GateSide == GlobalTypes.TGateSide.eRight ? 1 : 0, gateView);
            }
        }

        private void menuItemReplayBV_Click(object sender, System.EventArgs e)
        {
            ExceptionPublisherLog4Net.ClickLog("menuItemReplayBV", this.Name);
            // this will rename the BV in the bv list and in all the gates that refers to that BV.
            ListView.SelectedIndexCollection sc = this.listView1.SelectedIndices;

            if (sc.Count != 0)
            {
                if (this.m_bvCollection[sc[0]].BVStatus == BV.TBVStatus.eExaminated)
                {
                    if (LayoutManager.theLayoutManager.ReplayMode)
                    {
                        // move to regular mode [unReplay].
                        this.menuItemReplayBV.Checked = false;
                        this.UpdateBVList(Guid.Empty, false);
                    }
                    else
                    {
                        // move to Replay mode.
                        this.menuItemReplayBV.Checked = true;
                        this.UpdateBVList(Guid.Empty, ExaminationWasSaved);
                    }
                }
            }
        }

        private void contextMenu1_Popup(object sender, System.EventArgs e)
        {
            ListView.SelectedIndexCollection sc = this.listView1.SelectedIndices;
            // enable/disable the right click items menu.
            if (this.IsExamineBVExist())
                this.menuItemReplayBV.Enabled = true;
            else
                this.menuItemReplayBV.Enabled = false;

        }

        private void LayoutManager_LoadExamEvent(Guid examIndex)
        {
            m_selectedBVId = m_bvCollection.Count > 0 ? m_bvCollection[0].BVExaminationIndex : Guid.Empty;
            loadExamination(examIndex, Guid.Empty, false, true);

            if (LayoutManager.SystemLayoutMode != GlobalTypes.TSystemLayoutMode.MonitoringLoad)
                LayoutManager.theLayoutManager.Trends.Clear();

            if (LayoutManager.SystemLayoutMode == GlobalTypes.TSystemLayoutMode.Monitoring || LayoutManager.SystemLayoutMode == GlobalTypes.TSystemLayoutMode.MonitoringLoad)
            {
                LayoutManager.theLayoutManager.OnNewTrend(false);
                //Assaf (18/11/07) - Loading the trends pics into the trends objects zzzz
                foreach (var trend in LayoutManager.theLayoutManager.Trends)
                {
                    string fileName = Path.Combine(GlobalSettings.TrendsDataDir, examIndex + "_" + trend.Id + ".bmp");
                    if (File.Exists(fileName + ".tmp"))
                    {
                        ExceptionPublisherLog4Net.TraceLog(string.Format("LayoutManager_LoadExamEvent(...): File.Delete('{0}')   <--- FILE.DELETE", fileName), Name);
                        File.Delete(fileName);

                        ExceptionPublisherLog4Net.TraceLog(string.Format("LayoutManager_LoadExamEvent(...): File.Move('{0}', '{1}')   <--- FILE.MOVE", fileName + ".tmp", fileName), Name);
                        File.Move(fileName + ".tmp", fileName);
                    }
                    if (File.Exists(fileName))
                        trend.GetTrendGraph().BitmapOfFullResolution = (Bitmap)Image.FromFile(fileName);
                    else if (trend.GetTrendGraph().BitmapOfFullResolution == null)
                        MessageBox.Show(MainForm.ResourceManager.GetString("This is an old monitoring exam. Trends would not be supported."));
                }
                if (MainForm.Instance.CurrentStudyName.StartsWith("Evoked Flow"))
                {
                    var lastStopStimulationEvent = EventListViewCtrl.Instance.LastStopStimulationEvent;
                    if (lastStopStimulationEvent != null)
                    {
                        //NULL check was added for empty event list (RIMD-243)
                        if (LayoutManager.theLayoutManager.Trends.Count > 0)
                            LayoutManager.theLayoutManager.Trends[0].EvokedFlowPeak = lastStopStimulationEvent.FlowChangePrimary;
                        if (LayoutManager.theLayoutManager.Trends.Count > 1)
                            LayoutManager.theLayoutManager.Trends[1].EvokedFlowPeak = lastStopStimulationEvent.FlowChangeSecondary;
                    }
                }
                // Commented by Natalie at 10/21/2009 for fixing RIMD-165: Replay of VMR starts from the end
                //				if(MainForm.Instance.CurrentStudyName.StartsWith("VMR") )
                //					MainForm.Instance.MoveToLastVMRHistoryEvent();
            }
        }

        private void LayoutManager_LoadBVExamEvent(Guid examIndex, Guid bvIndex)
        {
            m_selectedBVId = bvIndex;
            loadExamination(examIndex, Guid.Empty, false, false);
        }
        
        private void LayoutManager_LoadGateExamEvent(Guid examIndex, GateIndexTagData gitd)
        {
            m_selectedBVId = gitd.BVExamIndex;
            loadExamination(gitd.ExamIndex, Guid.Empty, false, false);

            if (LayoutManager.theLayoutManager.StudyType == GlobalTypes.TStudyType.eUnilateral)
                LayoutManager.theLayoutManager.ReplayGate(gitd.GateIndex);
            else
                LayoutManager.theLayoutManager.BilateralReplayGate(gitd.GateIndex);
        }

        private void listView2_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (LayoutManager.theLayoutManager.StudyType != GlobalTypes.TStudyType.eMultifrequency)
                return;

            listView1_BilateralSelectedIndexChanged(sender, e);
        }

        private void BVListViewCtrl_SizeChanged(object sender, System.EventArgs e)
        {
            if (MainForm.Instance == null)
                return;

            if (this.Width < 50)
                this.Width = 50;

            MainForm.Instance.ResizePanelsSize();

            int hight = this.listView1.Parent.Height;
            if (this.ListType == EListType.DualList)
            {
                this.listView1.Height = hight / 2;
                this.listView2.Height = hight / 2;
            }
            else
            {
                this.listView1.Height = hight;
            }
            int widthCtrl = this.listView1.Width;
            this.listView1.Columns[0].Width = (int)(widthCtrl / 3) * 2;
            this.listView1.Columns[1].Width = widthCtrl - this.listView1.Columns[0].Width - 5;
            this.listView2.Columns[0].Width = (int)(widthCtrl / 3) * 2;
            this.listView2.Columns[1].Width = widthCtrl - this.listView2.Columns[0].Width - 5;
        }

        //Ofer
        //private void BVListViewCtrl_Click(object sender, System.EventArgs e)
        //{
        //    this.CheckVisible();
        //}

        //Ofer
        //private void listView1_AfterLabelEdit(object sender, System.Windows.Forms.LabelEditEventArgs e)
        //{
        //    int index = -1;
        //    for (int i = 0; i < listView1.Items.Count; ++i)
        //    {
        //        if (listView2.Items[index].Selected)
        //        {
        //            index = i;
        //            break;
        //        }
        //    }
        //}

        #endregion Event Handlers

        private Guid m_selectedBVId;
    }

    public delegate void BVChangedDelegate(BV[] bvArr, object sender);
}
