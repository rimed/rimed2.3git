﻿namespace TCD2003.GUI
{
    partial class HitsDebug
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.listBoxIndex = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.listBoxHistory = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblHistory = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // listBoxIndex
            // 
            this.listBoxIndex.Location = new System.Drawing.Point(16, 40);
            this.listBoxIndex.MultiColumn = true;
            this.listBoxIndex.Name = "listBoxIndex";
            this.listBoxIndex.Size = new System.Drawing.Size(248, 95);
            this.listBoxIndex.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label1.Location = new System.Drawing.Point(16, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(260, 24);
            this.label1.TabIndex = 5;
            this.label1.Text = "Index  Type  Energy  Speed  Duration";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // listBoxHistory
            // 
            this.listBoxHistory.Location = new System.Drawing.Point(16, 240);
            this.listBoxHistory.MultiColumn = true;
            this.listBoxHistory.Name = "listBoxHistory";
            this.listBoxHistory.Size = new System.Drawing.Size(240, 95);
            this.listBoxHistory.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label2.Location = new System.Drawing.Point(16, 216);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(248, 24);
            this.label2.TabIndex = 7;
            this.label2.Text = "Index  Type  Energy  Speed  Duration";
            // 
            // lblHistory
            // 
            this.lblHistory.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lblHistory.Location = new System.Drawing.Point(88, 192);
            this.lblHistory.Name = "lblHistory";
            this.lblHistory.Size = new System.Drawing.Size(64, 16);
            this.lblHistory.TabIndex = 8;
            this.lblHistory.Text = "History";
            // 
            // HitsDebug
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(288, 366);
            this.Controls.Add(this.lblHistory);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.listBoxHistory);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listBoxIndex);
            this.Name = "HitsDebug";
            this.Load += new System.EventHandler(this.HitsDebug_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxIndex;
        private System.Windows.Forms.Timer timer1;
        private System.ComponentModel.IContainer components;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblHistory;
        private System.Windows.Forms.ListBox listBoxHistory;
    }
}