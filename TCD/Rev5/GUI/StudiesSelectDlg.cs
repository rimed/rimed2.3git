using System;
using System.Drawing;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

using Syncfusion.Windows.Forms.Tools.XPMenus;

using TCD2003.Utils;

namespace TCD2003.GUI
{
    /// <summary>
    /// Summary description for StudiesSelectDlg.
    /// </summary>
    public partial class StudiesSelectDlg : Form
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public StudiesSelectDlg()
        {
            this.InitializeComponent();
            this.BackColor = GlobalSettings.BackgroundDlg;

            var strRes = ResourceManagerWrapper.GetWrapper(MainForm.ResourceManager); 

            this.buttonOK.Text = strRes.GetString("OK");
            this.buttonCancel.Text = strRes.GetString("Close");
            this.buttonDelete.Text = strRes.GetString("Delete");
            this.buttonHelp.Text = strRes.GetString("Help");
            this.Text = strRes.GetString("Studies");

            this.SelectedNodeTag = Guid.Empty;
        }

        public Guid SelectedNodeTag { get; private set; }

        public void InitData(BarItems items)
        {
            this.treeViewStudies.Nodes.Clear();
            foreach (ParentBarItem ib in items)
            {
                TreeNode n = new TreeNode(ib.Text);
                n.Tag = ib.Tag;
                this.treeViewStudies.Nodes.Add(n);
                foreach (BarItem bi in ib.Items)
                {
                    TreeNode nn = new TreeNode(bi.Text);
                    nn.Tag = bi.Tag;
                    n.Nodes.Add(nn);
                }
            }
        }

        private void OnOK(bool showWarning)
        {
            // Verify that the selected node is a study (a leaf in the tree)
            if ((this.treeViewStudies.SelectedNode != null) &&
                (this.treeViewStudies.SelectedNode.Nodes.Count == 0))
            {
                List<LogField> fields = new List<LogField>();
                fields.Add(new LogField("treeViewStudies", this.treeViewStudies.SelectedNode.Text));
                ExceptionPublisherLog4Net.FormActionLog("buttonOK", this.Name, fields);
                this.SelectedNodeTag = (Guid)treeViewStudies.SelectedNode.Tag;
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else if (showWarning)
            {
                MessageBox.Show(MainForm.ResourceManager.GetString("Please select a study."),
                    MainForm.ResourceManager.GetString("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void buttonOK_Click(object sender, System.EventArgs e)
        {
            this.OnOK(true);
        }

        private void treeViewStudies_DoubleClick(object sender, System.EventArgs e)
        {
            this.OnOK(false);
        }

        private void buttonDelete_Click(object sender, System.EventArgs e)
        {
            this.SelectedNodeTag = (Guid)this.treeViewStudies.SelectedNode.Tag;
            if (this.treeViewStudies.SelectedNode != null
                && this.treeViewStudies.SelectedNode.Nodes.Count == 0
                && this.treeViewStudies.SelectedNode.Text != "Default")
            {
                var strRes = ResourceManagerWrapper.GetWrapper(MainForm.ResourceManager); 

                if (MessageBox.Show(strRes.GetString("Are you sure you want to delete the selected study?"),
                    strRes.GetString("Warning"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == System.Windows.Forms.DialogResult.Yes)
                {
                    List<LogField> fields = new List<LogField>();
                    fields.Add(new LogField("treeViewStudies", treeViewStudies.SelectedNode.Text));
                    ExceptionPublisherLog4Net.FormActionLog("buttonDelete", this.Name, fields);

                    if (this.SelectedNodeTag == MainForm.Instance.CurrentStudyId)
                    {
                        MessageBox.Show(strRes.GetString("In order to delete this study first change the current study"),
                            strRes.GetString("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else if (DB.RimedDal.Instance.DeleteStudy(this.SelectedNodeTag))
                    {
                        // delete the selected study from the tree.
                        this.treeViewStudies.Nodes.Remove(this.treeViewStudies.SelectedNode);
                    }
                }
            }
            else
            {
                MessageBox.Show(MainForm.ResourceManager.GetString("You can not delete default study!"),
                    MainForm.ResourceManager.GetString("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void treeViewStudies_AfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e)
        {
            if (this.treeViewStudies.SelectedNode != null
                && this.treeViewStudies.SelectedNode.Index != 0
                && this.treeViewStudies.SelectedNode.FirstNode == null)
            {
                this.buttonDelete.Enabled = true;
            }
            else
            {
                this.buttonDelete.Enabled = false;
            }
        }
    }
}
