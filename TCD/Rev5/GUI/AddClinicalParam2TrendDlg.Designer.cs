﻿namespace TCD2003.GUI
{
    partial class AddClinicalParam2TrendDlg
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxChannelText = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.gbAlarm = new System.Windows.Forms.GroupBox();
            this.textBoxAlarmHigh = new System.Windows.Forms.NumericUpDown();
            this.lblHigh = new System.Windows.Forms.Label();
            this.lblLow = new System.Windows.Forms.Label();
            this.textBoxAlarmLow = new System.Windows.Forms.NumericUpDown();
            this.gbYAxis = new System.Windows.Forms.GroupBox();
            this.textBoxMin = new System.Windows.Forms.NumericUpDown();
            this.lblMin = new System.Windows.Forms.Label();
            this.lblMax = new System.Windows.Forms.Label();
            this.textBoxMax = new System.Windows.Forms.NumericUpDown();
            this.lblColor = new System.Windows.Forms.Label();
            this.lblUnits = new System.Windows.Forms.Label();
            this.comboBoxUnits = new System.Windows.Forms.ComboBox();
            this.btnOK = new System.Windows.Forms.Button();
            this.numericUpDownTrendNo = new System.Windows.Forms.NumericUpDown();
            this.lblChannel = new System.Windows.Forms.Label();
            this.comboBoxChannelName = new System.Windows.Forms.ComboBox();
            this.lblTrendNo = new System.Windows.Forms.Label();
            this.btnCancel = new System.Windows.Forms.Button();
            this.colorPickerButtonColor = new Syncfusion.Windows.Forms.ColorPickerButton();
            this.gbAlarm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxAlarmHigh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxAlarmLow)).BeginInit();
            this.gbYAxis.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTrendNo)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxChannelText
            // 
            this.textBoxChannelText.Location = new System.Drawing.Point(112, 72);
            this.textBoxChannelText.Name = "textBoxChannelText";
            this.textBoxChannelText.Size = new System.Drawing.Size(120, 20);
            this.textBoxChannelText.TabIndex = 62;
            this.textBoxChannelText.Text = "Peak-1";
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(8, 72);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(96, 24);
            this.lblName.TabIndex = 61;
            this.lblName.Text = "Channel name:";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gbAlarm
            // 
            this.gbAlarm.Controls.Add(this.textBoxAlarmHigh);
            this.gbAlarm.Controls.Add(this.lblHigh);
            this.gbAlarm.Controls.Add(this.lblLow);
            this.gbAlarm.Controls.Add(this.textBoxAlarmLow);
            this.gbAlarm.Location = new System.Drawing.Point(8, 248);
            this.gbAlarm.Name = "gbAlarm";
            this.gbAlarm.Size = new System.Drawing.Size(240, 64);
            this.gbAlarm.TabIndex = 60;
            this.gbAlarm.TabStop = false;
            this.gbAlarm.Text = "Alarm";
            // 
            // textBoxAlarmHigh
            // 
            this.textBoxAlarmHigh.Location = new System.Drawing.Point(60, 21);
            this.textBoxAlarmHigh.Name = "textBoxAlarmHigh";
            this.textBoxAlarmHigh.Size = new System.Drawing.Size(56, 20);
            this.textBoxAlarmHigh.TabIndex = 12;
            // 
            // lblHigh
            // 
            this.lblHigh.Location = new System.Drawing.Point(4, 21);
            this.lblHigh.Name = "lblHigh";
            this.lblHigh.Size = new System.Drawing.Size(48, 23);
            this.lblHigh.TabIndex = 10;
            this.lblHigh.Text = "High:";
            this.lblHigh.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblLow
            // 
            this.lblLow.Location = new System.Drawing.Point(122, 21);
            this.lblLow.Name = "lblLow";
            this.lblLow.Size = new System.Drawing.Size(50, 23);
            this.lblLow.TabIndex = 9;
            this.lblLow.Text = "Low:";
            this.lblLow.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxAlarmLow
            // 
            this.textBoxAlarmLow.Location = new System.Drawing.Point(178, 21);
            this.textBoxAlarmLow.Name = "textBoxAlarmLow";
            this.textBoxAlarmLow.Size = new System.Drawing.Size(56, 20);
            this.textBoxAlarmLow.TabIndex = 11;
            // 
            // gbYAxis
            // 
            this.gbYAxis.Controls.Add(this.textBoxMin);
            this.gbYAxis.Controls.Add(this.lblMin);
            this.gbYAxis.Controls.Add(this.lblMax);
            this.gbYAxis.Controls.Add(this.textBoxMax);
            this.gbYAxis.Location = new System.Drawing.Point(8, 168);
            this.gbYAxis.Name = "gbYAxis";
            this.gbYAxis.Size = new System.Drawing.Size(240, 64);
            this.gbYAxis.TabIndex = 59;
            this.gbYAxis.TabStop = false;
            this.gbYAxis.Text = "Y Axis";
            // 
            // textBoxMin
            // 
            this.textBoxMin.Location = new System.Drawing.Point(56, 21);
            this.textBoxMin.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.textBoxMin.Name = "textBoxMin";
            this.textBoxMin.Size = new System.Drawing.Size(60, 20);
            this.textBoxMin.TabIndex = 10;
            this.textBoxMin.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // lblMin
            // 
            this.lblMin.Location = new System.Drawing.Point(6, 21);
            this.lblMin.Name = "lblMin";
            this.lblMin.Size = new System.Drawing.Size(50, 23);
            this.lblMin.TabIndex = 8;
            this.lblMin.Text = "Min:";
            this.lblMin.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblMax
            // 
            this.lblMax.Location = new System.Drawing.Point(125, 21);
            this.lblMax.Name = "lblMax";
            this.lblMax.Size = new System.Drawing.Size(49, 23);
            this.lblMax.TabIndex = 7;
            this.lblMax.Text = "Max:";
            this.lblMax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxMax
            // 
            this.textBoxMax.Location = new System.Drawing.Point(178, 21);
            this.textBoxMax.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.textBoxMax.Name = "textBoxMax";
            this.textBoxMax.Size = new System.Drawing.Size(56, 20);
            this.textBoxMax.TabIndex = 9;
            // 
            // lblColor
            // 
            this.lblColor.Location = new System.Drawing.Point(16, 136);
            this.lblColor.Name = "lblColor";
            this.lblColor.Size = new System.Drawing.Size(88, 24);
            this.lblColor.TabIndex = 55;
            this.lblColor.Text = "Color";
            this.lblColor.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblUnits
            // 
            this.lblUnits.Location = new System.Drawing.Point(16, 104);
            this.lblUnits.Name = "lblUnits";
            this.lblUnits.Size = new System.Drawing.Size(88, 24);
            this.lblUnits.TabIndex = 56;
            this.lblUnits.Text = "Units:";
            this.lblUnits.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // comboBoxUnits
            // 
            this.comboBoxUnits.Items.AddRange(new object[] {
            "cm/sec"});
            this.comboBoxUnits.Location = new System.Drawing.Point(112, 104);
            this.comboBoxUnits.Name = "comboBoxUnits";
            this.comboBoxUnits.Size = new System.Drawing.Size(120, 21);
            this.comboBoxUnits.TabIndex = 58;
            // 
            // btnOK
            // 
            this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnOK.Location = new System.Drawing.Point(32, 328);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(80, 24);
            this.btnOK.TabIndex = 53;
            this.btnOK.Text = "OK";
            // 
            // numericUpDownTrendNo
            // 
            this.numericUpDownTrendNo.Location = new System.Drawing.Point(112, 40);
            this.numericUpDownTrendNo.Name = "numericUpDownTrendNo";
            this.numericUpDownTrendNo.Size = new System.Drawing.Size(120, 20);
            this.numericUpDownTrendNo.TabIndex = 52;
            this.numericUpDownTrendNo.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblChannel
            // 
            this.lblChannel.Location = new System.Drawing.Point(8, 8);
            this.lblChannel.Name = "lblChannel";
            this.lblChannel.Size = new System.Drawing.Size(96, 23);
            this.lblChannel.TabIndex = 50;
            this.lblChannel.Text = "Channel";
            this.lblChannel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // comboBoxChannelName
            // 
            this.comboBoxChannelName.Location = new System.Drawing.Point(112, 8);
            this.comboBoxChannelName.Name = "comboBoxChannelName";
            this.comboBoxChannelName.Size = new System.Drawing.Size(120, 21);
            this.comboBoxChannelName.TabIndex = 49;
            this.comboBoxChannelName.Text = "comboBox1";
            // 
            // lblTrendNo
            // 
            this.lblTrendNo.Location = new System.Drawing.Point(8, 40);
            this.lblTrendNo.Name = "lblTrendNo";
            this.lblTrendNo.Size = new System.Drawing.Size(96, 23);
            this.lblTrendNo.TabIndex = 51;
            this.lblTrendNo.Text = "Select trend No:";
            this.lblTrendNo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(144, 328);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 24);
            this.btnCancel.TabIndex = 54;
            this.btnCancel.Text = "Cancel";
            // 
            // colorPickerButtonColor
            // 
            this.colorPickerButtonColor.BackColor = System.Drawing.SystemColors.Control;
            this.colorPickerButtonColor.ColorUISize = new System.Drawing.Size(208, 230);
            this.colorPickerButtonColor.Location = new System.Drawing.Point(112, 136);
            this.colorPickerButtonColor.Name = "colorPickerButtonColor";
            this.colorPickerButtonColor.SelectedColorGroup = Syncfusion.Windows.Forms.ColorUISelectedGroup.None;
            this.colorPickerButtonColor.Size = new System.Drawing.Size(120, 24);
            this.colorPickerButtonColor.TabIndex = 63;
            this.colorPickerButtonColor.UseVisualStyleBackColor = false;
            // 
            // AddClinicalParam2TrendDlg
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(256, 366);
            this.Controls.Add(this.colorPickerButtonColor);
            this.Controls.Add(this.textBoxChannelText);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.gbAlarm);
            this.Controls.Add(this.gbYAxis);
            this.Controls.Add(this.lblColor);
            this.Controls.Add(this.lblUnits);
            this.Controls.Add(this.comboBoxUnits);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.numericUpDownTrendNo);
            this.Controls.Add(this.lblChannel);
            this.Controls.Add(this.comboBoxChannelName);
            this.Controls.Add(this.lblTrendNo);
            this.Controls.Add(this.btnCancel);
            this.Name = "AddClinicalParam2TrendDlg";
            this.gbAlarm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textBoxAlarmHigh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxAlarmLow)).EndInit();
            this.gbYAxis.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textBoxMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTrendNo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        
        #endregion

        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.GroupBox gbAlarm;
        private System.Windows.Forms.GroupBox gbYAxis;
        private System.Windows.Forms.Label lblColor;
        private System.Windows.Forms.Label lblUnits;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label lblChannel;
        private System.Windows.Forms.Label lblTrendNo;
        private System.Windows.Forms.Button btnCancel;
        private Syncfusion.Windows.Forms.ColorPickerButton colorPickerButtonColor;
        private System.Windows.Forms.NumericUpDown textBoxMin;
        private System.Windows.Forms.Label lblMin;
        private System.Windows.Forms.Label lblMax;
        private System.Windows.Forms.NumericUpDown textBoxMax;
        private System.Windows.Forms.TextBox textBoxChannelText;
        private System.Windows.Forms.ComboBox comboBoxUnits;
        private System.Windows.Forms.NumericUpDown numericUpDownTrendNo;
        private System.Windows.Forms.ComboBox comboBoxChannelName;
        private System.Windows.Forms.NumericUpDown textBoxAlarmHigh;
        private System.Windows.Forms.Label lblHigh;
        private System.Windows.Forms.Label lblLow;
        private System.Windows.Forms.NumericUpDown textBoxAlarmLow;
        private System.ComponentModel.Container components = null;
    }
}