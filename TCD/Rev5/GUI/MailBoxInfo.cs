using System.Text;
using System.Windows.Forms;

using TCD2003.DSP;

namespace TCD2003.GUI
{
    /// <summary>
    /// Summary description for MailBoxInfo.
    /// </summary>
    public partial class MailBoxInfo : Form
    {
        public MailBoxInfo()
        {
            this.InitializeComponent();
            this.Text = MainForm.ResourceManager.GetString("Mail Box Info");
        }

        public void InitText(string str)
        {
            this.textBox1.Text = str;
        }

        private void timer1_Tick_1(object sender, System.EventArgs e)
        {
            if (Dsps.Instance[0].DspExists)
            {
                StringBuilder tmp = new StringBuilder("DSP Debug Info:\n");
                for (uint i = 9; i < 41; i++)
                {
                    uint res = Dsps.Instance[0].ReadDsp2PcMbx(i);
                    if (res != 10)
                        tmp.Append(i.ToString() + " = ");
                    tmp.Append(res.ToString());
                    tmp.Append("\r\n");
                }

                this.InitText(tmp.ToString());
            }
        }
    }
}
