//using System;
//using System.Runtime.Serialization;
//using System.Runtime.Serialization.Formatters.Binary;
//using System.IO;
//
//namespace TCD2003.GUI
//{
//	/// <summary>
//	/// Summary description for Gate.
//	/// </summary>
//	[Serializable]
//	public class Gate : IComparable
//	{
//		public enum TGateStatus { eAdded2SummaryScreen, eGoing2beAdded2SummarysScreen, eNotInSummaryScreen };
//		private bool m_SaveOnDB = false;
//		public bool SaveOnDB
//		{
//			get { return m_SaveOnDB; }
//			set { m_SaveOnDB = value; }
//		}
//
//		public Gate()
//		{
//			m_GateID = Guid.NewGuid();
//			m_InSummaryScreen = Gate.TGateStatus.eNotInSummaryScreen;
//		}
//
//		public Gate(DB.dsGateExamination.tb_GateExaminationRow row)
//		{
//			m_GateID = row.Gate_Index;
//			if(((bool)row.InSummaryScreen) == true)
//				m_InSummaryScreen = Gate.TGateStatus.eGoing2beAdded2SummarysScreen;
//			else
//				m_InSummaryScreen = Gate.TGateStatus.eNotInSummaryScreen;
//
//			m_BVExaminationIndex = row.BVExaminationIndex;
//			m_BVIndex = row.BVIndex;
//			m_GateName = row.Name;
//			
//			m_Depth = row.Depth;
//			m_Gain = row.Gain;
//			m_Thump = row.Thump;
//			m_Width = row.Width;
//			m_Power = row.Power;
//			m_Range = row.Range;
//			
//
//			m_Id = row.ID;
//
//			// clinical parameter.
//			m_PeakTopVal = row.PeakTopVal;
//			m_PeakBottomVal = row.PeakBottomVal;
//
//			m_MeanTopVal = row.MeanTopVal;
//			m_MeanBottomVal = row.MeanBottomVal;	
//
//			m_DVTopVal = row.DVTopVal;	
//			m_DVBottomVal = row.DVBottomVal;
//
//			m_PITopVal = row.PITopVal;	
//			m_PIBottomVal = row.PIBottomVal;
//
//			m_SWTopVal = row.SWTopVal;	
//			m_SWBottomVal = row.SWBottomVal;
//
//			m_SDTopVal = row.SDTopVal;	
//			m_SDBottomVal = row.SDBottomVal;
//
//			m_RITopVal = row.RITopVal;	
//			m_RIBottomVal = row.RIBottomVal;
//
//			m_ModeTopVal = row.ModeTopVal;	
//			m_ModeBottomVal = row.ModeBottomVal;
//
//			m_AverageTopVal = row.AverageTopVal;	
//			m_AverageBottomVal = row.AverageBottomVal;
//
//			m_HitsTopVal = row.HitsTopVal;	
//			m_HitsBottomVal = row.HitsBottomVal;
//
//			MinYAxis = row.MinVal;
//			MaxYAxis = row.MaxVal;
//			ProbeName = row.ProbeName;
//			ProbeColor = System.Drawing.Color.FromArgb(row.ProbeColor);
//			m_ChartVisibilityState = (GlobalTypes.TChartVisibilityState)row.chartVisibilityState;
//			m_TimeDomainMode = row.TimeDomainMode;
//			m_ChartMode = (GlobalTypes.TSizeMode)row.chartMode;
//
//			m_ZeroLinePercent = row.ZeroLinePercent;
//			m_DrawSpectrum = row.DrawSpectrum;
//			m_DrawPeakFwdEnvelope = row.DrawPeakFwdEnvelope;
//			m_DrawPeakBckEnvelope = row.DrawPeakBckEnvelope;
//			m_DrawModeFwdEnvelope = row.DrawModeFwdEnvelope;
//			m_DrawModeBckEnvelope = row.DrawModeBckEnvelope;
//			m_FlowDirection = (GlobalTypes.TFlowDirection)row.FlowDirection;
//
//			m_LastDrawTimeMSec = row.LastDrawTime;
//			
//
//			byte [] picData = row.SummaryPicture;
//			MemoryStream ms = new MemoryStream(picData);
//			m_Picture = ms;
//
//			byte [] repPicData = row.ReportPicture;
//			MemoryStream msRep = new MemoryStream(picData);
//			m_RepPicture = msRep;
//			m_Selected = row.Selected;
//			this.m_Notes = row.Notes;
//		}
//		private Guid m_GateID;
//		public Guid GateID
//		{
//			get { return m_GateID; }
//		}
//
//		public int m_Id;
//		public Guid m_BVExaminationIndex;
//		public Guid BVExaminationIndex
//		{
//			get { return m_BVExaminationIndex; }
//			set { m_BVExaminationIndex = value; }
//		}
//
//
//		private Guid m_BVIndex;
//		public Guid BVIndex
//		{
//			get { return m_BVIndex; }
//			set { m_BVIndex = value; }
//		}
//
//
//		private string m_GateName;
//		public string GateName
//		{
//			get { return m_GateName; }
//			set { m_GateName = value; }
//		}
//
//
//		private bool m_GateExamined = false;
//		public bool GateExamined
//		{
//			get { return m_GateExamined; }
//			set { m_GateExamined = value; }
//		}
//
//		//		private string m_BitmapPath;
////		public string BitmapPath
////		{
////			get { return m_BackupPath; }
////			set { m_BitmapPath = value; }
////		}
//		public int IndexInProbe;
//
//		private bool m_Selected;
//		public bool Selected
//		{
//			get { return m_Selected; }
//			set { m_Selected = value; }
//		}
//
//
//		#region Doppler Variables
//		public double m_Depth;
//		public int m_Gain;
//		public int m_Thump;
//		public double m_Width;
//		public int m_Power;
//		public int m_Range;
//		public GlobalTypes.TFlowDirection m_FlowDirection;
//		public GlobalTypes.TSpectrumUnits m_Units;
//		#endregion
//
//		#region Clinical Param
//		public double m_PeakTopVal;
//		public double m_PeakBottomVal;	
//
//		public double m_MeanTopVal;
//		public double m_MeanBottomVal;	
//
//		public double m_DVTopVal;	
//		public double m_DVBottomVal;
//
//		public double m_PITopVal;	
//		public double m_PIBottomVal;
//
//		public double m_SWTopVal;	
//		public double m_SWBottomVal;
//
//		public double m_SDTopVal;	
//		public double m_SDBottomVal;
//
//		public double m_RITopVal;	
//		public double m_RIBottomVal;
//
//		public double m_ModeTopVal;	
//		public double m_ModeBottomVal;
//
//		public double m_AverageTopVal;	
//		public double m_AverageBottomVal;
//
//		public double m_HitsTopVal;	
//		public double m_HitsBottomVal;
//		#endregion
//
//
//		// the picture is used for the summary screen only.
//		// this variable will contain the last screen.
//		private System.IO.MemoryStream m_Picture;
//		public System.IO.MemoryStream Picture
//		{
//			get { return m_Picture; }
//			set 
//			{ 
//				if(m_Picture == null)
//					m_Picture = new System.IO.MemoryStream();
//				m_Picture = value; 
//			}
//		}
//
//
//		private TGateStatus m_InSummaryScreen;
//		public TGateStatus InSummaryScreen
//		{
//			get { return m_InSummaryScreen; }
//			set { m_InSummaryScreen = value; }
//		}
//
//
//		public double MinYAxis;
//		public double MaxYAxis;
//		public string ProbeName;
//		public System.Drawing.Color ProbeColor;
//		public GlobalTypes.TChartVisibilityState m_ChartVisibilityState;
//		public bool m_TimeDomainMode;
//		public GlobalTypes.TSizeMode m_ChartMode;
//
//		public int m_ZeroLinePercent;
//		public bool m_DrawSpectrum;
//		public bool m_DrawPeakFwdEnvelope;
//		public bool m_DrawPeakBckEnvelope;
//		public bool m_DrawModeFwdEnvelope;
//		public bool m_DrawModeBckEnvelope;
//
//		public double m_LastDrawTimeMSec;
//
//		public string m_Notes = string.Empty;
//
//
//		private System.IO.MemoryStream m_RepPicture;
//		public System.IO.MemoryStream RepPicture
//		{
//			get { return m_RepPicture; }
//			set 
//			{ 
//				if(m_RepPicture == null)
//					m_RepPicture = new System.IO.MemoryStream();
//				m_RepPicture = value; 
//			}
//		}
//
//        /// <summary>
//		/// Copy from the gateView to the gate in order to save the gate information for farther usege.
//		/// </summary>
//		/// <param name="src"></param>
//		public void CopyFrom (GateView src)
//		{
//			this.m_Id = src.Id;
//
//			this.m_Selected = src.Selected;
//
//			this.m_ChartMode = src.ChartMode;
//
//			// Gate Name.
//			this.GateName = src.dopplerBar.BVName;
//
////			string bmpFileName = GlobalSettings.GateInfoDir + "\\" + this.GateID + ".jpg";
////			System.IO.MemoryStream ms = src.Spectrum.Graph.GenerateBmp(false);
////			System.Drawing.Bitmap tmpBmp = new System.Drawing.Bitmap(bmpFileName);
////			tmpBmp.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
//
//			// picture.
//			this.Picture = src.Spectrum.Graph.GenerateBmp(false, new System.Drawing.Rectangle(0, 0, 957, 128));
//
//			this.RepPicture = src.Spectrum.Graph.GenerateBmp(true, new System.Drawing.Rectangle(0, 0, 957, 128));
//
//			// units.
//			this.m_Units = src.Spectrum.SpectrumUnits;
//
//			// Y Axis.
//			this.MaxYAxis = src.Spectrum.spectrumYAxis1.MaxVal;
//			this.MinYAxis = src.Spectrum.spectrumYAxis1.MinVal;
//
//			// probe.
//			this.ProbeColor = src.probe.Color;
//			this.ProbeName = src.probe.Name;
//
////			this.summarySpectrumChart1.CopyFrom(Src.Spectrum);
////			m_RowDataPath = DSP.Dsps.Instance[ src.DspId ].FileName;
//
//			// copy the doppler variables into the gate.
//			this.m_Depth = src.dopplerBar.DepthVar;
//			this.m_Width = src.dopplerBar.WidthVar;
//			this.m_Gain = src.dopplerBar.GainVar;
//			this.m_Thump = src.dopplerBar.thumpVar;
//			this.m_Power = src.dopplerBar.PowerVar;
//			this.m_Range = src.dopplerBar.RangeVar;
//
//			this.m_GateExamined = src.Spectrum.Graph.ExaminedGraph;
//
//			this.m_ZeroLinePercent = src.Spectrum.Graph.ZeroLinePercent;
//			this.m_DrawSpectrum = src.Spectrum.Graph.DrawSpectrum;
//			this.m_DrawPeakFwdEnvelope = src.Spectrum.Graph.DrawPeakFwdEnvelope;
//			this.m_DrawPeakBckEnvelope = src.Spectrum.Graph.DrawPeakBckEnvelope;
//			this.m_DrawModeFwdEnvelope = src.Spectrum.Graph.DrawModeFwdEnvelope;
//			this.m_DrawModeBckEnvelope = src.Spectrum.Graph.DrawModeBckEnvelope;
//
//			this.m_LastDrawTimeMSec = src.Spectrum.Graph.LastDrawTime.TotalMilliseconds;
//
//			this.m_FlowDirection = src.Spectrum.Graph.FlowDirection;
//
//			bool copyClinicalParam = LayoutManager.theLayoutManager.CopyClinicalParam;
//			// copy the clinical parm into the gate.
//			this.m_PeakTopVal = src.Spectrum.clinicalBar1.ClinicalParamLabelPeakUpper;
//			this.m_PeakBottomVal = (copyClinicalParam) ? src.Spectrum.clinicalBar1.ClinicalParamLabelPeakLower : 0;	
//
//			this.m_MeanTopVal = (copyClinicalParam) ? src.Spectrum.clinicalBar1.ClinicalParamLabelMeanUpper : 0;
//			this.m_MeanBottomVal = (copyClinicalParam) ? src.Spectrum.clinicalBar1.ClinicalParamLabelMeanLower : 0;	
//
//			this.m_DVTopVal = (copyClinicalParam) ? src.Spectrum.clinicalBar1.ClinicalParamLabelDVUpper : 0;	
//			this.m_DVBottomVal = (copyClinicalParam) ? src.Spectrum.clinicalBar1.ClinicalParamLabelDVLower : 0;
//
//			this.m_PITopVal = (copyClinicalParam) ? src.Spectrum.clinicalBar1.ClinicalParamLabelPIUpper : 0;	
//			this.m_PIBottomVal = (copyClinicalParam) ? src.Spectrum.clinicalBar1.ClinicalParamLabelPILower : 0;
//
//			this.m_SWTopVal = (copyClinicalParam) ? src.Spectrum.clinicalBar1.ClinicalParamLabelSWUpper : 0;	
//			this.m_SWBottomVal = (copyClinicalParam) ? src.Spectrum.clinicalBar1.ClinicalParamLabelSWLower : 0;
//
//			this.m_SDTopVal = (copyClinicalParam) ? src.Spectrum.clinicalBar1.ClinicalParamLabelSDUpper : 0;	
//			this.m_SDBottomVal = (copyClinicalParam) ? src.Spectrum.clinicalBar1.ClinicalParamLabelSDLower : 0;
//
//			this.m_RITopVal = (copyClinicalParam) ? src.Spectrum.clinicalBar1.ClinicalParamLabelRIUpper : 0;	
//			this.m_RIBottomVal = (copyClinicalParam) ? src.Spectrum.clinicalBar1.ClinicalParamLabelRILower : 0;
//
//			this.m_ModeTopVal = (copyClinicalParam) ? src.Spectrum.clinicalBar1.ClinicalParamLabelModeUpper : 0;	
//			this.m_ModeBottomVal = (copyClinicalParam) ? src.Spectrum.clinicalBar1.ClinicalParamLabelModeLower : 0;
//
//			this.m_AverageTopVal = (copyClinicalParam) ? src.Spectrum.clinicalBar1.ClinicalParamLabelAverageUpper : 0;	
//			this.m_AverageBottomVal = (copyClinicalParam) ? src.Spectrum.clinicalBar1.ClinicalParamLabelAverageLower : 0;
//
//			this.m_HitsTopVal = (copyClinicalParam) ? src.Spectrum.clinicalBar1.ClinicalParamLabelHitsUpper : 0;	
//			this.m_HitsBottomVal = (copyClinicalParam) ? src.Spectrum.clinicalBar1.ClinicalParamLabelHitsLower : 0;
//
//			this.m_ChartVisibilityState = src.chartVisibilityState;
//
//			this.m_TimeDomainMode = src.TimeDomainMode;
//			this.m_Notes = src.GVNotes;
//		}
//
//
//		public int CompareTo(object obj)
//		{
//			if (obj is Gate)
//				return ((Gate)obj).m_GateID.CompareTo(this.m_GateID);
//			else if (obj is string)
//				return ((string)obj).CompareTo(this.m_GateID);
//			else
//				return 0;
//		}
//
//
//		public void CopyTo(System.Data.DataRow row)
//		{
//			row["BVExaminationIndex"] = BVExaminationIndex;
//			row["BVIndex"] = BVIndex;
//
//			row["Gate_Index"] = GateID;
//			row["Name"] = GateName;
//			if(InSummaryScreen == TGateStatus.eAdded2SummaryScreen)
//				row["InSummaryScreen"] = true;
//			else
//				row["InSummaryScreen"] = false;
//
//
//			row["Depth"] = m_Depth;
//			row["Gain"] = m_Gain;
//			row["Thump"] = m_Thump;
//			row["Width"] = m_Width;
//			row["Power"] = m_Power;
//			row["Range"] = m_Range;
//			row["IndexInProbe"] = IndexInProbe;
//			row["ID"] = m_Id;
//			
//
//			// clinical parameter.
//			row["PeakTopVal"] = (double)m_PeakTopVal;
//			row["PeakBottomVal"] = (double)m_PeakBottomVal;
//
//			row["MeanTopVal"] = (double)m_MeanTopVal;
//			row["MeanBottomVal"] = (double)m_MeanBottomVal;	
//
//			row["DVTopVal"] = (double)m_DVTopVal;	
//			row["DVBottomVal"] = (double)m_DVBottomVal;
//
//			row["PITopVal"] = (double)m_PITopVal;	
//			row["PIBottomVal"] = (double)m_PIBottomVal;
//
//			row["SWTopVal"] = (double)m_SWTopVal;	
//			row["SWBottomVal"] = (double)m_SWBottomVal;
//
//			row["SDTopVal"] = (double)m_SDTopVal;	
//			row["SDBottomVal"] = (double)m_SDBottomVal;
//
//			row["RITopVal"] = (double)m_RITopVal;	
//			row["RIBottomVal"] = (double)m_RIBottomVal;
//
//			row["ModeTopVal"] = (double)m_ModeTopVal;	
//			row["ModeBottomVal"] = (double)m_ModeBottomVal;
//
//			row["AverageTopVal"] = (double)m_AverageTopVal;	
//			row["AverageBottomVal"] = (double)m_AverageBottomVal;
//
//			row["HitsTopVal"] = (double)m_HitsTopVal;	
//			row["HitsBottomVal"] = (double)m_HitsBottomVal;
//
//			row["MinVal"] = MinYAxis;
//			row["MaxVal"] = MaxYAxis;
//			row["ProbeName"] = ProbeName;
//			row["ProbeColor"] = ProbeColor.ToArgb();
//			row["chartVisibilityState"] = m_ChartVisibilityState;
//			row["TimeDomainMode"] = m_TimeDomainMode;
//			row["chartMode"] = m_ChartMode;
//
//			row["ZeroLinePercent"] = m_ZeroLinePercent;
//			row["DrawSpectrum"] = m_DrawSpectrum;
//			row["DrawPeakFwdEnvelope"] = m_DrawPeakFwdEnvelope;
//			row["DrawPeakBckEnvelope"] = m_DrawPeakBckEnvelope;
//			row["DrawModeFwdEnvelope"] = m_DrawModeFwdEnvelope;
//			row["DrawModeBckEnvelope"] = m_DrawModeBckEnvelope;
//			row["FlowDirection"] = m_FlowDirection;
//
//			row["LastDrawTime"] = m_LastDrawTimeMSec;
//
//			byte [] picData = new byte[m_Picture.Length];
//			picData = m_Picture.ToArray();
//			row["SummaryPicture"] = picData;
//
//			byte [] repPicData = new byte[m_Picture.Length];
//			repPicData = m_RepPicture.ToArray();
//			row["ReportPicture"] = repPicData;
//			row["Notes"] = m_Notes;
//		}
//
//		#region unused code
////		public void Serialize(System.IO.FileStream fs)
////		{
////			IFormatter formatter = new BinaryFormatter();
////			formatter.Serialize(fs, m_Id);
////			formatter.Serialize(fs, m_PeakTopVal);
////			formatter.Serialize(fs, m_PeakBottomVal);
////			formatter.Serialize(fs, m_MeanTopVal);
////			formatter.Serialize(fs, m_MeanBottomVal);
////			formatter.Serialize(fs, m_DVTopVal);
////			formatter.Serialize(fs, m_DVBottomVal);
////			formatter.Serialize(fs, m_PITopVal);
////			formatter.Serialize(fs, m_PIBottomVal);
////			formatter.Serialize(fs, m_SWTopVal);
////			formatter.Serialize(fs, m_SWBottomVal);
////			formatter.Serialize(fs, m_SDTopVal);
////			formatter.Serialize(fs, m_SDBottomVal);
////			formatter.Serialize(fs, m_RITopVal);
////			formatter.Serialize(fs, m_RIBottomVal);
////			formatter.Serialize(fs, m_ModeTopVal);
////			formatter.Serialize(fs, m_ModeBottomVal);
////			formatter.Serialize(fs, m_AverageTopVal);
////			formatter.Serialize(fs, m_AverageBottomVal);
////			formatter.Serialize(fs, m_HitsTopVal);
////			formatter.Serialize(fs, m_HitsBottomVal);
////
////
////			formatter.Serialize(fs, MinYAxis);
////			formatter.Serialize(fs, MaxYAxis);
////			formatter.Serialize(fs, ProbeName);
////			formatter.Serialize(fs, ProbeColor);
////			formatter.Serialize(fs, m_ChartVisibilityState);
////			formatter.Serialize(fs, m_TimeDomainMode);
////			formatter.Serialize(fs, m_ChartMode);
//////			if(m_AutoscanInfo != null)
//////				formatter.Serialize(fs, m_AutoscanInfo);
////
////			formatter.Serialize(fs, m_ZeroLinePercent);
////			formatter.Serialize(fs, m_DrawSpectrum);
////			formatter.Serialize(fs, m_DrawPeakFwdEnvelope);
////			formatter.Serialize(fs, m_DrawPeakBckEnvelope);
////			formatter.Serialize(fs, m_DrawModeFwdEnvelope);
////			formatter.Serialize(fs, m_DrawModeBckEnvelope);
////			formatter.Serialize(fs, m_FlowDirection);
////
////			formatter.Serialize(fs, m_LastDrawTimeMSec);
////
////
////			formatter.Serialize(fs, m_Picture);
////			formatter.Serialize(fs, m_RepPicture);
////
////			formatter.Serialize(fs, m_Selected);
////		}
////
////		public void Deserialize(System.IO.FileStream fs)
////		{
////			IFormatter formatter = new BinaryFormatter();
////			m_Id = (int)formatter.Deserialize(fs);
////
////			// clinical parameter.
////			m_PeakTopVal = (double)formatter.Deserialize(fs);
////			m_PeakBottomVal = (double)formatter.Deserialize(fs);	
////
////			m_MeanTopVal = (double)formatter.Deserialize(fs);
////			m_MeanBottomVal = (double)formatter.Deserialize(fs);	
////
////			m_DVTopVal = (double)formatter.Deserialize(fs);	
////			m_DVBottomVal = (double)formatter.Deserialize(fs);
////
////			m_PITopVal = (double)formatter.Deserialize(fs);	
////			m_PIBottomVal = (double)formatter.Deserialize(fs);
////
////			m_SWTopVal = (double)formatter.Deserialize(fs);	
////			m_SWBottomVal = (double)formatter.Deserialize(fs);
////
////			m_SDTopVal = (double)formatter.Deserialize(fs);	
////			m_SDBottomVal = (double)formatter.Deserialize(fs);
////
////			m_RITopVal = (double)formatter.Deserialize(fs);	
////			m_RIBottomVal = (double)formatter.Deserialize(fs);
////
////			m_ModeTopVal = (double)formatter.Deserialize(fs);	
////			m_ModeBottomVal = (double)formatter.Deserialize(fs);
////
////			m_AverageTopVal = (double)formatter.Deserialize(fs);	
////			m_AverageBottomVal = (double)formatter.Deserialize(fs);
////
////			m_HitsTopVal = (double)formatter.Deserialize(fs);	
////			m_HitsBottomVal = (double)formatter.Deserialize(fs);
////
////			MinYAxis = (double)formatter.Deserialize(fs);
////			MaxYAxis = (double)formatter.Deserialize(fs);
////			ProbeName = (string)formatter.Deserialize(fs);
////			ProbeColor = (System.Drawing.Color)formatter.Deserialize(fs);
////			m_ChartVisibilityState = (GlobalTypes.TChartVisibilityState)formatter.Deserialize(fs);
////			m_TimeDomainMode = (bool)formatter.Deserialize(fs);
////			m_ChartMode = (GlobalTypes.TSizeMode)formatter.Deserialize(fs);
////
////			m_ZeroLinePercent = (int)formatter.Deserialize(fs);
////			m_DrawSpectrum = (bool)formatter.Deserialize(fs);
////			m_DrawPeakFwdEnvelope = (bool)formatter.Deserialize(fs);
////			m_DrawPeakBckEnvelope = (bool)formatter.Deserialize(fs);
////			m_DrawModeFwdEnvelope = (bool)formatter.Deserialize(fs);
////			m_DrawModeBckEnvelope = (bool)formatter.Deserialize(fs);
////			m_FlowDirection = (GlobalTypes.TFlowDirection)formatter.Deserialize(fs);
////
////			m_LastDrawTimeMSec = (double)formatter.Deserialize(fs);
////			
////
////			m_Picture = (System.IO.MemoryStream)formatter.Deserialize(fs);
////			m_RepPicture = (System.IO.MemoryStream)formatter.Deserialize(fs);
////
////			m_Selected = (bool)formatter.Deserialize(fs);
////		}
//		#endregion
//	}
//}
