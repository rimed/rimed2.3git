using System;

namespace TCD2003.GUI
{
    public class SingleHistoryEvent
    {
        private Guid m_Event_Index;
        private Guid m_Examination_Index;
        private Guid m_Gate_Index;
        private int m_FileIndex;
        private long m_IndexInFile;
        private DateTime m_EventTime;
        private bool m_Deleted;
        private GlobalTypes.EventType m_EventType;
        private String m_EventName;
        private GlobalTypes.THitType m_HitType;
        private double m_HitVelocity;
        private double m_HitEnergy;
        private double m_AlarmValue;
        private double m_VMRPrimary;
        private double m_VMRSecondary;
        private double m_TrendSiganlIndexAtEventTime;
        private short m_FlowChangePrimary;
        private short m_FlowChangeSecondary;
        private bool m_EvokedFlowEvent;

        public SingleHistoryEvent(Guid Event_Index, Guid Examination_Index,
            int FileIndex, long IndexInFile, DateTime EventTime,
              bool Deleted, GlobalTypes.EventType EventType, String EventName, double VMRPrimary, double VMRSecondary, double TrendSiganlIndexAtEventTime,
            short FlowChangePrimary, short FlowChangeSecondary, bool EvokedFlowEvent, GlobalTypes.THitType HitType)
        {
            this.m_Event_Index = Event_Index;
            this.m_EventName = EventName;
            this.m_Examination_Index = Examination_Index;
            this.m_FileIndex = FileIndex;
            this.m_IndexInFile = IndexInFile;
            this.m_EventTime = EventTime;
            this.m_Deleted = Deleted;
            this.m_EventType = EventType;
            this.m_VMRPrimary = VMRPrimary;
            this.m_VMRSecondary = VMRSecondary;
            this.m_TrendSiganlIndexAtEventTime = TrendSiganlIndexAtEventTime;
            this.m_FlowChangePrimary = FlowChangePrimary;
            this.m_FlowChangeSecondary = FlowChangeSecondary;
            this.m_EvokedFlowEvent = EvokedFlowEvent;
            this.m_HitType = HitType;
        }

        public String EventName
        {
            get
            {
                return m_EventName;
            }
        }
        public Guid Event_Index
        {
            get
            {
                return m_Event_Index;
            }
        }
        public Guid Examination_Index
        {
            get
            {
                return m_Examination_Index;
            }
        }
        public Guid Gate_Index
        {
            get
            {
                return m_Gate_Index;
            }
            set
            {
                m_Gate_Index = value;
            }
        }
        public int File_Index
        {
            get
            {
                return m_FileIndex;
            }
        }
        public long IndexInFile
        {
            get
            {
                return m_IndexInFile;
            }
        }
        public DateTime EventTime
        {
            get
            {
                return m_EventTime;
            }
        }
        public bool Deleted
        {
            get
            {
                return m_Deleted;
            }
        }
        public GlobalTypes.EventType EventType
        {
            get
            {
                return m_EventType;
            }
        }
        public GlobalTypes.THitType HitType
        {
            get
            {
                return m_HitType;
            }
            set
            {
                m_HitType = value;
            }
        }
        public double HitVelocity
        {
            get
            {
                return m_HitVelocity;
            }
            set
            {
                m_HitVelocity = value;
            }
        }
        public double HitEnergy
        {
            get
            {
                return m_HitEnergy;
            }
            set
            {
                m_HitEnergy = value;
            }
        }
        public double AlarmValue
        {
            get
            {
                return m_AlarmValue;
            }
            set
            {
                m_AlarmValue = value;
            }
        }
        public double VMRSecondary
        {
            get
            {
                return m_VMRSecondary;
            }
            set
            {
                m_VMRSecondary = value;
            }
        }
        public double VMRPrimary
        {
            get
            {
                return m_VMRPrimary;
            }
            set
            {
                m_VMRPrimary = value;
            }
        }
        public double TrendSiganlIndexAtEventTime
        {
            get
            {
                return m_TrendSiganlIndexAtEventTime;
            }
            set
            {
                m_TrendSiganlIndexAtEventTime = value;
            }
        }
        public short FlowChangePrimary
        {
            get
            {
                return m_FlowChangePrimary;
            }
            set
            {
                m_FlowChangePrimary = value;
            }
        }
        public short FlowChangeSecondary
        {
            get
            {
                return m_FlowChangeSecondary;
            }
            set
            {
                m_FlowChangeSecondary = value;
            }
        }
        public bool EvokedFlowEvent
        {
            get
            {
                return m_EvokedFlowEvent;
            }
            set
            {
                m_EvokedFlowEvent = value;
            }
        }
    }
}
