using System;
using System.Threading;
using System.Windows.Forms;
using TCD2003.Utils;

namespace TCD2003.GUI
{
	/// <summary>
	/// Summary description for GlobalClass.
	/// </summary>
	public class CursorsPosEventArgs : EventArgs
	{
        public CursorsPosEventArgs(double masterCursorPos, double secondCursorPos, GraphCursors.Direction cursorDirection)
        {
            this.MasterCursorPos = masterCursorPos;
            this.SecondCursorPos = secondCursorPos;
            this.CursorDirection = cursorDirection;
        }

        /// <summary>
        /// the time/position in the graph
        /// </summary>
		public double MasterCursorPos { get; private set; }
        
        public double SecondCursorPos { get; private set; }
        
        /// <summary>
        /// horizontal or vertical
        /// </summary>
        public GraphCursors.Direction CursorDirection { get; private set; }
	}
	internal class ThreadExceptionHandler
	{
        /// <summary>
        /// Handles the thread exception.
        /// </summary>
		public void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
		{
			try
			{
                ExceptionPublisherLog4Net.ExceptionFatalLog(e.Exception, "Unhandled exception");
                // Exit the program if the user clicks Abort.
				DialogResult result = ShowThreadExceptionDialog(e.Exception);

				if (result == DialogResult.Abort) 
					Application.Exit();
			}
			catch (Exception ex)
			{
                // Fatal error, terminate program
				try
				{
                    ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
                    MessageBox.Show(MainForm.ResourceManager.GetString("Fatal Error"), MainForm.ResourceManager.GetString("Fatal Error"), MessageBoxButtons.OK, MessageBoxIcon.Stop);
				}
				finally
				{
					Application.Exit();
				}
			}
		}

		/// 
		/// Creates and displays the error message.
		/// 
		private DialogResult ShowThreadExceptionDialog(Exception ex) 
		{
			String errorMessage= 
				"Unhandled Exception:\n\n" +
				ex.Message + "\n\n" + 
				ex.GetType() + 
				"\n\nStack Trace:\n" + 
				ex.StackTrace;
			return MessageBox.Show(errorMessage,
                MainForm.ResourceManager.GetString("Application Error"), 
				MessageBoxButtons.AbortRetryIgnore, 
				MessageBoxIcon.Stop);
		}
	}
}
