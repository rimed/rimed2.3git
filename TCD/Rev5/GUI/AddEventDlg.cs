using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TCD2003.Utils;

namespace TCD2003.GUI
{
    public partial class AddEventDlg : Form
    {
        public AddEventDlg()
        {
            this.InitializeComponent();
        }

        private void AddEventDlg_Load(object sender, EventArgs e)
        {
            var strRes = ResourceManagerWrapper.GetWrapper(MainForm.ResourceManager); //Ofer, v1.18.2.9

            this.Text = strRes.GetString("Add Event");
            this.btnOK.Text = strRes.GetString("OK");
            this.btnCancel.Text = strRes.GetString("Cancel");
            this.lblName.Text = strRes.GetString("Event name");

            List<SingleEvent> events = EventListViewCtrl.Instance.PredefinedEvents;

            foreach (SingleEvent item in events)
                this.cbEventList.Items.Add(item.Name);
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(this.cbEventList.Text))
            {
                //TODO: Check Event Time
                EventListViewCtrl.Instance.AddHistoryEvent(cbEventList.Text, GlobalTypes.EventType.PreDefinedEvent, true);
                Close();
            }
        }
    }
}
