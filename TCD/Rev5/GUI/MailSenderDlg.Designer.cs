﻿namespace TCD2003.GUI
{
    partial class MailSenderDlg
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSend = new System.Windows.Forms.Button();
            this.btnCancal = new System.Windows.Forms.Button();
            this.textBoxTo = new System.Windows.Forms.TextBox();
            this.lblTo = new System.Windows.Forms.Label();
            this.lblSubject = new System.Windows.Forms.Label();
            this.lblCC = new System.Windows.Forms.Label();
            this.textBoxCc = new System.Windows.Forms.TextBox();
            this.textBoxSubject = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxBody = new System.Windows.Forms.TextBox();
            this.buttonHelp = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnSend
            // 
            this.btnSend.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSend.Location = new System.Drawing.Point(64, 288);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(96, 24);
            this.btnSend.TabIndex = 0;
            this.btnSend.Text = "Send";
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // btnCancal
            // 
            this.btnCancal.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancal.Location = new System.Drawing.Point(176, 288);
            this.btnCancal.Name = "btnCancal";
            this.btnCancal.Size = new System.Drawing.Size(96, 24);
            this.btnCancal.TabIndex = 0;
            this.btnCancal.Text = "Cancel";
            // 
            // textBoxTo
            // 
            this.textBoxTo.Location = new System.Drawing.Point(81, 16);
            this.textBoxTo.Name = "textBoxTo";
            this.textBoxTo.Size = new System.Drawing.Size(319, 20);
            this.textBoxTo.TabIndex = 1;
            // 
            // lblTo
            // 
            this.lblTo.Location = new System.Drawing.Point(8, 16);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(67, 24);
            this.lblTo.TabIndex = 2;
            this.lblTo.Text = "To...";
            this.lblTo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSubject
            // 
            this.lblSubject.Location = new System.Drawing.Point(8, 96);
            this.lblSubject.Name = "lblSubject";
            this.lblSubject.Size = new System.Drawing.Size(67, 24);
            this.lblSubject.TabIndex = 2;
            this.lblSubject.Text = "Subject:";
            this.lblSubject.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCC
            // 
            this.lblCC.Location = new System.Drawing.Point(8, 56);
            this.lblCC.Name = "lblCC";
            this.lblCC.Size = new System.Drawing.Size(67, 24);
            this.lblCC.TabIndex = 2;
            this.lblCC.Text = "Cc...";
            this.lblCC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxCc
            // 
            this.textBoxCc.Location = new System.Drawing.Point(81, 56);
            this.textBoxCc.Name = "textBoxCc";
            this.textBoxCc.Size = new System.Drawing.Size(319, 20);
            this.textBoxCc.TabIndex = 1;
            // 
            // textBoxSubject
            // 
            this.textBoxSubject.Location = new System.Drawing.Point(81, 96);
            this.textBoxSubject.Name = "textBoxSubject";
            this.textBoxSubject.Size = new System.Drawing.Size(319, 20);
            this.textBoxSubject.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(8, 136);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 24);
            this.label4.TabIndex = 2;
            this.label4.Text = "Message:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxBody
            // 
            this.textBoxBody.Location = new System.Drawing.Point(81, 136);
            this.textBoxBody.Multiline = true;
            this.textBoxBody.Name = "textBoxBody";
            this.textBoxBody.Size = new System.Drawing.Size(319, 136);
            this.textBoxBody.TabIndex = 1;
            // 
            // buttonHelp
            // 
            this.buttonHelp.Location = new System.Drawing.Point(288, 288);
            this.buttonHelp.Name = "buttonHelp";
            this.buttonHelp.Size = new System.Drawing.Size(96, 24);
            this.buttonHelp.TabIndex = 0;
            this.buttonHelp.Text = "Help";
            // 
            // MailSenderDlg
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(424, 326);
            this.Controls.Add(this.lblTo);
            this.Controls.Add(this.textBoxTo);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.btnCancal);
            this.Controls.Add(this.lblSubject);
            this.Controls.Add(this.lblCC);
            this.Controls.Add(this.textBoxCc);
            this.Controls.Add(this.textBoxSubject);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxBody);
            this.Controls.Add(this.buttonHelp);
            this.Name = "MailSenderDlg";
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        
        #endregion

        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.Button btnCancal;
        private System.Windows.Forms.TextBox textBoxTo;
        private System.Windows.Forms.Label lblTo;
        private System.Windows.Forms.Label lblSubject;
        private System.Windows.Forms.Label lblCC;
        private System.Windows.Forms.TextBox textBoxCc;
        private System.Windows.Forms.TextBox textBoxSubject;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxBody;
        private System.Windows.Forms.Button buttonHelp;
        private System.ComponentModel.Container components = null;
    }
}