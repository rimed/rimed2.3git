using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

namespace TCD2003.GUI
{
	/// <summary>
	/// Summary description for ProbeIndication.
	/// </summary>
	public partial class ProbeIndication : System.Windows.Forms.UserControl
	{
		private Color m_ProbeColor;
		private String m_ProbeName;
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public ProbeIndication()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
		}

		public Color ProbeColor
		{
			get { return m_ProbeColor; }
			set 
			{
				m_ProbeColor = value;
				this.ProbeRect.BackColor = m_ProbeColor;
			}
		}
		
        public String ProbeName
		{
			get { return m_ProbeName; }
			set
            { 
				m_ProbeName = value; 
				this.Probelabel.Text = m_ProbeName;
			}
		}
		
        public Font LabelFont
		{
			set { this.Probelabel.Font = value; }
		}
	}
}
