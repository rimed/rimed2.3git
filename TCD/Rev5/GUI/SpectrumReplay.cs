using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace TCD2003.GUI
{
	/// <summary>
	/// Summary description for SpectrumReplay.
	/// </summary>
	public class SpectrumReplay : System.Windows.Forms.Form
	{
		private System.Windows.Forms.MainMenu mainMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItem2;
		private TCD2003.GUI.ToolBarPanel toolBarPanel1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public SpectrumReplay()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(SpectrumReplay));
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.toolBarPanel1 = new TCD2003.GUI.ToolBarPanel();
			this.SuspendLayout();
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem1,
																					  this.menuItem2});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.Text = "Print";
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 1;
			this.menuItem2.Text = "Exit";
			// 
			// toolBarPanel1
			// 
			this.toolBarPanel1.BackColor = System.Drawing.SystemColors.Control;
			this.toolBarPanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("toolBarPanel1.BackgroundImage")));
			this.toolBarPanel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.toolBarPanel1.Location = new System.Drawing.Point(0, 0);
			this.toolBarPanel1.Name = "toolBarPanel1";
			this.toolBarPanel1.Size = new System.Drawing.Size(728, 36);
			this.toolBarPanel1.TabIndex = 0;
			// 
			// SpectrumReplay
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(728, 385);
			this.ControlBox = false;
			this.Controls.Add(this.toolBarPanel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Menu = this.mainMenu1;
			this.MinimizeBox = false;
			this.Name = "SpectrumReplay";
			this.ShowInTaskbar = false;
			this.Text = "SpectrumReplay";
			this.ResumeLayout(false);

		}
		#endregion
	}
}
