﻿namespace TCD2003.GUI
{
    partial class BvName
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxBvName = new System.Windows.Forms.TextBox();
            this.lblBVName = new System.Windows.Forms.Label();
            this.buttonOK = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            this.gbSide = new System.Windows.Forms.GroupBox();
            this.radioButtonLeft = new System.Windows.Forms.RadioButton();
            this.radioButtonRight = new System.Windows.Forms.RadioButton();
            this.radioButtonBothSides = new System.Windows.Forms.RadioButton();
            this.radioButtonMidle = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButtonExtracranial = new System.Windows.Forms.RadioButton();
            this.radioButtonPeriphral = new System.Windows.Forms.RadioButton();
            this.radioButtonIntracranial = new System.Windows.Forms.RadioButton();
            this.gbSide.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxBvName
            // 
            this.textBoxBvName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxBvName.Location = new System.Drawing.Point(144, 16);
            this.textBoxBvName.Name = "textBoxBvName";
            this.textBoxBvName.Size = new System.Drawing.Size(249, 20);
            this.textBoxBvName.TabIndex = 0;
            // 
            // lblBVName
            // 
            this.lblBVName.Location = new System.Drawing.Point(12, 15);
            this.lblBVName.Name = "lblBVName";
            this.lblBVName.Size = new System.Drawing.Size(128, 23);
            this.lblBVName.TabIndex = 1;
            this.lblBVName.Text = "Blood Vessel Name";
            this.lblBVName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buttonOK
            // 
            this.buttonOK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOK.Enabled = false;
            this.buttonOK.Location = new System.Drawing.Point(112, 168);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 2;
            this.buttonOK.Text = "OK";
            // 
            // bCancel
            // 
            this.bCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.bCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.bCancel.Location = new System.Drawing.Point(216, 168);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(75, 23);
            this.bCancel.TabIndex = 3;
            this.bCancel.Text = "Cancel";
            // 
            // gbSide
            // 
            this.gbSide.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbSide.Controls.Add(this.radioButtonLeft);
            this.gbSide.Controls.Add(this.radioButtonRight);
            this.gbSide.Controls.Add(this.radioButtonBothSides);
            this.gbSide.Controls.Add(this.radioButtonMidle);
            this.gbSide.Location = new System.Drawing.Point(8, 40);
            this.gbSide.Name = "gbSide";
            this.gbSide.Size = new System.Drawing.Size(393, 56);
            this.gbSide.TabIndex = 6;
            this.gbSide.TabStop = false;
            this.gbSide.Text = "Side";
            // 
            // radioButtonLeft
            // 
            this.radioButtonLeft.AutoSize = true;
            this.radioButtonLeft.Location = new System.Drawing.Point(8, 25);
            this.radioButtonLeft.Name = "radioButtonLeft";
            this.radioButtonLeft.Size = new System.Drawing.Size(43, 17);
            this.radioButtonLeft.TabIndex = 8;
            this.radioButtonLeft.Text = "Left";
            this.radioButtonLeft.CheckedChanged += new System.EventHandler(this.side_CheckedChanged);
            // 
            // radioButtonRight
            // 
            this.radioButtonRight.AutoSize = true;
            this.radioButtonRight.Location = new System.Drawing.Point(95, 25);
            this.radioButtonRight.Name = "radioButtonRight";
            this.radioButtonRight.Size = new System.Drawing.Size(50, 17);
            this.radioButtonRight.TabIndex = 9;
            this.radioButtonRight.Text = "Right";
            this.radioButtonRight.CheckedChanged += new System.EventHandler(this.side_CheckedChanged);
            // 
            // radioButtonBothSides
            // 
            this.radioButtonBothSides.AutoSize = true;
            this.radioButtonBothSides.Location = new System.Drawing.Point(184, 25);
            this.radioButtonBothSides.Name = "radioButtonBothSides";
            this.radioButtonBothSides.Size = new System.Drawing.Size(76, 17);
            this.radioButtonBothSides.TabIndex = 6;
            this.radioButtonBothSides.Text = "Both Sides";
            this.radioButtonBothSides.CheckedChanged += new System.EventHandler(this.side_CheckedChanged);
            // 
            // radioButtonMidle
            // 
            this.radioButtonMidle.AutoSize = true;
            this.radioButtonMidle.Location = new System.Drawing.Point(299, 25);
            this.radioButtonMidle.Name = "radioButtonMidle";
            this.radioButtonMidle.Size = new System.Drawing.Size(56, 17);
            this.radioButtonMidle.TabIndex = 7;
            this.radioButtonMidle.Text = "Middle";
            this.radioButtonMidle.CheckedChanged += new System.EventHandler(this.side_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.radioButtonExtracranial);
            this.groupBox1.Controls.Add(this.radioButtonPeriphral);
            this.groupBox1.Controls.Add(this.radioButtonIntracranial);
            this.groupBox1.Location = new System.Drawing.Point(8, 96);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(393, 56);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            // 
            // radioButtonExtracranial
            // 
            this.radioButtonExtracranial.AutoSize = true;
            this.radioButtonExtracranial.Location = new System.Drawing.Point(16, 24);
            this.radioButtonExtracranial.Name = "radioButtonExtracranial";
            this.radioButtonExtracranial.Size = new System.Drawing.Size(80, 17);
            this.radioButtonExtracranial.TabIndex = 0;
            this.radioButtonExtracranial.Text = "Extracranial";
            this.radioButtonExtracranial.CheckedChanged += new System.EventHandler(this.location_CheckedChanged);
            // 
            // radioButtonPeriphral
            // 
            this.radioButtonPeriphral.AutoSize = true;
            this.radioButtonPeriphral.Location = new System.Drawing.Point(274, 24);
            this.radioButtonPeriphral.Name = "radioButtonPeriphral";
            this.radioButtonPeriphral.Size = new System.Drawing.Size(72, 17);
            this.radioButtonPeriphral.TabIndex = 0;
            this.radioButtonPeriphral.Text = "Peripheral";
            this.radioButtonPeriphral.CheckedChanged += new System.EventHandler(this.location_CheckedChanged);
            // 
            // radioButtonIntracranial
            // 
            this.radioButtonIntracranial.AutoSize = true;
            this.radioButtonIntracranial.Location = new System.Drawing.Point(136, 24);
            this.radioButtonIntracranial.Name = "radioButtonIntracranial";
            this.radioButtonIntracranial.Size = new System.Drawing.Size(77, 17);
            this.radioButtonIntracranial.TabIndex = 0;
            this.radioButtonIntracranial.Text = "Intracranial";
            this.radioButtonIntracranial.CheckedChanged += new System.EventHandler(this.location_CheckedChanged);
            // 
            // BvName
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(411, 207);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbSide);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.lblBVName);
            this.Controls.Add(this.textBoxBvName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BvName";
            this.ShowInTaskbar = false;
            this.gbSide.ResumeLayout(false);
            this.gbSide.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxBvName;
        private System.Windows.Forms.Label lblBVName;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.GroupBox gbSide;
        private System.Windows.Forms.RadioButton radioButtonLeft;
        private System.Windows.Forms.RadioButton radioButtonRight;
        private System.Windows.Forms.RadioButton radioButtonBothSides;
        private System.Windows.Forms.RadioButton radioButtonMidle;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButtonExtracranial;
        private System.Windows.Forms.RadioButton radioButtonPeriphral;
        private System.Windows.Forms.RadioButton radioButtonIntracranial;
        private System.ComponentModel.Container components = null;
    }
}