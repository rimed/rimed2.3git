using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;

using TCD2003.Utils;

namespace TCD2003.GUI
{
    /// <summary>
    /// Summary description for probes combobox.
    /// </summary>
    public partial class ProbesCombobox : ComboBox
    {
        /// <summary>
        /// Probes combobox item
        /// </summary>
        private class ProbesComboboxItem
        {
            /// <summary>
            /// Probe color
            /// </summary>
            public Color ProbeColor { get; private set; }

            /// <summary>
            /// Probe name
            /// </summary>
            public String ProbeName { get; private set; }

            /// <summary>
            /// Probe index
            /// </summary>
            public Guid Index { get; private set; }

            public ProbesComboboxItem(Color probeColor, String probeName, Guid index)
            {
                ProbeColor = probeColor;
                ProbeName = probeName;
                Index = index;
            }
        }

        private bool m_initFlag = false;

        /// <summary>
        /// Constructor
        /// </summary>
        public ProbesCombobox()
        {
            this.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        }

        public Guid SelectedItemIndex
        {
            get
            {
                if (this.DesignMode)
                    return Guid.Empty;
                ProbesComboboxItem item = (ProbesComboboxItem)this.SelectedItem;
                return item.Index;
            }
        }

        #region Events and delegates

        public delegate void ProbeChangedDelegate(Guid probeIndex, object sender, int index);

        public event ProbeChangedDelegate ProbeChangedEvent;

        private delegate void TurnActivativationDelegate(bool offline);

        #endregion Events and delegates

        #region Public Methods

        public void AddItem(Color rgb, String text, Guid index)
        {
            if (!m_initFlag)
            {
                m_initFlag = true;
                Init();
            }
            ProbesComboboxItem item = new ProbesComboboxItem(rgb, text, index);
            this.Items.Add(item);
            this.SelectedIndex = 0;
        }

        public void ClearItems()
        {
            this.Items.Clear();
        }

        public void ChangeCurrentProbe()
        {
            if (this.SelectedIndex >= (this.Items.Count - 1))
                this.SelectedIndex = 0;
            else
                this.SelectedIndex++;
        }

        #endregion Public Methods

        private void Init()
        {
            BVListViewCtrl.sInstance.BVChangedEvent += new BVChangedDelegate(ProbesCombobox_BVChanged);
            this.SelectedIndexChanged += new System.EventHandler(this.ProbesCombobox_SelectedIndexChanged);
            this.SelectionChangeCommitted += new EventHandler(ProbesCombobox_SelectionChangeCommitted);
        }

        private void FireProbeChangedEvent(Guid index)
        {
            if (LayoutManager.theLayoutManager.StudyType == GlobalTypes.TStudyType.eUnilateral &&
                this.ProbeChangedEvent != null)
            {
                ProbeChangedEvent(index, this, 0);
            }
        }

        protected override void OnDrawItem(DrawItemEventArgs e)
        {
            e.DrawBackground();
            e.DrawFocusRectangle();

            Rectangle bounds = e.Bounds;

            if (e.Index == -1)
                return;
            ProbesComboboxItem item = (ProbesComboboxItem)this.Items[e.Index];

            SolidBrush rectColor = new SolidBrush(item.ProbeColor);

            e.Graphics.FillRectangle(rectColor, e.Bounds.Left + 1, e.Bounds.Y + 1, e.Bounds.Height - 2, e.Bounds.Height - 2);

            Pen rectBorder = new Pen(e.ForeColor);
            e.Graphics.DrawRectangle(rectBorder, e.Bounds.Left + 1, e.Bounds.Y + 1, e.Bounds.Height - 2, e.Bounds.Height - 2);
            rectBorder.Dispose();

            SolidBrush drawBrush = new SolidBrush(e.ForeColor);

            // Create point for upper-left corner of drawing.
            float x = e.Bounds.Left + e.Bounds.Height + 3;
            float y = e.Bounds.Y;
            // Set format of String.
            StringFormat drawFormat = new StringFormat();
            drawFormat.FormatFlags = StringFormatFlags.DirectionVertical;
            // Draw String to screen.
            e.Graphics.DrawString(item.ProbeName, e.Font, drawBrush, x, y);
            drawBrush.Dispose();

            base.OnDrawItem(e);
        }

        #region Event Handlers

        private void ProbesCombobox_BVChanged(BV[] bvArr, object sender)
        {
            if (!(sender is ListView || sender == this))
                return;

            BV bv = bvArr[0];
            ProbesComboboxItem selItem = (ProbesComboboxItem)this.SelectedItem;
            if (selItem == null || selItem.Index == bv.BVProbe.ID)
                return;

            for (int i = 0; i < this.Items.Count; i++)
            {
                ProbesComboboxItem item = (ProbesComboboxItem)this.Items[i];
                if (item.Index == bv.BVProbe.ID)
                {
                    this.SelectedItem = this.Items[i];
                    return;
                }
            }
        }

        private void ProbesCombobox_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            ProbesComboboxItem item = (ProbesComboboxItem)this.SelectedItem;

            FireProbeChangedEvent(item.Index);
        }

        private void ProbesCombobox_SelectionChangeCommitted(object sender, System.EventArgs e)
        {
            ProbesCombobox combo = sender as ProbesCombobox;
            ProbesComboboxItem item = (ProbesComboboxItem)this.SelectedItem;
            ExceptionPublisherLog4Net.ChangeComboValueLog(combo.Name, combo.Parent.Name, item.ProbeName);
        }

        public void ProbesCombobox_TurnActivativation(bool offline)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((TurnActivativationDelegate)ProbesCombobox_TurnActivativation, offline);
                return;
            }

            this.Enabled = offline;
            this.Visible = offline;
        }

        #endregion Event Handlers
    }
}
