namespace TCD2003.GUI
{
    partial class AutoScanChart
    {
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(AutoScanChart));
            this.AutoScanGraph = new TCD2003.GUI.AutoScanGraph();
            this.AutoScanYAxis = new TCD2003.GUI.SpectrumYAxis();
            this.panelRight = new System.Windows.Forms.Panel();
            this.verticalTextCtrl1 = new TCD2003.GUI.OrientedTextLabel();
            this.Line1 = new System.Windows.Forms.Panel();
            this.labelDepth = new System.Windows.Forms.Label();
            this.Arrow = new System.Windows.Forms.PictureBox();
            this.Line2 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.probeIndication1 = new TCD2003.GUI.ProbeIndication();
            this.gradientPanel = new Syncfusion.Windows.Forms.Tools.GradientPanel();
            this.VerticalTxtCtrlDepth = new TCD2003.GUI.OrientedTextLabel();
            this.AutoscanGradientPanel = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.pictureBoxGradientRed = new System.Windows.Forms.PictureBox();
            this.pictureBoxGradientBlue = new System.Windows.Forms.PictureBox();
            this.contextMenuAutoScan = new System.Windows.Forms.ContextMenu();
            this.menuItemClose = new System.Windows.Forms.MenuItem();
            this.panelRight.SuspendLayout();
            this.gradientPanel.SuspendLayout();
            this.AutoscanGradientPanel.SuspendLayout();
            this.SuspendLayout();
            //
            // AutoScanGraph
            // 
            this.AutoScanGraph.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(192)), ((System.Byte)(192)), ((System.Byte)(255)));
            this.AutoScanGraph.DepthLinePercent = 50;
            this.AutoScanGraph.DrawAutoScan = true;
            this.AutoScanGraph.Location = new System.Drawing.Point(96, 16);
            this.AutoScanGraph.MaxVal = 84;
            this.AutoScanGraph.MinVal = 20;
            this.AutoScanGraph.Name = "AutoScanGraph";
            this.AutoScanGraph.Size = new System.Drawing.Size(696, 408);
            this.AutoScanGraph.SpeedFactor = 2;
            this.AutoScanGraph.TabIndex = 2;
            this.AutoScanGraph.TimeInGraph = 5.5680000000000005;
            this.AutoScanGraph.YAxis = this.AutoScanYAxis;
            // 
            // AutoScanYAxis
            // 
            this.AutoScanYAxis.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(170)), ((System.Byte)(190)), ((System.Byte)(230)));
            this.AutoScanYAxis.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.AutoScanYAxis.Location = new System.Drawing.Point(16, 16);
            this.AutoScanYAxis.MaxValue = 84;
            this.AutoScanYAxis.MinValue = 20;
            this.AutoScanYAxis.Name = "AutoScanYAxis";
            this.AutoScanYAxis.Size = new System.Drawing.Size(32, 280);
            this.AutoScanYAxis.TabIndex = 0;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.verticalTextCtrl1);
            this.panelRight.Controls.Add(this.Line1);
            this.panelRight.Controls.Add(this.labelDepth);
            this.panelRight.Controls.Add(this.Arrow);
            this.panelRight.Controls.Add(this.Line2);
            this.panelRight.Controls.Add(this.panel2);
            this.panelRight.Controls.Add(this.probeIndication1);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(856, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(144, 456);
            this.panelRight.TabIndex = 3;
            // 
            // verticalTextCtrl1
            // 
            this.verticalTextCtrl1.RotationAngle = 270F;
            this.verticalTextCtrl1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((System.Byte)(177)));
            this.verticalTextCtrl1.Location = new System.Drawing.Point(32, 104);
            this.verticalTextCtrl1.Name = "verticalTextCtrl1";
            this.verticalTextCtrl1.Size = new System.Drawing.Size(24, 64);
            this.verticalTextCtrl1.TabIndex = 13;
            this.verticalTextCtrl1.Text = "20";
            // 
            // Line1
            // 
            this.Line1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(90)), ((System.Byte)(110)), ((System.Byte)(170)));
            this.Line1.Location = new System.Drawing.Point(32, 28);
            this.Line1.Name = "Line1";
            this.Line1.Size = new System.Drawing.Size(1, 232);
            this.Line1.TabIndex = 12;
            // 
            // labelDepth
            // 
            this.labelDepth.BackColor = System.Drawing.Color.Transparent;
            this.labelDepth.Location = new System.Drawing.Point(72, 104);
            this.labelDepth.Name = "labelDepth";
            this.labelDepth.Size = new System.Drawing.Size(40, 16);
            this.labelDepth.TabIndex = 9;
            this.labelDepth.Text = "55";
            this.labelDepth.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // Arrow
            // 
            this.Arrow.BackColor = System.Drawing.Color.Transparent;
            this.Arrow.Image = ((System.Drawing.Image)(resources.GetObject("Arrow.Image")));
            this.Arrow.Location = new System.Drawing.Point(56, 124);
            this.Arrow.Name = "Arrow";
            this.Arrow.Size = new System.Drawing.Size(88, 16);
            this.Arrow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Arrow.TabIndex = 8;
            this.Arrow.TabStop = false;
            // 
            // Line2
            // 
            this.Line2.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(90)), ((System.Byte)(110)), ((System.Byte)(170)));
            this.Line2.Location = new System.Drawing.Point(64, 28);
            this.Line2.Name = "Line2";
            this.Line2.Size = new System.Drawing.Size(1, 232);
            this.Line2.TabIndex = 11;
            // 
            // panel2
            // 
            this.panel2.Location = new System.Drawing.Point(64, 28);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1, 232);
            this.panel2.TabIndex = 10;
            // 
            // probeIndication1
            // 
            this.probeIndication1.Location = new System.Drawing.Point(8, 296);
            this.probeIndication1.Name = "probeIndication1";
            this.probeIndication1.ProbeColor = System.Drawing.Color.Empty;
            this.probeIndication1.ProbeName = null;
            this.probeIndication1.Size = new System.Drawing.Size(64, 16);
            this.probeIndication1.TabIndex = 6;
            this.probeIndication1.Visible = false;
            // 
            // gradientPanel
            // 
            this.gradientPanel.BackColor = System.Drawing.Color.Transparent;
            this.gradientPanel.Border3DStyle = System.Windows.Forms.Border3DStyle.Flat;
            this.gradientPanel.BorderColor = System.Drawing.Color.Black;
            this.gradientPanel.BorderSides = System.Windows.Forms.Border3DSide.Middle;
            this.gradientPanel.BorderSingle = System.Windows.Forms.ButtonBorderStyle.None;
            this.gradientPanel.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gradientPanel.Controls.Add(this.VerticalTxtCtrlDepth);
            this.gradientPanel.Controls.Add(this.AutoScanYAxis);
            this.gradientPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.gradientPanel.Location = new System.Drawing.Point(0, 0);
            this.gradientPanel.Name = "gradientPanel";
            this.gradientPanel.Size = new System.Drawing.Size(40, 456);
            this.gradientPanel.TabIndex = 4;
            // 
            // VerticalTxtCtrlDepth
            // 
            this.VerticalTxtCtrlDepth.RotationAngle = 270F;
            this.VerticalTxtCtrlDepth.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(170)), ((System.Byte)(190)), ((System.Byte)(230)));
            this.VerticalTxtCtrlDepth.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((System.Byte)(177)));
            this.VerticalTxtCtrlDepth.ForeColor = System.Drawing.Color.Red;
            this.VerticalTxtCtrlDepth.Location = new System.Drawing.Point(16, 96);
            this.VerticalTxtCtrlDepth.Name = "VerticalTxtCtrlDepth";
            this.VerticalTxtCtrlDepth.Size = new System.Drawing.Size(16, 80);
            this.VerticalTxtCtrlDepth.TabIndex = 1;
            this.VerticalTxtCtrlDepth.Text = "Depth";
            // 
            // AutoscanGradientPanel
            // 
            this.AutoscanGradientPanel.BackColor = System.Drawing.Color.White;
            this.AutoscanGradientPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.AutoscanGradientPanel.Controls.Add(this.pictureBox1);
            this.AutoscanGradientPanel.Controls.Add(this.splitter2);
            this.AutoscanGradientPanel.Controls.Add(this.splitter1);
            this.AutoscanGradientPanel.Controls.Add(this.pictureBoxGradientRed);
            this.AutoscanGradientPanel.Controls.Add(this.pictureBoxGradientBlue);
            this.AutoscanGradientPanel.Location = new System.Drawing.Point(816, 8);
            this.AutoscanGradientPanel.Name = "AutoscanGradientPanel";
            this.AutoscanGradientPanel.Size = new System.Drawing.Size(24, 424);
            this.AutoscanGradientPanel.TabIndex = 5;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Black;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(0, 57);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(20, 306);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // splitter2
            // 
            this.splitter2.BackColor = System.Drawing.Color.White;
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter2.Enabled = false;
            this.splitter2.Location = new System.Drawing.Point(0, 363);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(20, 1);
            this.splitter2.TabIndex = 2;
            this.splitter2.TabStop = false;
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.White;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Enabled = false;
            this.splitter1.Location = new System.Drawing.Point(0, 56);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(20, 1);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // pictureBoxGradientRed
            // 
            this.pictureBoxGradientRed.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBoxGradientRed.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxGradientRed.Image")));
            this.pictureBoxGradientRed.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxGradientRed.Name = "pictureBoxGradientRed";
            this.pictureBoxGradientRed.Size = new System.Drawing.Size(20, 56);
            this.pictureBoxGradientRed.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxGradientRed.TabIndex = 0;
            this.pictureBoxGradientRed.TabStop = false;
            // 
            // pictureBoxGradientBlue
            // 
            this.pictureBoxGradientBlue.BackColor = System.Drawing.Color.White;
            this.pictureBoxGradientBlue.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pictureBoxGradientBlue.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxGradientBlue.Image")));
            this.pictureBoxGradientBlue.Location = new System.Drawing.Point(0, 364);
            this.pictureBoxGradientBlue.Name = "pictureBoxGradientBlue";
            this.pictureBoxGradientBlue.Size = new System.Drawing.Size(20, 56);
            this.pictureBoxGradientBlue.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxGradientBlue.TabIndex = 0;
            this.pictureBoxGradientBlue.TabStop = false;
            // 
            // contextMenuAutoScan
            // 
            this.contextMenuAutoScan.MenuItems.AddRange(new System.Windows.Forms.MenuItem[]
            {
                this.menuItemClose});
            this.contextMenuAutoScan.Popup += new System.EventHandler(this.contextMenuAutoScan_Popup);
            // 
            // menuItemClose
            // 
            this.menuItemClose.Index = 0;
            this.menuItemClose.Click += new System.EventHandler(this.menuItemClose_Click);
            // 
            // AutoScanChart
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(170)), ((System.Byte)(190)), ((System.Byte)(230)));
            this.ContextMenu = this.contextMenuAutoScan;
            this.Controls.Add(this.AutoscanGradientPanel);
            this.Controls.Add(this.gradientPanel);
            this.Controls.Add(this.panelRight);
            this.Controls.Add(this.AutoScanGraph);
            this.Name = "AutoScanChart";
            this.Size = new System.Drawing.Size(1000, 456);
            this.SizeChanged += new System.EventHandler(this.AutoScanChart_SizeChanged);
            this.panelRight.ResumeLayout(false);
            this.gradientPanel.ResumeLayout(false);
            this.AutoscanGradientPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public TCD2003.GUI.AutoScanGraph AutoScanGraph;
        private System.Windows.Forms.Panel panelRight;
        private Syncfusion.Windows.Forms.Tools.GradientPanel gradientPanel;
        public TCD2003.GUI.SpectrumYAxis AutoScanYAxis;
        private System.Windows.Forms.Panel AutoscanGradientPanel;
        private System.Windows.Forms.PictureBox pictureBoxGradientRed;
        private System.Windows.Forms.PictureBox pictureBoxGradientBlue;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel Line1;
        private System.Windows.Forms.Label labelDepth;
        private System.Windows.Forms.PictureBox Arrow;
        private System.Windows.Forms.Panel Line2;
        private System.Windows.Forms.Panel panel2;
        private TCD2003.GUI.OrientedTextLabel verticalTextCtrl1;
        private TCD2003.GUI.OrientedTextLabel VerticalTxtCtrlDepth;
        private System.Windows.Forms.ContextMenu contextMenuAutoScan;
        private System.Windows.Forms.MenuItem menuItemClose;
        private TCD2003.GUI.ProbeIndication probeIndication1;
        private System.ComponentModel.Container components = null;
    }
}