using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;

using TCD2003.Utils;

namespace TCD2003.GUI
{
    public partial class ZipRawdataFilesDlg : Form
    {
        #region Private Fields

        private readonly string m_backupDir = null;
        private FileInfo[] m_fileInfo;
        private List<string> m_zipFilesList;
        private Thread m_zipThread = null;

        #endregion Private Fields

        public ZipRawdataFilesDlg()
        {
            this.InitializeComponent();
            this.BackColor = GlobalSettings.BackgroundDlg;

            var strRes = ResourceManagerWrapper.GetWrapper(MainForm.ResourceManager); 
            this.lbInfo.Text = strRes.GetString("The system is Zipping the raw data files, it might take a few seconds.");
            this.buttonStart.Text = strRes.GetString("Start");
            this.buttonClose.Text = strRes.GetString("Close");
            this.buttonHelp.Text = strRes.GetString("Help");
            this.Text = strRes.GetString("Zip Raw-Data Files");
        }

        public ZipRawdataFilesDlg(string backupDir, List<string> zipFilesList)
            : this()
        {
            m_backupDir = backupDir;
            ZipFilesList = zipFilesList;
        }

        private List<string> ZipFilesList
        {
            set
            {
                this.m_zipFilesList = value;
                this.m_fileInfo = new FileInfo[this.m_zipFilesList.Count];
                for (int i = 0; i < this.m_zipFilesList.Count; i++)
                {
                    this.m_fileInfo[i] = new FileInfo(this.m_zipFilesList[i]);
                }
            }
        }

        private void ZipRawdataFilesDlg_Load(object sender, System.EventArgs e)
        {
            if (m_fileInfo != null)
            {
                progressBarAdv1.Value = 0;

                m_zipThread = new Thread(ZipStart);
                m_zipThread.IsBackground = true;
                General.SetThreadName(m_zipThread, "ZipThread");
                m_zipThread.Priority = ThreadPriority.Normal;

                //buttonStart.Enabled = false;
                //buttonClose.Enabled = false;

                var x = Handle; //Ofer: Force handle creation. Fix System.InvalidOperationException. Message: Invoke or BeginInvoke cannot be called on a control until the window handle has been created.
                m_zipThread.Start();
            }
        }

        private void buttonStart_Click(object sender, System.EventArgs e)
        {
            //ExceptionPublisherLog4Net.FormActionLog("buttonStart", Name, null);

            //if (m_zipThread != null)
            //{
            //    buttonStart.Enabled = false;
            //    buttonClose.Enabled = false;
            //    m_zipThread.Start();
            //}
        }

        private void ZipStart()
        {
            ExceptionPublisherLog4Net.TraceLog("ZipStart(): START", Name);
            
            Thread.Sleep(100);

            try
            {
                for (var i = 0; i < m_fileInfo.Length; i++)
                {
                    if (m_backupDir.Length != 0)
                    {
                        // Doing the Zip Operation in the current thread
                        DSP.Dsps.Instance[0].ZipSourceFileName = m_fileInfo[i].FullName;
                        DSP.Dsps.Instance[0].ZipFileName = m_backupDir + @"\" + m_fileInfo[i].Name + ".tmp";
                        DSP.Dsps.Instance[0].DoZipOperation();
                    }
                    else
                    {
                        DSP.Dsps.Instance[0].ZipFileName = GlobalSettings.RawDataDir + m_fileInfo[i].Name;
                        DSP.Dsps.Instance[0].DoZipOperation();
                    }
                    
                    int progress = ((i+1) * 100) / m_fileInfo.Length;
                    progressBarAdv1.BeginInvoke((Action)delegate() {progressBarAdv1.Value = progress;});
                }

                progressBarAdv1.BeginInvoke((Action)delegate() { progressBarAdv1.Value = 100; });

                DialogResult = DialogResult.OK;
            }
            catch (ThreadAbortException)
            {
                ExceptionPublisherLog4Net.TraceLog("ZipStart(): ABORT", Name);
                ExceptionPublisherLog4Net.LogWarn(string.Format("RawData files zip ABORTED. Target folder: '{0}'.", m_backupDir));
                DialogResult = DialogResult.Abort;
            }
            catch (Exception ex)
            {
                ExceptionPublisherLog4Net.ExceptionErrorLog(ex, string.Format("RawData files zip error. Target folder: '{0}'.", m_backupDir));
                DialogResult = DialogResult.Cancel;
            }

            //BeginInvoke((Action)delegate() {buttonClose.Enabled = true;});

            BeginInvoke((Action)Close);
            
            ExceptionPublisherLog4Net.TraceLog("ZipStart(): END", Name);
        }
    }
}
