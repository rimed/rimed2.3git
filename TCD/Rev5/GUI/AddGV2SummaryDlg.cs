using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using TCD2003.Utils;

namespace TCD2003.GUI
{
	/// <summary>
	/// Summary description for AddGV2SummaryDlg.
	/// </summary>
	public partial class AddGV2SummaryDlg : Form
	{
		private GlobalTypes.TAdd2SummaryScreen2 m_add2SummaryScreen;

		public GlobalTypes.TAdd2SummaryScreen2 Add2SummaryScreen
		{
			get
            {
                return m_add2SummaryScreen;
            }
		}

		public AddGV2SummaryDlg()
		{
			this.InitializeComponent();
            var strRes = ResourceManagerWrapper.GetWrapper(MainForm.ResourceManager); 

            this.lblMessage.Text = strRes.GetString("You have already one or more tests for this BV and depth, What do you like to do with the new one?");
            this.radioButtonReplaceOld.Text = strRes.GetString("Replace old");
            this.radioButtonDiscardNew.Text = strRes.GetString("Discard new");
            this.radioButtonAddNew.Text = strRes.GetString("Add new");
            this.buttonOK.Text = strRes.GetString("OK");
            this.buttonHelp.Text = strRes.GetString("Help");
            this.Text = strRes.GetString("Save New test");
            
            this.BackColor = GlobalSettings.BackgroundDlg;
		}

		private void AddGV2SummaryDlg_Load(object sender, System.EventArgs e)
		{
			this.buttonOK.Enabled = false;
		}

		private void radioButtonDiscardNew_CheckedChanged(object sender, System.EventArgs e)
		{
			this.m_add2SummaryScreen = GlobalTypes.TAdd2SummaryScreen2.DiscardNew;
			this.buttonOK.Enabled = true;		
		}

		private void radioButtonAddNew_CheckedChanged(object sender, System.EventArgs e)
		{
            this.m_add2SummaryScreen = GlobalTypes.TAdd2SummaryScreen2.AddNew;
			this.buttonOK.Enabled = true;
		}

		private void radioButtonReplaceOld_CheckedChanged(object sender, System.EventArgs e)
		{
            this.m_add2SummaryScreen = GlobalTypes.TAdd2SummaryScreen2.ReplaceOld;
			this.buttonOK.Enabled = true;
		}
	}
}
