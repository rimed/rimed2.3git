using System;
using System.Collections.Generic;
using System.Windows.Forms;

using TCD2003.DB;
using TCD2003.Utils;

namespace TCD2003.GUI
{
    /// <summary>
    /// Summary description for PatientDeleteDlg.
    /// </summary>
    public partial class PatientDeleteDlg : Form
    {
        public PatientDeleteDlg()
        {
            this.InitializeComponent();
            var strRes = ResourceManagerWrapper.GetWrapper(MainForm.ResourceManager); 
            this.columnHeaderName.Text = strRes.GetString("Patient Name");
            this.columnHeaderID.Text = strRes.GetString("Patient ID");
            this.buttonDelete.Text = strRes.GetString("Delete");
            this.buttonCancel.Text = strRes.GetString("Close");
            this.buttonHelp.Text = strRes.GetString("Help");
            this.Text = strRes.GetString("Delete Patient");

            this.BackColor = GlobalSettings.BackgroundDlg;

            int widthCtrl = this.listView1.Width;
            this.listView1.Columns[0].Width = (int)(widthCtrl / 2);
            this.listView1.Columns[1].Width = widthCtrl - this.listView1.Columns[0].Width;
        }

        private void PatientDeleteDlg_Load(object sender, System.EventArgs e)
        {
            // fill the combobox.
            String select = "Select tb_Patient.* FROM tb_Patient WHERE (Deleted = false AND First_Name <> 'Undefined')";
            dsPatient dsP = RimedDal.Instance.GetPatients(select);

            foreach (dsPatient.tb_PatientRow row in dsP.tb_Patient.Rows)
            {
                ListViewItem item = this.listView1.Items.Add(row.Last_Name + " " + row.First_Name);
                item.SubItems.Add((String)row.ID);
                item.Tag = (Guid)row.Patient_Index;
            }
        }

        private void buttonDelete_Click(object sender, System.EventArgs e)
        {
            System.Windows.Forms.ListView.SelectedIndexCollection sc = this.listView1.SelectedIndices;
            if (sc.Count == 0)
            {
                MessageBox.Show(MainForm.ResourceManager.GetString("No patient was selected!"),
                    MainForm.ResourceManager.GetString("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);

                return;
            }

            List<LogField> fields = new List<LogField>();
            fields.Add(new LogField("listView1", this.listView1.Items[sc[0]].Text));
            ExceptionPublisherLog4Net.FormActionLog("buttonDelete", this.Name, fields);

            if (MainForm.Instance.SelectedPatientIndex == ((Guid)this.listView1.Items[sc[0]].Tag))
            {
                MessageBox.Show(MainForm.ResourceManager.GetString("You are trying to delete the current patient, please change the current patient and try again."),
                    MainForm.ResourceManager.GetString("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (MessageBox.Show(MainForm.ResourceManager.GetString("Are you sure you want to delete the folowing patients:") +
                "\n" + this.listView1.Items[sc[0]].Text,
                MainForm.ResourceManager.GetString("Warning"), MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            {
                // delete from the database.
                RimedDal.Instance.DeletePatient((Guid)this.listView1.Items[sc[0]].Tag);

                // delete from the list view.
                this.listView1.Items.Remove(this.listView1.Items[sc[0]]);
                RimedDal.Instance.UpdatePatientsToDB();
            }
        }

        /// <summary>
        /// Deny delete test patient (issue RIMD-409)
        /// </summary>
        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListView.SelectedIndexCollection sc = this.listView1.SelectedIndices;
            this.buttonDelete.Enabled = sc.Count == 0 || ((Guid)this.listView1.Items[sc[0]].Tag != Guid.Empty);
        }
    }
}
