using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

using TCD2003.DB;
using TCD2003.Utils;

namespace TCD2003.GUI
{
    /// <summary>
    /// Summary description for AuthorizationMgrDlg:  
    /// in charge of the authorization (Which studies the user can see). Works together with the
    /// Authorization manager tool
    /// </summary>
    public partial class AuthorizationMgrDlg : Form
    {
        #region Constants

        private const int VALIDATION_a_z = 219;
        private const int VALIDATION_A_Z = 155;
        private const int VALIDATION_0_9 = 105;

        private const int VALID1_POS = 0;
        private const int MONITORING_POS = 1;
        private const int AUTOSCAN_POS = 2;
        private const int HITS_POS = 3;
        private const int PROBE8MHZ_POS = 4;
        private const int Extracranial_POS = 5;
        private const int IntracraniaUnilateral_POS = 6;
        private const int IntracranialBilateral_POS = 7;
        private const int SEPERATOR1_POS = 8;

        private const int Peripheral_POS = 9;
        private const int MultifrequencyTwoProbes_POS = 10;
        private const int Intraoperative_POS = 11;
        private const int MExtracranial_POS = 12;
        private const int SEPERATOR2_POS = 13;

        private const int MIntracranialUnilateral_POS = 14;
        private const int MIntracranialBilateral_POS = 15;
        private const int PROBE1MHZ_POS = 16;
        private const int MPeripheral_POS = 17;
        private const int SEPERATOR3_POS = 18;

        private const int MMultifrequencyTwoProbes_POS = 19;
        private const int MIntraoperative_POS = 20;
        private const int PROBE16MHZ_POS = 21;
        private const int VMRCO2Reactivity_POS = 22;
        private const int SEPERATOR4_POS = 23;

        private const int VMRDiamoxtest_POS = 24;
        private const int MonitoringOftPA_POS = 25;
        private const int EvokedflowBilateraltest_POS = 26;
        private const int SerialNumber1_POS = 27;
        private const int VMRBilateralDiamoxtest_POS = 28;
        private const int Dicom_POS = 29;
        private const int PFOTest_POS = 30;
        //private const int VALID5_POS					= 31;
        private const int SerialNumber2_POS = 31;
        private const int Evokedflowtest_POS = 32;
        private const int ProbesNum_POS = 33;
        private const int VALID7_POS = 34;
        private const int VALID8_POS = 35;
        private const int VALID9_POS = 36;
        
        #endregion

        public AuthorizationMgrDlg()
        {
            this.InitializeComponent();
            this.BackColor = GlobalSettings.BackgroundDlg;
        }

        public bool OkButtonPressed { get; private set; }

        private void buttonOK_Click(object sender, System.EventArgs e)
        {
            List<LogField> fields = new List<LogField>();
            fields.Add(new LogField("textBox1", textBox1.Text));
            fields.Add(new LogField("textBox2", textBox2.Text));
            fields.Add(new LogField("textBox3", textBox3.Text));
            fields.Add(new LogField("textBox4", textBox4.Text));
            fields.Add(new LogField("textBox5", textBox5.Text));
            fields.Add(new LogField("textBox6", textBox6.Text));
            fields.Add(new LogField("textBox7", textBox7.Text));
            fields.Add(new LogField("textBox8", textBox8.Text));
            fields.Add(new LogField("textBox9", textBox9.Text));
            fields.Add(new LogField("textBox10", textBox10.Text));
            fields.Add(new LogField("textBoxSerialNumber", textBoxSerialNumber.Text));
            ExceptionPublisherLog4Net.FormActionLog("buttonOK", this.Name, fields);
            // get the system key and the product key from the controls.
            String sysKey = textBox6.Text + "-" + textBox7.Text + "-" + textBox8.Text + "-" + textBox9.Text + "-" + textBox10.Text;
            String productKey = textBox1.Text + "-" + textBox2.Text + "-" + textBox3.Text + "-" + textBox4.Text + "-" + textBox5.Text;

            CalculatePermission(sysKey, productKey);
            RimedDal.Instance.s_dsAuthorizationMgr.tb_AuthorizationMgr[0].ProductKey = productKey;
            RimedDal.Instance.s_dsAuthorizationMgr.tb_AuthorizationMgr[0].SystemKey = sysKey;

            RimedDal.Instance.UpdateAuthorizationMgr();
            this.OkButtonPressed = true;
        }

        static public bool CheckSerialNumber()
        {
            try
            {
                Utils.VolumeInfo info = Utils.VolumeInfo.CurrentVolume();
                uint serialNumber = info.SerialNumber;

                int n1 = (int)(serialNumber / 854127) % 10;
                int n2 = (int)(serialNumber / 54127) % 10;

                String productKey = RimedDal.Instance.s_dsAuthorizationMgr.tb_AuthorizationMgr[0].ProductKey;
                String str1 = Convert.ToString(productKey[SerialNumber1_POS]);
                String str2 = Convert.ToString(productKey[SerialNumber2_POS]);
                return str1.Equals(n1.ToString()) && str2.Equals(n2.ToString());
            }
            catch (Exception ex)
            {
                ExceptionPublisherLog4Net.ExceptionErrorLog(ex, "Error in authorization message");
                TopMostMessageBox.Show(MainForm.ResourceManager.GetString("Error in authorization message"));
            }
            return true;
        }

        private void AuthorizationMgrDlg_Load(object sender, System.EventArgs e)
        {
            var strRes = ResourceManagerWrapper.GetWrapper(MainForm.ResourceManager);   

            this.lblProductKey.Text = strRes.GetString("Product Key:");
            this.lblSystemKey.Text = strRes.GetString("System Key:");
            this.buttonCancel.Text = strRes.GetString("Close");
            this.buttonOK.Text = strRes.GetString("Apply");
            this.labelHD.Text = strRes.GetString("HD Serial Number:");
            this.Text = strRes.GetString("Authorization Manager");

            this.OkButtonPressed = false;
            String sysKey;
            try
            {
                sysKey = RimedDal.Instance.s_dsAuthorizationMgr.tb_AuthorizationMgr[0].SystemKey;
            }
            catch (Exception ex)
            {
                ExceptionPublisherLog4Net.ExceptionFatalLog(ex);
                return;
            }

            this.FillSystemKey(sysKey);

            String productKey;
            try
            {
                productKey = RimedDal.Instance.s_dsAuthorizationMgr.tb_AuthorizationMgr[0].ProductKey;
                this.FillProductKey(productKey);

                Utils.VolumeInfo info = Utils.VolumeInfo.CurrentVolume();
                this.textBoxSerialNumber.Text = info.SerialNumber.ToString();
            }
            catch (Exception ex)
            {
                ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
                // if the product key does not exist in the database - do nothing.
            }
        }
        private void CalculatePermission(String sysKey, String proKey)
        {
            byte validation = 0;
            int enable = 0;

            // get String exception.
            String errStr = MainForm.ResourceManager.GetString("No valid system.");

            for (int i = 0; i < sysKey.Length; i++)
            {
                if (sysKey[i] != '-')
                {
                    if (sysKey[i] >= 97 && sysKey[i] <= 122)		// a..z
                        validation = VALIDATION_a_z;
                    else if (sysKey[i] >= 65 && sysKey[i] <= 90)	// A..Z
                        validation = VALIDATION_A_Z;
                    else if (sysKey[i] >= 48 && sysKey[i] <= 57)	// 0..1
                        validation = VALIDATION_0_9;

                    enable = sysKey[i] + proKey[i] - validation;

                    if (i == VALID1_POS && enable != 1 ||
                        (i == VALID7_POS || i == VALID8_POS || i == VALID9_POS) && enable != 0)
                    {
                        try
                        {
                            throw new ApplicationException(errStr);
                        }
                        catch (ApplicationException ex)
                        {
                            ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
                            throw;
                        }
                    }

                    else if (i == MONITORING_POS)
                        RimedDal.Instance.s_dsAuthorizationMgr.tb_AuthorizationMgr[0].AMmonitoring = (enable == 1);

                    else if (i == AUTOSCAN_POS)
                        RimedDal.Instance.s_dsAuthorizationMgr.tb_AuthorizationMgr[0].AMautoscan = (enable == 1);

                    else if (i == HITS_POS)
                        RimedDal.Instance.s_dsAuthorizationMgr.tb_AuthorizationMgr[0].AMhits = (enable == 1);

                    else if (i == PROBE8MHZ_POS)
                        RimedDal.Instance.EnableProbeAppearance(8, (enable == 1));
                    else if (i == PROBE1MHZ_POS)
                        RimedDal.Instance.EnableProbeAppearance(1, (enable == 1));
                    else if (i == PROBE16MHZ_POS)
                        RimedDal.Instance.EnableProbeAppearance(16, (enable == 1));

                    else if (i == Extracranial_POS)
                        RimedDal.Instance.EnableStudyAppearance("Extracranial", false, (enable == 1));

                    else if (i == IntracraniaUnilateral_POS)
                        RimedDal.Instance.EnableStudyAppearance("Intracranial Unilateral", false, (enable == 1));
                    else if (i == IntracranialBilateral_POS)
                        RimedDal.Instance.EnableStudyAppearance("Intracranial Bilateral", false, (enable == 1));
                    else if (i == Peripheral_POS)
                        RimedDal.Instance.EnableStudyAppearance("Peripheral", false, (enable == 1));
                    else if (i == MultifrequencyTwoProbes_POS)
                        RimedDal.Instance.EnableStudyAppearance("Multifrequency Two Probes", false, (enable == 1));
                    else if (i == Intraoperative_POS)
                        RimedDal.Instance.EnableStudyAppearance("Intraoperative", false, (enable == 1));
                    else if (i == MExtracranial_POS)
                        RimedDal.Instance.EnableStudyAppearance("Extracranial", true, (enable == 1));
                    else if (i == MIntracranialUnilateral_POS)
                        RimedDal.Instance.EnableStudyAppearance("Monitoring Intracranial Unilateral", true, (enable == 1));
                    else if (i == MIntracranialBilateral_POS)
                        RimedDal.Instance.EnableStudyAppearance("Monitoring Intracranial Bilateral", true, (enable == 1));
                    else if (i == MPeripheral_POS)
                        RimedDal.Instance.EnableStudyAppearance("Peripheral", true, (enable == 1));
                    else if (i == MMultifrequencyTwoProbes_POS)
                        RimedDal.Instance.EnableStudyAppearance("Multifrequency Two Probes", true, (enable == 1));
                    else if (i == MIntraoperative_POS)
                        RimedDal.Instance.EnableStudyAppearance("Intraoperative", true, (enable == 1));
                    else if (i == VMRCO2Reactivity_POS)
                        RimedDal.Instance.EnableStudyAppearance("VMR CO2 Reactivity", true, (enable == 1));
                    else if (i == VMRDiamoxtest_POS)
                    {
                        RimedDal.Instance.EnableStudyAppearance("VMR", true, (enable == 1));
                    }
                    else if (i == MonitoringOftPA_POS)
                        RimedDal.Instance.EnableStudyAppearance("Monitoring Of tPA", true, (enable == 1));
                    //					else if(i == Monitoring)	
                    //						RimedDal.Instance.EnableStudyAppearance("Monitoring Of tPA", true, (enable == 1));
                    //					else if(i == MonitoringOftPA_POS)	
                    //						RimedDal.Instance.EnableStudyAppearance("Monitoring Of tPA", true, (enable == 1));
                    else if (i == EvokedflowBilateraltest_POS)
                        RimedDal.Instance.EnableStudyAppearance("Evoked Flow Bilateral", true, (enable == 1));
                    else if (i == VMRBilateralDiamoxtest_POS)
                        RimedDal.Instance.EnableStudyAppearance("VMR Bilateral", true, (enable == 1));
                    else if (i == Dicom_POS)
                        RimedDal.Instance.s_dsAuthorizationMgr.tb_AuthorizationMgr[0].Dicom = (enable == 1);
                    else if (i == PFOTest_POS)
                        RimedDal.Instance.EnableStudyAppearance("PFO Test", false, (enable == 1));
                    else if (i == Evokedflowtest_POS)
                    {
                        RimedDal.Instance.EnableStudyAppearance("Evoked Flow Unilateral", true, (enable == 1));
                    }
                    else if (i == ProbesNum_POS)
                        RimedDal.Instance.s_dsAuthorizationMgr.tb_AuthorizationMgr[0].ProbesNum = (byte)(sysKey[i] + proKey[i] - validation);
                }
                else
                    if (i != SEPERATOR1_POS && i != SEPERATOR2_POS && i != SEPERATOR3_POS && i != SEPERATOR4_POS)
                    {
                        try
                        {
                            throw new ApplicationException(errStr);
                        }
                        catch (ApplicationException ex)
                        {
                            ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
                            throw;
                        }
                    }
            }
        }

        private void buttonCancel_Click(object sender, System.EventArgs e)
        {
            ExceptionPublisherLog4Net.FormActionLog("buttonCancel", this.Name, null);
            this.OkButtonPressed = false;
        }

        private void btnPasteSystemKey_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            this.btnPasteSystemKey.BorderStyle = BorderStyle.Fixed3D;
        }

        private void btnPasteProductKey_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            this.btnPasteProductKey.BorderStyle = BorderStyle.Fixed3D;
        }

        private void btnPasteProductKey_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            this.btnPasteProductKey.BorderStyle = BorderStyle.None;
            IDataObject dataObj = Clipboard.GetDataObject();
            try
            {
                string productKey = dataObj.GetData(DataFormats.Text, false).ToString();
                this.FillProductKey(productKey);
            }
            catch (Exception ex)
            {
                ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
                this.textBox1.Text = textBox2.Text = textBox3.Text = textBox4.Text = textBox5.Text = "";
            }
        }

        private void btnPasteSystemKey_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            this.btnPasteSystemKey.BorderStyle = BorderStyle.None;
            IDataObject dataObj = Clipboard.GetDataObject();
            try
            {
                string sysKey = dataObj.GetData(DataFormats.Text, false).ToString();
                this.FillSystemKey(sysKey);
            }
            catch (Exception ex)
            {
                ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
                this.textBox6.Text = textBox7.Text = textBox8.Text = textBox9.Text = textBox10.Text = "";
            }
        }

        private void FillSystemKey(string systemKey)
        {
            String[] tmpArr = systemKey.Split('-');
            this.textBox6.Text = tmpArr[0];
            this.textBox7.Text = tmpArr[1];
            this.textBox8.Text = tmpArr[2];
            this.textBox9.Text = tmpArr[3];
            this.textBox10.Text = tmpArr[4];
        }

        private void FillProductKey(string productKey)
        {
            String[] tmpArr = productKey.Split('-');
            this.textBox1.Text = tmpArr[0];
            this.textBox2.Text = tmpArr[1];
            this.textBox3.Text = tmpArr[2];
            this.textBox4.Text = tmpArr[3];
            this.textBox5.Text = tmpArr[4];
        }

        private void AuthorizationMgrDlg_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            e.Handled = true;
            switch (e.KeyCode)
            {
                case Keys.V:
                    if (e.Control)
                    {
                        IDataObject dataObj = Clipboard.GetDataObject();
                        string key = dataObj.GetData(DataFormats.Text, false).ToString();

                        if (this.textBox6.Focused ||
                            this.textBox7.Focused ||
                            this.textBox8.Focused ||
                            this.textBox9.Focused ||
                            this.textBox10.Focused)
                        {
                            try
                            {
                                this.FillSystemKey(key);
                            }
                            catch (Exception ex)
                            {
                                ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
                                this.textBox6.Text = textBox7.Text = textBox8.Text = textBox9.Text = textBox10.Text = "";
                            }
                        }

                        if (this.textBox1.Focused ||
                            this.textBox2.Focused ||
                            this.textBox3.Focused ||
                            this.textBox4.Focused ||
                            this.textBox5.Focused)
                        {
                            try
                            {
                                this.FillProductKey(key);
                            }
                            catch (Exception ex)
                            {
                                ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
                                this.textBox1.Text = textBox2.Text = textBox3.Text = textBox4.Text = textBox5.Text = "";
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
