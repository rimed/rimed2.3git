using System;
using System.Drawing;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

using TCD2003.DB;
using TCD2003.Utils;

namespace TCD2003.GUI
{
    /// <summary>
    /// Summary description for BVRenameDlg.
    /// </summary>
    public partial class BVRenameDlg : System.Windows.Forms.Form
    {
        private Guid[] guidTable;
        public BVRenameDlg()
            : this(true, 0, String.Empty, String.Empty)
        {
        }

        private bool m_SingleList = true;
        public BVRenameDlg(bool single, int side, String rxProbeName, string name)
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            var strRes = ResourceManagerWrapper.GetWrapper(MainForm.ResourceManager); 
            this.Text = strRes.GetString("Rename BV");
            this.lblBVName.Text = strRes.GetString("Blood Vessel Name") + ":";
            this.radioButtonLeft.Text = strRes.GetString("Left");
            this.radioButtonRight.Text = strRes.GetString("Right");
            this.radioButtonMidle.Text = strRes.GetString("Middle");
            this.buttonOK.Text = strRes.GetString("OK");
            this.buttonCncel.Text = strRes.GetString("Cancel");

            this.BackColor = GlobalSettings.BackgroundDlg;

            m_SingleList = single;

            dsBVSetup2.Merge(RimedDal.Instance.s_dsBVSetup);

            // fill dsSetup1
            bool bvExist = false;
            dsBVSetup1.Clear();
            for (int i = 0; i < dsBVSetup2.tb_BloodVesselSetup.Rows.Count; i++)
            {
                DB.dsBVSetup.tb_BloodVesselSetupRow row2 = (DB.dsBVSetup.tb_BloodVesselSetupRow)this.dsBVSetup2.tb_BloodVesselSetup.Rows[i];
                for (int q = 0; q < dsBVSetup1.tb_BloodVesselSetup.Rows.Count; q++)
                {
                    DB.dsBVSetup.tb_BloodVesselSetupRow row1 = (DB.dsBVSetup.tb_BloodVesselSetupRow)this.dsBVSetup1.tb_BloodVesselSetup.Rows[q];

                    if (((String)row2["Name"]) == ((String)row1["Name"]))
                    {
                        bvExist = true;
                        if ((((GlobalTypes.ESide)row1["Side"]) == GlobalTypes.ESide.Left && ((GlobalTypes.ESide)row2["Side"]) == GlobalTypes.ESide.Right)
                            || (((GlobalTypes.ESide)row2["Side"]) == GlobalTypes.ESide.Left && ((GlobalTypes.ESide)row1["Side"]) == GlobalTypes.ESide.Right))
                        {
                            row1["Side"] = (int)GlobalTypes.ESide.BothSides;
                        }
                        continue;
                    }
                }
                if (!bvExist)
                {
                    DB.dsBVSetup.tb_BloodVesselSetupRow row = (DB.dsBVSetup.tb_BloodVesselSetupRow)dsBVSetup1.tb_BloodVesselSetup.NewRow();
                    row["Blood_Vessel_Index"] = row2["Blood_Vessel_Index"];
                    row["Name"] = row2["Name"];
                    row["Probe"] = row2["Probe"];
                    row["Depth"] = row2["Depth"];
                    row["Gain"] = row2["Gain"];
                    row["Thump"] = row2["Thump"];
                    row["Width"] = row2["Width"];
                    row["Width"] = row2["Width"];
                    row["Power"] = row2["Power"];
                    row["MinFrequency"] = row2["MinFrequency"];
                    row["MaxFrequency"] = row2["MaxFrequency"];
                    row["Units"] = row2["Units"];
                    row["ZeroLinePosition"] = row2["ZeroLinePosition"];
                    row["DirectionToProbe"] = row2["DirectionToProbe"];
                    row["Comments"] = row2["Comments"];
                    row["DisplayPeakFwdEnvelope"] = row2["DisplayPeakFwdEnvelope"];
                    row["DisplayPeakBckEnvelope"] = row2["DisplayPeakBckEnvelope"];
                    row["DisplayMeanFwdEnvelope"] = row2["DisplayMeanFwdEnvelope"];
                    row["DisplayMeanBckEnvelope"] = row2["DisplayMeanBckEnvelope"];
                    row["DisplaySecptrum"] = row2["DisplaySecptrum"];
                    row["HitsThreshold"] = row2["HitsThreshold"];
                    row["Angle"] = row2["Angle"];
                    row["FreqRang"] = row2["FreqRang"];
                    row["FreqRang"] = row2["FreqRang"];
                    row["Side"] = row2["Side"];
                    row["Location"] = row2["Location"];
                    dsBVSetup1.tb_BloodVesselSetup.Addtb_BloodVesselSetupRow(row);
                }
                bvExist = false;
            }

            this.comboBox1.Items.Clear();

            List<Guid> al = new List<Guid>();


            String strName = String.Empty;

            for (int i = 0; i < dsBVSetup2.tb_BloodVesselSetup.Rows.Count; i++)
            {
                DB.dsBVSetup.tb_BloodVesselSetupRow row = (DB.dsBVSetup.tb_BloodVesselSetupRow)this.dsBVSetup2.tb_BloodVesselSetup.Rows[i];
                strName = (String)row["Name"];

                //RIMD-352: No list of BV names for renaming in Extracranial and Intraoperative study
                //
                Guid guid = (Guid)row["Blood_Vessel_Index"];
                if (!BVListViewCtrl.sInstance.IsContained(guid))
                    continue;

                if (single)
                    if (side != (int)row["Side"])
                        if (!((name == "VA" && strName == "BA") || (name == "BA" && strName == "VA")))
                            continue;

                switch ((int)row["Side"])
                {
                    case (int)GlobalTypes.ESide.Left:
                        strName += "-L";
                        break;
                    case (int)GlobalTypes.ESide.Right:
                        strName += "-R";
                        break;
                    default:
                        break;
                }

                al.Add((Guid)row["Blood_Vessel_Index"]);
                this.comboBox1.Items.Add(strName);
            }

            guidTable = new Guid[al.Count];
            for (int i = 0; i < al.Count; ++i)
            {
                guidTable[i] = al[i];
            }
            this.comboBox1.Select(0, 1);
        }

        private GlobalTypes.ESide m_Side;

        private void radioButtonLeft_CheckedChanged(object sender, System.EventArgs e)
        {
            m_Side = GlobalTypes.ESide.Left;
            this.buttonOK.Enabled = true;
        }

        private void radioButtonRight_CheckedChanged(object sender, System.EventArgs e)
        {
            m_Side = GlobalTypes.ESide.Right;
            this.buttonOK.Enabled = true;
        }

        private void radioButtonMidle_CheckedChanged(object sender, System.EventArgs e)
        {
            m_Side = GlobalTypes.ESide.Middle;
            this.buttonOK.Enabled = true;
        }

        private void BVRenameDlg_Load(object sender, System.EventArgs e)
        {

        }

        public GlobalTypes.ESide BVSide
        {
            get { return m_Side; }
        }

        private String m_BVName;

        private void buttonOK_Click(object sender, System.EventArgs e)
        {
            String strBV = this.comboBox1.Text;
            List<LogField> fields = new List<LogField>();
            fields.Add(new LogField("comboBox1", comboBox1.Text));
            ExceptionPublisherLog4Net.FormActionLog("buttonOK", this.Name, fields);

            if (this.m_SingleList)
            {
                if (strBV.Length > 2)
                    m_BVName = strBV.Remove(strBV.Length - 2, 2);
                else
                    m_BVName = strBV;
                m_Guid = guidTable[this.comboBox1.SelectedIndex];
                if (strBV.EndsWith("-L"))
                {
                    m_Side = GlobalTypes.ESide.Left;
                }
                else
                {
                    if (strBV.EndsWith("-R"))
                    {
                        m_Side = GlobalTypes.ESide.Right;
                    }
                    else
                        m_Side = GlobalTypes.ESide.Middle;
                }
            }
            else
            {
                if (strBV.Length > 2)
                    strBV = strBV.Remove(strBV.Length - 2, 2);
                m_BVName = strBV;

                strBV = strBV + "-L";
                //Found the two BV guids
                for (int i = 0; i < guidTable.Length; ++i)
                {
                    if (strBV.Equals((String)this.comboBox1.Items[i]))
                    {
                        m_Guid = guidTable[i];
                        break;
                    }
                }

                strBV = m_BVName + "-R";
                for (int i = 0; i < guidTable.Length; ++i)
                {
                    if (strBV.Equals((String)this.comboBox1.Items[i]))
                    {
                        m_Guid2 = guidTable[i];
                        break;
                    }
                }
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            this.buttonOK.Enabled = true;
        }

        public Guid m_Guid = Guid.Empty;
        public Guid m_Guid2 = Guid.Empty;
        public Guid Blood_Vessel_Index
        {
            get { return m_Guid; }
        }
        public Guid Blood_Vessel_Index2
        {
            get { return m_Guid2; }
        }
        public String BVName
        {
            get { return m_BVName; }
        }
    }
}
