using Syncfusion.Windows.Forms.Tools;

namespace TCD2003.GUI
{
    partial class MainForm
    {
        protected override void Dispose(bool disposing)
        {
            //if(logInfo!=null)
            //	logInfo.Stop();
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.panelMainCharts = new System.Windows.Forms.Panel();
            this.panelTrend = new System.Windows.Forms.Panel();
            this.splitterBottom = new System.Windows.Forms.Splitter();
            this.panelRightCharts = new System.Windows.Forms.Panel();
            this.panel0Charts = new System.Windows.Forms.Panel();
            this.panel1Charts = new System.Windows.Forms.Panel();
            this.panel0Trend = new System.Windows.Forms.Panel();
            this.panel1Trend = new System.Windows.Forms.Panel();
            this.splashPanel4 = new Syncfusion.Windows.Forms.Tools.SplashPanel();
            this.button6 = new System.Windows.Forms.Button();
            this.labelSplash = new System.Windows.Forms.Label();
            this.panelMiddle = new System.Windows.Forms.Panel();
            this.splitterBottom2 = new System.Windows.Forms.Splitter();
            this.panelAutoscan0 = new System.Windows.Forms.Panel();
            this.panel0 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelAutoscan1 = new System.Windows.Forms.Panel();
            this.dockingManager1 = new Syncfusion.Windows.Forms.Tools.DockingManager(this.components);
            this.panelHits = new System.Windows.Forms.Panel();
            this.hitsHistogram1 = new TCD2003.GUI.HitsHistogram();
            this.panelBV = new System.Windows.Forms.Panel();
            this.bvListViewCtrl1 = new TCD2003.GUI.BVListViewCtrl();
            this.panelEvents = new System.Windows.Forms.Panel();
            this.eventListViewCtrl1 = new TCD2003.GUI.EventListViewCtrl();
            this.panel2 = new System.Windows.Forms.Panel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.panelFFT = new System.Windows.Forms.Panel();
            this.panelFFTEnergy = new System.Windows.Forms.Panel();
            this.labelE2 = new System.Windows.Forms.Label();
            this.labelE1 = new System.Windows.Forms.Label();
            this.label = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.dockingClientPanel1 = new Syncfusion.Windows.Forms.Tools.DockingClientPanel();
            this.splitterLeft = new System.Windows.Forms.Splitter();
            this.splitterRight = new System.Windows.Forms.Splitter();
            this.panelRight = new System.Windows.Forms.Panel();
            this.panelHITS2 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panelHeaderRight = new System.Windows.Forms.Panel();
            this.comboBoxTimeRight = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelLeftCharts = new System.Windows.Forms.Panel();
            this.panelHeaderLeft = new System.Windows.Forms.Panel();
            this.comboBoxTimeLeft = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.mainFrameBarManager1 = new Syncfusion.Windows.Forms.Tools.XPMenus.MainFrameBarManager(this.components, this);
            this.barMainMenu = new Syncfusion.Windows.Forms.Tools.XPMenus.Bar(this.mainFrameBarManager1, "MainMenu");
            this.parentBarItem1 = new Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem();
            this.barItemNewSpectrum = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemNewTrend = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemSaveStudy = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.imageListMenu = new System.Windows.Forms.ImageList(this.components);
            this.barItemDeleteStudy = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemUnFreeze = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItem1 = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.BarItem_UpZeroLine = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItem_DownZeroLine = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.SaveDebugData = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemSaveDebugFFT = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.parentBarItemPatient = new Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem();
            this.barItemPatientNew = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemPatientLoad = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemPatientSearch = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemPatientDelete = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemPatientExport = new Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem();
            this.barItemExportSelectedSpectrum = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemExportFullScreen = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemExportTrends = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemExportLog = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemBackup = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemRestore = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemPatientPrintPreview = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemPrint = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemExit = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemExitWindows = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.parentBarItemStudies = new Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem();
            this.parentBarItemSetup = new Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem();
            this.barItemPatientReportWizard = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemSetupGeneral = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemSetupStudies = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.parentBarItemFunctions = new Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem();
            this.barItemNextFunction = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemAutoscan = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemCursors = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemNotes = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemPatientReport = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemHitsDetection = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemAddEvent = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemDeleteEvent = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemNextBV = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemPrevBV = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemSummaryScreen = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemRecord = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemStop = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemPlay = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemExpandClinicalParameters = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.parentBarItemUtilities = new Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem();
            this.barItemAuthorizationMgr = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemCancelUtilitiesMenu = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemAdminExitWindows = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.parentBarItemHelp = new Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem();
            this.barItemHelpContent = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemHelpIndex = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemHelpSearch = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemHelpWhatIsThis = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemHelpAbout = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.parentBarItem2 = new Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem();
            this.barItemDepthDown = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemDepthUp = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemGainUp = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemGainDown = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.commandBar1 = new Syncfusion.Windows.Forms.Tools.CommandBar();
            this.toolBarPanel1 = new TCD2003.GUI.ToolBarPanel();
            this.barItemDebug = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemFreqDown = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemFreqUp = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemPowerDown = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemPowerUp = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemWidthDown = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemWidthUp = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemThumpDown = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemThumpUp = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemPanelHits = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItem2 = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemSave = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemExportToFile = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemZipRawDataFiles = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItem4 = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.imageListToolBar = new System.Windows.Forms.ImageList(this.components);
            this.timerPanels = new System.Windows.Forms.Timer(this.components);
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.cachedcrTableRep1 = new TCD2003.ReportMgr.CachedcrTableRep();
            this.splashPanel4.SuspendLayout();
            this.panelMiddle.SuspendLayout();
            this.panel0.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dockingManager1)).BeginInit();
            this.panelHits.SuspendLayout();
            this.panelBV.SuspendLayout();
            this.panelEvents.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panelFFT.SuspendLayout();
            this.panelFFTEnergy.SuspendLayout();
            this.dockingClientPanel1.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panelHITS2.SuspendLayout();
            this.panelHeaderRight.SuspendLayout();
            this.panelLeft.SuspendLayout();
            this.panelHeaderLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainFrameBarManager1)).BeginInit();
            this.commandBar1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelMainCharts
            // 
            this.panelMainCharts.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.panelMainCharts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMainCharts.Location = new System.Drawing.Point(0, 0);
            this.panelMainCharts.Name = "panelMainCharts";
            this.panelMainCharts.Size = new System.Drawing.Size(0, 482);
            this.panelMainCharts.TabIndex = 0;
            // 
            // panelTrend
            // 
            this.panelTrend.AutoScroll = true;
            this.panelTrend.BackColor = System.Drawing.Color.Magenta;
            this.panelTrend.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelTrend.Location = new System.Drawing.Point(0, 588);
            this.panelTrend.Name = "panelTrend";
            this.panelTrend.Size = new System.Drawing.Size(0, 100);
            this.panelTrend.TabIndex = 1;
            // 
            // splitterBottom
            // 
            this.splitterBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitterBottom.Location = new System.Drawing.Point(0, 585);
            this.splitterBottom.Name = "splitterBottom";
            this.splitterBottom.Size = new System.Drawing.Size(0, 3);
            this.splitterBottom.TabIndex = 0;
            this.splitterBottom.TabStop = false;
            // 
            // panelRightCharts
            // 
            this.panelRightCharts.Location = new System.Drawing.Point(0, 29);
            this.panelRightCharts.Name = "panelRightCharts";
            this.panelRightCharts.Size = new System.Drawing.Size(240, 657);
            this.panelRightCharts.TabIndex = 5;
            // 
            // panel0Charts
            // 
            this.panel0Charts.BackColor = System.Drawing.Color.Black;
            this.panel0Charts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel0Charts.Location = new System.Drawing.Point(0, 0);
            this.panel0Charts.Name = "panel0Charts";
            this.panel0Charts.Size = new System.Drawing.Size(498, 588);
            this.panel0Charts.TabIndex = 117;
            // 
            // panel1Charts
            // 
            this.panel1Charts.BackColor = System.Drawing.Color.Gray;
            this.panel1Charts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1Charts.Location = new System.Drawing.Point(0, 0);
            this.panel1Charts.Name = "panel1Charts";
            this.panel1Charts.Size = new System.Drawing.Size(498, 488);
            this.panel1Charts.TabIndex = 114;
            // 
            // panel0Trend
            // 
            this.panel0Trend.AutoScroll = true;
            this.panel0Trend.BackColor = System.Drawing.Color.Magenta;
            this.panel0Trend.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel0Trend.Location = new System.Drawing.Point(0, 588);
            this.panel0Trend.Name = "panel0Trend";
            this.panel0Trend.Size = new System.Drawing.Size(498, 100);
            this.panel0Trend.TabIndex = 1;
            this.panel0Trend.Visible = false;
            // 
            // panel1Trend
            // 
            this.panel1Trend.AutoScroll = true;
            this.panel1Trend.BackColor = System.Drawing.Color.Magenta;
            this.panel1Trend.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1Trend.Location = new System.Drawing.Point(0, 588);
            this.panel1Trend.Name = "panel1Trend";
            this.panel1Trend.Size = new System.Drawing.Size(498, 100);
            this.panel1Trend.TabIndex = 1;
            this.panel1Trend.Visible = false;
            // 
            // splashPanel4
            // 
            this.splashPanel4.AnimationSpeed = 10;
            this.splashPanel4.BackgroundColor = new Syncfusion.Drawing.BrushInfo(Syncfusion.Drawing.GradientStyle.Vertical, new Syncfusion.Drawing.BrushInfoColorArrayList(new System.Drawing.Color[] {
                System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192))))),
                System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))))}));
            this.splashPanel4.Controls.Add(this.button6);
            this.splashPanel4.Controls.Add(this.labelSplash);
            this.splashPanel4.DesktopAlignment = Syncfusion.Windows.Forms.Tools.SplashAlignment.RightBottom;
            this.splashPanel4.DiscreetLocation = new System.Drawing.Point(0, 0);
            this.splashPanel4.Location = new System.Drawing.Point(80, 136);
            this.splashPanel4.Name = "splashPanel4";
            this.splashPanel4.ShowAnimation = true;
            this.splashPanel4.Size = new System.Drawing.Size(160, 152);
            this.splashPanel4.SuspendAutoCloseWhenMouseOver = true;
            this.splashPanel4.TabIndex = 33;
            this.splashPanel4.TimerInterval = 7000;
            this.splashPanel4.Visible = false;
            // 
            // button6
            // 
            this.button6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Image = ((System.Drawing.Image)(resources.GetObject("button6.Image")));
            this.button6.Location = new System.Drawing.Point(120, 8);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(21, 21);
            this.button6.TabIndex = 3;
            // 
            // labelSplash
            // 
            this.labelSplash.BackColor = System.Drawing.Color.Transparent;
            this.labelSplash.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labelSplash.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.labelSplash.ForeColor = System.Drawing.Color.Yellow;
            this.labelSplash.Location = new System.Drawing.Point(16, 48);
            this.labelSplash.Name = "labelSplash";
            this.labelSplash.Size = new System.Drawing.Size(136, 64);
            this.labelSplash.TabIndex = 2;
            this.labelSplash.Text = "You have received a new message";
            this.labelSplash.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panelMiddle
            // 
            this.panelMiddle.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.panelMiddle.Controls.Add(this.panelMainCharts);
            this.panelMiddle.Controls.Add(this.splitterBottom2);
            this.panelMiddle.Controls.Add(this.panelAutoscan0);
            this.panelMiddle.Controls.Add(this.splitterBottom);
            this.panelMiddle.Controls.Add(this.panelTrend);
            this.panelMiddle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMiddle.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panelMiddle.Location = new System.Drawing.Point(240, 0);
            this.panelMiddle.Name = "panelMiddle";
            this.panelMiddle.Size = new System.Drawing.Size(0, 688);
            this.panelMiddle.TabIndex = 2;
            // 
            // splitterBottom2
            // 
            this.splitterBottom2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitterBottom2.Location = new System.Drawing.Point(0, 542);
            this.splitterBottom2.Name = "splitterBottom2";
            this.splitterBottom2.Size = new System.Drawing.Size(0, 3);
            this.splitterBottom2.TabIndex = 4;
            this.splitterBottom2.TabStop = false;
            // 
            // panelAutoscan0
            // 
            this.panelAutoscan0.AutoScroll = true;
            this.panelAutoscan0.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.panelAutoscan0.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelAutoscan0.Location = new System.Drawing.Point(0, 545);
            this.panelAutoscan0.Name = "panelAutoscan0";
            this.panelAutoscan0.Size = new System.Drawing.Size(0, 100);
            this.panelAutoscan0.TabIndex = 3;
            this.panelAutoscan0.Tag = 0;
            // 
            // panel0
            // 
            this.panel0.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.panel0.Controls.Add(this.panel0Charts);
            this.panel0.Controls.Add(this.panel0Trend);
            this.panel0.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel0.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel0.Location = new System.Drawing.Point(240, 0);
            this.panel0.Name = "panel0";
            this.panel0.Size = new System.Drawing.Size(498, 688);
            this.panel0.TabIndex = 110;
            this.panel0.Visible = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.panel1.Controls.Add(this.panel1Charts);
            this.panel1.Controls.Add(this.panelAutoscan1);
            this.panel1.Controls.Add(this.panel1Trend);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel1.Location = new System.Drawing.Point(111, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(498, 688);
            this.panel1.TabIndex = 111;
            this.panel1.Visible = false;
            // 
            // panelAutoscan1
            // 
            this.panelAutoscan1.AutoScroll = true;
            this.panelAutoscan1.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.panelAutoscan1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelAutoscan1.Location = new System.Drawing.Point(0, 548);
            this.panelAutoscan1.Name = "panelAutoscan1";
            this.panelAutoscan1.Size = new System.Drawing.Size(498, 100);
            this.panelAutoscan1.TabIndex = 133;
            this.panelAutoscan1.Tag = 1;
            this.panelAutoscan1.Visible = false;
            // 
            // dockingManager1
            // 
            this.dockingManager1.CloseEnabled = false;
            this.dockingManager1.DockLayoutStream = ((System.IO.MemoryStream)(resources.GetObject("dockingManager1.DockLayoutStream")));
            this.dockingManager1.HostControl = this;
            this.dockingManager1.DockStateChanged += new Syncfusion.Windows.Forms.Tools.DockStateChangeEventHandler(this.dockingManager1_DockStateChanged);
            this.dockingManager1.SetDockLabel(this.panelHits, "panelHits");
            this.dockingManager1.SetAutoHideOnLoad(this.panelHits, true);
            this.dockingManager1.SetDockLabel(this.panelEvents, "panelEvents");
            this.dockingManager1.SetDockLabel(this.panelBV, "panelBV");
            this.dockingManager1.SetDockLabel(this.panelFFT, "panelFFT");
            this.dockingManager1.SetAutoHideOnLoad(this.panelFFT, true);
            // 
            // panelHits
            // 
            this.panelHits.BackColor = System.Drawing.Color.White;
            this.panelHits.Controls.Add(this.hitsHistogram1);
            this.dockingManager1.SetEnableDocking(this.panelHits, true);
            this.panelHits.Location = new System.Drawing.Point(1, 23);
            this.panelHits.Name = "panelHits";
            this.panelHits.Size = new System.Drawing.Size(160, 505);
            this.panelHits.TabIndex = 0;
            // 
            // hitsHistogram1
            // 
            this.hitsHistogram1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(235)))), ((int)(((byte)(248)))));
            this.hitsHistogram1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hitsHistogram1.Location = new System.Drawing.Point(0, 0);
            this.hitsHistogram1.Name = "hitsHistogram1";
            this.hitsHistogram1.Size = new System.Drawing.Size(160, 553);
            this.hitsHistogram1.TabIndex = 2;
            // 
            // panelBV
            // 
            this.panelBV.BackColor = System.Drawing.Color.RoyalBlue;
            this.panelBV.Controls.Add(this.bvListViewCtrl1);
            this.dockingManager1.SetEnableDocking(this.panelBV, true);
            this.panelBV.Location = new System.Drawing.Point(1, 23);
            this.panelBV.Name = "panelBV";
            this.panelBV.Size = new System.Drawing.Size(160, 103);
            this.panelBV.TabIndex = 9;
            // 
            // bvListViewCtrl1
            // 
            this.bvListViewCtrl1.BackColor = System.Drawing.SystemColors.Control;
            this.bvListViewCtrl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bvListViewCtrl1.ExaminationWasSaved = true;
            this.bvListViewCtrl1.ExamNotes = "";
            this.bvListViewCtrl1.InSummaryStatus = TCD2003.GUI.GlobalTypes.TAdd2SummaryScreen.SelectedSpectrum;
            this.bvListViewCtrl1.Location = new System.Drawing.Point(0, 0);
            this.bvListViewCtrl1.Name = "bvListViewCtrl1";
            this.bvListViewCtrl1.Size = new System.Drawing.Size(160, 115);
            this.bvListViewCtrl1.SubExamNotes = "";
            this.bvListViewCtrl1.TabIndex = 3;
            // 
            // panelEvents
            // 
            this.panelEvents.BackColor = System.Drawing.Color.White;
            this.panelEvents.Controls.Add(this.eventListViewCtrl1);
            this.panelEvents.Controls.Add(this.panel2);
            this.dockingManager1.SetEnableDocking(this.panelEvents, true);
            this.panelEvents.Location = new System.Drawing.Point(1, 23);
            this.panelEvents.Name = "panelEvents";
            this.panelEvents.Size = new System.Drawing.Size(160, 505);
            this.panelEvents.TabIndex = 6;
            this.panelEvents.Visible = false;
            // 
            // eventListViewCtrl1
            // 
            this.eventListViewCtrl1.BackColor = System.Drawing.SystemColors.Control;
            this.eventListViewCtrl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.eventListViewCtrl1.EventListSaved = true;
            this.eventListViewCtrl1.Location = new System.Drawing.Point(0, 0);
            this.eventListViewCtrl1.Name = "eventListViewCtrl1";
            this.eventListViewCtrl1.Size = new System.Drawing.Size(160, 489);
            this.eventListViewCtrl1.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.textBox1);
            this.panel2.Controls.Add(this.button3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 489);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(160, 64);
            this.panel2.TabIndex = 3;
            this.panel2.Visible = false;
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(8, 38);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(144, 22);
            this.textBox1.TabIndex = 1;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(8, 24);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(48, 23);
            this.button3.TabIndex = 0;
            this.button3.Visible = false;
            // 
            // panelFFT
            // 
            this.panelFFT.BackColor = System.Drawing.Color.RoyalBlue;
            this.panelFFT.Controls.Add(this.panelFFTEnergy);
            this.panelFFT.Enabled = false;
            this.dockingManager1.SetEnableDocking(this.panelFFT, true);
            this.panelFFT.Location = new System.Drawing.Point(1, 23);
            this.panelFFT.Name = "panelFFT";
            this.panelFFT.Size = new System.Drawing.Size(160, 505);
            this.panelFFT.TabIndex = 10;
            this.panelFFT.Visible = false;
            // 
            // panelFFTEnergy
            // 
            this.panelFFTEnergy.BackColor = System.Drawing.Color.White;
            this.panelFFTEnergy.Controls.Add(this.labelE2);
            this.panelFFTEnergy.Controls.Add(this.labelE1);
            this.panelFFTEnergy.Controls.Add(this.label);
            this.panelFFTEnergy.Controls.Add(this.label15);
            this.panelFFTEnergy.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelFFTEnergy.Location = new System.Drawing.Point(0, 0);
            this.panelFFTEnergy.Name = "panelFFTEnergy";
            this.panelFFTEnergy.Size = new System.Drawing.Size(160, 80);
            this.panelFFTEnergy.TabIndex = 0;
            // 
            // labelE2
            // 
            this.labelE2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.labelE2.Location = new System.Drawing.Point(64, 48);
            this.labelE2.Name = "labelE2";
            this.labelE2.Size = new System.Drawing.Size(88, 32);
            this.labelE2.TabIndex = 2;
            this.labelE2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelE1
            // 
            this.labelE1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.labelE1.Location = new System.Drawing.Point(64, 8);
            this.labelE1.Name = "labelE1";
            this.labelE1.Size = new System.Drawing.Size(88, 32);
            this.labelE1.TabIndex = 1;
            this.labelE1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label
            // 
            this.label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label.Location = new System.Drawing.Point(8, 8);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(56, 32);
            this.label.TabIndex = 0;
            this.label.Text = "E1 =";
            this.label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label15.Location = new System.Drawing.Point(8, 48);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(56, 32);
            this.label15.TabIndex = 0;
            this.label15.Text = "E2 = ";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dockingClientPanel1
            // 
            this.dockingClientPanel1.BackColor = System.Drawing.Color.White;
            this.dockingClientPanel1.Controls.Add(this.splitterLeft);
            this.dockingClientPanel1.Controls.Add(this.panel0);
            this.dockingClientPanel1.Controls.Add(this.panelMiddle);
            this.dockingClientPanel1.Controls.Add(this.panel1);
            this.dockingClientPanel1.Controls.Add(this.splitterRight);
            this.dockingClientPanel1.Controls.Add(this.panelRight);
            this.dockingClientPanel1.Controls.Add(this.panelLeft);
            this.dockingClientPanel1.Location = new System.Drawing.Point(0, 0);
            this.dockingClientPanel1.Name = "dockingClientPanel1";
            this.dockingClientPanel1.Size = new System.Drawing.Size(852, 748);
            this.dockingClientPanel1.SizeToFit = true;
            this.dockingClientPanel1.TabIndex = 0;
            // 
            // splitterLeft
            // 
            this.splitterLeft.BackColor = System.Drawing.SystemColors.Control;
            this.splitterLeft.Location = new System.Drawing.Point(738, 0);
            this.splitterLeft.MinExtra = 400;
            this.splitterLeft.MinSize = 150;
            this.splitterLeft.Name = "splitterLeft";
            this.splitterLeft.Size = new System.Drawing.Size(3, 748);
            this.splitterLeft.TabIndex = 21;
            this.splitterLeft.TabStop = false;
            // 
            // splitterRight
            // 
            this.splitterRight.BackColor = System.Drawing.SystemColors.Control;
            this.splitterRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitterRight.Location = new System.Drawing.Point(609, 0);
            this.splitterRight.MinExtra = 400;
            this.splitterRight.MinSize = 150;
            this.splitterRight.Name = "splitterRight";
            this.splitterRight.Size = new System.Drawing.Size(3, 748);
            this.splitterRight.TabIndex = 2;
            this.splitterRight.TabStop = false;
            // 
            // panelRight
            // 
            this.panelRight.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.panelRight.Controls.Add(this.panelHITS2);
            this.panelRight.Controls.Add(this.panelRightCharts);
            this.panelRight.Controls.Add(this.panelHeaderRight);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(612, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(240, 748);
            this.panelRight.TabIndex = 0;
            // 
            // panelHITS2
            // 
            this.panelHITS2.Controls.Add(this.label13);
            this.panelHITS2.Controls.Add(this.panel3);
            this.panelHITS2.Location = new System.Drawing.Point(240, 0);
            this.panelHITS2.Name = "panelHITS2";
            this.panelHITS2.Size = new System.Drawing.Size(240, 286);
            this.panelHITS2.TabIndex = 4;
            this.panelHITS2.Visible = false;
            // 
            // label13
            // 
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label13.Dock = System.Windows.Forms.DockStyle.Top;
            this.label13.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label13.Location = new System.Drawing.Point(0, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(240, 23);
            this.label13.TabIndex = 0;
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(240, 286);
            this.panel3.TabIndex = 1;
            // 
            // panelHeaderRight
            // 
            this.panelHeaderRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(235)))), ((int)(((byte)(250)))));
            this.panelHeaderRight.Controls.Add(this.comboBoxTimeRight);
            this.panelHeaderRight.Controls.Add(this.label6);
            this.panelHeaderRight.Controls.Add(this.label5);
            this.panelHeaderRight.Controls.Add(this.label4);
            this.panelHeaderRight.Controls.Add(this.label3);
            this.panelHeaderRight.Controls.Add(this.label2);
            this.panelHeaderRight.Controls.Add(this.label1);
            this.panelHeaderRight.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeaderRight.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(55)))), ((int)(((byte)(95)))));
            this.panelHeaderRight.Location = new System.Drawing.Point(0, 0);
            this.panelHeaderRight.Name = "panelHeaderRight";
            this.panelHeaderRight.Padding = new System.Windows.Forms.Padding(3);
            this.panelHeaderRight.Size = new System.Drawing.Size(240, 29);
            this.panelHeaderRight.TabIndex = 3;
            // 
            // comboBoxTimeRight
            // 
            this.comboBoxTimeRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.comboBoxTimeRight.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTimeRight.Location = new System.Drawing.Point(181, 3);
            this.comboBoxTimeRight.Name = "comboBoxTimeRight";
            this.comboBoxTimeRight.Size = new System.Drawing.Size(56, 21);
            this.comboBoxTimeRight.TabIndex = 6;
            this.comboBoxTimeRight.SelectedIndexChanged += new System.EventHandler(this.comboBoxTime_SelectedIndexChanged);
            this.comboBoxTimeRight.SelectionChangeCommitted += new System.EventHandler(this.comboBoxTime_SelectionChangeCommitted);
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(120, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 24);
            this.label6.TabIndex = 5;
            this.label6.Text = "1";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(96, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(24, 24);
            this.label5.TabIndex = 4;
            this.label5.Text = "1";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(64, 8);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(24, 24);
            this.label4.TabIndex = 3;
            this.label4.Text = "1";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(24, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 24);
            this.label3.TabIndex = 2;
            this.label3.Text = "1";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(48, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "1";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(0, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "1";
            // 
            // panelLeft
            // 
            this.panelLeft.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.panelLeft.Controls.Add(this.panelLeftCharts);
            this.panelLeft.Controls.Add(this.panelHeaderLeft);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelLeft.Location = new System.Drawing.Point(0, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(240, 748);
            this.panelLeft.TabIndex = 20;
            // 
            // panelLeftCharts
            // 
            this.panelLeftCharts.AutoScroll = true;
            this.panelLeftCharts.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.panelLeftCharts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelLeftCharts.Location = new System.Drawing.Point(0, 29);
            this.panelLeftCharts.Name = "panelLeftCharts";
            this.panelLeftCharts.Size = new System.Drawing.Size(240, 719);
            this.panelLeftCharts.TabIndex = 1;
            // 
            // panelHeaderLeft
            // 
            this.panelHeaderLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(235)))), ((int)(((byte)(250)))));
            this.panelHeaderLeft.Controls.Add(this.comboBoxTimeLeft);
            this.panelHeaderLeft.Controls.Add(this.label7);
            this.panelHeaderLeft.Controls.Add(this.label8);
            this.panelHeaderLeft.Controls.Add(this.label9);
            this.panelHeaderLeft.Controls.Add(this.label10);
            this.panelHeaderLeft.Controls.Add(this.label11);
            this.panelHeaderLeft.Controls.Add(this.label12);
            this.panelHeaderLeft.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelHeaderLeft.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(55)))), ((int)(((byte)(95)))));
            this.panelHeaderLeft.Location = new System.Drawing.Point(0, 0);
            this.panelHeaderLeft.Name = "panelHeaderLeft";
            this.panelHeaderLeft.Padding = new System.Windows.Forms.Padding(3);
            this.panelHeaderLeft.Size = new System.Drawing.Size(240, 29);
            this.panelHeaderLeft.TabIndex = 3;
            // 
            // comboBoxTimeLeft
            // 
            this.comboBoxTimeLeft.Dock = System.Windows.Forms.DockStyle.Right;
            this.comboBoxTimeLeft.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTimeLeft.Location = new System.Drawing.Point(181, 3);
            this.comboBoxTimeLeft.Name = "comboBoxTimeLeft";
            this.comboBoxTimeLeft.Size = new System.Drawing.Size(56, 21);
            this.comboBoxTimeLeft.TabIndex = 7;
            this.comboBoxTimeLeft.SelectedIndexChanged += new System.EventHandler(this.comboBoxTime_SelectedIndexChanged);
            this.comboBoxTimeLeft.SelectionChangeCommitted += new System.EventHandler(this.comboBoxTime_SelectionChangeCommitted);
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(120, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 24);
            this.label7.TabIndex = 5;
            this.label7.Text = "1";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(96, 8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(24, 24);
            this.label8.TabIndex = 4;
            this.label8.Text = "1";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(72, 8);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(24, 24);
            this.label9.TabIndex = 3;
            this.label9.Text = "1";
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(24, 8);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(24, 24);
            this.label10.TabIndex = 2;
            this.label10.Text = "1";
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(48, 8);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(24, 24);
            this.label11.TabIndex = 1;
            this.label11.Text = "1";
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(0, 8);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(24, 24);
            this.label12.TabIndex = 0;
            this.label12.Text = "1";
            // 
            // mainFrameBarManager1
            // 
            this.mainFrameBarManager1.BarPositionInfo = ((System.IO.MemoryStream)(resources.GetObject("mainFrameBarManager1.BarPositionInfo")));
            this.mainFrameBarManager1.Bars.Add(this.barMainMenu);
            this.mainFrameBarManager1.Categories.Add("Patient");
            this.mainFrameBarManager1.Categories.Add("Studies");
            this.mainFrameBarManager1.Categories.Add("Setup");
            this.mainFrameBarManager1.Categories.Add("Function");
            this.mainFrameBarManager1.Categories.Add("Utilities");
            this.mainFrameBarManager1.Categories.Add("Help");
            this.mainFrameBarManager1.Categories.Add("Debug");
            this.mainFrameBarManager1.Categories.Add("KBShortCuts");
            this.mainFrameBarManager1.Categories.Add("MainMenuPopups");
            this.mainFrameBarManager1.CurrentBaseFormType = "System.Windows.Forms.Form";
            this.mainFrameBarManager1.DetachedCommandBars.Add(this.commandBar1);
            this.mainFrameBarManager1.Form = this;
            this.mainFrameBarManager1.ImageList = null;
            this.mainFrameBarManager1.Items.AddRange(new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem[] {
            this.parentBarItemPatient,
            this.parentBarItemStudies,
            this.barItemNewSpectrum,
            this.parentBarItemSetup,
            this.parentBarItemFunctions,
            this.parentBarItemUtilities,
            this.parentBarItemHelp,
            this.parentBarItem1,
            this.parentBarItem2,
            this.barItemNewTrend,
            this.SaveDebugData,
            this.barItemNextBV,
            this.barItemPrevBV,
            this.barItemPatientNew,
            this.barItemSaveStudy,
            this.barItemUnFreeze,
            this.barItemPatientLoad,
            this.barItem1,
            this.barItemBackup,
            this.BarItem_UpZeroLine,
            this.barItem_DownZeroLine,
            this.barItemRestore,
            this.barItemPatientSearch,
            this.barItemPatientDelete,
            this.barItemAuthorizationMgr,
            this.barItemDebug,
            this.barItemNextFunction,
            this.barItemPatientPrintPreview,
            this.barItemPrint,
            this.barItemSetupGeneral,
            this.barItemSetupStudies,
            this.barItemHelpContent,
            this.barItemHelpIndex,
            this.barItemHelpSearch,
            this.barItemHelpAbout,
            this.barItemNextFunction,
            this.barItemGainUp,
            this.barItemGainDown,
            this.barItemDepthUp,
            this.barItemDepthDown,
            this.barItemFreqDown,
            this.barItemFreqUp,
            this.barItemPowerDown,
            this.barItemPowerUp,
            this.barItemWidthDown,
            this.barItemWidthUp,
            this.barItemThumpDown,
            this.barItemThumpUp,
            this.barItemPanelHits,
            this.barItemHitsDetection,
            this.barItemAddEvent,
            this.barItemDeleteEvent,
            this.barItemSummaryScreen,
            this.barItem2,
            this.barItemHelpWhatIsThis,
            this.barItemCursors,
            this.barItemNotes,
            this.barItemPatientReport,
            this.barItemAutoscan,
            this.barItemRecord,
            this.barItemStop,
            this.barItemPlay,
            this.barItemSave,
            this.barItemExpandClinicalParameters,
            this.barItemExportToFile,
            this.barItemPatientReportWizard,
            this.barItemExit,
            this.barItemExitWindows,
            this.barItemAdminExitWindows,
            this.barItemCancelUtilitiesMenu,
            this.barItemZipRawDataFiles,
            this.barItemSaveDebugFFT,
            this.barItemPatientExport,
            this.barItemExportSelectedSpectrum,
            this.barItemExportFullScreen,
            this.barItemExportTrends,
            this.barItemExportLog,
            this.barItem4,
            this.barItemDeleteStudy});
            this.mainFrameBarManager1.LargeImageList = null;
            this.mainFrameBarManager1.ResetCustomization = false;
            // 
            // barMainMenu
            // 
            this.barMainMenu.BarName = "MainMenu";
            this.barMainMenu.BarStyle = ((Syncfusion.Windows.Forms.Tools.XPMenus.BarStyle)((((Syncfusion.Windows.Forms.Tools.XPMenus.BarStyle.IsMainMenu | Syncfusion.Windows.Forms.Tools.XPMenus.BarStyle.RotateWhenVertical) 
            | Syncfusion.Windows.Forms.Tools.XPMenus.BarStyle.Visible) 
            | Syncfusion.Windows.Forms.Tools.XPMenus.BarStyle.UseWholeRow)));
            this.barMainMenu.Items.AddRange(new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem[] {
            this.parentBarItem1,
            this.parentBarItemPatient,
            this.parentBarItemStudies,
            this.parentBarItemSetup,
            this.parentBarItemFunctions,
            this.parentBarItemUtilities,
            this.parentBarItemHelp,
            this.parentBarItem2});
            this.barMainMenu.Manager = this.mainFrameBarManager1;
            // 
            // parentBarItem1
            // 
            this.parentBarItem1.CategoryIndex = 8;
            this.parentBarItem1.ID = "&Debug";
            this.parentBarItem1.Items.AddRange(new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem[] {
            this.barItemNewSpectrum,
            this.barItemNewTrend,
            this.barItemSaveStudy,
            this.barItemDeleteStudy,
            this.barItemUnFreeze,
            this.barItem1,
            this.BarItem_UpZeroLine,
            this.barItem_DownZeroLine,
            this.SaveDebugData,
            this.barItemSaveDebugFFT});
            // 
            // barItemNewSpectrum
            // 
            this.barItemNewSpectrum.CategoryIndex = 6;
            this.barItemNewSpectrum.ID = "New Spectrum";
            this.barItemNewSpectrum.Shortcut = System.Windows.Forms.Shortcut.CtrlP;
            this.barItemNewSpectrum.Click += new System.EventHandler(this.buttonAddSpectrum_Click);
            // 
            // barItemNewTrend
            // 
            this.barItemNewTrend.CategoryIndex = 6;
            this.barItemNewTrend.ID = "New Trend";
            this.barItemNewTrend.Shortcut = System.Windows.Forms.Shortcut.CtrlT;
            this.barItemNewTrend.Click += new System.EventHandler(this.buttonAddTrend_Click);
            // 
            // barItemSaveStudy
            // 
            this.barItemSaveStudy.CategoryIndex = 6;
            this.barItemSaveStudy.ID = "&save Study";
            this.barItemSaveStudy.ImageIndex = 1;
            this.barItemSaveStudy.ImageList = this.imageListMenu;
            this.barItemSaveStudy.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.barItemSaveStudy.Click += new System.EventHandler(this.barItemSaveStudy_Click);
            // 
            // imageListMenu
            // 
            this.imageListMenu.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListMenu.ImageStream")));
            this.imageListMenu.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListMenu.Images.SetKeyName(0, "");
            this.imageListMenu.Images.SetKeyName(1, "");
            this.imageListMenu.Images.SetKeyName(2, "");
            // 
            // barItemDeleteStudy
            // 
            this.barItemDeleteStudy.CategoryIndex = 6;
            this.barItemDeleteStudy.ID = "&delete Study";
            this.barItemDeleteStudy.Shortcut = System.Windows.Forms.Shortcut.Del;
            this.barItemDeleteStudy.Click += new System.EventHandler(this.barItemDeleteStudy_Click);
            // 
            // barItemUnFreeze
            // 
            this.barItemUnFreeze.CategoryIndex = 6;
            this.barItemUnFreeze.ID = "UnFreeze";
            this.barItemUnFreeze.Click += new System.EventHandler(this.barItemUnFreeze_Click);
            // 
            // barItem1
            // 
            this.barItem1.CategoryIndex = 6;
            this.barItem1.ID = "ReadFFT";
            this.barItem1.Click += new System.EventHandler(this.barItem1_Click);
            // 
            // BarItem_UpZeroLine
            // 
            this.BarItem_UpZeroLine.CategoryIndex = 6;
            this.BarItem_UpZeroLine.ID = "UpZeroLine";
            this.BarItem_UpZeroLine.Shortcut = System.Windows.Forms.Shortcut.CtrlU;
            this.BarItem_UpZeroLine.Click += new System.EventHandler(this.BarItem_UpZeroLine_Click);
            // 
            // barItem_DownZeroLine
            // 
            this.barItem_DownZeroLine.CategoryIndex = 6;
            this.barItem_DownZeroLine.ID = "DownZeroLine";
            this.barItem_DownZeroLine.Shortcut = System.Windows.Forms.Shortcut.CtrlD;
            this.barItem_DownZeroLine.Click += new System.EventHandler(this.barItem_DownZeroLine_Click);
            // 
            // SaveDebugData
            // 
            this.SaveDebugData.CategoryIndex = 8;
            this.SaveDebugData.ID = "SaveDebugData";
            // 
            // barItemSaveDebugFFT
            // 
            this.barItemSaveDebugFFT.CategoryIndex = 6;
            this.barItemSaveDebugFFT.ID = "Save Debug FFT";
            // 
            // parentBarItemPatient
            // 
            this.parentBarItemPatient.CategoryIndex = 8;
            this.parentBarItemPatient.ID = "Patient";
            this.parentBarItemPatient.Items.AddRange(new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem[] {
            this.barItemPatientNew,
            this.barItemPatientLoad,
            this.barItemPatientSearch,
            this.barItemPatientDelete,
            this.barItemPatientExport,
            this.barItemBackup,
            this.barItemRestore,
            this.barItemPatientPrintPreview,
            this.barItemPrint,
            this.barItemExit,
            this.barItemExitWindows});
            this.parentBarItemPatient.SeparatorIndices.AddRange(new int[] {
            3,
            4,
            5,
            7,
            9});
            // 
            // barItemPatientNew
            // 
            this.barItemPatientNew.CategoryIndex = 0;
            this.barItemPatientNew.ID = "New";
            this.barItemPatientNew.Click += new System.EventHandler(this.buttonNewPatient_Click);
            // 
            // barItemPatientLoad
            // 
            this.barItemPatientLoad.CategoryIndex = 0;
            this.barItemPatientLoad.ID = "Load";
            this.barItemPatientLoad.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // barItemPatientSearch
            // 
            this.barItemPatientSearch.CategoryIndex = 0;
            this.barItemPatientSearch.ID = "Search";
            this.barItemPatientSearch.Click += new System.EventHandler(this.barItemPatientSearch_Click);
            // 
            // barItemPatientDelete
            // 
            this.barItemPatientDelete.CategoryIndex = 0;
            this.barItemPatientDelete.ID = "Delete";
            this.barItemPatientDelete.Click += new System.EventHandler(this.barItemPatientDelete_Click);
            // 
            // barItemPatientExport
            // 
            this.barItemPatientExport.CategoryIndex = 0;
            this.barItemPatientExport.ID = "Export";
            this.barItemPatientExport.Items.AddRange(new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem[] {
            this.barItemExportSelectedSpectrum,
            this.barItemExportFullScreen,
            this.barItemExportTrends,
            this.barItemExportLog});
            this.barItemPatientExport.Popup += new System.EventHandler(this.barItemPatientExport_Popup);
            // 
            // barItemExportSelectedSpectrum
            // 
            this.barItemExportSelectedSpectrum.CategoryIndex = 0;
            this.barItemExportSelectedSpectrum.ID = "Selected Spectrum";
            this.barItemExportSelectedSpectrum.Visible = false;
            this.barItemExportSelectedSpectrum.Click += new System.EventHandler(this.barItemExportSelectedSpectrum_Click);
            // 
            // barItemExportFullScreen
            // 
            this.barItemExportFullScreen.CategoryIndex = 0;
            this.barItemExportFullScreen.ID = "Full Screen";
            this.barItemExportFullScreen.Click += new System.EventHandler(this.barItemExportFullScreen_Click);
            // 
            // barItemExportTrends
            // 
            this.barItemExportTrends.CategoryIndex = 0;
            this.barItemExportTrends.ID = "Export trends to Excel";
            this.barItemExportTrends.Click += new System.EventHandler(this.barItemExportTrends_Click);
            // 
            // barItemExportLog
            // 
            this.barItemExportLog.CategoryIndex = 0;
            this.barItemExportLog.ID = "Export Logs";
            this.barItemExportLog.Click += new System.EventHandler(this.barItemExportLog_Click);
            // 
            // barItemBackup
            // 
            this.barItemBackup.CategoryIndex = 4;
            this.barItemBackup.ID = "Backup";
            this.barItemBackup.Click += new System.EventHandler(this.barItemBackup_Click);
            // 
            // barItemRestore
            // 
            this.barItemRestore.CategoryIndex = 4;
            this.barItemRestore.ID = "Restore";
            this.barItemRestore.Click += new System.EventHandler(this.barItemRestore_Click);
            // 
            // barItemPatientPrintPreview
            // 
            this.barItemPatientPrintPreview.CategoryIndex = 0;
            this.barItemPatientPrintPreview.ID = "Print Preview";
            this.barItemPatientPrintPreview.Click += new System.EventHandler(this.barItemPatientPrintPreview_Click);
            // 
            // barItemPrint
            // 
            this.barItemPrint.CategoryIndex = 0;
            this.barItemPrint.ID = "Print";
            this.barItemPrint.Click += new System.EventHandler(this.barItemPrint_Click);
            // 
            // barItemExit
            // 
            this.barItemExit.CategoryIndex = 0;
            this.barItemExit.ID = "Exit";
            this.barItemExit.Click += new System.EventHandler(this.barItemExit_Click);
            // 
            // barItemExitWindows
            // 
            this.barItemExitWindows.CategoryIndex = 0;
            this.barItemExitWindows.ID = "Exit windows";
            this.barItemExitWindows.Click += new System.EventHandler(this.barItemExitWindows_Click);
            // 
            // parentBarItemStudies
            // 
            this.parentBarItemStudies.CategoryIndex = 8;
            this.parentBarItemStudies.ID = "Studies";
            // 
            // parentBarItemSetup
            // 
            this.parentBarItemSetup.CategoryIndex = 8;
            this.parentBarItemSetup.ID = "Setup";
            this.parentBarItemSetup.Items.AddRange(new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem[] {
            this.barItemPatientReportWizard,
            this.barItemSetupGeneral,
            this.barItemSetupStudies,
            this.barItemSaveStudy,
            this.barItemDeleteStudy});
            this.parentBarItemSetup.SeparatorIndices.AddRange(new int[] {
            3});
            // 
            // barItemPatientReportWizard
            // 
            this.barItemPatientReportWizard.CategoryIndex = 2;
            this.barItemPatientReportWizard.ID = "Report Generator Wizard";
            this.barItemPatientReportWizard.ImageIndex = 2;
            this.barItemPatientReportWizard.ImageList = this.imageListMenu;
            this.barItemPatientReportWizard.Click += new System.EventHandler(this.barItemPatientReportWizard_Click);
            // 
            // barItemSetupGeneral
            // 
            this.barItemSetupGeneral.CategoryIndex = 2;
            this.barItemSetupGeneral.ID = "General";
            this.barItemSetupGeneral.ImageIndex = 0;
            this.barItemSetupGeneral.ImageList = this.imageListMenu;
            this.barItemSetupGeneral.Click += new System.EventHandler(this.barItemSetupGeneral_Click);
            // 
            // barItemSetupStudies
            // 
            this.barItemSetupStudies.CategoryIndex = 2;
            this.barItemSetupStudies.ID = "Studies_1";
            this.barItemSetupStudies.Click += new System.EventHandler(this.barItemSetupStudies_Click);
            // 
            // parentBarItemFunctions
            // 
            this.parentBarItemFunctions.CategoryIndex = 8;
            this.parentBarItemFunctions.ID = "Function";
            this.parentBarItemFunctions.Items.AddRange(new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem[] {
            this.barItemNextFunction,
            this.barItemNewSpectrum,
            this.barItemAutoscan,
            this.barItemCursors,
            this.barItemNotes,
            this.barItemPatientReport,
            this.barItemHitsDetection,
            this.barItemAddEvent,
            this.barItemDeleteEvent,
            this.barItemNewTrend,
            this.barItemNextBV,
            this.barItemPrevBV,
            this.barItemSummaryScreen,
            this.barItemRecord,
            this.barItemStop,
            this.barItemPlay,
            this.barItemExpandClinicalParameters});
            this.parentBarItemFunctions.SeparatorIndices.AddRange(new int[] {
            3,
            9,
            11});
            this.parentBarItemFunctions.BeforePopup += new System.ComponentModel.CancelEventHandler(this.parentBarItemFunctions_BeforePopup);
            // 
            // barItemNextFunction
            // 
            this.barItemNextFunction.CategoryIndex = -1;
            this.barItemNextFunction.ID = "_1";
            // 
            // barItemAutoscan
            // 
            this.barItemAutoscan.CategoryIndex = 3;
            this.barItemAutoscan.Checked = true;
            this.barItemAutoscan.ID = "M-Mode";
            this.barItemAutoscan.Click += new System.EventHandler(this.buttonAutoscan_Click);
            // 
            // barItemCursors
            // 
            this.barItemCursors.CategoryIndex = 3;
            this.barItemCursors.ID = "Cursors";
            this.barItemCursors.Click += new System.EventHandler(this.barItemCursors_Click);
            // 
            // barItemNotes
            // 
            this.barItemNotes.CategoryIndex = 3;
            this.barItemNotes.ID = "Notes";
            this.barItemNotes.Click += new System.EventHandler(this.buttonNotes_Click);
            // 
            // barItemPatientReport
            // 
            this.barItemPatientReport.CategoryIndex = 3;
            this.barItemPatientReport.ID = "Patient Report";
            this.barItemPatientReport.Click += new System.EventHandler(this.barItemPatientReport_Click);
            // 
            // barItemHitsDetection
            // 
            this.barItemHitsDetection.CategoryIndex = 3;
            this.barItemHitsDetection.ID = "Hits Detection";
            this.barItemHitsDetection.Shortcut = System.Windows.Forms.Shortcut.CtrlH;
            this.barItemHitsDetection.Click += new System.EventHandler(this.buttonAHitsDetection_Click);
            // 
            // barItemAddEvent
            // 
            this.barItemAddEvent.CategoryIndex = 3;
            this.barItemAddEvent.ID = "Add Event";
            this.barItemAddEvent.Click += new System.EventHandler(this.barItemAddEvent_Click);
            // 
            // barItemDeleteEvent
            // 
            this.barItemDeleteEvent.CategoryIndex = 3;
            this.barItemDeleteEvent.ID = "Delete Event";
            this.barItemDeleteEvent.Click += new System.EventHandler(this.barItemDeleteEvent_Click);
            // 
            // barItemNextBV
            // 
            this.barItemNextBV.CategoryIndex = 8;
            this.barItemNextBV.ID = "Next BV";
            this.barItemNextBV.Shortcut = System.Windows.Forms.Shortcut.CtrlB;
            this.barItemNextBV.Click += new System.EventHandler(this.barItemNextBV_Click);
            // 
            // barItemPrevBV
            // 
            this.barItemPrevBV.CategoryIndex = 8;
            this.barItemPrevBV.ID = "Previous BV";
            this.barItemPrevBV.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftB;
            this.barItemPrevBV.Click += new System.EventHandler(this.barItemPrevBV_Click);
            // 
            // barItemSummaryScreen
            // 
            this.barItemSummaryScreen.CategoryIndex = 3;
            this.barItemSummaryScreen.ID = "Summary Screen";
            this.barItemSummaryScreen.Click += new System.EventHandler(this.buttonSummary_Click);
            // 
            // barItemRecord
            // 
            this.barItemRecord.CategoryIndex = 3;
            this.barItemRecord.ID = "Record";
            this.barItemRecord.Click += new System.EventHandler(this.barItemRecord_Click);
            // 
            // barItemStop
            // 
            this.barItemStop.CategoryIndex = 3;
            this.barItemStop.ID = "Stop";
            this.barItemStop.Click += new System.EventHandler(this.barItemStop_Click);
            // 
            // barItemPlay
            // 
            this.barItemPlay.CategoryIndex = 3;
            this.barItemPlay.ID = "Play";
            this.barItemPlay.Click += new System.EventHandler(this.barItemPlay_Click);
            // 
            // barItemExpandClinicalParameters
            // 
            this.barItemExpandClinicalParameters.CategoryIndex = 3;
            this.barItemExpandClinicalParameters.ID = "Expand Clinical Parameters";
            // 
            // parentBarItemUtilities
            // 
            this.parentBarItemUtilities.CategoryIndex = 8;
            this.parentBarItemUtilities.ID = "Utilities";
            this.parentBarItemUtilities.Items.AddRange(new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem[] {
            this.barItemAuthorizationMgr,
            this.barItemCancelUtilitiesMenu,
            this.barItemAdminExitWindows});
            this.parentBarItemUtilities.SeparatorIndices.AddRange(new int[] {
            2});
            this.parentBarItemUtilities.Visible = false;
            // 
            // barItemAuthorizationMgr
            // 
            this.barItemAuthorizationMgr.CategoryIndex = 4;
            this.barItemAuthorizationMgr.ID = "Authorization Manager...";
            this.barItemAuthorizationMgr.Click += new System.EventHandler(this.barItemAuthorizationMgr_Click);
            // 
            // barItemCancelUtilitiesMenu
            // 
            this.barItemCancelUtilitiesMenu.CategoryIndex = 4;
            this.barItemCancelUtilitiesMenu.ID = "CancelUtilitiesMenu";
            this.barItemCancelUtilitiesMenu.Click += new System.EventHandler(this.barItemCancelUtilitiesMenu_Click);
            // 
            // barItemAdminExitWindows
            // 
            this.barItemAdminExitWindows.CategoryIndex = 4;
            this.barItemAdminExitWindows.ID = "ExitToWin";
            this.barItemAdminExitWindows.Click += new System.EventHandler(this.barItemAdminExitWindows_Click);
            // 
            // parentBarItemHelp
            // 
            this.parentBarItemHelp.CategoryIndex = 8;
            this.parentBarItemHelp.ID = "Help";
            this.parentBarItemHelp.Items.AddRange(new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem[] {
            this.barItemHelpContent,
            this.barItemHelpIndex,
            this.barItemHelpSearch,
            this.barItemHelpWhatIsThis,
            this.barItemHelpAbout});
            this.parentBarItemHelp.SeparatorIndices.AddRange(new int[] {
            3,
            4});
            // 
            // barItemHelpContent
            // 
            this.barItemHelpContent.CategoryIndex = 5;
            this.barItemHelpContent.ID = "Content";
            this.barItemHelpContent.Click += new System.EventHandler(this.barItemHelpContent_Click_1);
            // 
            // barItemHelpIndex
            // 
            this.barItemHelpIndex.CategoryIndex = 5;
            this.barItemHelpIndex.ID = "Index";
            // 
            // barItemHelpSearch
            // 
            this.barItemHelpSearch.CategoryIndex = 5;
            this.barItemHelpSearch.ID = "Search_1";
            // 
            // barItemHelpWhatIsThis
            // 
            this.barItemHelpWhatIsThis.CategoryIndex = 5;
            this.barItemHelpWhatIsThis.ID = "What is this?";
            // 
            // barItemHelpAbout
            // 
            this.barItemHelpAbout.CategoryIndex = 5;
            this.barItemHelpAbout.ID = "About";
            this.barItemHelpAbout.Click += new System.EventHandler(this.barItemHelpAbout_Click);
            // 
            // parentBarItem2
            // 
            this.parentBarItem2.CategoryIndex = 8;
            this.parentBarItem2.ID = "KBShortCuts";
            this.parentBarItem2.Items.AddRange(new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem[] {
            this.barItemDepthDown,
            this.barItemDepthUp,
            this.barItemGainUp,
            this.barItemGainDown});
            this.parentBarItem2.Visible = false;
            // 
            // barItemDepthDown
            // 
            this.barItemDepthDown.CategoryIndex = 7;
            this.barItemDepthDown.ID = "Depth Down";
            this.barItemDepthDown.Shortcut = System.Windows.Forms.Shortcut.F1;
            this.barItemDepthDown.Click += new System.EventHandler(this.barItemDepthDown_Click);
            // 
            // barItemDepthUp
            // 
            this.barItemDepthUp.CategoryIndex = 7;
            this.barItemDepthUp.ID = "Depth Up";
            this.barItemDepthUp.Shortcut = System.Windows.Forms.Shortcut.F2;
            this.barItemDepthUp.Click += new System.EventHandler(this.barItemDepthUp_Click);
            // 
            // barItemGainUp
            // 
            this.barItemGainUp.CategoryIndex = 7;
            this.barItemGainUp.ID = "Gain Up";
            this.barItemGainUp.Shortcut = System.Windows.Forms.Shortcut.F4;
            this.barItemGainUp.Click += new System.EventHandler(this.barItemGainUp_Click);
            // 
            // barItemGainDown
            // 
            this.barItemGainDown.CategoryIndex = 7;
            this.barItemGainDown.ID = "Gain Down";
            this.barItemGainDown.Shortcut = System.Windows.Forms.Shortcut.F3;
            this.barItemGainDown.Click += new System.EventHandler(this.barItemGainDown_Click);
            // 
            // commandBar1
            // 
            this.commandBar1.AllowedDockBorders = Syncfusion.Windows.Forms.Tools.CommandBarDockBorder.Top;
            this.commandBar1.BackColor = System.Drawing.Color.Transparent;
            this.commandBar1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("commandBar1.BackgroundImage")));
            this.commandBar1.Controls.Add(this.toolBarPanel1);
            this.commandBar1.DisableFloating = true;
            this.commandBar1.DockState = Syncfusion.Windows.Forms.Tools.CommandBarDockState.Top;
            this.commandBar1.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.World);
            this.commandBar1.HideDropDownButton = true;
            this.commandBar1.HideGripper = true;
            this.commandBar1.MaxLength = 200;
            this.commandBar1.MinHeight = 36;
            this.commandBar1.MinLength = 50;
            this.commandBar1.Name = "commandBar1";
            this.commandBar1.OccupyFullRow = true;
            this.commandBar1.RowIndex = 1;
            this.commandBar1.RowOffset = 0;
            this.commandBar1.TabIndex = 9;
            this.commandBar1.TabStop = false;
            // 
            // toolBarPanel1
            // 
            this.toolBarPanel1.BackColor = System.Drawing.SystemColors.Control;
            this.toolBarPanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("toolBarPanel1.BackgroundImage")));
            this.toolBarPanel1.Location = new System.Drawing.Point(2, 1);
            this.toolBarPanel1.Name = "toolBarPanel1";
            this.toolBarPanel1.Size = new System.Drawing.Size(1014, 34);
            this.toolBarPanel1.TabIndex = 0;
            // 
            // barItemDebug
            // 
            this.barItemDebug.CategoryIndex = 4;
            this.barItemDebug.ID = "DSP Debug tools";
            this.barItemDebug.Click += new System.EventHandler(this.barItemDebug_Click);
            // 
            // barItemFreqDown
            // 
            this.barItemFreqDown.CategoryIndex = 7;
            this.barItemFreqDown.ID = "FreqDown";
            this.barItemFreqDown.Shortcut = System.Windows.Forms.Shortcut.F5;
            this.barItemFreqDown.Click += new System.EventHandler(this.barItemRangeDown_Click);
            // 
            // barItemFreqUp
            // 
            this.barItemFreqUp.CategoryIndex = 7;
            this.barItemFreqUp.ID = "FreqUp";
            this.barItemFreqUp.Shortcut = System.Windows.Forms.Shortcut.F6;
            this.barItemFreqUp.Click += new System.EventHandler(this.barItemRangeUp_Click);
            // 
            // barItemPowerDown
            // 
            this.barItemPowerDown.CategoryIndex = 7;
            this.barItemPowerDown.ID = "PowerDown";
            this.barItemPowerDown.Shortcut = System.Windows.Forms.Shortcut.F7;
            this.barItemPowerDown.Click += new System.EventHandler(this.barItemPowerDown_Click);
            // 
            // barItemPowerUp
            // 
            this.barItemPowerUp.CategoryIndex = 7;
            this.barItemPowerUp.ID = "PowerUp";
            this.barItemPowerUp.Shortcut = System.Windows.Forms.Shortcut.F8;
            this.barItemPowerUp.Click += new System.EventHandler(this.barItemPowerUp_Click);
            // 
            // barItemWidthDown
            // 
            this.barItemWidthDown.CategoryIndex = 7;
            this.barItemWidthDown.ID = "Width Down";
            this.barItemWidthDown.Shortcut = System.Windows.Forms.Shortcut.F9;
            this.barItemWidthDown.Click += new System.EventHandler(this.barItemWidthDown_Click);
            // 
            // barItemWidthUp
            // 
            this.barItemWidthUp.CategoryIndex = 7;
            this.barItemWidthUp.ID = "WidhtUp";
            this.barItemWidthUp.Shortcut = System.Windows.Forms.Shortcut.F10;
            this.barItemWidthUp.Click += new System.EventHandler(this.barItemWidthUp_Click);
            // 
            // barItemThumpDown
            // 
            this.barItemThumpDown.CategoryIndex = 7;
            this.barItemThumpDown.ID = "ThumpDown";
            this.barItemThumpDown.Shortcut = System.Windows.Forms.Shortcut.F11;
            this.barItemThumpDown.Click += new System.EventHandler(this.barItemThumpDown_Click);
            // 
            // barItemThumpUp
            // 
            this.barItemThumpUp.CategoryIndex = 7;
            this.barItemThumpUp.ID = "ThumpUp";
            this.barItemThumpUp.Shortcut = System.Windows.Forms.Shortcut.F12;
            this.barItemThumpUp.Click += new System.EventHandler(this.barItemThumpUp_Click);
            // 
            // barItemPanelHits
            // 
            this.barItemPanelHits.CategoryIndex = 7;
            this.barItemPanelHits.ID = "PanelHits";
            this.barItemPanelHits.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftH;
            this.barItemPanelHits.Visible = false;
            this.barItemPanelHits.Click += new System.EventHandler(this.barItemPanelHits_Click);
            // 
            // barItem2
            // 
            this.barItem2.CategoryIndex = 3;
            this.barItem2.Checked = true;
            this.barItem2.ID = "Freeze";
            // 
            // barItemSave
            // 
            this.barItemSave.CategoryIndex = 3;
            this.barItemSave.ID = "Save";
            // 
            // barItemExportToFile
            // 
            this.barItemExportToFile.CategoryIndex = -1;
            this.barItemExportToFile.ID = "_2";
            // 
            // barItemZipRawDataFiles
            // 
            this.barItemZipRawDataFiles.CategoryIndex = 4;
            this.barItemZipRawDataFiles.ID = "Zip Raw-Data Files";
            this.barItemZipRawDataFiles.Click += new System.EventHandler(this.barItemZipRawDataFiles_Click);
            // 
            // barItem4
            // 
            this.barItem4.CategoryIndex = -1;
            this.barItem4.ID = "_3";
            // 
            // imageListToolBar
            // 
            this.imageListToolBar.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListToolBar.ImageStream")));
            this.imageListToolBar.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListToolBar.Images.SetKeyName(0, "");
            this.imageListToolBar.Images.SetKeyName(1, "");
            this.imageListToolBar.Images.SetKeyName(2, "");
            this.imageListToolBar.Images.SetKeyName(3, "");
            this.imageListToolBar.Images.SetKeyName(4, "");
            this.imageListToolBar.Images.SetKeyName(5, "");
            this.imageListToolBar.Images.SetKeyName(6, "");
            this.imageListToolBar.Images.SetKeyName(7, "");
            this.imageListToolBar.Images.SetKeyName(8, "");
            this.imageListToolBar.Images.SetKeyName(9, "");
            this.imageListToolBar.Images.SetKeyName(10, "");
            this.imageListToolBar.Images.SetKeyName(11, "");
            this.imageListToolBar.Images.SetKeyName(12, "");
            this.imageListToolBar.Images.SetKeyName(13, "");
            this.imageListToolBar.Images.SetKeyName(14, "");
            this.imageListToolBar.Images.SetKeyName(15, "");
            this.imageListToolBar.Images.SetKeyName(16, "");
            // 
            // timerPanels
            // 
            this.timerPanels.Interval = 50;
            this.timerPanels.Tick += new System.EventHandler(this.timerPanels_Tick);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "");
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(1018, 748);
            this.ControlBox = false;
            this.Controls.Add(this.dockingClientPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.KeyPreview = true;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.MainForm_Closing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.splashPanel4.ResumeLayout(false);
            this.panelMiddle.ResumeLayout(false);
            this.panel0.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dockingManager1)).EndInit();
            this.panelHits.ResumeLayout(false);
            this.panelBV.ResumeLayout(false);
            this.panelEvents.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panelFFT.ResumeLayout(false);
            this.panelFFTEnergy.ResumeLayout(false);
            this.dockingClientPanel1.ResumeLayout(false);
            this.panelRight.ResumeLayout(false);
            this.panelHITS2.ResumeLayout(false);
            this.panelHeaderRight.ResumeLayout(false);
            this.panelLeft.ResumeLayout(false);
            this.panelHeaderLeft.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainFrameBarManager1)).EndInit();
            this.commandBar1.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        #endregion

        private System.Windows.Forms.Panel panelMainCharts;
        private System.Windows.Forms.Panel panelRightCharts;
        private System.Windows.Forms.Splitter splitterBottom;
        private System.Windows.Forms.Panel panelTrend;
        private System.Windows.Forms.Panel panelMiddle;
        private Syncfusion.Windows.Forms.Tools.DockingManager dockingManager1;
        private Syncfusion.Windows.Forms.Tools.DockingClientPanel dockingClientPanel1;
        private System.Windows.Forms.Panel panelHits;
        private System.Windows.Forms.Panel panelEvents;
        private System.Windows.Forms.Panel panelBV;
        private System.Windows.Forms.Panel panelFFT;
        private Syncfusion.Windows.Forms.Tools.XPMenus.MainFrameBarManager mainFrameBarManager1;
        private Syncfusion.Windows.Forms.Tools.XPMenus.Bar barMainMenu;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemPatientNew;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemPatientLoad;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemPatientSearch;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemPatientDelete;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemPatientPrintPreview;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemPrint;
        //private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemMinimize;
        private Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem parentBarItemPatient;
        private Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem parentBarItemStudies;
        private Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem parentBarItemSetup;
        private Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem parentBarItemFunctions;
        private Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem parentBarItemUtilities;
        private Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem parentBarItemHelp;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemSetupGeneral;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemSetupStudies;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemHelpContent;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemHelpIndex;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemHelpSearch;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemHelpAbout;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemNewSpectrum;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemNewTrend;
        private Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem parentBarItem1;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemBackup;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemRestore;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemSaveStudy;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemDeleteStudy;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panelHeaderRight;
        private System.Windows.Forms.ImageList imageListMenu;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemUnFreeze;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItem1;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemNextFunction;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem BarItem_UpZeroLine;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItem_DownZeroLine;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemGainUp;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemGainDown;
        private Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem parentBarItem2;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemDepthUp;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemDepthDown;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemFreqDown;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemFreqUp;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemPowerDown;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemPowerUp;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemWidthDown;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemWidthUp;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemThumpDown;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemThumpUp;
        private System.Windows.Forms.ImageList imageListToolBar;
        private System.ComponentModel.IContainer components;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Panel panelHeaderLeft;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panelLeftCharts;
        private System.Windows.Forms.Splitter splitterRight;
        private System.Windows.Forms.Splitter splitterLeft;

        //		private TCD2003.IF.SerialPortContainer m_SerialPortContainer = new TCD2003.IF.SerialPortContainer();
        private System.Windows.Forms.Timer timerPanels;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemPanelHits;
        private System.Windows.Forms.Panel panelHITS2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel3;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemHitsDetection;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemAddEvent;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemDeleteEvent;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemSummaryScreen;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItem2;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemHelpWhatIsThis;
        private Syncfusion.Windows.Forms.Tools.CommandBar commandBar1;
        public TCD2003.GUI.ToolBarPanel toolBarPanel1;
        private System.Windows.Forms.ComboBox comboBoxTimeRight;
        private System.Windows.Forms.ComboBox comboBoxTimeLeft;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label15;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemCursors;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemNotes;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemPatientReport;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemAutoscan;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemRecord;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemStop;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemPlay;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemSave;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemExpandClinicalParameters;
        private TCD2003.GUI.HitsHistogram hitsHistogram1;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Label labelE1;
        private System.Windows.Forms.Label labelE2;
        private System.Windows.Forms.Panel panelFFTEnergy;
        //private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemNextFunction;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem SaveDebugData;
        //EventWork
        public TCD2003.GUI.BVListViewCtrl bvListViewCtrl1;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemNextBV;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemPrevBV;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemExportToFile;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemAuthorizationMgr;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemDebug;
        //private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemAddPrinter;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemExitWindows;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemExit;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemAdminExitWindows;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemCancelUtilitiesMenu;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemZipRawDataFiles;

        private System.Windows.Forms.Panel panel0;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel0Charts;
        private System.Windows.Forms.Panel panel1Charts;
        private System.Windows.Forms.Panel panel0Trend;
        private System.Windows.Forms.Panel panel1Trend;
        private System.Windows.Forms.Panel panelAutoscan0;
        private System.Windows.Forms.Panel panelAutoscan1;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemPatientReportWizard;
        private System.Windows.Forms.Splitter splitterBottom2;
        private Syncfusion.Windows.Forms.Tools.SplashPanel splashPanel4;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Label labelSplash;
        private System.Windows.Forms.HelpProvider helpProvider1;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemSaveDebugFFT;
        private Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem barItemPatientExport;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemExportSelectedSpectrum;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemExportFullScreen;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemExportTrends;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemExportLog;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItem4;
        private System.Windows.Forms.ImageList imageList1;
//#if (!DEBUG)
//        private Syncfusion.Windows.Forms.Tools.SplashControl splashControl1;
//#endif
        private TCD2003.GUI.EventListViewCtrl eventListViewCtrl1;
        private System.Windows.Forms.TextBox textBox1;
        private ReportMgr.CachedcrTableRep cachedcrTableRep1;
    }
}