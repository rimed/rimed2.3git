namespace TCD2003.GUI
{
    public partial class ToolBarPanel
    {
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToolBarPanel));
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.imageListToolBar = new System.Windows.Forms.ImageList(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.timeLabel = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.comboBoxSensitivitySecondary = new System.Windows.Forms.ComboBox();
            this.comboBoxSensitivity = new System.Windows.Forms.ComboBox();
            this.comboBoxTime = new System.Windows.Forms.ComboBox();
            this.buttonScrollForward = new TCD2003.GUI.ToolBarBtn();
            this.buttonScrollBackWard = new TCD2003.GUI.ToolBarBtn();
            this.comboBoxProbes = new TCD2003.GUI.ProbesCombobox();
            this.buttonPatientRep = new TCD2003.GUI.ToolBarBtn();
            this.buttonPlay = new TCD2003.GUI.ToolBarBtn();
            this.buttonFreeze = new TCD2003.GUI.ToolBarBtn();
            this.buttonLoad = new TCD2003.GUI.ToolBarBtn();
            this.buttonAddTrend = new TCD2003.GUI.ToolBarBtn();
            this.buttonClinicalParameters = new TCD2003.GUI.ToolBarBtn();
            this.buttonReturn = new TCD2003.GUI.ToolBarBtn();
            this.buttonPause = new TCD2003.GUI.ToolBarBtn();
            this.buttonStartRecording = new TCD2003.GUI.ToolBarBtn();
            this.buttonSendTo = new TCD2003.GUI.ToolBarBtn();
            this.buttonSave = new TCD2003.GUI.ToolBarBtn();
            this.buttonAutoscan = new TCD2003.GUI.ToolBarBtn();
            this.buttonAddSpectrum = new TCD2003.GUI.ToolBarBtn();
            this.buttonHitsDetection = new TCD2003.GUI.ToolBarBtn();
            this.buttonNextFunction = new TCD2003.GUI.ToolBarBtn();
            this.buttonSummary = new TCD2003.GUI.ToolBarBtn();
            this.buttonNotes = new TCD2003.GUI.ToolBarBtn();
            this.buttonCursors = new TCD2003.GUI.ToolBarBtn();
            this.buttonPrint = new TCD2003.GUI.ToolBarBtn();
            this.buttonNewPatient = new TCD2003.GUI.ToolBarBtn();
            this.buttonStudies = new TCD2003.GUI.ToolBarBtn();
            this.autoLabel1 = new Syncfusion.Windows.Forms.Tools.AutoLabel();
            this.buttonStopRecording = new TCD2003.GUI.ToolBarBtn();
            this.buttonTD = new TCD2003.GUI.ToolBarBtn();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(1372, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(128, 36);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(18, 36);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // imageListToolBar
            // 
            this.imageListToolBar.ColorDepth = System.Windows.Forms.ColorDepth.Depth24Bit;
            this.imageListToolBar.ImageSize = new System.Drawing.Size(46, 36);
            this.imageListToolBar.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListToolBar.ImageStream")));
            this.imageListToolBar.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.Controls.Add(this.timeLabel);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1500, 36);
            this.panel1.TabIndex = 50;
            // 
            // timeLabel
            // 
            this.timeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeLabel.Location = new System.Drawing.Point(1384, 8);
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(104, 23);
            this.timeLabel.TabIndex = 52;
            this.timeLabel.Text = "12:00:00 PM";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.comboBoxSensitivitySecondary);
            this.panel2.Controls.Add(this.comboBoxSensitivity);
            this.panel2.Controls.Add(this.comboBoxTime);
            this.panel2.Controls.Add(this.buttonScrollForward);
            this.panel2.Controls.Add(this.buttonScrollBackWard);
            this.panel2.Controls.Add(this.comboBoxProbes);
            this.panel2.Controls.Add(this.buttonPatientRep);
            this.panel2.Controls.Add(this.buttonPlay);
            this.panel2.Controls.Add(this.buttonFreeze);
            this.panel2.Controls.Add(this.buttonLoad);
            this.panel2.Controls.Add(this.buttonAddTrend);
            this.panel2.Controls.Add(this.buttonClinicalParameters);
            this.panel2.Controls.Add(this.buttonReturn);
            this.panel2.Controls.Add(this.buttonPause);
            this.panel2.Controls.Add(this.buttonStartRecording);
            this.panel2.Controls.Add(this.buttonSendTo);
            this.panel2.Controls.Add(this.buttonSave);
            this.panel2.Controls.Add(this.buttonAutoscan);
            this.panel2.Controls.Add(this.buttonAddSpectrum);
            this.panel2.Controls.Add(this.buttonHitsDetection);
            this.panel2.Controls.Add(this.buttonNextFunction);
            this.panel2.Controls.Add(this.buttonSummary);
            this.panel2.Controls.Add(this.buttonNotes);
            this.panel2.Controls.Add(this.buttonCursors);
            this.panel2.Controls.Add(this.buttonPrint);
            this.panel2.Controls.Add(this.buttonNewPatient);
            this.panel2.Controls.Add(this.buttonStudies);
            this.panel2.Controls.Add(this.autoLabel1);
            this.panel2.Controls.Add(this.buttonStopRecording);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(18, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1354, 36);
            this.panel2.TabIndex = 51;
            // 
            // comboBoxSensitivitySecondary
            // 
            this.comboBoxSensitivitySecondary.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxSensitivitySecondary.BackColor = System.Drawing.Color.Yellow;
            this.comboBoxSensitivitySecondary.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSensitivitySecondary.ForeColor = System.Drawing.SystemColors.ControlText;
            this.comboBoxSensitivitySecondary.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.comboBoxSensitivitySecondary.Location = new System.Drawing.Point(1248, 8);
            this.comboBoxSensitivitySecondary.Name = "comboBoxSensitivitySecondary";
            this.comboBoxSensitivitySecondary.Size = new System.Drawing.Size(48, 21);
            this.comboBoxSensitivitySecondary.TabIndex = 35;
            this.comboBoxSensitivitySecondary.SelectedIndexChanged += new System.EventHandler(this.comboBoxSensitivitySecondary_SelectedIndexChanged);
            this.comboBoxSensitivitySecondary.SelectionChangeCommitted += new System.EventHandler(this.comboBoxSensitivitySecondary_SelectionChangeCommitted);
            // 
            // comboBoxSensitivity
            // 
            this.comboBoxSensitivity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxSensitivity.BackColor = System.Drawing.Color.Green;
            this.comboBoxSensitivity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSensitivity.ForeColor = System.Drawing.SystemColors.ControlText;
            this.comboBoxSensitivity.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.comboBoxSensitivity.Location = new System.Drawing.Point(1192, 8);
            this.comboBoxSensitivity.Name = "comboBoxSensitivity";
            this.comboBoxSensitivity.Size = new System.Drawing.Size(48, 21);
            this.comboBoxSensitivity.TabIndex = 34;
            this.comboBoxSensitivity.SelectedIndexChanged += new System.EventHandler(this.comboBoxSensitivity_SelectedIndexChanged);
            this.comboBoxSensitivity.SelectionChangeCommitted += new System.EventHandler(this.comboBoxSensitivity_SelectionChangeCommitted);
            // 
            // comboBoxTime
            // 
            this.comboBoxTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxTime.BackColor = System.Drawing.SystemColors.Window;
            this.comboBoxTime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTime.ForeColor = System.Drawing.SystemColors.ControlText;
            this.comboBoxTime.Location = new System.Drawing.Point(1304, 8);
            this.comboBoxTime.Name = "comboBoxTime";
            this.comboBoxTime.Size = new System.Drawing.Size(48, 21);
            this.comboBoxTime.TabIndex = 0;
            // 
            // buttonScrollForward
            // 
            this.buttonScrollForward.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonScrollForward.ImageIndex = 78;
            this.buttonScrollForward.Images = this.imageListToolBar;
            this.buttonScrollForward.Location = new System.Drawing.Point(1104, 0);
            this.buttonScrollForward.Name = "buttonScrollForward";
            this.buttonScrollForward.Size = new System.Drawing.Size(46, 36);
            this.buttonScrollForward.TabIndex = 33;
            this.buttonScrollForward.ToolTip = "Forward";
            // 
            // buttonScrollBackWard
            // 
            this.buttonScrollBackWard.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonScrollBackWard.ImageIndex = 75;
            this.buttonScrollBackWard.Images = this.imageListToolBar;
            this.buttonScrollBackWard.Location = new System.Drawing.Point(1058, 0);
            this.buttonScrollBackWard.Name = "buttonScrollBackWard";
            this.buttonScrollBackWard.Size = new System.Drawing.Size(46, 36);
            this.buttonScrollBackWard.TabIndex = 32;
            this.buttonScrollBackWard.ToolTip = "Backward";
            // 
            // comboBoxProbes
            // 
            this.comboBoxProbes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxProbes.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.comboBoxProbes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxProbes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.comboBoxProbes.ItemHeight = 22;
            this.comboBoxProbes.Location = new System.Drawing.Point(1152, 4);
            this.comboBoxProbes.Name = "comboBoxProbes";
            this.comboBoxProbes.Size = new System.Drawing.Size(112, 28);
            this.comboBoxProbes.TabIndex = 31;
            // 
            // buttonPatientRep
            // 
            this.buttonPatientRep.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonPatientRep.ImageIndex = 69;
            this.buttonPatientRep.Images = this.imageListToolBar;
            this.buttonPatientRep.Location = new System.Drawing.Point(1012, 0);
            this.buttonPatientRep.Name = "buttonPatientRep";
            this.buttonPatientRep.Size = new System.Drawing.Size(46, 36);
            this.buttonPatientRep.TabIndex = 30;
            this.buttonPatientRep.ToolTip = "Patient Report";
            // 
            // buttonPlay
            // 
            this.buttonPlay.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonPlay.ImageIndex = 66;
            this.buttonPlay.Images = this.imageListToolBar;
            this.buttonPlay.Location = new System.Drawing.Point(966, 0);
            this.buttonPlay.Name = "buttonPlay";
            this.buttonPlay.Size = new System.Drawing.Size(46, 36);
            this.buttonPlay.TabIndex = 29;
            this.buttonPlay.ToolTip = "Replay";
            // 
            // buttonFreeze
            // 
            this.buttonFreeze.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonFreeze.EnableToggle = true;
            this.buttonFreeze.ImageIndex = 63;
            this.buttonFreeze.Images = this.imageListToolBar;
            this.buttonFreeze.Location = new System.Drawing.Point(920, 0);
            this.buttonFreeze.Name = "buttonFreeze";
            this.buttonFreeze.Size = new System.Drawing.Size(46, 36);
            this.buttonFreeze.TabIndex = 28;
            this.buttonFreeze.ToolTip = "Freeze/Unfreeze";
            // 
            // buttonLoad
            // 
            this.buttonLoad.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonLoad.ImageIndex = 6;
            this.buttonLoad.Images = this.imageListToolBar;
            this.buttonLoad.Location = new System.Drawing.Point(874, 0);
            this.buttonLoad.Name = "buttonLoad";
            this.buttonLoad.Size = new System.Drawing.Size(46, 36);
            this.buttonLoad.TabIndex = 27;
            this.buttonLoad.ToolTip = "Load";
            // 
            // buttonAddTrend
            // 
            this.buttonAddTrend.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonAddTrend.ImageIndex = 57;
            this.buttonAddTrend.Images = this.imageListToolBar;
            this.buttonAddTrend.Location = new System.Drawing.Point(828, 0);
            this.buttonAddTrend.Name = "buttonAddTrend";
            this.buttonAddTrend.Size = new System.Drawing.Size(46, 36);
            this.buttonAddTrend.TabIndex = 26;
            this.buttonAddTrend.ToolTip = "Add Trend";
            // 
            // buttonClinicalParameters
            // 
            this.buttonClinicalParameters.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonClinicalParameters.ImageIndex = 54;
            this.buttonClinicalParameters.Images = this.imageListToolBar;
            this.buttonClinicalParameters.Location = new System.Drawing.Point(782, 0);
            this.buttonClinicalParameters.Name = "buttonClinicalParameters";
            this.buttonClinicalParameters.Size = new System.Drawing.Size(46, 36);
            this.buttonClinicalParameters.TabIndex = 25;
            this.buttonClinicalParameters.ToolTip = "Expand Clinical Parameters";
            // 
            // buttonExit
            // 
            this.buttonReturn.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonReturn.ImageIndex = 51;
            this.buttonReturn.Images = this.imageListToolBar;
            this.buttonReturn.Location = new System.Drawing.Point(736, 0);
            this.buttonReturn.Name = "buttonExit";
            this.buttonReturn.Size = new System.Drawing.Size(46, 36);
            this.buttonReturn.TabIndex = 24;
            this.buttonReturn.ToolTip = "Return";
            // 
            // buttonPause
            // 
            this.buttonPause.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonPause.EnableToggle = true;
            this.buttonPause.ImageIndex = 48;
            this.buttonPause.Images = this.imageListToolBar;
            this.buttonPause.Location = new System.Drawing.Point(690, 0);
            this.buttonPause.Name = "buttonPause";
            this.buttonPause.Size = new System.Drawing.Size(46, 36);
            this.buttonPause.TabIndex = 23;
            this.buttonPause.ToolTip = "Pause";
            // 
            // buttonStartRecording
            // 
            this.buttonStartRecording.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonStartRecording.EnableToggle = true;
            this.buttonStartRecording.ImageIndex = 42;
            this.buttonStartRecording.Images = this.imageListToolBar;
            this.buttonStartRecording.Location = new System.Drawing.Point(644, 0);
            this.buttonStartRecording.Name = "buttonStartRecording";
            this.buttonStartRecording.Size = new System.Drawing.Size(46, 36);
            this.buttonStartRecording.TabIndex = 19;
            this.buttonStartRecording.ToolTip = "Record";
            // 
            // buttonSendTo
            // 
            this.buttonSendTo.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonSendTo.ImageIndex = 36;
            this.buttonSendTo.Images = this.imageListToolBar;
            this.buttonSendTo.Location = new System.Drawing.Point(598, 0);
            this.buttonSendTo.Name = "buttonSendTo";
            this.buttonSendTo.Size = new System.Drawing.Size(46, 36);
            this.buttonSendTo.TabIndex = 17;
            this.buttonSendTo.ToolTip = "Send To...";
            // 
            // buttonSave
            // 
            this.buttonSave.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonSave.ImageIndex = 15;
            this.buttonSave.Images = this.imageListToolBar;
            this.buttonSave.Location = new System.Drawing.Point(552, 0);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(46, 36);
            this.buttonSave.TabIndex = 16;
            this.buttonSave.ToolTip = "Save";
            // 
            // buttonAutoscan
            // 
            this.buttonAutoscan.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonAutoscan.ImageIndex = 33;
            this.buttonAutoscan.Images = this.imageListToolBar;
            this.buttonAutoscan.Location = new System.Drawing.Point(460, 0);
            this.buttonAutoscan.Name = "buttonAutoscan";
            this.buttonAutoscan.Size = new System.Drawing.Size(46, 36);
            this.buttonAutoscan.TabIndex = 14;
            this.buttonAutoscan.ToolTip = "M-Mode";
            // 
            // buttonAddSpectrum
            // 
            this.buttonAddSpectrum.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonAddSpectrum.ImageIndex = 18;
            this.buttonAddSpectrum.Images = this.imageListToolBar;
            this.buttonAddSpectrum.Location = new System.Drawing.Point(414, 0);
            this.buttonAddSpectrum.Name = "buttonAddSpectrum";
            this.buttonAddSpectrum.Size = new System.Drawing.Size(46, 36);
            this.buttonAddSpectrum.TabIndex = 13;
            this.buttonAddSpectrum.ToolTip = "Add Spectrum";
            // 
            // buttonHitsDetection
            // 
            this.buttonHitsDetection.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonHitsDetection.EnableToggle = true;
            this.buttonHitsDetection.ImageIndex = 24;
            this.buttonHitsDetection.Images = this.imageListToolBar;
            this.buttonHitsDetection.Location = new System.Drawing.Point(368, 0);
            this.buttonHitsDetection.Name = "buttonHitsDetection";
            this.buttonHitsDetection.Size = new System.Drawing.Size(46, 36);
            this.buttonHitsDetection.TabIndex = 12;
            this.buttonHitsDetection.ToolTip = "Hits Detection";
            // 
            // buttonNextFunction
            // 
            this.buttonNextFunction.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonNextFunction.ImageIndex = 60;
            this.buttonNextFunction.Images = this.imageListToolBar;
            this.buttonNextFunction.Location = new System.Drawing.Point(322, 0);
            this.buttonNextFunction.Name = "buttonNextFunction";
            this.buttonNextFunction.Size = new System.Drawing.Size(46, 36);
            this.buttonNextFunction.TabIndex = 11;
            this.buttonNextFunction.ToolTip = "Next Function";
            // 
            // buttonSummary
            // 
            this.buttonSummary.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonSummary.ImageIndex = 12;
            this.buttonSummary.Images = this.imageListToolBar;
            this.buttonSummary.Location = new System.Drawing.Point(276, 0);
            this.buttonSummary.Name = "buttonSummary";
            this.buttonSummary.Size = new System.Drawing.Size(46, 36);
            this.buttonSummary.TabIndex = 10;
            this.buttonSummary.ToolTip = "Summary Screen";
            // 
            // buttonNotes
            // 
            this.buttonNotes.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonNotes.ImageIndex = 30;
            this.buttonNotes.Images = this.imageListToolBar;
            this.buttonNotes.Location = new System.Drawing.Point(230, 0);
            this.buttonNotes.Name = "buttonNotes";
            this.buttonNotes.Size = new System.Drawing.Size(46, 36);
            this.buttonNotes.TabIndex = 9;
            this.buttonNotes.ToolTip = "Notes";
            // 
            // buttonCursors
            // 
            this.buttonCursors.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonCursors.EnableToggle = true;
            this.buttonCursors.ImageIndex = 21;
            this.buttonCursors.Images = this.imageListToolBar;
            this.buttonCursors.Location = new System.Drawing.Point(184, 0);
            this.buttonCursors.Name = "buttonCursors";
            this.buttonCursors.Size = new System.Drawing.Size(46, 36);
            this.buttonCursors.TabIndex = 8;
            this.buttonCursors.ToolTip = "Cursors";
            // 
            // buttonPrint
            // 
            this.buttonPrint.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonPrint.ImageIndex = 9;
            this.buttonPrint.Images = this.imageListToolBar;
            this.buttonPrint.Location = new System.Drawing.Point(138, 0);
            this.buttonPrint.Name = "buttonPrint";
            this.buttonPrint.Size = new System.Drawing.Size(46, 36);
            this.buttonPrint.TabIndex = 7;
            this.buttonPrint.ToolTip = "Print";
            // 
            // buttonNewPatient
            // 
            this.buttonNewPatient.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonNewPatient.ImageIndex = 3;
            this.buttonNewPatient.Images = this.imageListToolBar;
            this.buttonNewPatient.Location = new System.Drawing.Point(92, 0);
            this.buttonNewPatient.Name = "buttonNewPatient";
            this.buttonNewPatient.Size = new System.Drawing.Size(46, 36);
            this.buttonNewPatient.TabIndex = 22;
            this.buttonNewPatient.ToolTip = "New Patient";
            // 
            // buttonStudies
            // 
            this.buttonStudies.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonStudies.ImageIndex = 0;
            this.buttonStudies.Images = this.imageListToolBar;
            this.buttonStudies.Location = new System.Drawing.Point(46, 0);
            this.buttonStudies.Name = "buttonStudies";
            this.buttonStudies.Size = new System.Drawing.Size(46, 36);
            this.buttonStudies.TabIndex = 0;
            this.buttonStudies.ToolTip = "New Study";
            // 
            // autoLabel1
            // 
            this.autoLabel1.DX = -36;
            this.autoLabel1.DY = 38;
            this.autoLabel1.LabeledControl = this.panel1;
            this.autoLabel1.Location = new System.Drawing.Point(-36, 38);
            this.autoLabel1.Name = "autoLabel1";
            this.autoLabel1.Size = new System.Drawing.Size(32, 23);
            this.autoLabel1.TabIndex = 1;
            this.autoLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buttonStopRecording
            // 
            this.buttonStopRecording.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonStopRecording.ImageIndex = 45;
            this.buttonStopRecording.Images = this.imageListToolBar;
            this.buttonStopRecording.Location = new System.Drawing.Point(0, 0);
            this.buttonStopRecording.Name = "buttonStopRecording";
            this.buttonStopRecording.Size = new System.Drawing.Size(46, 36);
            this.buttonStopRecording.TabIndex = 20;
            this.buttonStopRecording.ToolTip = "Stop";
            // 
            // ToolBarPanel
            // 
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.Controls.Add(this.panel1);
            this.Name = "ToolBarPanel";
            this.Size = new System.Drawing.Size(1500, 36);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ImageList imageListToolBar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ComboBox comboBoxTime;
        public Syncfusion.Windows.Forms.Tools.AutoLabel autoLabel1;
        private System.Windows.Forms.ToolTip toolTip1;
        private TCD2003.GUI.ToolBarBtn buttonStudies;
        private TCD2003.GUI.ToolBarBtn buttonNewPatient;
        private TCD2003.GUI.ToolBarBtn buttonPrint;
        private TCD2003.GUI.ToolBarBtn buttonCursors;
        private TCD2003.GUI.ToolBarBtn buttonNotes;
        private TCD2003.GUI.ToolBarBtn buttonSummary;
        private TCD2003.GUI.ToolBarBtn buttonNextFunction;
        private TCD2003.GUI.ToolBarBtn buttonHitsDetection;
        private TCD2003.GUI.ToolBarBtn buttonAddSpectrum;
        private TCD2003.GUI.ToolBarBtn buttonAutoscan;
        private TCD2003.GUI.ToolBarBtn buttonTD;
        private TCD2003.GUI.ToolBarBtn buttonSave;
        private TCD2003.GUI.ToolBarBtn buttonSendTo;
        private TCD2003.GUI.ToolBarBtn buttonStartRecording;
        private TCD2003.GUI.ToolBarBtn buttonStopRecording;
        private TCD2003.GUI.ToolBarBtn buttonPause;
        private TCD2003.GUI.ToolBarBtn buttonReturn;
        private TCD2003.GUI.ToolBarBtn buttonClinicalParameters;
        private TCD2003.GUI.ToolBarBtn buttonAddTrend;
        private TCD2003.GUI.ToolBarBtn buttonLoad;
        private TCD2003.GUI.ToolBarBtn buttonFreeze;
        private TCD2003.GUI.ToolBarBtn buttonPlay;
        private TCD2003.GUI.ToolBarBtn buttonPatientRep;
        private ProbesCombobox comboBoxProbes;
        private TCD2003.GUI.ToolBarBtn buttonScrollBackWard;
        private TCD2003.GUI.ToolBarBtn buttonScrollForward;
        private System.Windows.Forms.Label timeLabel;
        private System.Windows.Forms.ComboBox comboBoxSensitivity;
        private System.Windows.Forms.ComboBox comboBoxSensitivitySecondary;
        private System.ComponentModel.IContainer components;
    }
}
