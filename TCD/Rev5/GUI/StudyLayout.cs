using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;


namespace TCD2003.GUI
{
	/// <summary>
	/// Summary description for StudyLayout.
	/// </summary>
	public class StudyLayout
	{
		public StudyLayout()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public int m_GateNumLeft = 0;
		public int m_GateNumRight = 0;
	    private Guid m_ProbeLeftIndex = Guid.Empty;
	    private Guid m_ProbeRightIndex = Guid.Empty;
		public bool m_DispAutoscan0 = false;
		public bool m_DispAutoscan1 = false;
		public void Clear()
		{
			m_GateNumLeft = 0;
			m_GateNumRight = 0;
			m_ProbeLeftIndex = Guid.Empty;
			m_ProbeRightIndex = Guid.Empty;
			m_DispAutoscan0 = false;
			m_DispAutoscan1 = false;
		}
		public void Serialize( Stream ms )
		{
			IFormatter formatter = new BinaryFormatter();
			formatter.Serialize(ms, m_GateNumLeft);
			formatter.Serialize(ms, m_GateNumRight);
			formatter.Serialize(ms, m_ProbeLeftIndex);
			formatter.Serialize(ms, m_ProbeRightIndex);
			formatter.Serialize(ms, m_DispAutoscan0);
			formatter.Serialize(ms, m_DispAutoscan1);
		}
		public void Deserialize( Stream ms )
		{
			IFormatter formatter = new BinaryFormatter();
			m_GateNumLeft = (int)formatter.Deserialize(ms);
			m_GateNumRight = (int)formatter.Deserialize(ms);
			m_ProbeLeftIndex = (Guid)formatter.Deserialize(ms);
			m_ProbeRightIndex = (Guid)formatter.Deserialize(ms);
			m_DispAutoscan0 = (bool)formatter.Deserialize(ms);
			m_DispAutoscan1 = (bool)formatter.Deserialize(ms);
		}
	}
}
