using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

using TCD2003.DB;
using TCD2003.GUI.Setup;
using TCD2003.Utils;

namespace TCD2003.GUI
{
    /// <summary>
    /// Summary description for ProbeListDlg.
    /// </summary>
    public partial class ProbeListDlg : Form
    {
        private List<ProbeComboBox> m_probeItems = new List<ProbeComboBox>();
        private DataView m_dvBVProbe;
        private Guid m_bvIndex;

        /// <summary>
        /// Constructor
        /// </summary>
        public ProbeListDlg()
        {
            this.InitializeComponent();
            this.dsProbe1.Merge(RimedDal.Instance.s_dsProbe);

            var strRes = ResourceManagerWrapper.GetWrapper(MainForm.ResourceManager); 
            this.buttonOK.Text = strRes.GetString("OK");
            this.buttonCancel.Text = strRes.GetString("Cancel");
            this.buttonHelp.Text = strRes.GetString("Help");
            this.Text = strRes.GetString("Probe List");
            this.listBox1.DisplayMember = "Name";
        }

        public Guid BVIndex
        {
            set
            {
                this.m_bvIndex = value;
            }
        }

        private void ProbeListDlg_Load(object sender, EventArgs e)
        {
            this.m_dvBVProbe = RimedDal.Instance.GetProbesByBVIndex(this.m_bvIndex);
            foreach (DB.dsProbe.tb_ProbeRow row in this.dsProbe1.tb_Probe.Rows)
            {
                // fill the list box with the all the probes that exist in the system.
                Guid probeIndex = (Guid)row["ProbeIndex"];
                ProbeComboBox item = new ProbeComboBox(probeIndex, (String)row["ProbeName"]);
                this.m_probeItems.Add(item);
                int itemIndex = this.listBox1.Items.Add(item);

                // mark the probes that allready refers to the selected BV.
                foreach (DataRowView r in this.m_dvBVProbe)
                {
                    if (probeIndex == (Guid)r["Probe_Index"] && true == (bool)r["Valid"])
                        this.listBox1.SetSelected(itemIndex, true);
                }
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            ExceptionPublisherLog4Net.FormActionLog("buttonOK", this.Name, null);

            // delete all the rows that refers to the selected BV.
            RimedDal.Instance.DeleteBVProbesByBVIndex(this.m_bvIndex);

            // update the DB refers to the results of the dialog
            ListBox.SelectedIndexCollection indexCollection = this.listBox1.SelectedIndices;
            int indexInBV = 0;
            foreach (int i in indexCollection)
            {
                RimedDal.Instance.AddProbe2BV(this.m_bvIndex, m_probeItems[i].ProbeIndex, indexInBV);
                indexInBV++;
            }
        }
    }
}
