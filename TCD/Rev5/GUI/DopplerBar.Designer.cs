namespace TCD2003.GUI
{
    partial class DopplerBar
    {
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DopplerBar));
            this.imageListVolume = new System.Windows.Forms.ImageList(this.components);
            this.pictureBoxVolume = new System.Windows.Forms.PictureBox();
            this.dopplerVarLabelDepth = new TCD2003.GUI.DopplerValLabelVertical();
            this.dopplerVarLabelGain = new TCD2003.GUI.DopplerVarLabel();
            this.dopplerVarLabelRange = new TCD2003.GUI.DopplerVarLabel();
            this.dopplerVarLabelWidth = new TCD2003.GUI.DopplerVarLabel();
            this.dopplerVarLabelThump = new TCD2003.GUI.DopplerVarLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBoxHeartRate = new System.Windows.Forms.PictureBox();
            this.autoLabelHeartRate = new System.Windows.Forms.Label();
            this.labelBV = new System.Windows.Forms.Label();
            this.dopplerVarLabelPower = new TCD2003.GUI.DopplerVarLabel();
            this.probeIndication1 = new TCD2003.GUI.ProbeIndication();
            this.picManualCalculations = new System.Windows.Forms.PictureBox();
            this.cachedcrTableRep1 = new TCD2003.ReportMgr.CachedcrTableRep();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxVolume)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHeartRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picManualCalculations)).BeginInit();
            this.SuspendLayout();
            // 
            // imageListVolume
            // 
            this.imageListVolume.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListVolume.ImageStream")));
            this.imageListVolume.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListVolume.Images.SetKeyName(0, "");
            this.imageListVolume.Images.SetKeyName(1, "");
            this.imageListVolume.Images.SetKeyName(2, "");
            this.imageListVolume.Images.SetKeyName(3, "");
            this.imageListVolume.Images.SetKeyName(4, "");
            this.imageListVolume.Images.SetKeyName(5, "");
            this.imageListVolume.Images.SetKeyName(6, "");
            this.imageListVolume.Images.SetKeyName(7, "");
            this.imageListVolume.Images.SetKeyName(8, "");
            this.imageListVolume.Images.SetKeyName(9, "");
            this.imageListVolume.Images.SetKeyName(10, "");
            this.imageListVolume.Images.SetKeyName(11, "");
            this.imageListVolume.Images.SetKeyName(12, "");
            this.imageListVolume.Images.SetKeyName(13, "");
            this.imageListVolume.Images.SetKeyName(14, "");
            this.imageListVolume.Images.SetKeyName(15, "");
            this.imageListVolume.Images.SetKeyName(16, "");
            this.imageListVolume.Images.SetKeyName(17, "");
            this.imageListVolume.Images.SetKeyName(18, "");
            this.imageListVolume.Images.SetKeyName(19, "");
            this.imageListVolume.Images.SetKeyName(20, "");
            // 
            // pictureBoxVolume
            // 
            this.pictureBoxVolume.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBoxVolume.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxVolume.Image")));
            this.pictureBoxVolume.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxVolume.Name = "pictureBoxVolume";
            this.pictureBoxVolume.Size = new System.Drawing.Size(41, 58);
            this.pictureBoxVolume.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBoxVolume.TabIndex = 1;
            this.pictureBoxVolume.TabStop = false;
            this.pictureBoxVolume.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxVolume_MouseDown);
            // 
            // dopplerVarLabelDepth
            // 
            this.dopplerVarLabelDepth.Active = false;
            this.dopplerVarLabelDepth.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(235)))), ((int)(((byte)(250)))));
            this.dopplerVarLabelDepth.ImageLabel = TCD2003.GUI.GlobalTypes.ImageLabelStyle.ImageAndLabel;
            this.dopplerVarLabelDepth.Location = new System.Drawing.Point(44, 0);
            this.dopplerVarLabelDepth.Name = "dopplerVarLabelDepth";
            this.dopplerVarLabelDepth.Padding = new System.Windows.Forms.Padding(3);
            this.dopplerVarLabelDepth.Size = new System.Drawing.Size(100, 60);
            this.dopplerVarLabelDepth.SizeMode = TCD2003.GUI.GlobalTypes.TSizeMode.Large;
            this.dopplerVarLabelDepth.TabIndex = 2;
            this.dopplerVarLabelDepth.VarText = "Depth";
            this.dopplerVarLabelDepth.VarValue = 55;
            this.dopplerVarLabelDepth.VarValueDouble = 55D;
            this.dopplerVarLabelDepth.Visible = false;
            // 
            // dopplerVarLabelGain
            // 
            this.dopplerVarLabelGain.Active = false;
            this.dopplerVarLabelGain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(235)))), ((int)(((byte)(250)))));
            this.dopplerVarLabelGain.ImageLabel = TCD2003.GUI.GlobalTypes.ImageLabelStyle.ImageAndLabel;
            this.dopplerVarLabelGain.Location = new System.Drawing.Point(129, 0);
            this.dopplerVarLabelGain.Name = "dopplerVarLabelGain";
            this.dopplerVarLabelGain.Padding = new System.Windows.Forms.Padding(3);
            this.dopplerVarLabelGain.Size = new System.Drawing.Size(80, 60);
            this.dopplerVarLabelGain.SizeMode = TCD2003.GUI.GlobalTypes.TSizeMode.Large;
            this.dopplerVarLabelGain.TabIndex = 3;
            this.dopplerVarLabelGain.VarText = "Gain";
            this.dopplerVarLabelGain.VarValue = 4;
            this.dopplerVarLabelGain.VarValueDouble = 4D;
            // 
            // dopplerVarLabelRange
            // 
            this.dopplerVarLabelRange.Active = false;
            this.dopplerVarLabelRange.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(235)))), ((int)(((byte)(250)))));
            this.dopplerVarLabelRange.ImageLabel = TCD2003.GUI.GlobalTypes.ImageLabelStyle.ImageAndLabel;
            this.dopplerVarLabelRange.Location = new System.Drawing.Point(209, 0);
            this.dopplerVarLabelRange.Name = "dopplerVarLabelRange";
            this.dopplerVarLabelRange.Padding = new System.Windows.Forms.Padding(3);
            this.dopplerVarLabelRange.Size = new System.Drawing.Size(80, 60);
            this.dopplerVarLabelRange.SizeMode = TCD2003.GUI.GlobalTypes.TSizeMode.Large;
            this.dopplerVarLabelRange.TabIndex = 4;
            this.dopplerVarLabelRange.VarText = "Scale";
            this.dopplerVarLabelRange.VarValue = 6;
            this.dopplerVarLabelRange.VarValueDouble = 6D;
            // 
            // dopplerVarLabelWidth
            // 
            this.dopplerVarLabelWidth.Active = false;
            this.dopplerVarLabelWidth.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(235)))), ((int)(((byte)(250)))));
            this.dopplerVarLabelWidth.ImageLabel = TCD2003.GUI.GlobalTypes.ImageLabelStyle.ImageAndLabel;
            this.dopplerVarLabelWidth.Location = new System.Drawing.Point(369, 0);
            this.dopplerVarLabelWidth.Name = "dopplerVarLabelWidth";
            this.dopplerVarLabelWidth.Padding = new System.Windows.Forms.Padding(3);
            this.dopplerVarLabelWidth.Size = new System.Drawing.Size(80, 60);
            this.dopplerVarLabelWidth.SizeMode = TCD2003.GUI.GlobalTypes.TSizeMode.Large;
            this.dopplerVarLabelWidth.TabIndex = 6;
            this.dopplerVarLabelWidth.VarText = "Sample";
            this.dopplerVarLabelWidth.VarValue = 15;
            this.dopplerVarLabelWidth.VarValueDouble = 15D;
            // 
            // dopplerVarLabelThump
            // 
            this.dopplerVarLabelThump.Active = false;
            this.dopplerVarLabelThump.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(235)))), ((int)(((byte)(250)))));
            this.dopplerVarLabelThump.ImageLabel = TCD2003.GUI.GlobalTypes.ImageLabelStyle.ImageAndLabel;
            this.dopplerVarLabelThump.Location = new System.Drawing.Point(449, 0);
            this.dopplerVarLabelThump.Name = "dopplerVarLabelThump";
            this.dopplerVarLabelThump.Padding = new System.Windows.Forms.Padding(3);
            this.dopplerVarLabelThump.Size = new System.Drawing.Size(80, 60);
            this.dopplerVarLabelThump.SizeMode = TCD2003.GUI.GlobalTypes.TSizeMode.Large;
            this.dopplerVarLabelThump.TabIndex = 7;
            this.dopplerVarLabelThump.VarText = "Filter";
            this.dopplerVarLabelThump.VarValue = 100;
            this.dopplerVarLabelThump.VarValueDouble = 100D;
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.ForeColor = System.Drawing.Color.Transparent;
            this.panel1.Location = new System.Drawing.Point(41, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1, 58);
            this.panel1.TabIndex = 8;
            // 
            // pictureBoxHeartRate
            // 
            this.pictureBoxHeartRate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxHeartRate.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxHeartRate.Image")));
            this.pictureBoxHeartRate.Location = new System.Drawing.Point(640, 0);
            this.pictureBoxHeartRate.Name = "pictureBoxHeartRate";
            this.pictureBoxHeartRate.Size = new System.Drawing.Size(30, 25);
            this.pictureBoxHeartRate.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBoxHeartRate.TabIndex = 9;
            this.pictureBoxHeartRate.TabStop = false;
            // 
            // autoLabelHeartRate
            // 
            this.autoLabelHeartRate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.autoLabelHeartRate.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.autoLabelHeartRate.Location = new System.Drawing.Point(672, 0);
            this.autoLabelHeartRate.Name = "autoLabelHeartRate";
            this.autoLabelHeartRate.Size = new System.Drawing.Size(56, 25);
            this.autoLabelHeartRate.TabIndex = 10;
            this.autoLabelHeartRate.Text = "HR - 180";
            this.autoLabelHeartRate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelBV
            // 
            this.labelBV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelBV.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.labelBV.Location = new System.Drawing.Point(552, 26);
            this.labelBV.Name = "labelBV";
            this.labelBV.Size = new System.Drawing.Size(100, 22);
            this.labelBV.TabIndex = 12;
            this.labelBV.Text = "MCA-L";
            this.labelBV.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dopplerVarLabelPower
            // 
            this.dopplerVarLabelPower.Active = false;
            this.dopplerVarLabelPower.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(235)))), ((int)(((byte)(250)))));
            this.dopplerVarLabelPower.ImageLabel = TCD2003.GUI.GlobalTypes.ImageLabelStyle.ImageAndLabel;
            this.dopplerVarLabelPower.Location = new System.Drawing.Point(289, 0);
            this.dopplerVarLabelPower.Name = "dopplerVarLabelPower";
            this.dopplerVarLabelPower.Padding = new System.Windows.Forms.Padding(3);
            this.dopplerVarLabelPower.Size = new System.Drawing.Size(80, 60);
            this.dopplerVarLabelPower.SizeMode = TCD2003.GUI.GlobalTypes.TSizeMode.Large;
            this.dopplerVarLabelPower.TabIndex = 13;
            this.dopplerVarLabelPower.VarText = "Power";
            this.dopplerVarLabelPower.VarValue = 100;
            this.dopplerVarLabelPower.VarValueDouble = 100D;
            // 
            // probeIndication1
            // 
            this.probeIndication1.Location = new System.Drawing.Point(664, 32);
            this.probeIndication1.Name = "probeIndication1";
            this.probeIndication1.ProbeColor = System.Drawing.Color.Empty;
            this.probeIndication1.ProbeName = null;
            this.probeIndication1.Size = new System.Drawing.Size(64, 16);
            this.probeIndication1.TabIndex = 16;
            // 
            // picManualCalculations
            // 
            this.picManualCalculations.Image = ((System.Drawing.Image)(resources.GetObject("picManualCalculations.Image")));
            this.picManualCalculations.Location = new System.Drawing.Point(544, 16);
            this.picManualCalculations.Name = "picManualCalculations";
            this.picManualCalculations.Size = new System.Drawing.Size(72, 27);
            this.picManualCalculations.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picManualCalculations.TabIndex = 17;
            this.picManualCalculations.TabStop = false;
            this.picManualCalculations.Click += new System.EventHandler(this.picManualCalculations_Click);
            this.picManualCalculations.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picManualCalculations_MouseDown);
            this.picManualCalculations.MouseLeave += new System.EventHandler(this.picManualCalculations_MouseLeave);
            this.picManualCalculations.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picManualCalculations_MouseUp);
            // 
            // DopplerBar
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(235)))), ((int)(((byte)(250)))));
            this.Controls.Add(this.picManualCalculations);
            this.Controls.Add(this.probeIndication1);
            this.Controls.Add(this.dopplerVarLabelDepth);
            this.Controls.Add(this.labelBV);
            this.Controls.Add(this.autoLabelHeartRate);
            this.Controls.Add(this.pictureBoxHeartRate);
            this.Controls.Add(this.dopplerVarLabelThump);
            this.Controls.Add(this.dopplerVarLabelWidth);
            this.Controls.Add(this.dopplerVarLabelPower);
            this.Controls.Add(this.dopplerVarLabelRange);
            this.Controls.Add(this.dopplerVarLabelGain);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBoxVolume);
            this.Name = "DopplerBar";
            this.Size = new System.Drawing.Size(752, 58);
            this.Load += new System.EventHandler(this.DopplerBar_Load);
            this.SizeChanged += new System.EventHandler(this.DopplerBar_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxVolume)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHeartRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picManualCalculations)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList imageListVolume;
        private System.Windows.Forms.PictureBox pictureBoxVolume;
        private TCD2003.GUI.DopplerVarLabel dopplerVarLabelGain;
        private TCD2003.GUI.DopplerVarLabel dopplerVarLabelRange;
        private TCD2003.GUI.DopplerVarLabel dopplerVarLabelWidth;
        private TCD2003.GUI.DopplerVarLabel dopplerVarLabelThump;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBoxHeartRate;
        private System.Windows.Forms.Label autoLabelHeartRate;
        private System.Windows.Forms.Label labelBV;
        private TCD2003.GUI.DopplerVarLabel dopplerVarLabelPower;
        private System.ComponentModel.IContainer components;
        private TCD2003.GUI.DopplerValLabelVertical dopplerVarLabelDepth;
        private System.Windows.Forms.PictureBox picManualCalculations;
        public TCD2003.GUI.ProbeIndication probeIndication1;
        private ReportMgr.CachedcrTableRep cachedcrTableRep1;
    }
}