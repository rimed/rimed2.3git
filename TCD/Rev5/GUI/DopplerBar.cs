using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

using TCD2003.DB;
using TCD2003.GUI.Images;
using TCD2003.Utils;

namespace TCD2003.GUI
{
    /// <summary>
    /// Summary description for DopplerBar.
    /// Manages the Doppler variable UI and functionality (e.i : Whats happan when the user
    /// pressing on the POWER button 
    /// </summary>
    public partial class DopplerBar : UserControl
    {
        #region Private Fields

        private bool m_fakeRowDataModeSign = false;
        private GlobalTypes.TSizeMode m_sizeMode = GlobalTypes.TSizeMode.Large;
        protected int m_volumeLevel;
        private double m_thump = 0.0;
        private int m_heartRate = 0;
        private string m_bvName = string.Empty;

        #endregion Private Fields

        #region Public Properties

        public PictureBox PicManualCalculations
        {
            get
            {
                return this.picManualCalculations;
            }
        }

        public DopplerValLabelVertical DopplerVarLabelDepth
        {
            get
            {
                return dopplerVarLabelDepth;
            }
        }
        public DopplerVarLabel DopplerVarLabelGain
        {
            get
            {
                return dopplerVarLabelGain;
            }
        }
        public DopplerVarLabel DopplerVarLabelRange
        {
            get
            {
                return dopplerVarLabelRange;
            }
        }
        public DopplerVarLabel DopplerVarLabelWidth
        {
            get
            {
                return dopplerVarLabelWidth;
            }
        }
        public DopplerVarLabel DopplerVarLabelThump
        {
            get
            {
                return dopplerVarLabelThump;
            }
        }
        public DopplerVarLabel DopplerVarLabelPower
        {
            get
            {
                return dopplerVarLabelPower;
            }
        }

        /// <summary>
        /// Volume [0..6]
        /// </summary>
        [Browsable(true)]
        [DefaultValue(0)]
        public int VolumeLevel
        {
            get
            {
                return this.m_volumeLevel;
            }
            set
            {
                this.m_volumeLevel = value;
                if (this.m_volumeLevel < GlobalSettings.MinVolume)
                    this.m_volumeLevel = GlobalSettings.MinVolume;
                if (this.m_volumeLevel > GlobalSettings.MaxVolume)
                    this.m_volumeLevel = GlobalSettings.MaxVolume;
                // select picture

                if (!this.m_fakeRowDataModeSign)
                {
                    if (this.InvokeRequired)
                        this.BeginInvoke((Action)delegate()
                        {
                            this.pictureBoxVolume.Image = imageListVolume.Images[this.m_volumeLevel];
                        });
                    else
                        this.pictureBoxVolume.Image = imageListVolume.Images[this.m_volumeLevel];
                }
            }
        }

        [Browsable(true)]
        [DefaultValue(GlobalTypes.TSizeMode.Large)]
        public GlobalTypes.TSizeMode SizeMode
        {
            get
            {
                return this.m_sizeMode;
            }
            set
            {
                if (this.m_sizeMode != value)
                    this.m_sizeMode = value;
            }
        }

        public double DepthVar
        {
            get
            {
                return this.dopplerVarLabelDepth.VarValueDouble;
            }
            set
            {
                this.dopplerVarLabelDepth.VarValueDouble = value;
            }
        }

        public int GainVar
        {
            get
            {
                return this.dopplerVarLabelGain.VarValue;
            }
            set
            {
                dopplerVarLabelGain.VarValue = value;
            }
        }

        public int RangeVar
        {
            get
            {
                return this.dopplerVarLabelRange.VarValue;
            }
            set
            {
                this.dopplerVarLabelRange.VarValue = value;
            }
        }

        public int PowerVar
        {
            get
            {
                return this.dopplerVarLabelPower.VarValue;
            }
            set
            {
                this.dopplerVarLabelPower.VarValue = value;
            }
        }

        public double WidthVar
        {
            get
            {
                return this.dopplerVarLabelWidth.VarValueDouble;
            }
            set
            {
                this.dopplerVarLabelWidth.VarValueDouble = value;
            }
        }

        /// <summary>
        /// We save the thump doppler variable as double because we display it as int.
        /// </summary>
        public double ThumpVar
        {
            get
            {
                return this.m_thump;
            }
            set
            {
                this.m_thump = value;
                this.dopplerVarLabelThump.VarValue = (int)m_thump;
            }
        }

        public int HeartRate
        {
            get
            {
                return this.m_heartRate;
            }
            set
            {
                this.m_heartRate = value;
                string text = "HR - " + ((this.m_heartRate != 0) ? this.m_heartRate.ToString() : string.Empty);
                if (this.InvokeRequired)
                    this.BeginInvoke((Action)delegate()
                    {
                        this.autoLabelHeartRate.Text = text;
                    });
                else
                    this.autoLabelHeartRate.Text = text;
            }
        }

        public string BVName
        {
            get
            {
                return m_bvName;
            }
            set
            {
                m_bvName = value;
                ExceptionPublisherLog4Net.TraceLog(string.Format("BVName = {0}", value), Name);

                if (InvokeRequired)
                    BeginInvoke((Action)delegate() { labelBV.Text = value; }); 
                else
                    labelBV.Text = value;
            }
        }

        public GlobalTypes.TSpectrumUnits DopplerUnits
        {
            set
            {
                /*
                f (KHz) = [ 2 * F(MHz) * v(cm/sec) *cos(alfa) ] / 154
                f (Hz) = [ 2 * F(MHz) * v(cm/sec) *cos(alfa) * 1000] / 154
                v (cm/sec) = [ 154 * f(KHz) ] / [2 * F(MHz) * cos(alfa)]
                v (cm/sec) = [ 154 * f(Hz) ] / [1000 * 2 * F(MHz) * cos(alfa)]

                Check: 
					
                F=2MHz, f=4KHz, cos=1, v=154cm/sec
                F=2MHz, f=6KHz, cos=1, v=231cm/sec
                F=2MHz, f=8KHz, cos=1, v=308cm/sec
                */
                double cos_pi = 1.0;
                double res = 0;

                double freq = ((GateView)this.Parent).Probe.Freq_MHz;
                if (value == GlobalTypes.TSpectrumUnits.KHz)
                {
                    if (LayoutManager.theLayoutManager.ReplayMode != true)
                        dopplerVarLabelRange.VarValue = ((GateView)this.Parent).Probe.Range;
                    else
                        dopplerVarLabelRange.VarValue = (int)System.Math.Round((RangeVar * 2 * freq * cos_pi) / GlobalSettings.MultiplierForConversionToMHz);
                }
                else
                {
                    res = GlobalSettings.MultiplierForConversionToMHz / (2 * freq * cos_pi);
                    this.dopplerVarLabelRange.VarValue = (int)(res * RangeVar);
                }

                if (value == GlobalTypes.TSpectrumUnits.KHz)
                    res = ((GateView)this.Parent).Probe.Thump;
                else
                    res = (GlobalSettings.MultiplierForConversionToMHz * ((double)ThumpVar)) / (1000 * 2 * freq * cos_pi);

                this.ThumpVar = res;
            }
        }

        public Panel HeaderPanel = null;

        #endregion Public Properties

        private void fireUpdateHeaderEvent()
        {
            if (UpdateHeaderEvent != null)
                UpdateHeaderEvent(this, HeaderPanel);
        }

        public event UpdateHeaderHandler UpdateHeaderEvent;
        public delegate void UpdateHeaderHandler(object sender, Panel headerPanel);

        /// <summary>
        /// Consructor
        /// </summary>
        public DopplerBar()
        {
            this.InitializeComponent();
            this.ReloadControlLabels();
        }

        private void ReloadControlLabels()
        {
            var strRes = ResourceManagerWrapper.GetWrapper(MainForm.ResourceManager); 
            this.dopplerVarLabelDepth.VarText = strRes.GetString("DepthDopplerBar");
            this.dopplerVarLabelGain.VarText = strRes.GetString("Gain");
            this.dopplerVarLabelRange.VarText = strRes.GetString("Scale");
            this.dopplerVarLabelWidth.VarText = strRes.GetString("Sample");
            this.dopplerVarLabelThump.VarText = strRes.GetString("Filter");
            this.dopplerVarLabelPower.VarText = strRes.GetString("Power");
        }

        private void UpdateCWLabel(DopplerVarLabel label, ref int xLocation, ref int yLocation)
        {
            label.SizeMode = this.m_sizeMode;
            if ((Parent as GateView).Probe.IsCW)
            {
                if (this.m_sizeMode == GlobalTypes.TSizeMode.Small)
                {
                    label.Location = new Point(xLocation, yLocation);
                    xLocation += this.dopplerVarLabelDepth.Width;
                    label.Enabled = false;
                }
                else
                {
                    label.Location = new Point(this.Width, yLocation);
                }
            }
            else
            {
                label.Location = new Point(xLocation, yLocation);
                xLocation += label.Width;
                label.Enabled = true;
            }
        }

        public void RecalcLayout()
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((Action)RecalcLayout);
                return;
            }

            if (this.Parent == null)
                return;

            this.pictureBoxHeartRate.Visible = (m_sizeMode != GlobalTypes.TSizeMode.Small);
            this.autoLabelHeartRate.Visible = (m_sizeMode != GlobalTypes.TSizeMode.Small);
            try
            {
                switch (m_sizeMode)
                {
                    case GlobalTypes.TSizeMode.Large:
                        //this.Size = new System.Drawing.Size(752, 58);
                        this.pictureBoxVolume.SizeMode = PictureBoxSizeMode.CenterImage;
                        this.pictureBoxVolume.Width = 41;
                        this.pictureBoxVolume.Height = 58;
                        this.labelBV.Width = 100;
                        this.labelBV.Font = GlobalSettings.F6;
                        this.probeIndication1.Width = 105;
                        this.probeIndication1.LabelFont = GlobalSettings.F9;
                        break;
                    case GlobalTypes.TSizeMode.Medium:
                        //this.Size = new System.Drawing.Size(752, 58);
                        this.pictureBoxVolume.SizeMode = PictureBoxSizeMode.StretchImage;
                        this.pictureBoxVolume.Width = 28;
                        this.pictureBoxVolume.Height = 28;
                        this.labelBV.Width = 50;
                        this.labelBV.Font = GlobalSettings.F2;
                        this.probeIndication1.Width = 70;
                        this.probeIndication1.LabelFont = GlobalSettings.F12;
                        break;
                    case GlobalTypes.TSizeMode.Small:
                        this.pictureBoxVolume.SizeMode = PictureBoxSizeMode.StretchImage;
                        this.pictureBoxVolume.Width = 30;
                        this.panel1.Width = 3;
                        this.labelBV.Width = 50;
                        this.labelBV.Font = GlobalSettings.F14;
                        this.probeIndication1.Width = 55;
                        this.probeIndication1.LabelFont = GlobalSettings.F18;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
            }

            this.probeIndication1.Height = labelBV.Height;
            this.probeIndication1.Location = new Point(this.Width - this.probeIndication1.Width - 1,
                this.Height - this.probeIndication1.Height - 2);


            this.labelBV.Height = (int)(this.labelBV.Font.Size) + 2;
            this.labelBV.Location = new Point(this.Width - this.probeIndication1.Width - this.labelBV.Width - 2,
                this.Height - this.labelBV.Height - 2);


            if ((this.Parent as GateView).Probe != null)
            {
                this.probeIndication1.ProbeColor = (this.Parent as GateView).Probe.Color;
                this.probeIndication1.ProbeName = (this.Parent as GateView).Probe.Name;
            }

            // resize the labels and rearrange them in the bar
            int xLocation = this.panel1.Location.X + this.panel1.Width;
            int yLocation = this.dopplerVarLabelDepth.Location.Y;

            this.UpdateCWLabel(this.dopplerVarLabelDepth, ref xLocation, ref yLocation);

            this.dopplerVarLabelGain.SizeMode = this.m_sizeMode;
            this.dopplerVarLabelGain.Location = new Point(xLocation, yLocation);
            xLocation += this.dopplerVarLabelGain.Width;

            this.dopplerVarLabelRange.SizeMode = this.m_sizeMode;
            this.dopplerVarLabelRange.Location = new Point(xLocation, yLocation);
            xLocation += dopplerVarLabelRange.Width;

            this.UpdateCWLabel(this.dopplerVarLabelPower, ref xLocation, ref yLocation);

            this.UpdateCWLabel(this.dopplerVarLabelWidth, ref xLocation, ref yLocation);

            this.dopplerVarLabelThump.SizeMode = this.m_sizeMode;
            this.dopplerVarLabelThump.Location = new Point(xLocation, yLocation);

            int y = (this.probeIndication1.Location.Y / 3);
            this.pictureBoxHeartRate.Location = new Point(this.pictureBoxHeartRate.Location.X, y);
            this.autoLabelHeartRate.Location = new Point(this.autoLabelHeartRate.Location.X, y);
            if (this.m_sizeMode == GlobalTypes.TSizeMode.Large 
                && !(LayoutManager.SystemLayoutMode == GlobalTypes.TSystemLayoutMode.MonitoringLoad || LayoutManager.SystemLayoutMode == GlobalTypes.TSystemLayoutMode.Monitoring))
            {
                this.picManualCalculations.Visible = true;
                this.picManualCalculations.Location = new Point(labelBV.Left + 15, this.labelBV.Top - this.picManualCalculations.Height - 5);
                this.picManualCalculations.Width = 72;
            }
            else
            {
                this.picManualCalculations.Visible = false;
            }
            this.SetLabelsVisibility();

            this.PerformLayout();
        }

        private void DopplerBar_Load(object sender, System.EventArgs e)
        {
            if (!this.DesignMode)
            {
                this.BackColor = GlobalSettings.P2;
                this.labelBV.ForeColor = GlobalSettings.P7;
            }
        }

        private void SetLabelsVisibility()
        {
            // control vars visibility -- all we can squeeze up to the labelBV
            // are visible
            this.SetDopplerVarVisibility(dopplerVarLabelThump, labelBV.Location.X);
            this.SetDopplerVarVisibility(dopplerVarLabelWidth, labelBV.Location.X);
            this.SetDopplerVarVisibility(dopplerVarLabelPower, labelBV.Location.X);
            this.SetDopplerVarVisibility(dopplerVarLabelRange, labelBV.Location.X);
            this.SetDopplerVarVisibility(dopplerVarLabelGain, labelBV.Location.X);
            this.SetDopplerVarVisibility(dopplerVarLabelDepth, labelBV.Location.X);
        }

        private void DopplerBar_SizeChanged(object sender, System.EventArgs e)
        {
            this.SetLabelsVisibility();

            // If this a small chart update the header event
            if (this.m_sizeMode == GlobalTypes.TSizeMode.Small)
                this.fireUpdateHeaderEvent();
            this.RecalcLayout();
        }

        private void SetDopplerVarVisibility(DopplerVarLabel var, int rightLimit)
        {
            if ((var.Location.X + var.Width) < rightLimit)
                var.Visible = true;
            else
                var.Visible = false;
        }

        public void DeactivateAll()
        {
            this.dopplerVarLabelThump.Active = false;
            this.dopplerVarLabelWidth.Active = false;
            this.dopplerVarLabelPower.Active = false;
            this.dopplerVarLabelRange.Active = false;
            this.dopplerVarLabelGain.Active = false;
            this.dopplerVarLabelDepth.Active = false;

        }

        public void DopplerVarChanged(GlobalTypes.TDopplerVar dopplerVar)
        {
            this.DeactivateAll();
            switch (dopplerVar)
            {
                case GlobalTypes.TDopplerVar.eDepth:
                    this.dopplerVarLabelDepth.Active = true;
                    break;
                case GlobalTypes.TDopplerVar.eGain:
                    this.dopplerVarLabelGain.Active = true;
                    break;
                case GlobalTypes.TDopplerVar.eRange:
                    this.dopplerVarLabelRange.Active = true;
                    break;
                case GlobalTypes.TDopplerVar.ePower:
                    this.dopplerVarLabelPower.Active = true;
                    break;
                case GlobalTypes.TDopplerVar.eWidth:
                    this.dopplerVarLabelWidth.Active = true;
                    break;
                case GlobalTypes.TDopplerVar.eThump:
                    this.dopplerVarLabelThump.Active = true;
                    break;
                case GlobalTypes.TDopplerVar.eToggle:
                default:
                    // do nothing;
                    break;
            }
        }

        public void CopyFrom(DopplerBar src)
        {
            this.dopplerVarLabelDepth.VarValueDouble = src.dopplerVarLabelDepth.VarValueDouble;
            this.dopplerVarLabelGain.VarValue = src.dopplerVarLabelGain.VarValue;
            this.dopplerVarLabelRange.VarValue = src.dopplerVarLabelRange.VarValue;
            this.dopplerVarLabelPower.VarValue = src.dopplerVarLabelPower.VarValue;
            this.dopplerVarLabelWidth.VarValueDouble = src.dopplerVarLabelWidth.VarValueDouble;
            this.dopplerVarLabelThump.VarValue = src.dopplerVarLabelThump.VarValue;
            this.dopplerVarLabelDepth.Active = src.dopplerVarLabelDepth.Active;
            this.dopplerVarLabelGain.Active = src.dopplerVarLabelGain.Active;
            this.dopplerVarLabelRange.Active = src.dopplerVarLabelRange.Active;
            this.dopplerVarLabelPower.Active = src.dopplerVarLabelPower.Active;
            this.dopplerVarLabelWidth.Active = src.dopplerVarLabelWidth.Active;
            this.dopplerVarLabelThump.Active = src.dopplerVarLabelThump.Active;
            this.BVName = src.BVName;
        }

        public delegate void ShowVolumeDelegate(bool show, GlobalTypes.TSizeMode size, bool fakeRowDataModeSign);
        public void ShowVolumeCtrl(bool show, GlobalTypes.TSizeMode size, bool fakeRowDataModeSign)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((ShowVolumeDelegate)ShowVolumeCtrl, show, size, fakeRowDataModeSign);
                return;
            }

            this.m_fakeRowDataModeSign = fakeRowDataModeSign;

            if (size == GlobalTypes.TSizeMode.Small)
            {
                this.pictureBoxVolume.Visible = false;
                this.panel1.Location = new Point(0, 0);
            }
            else
            {
                this.panel1.Location = new Point(pictureBoxVolume.Right, 0);
                this.pictureBoxVolume.Visible = true;
                if (show)
                {
                    if (this.m_fakeRowDataModeSign)
                    {
                        this.pictureBoxVolume.Image = ImageManager.Instance.GetImage(ImageKey.DeletedRowData);
                        this.pictureBoxVolume.SizeMode = PictureBoxSizeMode.CenterImage;
                    }
                    else
                    {
                        for (int i = 0; i < 7; i++)
                            if ((float)this.m_volumeLevel / GlobalSettings.MaxVolume < ((i * 60.0F + 30.0F) / 360.0F))
                            {
                                this.pictureBoxVolume.Image = imageListVolume.Images[i];
                                break;
                            }
                    }
                }
                else
                {
                    this.pictureBoxVolume.Image = null;
                }
            }
        }

        private void pictureBoxVolume_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            ExceptionPublisherLog4Net.ClickLog("pictureBoxVolume", Name);
            if (this.m_fakeRowDataModeSign)
            {
                return;
            }

            if (e.Y < Height / 2)
                MainForm.Instance.VolumeUp();
            else
                MainForm.Instance.VolumeDown();

        }

        private void picManualCalculations_Click(object sender, System.EventArgs e)
        {
            ExceptionPublisherLog4Net.ClickLog("picManualCalculations", Name);
            if (m_fakeRowDataModeSign)
                return;

            if (!LayoutManager.theLayoutManager.CursorsMode)
            {
                MessageBox.Show(MainForm.ResourceManager.GetString("Please insert cursors"), MainForm.ResourceManager.GetString("Manual Calculations"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            if (LayoutManager.SystemMode == GlobalTypes.TSystemModes.Offline && LayoutManager.theLayoutManager.CursorsMode/*&&LayoutManager.theLayoutManager.ReplayMode*/)
            {
                LayoutManager.theLayoutManager.OnManualCalculations();

                var side = LayoutManager.theLayoutManager.SelectedGv.BVSideSuffix;

                foreach (dsGateExamination.tb_GateExaminationRow gateRow in RimedDal.Instance.s_dsGateExamination.tb_GateExamination.Rows)
                {
                    if (gateRow.InSummaryScreen && gateRow.Gate_Index == LayoutManager.theLayoutManager.SelectedGv.Gate_Index)
                    {
                        gateRow.PeakBottomVal = LayoutManager.theLayoutManager.SelectedGv.Spectrum.clinicalBar1.ClinicalParamLabelPeakLower;
                        gateRow.PeakTopVal = LayoutManager.theLayoutManager.SelectedGv.Spectrum.clinicalBar1.ClinicalParamLabelPeakUpper;

                        gateRow.DVBottomVal = LayoutManager.theLayoutManager.SelectedGv.Spectrum.clinicalBar1.ClinicalParamLabelDVLower;
                        gateRow.DVTopVal = LayoutManager.theLayoutManager.SelectedGv.Spectrum.clinicalBar1.ClinicalParamLabelDVUpper;

                        gateRow.MeanBottomVal = LayoutManager.theLayoutManager.SelectedGv.Spectrum.clinicalBar1.ClinicalParamLabelMeanLower;
                        gateRow.MeanTopVal = LayoutManager.theLayoutManager.SelectedGv.Spectrum.clinicalBar1.ClinicalParamLabelMeanUpper;

                        gateRow.RIBottomVal = LayoutManager.theLayoutManager.SelectedGv.Spectrum.clinicalBar1.ClinicalParamLabelRILower;
                        gateRow.RITopVal = LayoutManager.theLayoutManager.SelectedGv.Spectrum.clinicalBar1.ClinicalParamLabelRIUpper;


                        gateRow.PIBottomVal = LayoutManager.theLayoutManager.SelectedGv.Spectrum.clinicalBar1.ClinicalParamLabelPILower;
                        gateRow.PITopVal = LayoutManager.theLayoutManager.SelectedGv.Spectrum.clinicalBar1.ClinicalParamLabelPIUpper;

                        gateRow.SDBottomVal = LayoutManager.theLayoutManager.SelectedGv.Spectrum.clinicalBar1.ClinicalParamLabelSDLower;
                        gateRow.SDTopVal = LayoutManager.theLayoutManager.SelectedGv.Spectrum.clinicalBar1.ClinicalParamLabelSDUpper;

                        //Saving image for the summary screen   //Ofer
                        var fileName = string.Format("{0}{1}{2}.jpg", GlobalSettings.SummaryImgDir, gateRow.Gate_Index, side);  
                        LayoutManager.theLayoutManager.SelectedGv.Spectrum.Graph.SaveBmp(false, fileName);
                        if (!System.IO.File.Exists(fileName))
                        {
                            ExceptionPublisherLog4Net.ExceptionErrorLog(null, string.Format("picManualCalculations_Click(): Saved image ('{0}') not found.", fileName));
                        }
                        else
                        {
                            using (var img = Image.FromFile(fileName))
                            {
                                LayoutManager.theLayoutManager.SelectedGv.Spectrum.Graph.SaveCursors((Bitmap)img);
                                try
                                {
                                    fileName = fileName.Replace(".jpg", "_MC.jpg");
                                    //ExceptionPublisherLog4Net.TraceLog(string.Format("picManualCalculations_Click(): '{0}'   <--- SAVE IMAGE.", fileName), Name);
                                    img.Save(fileName, ImageFormat.Jpeg);
                                }
                                catch (Exception ee)
                                {
                                    ExceptionPublisherLog4Net.ExceptionErrorLog(ee);
                                }
                            }
                        }

                        //Saving the temp bitmap to create new files to the patient report
                        using (var b2 = new Bitmap(230, 128))
                        {
                            LayoutManager.theLayoutManager.SelectedGv.Spectrum.Graph.GenerateBmp(true, b2);
                            try
                            {
                                fileName = GlobalSettings.TmpImgDir + gateRow.Gate_Index + side + ".jpg";
                                //ExceptionPublisherLog4Net.TraceLog(string.Format(".picManualCalculations_Click(): '{0}'  <--- SAVE IMAGE.", fileName), Name);
                                b2.Save(fileName, ImageFormat.Jpeg);
                            }
                            catch (Exception ee)
                            {
                                //Sometimes, Re-clicking the MC button causes error. Ignore it.
                                ExceptionPublisherLog4Net.ExceptionErrorLog(ee, "Error after re-clicking the MC button");
                            }

                            LayoutManager.theLayoutManager.SelectedGv.Spectrum.Graph.SaveCursors(b2);
                            try
                            {
                                fileName = fileName.Replace(".jpg", "_MC.jpg");
                                //ExceptionPublisherLog4Net.TraceLog(string.Format(".picManualCalculations_Click(): '{0}'   <--- SAVE IMAGE.", fileName), Name);
                                b2.Save(fileName, ImageFormat.Jpeg);
                            }
                            catch (Exception ee)
                            {
                                ExceptionPublisherLog4Net.ExceptionErrorLog(ee, "Bitmap saving error");
                            }
                        }

                        SummaryScreen.Instance.UpdateSummaryScreenWithNewValues(gateRow);
                        
                        //Saving the new clinical parameters of the gate to the DB. Natalie (08/10/2010): This check was added for RIMD-282 fix
                        if (LayoutManager.SystemLayoutMode == GlobalTypes.TSystemLayoutMode.DiagnosticLoad)
                            RimedDal.Instance.UpdateGateExamination();

                        SummaryScreen.Instance.SaveImage2HD(gateRow);
                    }
                }
            }
        }

        private void picManualCalculations_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (m_fakeRowDataModeSign)
                return;

            if (LayoutManager.SystemMode == GlobalTypes.TSystemModes.Offline && LayoutManager.theLayoutManager.CursorsMode)
                picManualCalculations.BorderStyle = BorderStyle.None;
        }

        private void picManualCalculations_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (m_fakeRowDataModeSign)
                return;

            if (LayoutManager.SystemMode == GlobalTypes.TSystemModes.Offline && LayoutManager.theLayoutManager.CursorsMode)
                picManualCalculations.BorderStyle = BorderStyle.Fixed3D;
        }

        private void picManualCalculations_MouseLeave(object sender, System.EventArgs e)
        {
            if (m_fakeRowDataModeSign)
                return;

            if (LayoutManager.SystemMode == GlobalTypes.TSystemModes.Offline && LayoutManager.theLayoutManager.CursorsMode)
                picManualCalculations.BorderStyle = BorderStyle.None;
        }
    }
}
