namespace TCD2003.GUI
{
    partial class GateView
    {
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            GateView.GateViewCursorsPosChanged -= new GateView.CursorsPosChangedHandler(this.GateView_CursorsPosChangedEvent);

            base.Dispose(disposing);

            RemoveEvents();
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.contextMenuSpectrum = new System.Windows.Forms.ContextMenu();
            this.AddToSummary = new System.Windows.Forms.MenuItem();
            this.RemoveFromSummary = new System.Windows.Forms.MenuItem();
            this.menuItemSpecInsertCursor = new System.Windows.Forms.MenuItem();
            this.menuItemSpecInsertNotes = new System.Windows.Forms.MenuItem();
            this.menuItemAutoscan = new System.Windows.Forms.MenuItem();
            this.menuItemHitsDetection = new System.Windows.Forms.MenuItem();
            this.menuItemSendTo = new System.Windows.Forms.MenuItem();
            this.SpectrumOnly = new System.Windows.Forms.MenuItem();
            this.FullScreen = new System.Windows.Forms.MenuItem();
            this.menuItemNextBV = new System.Windows.Forms.MenuItem();
            this.menuItemPrint = new System.Windows.Forms.MenuItem();
            this.menuItemClose = new System.Windows.Forms.MenuItem();
            this.menuItemAddEvent = new System.Windows.Forms.MenuItem();
            this.menuItemDeleteEvent = new System.Windows.Forms.MenuItem();
            this.menuItemAddHit = new System.Windows.Forms.MenuItem();
            this.menuItemDeleteHit = new System.Windows.Forms.MenuItem();
            this.labelId = new System.Windows.Forms.Label();
            this.dopplerBar1 = new TCD2003.GUI.DopplerBar();
            this.spectrumChart1 = new TCD2003.GUI.SpectrumChart();
            this.cachedcrTableRep1 = new TCD2003.ReportMgr.CachedcrTableRep();
            this.SuspendLayout();
            // 
            // contextMenuSpectrum
            // 
            this.contextMenuSpectrum.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.AddToSummary,
            this.RemoveFromSummary,
            this.menuItemSpecInsertCursor,
            this.menuItemSpecInsertNotes,
            this.menuItemAutoscan,
            this.menuItemHitsDetection,
            this.menuItemSendTo,
            this.menuItemNextBV,
            this.menuItemPrint,
            this.menuItemClose,
            this.menuItemAddEvent,
            this.menuItemDeleteEvent,
            this.menuItemAddHit,
            this.menuItemDeleteHit});
            this.contextMenuSpectrum.Popup += new System.EventHandler(this.contextMenuSpectrum_Popup);
            // 
            // AddToSummary
            // 
            this.AddToSummary.Index = 0;
            this.AddToSummary.Text = "Update Summary Image";
            this.AddToSummary.Click += new System.EventHandler(this.AddToSummary_Click);
            // 
            // RemoveFromSummary
            // 
            this.RemoveFromSummary.Index = 1;
            this.RemoveFromSummary.Text = "Remove from Summary";
            this.RemoveFromSummary.Click += new System.EventHandler(this.RemoveFromSummary_Click);
            // 
            // menuItemSpecInsertCursor
            // 
            this.menuItemSpecInsertCursor.Index = 2;
            this.menuItemSpecInsertCursor.Text = "Insert Cursors";
            this.menuItemSpecInsertCursor.Click += new System.EventHandler(this.menuItemSpecInsertCursor_Click);
            // 
            // menuItemSpecInsertNotes
            // 
            this.menuItemSpecInsertNotes.Index = 3;
            this.menuItemSpecInsertNotes.Text = "Insert Notes";
            this.menuItemSpecInsertNotes.Click += new System.EventHandler(this.menuItemSpecInsertNotes_Click);
            // 
            // menuItemAutoscan
            // 
            this.menuItemAutoscan.Index = 4;
            this.menuItemAutoscan.Text = "Autoscan";
            this.menuItemAutoscan.Click += new System.EventHandler(this.menuItemAutoscan_Click);
            // 
            // menuItemHitsDetection
            // 
            this.menuItemHitsDetection.Index = 5;
            this.menuItemHitsDetection.Text = "Hits Detection";
            this.menuItemHitsDetection.Click += new System.EventHandler(this.menuItemHitsDetection_Click);
            // 
            // menuItemSendTo
            // 
            this.menuItemSendTo.Index = 6;
            this.menuItemSendTo.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.SpectrumOnly,
            this.FullScreen});
            this.menuItemSendTo.Text = "Export";
            // 
            // SpectrumOnly
            // 
            this.SpectrumOnly.Index = 0;
            this.SpectrumOnly.Text = "Spectrum Only";
            this.SpectrumOnly.Click += new System.EventHandler(this.SpectrumOnly_Click);
            // 
            // FullScreen
            // 
            this.FullScreen.Index = 1;
            this.FullScreen.Text = "Full Screen";
            this.FullScreen.Click += new System.EventHandler(this.FullScreen_Click);
            // 
            // menuItemNextBV
            // 
            this.menuItemNextBV.Index = 7;
            this.menuItemNextBV.Text = "Next BV";
            // 
            // menuItemPrint
            // 
            this.menuItemPrint.Index = 8;
            this.menuItemPrint.Text = "Print";
            this.menuItemPrint.Click += new System.EventHandler(this.menuItemPrint_Click);
            // 
            // menuItemClose
            // 
            this.menuItemClose.Index = 9;
            this.menuItemClose.Text = "Close";
            this.menuItemClose.Click += new System.EventHandler(this.menuItemClose_Click);
            // 
            // menuItemAddEvent
            // 
            this.menuItemAddEvent.Index = 10;
            this.menuItemAddEvent.Text = "Add Event";
            this.menuItemAddEvent.Click += new System.EventHandler(this.menuItemAddEvent_Click);
            // 
            // menuItemDeleteEvent
            // 
            this.menuItemDeleteEvent.Index = 11;
            this.menuItemDeleteEvent.Text = "Delete Event";
            this.menuItemDeleteEvent.Click += new System.EventHandler(this.menuItemDeleteEvent_Click);
            // 
            // menuItemAddHit
            // 
            this.menuItemAddHit.Index = 12;
            this.menuItemAddHit.Text = "Add Hit";
            this.menuItemAddHit.Click += new System.EventHandler(this.menuItemAddHit_Click);
            // 
            // menuItemDeleteHit
            // 
            this.menuItemDeleteHit.Index = 13;
            this.menuItemDeleteHit.Text = "Delete Hit";
            this.menuItemDeleteHit.Click += new System.EventHandler(this.menuItemDeleteHit_Click);
            // 
            // labelId
            // 
            this.labelId.Location = new System.Drawing.Point(0, 0);
            this.labelId.Name = "labelId";
            this.labelId.Size = new System.Drawing.Size(32, 16);
            this.labelId.TabIndex = 2;
            this.labelId.Text = "labelId";
            // 
            // dopplerBar1
            // 
            this.dopplerBar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(235)))), ((int)(((byte)(250)))));
            this.dopplerBar1.BVName = "";
            this.dopplerBar1.DepthVar = 55D;
            this.dopplerBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.dopplerBar1.GainVar = 4;
            this.dopplerBar1.HeartRate = 0;
            this.dopplerBar1.Location = new System.Drawing.Point(0, 0);
            this.dopplerBar1.Name = "dopplerBar1";
            this.dopplerBar1.PowerVar = 100;
            this.dopplerBar1.RangeVar = 6;
            this.dopplerBar1.Size = new System.Drawing.Size(928, 58);
            this.dopplerBar1.TabIndex = 3;
            this.dopplerBar1.ThumpVar = 100D;
            this.dopplerBar1.WidthVar = 15D;
            // 
            // spectrumChart1
            // 
            this.spectrumChart1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(190)))), ((int)(((byte)(230)))));
            this.spectrumChart1.ChartMode = TCD2003.GUI.GlobalTypes.TSizeMode.Large;
            this.spectrumChart1.FlowDirection = TCD2003.GUI.GlobalTypes.TFlowDirection.FromProbe;
            this.spectrumChart1.IndexInProbe = 0;
            this.spectrumChart1.Location = new System.Drawing.Point(8, 72);
            this.spectrumChart1.Name = "spectrumChart1";
            this.spectrumChart1.Size = new System.Drawing.Size(808, 432);
            this.spectrumChart1.SpectrumUnits = TCD2003.GUI.GlobalTypes.TSpectrumUnits.KHz;
            this.spectrumChart1.SpeedFactor = 2;
            this.spectrumChart1.TabIndex = 5;
            // 
            // GateView
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.ContextMenu = this.contextMenuSpectrum;
            this.Controls.Add(this.dopplerBar1);
            this.Controls.Add(this.labelId);
            this.Controls.Add(this.spectrumChart1);
            this.Name = "GateView";
            this.Size = new System.Drawing.Size(928, 800);
            this.SizeChanged += new System.EventHandler(this.spectrumChart1_SizeChanged);
            this.VisibleChanged += new System.EventHandler(this.GateView_VisibleChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GateView_KeyDown);
            this.ResumeLayout(false);

        }
        #endregion

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
        private System.Windows.Forms.MenuItem menuItemAutoscan;
        private System.Windows.Forms.Label labelId;
        private System.Windows.Forms.MenuItem AddToSummary;
        private System.Windows.Forms.MenuItem RemoveFromSummary;
        private System.Windows.Forms.ContextMenu contextMenuSpectrum;
        private System.Windows.Forms.MenuItem menuItemSpecInsertCursor;
        private System.Windows.Forms.MenuItem menuItemSpecInsertNotes;
        private System.Windows.Forms.MenuItem menuItemHitsDetection;
        private System.Windows.Forms.MenuItem menuItemSendTo;
        private System.Windows.Forms.MenuItem menuItemClose;
        private System.Windows.Forms.MenuItem menuItemPrint;
        private System.Windows.Forms.MenuItem menuItemNextBV;
        private System.Windows.Forms.MenuItem menuItemAddEvent;
        private System.Windows.Forms.MenuItem menuItemDeleteEvent;
        private TCD2003.GUI.DopplerBar dopplerBar1;
        private System.Windows.Forms.MenuItem SpectrumOnly;
        private System.Windows.Forms.MenuItem FullScreen;
        private TCD2003.GUI.SpectrumChart spectrumChart1;
        private System.Windows.Forms.MenuItem menuItemAddHit;
        private System.Windows.Forms.MenuItem menuItemDeleteHit;
        private ReportMgr.CachedcrTableRep cachedcrTableRep1;
    }
}