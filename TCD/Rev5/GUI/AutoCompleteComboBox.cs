using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace TCD2003.GUI
{
    public class AutoCompleteComboBox : ComboBox
    {
        public event CancelEventHandler NotInList;

        private bool m_limitToList = true;
        private bool m_inEditMode = false;

        [Category("Behavior")]
        public bool LimitToList
        {
            get
            {
                return this.m_limitToList;
            }
            set
            {
                this.m_limitToList = value;
            }
        }

        protected virtual void OnNotInList(System.ComponentModel.CancelEventArgs e)
        {
            if (this.NotInList != null)
            {
                this.NotInList(this, e);
            }
        }   

        protected override void OnTextChanged(System.EventArgs e)
        {
            if (this.m_inEditMode)
            {
                String input = this.Text;
                int index = this.FindString(input);

                if (index >= 0)
                {
                    this.m_inEditMode = false;
                    this.SelectedIndex = index;
                    this.m_inEditMode = true;
                    this.Select(input.Length, Text.Length);
                }
            }

            base.OnTextChanged(e);
        }

        protected override void OnValidating(System.ComponentModel.CancelEventArgs e)
        {
			if (this.LimitToList)
			{
				int pos = this.FindStringExact(this.Text);

				if (pos == -1)
                    this.OnNotInList(e);
				else
					this.SelectedIndex = pos;
			}

			base.OnValidating(e);
        }

        protected override void OnKeyDown(System.Windows.Forms.KeyEventArgs e)
        {
            this.m_inEditMode = (e.KeyCode != Keys.Back && e.KeyCode != Keys.Delete);
            base.OnKeyDown(e);
        }
    }
}