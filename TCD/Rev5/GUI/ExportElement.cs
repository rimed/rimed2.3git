using System;

namespace TCD2003.GUI
{
	/// <summary>
	/// Summary description for ExportElement.
	/// </summary>
	public class ExportElement
	{
		public DateTime currentTime;

		public double[] parameters;

		private const int defaultCount = 5;
		
		/// <summary>
		/// Count is number of displayed clinical parameters 
		/// </summary>
		public ExportElement(int count)
		{
			parameters = new double[count];
		}
		
		public ExportElement()
		{
			parameters = new double[defaultCount];
		}
	}
}
