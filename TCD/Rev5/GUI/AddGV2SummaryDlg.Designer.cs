﻿namespace TCD2003.GUI
{
    partial class AddGV2SummaryDlg
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMessage = new System.Windows.Forms.Label();
            this.radioButtonReplaceOld = new System.Windows.Forms.RadioButton();
            this.radioButtonDiscardNew = new System.Windows.Forms.RadioButton();
            this.radioButtonAddNew = new System.Windows.Forms.RadioButton();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonHelp = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblMessage
            // 
            this.lblMessage.Location = new System.Drawing.Point(24, 16);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(296, 32);
            this.lblMessage.TabIndex = 0;
            this.lblMessage.Text = "You have already one or more tests for this BV and depth, What do you like to do " +
                "with the new one?";
            // 
            // radioButtonReplaceOld
            // 
            this.radioButtonReplaceOld.AutoSize = true;
            this.radioButtonReplaceOld.Location = new System.Drawing.Point(27, 60);
            this.radioButtonReplaceOld.Name = "radioButtonReplaceOld";
            this.radioButtonReplaceOld.Size = new System.Drawing.Size(82, 17);
            this.radioButtonReplaceOld.TabIndex = 1;
            this.radioButtonReplaceOld.Text = "Replace old";
            this.radioButtonReplaceOld.CheckedChanged += new System.EventHandler(this.radioButtonReplaceOld_CheckedChanged);
            // 
            // radioButtonDiscardNew
            // 
            this.radioButtonDiscardNew.AutoSize = true;
            this.radioButtonDiscardNew.Location = new System.Drawing.Point(123, 60);
            this.radioButtonDiscardNew.Name = "radioButtonDiscardNew";
            this.radioButtonDiscardNew.Size = new System.Drawing.Size(84, 17);
            this.radioButtonDiscardNew.TabIndex = 1;
            this.radioButtonDiscardNew.Text = "Discard new";
            this.radioButtonDiscardNew.CheckedChanged += new System.EventHandler(this.radioButtonDiscardNew_CheckedChanged);
            // 
            // radioButtonAddNew
            // 
            this.radioButtonAddNew.AutoSize = true;
            this.radioButtonAddNew.Location = new System.Drawing.Point(227, 60);
            this.radioButtonAddNew.Name = "radioButtonAddNew";
            this.radioButtonAddNew.Size = new System.Drawing.Size(67, 17);
            this.radioButtonAddNew.TabIndex = 1;
            this.radioButtonAddNew.Text = "Add new";
            this.radioButtonAddNew.CheckedChanged += new System.EventHandler(this.radioButtonAddNew_CheckedChanged);
            // 
            // buttonOK
            // 
            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOK.Location = new System.Drawing.Point(56, 96);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(96, 24);
            this.buttonOK.TabIndex = 2;
            this.buttonOK.Text = "OK";
            // 
            // buttonHelp
            // 
            this.buttonHelp.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonHelp.Location = new System.Drawing.Point(192, 96);
            this.buttonHelp.Name = "buttonHelp";
            this.buttonHelp.Size = new System.Drawing.Size(96, 24);
            this.buttonHelp.TabIndex = 2;
            this.buttonHelp.Text = "Help";
            // 
            // AddGV2SummaryDlg
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(344, 142);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.radioButtonReplaceOld);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.radioButtonDiscardNew);
            this.Controls.Add(this.radioButtonAddNew);
            this.Controls.Add(this.buttonHelp);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddGV2SummaryDlg";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Save New test";
            this.Load += new System.EventHandler(this.AddGV2SummaryDlg_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
 
        #endregion

        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.RadioButton radioButtonReplaceOld;
        private System.Windows.Forms.RadioButton radioButtonDiscardNew;
        private System.Windows.Forms.RadioButton radioButtonAddNew;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonHelp;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
    }
}