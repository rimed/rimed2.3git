namespace TCD2003.GUI
{
    partial class PatientDetails
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblHomePhone = new System.Windows.Forms.Label();
            this.maskedEditBoxTel = new System.Windows.Forms.MaskedTextBox();
            this.dsPatient1 = new TCD2003.DB.dsPatient();
            this.lblAge = new System.Windows.Forms.Label();
            this.textBoxAge = new System.Windows.Forms.NumericUpDown();
            this.lblBirthDate = new System.Windows.Forms.Label();
            this.dateTimePickerBirthDate = new System.Windows.Forms.DateTimePicker();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.gbPatientData = new System.Windows.Forms.GroupBox();
            this.lblStar = new System.Windows.Forms.Label();
            this.comboBoxTitle = new System.Windows.Forms.ComboBox();
            this.lblTitle = new System.Windows.Forms.Label();
            this.radioButtonMale = new System.Windows.Forms.RadioButton();
            this.lblID = new System.Windows.Forms.Label();
            this.textBoxAddress = new System.Windows.Forms.TextBox();
            this.lblAddress = new System.Windows.Forms.Label();
            this.txtAccessionNumber = new System.Windows.Forms.TextBox();
            this.lblAccessionNumber = new System.Windows.Forms.Label();
            this.lblFirstName = new System.Windows.Forms.Label();
            this.textBoxFirstName = new System.Windows.Forms.TextBox();
            this.textBoxFamilyName = new System.Windows.Forms.TextBox();
            this.lblLastName = new System.Windows.Forms.Label();
            this.txtMail = new System.Windows.Forms.TextBox();
            this.lblMail = new System.Windows.Forms.Label();
            this.lblMobile = new System.Windows.Forms.Label();
            this.maskedEditBox2 = new System.Windows.Forms.MaskedTextBox();
            this.textBoxID = new System.Windows.Forms.TextBox();
            this.maskedEditBox3 = new System.Windows.Forms.MaskedTextBox();
            this.lblWorkPhone = new System.Windows.Forms.Label();
            this.radioButtonFemale = new System.Windows.Forms.RadioButton();
            this.comboBoxSex = new System.Windows.Forms.ComboBox();
            this.lblMiddle = new System.Windows.Forms.Label();
            this.textBoxMiddleName = new System.Windows.Forms.TextBox();
            this.gbClinicalDetails = new System.Windows.Forms.GroupBox();
            this.textBoxRefferedBy = new System.Windows.Forms.TextBox();
            this.lblReason = new System.Windows.Forms.Label();
            this.textBoxReasson = new System.Windows.Forms.TextBox();
            this.groupBoxHistory = new System.Windows.Forms.GroupBox();
            this.checkBoxCoronaryByPass = new System.Windows.Forms.CheckBox();
            this.checkBoxProstheticHeartValves = new System.Windows.Forms.CheckBox();
            this.checkBoxAtrialFibrillation = new System.Windows.Forms.CheckBox();
            this.checkBoxCVA = new System.Windows.Forms.CheckBox();
            this.checkBoxTIA = new System.Windows.Forms.CheckBox();
            this.checkBoxAsymptomaticCarotidStenosis = new System.Windows.Forms.CheckBox();
            this.checkBoxSymptomaticCarotidStenosis = new System.Windows.Forms.CheckBox();
            this.checkBoxHypertension = new System.Windows.Forms.CheckBox();
            this.checkBoxSmoking = new System.Windows.Forms.CheckBox();
            this.checkBoxArrhythmia = new System.Windows.Forms.CheckBox();
            this.lblRefferedBy = new System.Windows.Forms.Label();
            this.buttonHelp = new System.Windows.Forms.Button();
            this.VirtualKeyboard = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dsPatient1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxAge)).BeginInit();
            this.gbPatientData.SuspendLayout();
            this.gbClinicalDetails.SuspendLayout();
            this.groupBoxHistory.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblHomePhone
            // 
            this.lblHomePhone.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblHomePhone.Location = new System.Drawing.Point(556, 106);
            this.lblHomePhone.Name = "lblHomePhone";
            this.lblHomePhone.Size = new System.Drawing.Size(69, 16);
            this.lblHomePhone.TabIndex = 10;
            this.lblHomePhone.Text = "Tel Home:";
            this.lblHomePhone.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // maskedEditBoxTel
            // 
            this.maskedEditBoxTel.AccessibleDescription = "MaskedEdit TextBox";
            this.maskedEditBoxTel.AccessibleName = "MaskedEditBox";
            this.maskedEditBoxTel.AccessibleRole = System.Windows.Forms.AccessibleRole.Text;
            this.maskedEditBoxTel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.maskedEditBoxTel.Culture = new System.Globalization.CultureInfo("en-US");
            this.maskedEditBoxTel.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsPatient1, "tb_Patient.Home_Phone", true));
            this.maskedEditBoxTel.Location = new System.Drawing.Point(631, 104);
            this.maskedEditBoxTel.Mask = "(>AAA)AAAAAAAAAAAAAAAAA";
            this.maskedEditBoxTel.Name = "maskedEditBoxTel";
            this.maskedEditBoxTel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.maskedEditBoxTel.Size = new System.Drawing.Size(136, 20);
            this.maskedEditBoxTel.TabIndex = 7;
            // 
            // dsPatient1
            // 
            this.dsPatient1.DataSetName = "dsPatient";
            this.dsPatient1.Locale = new System.Globalization.CultureInfo("he-IL");
            this.dsPatient1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lblAge
            // 
            this.lblAge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAge.Location = new System.Drawing.Point(630, 65);
            this.lblAge.Name = "lblAge";
            this.lblAge.Size = new System.Drawing.Size(66, 16);
            this.lblAge.TabIndex = 11;
            this.lblAge.Text = "Age:";
            this.lblAge.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxAge
            // 
            this.textBoxAge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxAge.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsPatient1, "tb_Patient.Age", true));
            this.textBoxAge.Location = new System.Drawing.Point(702, 63);
            this.textBoxAge.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.textBoxAge.Name = "textBoxAge";
            this.textBoxAge.Size = new System.Drawing.Size(64, 20);
            this.textBoxAge.TabIndex = 5;
            this.textBoxAge.ValueChanged += new System.EventHandler(this.textBoxAge_ValueChanged);
            // 
            // lblBirthDate
            // 
            this.lblBirthDate.Location = new System.Drawing.Point(363, 65);
            this.lblBirthDate.Name = "lblBirthDate";
            this.lblBirthDate.Size = new System.Drawing.Size(96, 16);
            this.lblBirthDate.TabIndex = 12;
            this.lblBirthDate.Text = "Birth Date:";
            this.lblBirthDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dateTimePickerBirthDate
            // 
            this.dateTimePickerBirthDate.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsPatient1, "tb_Patient.Birth_date", true));
            this.dateTimePickerBirthDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerBirthDate.Location = new System.Drawing.Point(465, 62);
            this.dateTimePickerBirthDate.Name = "dateTimePickerBirthDate";
            this.dateTimePickerBirthDate.Size = new System.Drawing.Size(96, 20);
            this.dateTimePickerBirthDate.TabIndex = 4;
            this.dateTimePickerBirthDate.ValueChanged += new System.EventHandler(this.dateTimePickerBirthDate_ValueChanged);
            this.dateTimePickerBirthDate.Validated += new System.EventHandler(this.dateTimePickerBirthDate_Validated);
            // 
            // buttonOK
            // 
            this.buttonOK.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonOK.Location = new System.Drawing.Point(159, 544);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(96, 23);
            this.buttonOK.TabIndex = 2;
            this.buttonOK.Text = "OK";
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(315, 544);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(96, 23);
            this.buttonCancel.TabIndex = 3;
            this.buttonCancel.Text = "Cancel";
            // 
            // gbPatientData
            // 
            this.gbPatientData.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbPatientData.Controls.Add(this.lblStar);
            this.gbPatientData.Controls.Add(this.comboBoxTitle);
            this.gbPatientData.Controls.Add(this.lblTitle);
            this.gbPatientData.Controls.Add(this.radioButtonMale);
            this.gbPatientData.Controls.Add(this.lblID);
            this.gbPatientData.Controls.Add(this.textBoxAddress);
            this.gbPatientData.Controls.Add(this.lblAddress);
            this.gbPatientData.Controls.Add(this.txtAccessionNumber);
            this.gbPatientData.Controls.Add(this.lblAccessionNumber);
            this.gbPatientData.Controls.Add(this.lblFirstName);
            this.gbPatientData.Controls.Add(this.textBoxFirstName);
            this.gbPatientData.Controls.Add(this.textBoxFamilyName);
            this.gbPatientData.Controls.Add(this.lblLastName);
            this.gbPatientData.Controls.Add(this.txtMail);
            this.gbPatientData.Controls.Add(this.lblMail);
            this.gbPatientData.Controls.Add(this.maskedEditBoxTel);
            this.gbPatientData.Controls.Add(this.dateTimePickerBirthDate);
            this.gbPatientData.Controls.Add(this.lblBirthDate);
            this.gbPatientData.Controls.Add(this.lblAge);
            this.gbPatientData.Controls.Add(this.textBoxAge);
            this.gbPatientData.Controls.Add(this.lblHomePhone);
            this.gbPatientData.Controls.Add(this.lblMobile);
            this.gbPatientData.Controls.Add(this.maskedEditBox2);
            this.gbPatientData.Controls.Add(this.textBoxID);
            this.gbPatientData.Controls.Add(this.maskedEditBox3);
            this.gbPatientData.Controls.Add(this.lblWorkPhone);
            this.gbPatientData.Controls.Add(this.radioButtonFemale);
            this.gbPatientData.Controls.Add(this.comboBoxSex);
            this.gbPatientData.Controls.Add(this.lblMiddle);
            this.gbPatientData.Controls.Add(this.textBoxMiddleName);
            this.gbPatientData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gbPatientData.Location = new System.Drawing.Point(16, 0);
            this.gbPatientData.Name = "gbPatientData";
            this.gbPatientData.Size = new System.Drawing.Size(783, 224);
            this.gbPatientData.TabIndex = 0;
            this.gbPatientData.TabStop = false;
            // 
            // lblStar
            // 
            this.lblStar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStar.ForeColor = System.Drawing.Color.Red;
            this.lblStar.Location = new System.Drawing.Point(5, 27);
            this.lblStar.Name = "lblStar";
            this.lblStar.Size = new System.Drawing.Size(10, 16);
            this.lblStar.TabIndex = 28;
            this.lblStar.Text = "*";
            this.lblStar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // comboBoxTitle
            // 
            this.comboBoxTitle.DataBindings.Add(new System.Windows.Forms.Binding("SelectedItem", this.dsPatient1, "tb_Patient.Title", true));
            this.comboBoxTitle.Location = new System.Drawing.Point(72, 62);
            this.comboBoxTitle.Name = "comboBoxTitle";
            this.comboBoxTitle.Size = new System.Drawing.Size(88, 21);
            this.comboBoxTitle.TabIndex = 27;
            // 
            // lblTitle
            // 
            this.lblTitle.Location = new System.Drawing.Point(9, 61);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(63, 23);
            this.lblTitle.TabIndex = 26;
            this.lblTitle.Text = "Title:";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // radioButtonMale
            // 
            this.radioButtonMale.AutoSize = true;
            this.radioButtonMale.Location = new System.Drawing.Point(191, 62);
            this.radioButtonMale.Name = "radioButtonMale";
            this.radioButtonMale.Size = new System.Drawing.Size(48, 17);
            this.radioButtonMale.TabIndex = 24;
            this.radioButtonMale.Tag = "Male";
            this.radioButtonMale.Text = "Male";
            this.radioButtonMale.CheckedChanged += new System.EventHandler(this.radioButtonMale_CheckedChanged);
            // 
            // lblID
            // 
            this.lblID.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.471698F, System.Drawing.FontStyle.Bold);
            this.lblID.Location = new System.Drawing.Point(20, 24);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(28, 16);
            this.lblID.TabIndex = 23;
            this.lblID.Text = "ID:";
            this.lblID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxAddress
            // 
            this.textBoxAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxAddress.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsPatient1, "tb_Patient.Address", true));
            this.textBoxAddress.Location = new System.Drawing.Point(70, 130);
            this.textBoxAddress.MaxLength = 100;
            this.textBoxAddress.Multiline = true;
            this.textBoxAddress.Name = "textBoxAddress";
            this.textBoxAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxAddress.Size = new System.Drawing.Size(480, 46);
            this.textBoxAddress.TabIndex = 5;
            // 
            // lblAddress
            // 
            this.lblAddress.Location = new System.Drawing.Point(2, 130);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(62, 46);
            this.lblAddress.TabIndex = 21;
            this.lblAddress.Text = "Address:";
            this.lblAddress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtAccessionNumber
            // 
            this.txtAccessionNumber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccessionNumber.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsPatient1, "tb_Patient.AccessionNumber", true));
            this.txtAccessionNumber.Location = new System.Drawing.Point(124, 104);
            this.txtAccessionNumber.MaxLength = 64;
            this.txtAccessionNumber.Multiline = true;
            this.txtAccessionNumber.Name = "txtAccessionNumber";
            this.txtAccessionNumber.Size = new System.Drawing.Size(426, 20);
            this.txtAccessionNumber.TabIndex = 22;
            // 
            // lblAccessionNumber
            // 
            this.lblAccessionNumber.Location = new System.Drawing.Point(2, 104);
            this.lblAccessionNumber.Name = "lblAccessionNumber";
            this.lblAccessionNumber.Size = new System.Drawing.Size(116, 20);
            this.lblAccessionNumber.TabIndex = 29;
            this.lblAccessionNumber.Text = "Accession Number:";
            this.lblAccessionNumber.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblFirstName
            // 
            this.lblFirstName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblFirstName.Location = new System.Drawing.Point(570, 21);
            this.lblFirstName.Name = "lblFirstName";
            this.lblFirstName.Size = new System.Drawing.Size(80, 24);
            this.lblFirstName.TabIndex = 20;
            this.lblFirstName.Text = "First Name:";
            this.lblFirstName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxFirstName
            // 
            this.textBoxFirstName.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsPatient1, "tb_Patient.First_Name", true));
            this.textBoxFirstName.Location = new System.Drawing.Point(654, 24);
            this.textBoxFirstName.MaxLength = 15;
            this.textBoxFirstName.Name = "textBoxFirstName";
            this.textBoxFirstName.Size = new System.Drawing.Size(112, 20);
            this.textBoxFirstName.TabIndex = 2;
            // 
            // textBoxFamilyName
            // 
            this.textBoxFamilyName.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsPatient1, "tb_Patient.Last_Name", true));
            this.textBoxFamilyName.Location = new System.Drawing.Point(252, 24);
            this.textBoxFamilyName.MaxLength = 15;
            this.textBoxFamilyName.Name = "textBoxFamilyName";
            this.textBoxFamilyName.Size = new System.Drawing.Size(112, 20);
            this.textBoxFamilyName.TabIndex = 1;
            // 
            // lblLastName
            // 
            this.lblLastName.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblLastName.Location = new System.Drawing.Point(166, 22);
            this.lblLastName.Name = "lblLastName";
            this.lblLastName.Size = new System.Drawing.Size(81, 24);
            this.lblLastName.TabIndex = 18;
            this.lblLastName.Text = "Last Name:";
            this.lblLastName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtMail
            // 
            this.txtMail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMail.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsPatient1, "tb_Patient.Email", true));
            this.txtMail.Location = new System.Drawing.Point(70, 184);
            this.txtMail.MaxLength = 50;
            this.txtMail.Name = "txtMail";
            this.txtMail.Size = new System.Drawing.Size(480, 20);
            this.txtMail.TabIndex = 6;
            // 
            // lblMail
            // 
            this.lblMail.Location = new System.Drawing.Point(6, 186);
            this.lblMail.Name = "lblMail";
            this.lblMail.Size = new System.Drawing.Size(58, 16);
            this.lblMail.TabIndex = 17;
            this.lblMail.Text = "E-Mail:";
            this.lblMail.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblMobile
            // 
            this.lblMobile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMobile.Location = new System.Drawing.Point(556, 185);
            this.lblMobile.Name = "lblMobile";
            this.lblMobile.Size = new System.Drawing.Size(69, 16);
            this.lblMobile.TabIndex = 10;
            this.lblMobile.Text = "Mobile:";
            this.lblMobile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // maskedEditBox2
            // 
            this.maskedEditBox2.AccessibleDescription = "MaskedEdit TextBox";
            this.maskedEditBox2.AccessibleName = "MaskedEditBox";
            this.maskedEditBox2.AccessibleRole = System.Windows.Forms.AccessibleRole.Text;
            this.maskedEditBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.maskedEditBox2.Culture = new System.Globalization.CultureInfo("en-US");
            this.maskedEditBox2.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsPatient1, "tb_Patient.Cellular_Phone", true));
            this.maskedEditBox2.Location = new System.Drawing.Point(631, 184);
            this.maskedEditBox2.Mask = "(>AAA)AAAAAAAAAAAAAAAAA";
            this.maskedEditBox2.Name = "maskedEditBox2";
            this.maskedEditBox2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.maskedEditBox2.Size = new System.Drawing.Size(136, 20);
            this.maskedEditBox2.TabIndex = 9;
            // 
            // textBoxID
            // 
            this.textBoxID.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsPatient1, "tb_Patient.ID", true));
            this.textBoxID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxID.Location = new System.Drawing.Point(48, 24);
            this.textBoxID.MaxLength = 20;
            this.textBoxID.Name = "textBoxID";
            this.textBoxID.Size = new System.Drawing.Size(112, 20);
            this.textBoxID.TabIndex = 0;
            this.textBoxID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxID_KeyPress);
            // 
            // maskedEditBox3
            // 
            this.maskedEditBox3.AccessibleDescription = "MaskedEdit TextBox";
            this.maskedEditBox3.AccessibleName = "MaskedEditBox";
            this.maskedEditBox3.AccessibleRole = System.Windows.Forms.AccessibleRole.Text;
            this.maskedEditBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.maskedEditBox3.Culture = new System.Globalization.CultureInfo("en-US");
            this.maskedEditBox3.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsPatient1, "tb_Patient.Work_Phone", true));
            this.maskedEditBox3.Location = new System.Drawing.Point(631, 144);
            this.maskedEditBox3.Mask = "(>AAA)AAAAAAAAAAAAAAAAA";
            this.maskedEditBox3.Name = "maskedEditBox3";
            this.maskedEditBox3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.maskedEditBox3.Size = new System.Drawing.Size(136, 20);
            this.maskedEditBox3.TabIndex = 7;
            // 
            // lblWorkPhone
            // 
            this.lblWorkPhone.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblWorkPhone.Location = new System.Drawing.Point(556, 145);
            this.lblWorkPhone.Name = "lblWorkPhone";
            this.lblWorkPhone.Size = new System.Drawing.Size(69, 16);
            this.lblWorkPhone.TabIndex = 10;
            this.lblWorkPhone.Text = "Tel Work:";
            this.lblWorkPhone.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // radioButtonFemale
            // 
            this.radioButtonFemale.AutoSize = true;
            this.radioButtonFemale.Location = new System.Drawing.Point(268, 62);
            this.radioButtonFemale.Name = "radioButtonFemale";
            this.radioButtonFemale.Size = new System.Drawing.Size(59, 17);
            this.radioButtonFemale.TabIndex = 24;
            this.radioButtonFemale.Tag = "Female";
            this.radioButtonFemale.Text = "Female";
            this.radioButtonFemale.CheckedChanged += new System.EventHandler(this.radioButtonFemale_CheckedChanged);
            // 
            // comboBoxSex
            // 
            this.comboBoxSex.DataBindings.Add(new System.Windows.Forms.Binding("SelectedItem", this.dsPatient1, "tb_Patient.Sex", true));
            this.comboBoxSex.Location = new System.Drawing.Point(108, 62);
            this.comboBoxSex.Name = "comboBoxSex";
            this.comboBoxSex.Size = new System.Drawing.Size(52, 21);
            this.comboBoxSex.TabIndex = 25;
            // 
            // lblMiddle
            // 
            this.lblMiddle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblMiddle.Location = new System.Drawing.Point(370, 22);
            this.lblMiddle.Name = "lblMiddle";
            this.lblMiddle.Size = new System.Drawing.Size(89, 24);
            this.lblMiddle.TabIndex = 18;
            this.lblMiddle.Text = "Middle Name:";
            this.lblMiddle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxMiddleName
            // 
            this.textBoxMiddleName.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsPatient1, "tb_Patient.Middle_Name", true));
            this.textBoxMiddleName.Location = new System.Drawing.Point(465, 25);
            this.textBoxMiddleName.MaxLength = 15;
            this.textBoxMiddleName.Name = "textBoxMiddleName";
            this.textBoxMiddleName.Size = new System.Drawing.Size(96, 20);
            this.textBoxMiddleName.TabIndex = 1;
            // 
            // gbClinicalDetails
            // 
            this.gbClinicalDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbClinicalDetails.Controls.Add(this.textBoxRefferedBy);
            this.gbClinicalDetails.Controls.Add(this.lblReason);
            this.gbClinicalDetails.Controls.Add(this.textBoxReasson);
            this.gbClinicalDetails.Controls.Add(this.groupBoxHistory);
            this.gbClinicalDetails.Controls.Add(this.lblRefferedBy);
            this.gbClinicalDetails.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gbClinicalDetails.Location = new System.Drawing.Point(16, 232);
            this.gbClinicalDetails.MinimumSize = new System.Drawing.Size(680, 296);
            this.gbClinicalDetails.Name = "gbClinicalDetails";
            this.gbClinicalDetails.Size = new System.Drawing.Size(783, 296);
            this.gbClinicalDetails.TabIndex = 1;
            this.gbClinicalDetails.TabStop = false;
            this.gbClinicalDetails.Text = "Clinical details";
            // 
            // textBoxRefferedBy
            // 
            this.textBoxRefferedBy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxRefferedBy.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsPatient1, "tb_Patient.RefferedBy", true));
            this.textBoxRefferedBy.Location = new System.Drawing.Point(16, 48);
            this.textBoxRefferedBy.MaxLength = 255;
            this.textBoxRefferedBy.Multiline = true;
            this.textBoxRefferedBy.Name = "textBoxRefferedBy";
            this.textBoxRefferedBy.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxRefferedBy.Size = new System.Drawing.Size(460, 56);
            this.textBoxRefferedBy.TabIndex = 19;
            // 
            // lblReason
            // 
            this.lblReason.AutoSize = true;
            this.lblReason.Location = new System.Drawing.Point(16, 112);
            this.lblReason.Name = "lblReason";
            this.lblReason.Size = new System.Drawing.Size(121, 13);
            this.lblReason.TabIndex = 18;
            this.lblReason.Text = "Reason for examination:";
            // 
            // textBoxReasson
            // 
            this.textBoxReasson.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxReasson.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsPatient1, "tb_Patient.Reason_For_Examination", true));
            this.textBoxReasson.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.textBoxReasson.Location = new System.Drawing.Point(16, 136);
            this.textBoxReasson.MaxLength = 255;
            this.textBoxReasson.Multiline = true;
            this.textBoxReasson.Name = "textBoxReasson";
            this.textBoxReasson.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxReasson.Size = new System.Drawing.Size(460, 144);
            this.textBoxReasson.TabIndex = 1;
            // 
            // groupBoxHistory
            // 
            this.groupBoxHistory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxHistory.Controls.Add(this.checkBoxCoronaryByPass);
            this.groupBoxHistory.Controls.Add(this.checkBoxProstheticHeartValves);
            this.groupBoxHistory.Controls.Add(this.checkBoxAtrialFibrillation);
            this.groupBoxHistory.Controls.Add(this.checkBoxCVA);
            this.groupBoxHistory.Controls.Add(this.checkBoxTIA);
            this.groupBoxHistory.Controls.Add(this.checkBoxAsymptomaticCarotidStenosis);
            this.groupBoxHistory.Controls.Add(this.checkBoxSymptomaticCarotidStenosis);
            this.groupBoxHistory.Controls.Add(this.checkBoxHypertension);
            this.groupBoxHistory.Controls.Add(this.checkBoxSmoking);
            this.groupBoxHistory.Controls.Add(this.checkBoxArrhythmia);
            this.groupBoxHistory.Location = new System.Drawing.Point(482, 32);
            this.groupBoxHistory.Name = "groupBoxHistory";
            this.groupBoxHistory.Size = new System.Drawing.Size(293, 248);
            this.groupBoxHistory.TabIndex = 7;
            this.groupBoxHistory.TabStop = false;
            this.groupBoxHistory.Text = "History";
            // 
            // checkBoxCoronaryByPass
            // 
            this.checkBoxCoronaryByPass.Location = new System.Drawing.Point(149, 152);
            this.checkBoxCoronaryByPass.Name = "checkBoxCoronaryByPass";
            this.checkBoxCoronaryByPass.Size = new System.Drawing.Size(135, 32);
            this.checkBoxCoronaryByPass.TabIndex = 18;
            this.checkBoxCoronaryByPass.Text = "Coronary by Pass";
            this.checkBoxCoronaryByPass.CheckedChanged += new System.EventHandler(this.checkBoxCoronaryByPass_CheckedChanged);
            // 
            // checkBoxProstheticHeartValves
            // 
            this.checkBoxProstheticHeartValves.Location = new System.Drawing.Point(149, 112);
            this.checkBoxProstheticHeartValves.Name = "checkBoxProstheticHeartValves";
            this.checkBoxProstheticHeartValves.Size = new System.Drawing.Size(135, 32);
            this.checkBoxProstheticHeartValves.TabIndex = 17;
            this.checkBoxProstheticHeartValves.Text = "Prosthetic Heart Valves";
            this.checkBoxProstheticHeartValves.CheckedChanged += new System.EventHandler(this.checkBoxProstheticHeartValves_CheckedChanged);
            // 
            // checkBoxAtrialFibrillation
            // 
            this.checkBoxAtrialFibrillation.Location = new System.Drawing.Point(149, 72);
            this.checkBoxAtrialFibrillation.Name = "checkBoxAtrialFibrillation";
            this.checkBoxAtrialFibrillation.Size = new System.Drawing.Size(135, 32);
            this.checkBoxAtrialFibrillation.TabIndex = 16;
            this.checkBoxAtrialFibrillation.Text = "Atrial Fibrillation";
            this.checkBoxAtrialFibrillation.CheckedChanged += new System.EventHandler(this.checkBoxAtrialFibrillation_CheckedChanged);
            // 
            // checkBoxCVA
            // 
            this.checkBoxCVA.Location = new System.Drawing.Point(149, 32);
            this.checkBoxCVA.Name = "checkBoxCVA";
            this.checkBoxCVA.Size = new System.Drawing.Size(135, 32);
            this.checkBoxCVA.TabIndex = 15;
            this.checkBoxCVA.Text = "CVA";
            this.checkBoxCVA.CheckedChanged += new System.EventHandler(this.checkBoxCVA_CheckedChanged);
            // 
            // checkBoxTIA
            // 
            this.checkBoxTIA.Location = new System.Drawing.Point(8, 200);
            this.checkBoxTIA.Name = "checkBoxTIA";
            this.checkBoxTIA.Size = new System.Drawing.Size(135, 32);
            this.checkBoxTIA.TabIndex = 14;
            this.checkBoxTIA.Text = "TIA";
            this.checkBoxTIA.CheckedChanged += new System.EventHandler(this.checkBoxTIA_CheckedChanged);
            // 
            // checkBoxAsymptomaticCarotidStenosis
            // 
            this.checkBoxAsymptomaticCarotidStenosis.Location = new System.Drawing.Point(8, 152);
            this.checkBoxAsymptomaticCarotidStenosis.Name = "checkBoxAsymptomaticCarotidStenosis";
            this.checkBoxAsymptomaticCarotidStenosis.Size = new System.Drawing.Size(135, 32);
            this.checkBoxAsymptomaticCarotidStenosis.TabIndex = 13;
            this.checkBoxAsymptomaticCarotidStenosis.Text = "Asymptomatic Carotid Stenosis";
            this.checkBoxAsymptomaticCarotidStenosis.CheckedChanged += new System.EventHandler(this.checkBoxAsymptomaticCarotidStenosis_CheckedChanged);
            // 
            // checkBoxSymptomaticCarotidStenosis
            // 
            this.checkBoxSymptomaticCarotidStenosis.Location = new System.Drawing.Point(8, 112);
            this.checkBoxSymptomaticCarotidStenosis.Name = "checkBoxSymptomaticCarotidStenosis";
            this.checkBoxSymptomaticCarotidStenosis.Size = new System.Drawing.Size(135, 32);
            this.checkBoxSymptomaticCarotidStenosis.TabIndex = 12;
            this.checkBoxSymptomaticCarotidStenosis.Text = "Symptomatic Carotid Stenosis";
            this.checkBoxSymptomaticCarotidStenosis.CheckedChanged += new System.EventHandler(this.checkBoxSymptomaticCarotidStenosis_CheckedChanged);
            // 
            // checkBoxHypertension
            // 
            this.checkBoxHypertension.Location = new System.Drawing.Point(8, 72);
            this.checkBoxHypertension.Name = "checkBoxHypertension";
            this.checkBoxHypertension.Size = new System.Drawing.Size(135, 32);
            this.checkBoxHypertension.TabIndex = 11;
            this.checkBoxHypertension.Text = "Hypertension";
            this.checkBoxHypertension.CheckedChanged += new System.EventHandler(this.checkBoxHypertension_CheckedChanged);
            // 
            // checkBoxSmoking
            // 
            this.checkBoxSmoking.Location = new System.Drawing.Point(8, 32);
            this.checkBoxSmoking.Name = "checkBoxSmoking";
            this.checkBoxSmoking.Size = new System.Drawing.Size(135, 32);
            this.checkBoxSmoking.TabIndex = 10;
            this.checkBoxSmoking.Text = "Smoking";
            this.checkBoxSmoking.CheckedChanged += new System.EventHandler(this.checkBoxSmoking_CheckedChanged);
            // 
            // checkBoxArrhythmia
            // 
            this.checkBoxArrhythmia.Location = new System.Drawing.Point(149, 200);
            this.checkBoxArrhythmia.Name = "checkBoxArrhythmia";
            this.checkBoxArrhythmia.Size = new System.Drawing.Size(135, 32);
            this.checkBoxArrhythmia.TabIndex = 9;
            this.checkBoxArrhythmia.Text = "Arrhythmia";
            this.checkBoxArrhythmia.CheckedChanged += new System.EventHandler(this.checkBoxArrithymia_CheckedChanged);
            // 
            // lblRefferedBy
            // 
            this.lblRefferedBy.AutoSize = true;
            this.lblRefferedBy.Location = new System.Drawing.Point(16, 24);
            this.lblRefferedBy.Name = "lblRefferedBy";
            this.lblRefferedBy.Size = new System.Drawing.Size(66, 13);
            this.lblRefferedBy.TabIndex = 5;
            this.lblRefferedBy.Text = "Referred By:";
            // 
            // buttonHelp
            // 
            this.buttonHelp.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonHelp.Location = new System.Drawing.Point(465, 544);
            this.buttonHelp.Name = "buttonHelp";
            this.buttonHelp.Size = new System.Drawing.Size(96, 23);
            this.buttonHelp.TabIndex = 3;
            this.buttonHelp.Text = "Help";
            // 
            // VirtualKeyboard
            // 
            this.VirtualKeyboard.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.VirtualKeyboard.AutoSize = true;
            this.VirtualKeyboard.Location = new System.Drawing.Point(595, 544);
            this.VirtualKeyboard.Name = "VirtualKeyboard";
            this.VirtualKeyboard.Size = new System.Drawing.Size(96, 23);
            this.VirtualKeyboard.TabIndex = 5;
            this.VirtualKeyboard.Text = "Virtual Keyboard";
            this.VirtualKeyboard.Click += new System.EventHandler(this.VirtualKeyboard_Click);
            // 
            // PatientDetails
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(809, 581);
            this.Controls.Add(this.VirtualKeyboard);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.gbPatientData);
            this.Controls.Add(this.gbClinicalDetails);
            this.Controls.Add(this.buttonHelp);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PatientDetails";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Closed += new System.EventHandler(this.PatientDetails_Closed);
            this.Load += new System.EventHandler(this.PatientDetails_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dsPatient1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxAge)).EndInit();
            this.gbPatientData.ResumeLayout(false);
            this.gbPatientData.PerformLayout();
            this.gbClinicalDetails.ResumeLayout(false);
            this.gbClinicalDetails.PerformLayout();
            this.groupBoxHistory.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblHomePhone;
        private System.Windows.Forms.Label lblAge;
        private System.Windows.Forms.Label lblBirthDate;
        private System.Windows.Forms.DateTimePicker dateTimePickerBirthDate;
        private System.Windows.Forms.NumericUpDown textBoxAge;
        private System.Windows.Forms.MaskedTextBox maskedEditBoxTel;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.GroupBox gbPatientData;
        private System.Windows.Forms.GroupBox gbClinicalDetails;
        private System.Windows.Forms.Label lblRefferedBy;
        private System.Windows.Forms.GroupBox groupBoxHistory;
        private System.Windows.Forms.CheckBox checkBoxArrhythmia;
        private System.Windows.Forms.TextBox textBoxReasson;
        private System.Windows.Forms.Label lblMobile;
        private System.Windows.Forms.MaskedTextBox maskedEditBox2;
        private System.Windows.Forms.Label lblMail;
        private System.Windows.Forms.TextBox txtMail;
        private System.Windows.Forms.Label lblReason;
        private System.Windows.Forms.Label lblLastName;
        private System.Windows.Forms.TextBox textBoxFamilyName;
        private System.Windows.Forms.TextBox textBoxFirstName;
        private System.Windows.Forms.Label lblFirstName;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.TextBox textBoxAddress;
        private System.Windows.Forms.TextBox txtAccessionNumber;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.Label lblAccessionNumber;
        private System.Windows.Forms.TextBox textBoxID;
        private System.Windows.Forms.MaskedTextBox maskedEditBox3;
        private System.Windows.Forms.Label lblWorkPhone;
        private System.ComponentModel.Container components = null;
        private System.Windows.Forms.Button buttonHelp;
        private System.Windows.Forms.RadioButton radioButtonMale;
        private System.Windows.Forms.RadioButton radioButtonFemale;
        private System.Windows.Forms.ComboBox comboBoxSex;
        private System.Windows.Forms.Label lblMiddle;
        private System.Windows.Forms.TextBox textBoxMiddleName;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.ComboBox comboBoxTitle;
        private TCD2003.DB.dsPatient dsPatient1;
        private System.Windows.Forms.CheckBox checkBoxSmoking;
        private System.Windows.Forms.CheckBox checkBoxHypertension;
        private System.Windows.Forms.CheckBox checkBoxSymptomaticCarotidStenosis;
        private System.Windows.Forms.CheckBox checkBoxAsymptomaticCarotidStenosis;
        private System.Windows.Forms.CheckBox checkBoxTIA;
        private System.Windows.Forms.CheckBox checkBoxCVA;
        private System.Windows.Forms.CheckBox checkBoxAtrialFibrillation;
        private System.Windows.Forms.CheckBox checkBoxProstheticHeartValves;
        private System.Windows.Forms.CheckBox checkBoxCoronaryByPass;
        private System.Windows.Forms.TextBox textBoxRefferedBy;
        private System.Windows.Forms.Label lblStar;
        private System.Windows.Forms.Button VirtualKeyboard;
    }
}