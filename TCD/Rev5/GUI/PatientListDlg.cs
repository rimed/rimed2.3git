using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

using TCD2003.DB;
using TCD2003.Utils;

namespace TCD2003.GUI
{
    /// <summary>
    /// Summary description for PatientListDlg.
    /// </summary>
    public partial class PatientListDlg : Form
    {
        #region Private Fields

        private List<string> m_patientsNames = new List<string>();
        private List<string> m_ids = new List<string>();
        private List<Guid> m_patientsIndexes = new List<Guid>();
        private Guid m_selPatientIndex = Guid.Empty;

        #endregion Private Fields

        public PatientListDlg()
        {
            //
            // Required for Windows Form Designer support
            //
            this.InitializeComponent();
            var strRes = ResourceManagerWrapper.GetWrapper(MainForm.ResourceManager); 
            this.buttonOK.Text = strRes.GetString("OK");
            this.buttonCancel.Text = strRes.GetString("Cancel");
            this.lblName.Text = strRes.GetString("Name") + ":";
            this.lblID.Text = strRes.GetString("ID Number") + ":";
            this.buttonDetails.Text = strRes.GetString("Details");
            this.buttonCreateNewPatient.Text = strRes.GetString("Create New Patient");
            this.buttonHelp.Text = strRes.GetString("Help");
            this.Text = strRes.GetString("Select Patient Dialog");

            this.BackColor = GlobalSettings.BackgroundDlg;
        }

        public Guid SelPatientIndex
        {
            get
            {
                return this.m_selPatientIndex;
            }
        }

        private void PatientListDlg_Load(object sender, EventArgs e)
        {
            this.autoCompleteComboBoxName.DataSource = null;
            this.autoCompleteComboBoxID.DataSource = null;

            DataView dv = new System.Data.DataView(DB.RimedDal.Instance.s_dsPatient.tb_Patient, "Deleted = false", null, DataViewRowState.CurrentRows);
            this.m_patientsNames.Clear();
            this.m_ids.Clear();
            this.m_patientsIndexes.Clear();
            List<DB.Patient> patients = new List<DB.Patient>();
            foreach (DataRowView row in dv)
            {
                DB.Patient p = new DB.Patient((Guid)row["Patient_Index"], row["Last_Name"] + " " + row["First_Name"], (String)row["ID"]);
                patients.Add(p);
            }
            patients.Sort();
            foreach (DB.Patient p in patients)
            {
                this.m_patientsNames.Add(p.Name);
                this.m_ids.Add(p.ID);
                this.m_patientsIndexes.Add(p.Guid);
            }

            this.autoCompleteComboBoxName.DataSource = this.m_patientsNames;
            this.autoCompleteComboBoxID.DataSource = this.m_ids;
        }

        private void buttonOK_Click(object sender, System.EventArgs e)
        {
            List<LogField> fields = new List<LogField>();
            fields.Add(new LogField("autoCompleteComboBoxName", autoCompleteComboBoxName.Text));
            fields.Add(new LogField("autoCompleteComboBoxID", autoCompleteComboBoxID.Text));
            ExceptionPublisherLog4Net.FormActionLog("buttonOK", this.Name, fields);

            int index = autoCompleteComboBoxID.SelectedIndex;
            if (index != -1)
                m_selPatientIndex = (Guid)m_patientsIndexes[index];
        }

        private void autoCompleteComboBoxName_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            try
            {
                int index = this.autoCompleteComboBoxName.SelectedIndex;
                if (index < this.autoCompleteComboBoxID.Items.Count)
                    this.autoCompleteComboBoxID.SelectedIndex = index;
            }
            catch (Exception ee)
            {
                ExceptionPublisherLog4Net.ExceptionErrorLog(ee, "autoCompleteComboBoxName_SelectedIndexChanged : ");
                MessageBox.Show("autoCompleteComboBoxName_SelectedIndexChanged : " + ee.Message);
            }
        }

        private void autoCompleteComboBoxID_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            int index = this.autoCompleteComboBoxID.SelectedIndex;
            this.autoCompleteComboBoxName.SelectedIndex = index;
        }

        private void buttonCreateNewPatient_Click(object sender, System.EventArgs e)
        {
            ExceptionPublisherLog4Net.FormActionLog("buttonCreateNewPatient", this.Name, null);
            PatientDetails dlg = new PatientDetails();
            if (dlg.ShowDialog(this) == DialogResult.OK)
            {
                this.m_selPatientIndex = dlg.PatientIndex;
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
        }

        private void buttonDetails_Click(object sender, System.EventArgs e)
        {
            List<LogField> fields = new List<LogField>();
            fields.Add(new LogField("autoCompleteComboBoxName", autoCompleteComboBoxName.Text));
            fields.Add(new LogField("autoCompleteComboBoxID", autoCompleteComboBoxID.Text));
            ExceptionPublisherLog4Net.FormActionLog("buttonDetails", this.Name, fields);

            String filter = "Select * FROM tb_Patient WHERE (Patient_Index = '" + m_patientsIndexes[autoCompleteComboBoxID.SelectedIndex].ToString() + "')";
            PatientDetails dlg = new PatientDetails(RimedDal.Instance.GetPatients(filter));
            dlg.DisplayPurpose = PatientDetails.TDisplayPurpose.eViewChange;
            dlg.ShowDialog();
        }

        private void autoCompleteComboBoxID_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar) && !Char.IsLetter(e.KeyChar))
                e.Handled = true;
        }
    }
}
