using System;

namespace TCD2003.GUI
{
    /// <summary>
    /// Summary description for HitsParameters.
    /// </summary>
    public class HitsParameters
    {
        private int m_step;
        private int m_rate;
        private int m_threshold;

        /// <summary>
        /// Constructor
        /// </summary>
        public HitsParameters(int step, int rate, int threshold)
        {
            this.m_step = step;
            this.m_rate = rate;
            this.m_threshold = threshold;
        }

        public int Step
        {
            get
            {
                return m_step;
            }
        }

        public int Rate
        {
            get
            {
                return m_rate;
            }
        }

        public int Threshold
        {
            get
            {
                return m_threshold;
            }
            set
            {
                m_threshold = value;
            }
        }
    }
}