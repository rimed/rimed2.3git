﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TCD2003.Utils;

namespace TCD2003.GUI
{
    public partial class AddSecondSpectrumDlg : Form
    {
        public AddSecondSpectrumDlg()
        {
            InitializeComponent();

            var strRes = ResourceManagerWrapper.GetWrapper(MainForm.ResourceManager); 
            lblCaption.Text = strRes.GetString("You need to close the M-Mode window or the Trend window in order to add Spectrum window");
            btnMModeClose.Text = strRes.GetString("Close M-Mode");
            btnTrendClose.Text = strRes.GetString("Close Trend");
            btnCancel.Text = strRes.GetString("Cancel");
        }
    }
}
