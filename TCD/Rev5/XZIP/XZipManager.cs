﻿using System;
using System.IO;

namespace TCD2003.XZip
{
    public class XZipManager
    {
        static private XZipManager m_XZipManager = new XZipManager();

        /// <summary>
        /// Instance of class
        /// </summary>
        static public XZipManager theXZip
        {
            get
            {
                return m_XZipManager;
            }
        }

        /// <summary>
        /// Create archive
        /// </summary>
        /// <param name="fileName">File name to archive</param>
        /// <param name="archiveFileName"></param>
        /// <param name="innerFileName"></param>
        /// <returns></returns>
        public bool ZipFile(string fileName, string archiveFileName, string innerFileName)
        {
            using (var zipFile = new Ionic.Zip.ZipFile(archiveFileName))
            {
                var ff = File.OpenRead(fileName);
                zipFile.AddEntry(innerFileName, ff);
                zipFile.Save();
                ff.Close();
            }
            return true;
        }

        /// <summary>
        /// Unpack archive
        /// </summary>
        /// <param name="archiveFileName">Archive file name</param>
        /// <param name="fileName">File name to unpack</param>
        /// <returns></returns>
        public bool UnzipFile(string archiveFileName, string fileName)
        {
            if (!File.Exists(archiveFileName) ||
                !Ionic.Zip.ZipFile.IsZipFile(archiveFileName))
                return false;

            try
            {
                using (var zipFile = Ionic.Zip.ZipFile.Read(archiveFileName))
                {
                    if (zipFile.Count == 0)
                        return false;
                    var ff = File.Create(fileName);
                    foreach (var entry in zipFile)
                    {
                        entry.Extract(ff);
                    }
                    ff.Close();
                }
            }
            catch
            {
                return false;
            }

            return true;
        }
    }
}
