﻿using System.Reflection;
using System.Resources;

namespace TCD2003.Utils
{
    public class ResourceManagerExt : ResourceManager
    {
        public ResourceManagerExt(string baseName, Assembly assembly): base(baseName, assembly)
        {
        }

        public override string GetString(string name)
        {
            return getString(name, true);
        }

        private string getString(string key, bool isReturnKeyOnNull)
        {
            if (key == null)
                return string.Empty;

            var val = base.GetString(key);

            if (val == null && isReturnKeyOnNull)
                return key;

            return val;
        }
    }
}
