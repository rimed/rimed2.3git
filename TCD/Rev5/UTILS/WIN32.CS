using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Text;

namespace TCD2003.Utils
{
	/// <summary>
	/// This class wraps accessors to the Win32 API used elsewhere in the application.
	/// </summary>
	public sealed class Win32
	{
		Win32()
		{

		}

		public const uint GENERIC_READ = 0x80000000;
		public const uint GENERIC_WRITE = 0x40000000;
		public const uint OPEN_EXISTING = 0x3;
		public const uint FILE_FLAG_OVERLAPPED = 0x40000000;
		public const uint ERROR_IO_PENDING = 997;

		public const uint MB_ICONHAND		= 0x10;
		public const uint MB_ICONQUESTION	= 0x20;
		public const uint MB_ICONEXCLAMATION= 0x30;
		public const uint MB_ICONASTERISK	= 0x40;

		public const Int32 INVALID_HANDLE_VALUE	= -1;
		public const UInt32 ERROR_ACCESS_DENIED	= 5;

		public const int WM_SETFOCUS        = 0x0007;
		public const int WM_LBUTTONDOWN		= 0x0201;
		public const int WM_LBUTTONUP		= 0x0202;
		public const int MK_LBUTTON		= 0x0001;
		public const int WM_RBUTTONDOWN	= 0x0204;
		public const int WM_RBUTTONUP		= 0x0205;

		public const int SW_HIDE = 0x0000;
		public const int SW_SHOWNORMAL = 0x0001;
		public const int SW_RESTORE = 0x0009;
		public const int SW_SHOWMAXIMIZED = 0x0003;
		public readonly static IntPtr HWND_BOTTOM = (IntPtr)1;
		public const uint SWP_NOMOVE = 0x0002;

		public const int READ_CONTROL = 0x00020000;
		public const int ERROR_FILE_NOT_FOUND = 0x2;
		
		[DllImport("kernel32.dll", EntryPoint="CreateFileW",  SetLastError=true,
			 CharSet=CharSet.Unicode, ExactSpelling=true)]
		public static extern int CreateFile(
			string filename, uint access, uint sharemode, 
			uint security_attributes, uint creation, 
			uint flags, uint template);

		[DllImport("kernel32.dll", SetLastError=true)]
		public static extern bool CloseHandle(int handle);

		[DllImport("kernel32.dll", SetLastError=true)]
		public static unsafe extern bool ReadFile(int hFile, byte* lpBuffer, 
			int nNumberOfBytesToRead, int* lpNumberOfBytesRead, 
			NativeOverlapped* lpOverlapped);

		[DllImport("kernel32.dll", SetLastError=true)]
		public static unsafe extern bool WriteFile(int hFile, byte* lpBuffer, 
			int nNumberOfBytesToWrite, int* lpNumberOfBytesWritten, 
			NativeOverlapped* lpOverlapped);

		[DllImport("kernel32.dll", SetLastError=true)]
		public static unsafe extern bool DeviceIoControl(int hDevice, 
			int dwIoControlCode, byte* lpInBuffer, int nInBufferSize,
			byte* lpOutBuffer, int nOutBufferSize,
			int* lpBytesReturned, NativeOverlapped* lpOverlapped
			);

		[DllImport("kernel32.dll", SetLastError=true)]
		public static unsafe extern bool GetOverlappedResult(
			int hDevice,                       // handle to file, pipe, or device
			NativeOverlapped* lpOverlapped,          // overlapped structure
			int* lpNumberOfBytesTransferred, // bytes transferred
			bool bWait                          // wait option
			);

		[DllImport("kernel32.dll", SetLastError=true)]
		public static unsafe extern bool CancelIo(
			int hDevice                       // handle to file, pipe, or device
			);

		[DllImport("kernel32.dll", EntryPoint="WritePrivateProfileStringW",CharSet=CharSet.Unicode,SetLastError=true)]
		public static extern
			bool WritePrivateProfileString(
			string lpAppName,  // section name
			string lpKeyName,  // key name
			string lpString,   // string to add
			string lpFileName  // initialization file
			);

		[DllImport("kernel32.dll", EntryPoint="GetPrivateProfileStringW",CharSet=CharSet.Unicode,SetLastError=true)]
		public static extern
			UInt32 GetPrivateProfileString(
			string lpAppName,        // section name
			string lpKeyName,        // key name
			string lpDefault,        // default string
			StringBuilder lpReturnedString,  // destination buffer
			UInt32 nSize,              // size of destination buffer
			string lpFileName        // initialization file name
			); 

		[DllImport("user32.dll")]
		public static extern bool MessageBeep(uint uType);


		[DllImport("kernel32.dll")]
		public static extern Boolean SetupComm(int hFile, uint dwInQueue, uint dwOutQueue);

		[DllImport("kernel32.dll")]
		public static extern Boolean SetCommState(int hFile, [In] ref DCB lpDCB);

		[DllImport("kernel32.dll")]
		public static extern Boolean SetCommTimeouts(int hFile, [In] ref COMMTIMEOUTS lpCommTimeouts);

		
		[DllImport("user32.dll", SetLastError=true)]
		public static extern bool PostMessage(
			uint hWnd,
			int Msg,
			uint wParam,
			uint lParam);

		[DllImport("user32.dll", SetLastError=true)]
		public static extern bool SendMessage(
			uint hWnd,
			int Msg,
			uint wParam,
			uint lParam);


		[DllImport("user32.dll", SetLastError=true)]
		public static extern uint WindowFromPoint(System.Drawing.Point Point);

		[DllImport("user32.dll", SetLastError=true)]
		public static extern uint FromHandle(uint hWnd ); 

		[DllImport("user32.dll", SetLastError=true)]
		public static extern uint GetParent();

		[DllImport("user32.dll", SetLastError=true)]
		public static extern uint SetCapture(uint hWnd);

		[DllImport("user32.dll", SetLastError=true)]
		public static extern uint ReleaseCapture();

		[DllImport("user32.dll", SetLastError=true)]
		public static extern bool ScreenToClient(uint hWnd, [In] ref System.Drawing.Point Point);


		[DllImport("user32.dll", SetLastError=true)]
		public static extern bool ClientToScreen(uint hWnd, [In] ref System.Drawing.Point Point);

		[DllImport("user32.dll", SetLastError=true)]
		public static extern uint ChildWindowFromPoint(uint hWnd, System.Drawing.Point Point);

		[DllImport("user32.dll")]
		public static extern bool GetWindowRect(IntPtr hwnd, ref Rect rectangle);

		[DllImport("user32.dll")]
		public static extern IntPtr GetWindowDC(IntPtr hwnd);

		[DllImport("user32.dll")]
		public static extern Int32 ReleaseDC(IntPtr hwnd, IntPtr hdc);

		[DllImport("gdi32.dll")]
		public static extern bool BitBlt(IntPtr hdcDst, int xDst, int yDst, int cx, int cy, IntPtr hdcSrc, int xSrc, int ySrc, uint ulRop);

//		[DllImport("user32.dll")]
//		public static extern IntPtr GetActiveWindow();

		[DllImport("user32.dll")]
		public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

		[DllImport("user32.dll")]
		internal static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

		[DllImport("user32.dll")]
		internal static extern void SwitchToThisWindow(IntPtr hWnd, bool fAltTab);

		[DllImport("user32.dll")]
		internal static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);

		[DllImport("kernel32.dll", CharSet=CharSet.Auto, SetLastError=true)]
		internal static extern IntPtr OpenMutex(int desiredAccess, bool inheritHandle, string name);

		[DllImport("kernel32", CharSet=CharSet.Auto, SetLastError=true, ExactSpelling=true)]
		internal static extern int CloseHandle(IntPtr hObject);

		public enum TernaryRasterOperations
		{
			SRCCOPY            = 0x00CC0020, /* dest = source                   */
			SRCPAINT           = 0x00EE0086, /* dest = source OR dest           */
			SRCAND             = 0x008800C6, /* dest = source AND dest          */
			SRCINVERT          = 0x00660046, /* dest = source XOR dest          */
			SRCERASE           = 0x00440328, /* dest = source AND (NOT dest )   */
			NOTSRCCOPY         = 0x00330008, /* dest = (NOT source)             */
			NOTSRCERASE        = 0x001100A6, /* dest = (NOT src) AND (NOT dest) */
			MERGECOPY          = 0x00C000CA, /* dest = (source AND pattern)     */
			MERGEPAINT         = 0x00BB0226, /* dest = (NOT source) OR dest     */
			PATCOPY            = 0x00F00021, /* dest = pattern                  */
			PATPAINT           = 0x00FB0A09, /* dest = DPSnoo                   */
			PATINVERT          = 0x005A0049, /* dest = pattern XOR dest         */
			DSTINVERT          = 0x00550009, /* dest = (NOT dest)               */
			BLACKNESS          = 0x00000042, /* dest = BLACK                    */
			WHITENESS          = 0x00FF0062 /* dest = WHITE                    */

		}
		public struct Rect
		{
			public int left;
			public int top;
			public int right;
			public int bottom;

			public int Width
			{
				get
				{
					return right - left;
				}
			}

			public int Height
			{
				get
				{
					return bottom - top;
				}
			}
		}



		#region Structs
		[StructLayout( LayoutKind.Sequential )] public struct COMMTIMEOUTS 
		{
			// Changed Int32 to UInt32 to allow setting to MAXDWORD
			public UInt32 ReadIntervalTimeout;
			public UInt32 ReadTotalTimeoutMultiplier;
			public UInt32 ReadTotalTimeoutConstant;
			public UInt32 WriteTotalTimeoutMultiplier;
			public UInt32 WriteTotalTimeoutConstant;
		}

		[StructLayout( LayoutKind.Sequential )] public struct DCB 
		{
			public Int32 DCBlength;
			public Int32 BaudRate;
			public Int32 PackedValues;
			public Int16 wReserved;
			public Int16 XonLim;
			public Int16 XoffLim;
			public Byte  ByteSize;
			public Byte  Parity;
			public Byte  StopBits;
			public Byte XonChar;
			public Byte XoffChar;
			public Byte ErrorChar;
			public Byte EofChar;
			public Byte EvtChar;
			public Int16 wReserved1;

//			public void init(bool parity, bool outCTS, bool outDSR, int dtr, bool inDSR, bool txc, bool xOut,
//				bool xIn, int rts)
//			{
//				//This function is not called
//				DCBlength = 28; 
//				PackedValues = 0x8001;
//				if (parity) 
//					PackedValues |= 0x0002;
//				if (outCTS) 
//					PackedValues |= 0x0004;
//				if (outDSR) 
//					PackedValues |= 0x0008;
//				PackedValues |= ((dtr & 0x0003) << 4);
//
//				if (inDSR) 
//					PackedValues |= 0x0040;
//				if (txc) 
//					PackedValues |= 0x0080;
//				if (xOut) 
//					PackedValues |= 0x0100;
//				if (xIn) 
//					PackedValues |= 0x0200;
//				PackedValues |= ((rts & 0x0003) << 12);
//			}

			public void InitControl(int rts,int dtr)
			{
				//	RTS (request-to-send) flow control. This member can be one of the following values.
				// 0 = RTS_CONTROL_DISABLE
				// 1 = RTS_CONTROL_ENABLE
				// 2 = RTS_CONTROL_HANDSHAKE
				// 3 = RTS_CONTROL_TOGGLE
				PackedValues |= (  ((rts & 0x0003) << 12) | ((dtr & 0x0003) << 4) );
			}
		}
		#endregion
	}
}
