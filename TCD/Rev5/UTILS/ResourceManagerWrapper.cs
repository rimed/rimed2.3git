using System.Resources;

namespace TCD2003.Utils
{
    public class ResourceManagerWrapper
    {
        private readonly ResourceManager m_resourceManager;

        private ResourceManagerWrapper(ResourceManager mgr)
        {
            m_resourceManager = mgr;
        }

        public string GetString(string key, bool isReturnKeyOnNull = true)
        {
            if (key == null)
                return string.Empty;

            if (m_resourceManager == null)
                return isReturnKeyOnNull ? key : null;

            var val = m_resourceManager.GetString(key);

            if (val == null && isReturnKeyOnNull)
                return key;

            return val;
        }


        public static ResourceManagerWrapper GetWrapper(ResourceManager mgr)
        {
            return new ResourceManagerWrapper(mgr);
        }
    }
}
