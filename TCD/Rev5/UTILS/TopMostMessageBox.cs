using System;

namespace TCD2003.Utils
{
	/// <summary>
	/// Summary description for TopMostMessageBox.
	/// </summary>
	public sealed class TopMostMessageBox
	{
        public static string Name { get { return "TopMostMessageBox"; } }
		public static void Show(string text, string caption = null)
		{
            using (var msgFrm = new frmMessage())
            {
                msgFrm.Message = text;
                if (!string.IsNullOrWhiteSpace(caption))
                    msgFrm.Text = caption;
                var res = msgFrm.ShowDialog();

                ExceptionPublisherLog4Net.TraceLog(string.Format("Show({0}, {1}): {2}", text, caption, res), Name);
            }

        }
	}
}
