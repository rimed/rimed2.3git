using System;
using System.IO;
using System.Windows.Forms;
using rzdcxLib;

namespace TCD2003.Utils
{
	/// <summary>
	/// Summary description for DicomExporter.
	/// </summary>
	public class DicomExporter
	{
		private DCXELM m_StudyUid = new DCXELM();
		private DCXELM m_SeriesUid = new DCXELM();
		DCXAPP dicomLog = new DCXAPP();
		
		public DicomExporter()
		{
			DCXUID uid = new DCXUID();

			// Study Instance UID
			m_StudyUid.Init((int)DICOM_TAGS_ENUM.studyInstanceUID);
			m_StudyUid.Value = uid.CreateUID(UID_TYPE.UID_TYPE_STUDY);

			// Series Instance UID
			m_SeriesUid.Init((int)DICOM_TAGS_ENUM.seriesInstanceUID);
			m_SeriesUid.Value = uid.CreateUID(UID_TYPE.UID_TYPE_SERIES);

			//DICOM logs
			dicomLog.NewInstanceUIDPolicy = new NEW_INSTANCE_CREATION_POLICY();
		}

		/// <summary>
		/// Convert the bitmap file into a secondary capture file
		/// All files 
		/// </summary>
		/// <param name="bmpFileName">name of the bmp-file</param>
		/// <returns>full name of DICOM file</returns>
		public string convertAndSave(string bmpFileName, int intanceNum, 
			string patientID,
			string patientName,
			string patientSex,
			DateTime patientBirthDate,
			string patientRefferedBy,
			DateTime studyDate,
			string Notes,
			string AccessionNumber)
		{
			DCXOBJ o = new DCXOBJ();
			DCXELM e = new DCXELM();

			//////////////////////////////////////////////
			// Insert the image
			//////////////////////////////////////////////

			//o.SetBMPFrames(bmpFileName);
			o.SetJpegFrames(bmpFileName);

			//////////////////////////////////////////////
			// Insert all other info
			//////////////////////////////////////////////

			// Manufacturer
			e.Init((int)DICOM_TAGS_ENUM.Manufacturer);
			e.Value = "RIMED LTD";
			o.insertElement(e);

			e.Init((int)DICOM_TAGS_ENUM.ManufacturerModelName);
			e.Value = "Rimed Transcranial Doppler System model 1"; // Set here valid model name
			o.insertElement(e);

			// SOP Instance UID - The unique id of the image
			DCXUID uid = new DCXUID();
			e.Init((int)DICOM_TAGS_ENUM.sopInstanceUID);
			e.Value = uid.CreateUID(UID_TYPE.UID_TYPE_INSTANCE);
			o.insertElement(e);

			// Instance Number - can be zero length but we will number them by the order of the report
			e.Init((int)DICOM_TAGS_ENUM.InstanceNumber);
			e.Value = intanceNum;
			o.insertElement(e);

			//////////////////////////////////////////////
			// Patient info

			// Patient name
			e.Init((int)DICOM_TAGS_ENUM.patientName);
			e.Value = patientName;
			o.insertElement(e);

			// Patient ID
			e.Init((int)DICOM_TAGS_ENUM.patientID);
			e.Value = patientID;
			o.insertElement(e);

			// patient sex
			// Can be M - Male/F - Female/O - Other
			e.Init((int)DICOM_TAGS_ENUM.PatientSex);
			if (patientSex == "Male")
				e.Value = "M";
			else
				if (patientSex == "Female")
				e.Value = "F";
			else
				e.Value = "O";
			o.insertElement(e);

			// patient birth date
			e.Init((int)DICOM_TAGS_ENUM.PatientBirthDate);
            e.Value = patientBirthDate.ToString("yyyyMMdd"); // Format is YYYYMMDD
			o.insertElement(e);


			//////////////////////////////////////////////
			// Study info
            
			// Study Instance UID
			o.insertElement(m_StudyUid);

			// Study Date
			e.Init((int)DICOM_TAGS_ENUM.StudyDate);
            e.Value = studyDate.ToString("yyyyMMdd");
			o.insertElement(e);

			// Study Time
			e.Init((int)DICOM_TAGS_ENUM.StudyTime);
			e.Value = studyDate.ToString("HHmm");
			o.insertElement(e);

			// Study Description
			e.Init((int)DICOM_TAGS_ENUM.StudyDescription);
			e.Value = Notes;
			o.insertElement(e);

			// Study ID - can be zero length (""). We put 1. You can number it as you like.
			e.Init((int)DICOM_TAGS_ENUM.StudyID);
			e.Value = "1";
			o.insertElement(e);

			// This number comes from the RIS. You can put it "" if you don't have the value
			// When integrating with Modality Worklist, you will have it
			e.Init((int)DICOM_TAGS_ENUM.AccessionNumber);
			e.Value = AccessionNumber;
			o.insertElement(e);

			// If you know it, put it in.
			e.Init((int)DICOM_TAGS_ENUM.ReferringPhysicianName);
			e.Value = patientRefferedBy;
			o.insertElement(e);


			//////////////////////////////////////////////
			// Series info

			// Series Instance UID
			o.insertElement(m_SeriesUid);

			e.Init((int)DICOM_TAGS_ENUM.Modality);
			e.Value = "US"; // Other (was "OT" in the Roni's example)
			o.insertElement(e);

			//DV = Digitized Video
			//DI = Digital Interface
			//DF = Digitized Film
			//WSD = Workstation
			//SD  = Scanned Document
			//SI  = Scanned Image
			//DRW   = Drawing
			//SYN  = Synthetic Image
			e.Init((int)DICOM_TAGS_ENUM.ConversionType);
			e.Value = "DRW"; 
			o.insertElement(e);

			e.Init((int)DICOM_TAGS_ENUM.PatientOrientation);
			e.Value = ""; // No value
			o.insertElement(e);

			e.Init((int)DICOM_TAGS_ENUM.SeriesNumber);
			e.Value = "1"; // Can be with no value. We put 1 by default
			o.insertElement(e);

			//Series Date is the same as Study Date
			e.Init((int)DICOM_TAGS_ENUM.SeriesDate);
            e.Value = studyDate.ToString("yyyyMMdd");
			o.insertElement(e);

			//Setting the some other necessary dates
			e.Init((int)DICOM_TAGS_ENUM.AcquisitionDate);
            e.Value = studyDate.ToString("yyyyMMdd");
			o.insertElement(e);

			e.Init((int)DICOM_TAGS_ENUM.ContentDate);
            e.Value = studyDate.ToString("yyyyMMdd");
			o.insertElement(e);

			//////////////////////////////////////////////
			// Save the file
			//////////////////////////////////////////////

			string dicomDir = @"C:\DICOM";

			if(!Directory.Exists(dicomDir))
				Directory.CreateDirectory(dicomDir);

			FileInfo fi = new FileInfo(bmpFileName);

			string dcmFileName = Path.Combine(dicomDir, fi.Name + ".dcm");
			o.saveFile(dcmFileName);

            ExceptionPublisherLog4Net.TraceLog(string.Format("convertAndSave(...): File.Delete('{0}')", bmpFileName), "DicomExplorter");

            File.Delete(bmpFileName);
			return dcmFileName;
		}

		/// <summary>
		/// Send list of DICOM files to the server
		/// </summary>
		/// <param name="filesToSend">list of DICOM files to send</param>
		/// <param name="localAETitle">local AE title</param>
		/// <param name="targetAETitle">target AE title</param>
		/// <param name="host">server host</param>
		/// <param name="port">server port</param>
		/// <returns>list of failed files</returns>
		public string Send(string filesToSend, string localAETitle, string targetAETitle, string host, short port)
		{
			//RIMD-375: Turn on the Logs for the Dicom
			string Date = DateTime.Today.Year.ToString() + DateTime.Today.Month.ToString() + DateTime.Today.Day.ToString();
			string Time = DateTime.Now.Hour.ToString() + DateTime.Now.Minute.ToString();
			string logFileName = String.Format(@"C:\Rimed\SRC\TCD2003\GUI\Logs\dicomlogfile_{0}_{1}.txt", Date, Time);
			dicomLog.StartLogging(logFileName);
			
			DCXREQ requester = new DCXREQ();
			string failedList = "";
			string succeededList;
			
			requester.Send(localAETitle, targetAETitle, host, Convert.ToUInt16(port), 
				filesToSend, out succeededList, out failedList);

			dicomLog.StopLogging();

			MessageBox.Show("Send ended.\nSent files: " + succeededList + "\nFailed files: " + failedList, "C-STORE");
			
			return failedList;
		}

		public bool Echo(string localAETitle, string targetAETitle, string host, short port)
		{
			DCXREQ requester = new DCXREQ();
			try
			{
			    requester.Echo(localAETitle, targetAETitle, host, Convert.ToUInt16(port));
			    MessageBox.Show("Echo succeeded", "C-ECHO");
			}
			catch (System.Runtime.InteropServices.COMException ex)
			{
			    ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
			    MessageBox.Show("Echo failed: " + ex.Message, "C-ECHO");
			    return false;
			}
			catch (Exception ex)
			{
                ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
			    throw;
			}

			return true;
		}
	}
}
