using System;
using System.Diagnostics;

namespace TCD2003.Utils
{
	/// <summary>
	/// Summary description for ProcessController.
	/// </summary>
	public static class ProcessController
	{
		public static void CloseMainMenu()
		{
			Process [] mainMenu = Process.GetProcessesByName("StartUp");
            if (mainMenu.Length > 0)
			    mainMenu[0].Kill();
		}

        public static void ShutdownWindows()
        {
            //RIMD-498: Problem with Shutdown from Extracranial study in Windows XP
            
            //Determine an OS version
            OperatingSystem os = Environment.OSVersion;
            if (os.Platform == PlatformID.Win32NT)
            {
                switch (os.Version.Major)
                {
                    case 5:
                        //Windows 2000 / Windows XP / Windows Server 2003
                        var a = new Shell32.ShellClass();
                        a.ShutdownWindows();
                        break;
                    case 6:
                        //Windows Vista / Windows 7
                        Process.Start("shutdown.exe", "-s -t 00");
                        break;
                }
            }
        }
	}
}
