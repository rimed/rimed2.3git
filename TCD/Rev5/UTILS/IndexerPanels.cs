using System;
using System.Windows.Forms;

namespace TCD2003.Utils
{
    /// <summary>
    /// Summary description for IndexerPanels.
    /// </summary>
    public class PanelsArr
    {
        private Panel[] m_panelsArray;
        public PanelsArr(Panel panel0, Panel panel1)
        {
            m_panelsArray = new Panel[2];
            m_panelsArray[0] = panel0;
            m_panelsArray[1] = panel1;
        }

        public Panel this[int i]
        {
            get
            {
                return m_panelsArray[i % m_panelsArray.Length];
            }
        }
    }

    public class ObjectArr
    {
        private object[] m_objectArr;

        public ObjectArr(object ob1, object ob2)
        {
            m_objectArr = new object[2];
            m_objectArr[0] = ob1;
            m_objectArr[1] = ob2;
        }

        public object this[int i]
        {
            get
            {
                return m_objectArr[i % m_objectArr.Length];
            }
        }
    }
}
