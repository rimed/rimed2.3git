﻿namespace TCD2003.Utils
{
    partial class GuiAppenderForm
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxErrorDetails = new System.Windows.Forms.TextBox();
            this.buttonContinue = new System.Windows.Forms.Button();
            this.buttonTerminate = new System.Windows.Forms.Button();
            this.buttonBreak = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 8);
            this.label1.Name = "label1";
            this.label1.TabIndex = 0;
            this.label1.Text = "Error Details:";
            // 
            // textBoxErrorDetails
            // 
            this.textBoxErrorDetails.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                | System.Windows.Forms.AnchorStyles.Left)
                | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxErrorDetails.Location = new System.Drawing.Point(8, 32);
            this.textBoxErrorDetails.Multiline = true;
            this.textBoxErrorDetails.Name = "textBoxErrorDetails";
            this.textBoxErrorDetails.ReadOnly = true;
            this.textBoxErrorDetails.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxErrorDetails.Size = new System.Drawing.Size(552, 320);
            this.textBoxErrorDetails.TabIndex = 1;
            this.textBoxErrorDetails.Text = "";
            // 
            // buttonContinue
            // 
            this.buttonContinue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonContinue.Location = new System.Drawing.Point(163, 376);
            this.buttonContinue.Name = "buttonContinue";
            this.buttonContinue.TabIndex = 2;
            this.buttonContinue.Text = "Continue";
            this.buttonContinue.Click += new System.EventHandler(this.buttonContinue_Click);
            // 
            // buttonTerminate
            // 
            this.buttonTerminate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonTerminate.Location = new System.Drawing.Point(339, 376);
            this.buttonTerminate.Name = "buttonTerminate";
            this.buttonTerminate.TabIndex = 3;
            this.buttonTerminate.Text = "Terminate";
            this.buttonTerminate.Click += new System.EventHandler(this.buttonTerminate_Click);
            // 
            // buttonBreak
            // 
            this.buttonBreak.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonBreak.Location = new System.Drawing.Point(251, 376);
            this.buttonBreak.Name = "buttonBreak";
            this.buttonBreak.TabIndex = 4;
            this.buttonBreak.Text = "Break";
            this.buttonBreak.Click += new System.EventHandler(this.buttonBreak_Click);
            // 
            // GuiAppenderForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(576, 414);
            this.Controls.Add(this.buttonBreak);
            this.Controls.Add(this.buttonTerminate);
            this.Controls.Add(this.buttonContinue);
            this.Controls.Add(this.textBoxErrorDetails);
            this.Controls.Add(this.label1);
            this.Name = "GuiAppenderForm";
            this.Text = "Application Error";
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxErrorDetails;
        private System.Windows.Forms.Button buttonContinue;
        private System.Windows.Forms.Button buttonTerminate;
        private System.Windows.Forms.Button buttonBreak;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
    }
}