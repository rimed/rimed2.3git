using System;

namespace TCD2003.Utils
{
	/// <summary>
	/// Summary description for ExceptionRimedData.
	/// </summary>
	public class ExceptionRimedData : Exception
	{

		public string exceptionText = "Rimed Exception:Adding data"; 
		
		public ExceptionRimedData()
		{
			//
			// TODO: Add constructor logic here
			//
		}
	}

	public class InvalidVolumeException : ApplicationException
	{
		public InvalidVolumeException(Uri VolUri) : base("Volume information could not be retreived for the path '" + VolUri.LocalPath + "'. Verify that the path is valid and ends in a trailing backslash, and try again."){}
	}

	public class InvalidVolumeTypeException : ApplicationException
	{
		public InvalidVolumeTypeException() : base("This action cannot be performed because of the volume is of the wrong type."){}
	}

	public class VolumeAccessException : ApplicationException
	{
		public VolumeAccessException() : base("The volume could not be accessed and may be offline."){}
	}

}
