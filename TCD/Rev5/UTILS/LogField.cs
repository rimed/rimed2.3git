using System;

namespace TCD2003.Utils
{
	/// <summary>
	/// Summary description for LogField.
	/// </summary>
	public class LogField
	{
		public string fieldName;
		public string fieldValue;

		public LogField()
		{
		}

		public LogField(string fldName, string fldValue)
		{
			fieldName = fldName;
			fieldValue = fldValue;
		}
	}
}
