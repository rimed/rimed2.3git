using System;
using System.Runtime.InteropServices;

namespace TCD2003.Utils
{
	/// <summary>
	/// Summary description for WindowsController.
	/// </summary>
    public static class WindowsController
	{
		/// <summary>
		/// Hides window with specified text (caption)
		/// </summary>
		/// <param name="wText">Window's caption</param>
		public static void HideWindow(string wText)
		{
			try
			{
				Win32.ShowWindow(Win32.FindWindow(null, wText), Win32.SW_HIDE);
			}
			catch(Exception e)
			{
				ExceptionPublisherLog4Net.ExceptionErrorLog(e); 
			}
		}

		/// <summary>
		/// Shows window with specified text (caption) and gives focus to it
		/// </summary>
		/// <param name="wText">Window's caption</param>
		public static void ShowWindow(string wText)
		{
			try
			{
				IntPtr hWnd = Win32.FindWindow(null, wText);
				Win32.ShowWindow(hWnd, Win32.SW_SHOWNORMAL);
				Win32.SwitchToThisWindow(hWnd, true);
			}
			catch(Exception e)
			{
				ExceptionPublisherLog4Net.ExceptionErrorLog(e);
			}
		}

		/// <summary>
		/// Restores window with specified text (caption) and gives focus to it
		/// </summary>
		/// <param name="wText">Window's caption</param>
		public static void RestoreWindow(string wText)
		{
			try
			{
				IntPtr hWnd = Win32.FindWindow(null, wText);
				Win32.ShowWindow(hWnd, Win32.SW_SHOWMAXIMIZED);
				Win32.SwitchToThisWindow(hWnd, true);
			}
			catch(Exception e)
			{
				ExceptionPublisherLog4Net.ExceptionErrorLog( e);
			}
		}

		/// <summary>
		/// Moves window with specified text (caption) to the bottom of Z position on screen
		/// </summary>
		/// <param name="wText">Window's caption</param>
		/// <returns>Returns zero if the function fails</returns>
		public static bool SetWindowPosBottom(string wText)
		{
			try
			{
				IntPtr hWnd = Win32.FindWindow(null, wText);
				return Win32.SetWindowPos(hWnd, Win32.HWND_BOTTOM, 0, 0, 0, 0, Win32.SWP_NOMOVE);
			}
			catch(Exception e)
			{
				ExceptionPublisherLog4Net.ExceptionErrorLog(e);
			}
			return false;
		}

		public static bool IsSpyExist
		{
			get
			{
				IntPtr hndl = Win32.OpenMutex(Win32.READ_CONTROL, false, "Spy");
				if (Marshal.GetLastWin32Error() == Win32.ERROR_FILE_NOT_FOUND)
					return false;
				if (hndl == (IntPtr)0)
					return false;
				Win32.CloseHandle(hndl);
				return true;
			}
		}
	}
}
