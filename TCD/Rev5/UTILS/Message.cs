using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace TCD2003.Utils
{
	/// <summary>
	/// Summary description for ErrorMessage.
	/// </summary>
	public partial class frmMessage : System.Windows.Forms.Form
	{
		public string Message
		{
			set
			{
				lbMessage.Text = value;
			}
		}
		
		public frmMessage()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		private void btnOK_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}
	}
}
