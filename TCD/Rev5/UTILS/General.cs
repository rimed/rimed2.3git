﻿using System;
using System.Drawing;
using System.Threading;

namespace TCD2003.Utils
{
    public static class General
    {
        private static int s_threadNameCounter = 0;
        public static bool SetThreadName(Thread trd, string name)
        {
            if (trd == null || trd.Name != null)
                return false;

            if (string.IsNullOrWhiteSpace(name))
                name = "ManagedThread";

            var id = Interlocked.Increment(ref s_threadNameCounter);
            trd.Name = String.Format("{0:X4}.{1}", id, name);

            return true;
        }


        public static string GetImageFileNameSuffix(string name)
        {
            var suffix = "_M.jpg";

            if (name.EndsWith("-L", StringComparison.InvariantCultureIgnoreCase))
                suffix = "_L.jpg";
            else if (name.EndsWith("-R", StringComparison.InvariantCultureIgnoreCase))
                suffix = "_R.jpg";

            return suffix;
        }

    }
}
