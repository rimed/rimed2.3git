using System;
using System.Windows.Forms;
using System.Threading;

namespace TCD2003.Utils
{
    /// <summary>
    /// Summary description for CiclicArray.
    /// </summary>
    public class CiclicArray
    {
        private int[] m_intArray;
        private int m_size;
        private int m_location = 0;

        public CiclicArray(int size)
        {
            this.ReInit(size);
        }

        public void ReInit(int size)
        {
            if (this.m_size != size)
            {
                this.m_size = size;
                this.m_intArray = new int[size];
            }
            this.Reset();
        }

        public void Reset()
        {
            for (int i = 0; i < this.m_size; ++i)
            {
                this.m_intArray[i] = 0;
            }
        }

        public int Sum
        {
            get
            {
                int total = 0;
                for (int i = 0; i < this.m_size; ++i)
                {
                    total += this.m_intArray[i];
                }
                return total;
            }
        }

        public int Average
        {
            get
            {
                return this.m_size > 0 ? this.Sum / this.m_size : 0;
            }
        }

        private int GetLocation()
        {
            this.m_location++;
            if (this.m_location >= m_size)
                this.m_location = 0;

            return this.m_location;
        }

        public void UpdateValue(int i)
        {
            this.m_intArray[this.GetLocation()] = i;
        }
    }
}
