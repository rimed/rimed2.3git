﻿namespace TCD2003.Utils
{
    public class Constants
    {
        public const string DataPath = @"C:\Rimed\SRC\TCD2003\DATA\";
        public const string IpImagesPath = DataPath + @"IpImages\";
        public const string GatesImagePath = DataPath + @"GateImg\";
        public const string RawDataPath = DataPath + @"RawData\";
        public const string SummaryImgPath = DataPath + @"SummaryImg\";
        public const string TmpImgPath = DataPath + @"TmpImg\";
        public const string TrendsPath = Constants.DataPath + @"Trends\";
        public const string ReplayImgPath = Constants.DataPath + @"ReplayImg\";
        public const string GhostScriptPath = @"C:\Rimed\SRC\TCD2003\Ghostscript\";
        public const string AppPath = @"C:\Rimed\SRC\TCD2003\GUI\Bin\Debug\";
        public const string NewLine = "\r\n"; //for non-Unix platforms

        public const string USER_MANUAL_FILE = AppPath + @"\UserManual\UserManual.htm";
    }
}
