using System;
using System.Collections;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using TCD2003.Utils;
using Microsoft.Win32;

namespace PostInstallation
{
    [RunInstaller(true)]
    public class Installer : System.Configuration.Install.Installer
    {
        private const string FMT_TRACE_LINE = "Rimed.DigiLite.PostInstallation: {0}.";

        private readonly System.ComponentModel.Container components = null;

        public Installer()
        {
            Trace.WriteLine(FMT_TRACE_LINE, "Installer.ctor().START");

            InitializeComponent();

            Trace.WriteLine(FMT_TRACE_LINE, "Installer.ctor().END");
        }

        protected override void Dispose(bool disposing)
        {
            Trace.WriteLine(FMT_TRACE_LINE, "Installer.Dispose().START");

            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);

            Trace.WriteLine(FMT_TRACE_LINE, "Installer.Dispose().END");
        }

        #region Component Designer generated code
        /// <summary>Required method for Designer support - do not modify the contents of this method with the code editor.</summary>
        private void InitializeComponent()
        {

        }
        #endregion

        #region Override methods
        public override void Install(System.Collections.IDictionary stateSaver)
        {
            Trace.WriteLine(FMT_TRACE_LINE, "Install(...).START");

            base.Install(stateSaver);

            CreateDir();

            // Data source - DummyData or DSP.
            var res = Context.Parameters["DataSrc"];

            // in case of "1" - do nothing because the app config file is set to DSP data source.
            if (res == "2")
                Change2DummyData();
            else
                Change2RealData();

            // Move register keys to Current User root
            UpdateRegisterForWin7();

            execUpdateRimedDBScript();

            execDICOMRegistration();

            Trace.WriteLine(FMT_TRACE_LINE, "Install(...).END");
        }

        //public override void Commit(IDictionary savedState)
        //{
        //    base.Commit(savedState);
        //}

        //public override void Rollback(IDictionary savedState)
        //{
        //    base.Rollback(savedState);
        //}

        public override void Uninstall(IDictionary savedState)
        {
            Trace.WriteLine(FMT_TRACE_LINE, "Uninstall(...).START");

            var myKey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Rimed", true);
            if (myKey != null)
            {
                if (myKey.GetValue("OnePageReport") != null)
                {
                    myKey.DeleteValue("OnePageReport");
                }
            }

            base.Uninstall(savedState);

            Trace.WriteLine(FMT_TRACE_LINE, "Uninstall(...).END");
        }
        #endregion


        private void Change2RealData()
        {
            Trace.WriteLine(FMT_TRACE_LINE, "Change2RealData().START");

            try
            {
                TextReader tr = new StreamReader(Path.Combine(Constants.AppPath, "GUI.exe.config"));
                var text = tr.ReadToEnd();					// read the entire file.
                text = text.Replace(@"add key=""DspExists"" value=""false", @"add key=""DspExists"" value=""true");
                text = text.Replace(@"add key=""RemoteCom"" value=""COM1", @"add key=""RemoteCom"" value=""COM2");
                text = text.Replace(@"add key=""RemoteCom"" value=""NoCOM", @"add key=""RemoteCom"" value=""COM2"); 
                tr.Close();

                TextWriter tw = new StreamWriter(Path.Combine(Constants.AppPath, "GUI.exe.config"));
                tw.Write(text);
                tw.Close();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(FMT_TRACE_LINE, "Change2RealData().EXCEPTION "+ ex);

                ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
            }

            Trace.WriteLine(FMT_TRACE_LINE, "Change2RealData().END");
        }

        private void Change2DummyData()
        {
            Trace.WriteLine(FMT_TRACE_LINE, "Change2DummyData().START");

            try
            {
                TextReader tr = new StreamReader(Path.Combine(Constants.AppPath, "GUI.exe.config"));
                string text = tr.ReadToEnd();			// reaf the entire file.
                text = text.Replace(@"add key=""DspExists"" value=""true", @"add key=""DspExists"" value=""false");
                text = text.Replace(@"add key=""RemoteCom"" value=""COM2", @"add key=""RemoteCom"" value=""NoCOM");
                text = text.Replace(@"add key=""RemoteCom"" value=""COM1", @"add key=""RemoteCom"" value=""NoCOM");
                tr.Close();

                TextWriter tw = new StreamWriter(Path.Combine(Constants.AppPath, "GUI.exe.config"));
                tw.Write(text);
                tw.Close();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(FMT_TRACE_LINE, "Change2DummyData().EXCEPTION "+ ex);
                ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
            }
            
            Trace.WriteLine(FMT_TRACE_LINE, "Change2DummyData().END");
        }

        private void CreateDir()
        {
            Trace.WriteLine(FMT_TRACE_LINE, "CreateDir().START");

            System.IO.Directory.CreateDirectory(Path.Combine(Constants.DataPath, "GateImg"));
            System.IO.Directory.CreateDirectory(Path.Combine(Constants.DataPath, "RawData"));
            System.IO.Directory.CreateDirectory(Path.Combine(Constants.DataPath, "SummaryImg"));
            System.IO.Directory.CreateDirectory(Path.Combine(Constants.DataPath, "TmpImg"));

            Trace.WriteLine(FMT_TRACE_LINE, "CreateDir().END");
        }

        /// <summary>
        /// This method moves the keys from HKEY_LOCAL_MACHINE to HKEY_CURRENT_USER
        /// Key's list:
        /// ExportInterval - an interval for clinical parameters export to csv-files
        /// ExportSeparator - a symbol-separator for csv-files
        /// IpImagesPerPage - a number of IP images per report's page
        /// OnePageReport - a flag of simple/full patient report
        /// ShowHitsEvent - a flag to show or not HITS events on the trend diagram
        /// </summary>
        private void UpdateRegisterForWin7()
        {
            Trace.WriteLine(FMT_TRACE_LINE, "UpdateRegisterForWin7().START");

            var currentUserKey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Rimed", true);
            if (currentUserKey == null)
                currentUserKey = Registry.CurrentUser.CreateSubKey(@"SOFTWARE\Rimed");

            var localKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Rimed", true);
            if (localKey != null)
            {
                MoveKeyToCurrentUser(localKey, currentUserKey, "ExportInterval");
                MoveKeyToCurrentUser(localKey, currentUserKey, "ExportSeparator");
                MoveKeyToCurrentUser(localKey, currentUserKey, "IpImagesPerPage");
                MoveKeyToCurrentUser(localKey, currentUserKey, "OnePageReport");

                if (localKey.GetValue("ShowHitsEvent") != null)
                    localKey.DeleteValue("ShowHitsEvent");

                if (currentUserKey.GetValue("ShowHitsEvent") != null)
                    currentUserKey.DeleteValue("ShowHitsEvent");
            }

            Trace.WriteLine(FMT_TRACE_LINE, "UpdateRegisterForWin7().END");
        }

        private void MoveKeyToCurrentUser(RegistryKey localMachineKey, RegistryKey currentUserKey, string keyName)
        {
            Trace.WriteLine(FMT_TRACE_LINE, "MoveKeyToCurrentUser(...).START");

            var keyValue = localMachineKey.GetValue(keyName);
            if (keyValue != null)
            {
                if (currentUserKey.GetValue(keyName) == null)
                    currentUserKey.SetValue(keyName, keyValue);
                localMachineKey.DeleteValue(keyName);
            }

            Trace.WriteLine(FMT_TRACE_LINE, "MoveKeyToCurrentUser(...).END");
        }

        private bool execUpdateRimedDBScript()
        {
            Trace.WriteLine(FMT_TRACE_LINE, "execUpdateRimedDBScript().START");

            var res = false;
            const string VBS_FILE = Constants.AppPath + "UpdateRimedDB.vbs";

            Trace.WriteLine(FMT_TRACE_LINE, "execUpdateRimedDBScript().SCRIPT=" + VBS_FILE);

            if (!File.Exists(VBS_FILE))
            {
                MessageBox.Show("DB update file not found. DB update process will not be executed.", "Rimed ltd.");
            }
            else
            {
                try
                {
                    //Cmd line options: //B = Batch mode, suppress errors and prompts
                    using (var p = Process.Start("wscript.exe", VBS_FILE + " //B //Nologo"))
                    {
                        p.WaitForExit(60000);
                    }

                    res = true;
                }
                catch (Exception ex)
                {
                    Trace.WriteLine(FMT_TRACE_LINE, "execUpdateRimedDBScript().EXCEPTION "+ ex);
                    MessageBox.Show("Rimed DB upgrade procedure fail.\n"+ ex.Message, "Rimed ltd.");
                }
            }

            Trace.WriteLine(FMT_TRACE_LINE, "execUpdateRimedDBScript().END");

            return res;
        }

        private bool execDICOMRegistration()
        {
            Trace.WriteLine(FMT_TRACE_LINE, "execDICOMRegistration().START");

            const string CMD = Constants.AppPath + "dicom.bat";

            Trace.WriteLine(FMT_TRACE_LINE, "execDICOMRegistration().CMD=" + CMD);
            var res = false;
            try
            {
                //RIMD-358: COM exception in Dicom report
                using (var p = Process.Start(CMD))
                {
                    p.WaitForExit(60000);
                }

                res = true;
            }
            catch (Exception ex)
            {
                Trace.WriteLine(FMT_TRACE_LINE, "execDICOMRegistration().EXCEPTION "+ ex);
                MessageBox.Show("Rimed DICOM server registratin fail.\n"+ ex.Message, "Rimed ltd.");
            }

            Trace.WriteLine(FMT_TRACE_LINE, "execDICOMRegistration().END");

            return res;
        }        
    }
}
