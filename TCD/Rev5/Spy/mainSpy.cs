using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using Microsoft.ManagedSpy;
using System.Timers;
using System.Threading;

using System.IO;

namespace Spy
{
	public partial class mainSpy : Form
	{
		private string[] names;

		private Process echoProc;

		public mainSpy()
		{
			InitializeComponent();
		}

		/*private ControlProxy FindSpecifiedControl(ControlProxy currentNode, string[] names, int pos, string filename)
		{
			ControlProxy[] proxies = currentNode.Children;
			foreach (ControlProxy proxy in proxies)
			{
				if (string.Equals(proxy.GetComponentName(), names[pos], StringComparison.OrdinalIgnoreCase))
				{
					TraceWriteLine(filename, String.Format("Find component {0};\r\n", proxy.GetComponentName()));
					if (pos + 1 < names.Length)
						return FindSpecifiedControl(proxy, names, pos + 1, filename);
					else
						return proxy;
				}
			}
			return null;
		}*/

		private ControlProxy FindMinimizeButton(ControlProxy currentNode, string[] names, int pos, string filename)
		{
			if (pos + 1 < names.Length)
			{
				ControlProxy[] proxies = currentNode.Children;
				foreach (ControlProxy proxy in proxies)
				{
					if (string.Equals(proxy.GetComponentName(), names[pos], StringComparison.OrdinalIgnoreCase))
					{
						TraceWriteLine(filename, String.Format("Find component {0};\r\n", proxy.GetComponentName()));
						return FindMinimizeButton(proxy, names, pos + 1, filename);
					}
				}
			}
			else
				return FindSpecifiedControl(currentNode, names[pos], filename);
			
			return null;
		}

		private ControlProxy FindSpecifiedControl(ControlProxy currentNode, string name, string filename)
		{
			ControlProxy[] proxies = currentNode.Children;
			foreach (ControlProxy proxy in proxies)
			{
				TraceWriteLine(filename, String.Format("Find component {0};\r\n", proxy.GetComponentName()));
				if (string.Equals(proxy.GetComponentName(), name, StringComparison.OrdinalIgnoreCase))
					return proxy;
				else
				{
					ControlProxy control = FindSpecifiedControl(proxy, name, filename);
					if (control != null)
						return control;
				}
			}
			return null;
		}

		private void mainSpy_Load(object sender, EventArgs e)
		{
			string filename = Path.Combine(Application.StartupPath, "log.txt");
			File.Delete(filename);
			TraceWriteLine(filename, "Start to find Digi-Imager...\r\n");
			ControlList list = new ControlList();
			names = list.GetControlNameList("EchoWave");
			Process[] processes = Process.GetProcessesByName("EchoWave");
			if (processes.Length > 0)
			{
				echoProc = processes[0];
				if (echoProc != null && echoProc.Id != Process.GetCurrentProcess().Id)
				{
					TraceWriteLine(filename, "Digi-Imager was finding...\r\n");

					int counter = 0;
					while (true)
					{
						counter++;
                        if (counter > 50)
                        {
                            TraceWriteLine(filename, "50 DKP minus! Spy was closed\r\n");
                            Close();
                        }

						ControlProxy proxy = new ControlProxy(echoProc.MainWindowHandle);
						TraceWriteLine(filename, String.Format("Current Main Window title is: {0}\r\n", echoProc.MainWindowTitle));
						ControlProxy rez = FindMinimizeButton(proxy, names, 0, filename);
						if (rez != null)
						{
							//PropertyDescriptor property = rez.GetProperties().Find("Visible", true);
							//property.SetValue(rez, false);
							//TraceWriteLine(filename, "Set Minimize 'Visible' property to 'False'\r\n");
							foreach (EventDescriptor ed in rez.GetEvents())
							{
								if (ed.Name.Equals("MouseClick"))
								{
									rez.SubscribeEvent(ed);
								}
							}
							rez.EventFired += new ControlProxyEventHandler(ProxyEventFired);

							break;
						}
						else
						{
							Thread.Sleep(2000);
							TraceWriteLine(filename, counter.ToString() + ". Minimize was not finded. Try again...\r\n");
							processes = Process.GetProcessesByName("EchoWave");
							echoProc = processes[0];
							if (echoProc == null)
							{
								TraceWriteLine(filename, "Digi-Imager process was lost\r\n");
								break;
							}
						}
					}
				}
                else
                    TraceWriteLine(filename, "Digi-Imager hasn't been found\r\n");
			}
			else
			{
                MessageBox.Show("DigiLite™ IP application is not running");
                TraceWriteLine(filename, "Digi-Imager application is not running\r\n");
			}
			//TraceWriteLine(filename, "Spy was closed");
			//Close();
		}

		private void TraceWriteLine(string filename, string line)
		{
			File.AppendAllText(filename, line);
		}

		/// <summary>
		/// This is called when the selected ControlProxy raises an event
		/// </summary>
		private void ProxyEventFired(object sender, ProxyEventArgs args)
		{
			IntPtr hWnd = Win32.FindWindow(null, "Main Menu");
			Win32.ShowWindow(hWnd, Win32.SW_SHOWNORMAL);
			//Win32.SendMessage((uint)hWnd, Win32.WM_SHOWWINDOW, 0, 1);
		}
	}
}