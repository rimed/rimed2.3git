using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;

namespace Spy
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			bool OneInstanceOnly = false;
			using (Mutex mx = new Mutex(true, "Spy", out OneInstanceOnly))
			{
				if (OneInstanceOnly)
				{
					Application.EnableVisualStyles();
					Application.SetCompatibleTextRenderingDefault(false);
					Application.Run(new mainSpy());
				}
			}
		}
	}
}