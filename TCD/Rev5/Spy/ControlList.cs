using System;
using System.Collections.Generic;
using System.Text;

namespace Spy
{
	class ControlList
	{
		public ControlList()
		{
		}
		
		/// <summary>
		/// Return list of control's names in necessary branch
		/// </summary>
		/// <returns></returns>
		public string[] GetControlNameList(string processName)
		{
			List<string> lst = new List<string>();
			switch (processName)
			{
				case "EchoWave":
					{
						lst.Add("top_toolbar_panel");
						lst.Add("minimize_button");
						break;
					}
				default: break;
			}
			return lst.ToArray();
		}
	}
}
