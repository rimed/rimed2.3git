/****************************************************************************************************
* File Name		:	TCD_DSP2PC.h																	*
* Project		:	TCD-2003																		*
* Subsystem		:	DSP module																		*
* Description	:	This header file, describes the Coding Convention of all DSP's TCD-2003 project *
*					files. This file is not included in	the	compilation process it is text only !	*
*																									*
* Structs		:	R_DSP2PC - Sctucts Block to send the PC.
*
*																									*
* Date				Programmer						Revision Notes									*
* July-2003			Rimed							v1.0											*
*																									*
* Notes			:																					*
* Platform		:	C - Compilaed by CCS V2.2														*
****************************************************************************************************/

#include "TCD_PC2DSP.h"

#ifndef TCD_DSP2PC_H__
#define TCD_DSP2PC_H__

#ifndef Uint8
	#define Uint8	unsigned	char
	#define Int8				char
	#define Uint16	unsigned	short
	#define Int16				short
	#define Uint32	unsigned	int
	#define Int32				int
#endif

#ifdef WIN32
//#pragma pack(show)
#pragma pack(push)
#pragma pack(1)
//#pragma pack(show)
#endif

#ifndef GATES_PER_PROBE
	#define GATES_PER_PROBE		8			/* Number of gates per Probe					*/
#endif

#ifndef NUMBER_OF_PROBES
	#define NUMBER_OF_PROBES	2			/* Number of probes								*/
#endif

// *************************
// R_FFT Spectrum block: 1.1
// *************************

/*===================================================================================================
	Struct decliration:
===================================================================================================*/
/*---------------------------------------------------------------------------------------------------
	R_FFT_Column - Spectrum block: 1.1
---------------------------------------------------------------------------------------------------*/
typedef struct R_FFT_Column_s
{
	Uint8 m_Index[256];		// R_FFT Column with 256 pixel of 8 bit each: 1.1.1.x
}R_FFT_Column;
/*---------------------------------------------------------------------------------------------------
	FFT_Spectrum_s - Spectrum block
---------------------------------------------------------------------------------------------------*/
typedef struct R_FFT_Spectrum_Gate_s 	// One gate of R_FFT spectrum: 1.1.x
{
	R_FFT_Column m_Column_sa[3];  // 3 Column for each 8mSec within 24mSec
}R_FFT_Spectrum_Gate;

typedef struct FFT_Spectrum_s	// R_FFT Spectrum Block: 1.1
{
	R_FFT_Spectrum_Gate m_Gate_sa[GATES_PER_PROBE * NUMBER_OF_PROBES];  // 16 gates
}R_FFT_Spectrum;

/*---------------------------------------------------------------------------------------------------
	R_FFT R_Envelope block: 1.2
---------------------------------------------------------------------------------------------------*/
typedef struct Envelope_Indexes_s //  R_Envelope Indexes : 1.2.x.1.1.x
{
	Uint8 m_Forward;
	Uint8 m_Reverse;
}R_Envelope_Indexes;

typedef struct Envelope_Gate_s	//  R_Envelope Colomn of 3x8mSec : 1.2.x.1.x
{
	R_Envelope_Indexes m_Column_sa[3];
}R_Envelope_Gate;

typedef struct Envelope_s	// Mode R_Envelope of 16 gates: 1.2.2.x
{
	R_Envelope_Gate m_Gate_sa[GATES_PER_PROBE * NUMBER_OF_PROBES];
}R_Envelope;

typedef struct FFT_Envelopes_s // R_FFT_Envelopes - Peak & Mode Emvelops 1.2
{
	R_Envelope m_Peak_s;
	R_Envelope m_Mode_s;
}R_FFT_Envelopes;
/*---------------------------------------------------------------------------------------------------
	Hits Table block: 1.3
---------------------------------------------------------------------------------------------------*/
typedef struct HITS_Det_s	// R_HITS property: 1.3.x
{
	Uint8 m_Type;			// 0 - no HITS, 1 HITS found. TBD - gas or solid
	Uint8 m_Energy;     	// in dB units
	Uint16 m_Speed_Index;	// the same like peak units
	Uint16 m_Duration;       // in 8msec columns
}R_HITS_Det;

typedef struct HITS_Gate_s
{
	R_HITS_Det m_Column_sa[3];
	Uint8 RESERVE[14];	// reserve to keep compatability with previous interface
}R_HITS_Gate;

typedef struct HITS_Data_s
{
	R_HITS_Gate m_Gate_sa[GATES_PER_PROBE * NUMBER_OF_PROBES];
}R_HITS_Data;

/*---------------------------------------------------------------------------------------------------
	Clinical parameters: 1.4
---------------------------------------------------------------------------------------------------*/
typedef struct Clinical_Parameters_Value_s
{
	Uint16 m_Forward;	// w=0
	Uint16 m_Reverse;	// w=1
}R_Clinical_Parameters_Value;

typedef struct Clinical_Parameters_Gate_s
{
	R_Clinical_Parameters_Value		m_Peak_s;		//1.4.3.1+W
	R_Clinical_Parameters_Value		m_Mode_s;		//1.4.3.3+W
	R_Clinical_Parameters_Value		m_Mean_s;		//1.4.3.5+W
	R_Clinical_Parameters_Value		m_Average_s;	//1.4.3.7+W
	R_Clinical_Parameters_Value		m_DV_s;			//1.4.3.9+W
	R_Clinical_Parameters_Value		m_SW_s;			//1.4.3.11+W
}R_Clinical_Parameters_Gate;

typedef struct Clinical_Parameters_s 			// 1.4
{
    Uint16 m_Flags;                                  //1.4.1
	Uint16 m_Heart_Rate;							//1.4.2
	R_Clinical_Parameters_Gate m_Gate_sa[GATES_PER_PROBE * NUMBER_OF_PROBES];	 	//1.4.3-18
}R_Clinical_Parameters;

/*---------------------------------------------------------------------------------------------------
	Auto-Mode: 1.5
---------------------------------------------------------------------------------------------------*/
typedef struct Auto_Scan_Column_s
{
	Uint8 m_Pixel_a[64];
}R_Auto_Scan_Column;

typedef struct Auto_Scan_Probe_s
{
	R_Auto_Scan_Column	m_Column_sa[3];	//1.5.x
}R_Auto_Scan_Probe;

typedef struct Auto_Scan_s
{
	R_Auto_Scan_Probe	m_Brobe_sa[2];	//Auto scan for each probe
}R_Auto_Scan;

/*---------------------------------------------------------------------------------------------------
	Time Domain: 1.6
---------------------------------------------------------------------------------------------------*/
typedef struct Time_Domain_Value_s
{
	Int16 m_I;
	Int16 m_Q;
}R_Time_Domain_Value;

typedef struct Time_Domain_Gate_s
{
	R_Time_Domain_Value	m_Value_sa[32*24]; // 32k x 24m
}R_Time_Domain_Gate;

typedef struct Time_Domain_s
{
	R_Time_Domain_Gate	m_Gate_sa[GATES_PER_PROBE * NUMBER_OF_PROBES];	// 1.6.x
}R_Time_Domain;

/*---------------------------------------------------------------------------------------------------
	External channel : 1.7
---------------------------------------------------------------------------------------------------*/
typedef struct Ex_Channel_s
{
	Int16 m_Val_a[3];
}R_Ex_Channel;


typedef struct PC2DSP_Echo_s
{
	R_Card_Info		m_GenCardInfo_s; // size of  = 20
	R_Probe_Info	m_ProbeInfo_sa[ NUMBER_OF_PROBES ]; // size of  = 112
}R_PC2DSP_Echo;



/*---------------------------------------------------------------------------------------------------
	Control, Flags & DEBUG : 1.8
---------------------------------------------------------------------------------------------------*/
typedef struct Control_Flags_and_Debug_s
{
	Int8 TBD[ 2564 - sizeof(R_PC2DSP_Echo) ];
}R_Control_Flags_and_Debug;

 /*---------------------------------------------------------------------------------------------------
/---------------------------------------------------------------------------------------------------
	DSP 2 PC structure
 /---------------------------------------------------------------------------------------------------
/---------------------------------------------------------------------------------------------------*/
typedef struct DSP2PC_s
{
	unsigned int m_Numerator;	// 32bits Counter - for Verifying

	R_FFT_Spectrum 				m_FFT_Spectrum_Block_s;			// 1.1
	R_FFT_Envelopes 			m_FFT_Envelopes_Block_s; 		// 1.2
	R_HITS_Data 				m_HITS_Data_Block_s;			// 1.3
	R_Clinical_Parameters 		m_Clinical_Parameters_Block_s;	// 1.4
	R_Auto_Scan					m_Auto_Scan_Block_s;		// 1.5
	R_Time_Domain				m_Time_Domain_Block_s;		// 1.6
	R_Ex_Channel				m_Ex_Channels_sa[8];		// 1.7

	R_PC2DSP_Echo				m_PC2DSC_Echo_s;				// 1.75
	R_Control_Flags_and_Debug	Control_Flags_and_Debug_Block; // 1.8

	unsigned int m_Dup_Numerator;// The last item in the block to compare with the first one
}R_DSP2PC;

/*#######################################  End Of File  ###########################################*/
#ifdef WIN32
#pragma pack(pop)
//#pragma pack(show)
#endif

#endif

