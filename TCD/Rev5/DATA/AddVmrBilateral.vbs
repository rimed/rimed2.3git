Const adOpenStatic = 3
Const adLockOptimistic = 3
Const adOpenKeyset =1
Const adCmdText=1

' Enable error handling
'On Error Resume Next

Set objConnection = CreateObject("ADODB.Connection")
Set objRecordSet = CreateObject("ADODB.Recordset")


objConnection.Open _
    "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Rimed\SRC\TCD2003\DATA\RimedDB.mdb;" & _
        "User ID=Admin;"
'InsertVMRBilateralstudy
'InsertRecordsForVMRBilateralStudy
AddFieldsToSupportSaveOnlySummaryImages 
Sub AddFieldsToSupportSaveOnlySummaryImages ()
	objConnection.Execute "ALTER TABLE tb_BasicConfiguration ADD SaveOnlySummaryImages YesNo NULL DEFAULT 0" 
	objConnection.Execute "ALTER TABLE tb_BVExamination ADD SaveOnlySummaryImages YesNo NULL DEFAULT 0" 
	
End Sub
sub  InsertRecordsForVMRBilateralStudy()
	
	
	
	
	
    objRecordSet.Open "SELECT * FROM tb_Study Where Name='VMR Bilateral'", _
    objConnection, adOpenStatic, adLockOptimistic
    Set objRecordSet2 = CreateObject("ADODB.Recordset")	
    objRecordSet2.Open "SELECT Blood_Vessel_Index FROM tb_BloodVesselSetup WHERE Name = 'MCA'and Side =0", _
    objConnection, adOpenStatic, adLockOptimistic 
    
    ' 2.1) Insert the MCA-L bv to the VMRBilateral study
    if objRecordSet2.RecordCount>0 Then
		 s= 	"INSERT INTO tb_Study_BV  (Study_Index,Blood_Vessel_Index,IndexInStudy,Valid) VALUES (" +objRecordSet.Fields("Study_Index").Value + "," +objRecordSet2.Fields("Blood_Vessel_Index").Value + ",0,1)"
		 objConnection.Execute s
	end if
	' 2.2) Insert the MCA-L bv to the VMRBilateral study
	objRecordSet2.Close
	objRecordSet2.Open "SELECT Blood_Vessel_Index FROM tb_BloodVesselSetup WHERE Name = 'MCA'and Side =1", _
    	objConnection, adOpenStatic, adLockOptimistic 
	if objRecordSet2.RecordCount>0 Then
		 s= 	"INSERT INTO tb_Study_BV  (Study_Index,Blood_Vessel_Index,IndexInStudy,Valid) VALUES (" +objRecordSet.Fields("Study_Index").Value + "," +objRecordSet2.Fields("Blood_Vessel_Index").Value + ",0,1)"
		 objConnection.Execute s
	end if
	'3) Insert the relevant CP to the VMR Bilateral study (The VMR CP And the PEAK CP)
	objRecordSet2.Close
	objRecordSet2.Open "SELECT Index FROM tb_ClinicalParam WHERE Name = 'VMR'", _
    objConnection, adOpenStatic, adLockOptimistic 
	
	
	s= 	"INSERT INTO tb_Study_ClinicalParam  (ClinicalParam_Index,Study_Index,IndexInStudy,Valid) VALUES (" +objRecordSet2.Fields("Index").Value + "," +objRecordSet.Fields("Study_Index").Value + ",0,1)"
	objConnection.Execute s
	
	objRecordSet2.Close
	objRecordSet2.Open "SELECT Index FROM tb_ClinicalParam WHERE Name = 'Peak'", _
    objConnection, adOpenStatic, adLockOptimistic 
	
	
	s= 	"INSERT INTO tb_Study_ClinicalParam  (ClinicalParam_Index,Study_Index,IndexInStudy,Valid) VALUES (" +objRecordSet2.Fields("Index").Value + "," +objRecordSet.Fields("Study_Index").Value + ",1,1)"
	objConnection.Execute s
	
	'4) Insert the relevants events to the VMR Bilateral Study (Baseline DIMOX and TEST)
	s= "INSERT INTO tb_Events (Study_Index, Name,IndexInStudy, EventType, Valid ) VALUES(" +objRecordSet.Fields("Study_Index").Value + ",'BaseLine Velocity',0,1,1)"
	objConnection.Execute s
	s= "INSERT INTO tb_Events (Study_Index, Name,IndexInStudy, EventType, Valid ) VALUES(" +objRecordSet.Fields("Study_Index").Value + ",'DIMOX',1,1,1)"
	objConnection.Execute s
	s= "INSERT INTO tb_Events (Study_Index, Name,IndexInStudy, EventType, Valid ) VALUES(" +objRecordSet.Fields("Study_Index").Value + ",'Test Velocity',2,1,1)"
	objConnection.Execute s
	
	
    set objRecordSet2 =Nothing
    objRecordSet.Close
    
end Sub

Sub InsertVMRBilateralstudy()

    objRecordSet.Open "SELECT * FROM tb_Study Where Name='VMR Bilateral'", _
    objConnection, adOpenStatic, adLockOptimistic
    Set objRecordSet2 = CreateObject("ADODB.Recordset")	
    objRecordSet2.Open "SELECT Name,SerializedData FROM tb_Study WHERE Name = 'Monitoring Intracranial Bilateral'", _
    objConnection, adOpenStatic, adLockOptimistic 
    
    
  	'If the VMR Study Does not exist Yet	
    If objRecordSet.RecordCount<1 Then
        s= 	"INSERT INTO tb_Study  (Name,DefaultStudy,BasedOn,NumOfProbes,Monitoring,AuthorizationMgr,SpecialInfo ) VALUES ('VMR Bilateral',1,'00000000-0000-0000-0000-000000000000',2,1,1,'7')"
	    objConnection.Execute s
        objRecordSet.Close
        objRecordSet.Open "SELECT * FROM tb_Study Where Name='VMR Bilateral'", _
	    objConnection, adOpenStatic, adLockOptimistic 
        objRecordSet.MoveFirst
        ' Copying the OLE object (The save Layout class ) from  'Monitoring Intracranial Bilateral'
        objRecordSet.Fields("SerializedData").value =objRecordSet2.Fields("SerializedData").value
       
	    objRecordSet.update	
        set objRecordSet2 = nothing	
        
    End If

    objRecordSet.Close 
    Set objRecordSet2 = nothing
End sub