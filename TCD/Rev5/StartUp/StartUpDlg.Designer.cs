﻿namespace StartUp
{
    partial class StartUpDlg
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StartUpDlg));
            this.btnTcd = new System.Windows.Forms.Button();
            this.btnCarotid = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnExitWindows = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnTcd
            // 
            this.btnTcd.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnTcd.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnTcd.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnTcd.Location = new System.Drawing.Point(328, 240);
            this.btnTcd.Name = "btnTcd";
            this.btnTcd.Size = new System.Drawing.Size(368, 32);
            this.btnTcd.TabIndex = 0;
            this.btnTcd.Text = "Digi-Lite™ TCD application ";
            this.btnTcd.UseVisualStyleBackColor = false;
            this.btnTcd.Click += new System.EventHandler(this.btnTcd_Click);
            // 
            // btnCarotid
            // 
            this.btnCarotid.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnCarotid.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnCarotid.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCarotid.Location = new System.Drawing.Point(328, 284);
            this.btnCarotid.Name = "btnCarotid";
            this.btnCarotid.Size = new System.Drawing.Size(368, 32);
            this.btnCarotid.TabIndex = 1;
            this.btnCarotid.Text = "Digi-Lite™IP carotid imaging application ";
            this.btnCarotid.UseVisualStyleBackColor = false;
            this.btnCarotid.Click += new System.EventHandler(this.btnCarotid_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1024, 768);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // btnExitWindows
            // 
            this.btnExitWindows.BackColor = System.Drawing.Color.DarkRed;
            this.btnExitWindows.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnExitWindows.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnExitWindows.Location = new System.Drawing.Point(464, 416);
            this.btnExitWindows.Name = "btnExitWindows";
            this.btnExitWindows.Size = new System.Drawing.Size(112, 32);
            this.btnExitWindows.TabIndex = 5;
            this.btnExitWindows.Text = "Shut down";
            this.btnExitWindows.UseVisualStyleBackColor = false;
            this.btnExitWindows.Click += new System.EventHandler(this.btnExitWindows_Click);
            // 
            // StartUpDlg
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.ControlBox = false;
            this.Controls.Add(this.btnExitWindows);
            this.Controls.Add(this.btnCarotid);
            this.Controls.Add(this.btnTcd);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1024, 768);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1024, 768);
            this.Name = "StartUpDlg";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Main Menu";
            this.TopMost = true;
            this.Closing += new System.ComponentModel.CancelEventHandler(this.StartUpDlg_Closing);
            this.VisibleChanged += new System.EventHandler(this.StartUpDlg_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnTcd;
        private System.Windows.Forms.Button btnCarotid;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnExitWindows;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
    }
}