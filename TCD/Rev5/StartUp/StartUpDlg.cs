using System;
using System.Diagnostics;
using System.Drawing;
using System.ComponentModel;
using System.IO;
using System.Timers;
using System.Threading;
using System.Windows.Forms;

using TCD2003.Utils;

namespace StartUp
{
    /// <summary>
    /// Summary description for StartUpDlg.
    /// </summary>
    public partial class StartUpDlg : System.Windows.Forms.Form
    {
        const int ERROR_FILE_NOT_FOUND = 2;
        const int ERROR_ACCESS_DENIED = 5;

        private const int WM_QUERYENDSESSION = 0x0011;
        internal static bool shuttingDown;

        private Process dlProcess = new Process();
        private Process diProcess = new Process();
        private Process spyProcess = new Process();

        private string ewVersion = "DigiLite™ IP";

        public StartUpDlg()
        {
            InitializeComponent();
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            bool OneInstanceOnly = false;
            using (Mutex mx = new Mutex(true, "DG-Lite StartUp", out OneInstanceOnly))
            {
                if (OneInstanceOnly)
                {
                    try
                    {
                        Application.Run(new StartUpDlg());
                    }
                    catch (Exception ex)
                    {
                        ExceptionPublisherLog4Net.ExceptionErrorLog( ex);
                    }
                }
                else
                {
                    StartUpDlg.ActiveForm.Show();
                    StartUpDlg.ActiveForm.Activate();
                }
            }
        }

        private void btnTcd_Click(object sender, System.EventArgs e)
        {
            btnTcd.BackColor = Color.LightSteelBlue;
            RunApplication(Path.Combine(Constants.AppPath, "GUI.exe"), false, dlProcess);
            //WindowsController.HideWindow(this.Text);
        }

        private void btnCarotid_Click(object sender, System.EventArgs e)
        {
            btnCarotid.BackColor = Color.LightSteelBlue;
            this.Cursor = Cursors.WaitCursor;

            if (WindowsController.IsSpyExist)
            {
                WindowsController.HideWindow("Spy(Digi-Imager Plug-in)");
                WindowsController.RestoreWindow(ewVersion);
                WindowsController.HideWindow(this.Text);
            }
            else
            {
                if (File.Exists(@"C:\Program Files\Rimed\Digi-Imager\EchoWave.exe"))
                {
                    ewVersion = "Digi-Imager";
                    RunApplication(@"C:\Program Files\Rimed\Digi-Imager\EchoWave.exe", true, diProcess);
                }
                else
                    if (File.Exists(@"C:\Program Files\Rimed\Digi-Lite tm IP\EchoWave.exe"))
                    {
                        ewVersion = "Digi-Lite tm IP";
                        RunApplication(@"C:\Program Files\Rimed\Digi-Lite tm IP\EchoWave.exe", true, diProcess);
                    }
                    else
                    {
                        MessageBox.Show("DigiLite™ IP application was not purchased", "Information", MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
                        RestoreMainMenu();
                    }
            }
        }

        private void RunApplication(string fileName, bool isDigiImager, Process appProcess)
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {
                btnTcd.Enabled = false;
                btnCarotid.Enabled = false;
                btnExitWindows.Enabled = false;

                appProcess.StartInfo.FileName = fileName;
                //Commented by Natalie: for RIMD-139 fix. 
                //In case when DL was closed correctly, actions from appProcess_Exited will complete twice
                appProcess.EnableRaisingEvents = true;
                appProcess.Exited += new EventHandler(appProcess_Exited);
                appProcess.Start();

                if (isDigiImager)
                {
                    System.Timers.Timer timer = new System.Timers.Timer(20000);
                    timer.Elapsed += new ElapsedEventHandler(OnTimedHide);
                    timer.Start();
                }

                TopMost = false;
            }
            catch (Win32Exception ex)
            {
                if (ex.NativeErrorCode == ERROR_FILE_NOT_FOUND)
                {
                    ExceptionPublisherLog4Net.ExceptionErrorLog(ex, "This application was not purchased");
                    MessageBox.Show("DigiLite™ IP application was not purchased", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    RestoreMainMenu();
                }

                else if (ex.NativeErrorCode == ERROR_ACCESS_DENIED)
                {
                    ExceptionPublisherLog4Net.ExceptionErrorLog(ex, "You don't have permission to DigiLite™ imaging of Carotid");
                    MessageBox.Show("You don't have permission to DigiLite™ imaging of Carotid", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    RestoreMainMenu();
                }
                else
                    ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
            }
            catch (Exception ex)
            {
                ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
                throw;
            }
        }

        private void OnTimedHide(object source, ElapsedEventArgs e)
        {
            try
            {
                spyProcess.StartInfo.FileName = Path.Combine(Constants.AppPath, "Spy.exe");
                spyProcess.EnableRaisingEvents = true;
                spyProcess.Start();
            }
            catch (Win32Exception ex)
            {
                if (ex.NativeErrorCode == ERROR_FILE_NOT_FOUND)
                {
                    MessageBox.Show("Some files not found on your computer. Software can work incorrectly. Please reinstall DigiLite™ application.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    ExceptionPublisherLog4Net.ExceptionErrorLog(ex, "Some files not found on your computer");
                    RestoreMainMenu();
                }
                else
                    ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
            }
            catch (Exception ex)
            {
                ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
                throw;
            }
            ((System.Timers.Timer)source).Stop();
            WindowsController.HideWindow(this.Text);
        }

        private void btnExitWindows_Click(object sender, System.EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to quit?", "Information", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1) == DialogResult.OK)
            {
                ProcessController.ShutdownWindows();
            }
        }



        private void appProcess_Exited(object sender, EventArgs e)
        {
            if (((Process)sender).StartInfo.FileName.EndsWith("EchoWave.exe"))
                spyProcess.Kill();
            RestoreMainMenu();
            WindowsController.ShowWindow(this.Text);
        }

        private void RestoreMainMenu()
        {
            if (InvokeRequired)
            {
                Invoke((Action) RestoreMainMenu);
                return;
            }

            btnTcd.Enabled = true;
            btnCarotid.Enabled = true;
            btnExitWindows.Enabled = true;

            btnTcd.BackColor = Color.LightSlateGray;
            btnCarotid.BackColor = Color.LightSlateGray;
            Cursor = Cursors.Default;

            TopMost = true;
        }

        private void StartUpDlg_VisibleChanged(object sender, System.EventArgs e)
        {
            if (Visible)
                RestoreMainMenu();
        }

        //Disable Alt+F4. Added by Natalie on 08/21/2009
        private void StartUpDlg_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!shuttingDown)
                e.Cancel = true;
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_QUERYENDSESSION)
                shuttingDown = true;

            try
            {
                base.WndProc(ref m);
            }
            catch (Exception ex)
            {
                ExceptionPublisherLog4Net.ExceptionErrorLog(ex, string.Format("{0}.WndProc({1})", Name, m));
            }
        }
    }
}
