#include "StdAfx.h"
#include "SpectrumPainter.h"

TCD2003::DspBlock::SpectrumPainter^ TCD2003::DspBlock::SpectrumPainter::Create()
{
	if (s_Instance == nullptr)
		s_Instance = gcnew SpectrumPainter();
	return s_Instance;
}

TCD2003::DspBlock::SpectrumPainter::SpectrumPainter()
: m_Disposed(false)
{
}

TCD2003::DspBlock::SpectrumPainter::~SpectrumPainter()
{
	if (!m_Disposed)
	{
		m_Disposed = true;
	}
	s_Instance = nullptr;
}

void TCD2003::DspBlock::SpectrumPainter::SetPixel(System::IntPtr pointer, int x, int y, int stride, byte R, byte G, byte B)
{
    BYTE* dst = (BYTE*)pointer.ToPointer();
	int offset = x * sizeof(Pixel) + y * stride;

	Pixel& p = *reinterpret_cast<Pixel*>(dst + offset);

	p.Red = R;
	p.Green = G;
	p.Blue = B;
}

void TCD2003::DspBlock::SpectrumPainter::SetLine(System::IntPtr pointer, int x1, int y1, int x2, int y2, int stride, byte R, byte G, byte B)
{
    BYTE* dst = (BYTE*)pointer.ToPointer();

    for(int i = x1; i <= x2; i++)
        for(int j = y1; j <= y2; j++)
        {
        	int offset = i * sizeof(Pixel) + j * stride;
	        Pixel& p = *reinterpret_cast<Pixel*>(dst + offset);

	        p.Red = R;
	        p.Green = G;
	        p.Blue = B;
        }
}