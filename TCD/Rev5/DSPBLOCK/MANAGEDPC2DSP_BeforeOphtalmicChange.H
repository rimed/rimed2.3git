#pragma once

#include "WrapperMacros.h"


namespace TCD2003
{
namespace DspBlock
{

	/// See Pc2Dsp documentation for data structure
	//-----------------------------------------------


/*===================================================================================================
	PC2DSP Struct definition
===================================================================================================*/
BEGIN_CLASS( R_Gate_Info )
	ADD_GET_SET_VAL( R_Gate_Info, WORD, m_Depth )
	ADD_GET_SET_VAL( R_Gate_Info, WORD, m_HitEnable )
	ADD_GET_SET_VAL_ARRAY( R_Gate_Info, BYTE, RESERVE )
END_CLASS

BEGIN_CLASS( R_Card_Info )
	ADD_GET_SET_VAL( R_Card_Info, WORD, m_OperationMode )
	ADD_GET_SET_VAL( R_Card_Info, WORD, m_SoundVolume )
	ADD_GET_SET_VAL( R_Card_Info, WORD, m_Gate2Hear )
	ADD_GET_SET_VAL( R_Card_Info, WORD, m_FFTSize )
	ADD_GET_SET_VAL( R_Card_Info, WORD, m_HitThreshHold )
	ADD_GET_SET_VAL( R_Card_Info, WORD, m_SpeakerOrHeadphone )
	ADD_GET_SET_VAL( R_Card_Info, BYTE, m_MModeThreshould1 )
	ADD_GET_SET_VAL( R_Card_Info, BYTE, m_MModeThreshould2)
	ADD_GET_SET_VAL( R_Card_Info, BYTE, m_ZeroLinePercent)
	ADD_GET_SET_VAL_ARRAY( R_Gate_Info, BYTE, RESERVE );
END_CLASS

BEGIN_CLASS( R_Probe_Info )
	ADD_GET_SET_VAL( R_Probe_Info, WORD, m_ProbeType )
	ADD_GET_SET_VAL( R_Probe_Info, WORD, m_Width )
	ADD_GET_SET_VAL( R_Probe_Info, WORD, m_Power )
	ADD_GET_SET_VAL( R_Probe_Info, WORD, m_Gain )
	ADD_GET_SET_VAL( R_Probe_Info, WORD, m_Range )
	ADD_GET_SET_VAL( R_Probe_Info, WORD, m_Thump )
	ADD_GET_SET_VAL( R_Probe_Info, WORD, m_RangeDivisor )
	ADD_GET_SET_VAL( R_Probe_Info, WORD, m_FcShift )
	ADD_GET_SET_VAL( R_Probe_Info, WORD, m_NumOfGates )
	ADD_GET_SET_VAL( R_Probe_Info, WORD, m_StartDepth )
	ADD_GET_SET_VAL( R_Probe_Info, WORD, m_DepthStep )
	ADD_GET_SET_VAL( R_Probe_Info, WORD, m_DirectionSwitch )
 	ADD_GET_SET_VAL_ARRAY( R_Gate_Info, BYTE, RESERVE )
	ADD_GET_ARRAY( R_Probe_Info, R_Gate_Info, m_Gate_sa )
END_CLASS

BEGIN_CLASS( R_PC2DSP )
	// 32bits Counter - for Verifying
	ADD_GET_SET_VAL( R_DSP2PC, int, Numerator )
	ADD_GET( R_PC2DSP, R_Card_Info, m_GenCardInfo_s )
	ADD_GET_ARRAY( R_PC2DSP, R_Probe_Info, m_ProbeInfo_sa )
	ADD_GET_SET_VAL_ARRAY( R_PC2DSP, WORD, _DEBUG_ )
END_CLASS



}
}