// DspBlock.h

#pragma once

#include "vcclr.h"

#include "TCD_DSP2PC.h"
#include "ManagedDsp2Pc.h"

using namespace System;
using namespace System::Threading;
using namespace TCD2003::DummyData;
using namespace TCD2003::XZip;

namespace TCD2003
{
	namespace DspBlock
	{
		/// implementation of the Dsp2PcData
		public ref class Dsp2PcData sealed
		{
		private: 

			// ELiran - mark this line out
			const Dsp2PcData ^m_DspData;
			//Mutex* mut ;

			// Raw data Array 
			BYTE * m_pRawData;

			// Fill the buffer from a file. This is used as dummy data for simulation
			// with no DSP.
			// This function fills the spectrum data, clinical parameters and TimeDomain data in the
			// entire buffer (30 seconds) according to the NUM_OF_DSP2PC_BLOCKS.
			void FillBuffer()
			{
				//(((::R_DSP2PC *)m_pRawData)+1)->Numerator=1;
				////((::R_DSP2PC *)m_pRawData[1])->Numerator=2;

				//::R_DSP2PC* p = ((::R_DSP2PC*)&m_pRawData[0]);
				//p->Numerator = 1;
				//(p+1)->Numerator = 2;

				AutoScanDummyData::theAutoScanData->Init();
				FFTDummyData::theFFTData->Init();

				double GeneralTime = 0;

				int counter = 0;
				//for each on of the DSP blocks (which is 3 spectrum columns)
				for (int i = 0; i < NUM_OF_DSP2PC_BLOCKS; i++)
				{
					//get a ptr to the relevan block
					::R_DSP2PC* p = ((::R_DSP2PC*)&m_pRawData[0]);
					p = p + i;
					p->Numerator = i;
					//add some data to the debug block. The data in this section can be saved
					//using the debug menu option - save debug data.
					if (i == 10)
					{
						for (int ii = 0; ii < 100; ii++)
						{
							p->Control_Flags_and_Debug_Block.TBD[ii] = ii;
						}
						p->Control_Flags_and_Debug_Block.Size = 100;
					}
					else
					{
						p->Control_Flags_and_Debug_Block.Size = 0;
					}


					//fill the autoscan data 
					for(int probe=0; probe<2; probe++)
					{
						double dLocalTime=GeneralTime;

						for(int col=0; col<3; col++, dLocalTime+=0.008)
						{
							ASSingleCycle^ AutoScanSC = AutoScanDummyData::theAutoScanData->GetData(dLocalTime);
							for(int pixel=0; pixel<64; pixel++)
							{
								BYTE *b = &p->Auto_Scan_Block.R_Probe[probe].Column[col].Pixel[pixel];
								*b = AutoScanSC->Data[pixel];
							}
						}
					}

					//fill the spectrum data for each gate in the card
					for (int Gate=0; Gate<(GATES_PER_PROBE * NUMBER_OF_PROBES); Gate++)
					{
						double dLocalTime=GeneralTime;
						for (int Column=0;Column<3;Column++)
						{
							//get the relevant data from the dummy data object with an offset for each gate
							//so that each gate will have a different picture.
							SingleCycle ^SC=FFTDummyData::theFFTData->GetData(dLocalTime+Gate*10*0.008);
							//fill the 256 FFT values and the envelopes
							for (int Index=0;Index<256;Index++)
							{
								BYTE *b=&p->FFT_Spectrum_Block.Gate[Gate].Column[Column].Index[Index];
								*b=SC->FFT[Index];
							}
							p->FFT_Envelopes_Block.Peak.Gate[Gate].Column[Column].Forward=(BYTE)SC->Peak_fwd_envelope;
							p->FFT_Envelopes_Block.Peak.Gate[Gate].Column[Column].Reverse=(BYTE)SC->Peak_bck_envelope;
							p->FFT_Envelopes_Block.Mode.Gate[Gate].Column[Column].Forward=(BYTE)SC->Mode_fwd_envelope;
							p->FFT_Envelopes_Block.Mode.Gate[Gate].Column[Column].Reverse=(BYTE)SC->Mode_bck_envelope;

							//clinical parameters
							p->Clinical_Parameters_Block.Gate[Gate].Peak.Forward=SC->Peak_fwd;
							BYTE *b=(BYTE *)&p->Clinical_Parameters_Block.Gate[Gate].Peak.Forward;
							p->Clinical_Parameters_Block.Gate[Gate].Peak.Reverse=SC->Peak_bck;
							p->Clinical_Parameters_Block.Gate[Gate].Mean.Forward=SC->Mean_fwd;
							p->Clinical_Parameters_Block.Gate[Gate].Mean.Reverse=SC->Mean_bck;
							p->Clinical_Parameters_Block.Gate[Gate].Mode.Forward=SC->Mode_fwd;
							p->Clinical_Parameters_Block.Gate[Gate].Mode.Reverse=SC->Mode_bck;
							p->Clinical_Parameters_Block.Gate[Gate].SW.Forward=SC->SW_fwd;
							p->Clinical_Parameters_Block.Gate[Gate].SW.Reverse=SC->SW_bck;


							//advance the time to the next cokumn
							dLocalTime+=DSP_INT_CYCLE_SEC/3.0;
						}


						////Time Domain
						//double TDTime=GeneralTime;
						//for (int TDCounter=0;TDCounter<(int)(0.024/0.000125);TDCounter++)
						//{
						//	TDSingleCycle *TDSC = TimeDomainDummyData::get_Instance()->GetData(TDTime+0.000125*TDCounter);
						//	p->Time_Domain_Block.Gate[Gate].Value[TDCounter].I=TDSC->I;
						//	p->Time_Domain_Block.Gate[Gate].Value[TDCounter].Q=TDSC->Q;
						//}

					}
					//advance the time to hte next block
					GeneralTime+=DSP_INT_CYCLE_SEC;

					// add some random data to hits table
					int random = (int)(GeneralTime*3652);
					if ((random%40)==0)
						p->HITS_Data.m_Gate_sa[0].m_Column_sa[0].Energy = random%250;
					else
						p->HITS_Data.m_Gate_sa[0].m_Column_sa[0].Energy = 0;
					p->HITS_Data.m_Gate_sa[0].m_Column_sa[0].Energy = 0;
				}


			}
			/*
			// static constructor
			static Dsp2PcData()
			{
			m_DspData = new Dsp2PcData();
			}
			*/

			bool m_DspExists; // Flags if the real HW is in the system
		public:

			Dsp2PcData(bool bDspExists, int _DSP2PC_BUFFER_LEN_SEC)
			{
				NUM_OF_DSP2PC_BLOCKS = (int)((double)_DSP2PC_BUFFER_LEN_SEC / DSP_INT_CYCLE_SEC);	// 60 (or 30) seconds divided by 24 milliSec per block

				m_DspExists = bDspExists;

				// Allocate the raw data buffer
				int Size = sizeof( ::R_DSP2PC ) * NUM_OF_DSP2PC_BLOCKS;
				m_pRawData = new BYTE[ sizeof( ::R_DSP2PC ) * NUM_OF_DSP2PC_BLOCKS ];

				if ( m_pRawData == NULL )
					System::Diagnostics::Trace::Fail( "Failed to allocate DSP buffer" );
				ZeroMemory(m_pRawData, sizeof( ::R_DSP2PC ) * NUM_OF_DSP2PC_BLOCKS);
				//for (int i=0;i<sizeof( ::R_DSP2PC ) * NUM_OF_DSP2PC_BLOCKS;i++)
				//m_pRawData[i]=69; // just for debug

				if ( ! m_DspExists )
					FillBuffer();

				// mut = new Mutex();


			}

			static const double DSP_INT_CYCLE_SEC = 0.024; // DSP Interrupts every 24 milli sec
			static const double DSP_TD_SAMPLE_RATE = 8; //number of TD samples in milisec
			static int NUM_OF_DSP2PC_BLOCKS;

		public:
			/*
			__property static const Dsp2PcData* get_Instance()
			{
			return m_DspData;
			}
			*/ 

			//This function is used for getting the right block of data for TimeDomain according to time.
			//Get a column index in the TD block according to a specified time
			// 
			static int ColumnInTDBlock (double t)
			{
				t=t-((int)(t/DSP_INT_CYCLE_SEC))*DSP_INT_CYCLE_SEC;
				int res=(int)System::Math::Round(t/DSP_INT_CYCLE_SEC/(1/(DSP_TD_SAMPLE_RATE*24)));
				return res;
			}

			// Get the right column in the block since there are 3 columns in each block and we only
			// want 1 of them.
			static int ColumnInBlock (double t)
			{
				double Backt=t;
				t=System::Math::Round(t*1000)/1000;
				t=t-((int)(System::Math::Round((t*1000)/DSP_INT_CYCLE_SEC)/1000)*DSP_INT_CYCLE_SEC);
				int res=(int)System::Math::Round((t/DSP_INT_CYCLE_SEC)*3.0);
				if (res>2)
					res=2;
				return res;
			}

			// Get a single block corresponding to a specified time
			R_DSP2PC GetBlockByTime( double t)
			{
				//round the to the 3rd digit agter decimal point
				int index = (int)(System::Math::Round((t / DSP_INT_CYCLE_SEC)*1000)/1000); // 24 mSec per block
				return GetBlockByIndex( index);
			}

			// get a single block by an index
			R_DSP2PC GetBlockByIndex( int index )
			{
				//int ww = 0;
				//if ( index == -1 )
				//	ww=5;

				//System::Diagno9stics::Debug::WriteLine( index.ToString() );
				int i = index % NUM_OF_DSP2PC_BLOCKS; // Wrap around in the buffer
				int test = sizeof( ::R_DSP2PC );
				return R_DSP2PC( m_pRawData + i * sizeof( ::R_DSP2PC ) );
			}
			bool UnzipFile(System::String ^FileName)
			{
				CString x = FileName;
				CFile ff;
				BOOL res = ff.Open(x, CFile::modeRead);
				if(!res) // the file might be zipped.
				{
					x += ".tmp";
					XZipManager::theXZip->UnzipFile(gcnew System::String(x), FileName);
					//after reading the file delete the tmp file
					//DeleteFile(tmp);
				}
				return true;
			}
			// This function gets a file name and fills the buffer of the DSP from it.
			// The function extracts the dat (.zip) file and retreive the data into the buffer.
			// Params:
			//	string *FileName - full path to the file to load
			//	int &LastBuffer -  out param to indicate the number of buffers loaded. used in the replay process.
			//	return - true if successful.
			bool LoadRawDataFromFile(System::String ^FileName, int &LastBuffer)
			{
				CString x = FileName;
				CFile ff;
				BOOL res = ff.Open(x, CFile::modeRead);
				ZeroMemory(m_pRawData, sizeof( ::R_DSP2PC ) * NUM_OF_DSP2PC_BLOCKS);
				if(!res) // the file might be zipped.
				{
					x += ".tmp";
					if (XZipManager::theXZip->UnzipFile(gcnew System::String(x), FileName))
					{
						CString tmp = FileName;

						CFile f;
						f.Open(tmp, CFile::modeRead);

						//now that the file is extracted we can read it into the buffer
						//read the file into the buffer
						f.Seek(0, CFile::begin);
						f.Read(&LastBuffer, sizeof(LastBuffer));
						f.Read(m_pRawData, LastBuffer*sizeof( ::R_DSP2PC ));
						f.Close();
						//after reading the file delete the tmp file
						DeleteFile(tmp);
					}
				}
				else
				{
					// regular open.
					//now that the file is extracted we can read it into the buffer
					//read the file into the buffer
					ff.Seek(0,CFile::begin);
					ff.Read(&LastBuffer,sizeof(LastBuffer));
					ff.Read(m_pRawData,LastBuffer*sizeof( ::R_DSP2PC ));
					ff.Close();
				}
				return true;
			}

			/*Assaf : there a a change when loading file in monitoring mode. We use "Double Buffering" and
			the allocated  in the depending on its number (ODD/EVEN) */
			bool LoadMonitoringRawDataFromFile(System::String ^FileName, int &LastBuffer,bool odd)
			{
				CString x = FileName;
				CFile ff;
				BOOL res = ff.Open(x, CFile::modeRead);
				int i =sizeof( ::R_DSP2PC ) * NUM_OF_DSP2PC_BLOCKS;

				if(!res) // the file might be zipped.
				{
					x += ".tmp";
					CString tmp = FileName;

					CFile f;
					// we will create a temporary file to open the zip file to it, 
					// then we will copy the file to the buffer
					// and then we will delete it.
					if (XZipManager::theXZip->UnzipFile(gcnew System::String(x), FileName))
						f.Open(tmp, CFile::modeRead);
					else
						f.Open(tmp, CFile::modeReadWrite | CFile::modeCreate);

					//now that the file is extracted we can read it into the buffer
					//read the file into the buffer
					//ZeroMemory(m_pRawData, sizeof( ::R_DSP2PC ) * NUM_OF_DSP2PC_BLOCKS);
					f.Seek(0, CFile::begin);
					f.Read(&LastBuffer, sizeof(LastBuffer));
					int j=sizeof( ::R_DSP2PC ) * NUM_OF_DSP2PC_BLOCKS;
					System::Diagnostics::Debug::WriteLine("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
					try
					{
						if(odd)
						{
							f.Read(m_pRawData+(j/2), LastBuffer*sizeof( ::R_DSP2PC ));
							System::Diagnostics::Debug::WriteLine("Second part of buffer loaded");
						}
						else
						{
							f.Read(m_pRawData, LastBuffer*sizeof( ::R_DSP2PC ));
							System::Diagnostics::Debug::WriteLine("First part of buffer loaded");
						}
						System::Diagnostics::Debug::WriteLine("File name:" );
						System::Diagnostics::Debug::WriteLine(*x);
						System::Diagnostics::Debug::WriteLine("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV");
					}
					catch (CException*)
					{

					}

					f.Close();
					//after reading the file delete the tmp file
					DeleteFile(tmp);
				}
				else
				{
					//regular open.
					//now that the file is extracted we can read it into the buffer
					//read the file into the buffer
					ff.Seek(0,CFile::begin);
					ff.Read(&LastBuffer,sizeof(LastBuffer));

					if(m_pRawData != NULL)
					{
						if(!odd)
						{
							ff.Read(m_pRawData, LastBuffer*sizeof( ::R_DSP2PC )/2);

						}
						else
						{
							ff.Read(m_pRawData+(i/2), LastBuffer*sizeof( ::R_DSP2PC )/2);

						}
					}

					ff.Close();
				}

				//System::Diagnostics::Debug::WriteLine(System::DateTime::Now.Millisecond.ToString());*/
				return true;

			}
			// This function gets a file name and saves the DSP buffer to this file
			// and zips it.
			//	string *FileName - full path to the file to save
			//	int LastBuffer -  Number of DSP buffers to save (up to 30 seconds of buffers).
			//	return - true if successful.
			bool Save2File(System::String ^FileName,int LastBuffer)
			{
				int i=LastBuffer%NUM_OF_DSP2PC_BLOCKS;
				TCHAR *tmp=new TCHAR[100];

				/*
				//Zip in memory and write to file
				int BufLen=10*1024*1024;
				HZIP hz=CreateZip(0,BufLen ,ZIP_MEMORY);	//create zip file in page file

				int i=LastBuffer%NUM_OF_DSP2PC_BLOCKS;
				zzz= ZipAdd(hz,("f1.dat"), m_pRawData,sizeof( ::R_DSP2PC ) * (i+1),ZIP_MEMORY);
				zzz= ZipAdd(hz,("f2.dat"), m_pRawData+sizeof( ::R_DSP2PC )*(i+1),sizeof( ::R_DSP2PC )*(NUM_OF_DSP2PC_BLOCKS-i-1),ZIP_MEMORY);
				void *ResBuf;
				unsigned long len;
				zzz=ZipGetMemory(hz, &ResBuf, &len);
				CFile f;
				CString x=FileName;

				if (!f.Open(x,CFile::modeCreate|CFile::modeWrite))
				{
				return (false);
				}
				f.Write(ResBuf,len);
				f.Close();
				*/
				//Zip directly to file
				/*
				CFile f;
				CString x=FileName;
				if (!f.Open(x,CFile::modeCreate|CFile::modeWrite))
				{
				return (false);
				}

				HZIP hz=CreateZip(f.m_hFile,0,ZIP_HANDLE);
				//			ZRESULT zzz= ZipAdd(hz,("file.dat"), m_pRawData,sizeof( ::R_DSP2PC ) * NUM_OF_DSP2PC_BLOCKS,ZIP_MEMORY);
				int i=LastBuffer%NUM_OF_DSP2PC_BLOCKS;
				zzz= ZipAdd(hz,("f1.dat"), m_pRawData,sizeof( ::R_DSP2PC ) * (i+1),_MEMORY);
				//save the second file only if there was a loop back in the data
				if (LastBuffer>NUM_OF_DSP2PC_BLOCKS)
				zzz= ZipAdd(hz,("f2.dat"), m_pRawData+sizeof( ::R_DSP2PC )*(i+1),sizeof( ::R_DSP2PC )*(NUM_OF_DSP2PC_BLOCKS-i-1),ZIP_MEMORY);
				else
				zzz= ZipAdd(hz,("f2.dat"), m_pRawData,1,ZIP_MEMORY);
				CloseZip(hz);
				f.Close();
				*/

				//Write file directly
				CFile f;
				CString x=FileName;

				f.Open(x,CFile::modeCreate|CFile::modeWrite);
				//write the number of buffers that will be in the file
				//max number of buffers or the real number according to hte LastBuffer param
				if (LastBuffer>NUM_OF_DSP2PC_BLOCKS)
				{
					pin_ptr<int> p = &NUM_OF_DSP2PC_BLOCKS;
					f.Write(p,sizeof(NUM_OF_DSP2PC_BLOCKS));
				}
				else
					f.Write(&LastBuffer,sizeof(LastBuffer));
				//Write data to the file. Since the buffer is cyclic buffer write the end of the buffer
				//first and then the beginning.
				if (LastBuffer>NUM_OF_DSP2PC_BLOCKS)
				{
					f.Write(m_pRawData+sizeof( ::R_DSP2PC )*(i+1),sizeof( ::R_DSP2PC )*(NUM_OF_DSP2PC_BLOCKS-i-1));
					f.Write(m_pRawData,sizeof( ::R_DSP2PC ) * (i+1));
				}
				else
				{
					f.Write(m_pRawData,sizeof( ::R_DSP2PC ) * (i+1));		

				}
				f.Close();

				// need to zip x to FileName.
				//now zip the file and then delete the original file

				return (true);
			}
			bool ZipFile(System::String ^FileName)
			{
				CString x = FileName;
				x += ".tmp";
				XZipManager::theXZip->ZipFile(FileName, gcnew System::String(x), "1");
				CString LocalFile = FileName;
				DeleteFile(LocalFile);
				System::Diagnostics::Trace::WriteLine( FileName );
				return (true);
			}
			bool ZipFile(System::String ^FileName, System::String ^ZipFileName)
			{
				XZipManager::theXZip->ZipFile(FileName, ZipFileName, "1");
				return (true);
			}
			// Get a pointer to the start of the raw data buffer
			BYTE *GetPtr()
			{
				return (m_pRawData);
			}
			void DeletePtr()
			{
				ZeroMemory(m_pRawData, sizeof( ::R_DSP2PC ) * NUM_OF_DSP2PC_BLOCKS);
				delete m_pRawData;
			}
			void ZeroMem()
			{
				ZeroMemory(m_pRawData, sizeof( ::R_DSP2PC ) * NUM_OF_DSP2PC_BLOCKS);
			}
			// Get the raw data size
			int GetDataSize()
			{
				return (sizeof( ::R_DSP2PC ) * NUM_OF_DSP2PC_BLOCKS);
			}
		};
	}
}