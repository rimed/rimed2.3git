/////This feature was mark out beacause it is not working well.
//
//
//using System;
//using System.Windows.Forms;
//using SpeechLib;
//
//
//namespace TCD2003.IF
//{
//	/// <summary>
//	/// Summary description for Class1.
//	/// </summary>
//	public class SpeechReconition
//	{
//		private enum LastSelection { eDepth, eRange, eWidth, eThump, ePower, eGain, eZeroLine, eVolume }
//		private LastSelection m_LastSelection;
//		private SpeechLib.SpSharedRecoContext		objRecoContext;
//		private SpeechLib.ISpeechRecoGrammar		grammar;
//		private bool m_Activate = false;
//
//		static private SpeechReconition	m_SpeechReconition = new SpeechReconition();
//
//		static public SpeechReconition theSpeechReconition
//		{
//			get { return m_SpeechReconition; }
//		}
//
//		public SpeechReconition()
//		{
//			m_LastSelection = LastSelection.eDepth;
//			initSAPI();
//		}
//
//		public void initSAPI()
//		{
//			try
//			{
//				objRecoContext = new SpeechLib.SpSharedRecoContext();
//				objRecoContext.Recognition+= 
//					new _ISpeechRecoContextEvents_RecognitionEventHandler(RecoContext_Recognition);
//				objRecoContext.EventInterests=
//					SpeechLib.SpeechRecoEvents.SRERecognition | 
//					SpeechLib.SpeechRecoEvents.SREAudioLevel;
// 
//				//create grammar interface with ID = 0
//				grammar=objRecoContext.CreateGrammar(0);
//			}
//			catch(Exception ex)
//			{
//				MessageBox.Show("Exeption \n"+ex.ToString(),"Error - initSAPI");
//			}
//			SAPIGrammarFromFile(Constants.DataPath + "XMLActivate.xml");
//		}
//		public void RecoContext_Recognition(int StreamNumber, object StreamPosition, SpeechRecognitionType RecognitionType,	ISpeechRecoResult e)
//		{
//			//calculate accuracy
//			float accuracy=(float)e.PhraseInfo.Elements.Item(0).EngineConfidence;
//			
//			if(e.PhraseInfo.Rule.Name == "Activate")
//				m_Activate = true;
//
//			if(m_Activate == false)
//				return;
//
//			//Only if agent enabled
//			switch (e.PhraseInfo.Rule.Name)		//rule name (not the phrase !)
//			{
//				case "Activate":
//					m_Activate = true;
//					FireSpeechReconitionEvent("SpeechActivate");
//					break;
//				case "Deactivate":
//					m_Activate = false;
//					FireSpeechReconitionEvent("SpeechDeactivate");
//					break;
//				case "CloseApp":
//					FireSpeechReconitionEvent("CloseApp");
//					break;
//				case "DepthUp":
//					FireSpeechReconitionEvent("DepthUp");
//					m_LastSelection = LastSelection.eDepth;
//					break;
//				case "DepthDown":
//					FireSpeechReconitionEvent("DepthDown");
//					m_LastSelection = LastSelection.eDepth;
//					break;
//				case "RangeUp":
//					FireSpeechReconitionEvent("RangeUp");
//					m_LastSelection = LastSelection.eRange;
//					break;
//				case "RangeDown":
//					FireSpeechReconitionEvent("RangeDown");
//					m_LastSelection = LastSelection.eRange;
//					break;
//				case "PowerUp":
//					FireSpeechReconitionEvent("PowerUp");
//					m_LastSelection = LastSelection.ePower;
//					break;
//				case "PowerDown":
//					FireSpeechReconitionEvent("PowerDown");
//					m_LastSelection = LastSelection.ePower;
//					break;
//				case "WidthUp":
//					FireSpeechReconitionEvent("WidthUp");
//					m_LastSelection = LastSelection.eWidth;
//					break;
//				case "WidthDown":
//					FireSpeechReconitionEvent("WidthDown");
//					m_LastSelection = LastSelection.eWidth;
//					break;
//				case "ThumpUp":
//					FireSpeechReconitionEvent("ThumpUp");
//					m_LastSelection = LastSelection.eThump;
//					break;
//				case "ThumpDown":
//					FireSpeechReconitionEvent("ThumpDown");
//					m_LastSelection = LastSelection.eThump;
//					break;
//				case "GainUp":
//					FireSpeechReconitionEvent("GainUp");
//					m_LastSelection = LastSelection.eGain;
//					break;
//				case "GainDown":
//					FireSpeechReconitionEvent("GainDown");
//					m_LastSelection = LastSelection.eGain;
//					break;
//				case "Freeze":
//					FireSpeechReconitionEvent("Freeze");
//					break;
//				case "Unfreeze":
//					FireSpeechReconitionEvent("Unfreeze");
//					break;
//				case "ZeroLineUp":
//					FireSpeechReconitionEvent("ZeroLineUp");
//					m_LastSelection = LastSelection.eZeroLine;
//					break;
//				case "ZeroLineDown":
//					FireSpeechReconitionEvent("ZeroLineDown");
//					m_LastSelection = LastSelection.eZeroLine;
//					break;
//				case "Up":
//				{
//					switch (m_LastSelection)
//					{
//						case LastSelection.eDepth:
//							FireSpeechReconitionEvent("DepthUp");
//							break;
//						case LastSelection.eGain:
//							FireSpeechReconitionEvent("GainUp");
//							break;
//						case LastSelection.ePower:
//							FireSpeechReconitionEvent("PowerUp");
//							break;
//						case LastSelection.eRange:
//							FireSpeechReconitionEvent("RangeUp");
//							break;
//						case LastSelection.eThump:
//							FireSpeechReconitionEvent("ThumpUp");
//							break;
//						case LastSelection.eWidth:
//							FireSpeechReconitionEvent("WidthUp");
//							break;
//						case LastSelection.eZeroLine:
//							FireSpeechReconitionEvent("ZeroLineUp");
//							break;
//						case LastSelection.eVolume:
//							FireSpeechReconitionEvent("VolumeUp");
//							break;
//					}
//				}
//					break;
//
//				case "Down":
//				{
//					switch (m_LastSelection)
//					{
//						case LastSelection.eDepth:
//							FireSpeechReconitionEvent("DepthDown");
//							break;
//						case LastSelection.eGain:
//							FireSpeechReconitionEvent("GainDown");
//							break;
//						case LastSelection.ePower:
//							FireSpeechReconitionEvent("PowerDown");
//							break;
//						case LastSelection.eRange:
//							FireSpeechReconitionEvent("RangeDown");
//							break;
//						case LastSelection.eThump:
//							FireSpeechReconitionEvent("ThumpDown");
//							break;
//						case LastSelection.eWidth:
//							FireSpeechReconitionEvent("WidthDown");
//							break;
//						case LastSelection.eZeroLine:
//							FireSpeechReconitionEvent("ZeroLineDown");
//							break;
//						case LastSelection.eVolume:
//							FireSpeechReconitionEvent("VolumeDown");
//							break;
//					}
//				}
//				break;
//				case "Hits":
//					FireSpeechReconitionEvent("Hits");
//					break;
//				case "VolumeUp":
//					FireSpeechReconitionEvent("VolumeUp");
//					m_LastSelection = LastSelection.eVolume;
//					break;
//				case "VolumeDown":
//					FireSpeechReconitionEvent("VolumeDown");
//					m_LastSelection = LastSelection.eVolume;
//					break;
//				case "SummaryScreen":
//					FireSpeechReconitionEvent("SummaryScreen");
//					break;
//				case "Return":
//					FireSpeechReconitionEvent("Return");
//					break;
//				case "Load":
//					FireSpeechReconitionEvent("Load");
//					break;
//			}
//		}
//
//		private void SAPIGrammarFromFile(string FileName)
//		{
//			try
//			{
//				grammar.CmdLoadFromFile(FileName,SpeechLib.SpeechLoadOption.SLODynamic);
//				grammar.CmdSetRuleIdState(0,SpeechRuleState.SGDSActive);
//			}
//			catch
//			{
//				MessageBox.Show("Error loading file "+FileName+"\n","Error - SAPIGrammarFromFile");
//			}
//		}
//
//		public event SpeechReconitionDelegate SpeechReconitionEvent;
//		protected void FireSpeechReconitionEvent(string str) 
//		{
//			if(SpeechReconitionEvent != null)
//			{
//				SpeechReconitionEvent(str);
//			}
//		}
//
//	}
//	public delegate void SpeechReconitionDelegate(string str);
//}
