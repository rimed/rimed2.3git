
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

#if defined(WIN16)
    #include <string.h>
    #define vsnprintf _vsnprintf
#else
    #include "../include/windrvr.h" 
#endif
#include "../include/cstring.h"

#define INITIAL_CSTRING_SIZE 1024
#define MAX_CSTRING_SIZE (ULONG)1024*256  
    
CCString::CCString()
{
    Init();
}

CCString::CCString(PCSTR pcStr)
{
    Init();
    *this = pcStr;
}

CCString::CCString(const CCString& stringSrc)
{
    Init();
    *this = stringSrc;
}

void CCString::Init()
{
    m_str = NULL;
    GetBuffer(INITIAL_CSTRING_SIZE);
}

const CCString& CCString::operator= (PCSTR pcStr)
{
    int len = strlen(pcStr);

    GetBuffer(len);
    strcpy (m_str, pcStr);

    return *this;
}

int CCString::Length()
{
    return strlen(m_str);
}

BOOL CCString::is_empty()
{
    return Length()==0;
}

CCString::operator PCSTR() const 
{
    return m_str;
}

#if defined(WIN16)
CCString::operator LPCSTR() const 
{
    return (LPCSTR) m_str;
}
#endif

CCString::operator char *() const 
{
    return m_str;
}

CCString::~CCString()
{
    if (m_str)
        free (m_str);
}

const BOOL CCString::operator==(const CCString& stringSrc)
{
    return Compare(stringSrc)==0;
}

const BOOL CCString::operator==(const char *stringSrc)
{
    return Compare(stringSrc)==0;
}

const BOOL CCString::operator!=(const CCString& stringSrc)
{
    return Compare(stringSrc)!=0;
}

const BOOL CCString::operator!=(const char *stringSrc)
{
    return Compare(stringSrc)!=0;
}

const CCString& CCString::operator=(const CCString& stringSrc)
{
    *this = stringSrc.m_str;
    return *this;
}

const CCString& CCString::operator+=(const PCSTR pcStr)
{
    int len = strlen(pcStr);

    GetBuffer(Length() + len);
    strcat (m_str, pcStr);

    return *this;
}

const CCString CCString::operator+(const PCSTR pcStr)
{
    CCString tmp(*this);
    tmp += pcStr;
    return tmp;
}

const CCString operator+(const CCString &str1, const CCString &str2)
{
    CCString tmp(str1);
    tmp += str2;
    return tmp;
}

char& CCString::operator[](int i)
{
    return m_str[i];
}

int CCString::Compare(PCSTR psStr)
{
    return ::strcmp(m_str, psStr);
}

#if defined(WIN32)
    #define vsnprintf _vsnprintf
#endif

void CCString::Format(const PCSTR format, ...)
{
    va_list ap;
    va_start(ap, format);
    vsprintf(format, ap);
    va_end(ap);
}

BOOL CCString::IsAllocOK() 
{
    return TRUE;
}  
    
// len is the needed maximum number of characters in string, excluding the terminating NULL
BOOL CCString::GetBuffer(ULONG len)
{
    if (!m_str) 
        m_buf_size = 0;

    // add the terminating NULL and translate to bytes
    int nNeeded = (int) len+1;
    if (m_buf_size>=nNeeded)
        return TRUE;
    
    int nNewSize = nNeeded > INITIAL_CSTRING_SIZE ? nNeeded : INITIAL_CSTRING_SIZE;
    char *tmpBuffer = (char *) malloc (nNewSize);
    if (!tmpBuffer) // Error, Not enough memory
        return FALSE;
    tmpBuffer[0] = '\0';

    if (m_str)
    {
        memcpy(tmpBuffer, m_str, m_buf_size);
        free(m_str);
    }
    m_buf_size = nNewSize;
    m_str = tmpBuffer;

    return TRUE;
}

#if !defined(WIN32)
int stricmp(PCSTR str1, PCSTR str2)
{
    int i;
    for (i=0; str1[i] && str2[i]; i++)
       if (toupper(str1[i])!=toupper(str2[i]))
           return -1;
    return str1[i] || str2[i] ? -1 : 0;
}
#endif

CCString CCString::Mid(int nFirst, int nCount)
{
    CCString sRet;
    if (nFirst>=Length())
        return sRet;
    sRet = (m_str + nFirst);
    if (nCount<Length())
        sRet.m_str[nCount-nFirst] = '\0';
    return sRet;
}

CCString CCString::Mid(int nFirst)
{
    CCString sRet;
    if (nFirst>=Length())
        return sRet;
    sRet = (m_str + nFirst);
    return sRet;
}

int CCString::CompareNoCase(PCSTR str)
{
    unsigned int i;

    for (i=0; i<strlen(str); i++)
    {
        if (::tolower(m_str[i])!=::tolower(str[i]))
            return 1;
    }
    if (i!=strlen(m_str))
        return 1;
    return 0;

}

void CCString::MakeUpper()
{
    int i;

    for (i=0; (UINT) i<strlen(m_str); i++)
        m_str[i] = ::toupper(m_str[i]);
}

void CCString::MakeLower()
{
    int i;

    for (i=0; (UINT) i<strlen(m_str); i++)
        m_str[i] = ::tolower(m_str[i]);
}

void CCString::sprintf(const PCSTR format, ...)
{
    va_list ap;
    va_start(ap, format);
    vsprintf(format, ap);
    va_end(ap);
}

void CCString::vsprintf(const PCSTR format, va_list ap)
{
    int i;
    for (i=INITIAL_CSTRING_SIZE; i<MAX_CSTRING_SIZE; i*=2)
    {
        GetBuffer(i);
        if (vsnprintf(m_str, m_buf_size-1, format, ap)>=0)
            break;
    }
}

int CCString::strcmp(const PCSTR s)
{
    return ::strcmp(m_str, s);
}

int CCString::stricmp(const PCSTR s)
{
    return ::stricmp(m_str, s);
}

void CCString::toupper()
{
    MakeUpper();
}

void CCString::tolower()
{
    MakeLower();
}

const BOOL CCString::operator!=(char *s)
{
    return Compare(s)!=0;
}


