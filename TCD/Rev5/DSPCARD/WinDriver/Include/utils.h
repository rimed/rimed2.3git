#ifndef _WD_UTILS_H_
#define _WD_UTILS_H_

#if !defined(WIN16)
    #include "windrvr.h"
#endif

#if defined(__cplusplus)
extern "C" {
#endif

typedef enum 
{
    OS_NOT_SET = 0,
    OS_CAN_NOT_DETECT,
    OS_WIN_95,
    OS_WIN_98,
    OS_WIN_NT_4,
    OS_WIN_NT_5,
    OS_LINUX,
    OS_SOLARIS,
    OS_VXWORKS,
    OS_OS2,
} OS_TYPE;

OS_TYPE get_os_type();

#if !defined MAX_PATH
    #define MAX_PATH 4096
#endif

#if !defined(WIN16)

void replace_slashes(char *sPath, BOOL dos_to_unix);

typedef void (DLLCALLCONV *HANDLER_FUNC)(void *pData);

#if !(defined(WIN32) || defined(WIN16)) || defined(WINCE) || defined(_MT) 
DWORD DLLCALLCONV ThreadStart(HANDLE *phThread, HANDLER_FUNC pFunc, void *pData);
void DLLCALLCONV ThreadStop(HANDLE hThread);
#endif

DWORD DLLCALLCONV OsEventCreate(HANDLE *phOsEvent);
void DLLCALLCONV OsEventClose(HANDLE hOsEvent);
DWORD DLLCALLCONV OsEventWait(HANDLE hOsEvent, DWORD dwSecTimeout);
DWORD DLLCALLCONV OsEventSignal(HANDLE hOsEvent);
void DLLCALLCONV PrintDbgMessage(DWORD dwLevel, DWORD dwSection, 
    const char *format, ...);

#endif // !defined(WIN16)

#ifdef __cplusplus
}
#endif

#endif
