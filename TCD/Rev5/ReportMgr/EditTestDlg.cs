using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace TCD2003.ReportMgr
{
	/// <summary>
	/// Summary description for EditTestDlg.
	/// </summary>
	public partial class EditTestDlg : System.Windows.Forms.Form
	{
		private EditTestDlg()
		{
			InitializeComponent();

		}
		
        public EditTestDlg(string caption)
		{
			InitializeComponent();
			this.Text = caption;
		}
	}
}
