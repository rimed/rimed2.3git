using System;

namespace TCD2003.ReportMgr
{
	/// <summary>
	/// Summary description for ResourceManager.
	/// </summary>
	public sealed class ReportResourceManager
	{
		private string m_LanguageResourceFileName ;
		public string LanguageResourceFileName
		{
			get{return m_LanguageResourceFileName;}
		}

		static private readonly ReportResourceManager m_ReportResourceManagerInstance = new ReportResourceManager();
		
		static public ReportResourceManager theResourceManager
		{ 
			get
			{
				return m_ReportResourceManagerInstance;
			} 
		}
		
		public ReportResourceManager()
		{
			SetLangugeResourceFileName();
		}

		public void SetLangugeResourceFileName()
		{
			// Read the Language from the configuration file
			System.Configuration.AppSettingsReader reader = new System.Configuration.AppSettingsReader();
			
			string Language = (string)reader.GetValue("Language",typeof(string));
			switch ( Language )
			{
				case "English":
					m_LanguageResourceFileName =  ".ReportResource";
					break;

				case "Chinese":
					m_LanguageResourceFileName = ".ReportResourceChinese";
					break;

                case "Russian":
                    m_LanguageResourceFileName = ".ReportResourceRussian";
                    break;

                default:
					m_LanguageResourceFileName = ".ReportResource";
					break;
			}
		}
	}
}
