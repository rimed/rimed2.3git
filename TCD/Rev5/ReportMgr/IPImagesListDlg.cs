using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using TCD2003.Utils;

namespace TCD2003.ReportMgr
{
    /// <summary>
    /// Summary description for IPImagesListDlg.
    /// </summary>
    public partial class IPImagesListDlg : Form
    {
        #region Private Fields

        private List<string> m_files = new List<string>();
        private const string IP_DEFAULT_PATH = @"C:\Echo Images";
        private readonly System.Resources.ResourceManager m_strRes;

        #endregion Private Fields

        #region Constructors

        public IPImagesListDlg()
        {
            InitializeComponent();
            m_strRes = new ResourceManagerExt(GetType().Namespace + ReportResourceManager.theResourceManager.LanguageResourceFileName, GetType().Assembly);
        }

        public IPImagesListDlg(string ordFileName)
            : this()
        {
            if (File.Exists(ordFileName))
            {
                StreamReader file = new StreamReader(ordFileName);
                while (file.Peek() != -1)
                    this.m_files.Add(file.ReadLine());
                this.FillListCtrl();
            }
        }

        #endregion Constructors

        public List<string> ImageList
        {
            get
            {
                return this.m_files;
            }
        }

        #region Event Handlers

        private void IPImagesListDlg_Load(object sender, EventArgs e)
        {
            var res = ResourceManagerWrapper.GetWrapper(m_strRes); 
            this.btnSave.Text = res.GetString("Save");
            this.btnCancel.Text = res.GetString("Cancel");
            this.lblCaption.Text = res.GetString("Manage DigiLite IP Images");
        }

        private void bAdd_Click(object sender, EventArgs e)
        {
            if (Directory.Exists(IP_DEFAULT_PATH))
            {
                this.openFileDialog1.InitialDirectory = IP_DEFAULT_PATH;
                if (this.openFileDialog1.ShowDialog(this) == DialogResult.OK)
                {
                    int number = 12 - m_files.Count; //TODO: "12" can be setting
                    number = (number > this.openFileDialog1.FileNames.Length ? this.openFileDialog1.FileNames.Length : number);
                    for (int i = 0; i < number; i++)
                    {
                        if (!this.m_files.Contains(this.openFileDialog1.FileNames[i]))
                        {
                            this.m_files.Add(this.openFileDialog1.FileNames[i]);
                            this.FillListCtrl();
                            this.btnSave.Enabled = true;
                        }
                        else
                            MessageBox.Show(m_strRes.GetString("Selected IP image has already been added"));
                    }
                }
                this.EnableButtons();
            }
            else
                MessageBox.Show(m_strRes.GetString("You didn't purchase the DigiLite IP software yet."));
        }

        protected void FillListCtrl()
        {
            if (this.listBoxImages.Items.Count != 0)
                this.listBoxImages.Items.Clear();

            for (int i = 0; i < this.m_files.Count; i++)
            {
                this.listBoxImages.Items.Add(String.Format("{0}. {1}", i + 1, this.m_files[i]));
            }
        }

        private void EnableButtons()
        {
            this.btnEdit.Enabled = false;
            this.btnRemove.Enabled = false;
            this.btnMoveUp.Enabled = false;
            this.btnMoveDown.Enabled = false;
        }

        private void bEdit_Click(object sender, System.EventArgs e)
        {
            if (this.openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                string item = this.listBoxImages.SelectedItem.ToString();
                item = item.Substring(item.IndexOf(' ') + 1);
                int i = this.m_files.IndexOf(item);
                if (!this.m_files.Contains(this.openFileDialog1.FileName))
                {
                    this.m_files[i] = this.openFileDialog1.FileName;
                    this.FillListCtrl();
                }
                else
                    MessageBox.Show(m_strRes.GetString("Selected IP image has already been added"));
            }
            this.EnableButtons();
        }

        private void bRemove_Click(object sender, System.EventArgs e)
        {
            string item = this.listBoxImages.SelectedItem.ToString();
            item = item.Substring(item.IndexOf(' ') + 1);
            this.m_files.Remove(item);
            this.FillListCtrl();
            this.EnableButtons();
        }

        private void listBoxImages_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (this.listBoxImages.SelectedItem != null)
            {
                this.btnEdit.Enabled = true;
                this.btnRemove.Enabled = true;
                if (this.listBoxImages.SelectedIndex > 0)
                    this.btnMoveUp.Enabled = true;
                if (this.listBoxImages.SelectedIndex < this.listBoxImages.Items.Count - 1)
                    this.btnMoveDown.Enabled = true;
            }
        }

        private void listBoxImages_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (this.listBoxImages.SelectedIndex > 0)
                this.btnMoveUp.Enabled = true;
            else
                this.btnMoveUp.Enabled = false;
            if (this.listBoxImages.SelectedIndex < this.listBoxImages.Items.Count - 1)
                this.btnMoveDown.Enabled = true;
            else
                this.btnMoveDown.Enabled = false;
        }

        private void bMoveUp_Click(object sender, System.EventArgs e)
        {
            int i = this.listBoxImages.SelectedIndex;
            string item = this.m_files[i - 1];
            this.m_files[i - 1] = this.m_files[i];
            this.m_files[i] = item;
            this.FillListCtrl();
            this.listBoxImages.SelectedIndex = i - 1;
        }

        private void bMoveDown_Click(object sender, System.EventArgs e)
        {
            int i = this.listBoxImages.SelectedIndex;
            string item = this.m_files[i + 1];
            this.m_files[i + 1] = this.m_files[i];
            this.m_files[i] = item;
            this.FillListCtrl();
            this.listBoxImages.SelectedIndex = i + 1;
        }

        #endregion Event Handlers
    }
}
