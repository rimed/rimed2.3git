using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

using TCD2003.DB;
using TCD2003.Utils;

using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

namespace TCD2003.ReportMgr
{
    /// <summary>
    /// Summary description for ReportViewer.
    /// </summary>
    /// 
    public partial class ReportViewer : Form
    {
        #region Private Fields

        private ReportClass m_reportClass;//= new ReportClass();
        private Guid m_examIndex = Guid.Empty;
        private string m_dicomDirName = string.Empty;
        private bool m_isSaved = false;
        private readonly IPImagesListDlg m_ipDlg;
        private readonly object m_syncWriterLock;
        private readonly System.Resources.ResourceManager m_strRes;
        private bool m_lockPageChanging = false;
        private int m_currentPageNum = 1;
        
        private const string DICOM_FILES_PATH = Constants.DataPath + "DICOM_Export_Files";

        // Performs an operation on a specified file.
        private const int SW_SHOW = 5;
        private const uint SEE_MASK_INVOKEIDLIST = 12;

        public const int WM_NCLBUTTONDOWN = 0x00A1;
        public const int WM_NCLBUTTONDBLCLK = 0x00A3;
        public const int WM_NCRBUTTONDOWN = 0x00A4;
        public const int HTCAPTION = 0x0002;

        #endregion Private Fields

        /// <summary>
        /// Constructor
        /// </summary>
        public ReportViewer(Guid examIndex)
        {
            this.InitializeComponent();

            m_strRes = new ResourceManagerExt(GetType().Namespace + ReportResourceManager.theResourceManager.LanguageResourceFileName, GetType().Assembly);

            this.DICOMExport.Enabled = RimedDal.Instance.s_dsAuthorizationMgr.tb_AuthorizationMgr[0].Dicom;

            this.m_syncWriterLock = new object();
            this.m_examIndex = examIndex;

            if (Directory.Exists(Path.Combine(Constants.DataPath + "IpImages", m_examIndex.ToString())))
            {
                this.m_isSaved = true;
                this.btnSave.Enabled = false;
            }

            string ordFileName = Path.Combine(Path.Combine(Constants.IpImagesPath, m_examIndex.ToString()), "files.ord");
            if (File.Exists(ordFileName))
                this.m_ipDlg = new IPImagesListDlg(ordFileName);
            else
                this.m_ipDlg = new IPImagesListDlg();
        }

        public ReportClass PatientReportClass
        {
            set
            {
                this.crystalReportViewer1.ReportSource = value;
                this.crystalReportViewer1.RefreshReport();
            }
        }
        public string CurrentPatientID { get; set; }

        public void RefreshPatientRep()
        {
            this.crystalReportViewer1.RefreshReport();
            if (this.m_currentPageNum != 1)
                this.crystalReportViewer1.ShowNthPage(this.m_currentPageNum);
        }

        public event AddText2RepDelegate AddText2RepEvent;
        public event AddIpImages2RepDelegate AddIpImages2RepEvent;
        public event SaveIpImagesDelegate SaveIpImagesEvent;

        private void crystalReportViewer1_DrillDownSubreport(object source, CrystalDecisions.Windows.Forms.DrillSubreportEventArgs e)
        {
            e.Handled = true;
            if (e.NewSubreportName == "Indication")
            {
                this.m_lockPageChanging = true;
                EditTestDlg dlg = new EditTestDlg("Indication");
                if (dlg.ShowDialog() == DialogResult.OK)
                    this.FireAddText2RepEvent("Indication", dlg.richTextBox1.Text);
            }
            else if (e.NewSubreportName == "Interpretation")
            {
                this.m_lockPageChanging = true;
                EditTestDlg dlg = new EditTestDlg("Interpretation");
                if (dlg.ShowDialog() == DialogResult.OK)
                    this.FireAddText2RepEvent("Interpretation", dlg.richTextBox1.Text);
            }
            else if (!m_isSaved && (e.NewSubreportName == "IpImages1" || e.NewSubreportName == "IpImages2" || e.NewSubreportName == "IpImages4"))
            {
                this.m_lockPageChanging = true;
                if (this.m_ipDlg.ShowDialog() == DialogResult.OK)
                {
                    this.FireAddIpImages2RepEvent(m_ipDlg.ImageList);
                }
                this.m_lockPageChanging = false;
            }
        }
        private void FireAddText2RepEvent(string field, string text)
        {
            if (this.AddText2RepEvent != null)
                this.AddText2RepEvent(field, text);
        }

        private void FireAddIpImages2RepEvent(List<string> files)
        {
            if (this.AddIpImages2RepEvent != null)
                this.AddIpImages2RepEvent(files);
        }

        private void crystalReportViewer1_Navigate(object source, CrystalDecisions.Windows.Forms.NavigateEventArgs e)
        {
            if (this.m_lockPageChanging)
                this.m_lockPageChanging = false;
            else
                this.m_currentPageNum = e.NewPageNumber;
        }

        private string ExportASPDF()
        {
            DiskFileDestinationOptions diskOpts = new DiskFileDestinationOptions();

            //set the export format
            this.m_reportClass = (ReportClass)this.crystalReportViewer1.ReportSource;
            this.m_reportClass.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
            this.m_reportClass.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;

            //set the disk file options
            diskOpts.DiskFileName = string.Format(@"{0}\Report_{1}.pdf", m_dicomDirName, DateTime.Now.ToShortDateString().Replace('/', '_'));
            this.m_reportClass.ExportOptions.DestinationOptions = diskOpts;
            this.m_reportClass.Export();
            return diskOpts.DiskFileName;
        }

        [DllImport("shell32.dll")]
        static extern bool ShellExecuteEx(ref SHELLEXECUTEINFO lpExecInfo);
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        private struct SHELLEXECUTEINFO
        {
            public int cbSize;
            public uint fMask;
            public IntPtr hwnd;
            public String lpVerb;
            public String lpFile;
            public String lpParameters;
            public String lpDirectory;
            public int nShow;
            public int hInstApp;
            public int lpIDList;
            public String lpClass;
            public int hkeyClass;
            public uint dwHotKey;
            public int hIcon;
            public int hProcess;
        }

        //public static void ShowFileProperties(string Filename)
        //{
        //    SHELLEXECUTEINFO info = new SHELLEXECUTEINFO();
        //    info.cbSize = System.Runtime.InteropServices.Marshal.SizeOf(info);
        //    //info.lpVerb = "properties";
        //    info.lpFile = Filename;
        //    info.nShow = SW_SHOW;
        //    info.fMask = SEE_MASK_INVOKEIDLIST;
        //    ShellExecuteEx(ref info);
        //}

        private ManualResetEvent m_manualEvent;

        private void DICOMExport_Click(object sender, System.EventArgs e)
        {
            if (!Directory.Exists(DICOM_FILES_PATH))
                Directory.CreateDirectory(DICOM_FILES_PATH);
            this.m_dicomDirName = Path.Combine(DICOM_FILES_PATH, CurrentPatientID.ToString());
            if (!Directory.Exists(this.m_dicomDirName))
                Directory.CreateDirectory(this.m_dicomDirName);

            m_manualEvent = new ManualResetEvent(false);

            lock (this.m_syncWriterLock)
            {
                string filename = this.ExportASPDF();

                string simplename = Path.GetFileName(filename);
                simplename = simplename.Substring(0, simplename.Length - 4);

                Process convertProcess = new Process();
                convertProcess.StartInfo.FileName = Constants.GhostScriptPath + "gswin32c.exe";
                convertProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                Cursor.Current = Cursors.WaitCursor;
                convertProcess.StartInfo.Arguments =
                    string.Format(
                        @"-dBATCH -dNOPAUSE -dNOPLATFONTS -sDEVICE=jpeg -dJPEGQ=100 -sFONTPATH=C:\Windows\Fonts -r300x300 -sOutputFile={0}\{1}_page-%d.jpg {2}",
                        m_dicomDirName,
                        simplename,
                        filename);
                convertProcess.EnableRaisingEvents = true;
                convertProcess.Exited += new EventHandler(convertProcess_Exited);
                convertProcess.Start();

                m_manualEvent.WaitOne();

                //Convert bmp files to DICOM format
                string namepart = simplename + "_page-*";
                string[] bmpfiles = Directory.GetFiles(m_dicomDirName, namepart);

                dsPatient.tb_PatientRow patient = null;
                for (int i = 0; i < RimedDal.Instance.s_dsPatient.tb_Patient.Rows.Count; i++)
                {
                    if (RimedDal.Instance.s_dsPatient.tb_Patient.Rows[i].ItemArray[0].ToString() == CurrentPatientID)
                    {
                        patient = (dsPatient.tb_PatientRow)RimedDal.Instance.s_dsPatient.tb_Patient.Rows[i];
                        break;
                    }
                }
                dsExamination.tb_ExaminationRow examination = null;
                for (int i = 0; i < RimedDal.Instance.s_dsExamination.tb_Examination.Rows.Count; i++)
                {
                    if (RimedDal.Instance.s_dsExamination.tb_Examination.Rows[i].ItemArray[0].ToString() == m_examIndex.ToString())
                    {
                        examination = (dsExamination.tb_ExaminationRow)RimedDal.Instance.s_dsExamination.tb_Examination.Rows[i];
                        break;
                    }
                }

                DicomExporter exporter = new DicomExporter();
                string filesToSend = "";
                for (int i = 0; i < bmpfiles.Length; i++)
                {
                    filesToSend = string.Format("{0}{1};",
                        filesToSend,
                        exporter.convertAndSave(bmpfiles[i], i + 1,
                        patient.ID,
                        string.Format("{0} {1}", patient.First_Name, patient.Last_Name),
                        patient.Sex,
                        patient.Birth_date,
                        patient.RefferedBy,
                        examination.Date,
                        examination.Notes,
                        patient.AccessionNumber));
                }

                //RIMD-362: Make DICOM sending data to server function
                bool wasSent = false;

                while (!wasSent)
                {
                    try
                    {
                        string localAE_Title = String.Empty;
                        string AE_Title = String.Empty;
                        string IP_Address = String.Empty;
                        short Port_Number = 0;
                        try
                        {
                            localAE_Title = RimedDal.Instance.s_DS.tb_HospitalDetails[0].localAE_Title;
                            AE_Title = RimedDal.Instance.s_DS.tb_HospitalDetails[0].AE_Title;
                            IP_Address = RimedDal.Instance.s_DS.tb_HospitalDetails[0].IP_Address;
                            Port_Number = RimedDal.Instance.s_DS.tb_HospitalDetails[0].Port_Number;
                        }
                        catch (StrongTypingException ex)
                        {
                            ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
                            MessageBox.Show(m_strRes.GetString("Set values for DICOM export: host, port, AE Title, local AE title"));
                            return;
                        }

                        string failedList = exporter.Send(filesToSend, localAE_Title, AE_Title, IP_Address, Port_Number);
                        if (failedList == null || failedList == "")
                            wasSent = true;
                        else
                            filesToSend = failedList;
                    }
                    catch (COMException ex)
                    {
                        ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
                        MessageBox.Show(m_strRes.GetString("DICOM transfer failed: ") + ex.Message, "C-ECHO");
                    }
                    catch (Exception ex)
                    {
                        ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
                        throw;
                    }
                    if (!wasSent)
                    {
                        if (MessageBox.Show(m_strRes.GetString("Some files were not sent. Do you want to try again?"),
                            "DICOM sending", MessageBoxButtons.RetryCancel) == DialogResult.Cancel)
                            wasSent = true;
                    }
                    else
                        MessageBox.Show(m_strRes.GetString("DICOM Export was successfully completed!"), "INFO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                Cursor.Current = Cursors.Default;
            }
        }

        private void btnSave_Click(object sender, System.EventArgs e)
        {
            if (this.SaveIpImagesEvent != null)
            {
                this.SaveIpImagesEvent();
                this.m_isSaved = true;
                this.btnSave.Enabled = false;
            }
        }

        private void convertProcess_Exited(object sender, EventArgs e)
        {
            this.m_manualEvent.Set();
        }

        protected override void WndProc(ref Message m)
        {
            // RIMD-496: Digi-Lite and Summary Screen windows can be moved in Windows Aero user scheme
            if ((m.Msg == WM_NCLBUTTONDOWN || m.Msg == WM_NCLBUTTONDBLCLK || m.Msg == WM_NCRBUTTONDOWN)
                && m.WParam == (IntPtr)HTCAPTION)
                return;

            try
            {
                base.WndProc(ref m);
            }
            catch (Exception ex)
            {
                ExceptionPublisherLog4Net.ExceptionErrorLog(ex, string.Format("{0}.WndProc({1})", Name, m));
            }
        }
    }

    public delegate void AddText2RepDelegate(string field, string text);
    public delegate void AddIpImages2RepDelegate(List<string> files);
    public delegate void SaveIpImagesDelegate();
}
