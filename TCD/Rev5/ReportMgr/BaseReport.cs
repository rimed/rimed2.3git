using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Data;
using System.IO;
using System.Windows.Forms;
using TCD2003.DB;
using TCD2003.Utils;

namespace TCD2003.ReportMgr
{
    /// <summary>
    /// Summary description for BaseReport.
    /// </summary>
    public class BaseReport
    {
        protected DataSet Ds = null;
        protected Guid ExamIndex = Guid.Empty;
        protected string StudyName = "Intracranial Unilateral";
        private string m_currentPatientId;
        protected dsExamination.tb_ExaminationRow ExamRow = null;

        protected System.Resources.ResourceManager StrRes;

        protected ReportViewer ReportViewer = null;
        protected CrystalDecisions.CrystalReports.Engine.ReportClass Report;
        protected static CrystalDecisions.CrystalReports.Engine.ReportClass ReportClass1;

        /// <summary>
        /// Constructor
        /// </summary>
        public BaseReport()
        {
            StrRes = new ResourceManagerExt(GetType().Namespace + ReportResourceManager.theResourceManager.LanguageResourceFileName, GetType().Assembly);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public BaseReport(Guid examIndex)
            : this()
        {
            this.ExamIndex = examIndex;

            ExamRow = RimedDal.Instance.GetExaminationByIndex(examIndex);

            this.StudyName = RimedDal.Instance.GetStudyName(ExamRow.Study_Index);
        }

        /// <summary>Create report layout and catch it till application run It fixes RIMD-471</summary>
        public static void Initialize()
        {
            // Initializing Crystal Reports
            System.Threading.Thread.Sleep(1000);
            ReportClass1 = new crPatientRep();
            ReportClass1.Load();
            ReportClass1.Close();
        }

        public virtual void Display(bool preview)
        {
            ReportViewer = new ReportViewer(ExamIndex);
            ReportViewer.AddText2RepEvent += new AddText2RepDelegate(RepViewerInstance_AddText2RepEvent);
            ReportViewer.AddIpImages2RepEvent += new AddIpImages2RepDelegate(ReportViewer_AddIpImages2RepEvent);
            ReportViewer.SaveIpImagesEvent += new SaveIpImagesDelegate(ReportViewer_SaveIpImagesEvent);

            if (preview)
            {
                ReportViewer.CurrentPatientID = m_currentPatientId;
                ReportViewer.PatientReportClass = Report;
                ReportViewer.ShowDialog();
                ReportViewer.AddText2RepEvent -= new AddText2RepDelegate(RepViewerInstance_AddText2RepEvent);
                ReportViewer.AddIpImages2RepEvent -= new AddIpImages2RepDelegate(ReportViewer_AddIpImages2RepEvent);
                ReportViewer.SaveIpImagesEvent -= new SaveIpImagesDelegate(ReportViewer_SaveIpImagesEvent);
            }
            else
            {
                try
                {
                    Report.PrintToPrinter(1, true, 0, 0);
                }
                // Nadiya, RIMD-442: in new env exception System.Drawing.Printing.InvalidPrinterException is thrown.
                // catch(CrystalDecisions.CrystalReports.Engine.EngineException ex)
                catch (Exception ex)
                {
                    ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
                    MessageBox.Show("Printer was not found!", "Information", System.Windows.Forms.MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
        public virtual void UpdateGeneralData(DataView dvHospital, DataView dvPatient)
        {
            Ds.Tables["tb_GenralInfo"].Clear();
            Ds.Tables["tb_Patient"].Clear();
            Ds.Tables["tb_HospitalDetails"].Clear();

            // copy hospital details.
            System.Data.DataRow hosNewRow = Ds.Tables["tb_HospitalDetails"].NewRow();
            hosNewRow.ItemArray = dvHospital[0].Row.ItemArray;
            Ds.Tables["tb_HospitalDetails"].Rows.Add(hosNewRow);

            //Assaf fix (22/11/06) - Adding the data for the SummaryReport (Printing from summaryScreen)
            // add one row to tb_GenralInfo.
            System.Data.DataRow genInfoNewRow = Ds.Tables["tb_GenralInfo"].NewRow();

            genInfoNewRow["Indication"] = this.ExamRow["Indication"];
            genInfoNewRow["Interpretation"] = (string)this.ExamRow["Interpretation"];

            genInfoNewRow["ReportTitle"] = "TCD Examination: " + StudyName;

            genInfoNewRow["ExamDate"] = ((DateTime)this.ExamRow["Date"]).ToShortDateString();
            // generate time string
            string time =
                ((((int)this.ExamRow["TimeHours"]) < 10) ? "0" : "") + ((int)this.ExamRow["TimeHours"]).ToString()
                + ":" +
                ((((int)this.ExamRow["TimeMinutes"]) < 10) ? "0" : "") + ((int)this.ExamRow["TimeMinutes"]).ToString();

            genInfoNewRow["ExamTime"] = time;

            genInfoNewRow["Notes"] = RimedDal.Instance.s_dsExamination.tb_Examination[0].Notes;
            Ds.Tables["tb_GenralInfo"].Rows.Add(genInfoNewRow);

            // copy the current patient.
            System.Data.DataRow patientNewRow = Ds.Tables["tb_Patient"].NewRow();

            patientNewRow["Patient_Index"] = dvPatient[0]["Patient_Index"];
            patientNewRow["First_Name"] = dvPatient[0]["First_Name"];
            patientNewRow["Middle_Name"] = dvPatient[0]["Middle_Name"];
            patientNewRow["Last_Name"] = dvPatient[0]["Last_Name"];
            patientNewRow["Deleted"] = dvPatient[0]["Deleted"];
            patientNewRow["ID"] = dvPatient[0]["ID"];
            patientNewRow["Sex"] = dvPatient[0]["Sex"];
            patientNewRow["Birth_date"] = dvPatient[0]["Birth_date"];
            patientNewRow["Age"] = dvPatient[0]["Age"];
            patientNewRow["Home_Phone"] = dvPatient[0]["Home_Phone"];
            patientNewRow["Cellular_Phone"] = dvPatient[0]["Cellular_Phone"];
            patientNewRow["Work_Phone"] = dvPatient[0]["Work_Phone"];
            patientNewRow["Address"] = dvPatient[0]["Address"];
            patientNewRow["Reason_For_Examination"] = dvPatient[0]["Reason_For_Examination"];
            patientNewRow["RefferedBy"] = dvPatient[0]["RefferedBy"];
            patientNewRow["Email"] = dvPatient[0]["Email"];
            patientNewRow["Alive"] = dvPatient[0]["Alive"];
            patientNewRow["Smoking"] = (((bool)dvPatient[0]["Smoking"])) ? "Yes" : "No";
            patientNewRow["Hypertension"] = (((bool)dvPatient[0]["Hypertension"])) ? "Yes" : "No";
            patientNewRow["Symptomatic_Carotid_Stenosis"] = (((bool)dvPatient[0]["Symptomatic_Carotid_Stenosis"])) ? "Yes" : "No";
            patientNewRow["Asymptomatic_Carotid_Stenosis"] = (((bool)dvPatient[0]["Asymptomatic_Carotid_Stenosis"])) ? "Yes" : "No";
            patientNewRow["TIA"] = (((bool)dvPatient[0]["TIA"])) ? "Yes" : "No";
            patientNewRow["CVA"] = (((bool)dvPatient[0]["CVA"])) ? "Yes" : "No";
            patientNewRow["Atrial_Fibrilation"] = (((bool)dvPatient[0]["Atrial_Fibrilation"])) ? "Yes" : "No";
            patientNewRow["Prosthetic_Heart_Valves"] = (((bool)dvPatient[0]["Prosthetic_Heart_Valves"])) ? "Yes" : "No";
            patientNewRow["Coronary_By_Pass"] = (((bool)dvPatient[0]["Coronary_By_Pass"])) ? "Yes" : "No";
            patientNewRow["Arrithymia"] = (((bool)dvPatient[0]["Arrithymia"])) ? "Yes" : "No";
            patientNewRow["Title"] = dvPatient[0]["Title"];

            Ds.Tables["tb_Patient"].Rows.Add(patientNewRow);
            m_currentPatientId = dvPatient[0]["Patient_Index"].ToString();
        }

        protected virtual void RepViewerInstance_AddText2RepEvent(string field, string text)
        {
            // do nothing in base.
        }

        protected virtual void ReportViewer_AddIpImages2RepEvent(List<string> files)
        {
            // do nothing in base
        }

        protected virtual void ReportViewer_SaveIpImagesEvent()
        {
            // do nothing in base
        }

        protected byte[] GetReportImage(string fileName, int width, int height)
        {
            var img = System.Drawing.Image.FromFile(fileName);
            return GetReportImage(img, width, height);
        }

        protected byte[] GetReportImage(MemoryStream stream, int width, int height)
        {
            var img = System.Drawing.Image.FromStream(stream);
            return GetReportImage(img, width, height);
        }

        private static byte[] GetReportImage(System.Drawing.Image img, int width, int height)
        {
            MemoryStream memStream = new System.IO.MemoryStream();
            if (img.Width > width && img.Height > height)
            {
                var img2 = img.GetThumbnailImage(width, height, null, IntPtr.Zero);
                img2.Save(memStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                img2.Dispose();
            }
            else
                img.Save(memStream, System.Drawing.Imaging.ImageFormat.Jpeg);
            img.Dispose();

            memStream.Position = 0;
            byte[] image = memStream.ToArray();
            memStream.Dispose();

            return image;
        }
    }

    public class LayoutReport : BaseReport
    {
        public enum TLayoutRep { e1Chart = 1, e2Chart = 2, e4Chart = 4, e6Chart = 6 };
        private TLayoutRep m_LayoutRep = TLayoutRep.e1Chart;
        private int m_ColNum = 1;
        public TLayoutRep TemplateLayoutRep
        {
            set
            {
                if (!IsLayoutRepEmpty())
                    PrintLayoutRep(true);
                m_LayoutRep = value;
            }
        }
        static private LayoutReport m_Instance = new LayoutReport();
        private LayoutReport()
        {
            Ds = new dsLayoutRep();
        }
        static public LayoutReport Instance
        {
            get
            {
                return m_Instance;
            }
        }
        public void AddChart(MemoryStream ms, string notes)
        {
            int chartNum = Add2LayoutRep(ms, notes, m_ColNum);
            if (chartNum >= ((int)m_LayoutRep))
            {
                PrintLayoutRep(false);
                Ds.Tables["tb_Charts"].Rows.Clear();
            }
        }
        public void PrintLayoutRep(bool preview)
        {
            Report.SetDataSource(Ds);
            Display(preview);
        }

        public bool IsLayoutRepEmpty()
        {
            return (Ds.Tables["tb_Charts"].Rows.Count == 0);
        }
        public int Add2LayoutRep(MemoryStream ms, string notes, int colNum)
        {
            //Assad (21/12/06) - I have added the next 2 rows for initating the report when it's needed
            if (Ds.Tables["tb_Charts"].Rows.Count == 0)
                ReportMgr.LayoutReport.Instance.TemplateLayoutRep = (ReportMgr.LayoutReport.TLayoutRep)RimedDal.Instance.GetLayoutRepTemplate();
            switch (m_LayoutRep)
            {
                case TLayoutRep.e1Chart:
                    Report = new TCD2003.ReportMgr.crLayoutRep1();
                    m_ColNum = 1;
                    break;
                case TLayoutRep.e2Chart:
                    Report = new TCD2003.ReportMgr.crLayoutRep2();
                    m_ColNum = 1;
                    break;
                case TLayoutRep.e4Chart:
                    Report = new TCD2003.ReportMgr.crLayoutRep4();
                    m_ColNum = 2;
                    break;
                case TLayoutRep.e6Chart:
                    Report = new TCD2003.ReportMgr.crLayoutRep6();
                    m_ColNum = 2;
                    break;
            }

            byte[] imgData = new byte[ms.Length];
            imgData = ms.ToArray();

            if (colNum == 1)
            {
                DataRow newRow = Ds.Tables["tb_Charts"].NewRow();
                newRow["GraphImg"] = imgData;
                newRow["Notes"] = notes;
                newRow["GraphImg1"] = imgData;
                newRow["Notes1"] = notes;
                Ds.Tables["tb_Charts"].Rows.Add(newRow);

                return Ds.Tables["tb_Charts"].Rows.Count;
            }
            else
            {
                if (Ds.Tables["tb_Charts"].Rows.Count == 0)
                {
                    DataRow newRow = Ds.Tables["tb_Charts"].NewRow();
                    newRow["GraphImg"] = imgData;
                    newRow["Notes"] = notes;
                    newRow["GraphImg1"] = imgData;
                    newRow["Notes1"] = notes;
                    Ds.Tables["tb_Charts"].Rows.Add(newRow);
                    return 1;
                }
                else
                {
                    // retrieve the last row.
                    dsLayoutRep.tb_ChartsRow row = (dsLayoutRep.tb_ChartsRow)Ds.Tables["tb_Charts"].Rows[Ds.Tables["tb_Charts"].Rows.Count - 1];
                    if (row.GraphImg == row.GraphImg1 && row.Notes == row.Notes1)
                    {
                        row.GraphImg1 = imgData;
                        row.Notes1 = notes;
                        return (Ds.Tables["tb_Charts"].Rows.Count * colNum);
                    }
                    else
                    {
                        DataRow newRow = Ds.Tables["tb_Charts"].NewRow();
                        newRow["GraphImg"] = imgData;
                        newRow["Notes"] = notes;
                        newRow["GraphImg1"] = imgData;
                        newRow["Notes1"] = notes;
                        Ds.Tables["tb_Charts"].Rows.Add(newRow);

                        return ((Ds.Tables["tb_Charts"].Rows.Count - 1) * colNum + 1);
                    }
                }
            }
        }
    }

    public class PrintScreenReport : BaseReport
    {
        public PrintScreenReport(Guid examIndex, MemoryStream ms, string notes,
            DataView dvHospital, DataView dvPatient, DataView dvExam)
            : base(examIndex)
        {
            Ds = new dsPrintScreenRep();

            Report = new crPrintScreenRep();
            try
            {
                UpdateGeneralData(dvHospital, dvPatient);
                UpdatePrintScreenRepDS(examIndex, ms, notes);
                Report.SetDataSource(Ds);
            }
            catch (System.IndexOutOfRangeException ex)
            {
                ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
            }
            catch (Exception ex)
            {
                ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
                throw;
            }
        }
        public void UpdatePrintScreenRepDS(System.Guid examIndex, System.IO.MemoryStream ms, string notes)
        {
            Ds.Tables["tb_PrintScreen"].Rows.Clear();

            DataRow newRow = Ds.Tables["tb_PrintScreen"].NewRow();
            newRow["Img"] = GetReportImage(ms, 711, 519);
            newRow["SubExamNotes"] = notes;
            Ds.Tables["tb_PrintScreen"].Rows.Add(newRow);
        }
    }

    public class GenerateExamReport : BaseReport
    {
        public enum TRepType { eSummary = 0, eTable = 1, eBoth = 2, eNone = 3 };

        protected dsGateExamination m_dsGateExam = new dsGateExamination();
        protected dsExamination m_dsExam = new dsExamination();
        protected List<string> m_leftCol = null;
        protected List<string> m_middleCol = null;
        protected List<string> m_rightCol = null;
        protected TRepType m_reportType = TRepType.eSummary;

        public GenerateExamReport(Guid examIndex, DataView dvHospital, DataView dvPatient,
            dsExamination dsExam, dsGateExamination dsGateExam)
            : base(examIndex)
        {
            m_dsExam = (dsExamination)dsExam.Copy();
            m_dsGateExam = (dsGateExamination)dsGateExam.Copy();
        }
        protected void FillExamArr()
        {
            string gatesImagePath = Constants.GatesImagePath;

            if (m_leftCol == null)
                m_leftCol = new List<string>();
            if (m_middleCol == null)
                m_middleCol = new List<string>();
            if (m_rightCol == null)
                m_rightCol = new List<string>();

            m_leftCol.Clear();
            m_middleCol.Clear();
            m_rightCol.Clear();

            switch (this.StudyName)
            {
                case "Extracranial":
                    // insert the examination schema
                    m_middleCol.Add(Path.Combine(Constants.DataPath, "Extracranial1.BMP")); 
                    m_middleCol.Add(Path.Combine(Constants.DataPath, "Extracranial2.BMP")); 
                    m_middleCol.Add(Path.Combine(Constants.DataPath, "Extracranial3.BMP")); 
                    break;

                case "Peripheral":
                    // insert the examination schema
                    m_middleCol.Add(Path.Combine(Constants.DataPath, "Periferal1.BMP"));    
                    m_middleCol.Add(Path.Combine(Constants.DataPath, "Periferal2.BMP"));    
                    m_middleCol.Add(Path.Combine(Constants.DataPath, "Periferal3.BMP"));    
                    break;

                default:
                    // insert the examination schema
                    m_middleCol.Add(Path.Combine(Constants.DataPath, "NO_ANIM1.BMP"));      
                    m_middleCol.Add(Path.Combine(Constants.DataPath, "NO_ANIM2.BMP"));      
                    m_middleCol.Add(Path.Combine(Constants.DataPath, "NO_ANIM3.BMP"));      
                    break;
            }

            foreach (dsGateExamination.tb_GateExaminationRow gateExamRow in m_dsGateExam.tb_GateExamination.Rows)
            {
                string imgPath;
                if (File.Exists(gatesImagePath + gateExamRow.Gate_Index + "_L.jpg"))
                {
                    imgPath = gatesImagePath + gateExamRow.Gate_Index + "_L.jpg";
                    m_leftCol.Add(imgPath);
                }
                else if (File.Exists(gatesImagePath + gateExamRow.Gate_Index + "_M.jpg"))
                {
                    imgPath = gatesImagePath + gateExamRow.Gate_Index + "_M.jpg";
                    m_middleCol.Add(imgPath);
                }
                else if (File.Exists(gatesImagePath + gateExamRow.Gate_Index + "_R.jpg"))
                {
                    imgPath = gatesImagePath + gateExamRow.Gate_Index + "_R.jpg";
                    m_rightCol.Add(imgPath);
                }
            }
        }

        protected void AddEmptyGates()
        {
            // check max array count.
            int maxGV = (m_leftCol.Count > m_middleCol.Count) ? m_leftCol.Count : m_middleCol.Count;
            maxGV = (m_rightCol.Count > maxGV) ? m_rightCol.Count : maxGV;

            string emptyGVPath = Path.Combine(Constants.DataPath, "emptyGV.BMP");   

            for (int i = m_leftCol.Count; i < maxGV; i++)
                m_leftCol.Add(emptyGVPath);

            for (int i = m_middleCol.Count; i < maxGV; i++)
                m_middleCol.Add(emptyGVPath);

            for (int i = m_rightCol.Count; i < maxGV; i++)
                m_rightCol.Add(emptyGVPath);
        }

        protected void UpdateGeneralExamInfo()
        {
            Ds.Tables["tb_GenralInfo"].Rows[0]["Indication"] = this.ExamRow["Indication"];
            Ds.Tables["tb_GenralInfo"].Rows[0]["Interpretation"] = (string)this.ExamRow["Interpretation"];
            Ds.Tables["tb_GenralInfo"].Rows[0]["ReportTitle"] = "TCD Examination: " + StudyName;
            Ds.Tables["tb_GenralInfo"].Rows[0]["ExamDate"] = ((DateTime)this.ExamRow["Date"]).ToShortDateString();
            // generate time string
            string time =
                ((((int)this.ExamRow["TimeHours"]) < 10) ? "0" : "") + ((int)this.ExamRow["TimeHours"])
                + ":" +
                ((((int)this.ExamRow["TimeMinutes"]) < 10) ? "0" : "") + ((int)this.ExamRow["TimeMinutes"]);

            Ds.Tables["tb_GenralInfo"].Rows[0]["ExamTime"] = time;
        }
    }

    public class SummaryExamReport : GenerateExamReport
    {
        public SummaryExamReport(string studyName, Guid examIndex, DataView dvHospital, DataView dvPatient, dsExamination dsExam, dsGateExamination dsGateExam)
            : base(examIndex, dvHospital, dvPatient, dsExam, dsGateExam)
        {
            this.StudyName = studyName;

            Ds = new dsSummaryRep();

            Report = new crSummaryRep();
            try
            {
                UpdateGeneralData(dvHospital, dvPatient);
                FillExamArr();
                AddEmptyGates();
                UpdateSummaryDS();

                Report.SetDataSource(Ds);
            }
            catch (System.IndexOutOfRangeException ex)
            {
                ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
            }
            catch (Exception ex)
            {
                ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
                throw;
            }
        }
        private void UpdateSummaryDS()
        {
            int width = 243;
            int height = 105;
            // update Summary dataset.
            for (int i = 0; i < m_leftCol.Count; i++)
            {
                DataRow summaryRow = Ds.Tables["tb_SummaryScreen"].NewRow();

                summaryRow["LeftImg"] = GetReportImage(m_leftCol[i], width, height);
                summaryRow["MiddleImg"] = GetReportImage(m_middleCol[i], width, height);
                summaryRow["RightImg"] = GetReportImage(m_rightCol[i], width, height);

                Ds.Tables["tb_SummaryScreen"].Rows.Add(summaryRow);
            }
        }
    }

    public abstract class FinalReport : GenerateExamReport
    {
        /// <summary>
        /// Constructor
        /// </summary>
        protected FinalReport(Guid examIndex, DataView dvHospital, DataView dvPatient, dsExamination dsExam, dsGateExamination dsGateExam)
            : base(examIndex, dvHospital, dvPatient, dsExam, dsGateExam)
        {
            // initial report's dataset.
            this.Ds = new dsPatientRep();
            this.UpdateGeneralData(dvHospital, dvPatient);
        }

        protected virtual void GenReportData()
        {
            if (updateExamDataWrapper())        //Ofer: Unhandled System.ArgumentException: Value does not fall within the expected range (ErrorCode: 0x80070057), on CrystalDecisions.CrystalReports.Engine
                Report.SetDataSource(Ds);
        }

        protected virtual void UpdateReportDS()
        {
            FillExamHistory();
            updateExamDataWrapper();            //Ofer: Unhandled System.ArgumentException: Value does not fall within the expected range (ErrorCode: 0x80070057), on CrystalDecisions.CrystalReports.Engine
        }

        private void FillExamHistory()
        {
            this.Ds.Tables["tb_ExaminationHistory"].Clear();

            foreach (dsExamination.tb_ExaminationRow row in this.m_dsExam.tb_Examination.Rows)
            {
                string time =
                    ((((int)row.TimeHours) < 10) ? "0" : "") + ((int)row.TimeHours).ToString()
                    + ":" +
                    ((((int)row.TimeMinutes) < 10) ? "0" : "") + ((int)row.TimeMinutes).ToString();

                DataRow examHistoryNewRow = this.Ds.Tables["tb_ExaminationHistory"].NewRow();
                examHistoryNewRow["Examination_Index"] = row.Examination_Index;
                examHistoryNewRow["Date"] = ((DateTime)row.Date).ToShortDateString();
                examHistoryNewRow["Indication"] = row.Indication;
                examHistoryNewRow["Interpretation"] = row.Interpretation;
                examHistoryNewRow["time"] = time;

                examHistoryNewRow["Type"] = StudyName;
                examHistoryNewRow["Notes"] = row.Notes;
                this.Ds.Tables["tb_ExaminationHistory"].Rows.Add(examHistoryNewRow);
            }
        }

        //Ofer: Unhandled System.ArgumentException: Value does not fall within the expected range (ErrorCode: 0x80070057), on CrystalDecisions.CrystalReports.Engine
        private bool updateExamDataWrapper()    
        {
            try
            {
                UpdateExamData();
            }
            catch (Exception ex)
            {
                //ExceptionPublisherLog4Net.ExceptionErrorLog(ex, "Examination update error.");
                MessageBox.Show("Examination update fail\n"+ ex.Message, "Examination update", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            return true;
        }

        protected abstract void UpdateExamData();

        protected override void RepViewerInstance_AddText2RepEvent(string field, string text)
        {
            text = "\n" + DateTime.Now.ToString() + " - " + text;
            this.ReportViewer.SuspendLayout();

            // update the main database.
            RimedDal.Instance.UpdateExamination(this.ExamRow, field, text);

            // TODO: add updating the report dataset. 
            this.GenReportData();
            this.ReportViewer.RefreshPatientRep();
            this.ReportViewer.ResumeLayout(false);
        }
    }

    public class PatientReport : FinalReport
    {
        #region Private Fields

        private int m_rowsFirstPageNum = 0;
        private int m_ipImagesPerPage = 0;

        //List of file pathes in IP Imager folder
        private List<string> m_imagerIpFiles = new List<string>();
        private string m_directoryName = string.Empty;

        //IP Images path
        private const string IP_DEFAULT_PATH = @"C:\Echo Images";

        #endregion Private Fields

        /// <summary>
        /// Constructor
        /// </summary>
        public PatientReport(Guid examIndex, DataView dvHospital, DataView dvPatient, dsExamination dsExam, dsGateExamination dsGateExam)
            : base(examIndex, dvHospital, dvPatient, dsExam, dsGateExam)
        {
            m_directoryName = Path.Combine(Constants.IpImagesPath, ExamIndex.ToString());

            Microsoft.Win32.RegistryKey myKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Rimed", true);
            if (myKey != null && myKey.GetValue("OnePageReport") != null && Convert.ToBoolean(myKey.GetValue("OnePageReport")))
                Report = new crPatientRepSimple();
            else
                Report = new crPatientRep();

            try
            {
                m_rowsFirstPageNum = genReportLayout();
                if (m_rowsFirstPageNum >= 0) 
                {
                    UpdateReportDS();
                    Report.SetDataSource(Ds);
                }
            }
            catch (Exception ex)
            {
                ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
            }
        }

        private int genReportLayout()
        {
            if (Report == null)     
                return -1;

            var rowExamHeight   = 100;
            var paperSize       = 15120 - 360 - 360;    //Ofer: - topMargin - bottomMargin
            if (Report.PrintOptions != null)            //Ofer: Object reference not set to an instance of an object. (ErrorCode: 0x80004003) at CrystalDecisions.CrystalReports.Engine.PrintOptions.get_PageContentHeight()
            {
                try
                {
                    paperSize = Report.PrintOptions.PageContentHeight;
                    paperSize -= Report.PrintOptions.PageMargins.bottomMargin;
                    paperSize -= Report.PrintOptions.PageMargins.topMargin;
                }
                catch
                {
                    paperSize = 15120 - 360 - 360;      //Ofer  
                }
            }

            var dvRepInfo = new DataView(RimedDal.Instance.s_dsPatientRepLayout.tb_PatientRepLayout);

            var dv = new DataView();

            if (((bool) dvRepInfo[0]["DateDisplay"]) == false
                && ((bool) dvRepInfo[0]["HospitalDetails"]) == false
                && ((bool) dvRepInfo[0]["HospitalLogo"]) == false)
            {
                Report.ReportDefinition.Sections["SectionHospitalInfo"].SectionFormat.EnableSuppress = true;
            }
            else
            {
                paperSize -= Report.ReportDefinition.Sections["SectionHospitalInfo"].Height;

                if (((bool)dvRepInfo[0]["DateDisplay"]) == false)
                    Report.ReportDefinition.ReportObjects["FieldDate"].ObjectFormat.EnableSuppress = true;

                // Hospital Details.
                dv.Table = DB.RimedDal.Instance.s_DS.tb_HospitalDetails;
                for (int i = 0; i < Report.ReportDefinition.ReportObjects.Count - 1; i++)
                {
                    Debug.WriteLine(Report.ReportDefinition.ReportObjects[i].Name);
                }
                if (((bool)dvRepInfo[0]["HospitalDetails"]) == false)
                {
                    // Change the Visibility of the Hospital Details to FALSE.
                    Report.ReportDefinition.ReportObjects[1].ObjectFormat.EnableSuppress = true;
                    Report.ReportDefinition.ReportObjects[2].ObjectFormat.EnableSuppress = true;
                    Report.ReportDefinition.ReportObjects[3].ObjectFormat.EnableSuppress = true;
                }

                // Hospital Logo
                if (((bool)dvRepInfo[0]["HospitalLogo"]) == false)
                    Report.ReportDefinition.ReportObjects[5].ObjectFormat.EnableSuppress = true;
            }

            // Title
            if (((bool)dvRepInfo[0]["TitleDisplay"]) == false)
                Report.ReportDefinition.Sections["SectionTitle"].SectionFormat.EnableSuppress = true;
            else
                paperSize -= Report.ReportDefinition.Sections["SectionTitle"].Height;

            // Patient Details.
            if (((int)dvRepInfo[0]["PatientDetails"]) == 0)
                // minimal patient details.
                Report.ReportDefinition.Sections["SectionPatientDetailFull"].SectionFormat.EnableSuppress = true;
            else
                // full patient details.
                paperSize -= Report.ReportDefinition.Sections["SectionPatientDetailFull"].Height;

            //Added by Natalie for Simple Report
            bool isSimple = false;

            Microsoft.Win32.RegistryKey myKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Rimed", true);
            if (myKey != null)
            {
                if (myKey.GetValue("OnePageReport") != null)
                {
                    if (Convert.ToBoolean(myKey.GetValue("OnePageReport")))
                        isSimple = true;
                }
                //RIMD-321: United report for DigiLite and IP
                if (myKey.GetValue("IpImagesPerPage") != null && !isSimple)
                {
                    m_ipImagesPerPage = Convert.ToInt16(myKey.GetValue("IpImagesPerPage"));
                    switch (m_ipImagesPerPage)
                    {
                        case 0:
                            Report.ReportDefinition.Sections["SectionIpImages1"].SectionFormat.EnableSuppress = true;
                            Report.ReportDefinition.Sections["SectionIpImages2"].SectionFormat.EnableSuppress = true;
                            Report.ReportDefinition.Sections["SectionIpImages4"].SectionFormat.EnableSuppress = true;
                            break;
                        case 1:
                            Report.ReportDefinition.Sections["SectionIpImages1"].SectionFormat.EnableSuppress = false;
                            Report.ReportDefinition.Sections["SectionIpImages2"].SectionFormat.EnableSuppress = true;
                            Report.ReportDefinition.Sections["SectionIpImages4"].SectionFormat.EnableSuppress = true;
                            break;
                        case 2:
                            Report.ReportDefinition.Sections["SectionIpImages1"].SectionFormat.EnableSuppress = true;
                            Report.ReportDefinition.Sections["SectionIpImages2"].SectionFormat.EnableSuppress = false;
                            Report.ReportDefinition.Sections["SectionIpImages4"].SectionFormat.EnableSuppress = true;
                            break;
                        case 4:
                            Report.ReportDefinition.Sections["SectionIpImages1"].SectionFormat.EnableSuppress = true;
                            Report.ReportDefinition.Sections["SectionIpImages2"].SectionFormat.EnableSuppress = true;
                            Report.ReportDefinition.Sections["SectionIpImages4"].SectionFormat.EnableSuppress = false;
                            break;
                    }
                }
            }
            if (((myKey == null || myKey.GetValue("IpImagesPerPage") == null)) && (!isSimple))
            {
                Report.ReportDefinition.Sections["SectionIpImages1"].SectionFormat.EnableSuppress = false;
                Report.ReportDefinition.Sections["SectionIpImages2"].SectionFormat.EnableSuppress = true;
                Report.ReportDefinition.Sections["SectionIpImages4"].SectionFormat.EnableSuppress = true;
            }

            // Examination history.
            if (((bool)dvRepInfo[0]["ExaminationHistory"]) == false)
                if (!isSimple)
                    Report.ReportDefinition.Sections["SectionExamHistory"].SectionFormat.EnableSuppress = true;
                else
                {
                    // fucking work around: i did not had the time to find out how to get the subreport sections height.
                    paperSize -= 1126;	// subReport History examination Header.
                    paperSize -= m_dsExam.tb_Examination.Rows.Count * 408; //// subReport History examination Details.
                    paperSize -= 225;	// subReport History examination Footer.
                }

            if (((bool)dvRepInfo[0]["PatientHistory"]) == false)
                if (!isSimple)
                    Report.ReportDefinition.Sections["SectionPatientHistory"].SectionFormat.EnableSuppress = true;
                else
                    //paperSize -= this.m_ReportClass.ReportDefinition.Sections["SectionPatientHistory"].Height;
                    paperSize -= 760;

            // Normal Values
            //Ofer      bool normalValues = ((bool)dvRepInfo[0]["NormalValues"]);

            // layout.
            m_reportType = (TRepType)(byte)dvRepInfo[0]["Layout"];

            if (m_reportType == TRepType.eSummary || m_reportType == TRepType.eBoth)	// summary
            {
                paperSize -= 480;
                rowExamHeight = 1680;
            }

            // Indication
            if (((bool)dvRepInfo[0]["Indication"]) == false)
                Report.ReportDefinition.Sections["SectionIndication"].SectionFormat.EnableSuppress = true;
            else
                paperSize -= Report.ReportDefinition.Sections["SectionIndication"].Height;

            // Interpretation
            if (((bool)dvRepInfo[0]["Interpretation"]) == false)
                Report.ReportDefinition.Sections["SectionInterpretation"].SectionFormat.EnableSuppress = true;

            // Signature
            if (((bool)dvRepInfo[0]["Signature"]) == false)
                Report.ReportDefinition.Sections["SectionSignature"].SectionFormat.EnableSuppress = true;


            // calculate the number of gate that be able to insert at the rest of the report.
            // this is work around the "CR feature" that start a new page in case the section can not be insert.
            int rowNum = 0;
            if (paperSize > 0)
                rowNum = paperSize / rowExamHeight - 1;

            // validation.
            if (rowNum == -1)
                rowNum = 0;

            return rowNum;
        }

        protected override void GenReportData()
        {
            base.GenReportData();
            this.UpdateIpImages();
        }

        protected override void ReportViewer_AddIpImages2RepEvent(List<string> files)
        {
            if (files == null || files.Count == 0)
                return;

            this.ReportViewer.SuspendLayout();
            this.FillIpImagesTable(files);
            this.Report.SetDataSource(Ds);
            this.FillRimedIpFiles(files);
            this.ReportViewer.RefreshPatientRep();
            this.ReportViewer.ResumeLayout(false);
        }

        private void FillRimedIpFiles(List<string> files)
        {
            this.m_imagerIpFiles.Clear();
            foreach (string fileName in files)
            {
                this.m_imagerIpFiles.Add(fileName);
            }

            this.m_directoryName = Path.Combine(Constants.IpImagesPath, this.ExamIndex.ToString());
        }

        /// <summary>
        /// Copies selected IP Images from "C:\Echo Images" folder to Rimed folder
        /// </summary>
        protected override void ReportViewer_SaveIpImagesEvent()
        {
            if (!Directory.Exists(Constants.IpImagesPath))
                Directory.CreateDirectory(Constants.IpImagesPath);

            //Find/create new folder for Rimed IP files (in the special Examination folder)
            this.m_directoryName = Path.Combine(Constants.IpImagesPath, ExamIndex.ToString());
            if (!Directory.Exists(this.m_directoryName))
                Directory.CreateDirectory(this.m_directoryName);

            //List of file pathes in Rimed folder
            List<string> rimedIpFiles = new List<string>();
            foreach (string fullname in this.m_imagerIpFiles)
                rimedIpFiles.Add(Path.Combine(this.m_directoryName, Path.GetFileName(fullname)));

            //Form the file with new pathes and write it to HD
            StreamWriter file = new StreamWriter(this.m_directoryName + @"\files.ord", false);

            foreach (string filename in rimedIpFiles)
            {
                file.Write(string.Format("{0}\r\n", filename));
            }
            file.Close();

            //Get list of the old files of this Examination
            string[] rimedOldFiles = Directory.GetFiles(this.m_directoryName);

            //Make list of new files of this Saving / Delete old files
            foreach (string oldfile in rimedOldFiles)
            {
                string ipFileName = GetFileFromIPList(this.m_imagerIpFiles, oldfile);
                if (Path.GetFileName(oldfile) != "files.ord")
                {
                    if (ipFileName == null)
                    {
                        ExceptionPublisherLog4Net.TraceLog(string.Format("ReportViewer_SaveIpImagesEvent(...): File.Delete('{0}')   <--- FILE.DELETE", oldfile), "ReportViewer");   
                        File.Delete(oldfile);
                    }
                    else
                    {
                        int i = this.m_imagerIpFiles.IndexOf(ipFileName);
                        rimedIpFiles.RemoveAt(i);
                        this.m_imagerIpFiles.Remove(ipFileName);
                    }
                }
            }

            //Copy files from IP folder to Rimed folder
            for (int i = 0; i < this.m_imagerIpFiles.Count; i++)
                File.Copy(this.m_imagerIpFiles[i], rimedIpFiles[i]);

            var res = ResourceManagerWrapper.GetWrapper(StrRes); 
            MessageBox.Show(res.GetString("Report was saved"));
        }

        /// <summary>
        /// Return full name of IP file, if Rimed file's name is the same with it
        /// </summary>
        /// <param name="list">List of image files in IP folder</param>
        /// <param name="fileName">Name of the file from Rimed folder</param>
        /// <returns></returns>
        private string GetFileFromIPList(List<string> list, string fileName)
        {
            foreach (string file in list)
            {
                if (Path.GetFileName(file) == Path.GetFileName(fileName))
                    return file;
            }
            return null;
        }

        protected override void UpdateReportDS()
        {
            base.UpdateReportDS();
            this.UpdateIpImages();
        }

        protected override void UpdateExamData()
        {
            this.Ds.Tables["tb_SummaryScreen0"].Clear();
            this.Ds.Tables["tb_SummaryScreen1"].Clear();

            // update general reprot info
            this.UpdateGeneralExamInfo();

            switch (this.m_reportType)
            {
                case TRepType.eSummary: // summary.
                    this.FillExamArr();
                    this.AddEmptyGates();
                    this.FillSummaryTable(0, (this.m_rowsFirstPageNum > this.m_leftCol.Count) ? this.m_leftCol.Count : this.m_rowsFirstPageNum, 0);
                    if ((this.m_leftCol.Count - this.m_rowsFirstPageNum) > 0)
                    {
                        FillSummaryTable(m_rowsFirstPageNum, m_leftCol.Count, 1);
                        this.Report.ReportDefinition.Sections["SectionSummary1"].SectionFormat.EnableSuppress = false;
                    }
                    else
                    {
                        //Added by Natalie for RIMD-129: Error when export to pdf from final patient report
                        //This code hide empty graphic section (SectionSummary1) in case when all images of
                        //BVs can go into the SectionSummary
                        this.Report.ReportDefinition.Sections["SectionSummary1"].SectionFormat.EnableSuppress = true;
                    }

                    this.Report.ReportDefinition.Sections["SectionTableReport"].SectionFormat.EnableSuppress = true;
                    this.Report.ReportDefinition.Sections["SectionTableReport1"].SectionFormat.EnableSuppress = true;

                    break;

                case TRepType.eTable: // table.
                    this.FillCPTable();
                    this.FillLeftRightTable();
                    this.FillBottomTable();
                    //Added by Natalie for Simple Report
                    Microsoft.Win32.RegistryKey myKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Rimed", true);
                    if (myKey == null || !Convert.ToBoolean(myKey.GetValue("OnePageReport")))
                    {
                        this.Report.ReportDefinition.Sections["SectionSummary"].SectionFormat.EnableSuppress = true;
                        this.Report.ReportDefinition.Sections["SectionSummary1"].SectionFormat.EnableSuppress = true;
                    }
                    break;

                case TRepType.eBoth:
                    // summary screen
                    this.FillExamArr();
                    this.AddEmptyGates();
                    //m_RowsFirstPageNum=8;
                    this.FillSummaryTable(0, (this.m_rowsFirstPageNum > this.m_leftCol.Count) ? this.m_leftCol.Count : this.m_rowsFirstPageNum, 0);
                    if ((this.m_leftCol.Count - this.m_rowsFirstPageNum) > 0)
                        this.FillSummaryTable(this.m_rowsFirstPageNum, this.m_leftCol.Count, 1);
                    else
                    {
                        //Added by Natalie for RIMD-129: Error when export to pdf from final patient report
                        //This code hide empty graphic section (SectionSummary1) in case when all images of
                        //BVs can go into the SectionSummary
                        this.Report.ReportDefinition.Sections["SectionSummary1"].SectionFormat.EnableSuppress = true;
                    }

                    // table.
                    this.FillCPTable();
                    this.FillLeftRightTable();
                    this.FillBottomTable();
                    break;

                case TRepType.eNone:
                    this.Report.ReportDefinition.Sections["SectionTableReport"].SectionFormat.EnableSuppress = true;
                    this.Report.ReportDefinition.Sections["SectionTableReport1"].SectionFormat.EnableSuppress = true;
                    this.Report.ReportDefinition.Sections["SectionSummary"].SectionFormat.EnableSuppress = true;
                    this.Report.ReportDefinition.Sections["SectionSummary1"].SectionFormat.EnableSuppress = true;
                    break;
            }
        }

        //Fill Ip Images in Report
        private void UpdateIpImages()
        {
            string path = string.Format(@"{0}\files.ord", this.m_directoryName);
            if (File.Exists(path))
            {
                StreamReader file = new StreamReader(path);
                List<string> files = new List<string>();
                while (file.Peek() != -1)
                {
                    files.Add(file.ReadLine());
                }
                FillIpImagesTable(files);
            }
        }

        /// <summary>
        /// Fill 3 array with the examination data.
        /// </summary>
        private void FillSummaryTable(int startFill, int endFill, int tableNum)
        {
            int width = 243;
            int height = 105;
            for (int i = startFill; i < endFill; i++)
            {
                DataRow summaryRow = Ds.Tables["tb_SummaryScreen" + tableNum.ToString()].NewRow();
                summaryRow["LeftImg"] = GetReportImage(m_leftCol[i], width, height);
                summaryRow["MiddleImg"] = GetReportImage(m_middleCol[i], width, height);
                summaryRow["RightImg"] = GetReportImage(m_rightCol[i], width, height);

                this.Ds.Tables["tb_SummaryScreen" + tableNum.ToString()].Rows.Add(summaryRow);
            }
        }

        private void FillIpImagesTable(List<string> files)
        {
            this.Ds.Tables["tb_IpImages"].Clear();

            for (int i = 0; i < files.Count; i++)
            {
                DataRow imageRow = this.Ds.Tables["tb_IpImages"].NewRow();

                if (this.m_ipImagesPerPage == 1)
                    imageRow["LeftImg"] = GetReportImage(files[i], 617, 522);
                else if (this.m_ipImagesPerPage == 2)
                    imageRow["LeftImg"] = GetReportImage(files[i], 443, 373);
                else if (this.m_ipImagesPerPage == 4)
                {
                    imageRow["LeftImg"] = GetReportImage(files[i], 311, 262);

                    if (i < files.Count - 1)
                    {
                        imageRow["RightImg"] = this.GetReportImage(files[++i], 311, 262);
                    }
                }
                else
                    continue;
                this.Ds.Tables["tb_IpImages"].Rows.Add(imageRow);
            }
        }

        public struct CP
        {
            public string Name;
            public string Units;
            public CP(string name, string units)
            {
                Name = name;
                Units = units;
            }
        }

        private List<DataRowView> m_GateExamLeft = null;
        private List<DataRowView> m_GateExamRight = null;
        private List<DataRowView> m_GateExamMiddle = null;

        private List<string> m_GateNameContainer = null;

        private List<CP> m_CpOrder = null;

        private void FillCPTable()
        {
            Ds.Tables["tb_ClinicalParam"].Rows.Clear();
            m_CpOrder = new List<CP>();
            DB.dsClinicalParamSetup dsCPSetup = DB.RimedDal.Instance.GetAllClinicalParam();
            DataView dvCP = RimedDal.Instance.GetClinicalParamByStudyIndex((Guid)this.ExamRow["Study_Index"]);

            int i = 1;
            DataRow newCPRow = Ds.Tables["tb_ClinicalParam"].NewRow();

            foreach (DataRowView dvRow in dvCP)
            {
                string filter = "Index = '" + ((Guid)dvRow["ClinicalParam_Index"]) + "'";
                DataRow[] drCP = RimedDal.Instance.s_dsClinicalParamSetup.tb_ClinicalParam.Select(filter);
                CP cp = new CP((string)drCP[0]["Name"], (string)drCP[0]["Units"]);
                m_CpOrder.Add(cp);

                newCPRow["CpName" + i.ToString()] = cp.Name;
                newCPRow["CpUnits" + i.ToString()] = cp.Units;
                i++;
            }
            if (i < 10)
            {
                for (; i <= 10; i++)
                {
                    newCPRow["CpName" + i.ToString()] = "";
                    newCPRow["CpUnits" + i.ToString()] = "";
                }
            }
            Ds.Tables["tb_ClinicalParam"].Rows.Add(newCPRow);

        }
        
        private int lenOfSubString(string bvName)
        {
            //Ofer: System.ArgumentOutOfRangeException: StartIndex cannot be less than zero. Parameter name: startIndex (ErrorCode: 0x80131502)
            if (string.IsNullOrWhiteSpace(bvName))
                return 0;

            int len = bvName.Length;
            if (len >= 2)
            {
                var s = bvName.Substring(len - 2, 2);
                if (s == "-L" || s == "-R")
                    len = len - 2;
            }

            return len;
        }

        private void FillLeftRightTable()
        {
            if (this.m_GateExamLeft == null)
                this.m_GateExamLeft = new List<DataRowView>();
            else
                this.m_GateExamLeft.Clear();
            if (this.m_GateExamRight == null)
                this.m_GateExamRight = new List<DataRowView>();
            else
                this.m_GateExamRight.Clear();
            if (this.m_GateExamMiddle == null)
                this.m_GateExamMiddle = new List<DataRowView>();
            else
                this.m_GateExamMiddle.Clear();
            
            var gateNames = new List<string>();

            //Ofer: System.ArgumentOutOfRangeException: StartIndex cannot be less than zero. Parameter name: startIndex (ErrorCode: 0x80131502)
            var copyDvGateExam = new DataView(m_dsGateExam.tb_GateExamination);
            foreach (DataRowView item in copyDvGateExam)
            {
                if (item == null)
                    continue;

                var rowName = (string)item["Name"];
                var subStrLen = lenOfSubString(rowName);
                if (subStrLen == 0)
                    continue;

                var gateName = rowName.Substring(0, subStrLen);
                if (!gateNames.Contains(gateName))
                    gateNames.Add(gateName);

                if (rowName.EndsWith("-L"))
                    m_GateExamLeft.Add(item);
                else if (rowName.EndsWith("-R"))
                    m_GateExamRight.Add(item);
                else
                    m_GateExamMiddle.Add(item);
            }

            //// collect all the gate exam name.
            //foreach (dsGateExamination.tb_GateExaminationRow row in m_dsGateExam.tb_GateExamination.Rows)
            //{
            //    var subStrLen = lenOfSubString(row.Name);
            //    if (subStrLen > 0)
            //    {
            //        var gateName = row.Name.Substring(0, subStrLen);
            //        if (!gateNames.Contains(gateName))
            //            gateNames.Add(gateName);
            //    }
            //}

            //int i = 0;
            //foreach (dsGateExamination.tb_GateExaminationRow row in m_dsGateExam.tb_GateExamination.Rows)
            //{
            //    var rowName = row.Name;

                //var dvRow = copyDvGateExam[i];
                //if (rowName == ((string)dvRow["Name"]))
                //{
                //    if (((string)dvRow["Name"]).EndsWith("-L"))
                //        m_GateExamLeft.Add(dvRow);
                //    else if (((string)dvRow["Name"]).EndsWith("-R"))
                //        m_GateExamRight.Add(dvRow);
                //    else
                //        m_GateExamMiddle.Add(dvRow);
                //}

                //i++;
            //}

            m_GateNameContainer = new List<string>();
            Ds.Tables["tb_TableRepLeftRight"].Clear();
            AddLeftRightTbRow(0, 0, gateNames, 0);
        }

        private void AddLeftRightTbRow(int iLeft, int iRight, List<string> gatesName, int iCur)
        {
            int numberOfRowsInTable = 0;
            if (this.m_GateExamRight.Count >= this.m_GateExamLeft.Count)
                numberOfRowsInTable = this.m_GateExamRight.Count;
            else
                numberOfRowsInTable = this.m_GateExamLeft.Count;
            for (int i = 1; i <= numberOfRowsInTable; i++)
            {
                DataRowView dvL, dvR;
                if (this.m_GateExamRight.Count >= i && this.m_GateExamLeft.Count >= i)
                {
                    dvL = this.m_GateExamLeft[i - 1];
                    dvR = this.m_GateExamRight[i - 1];
                    this.AddLRRow(dvL, dvR);
                }
                if (this.m_GateExamRight.Count >= i && this.m_GateExamLeft.Count < i)
                {
                    dvR = m_GateExamRight[i - 1];
                    this.AddLRRow(null, dvR);
                }
                if (this.m_GateExamRight.Count < i && this.m_GateExamLeft.Count >= i)
                {
                    dvL = m_GateExamLeft[i - 1];
                    this.AddLRRow(dvL, null);
                }
            }
        }

        private void AddLRRow(DataRowView dvRowL, DataRowView dvRowR)
        {
            string gateName = string.Empty;
            DataRow newRow = this.Ds.Tables["tb_TableRepLeftRight"].NewRow();

            if (dvRowL == null)
            {
                newRow["DepthLeft"] = "";
                this.ClearCP(newRow, 1, "Left");
            }
            else
            {
                if (this.m_GateNameContainer.Contains(gateName) != true)
                {
                    gateName = ((string)dvRowL["Name"]).Substring(0, ((string)dvRowL["Name"]).Length - 2);
                    this.m_GateNameContainer.Add(gateName);
                }
                newRow["BVName"] = gateName;
                newRow["DepthLeft"] = ((double)dvRowL["Depth"]).ToString();
                this.InsertCP(newRow, "Left", dvRowL);
            }

            if (dvRowR == null)
            {
                newRow["DepthRight"] = "";
                this.ClearCP(newRow, 1, "Right");
            }
            else
            {
                if (this.m_GateNameContainer.Contains(gateName) != true)
                {
                    gateName = ((string)dvRowR["Name"]).Substring(0, ((string)dvRowR["Name"]).Length - 2);
                    this.m_GateNameContainer.Add(gateName);
                }
                newRow["BVName"] = gateName;
                newRow["DepthRight"] = ((double)dvRowR["Depth"]).ToString();
                this.InsertCP(newRow, "Right", dvRowR);
            }

            //Natalie, 12/18/2009:
            //Original checks were commented for RIMD-185: Show names of BVs in the right column of Patient report
            //			if(dvRowL!=null&&dvRowR!=null)
            //			{
            //if(((string)dvRowL["Name"]).Substring(0, ((string)dvRowL["Name"]).Length-2)!=((string)dvRowR["Name"]).Substring(0, ((string)dvRowR["Name"]).Length-2))
            //Assaf: I use this unused field to hold the name of right BV

            //Natalie, 05/18/2010:
            //Check was added for RIMD-251
            if (dvRowR != null)
                newRow["cpLeftTop9"] = ((string)dvRowR["Name"]).Substring(0, ((string)dvRowR["Name"]).Length - 2);
            else
                if (dvRowL != null)
                    newRow["cpLeftTop9"] = ((string)dvRowL["Name"]).Substring(0, ((string)dvRowL["Name"]).Length - 2);
            //			}
            this.Ds.Tables["tb_TableRepLeftRight"].Rows.Add(newRow);

        }
        
        private void ClearCP(DataRow row, int startPos, string side)
        {
            for (int i = startPos; i < 11; i++)
            {
                row["Cp" + side + "Top" + i.ToString()] = string.Empty;
                row["Cp" + side + "Bottom" + i.ToString()] = string.Empty;
            }
        }
        
        private void InsertCP(DataRow row, string side, DataRowView dvRow)
        {
            for (int i = 0; i < this.m_CpOrder.Count; i++)
            {
                string cpName = this.m_CpOrder[i].Name;
                if (cpName == "S/D") cpName = "SD";
                if (cpName == "P.I.") cpName = "PI";

                string cpNameTop = cpName + "TopVal";
                string cpNameBottom = cpName + "BottomVal";

                int cpIndex = i + 1;
                row["Cp" + side + "Top" + cpIndex.ToString()] = (((double)dvRow[cpNameTop]) == 0) ? string.Empty : ((double)dvRow[cpNameTop]).ToString();
                row["Cp" + side + "Bottom" + cpIndex.ToString()] = (((double)dvRow[cpNameBottom]) == 0) ? string.Empty : ((double)dvRow[cpNameBottom]).ToString();
            }
            this.ClearCP(row, this.m_CpOrder.Count + 1, side);
        }

        private void FillBottomTable()
        {
            this.Ds.Tables["tb_TableRepMiddle"].Clear();
            foreach (DataRowView dvRow in this.m_GateExamMiddle)
            {
                DataRow newRow = Ds.Tables["tb_TableRepMiddle"].NewRow();
                newRow["BVName"] = (string)dvRow["Name"];
                newRow["Depth"] = ((double)dvRow["Depth"]).ToString();
                this.InsertCP(newRow, "Middle", dvRow);
                this.Ds.Tables["tb_TableRepMiddle"].Rows.Add(newRow);
            }
        }
    }

    public class MonitoringReport : FinalReport
    {
        #region Private Fields

        private class ReportEvent
        {
            public string EventName { get; private set; }
            public string EventTime { get; private set; }
            public string EventPath { get; private set; }

            public ReportEvent(string eventName, string eventTime, string eventPath)
            {
                this.EventName = eventName;
                this.EventTime = eventTime;
                this.EventPath = eventPath;
            }
        }

        private dsEventExamination m_dsEventExam = new dsEventExamination();
        private List<ReportEvent> m_leftEvents = new List<ReportEvent>();
        private List<ReportEvent> m_middleEvents = new List<ReportEvent>();
        private List<ReportEvent> m_rightEvents = new List<ReportEvent>();

        #endregion Private Fields

        /// <summary>
        /// Constructor
        /// </summary>
        public MonitoringReport(Guid examIndex, DataView dvHospital, DataView dvPatient, dsExamination dsExam, dsGateExamination dsGateExam, dsEventExamination dsEventExam)
            : base(examIndex, dvHospital, dvPatient, dsExam, dsGateExam)
        {
            this.m_dsEventExam = (dsEventExamination)dsEventExam.Copy();

            this.Report = new crMonitoringRep();

            try
            {
                this.GenReportLayout();
                this.UpdateReportDS();
                this.Report.SetDataSource(this.Ds);
            }
            catch (IndexOutOfRangeException ex)
            {
                ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
            }
            catch (Exception ex)
            {
                ExceptionPublisherLog4Net.ExceptionErrorLog(ex);
                throw;
            }
        }

        private void GenReportLayout()
        {
            DataView dvRepInfo = new DataView(RimedDal.Instance.s_dsPatientRepLayout.tb_PatientRepLayout);

            DataView dv = new DataView();

            if (((bool)dvRepInfo[0]["DateDisplay"]) == false
                && ((bool)dvRepInfo[0]["HospitalDetails"]) == false
                && ((bool)dvRepInfo[0]["HospitalLogo"]) == false)

                this.Report.ReportDefinition.Sections["SectionHospitalInfo"].SectionFormat.EnableSuppress = true;
            else
            {
                if (((bool)dvRepInfo[0]["DateDisplay"]) == false)
                    this.Report.ReportDefinition.ReportObjects["FieldDate"].ObjectFormat.EnableSuppress = true;

                // Hospital Details.
                dv.Table = DB.RimedDal.Instance.s_DS.tb_HospitalDetails;
                for (int i = 0; i < this.Report.ReportDefinition.ReportObjects.Count - 1; i++)
                {
                    Debug.WriteLine(this.Report.ReportDefinition.ReportObjects[i].Name);
                }
                if (((bool)dvRepInfo[0]["HospitalDetails"]) == false)
                {
                    // Change the Visibility of the Hospital Details to FALSE.
                    this.Report.ReportDefinition.ReportObjects[1].ObjectFormat.EnableSuppress = true;
                    this.Report.ReportDefinition.ReportObjects[2].ObjectFormat.EnableSuppress = true;
                    this.Report.ReportDefinition.ReportObjects[3].ObjectFormat.EnableSuppress = true;
                }

                // Hospital Logo
                if (((bool)dvRepInfo[0]["HospitalLogo"]) == false)
                    this.Report.ReportDefinition.ReportObjects[5].ObjectFormat.EnableSuppress = true;
            }

            // Title
            if (((bool)dvRepInfo[0]["TitleDisplay"]) == false)
                this.Report.ReportDefinition.Sections["SectionTitle"].SectionFormat.EnableSuppress = true;

            // Patient Details.
            if (((int)dvRepInfo[0]["PatientDetails"]) == 0)
                // minimal patient details.
                this.Report.ReportDefinition.Sections["SectionPatientDetailFull"].SectionFormat.EnableSuppress = true;

            // Examination history.
            if (((bool)dvRepInfo[0]["ExaminationHistory"]) == false)
                this.Report.ReportDefinition.Sections["SectionExamHistory"].SectionFormat.EnableSuppress = true;

            if (((bool)dvRepInfo[0]["PatientHistory"]) == false)
                this.Report.ReportDefinition.Sections["SectionPatientHistory"].SectionFormat.EnableSuppress = true;

            // layout.
            this.m_reportType = (TRepType)(byte)dvRepInfo[0]["Layout"];

            // Indication
            if (((bool)dvRepInfo[0]["Indication"]) == false)
                this.Report.ReportDefinition.Sections["SectionIndication"].SectionFormat.EnableSuppress = true;

            // Interpretation
            if (((bool)dvRepInfo[0]["Interpretation"]) == false)
                this.Report.ReportDefinition.Sections["SectionInterpretation"].SectionFormat.EnableSuppress = true;

            // Signature
            if (((bool)dvRepInfo[0]["Signature"]) == false)
                this.Report.ReportDefinition.Sections["SectionSignature"].SectionFormat.EnableSuppress = true;
        }

        protected override void UpdateExamData()
        {
            this.Ds.Tables["tb_SummaryScreen0"].Clear();

            // update general report info
            this.UpdateGeneralExamInfo();

            this.Report.ReportDefinition.Sections["SectionEvents"].SectionFormat.EnableSuppress = true;
            this.Report.ReportDefinition.Sections["SectionBilateralEvents"].SectionFormat.EnableSuppress = true;
            if (this.m_reportType == TRepType.eSummary || this.m_reportType == TRepType.eBoth)
            {
                this.FillExamArr();
                this.FillEventTable();
                if (this.StudyName.Contains("Unilateral"))
                {
                    this.Report.ReportDefinition.Sections["SectionEvents"].SectionFormat.EnableSuppress = false;
                }
                else if (this.StudyName.Contains("Bilateral"))
                {
                    this.Report.ReportDefinition.Sections["SectionBilateralEvents"].SectionFormat.EnableSuppress = false;
                }
            }
        }

        private new void FillExamArr()
        {
            if (this.m_leftEvents == null)
                this.m_leftEvents = new List<ReportEvent>();
            else
                this.m_leftEvents.Clear();
            if (this.m_middleEvents == null)
                this.m_middleEvents = new List<ReportEvent>();
            else
                this.m_middleEvents.Clear();
            if (this.m_rightEvents == null)
                this.m_rightEvents = new List<ReportEvent>();
            else
                this.m_rightEvents.Clear();

            string imgPath;

            if (this.StudyName.Contains("Unilateral"))
            {
                var row = ((dsGateExamination.tb_GateExaminationRow)m_dsGateExam.tb_GateExamination.Rows[0]);
                var bvName = row.Name;

                var bvSidePart = General.GetImageFileNameSuffix(bvName);

                for (int i = 0; i < this.m_dsEventExam.tb_ExaminationEvent.Rows.Count; i++)
                {
                    var examEventRow = (dsEventExamination.tb_ExaminationEventRow)m_dsEventExam.tb_ExaminationEvent.Rows[i];
                        imgPath = String.Format("{0}{1}{2}", Constants.GatesImagePath, examEventRow.Event_Index.ToString(), bvSidePart);
                    if (File.Exists(imgPath))
                    {
                        switch (i % 3)
                        {
                            case 0:
                                    m_leftEvents.Add(new ReportEvent(examEventRow.EventName, examEventRow.EventTime.ToString("hh:mm:ss"), imgPath));
                                break;
                            case 1:
                                    m_middleEvents.Add(new ReportEvent(examEventRow.EventName, examEventRow.EventTime.ToString("hh:mm:ss"), imgPath));
                                break;
                            case 2:
                                    m_rightEvents.Add(new ReportEvent(examEventRow.EventName, examEventRow.EventTime.ToString("hh:mm:ss"), imgPath));
                                break;
                        }
                    }
                }
            }
            else if (StudyName.Contains("Bilateral"))
            {
                foreach (dsEventExamination.tb_ExaminationEventRow examEventRow in m_dsEventExam.tb_ExaminationEvent.Rows)
                {
                    imgPath = Constants.GatesImagePath + examEventRow.Event_Index + "_L.jpg";
                    if (File.Exists(imgPath))
                        m_leftEvents.Add(new ReportEvent(examEventRow.EventName, examEventRow.EventTime.ToString("hh:mm:ss"), imgPath));

                    imgPath = Constants.GatesImagePath + examEventRow.Event_Index + "_R.jpg";
                    if (File.Exists(imgPath))
                        m_rightEvents.Add(new ReportEvent(examEventRow.EventName, examEventRow.EventTime.ToString("hh:mm:ss"), imgPath));
                }
            }
        }

        /// <summary>
        /// Fill 3 array with the event screens
        /// </summary>
        private void FillEventTable()
        {
            int width = 243;
            int height = 105;
            for (int i = 0; i < this.m_leftEvents.Count; i++)
            {
                DataRow summaryRow = Ds.Tables["tb_SummaryScreen0"].NewRow();
                summaryRow["LeftImg"] = this.GetReportImage(this.m_leftEvents[i].EventPath, width, height);
                summaryRow["LeftEventName"] = this.m_leftEvents[i].EventName;
                summaryRow["LeftEventTime"] = this.m_leftEvents[i].EventTime;
                if (this.m_middleEvents.Count > i)
                {
                    summaryRow["MiddleImg"] = this.GetReportImage(this.m_middleEvents[i].EventPath, width, height);
                    summaryRow["MiddleEventName"] = this.m_middleEvents[i].EventName;
                    summaryRow["MiddleEventTime"] = this.m_middleEvents[i].EventTime;
                }
                if (this.m_rightEvents.Count > i)
                {
                    summaryRow["RightImg"] = this.GetReportImage(this.m_rightEvents[i].EventPath, width, height);
                    summaryRow["RightEventName"] = this.m_rightEvents[i].EventName;
                    summaryRow["RightEventTime"] = this.m_rightEvents[i].EventTime;
                }
                this.Ds.Tables["tb_SummaryScreen0"].Rows.Add(summaryRow);
            }
        }
    }

    public delegate void ReportTextChangedHandler(Guid examIndex, string field, string text);
}
