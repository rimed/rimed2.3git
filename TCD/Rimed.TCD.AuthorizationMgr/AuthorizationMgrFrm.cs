using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Text;

namespace Rimed.TCD.AuthorizationMgr
{
    /// <summary>
    /// Summary description for AuthorizationMgrFrm.
    /// </summary>
    public partial class AuthorizationMgrFrm : Form
    {
        [DllImport("kernel32.dll")]
        private static extern long GetVolumeInformation(string PathName, StringBuilder VolumeNameBuffer, UInt32 VolumeNameSize, ref UInt32 VolumeSerialNumber, ref UInt32 MaximumComponentLength, ref UInt32 FileSystemFlags, StringBuilder FileSystemNameBuffer, UInt32 FileSystemNameSize);

        private const int VALIDATION_a_z = 219;
        private const int VALIDATION_A_Z = 155;
        private const int VALIDATION_0_9 = 105;

        private const int VALID1_POS = 0;
        private const int MONITORING_POS = 1;
        private const int AUTOSCAN_POS = 2;
        private const int HITS_POS = 3;
        private const int PROBE8MHZ_POS = 4;
        private const int Extracranial_POS = 5;
        private const int IntracraniaUnilateral_POS = 6;
        private const int IntracranialBilateral_POS = 7;

        private const int SEPERATOR1_POS = 8;

        private const int Peripheral_POS = 9;
        private const int MultifrequencyTwoProbes_POS = 10;
        private const int Intraoperative_POS = 11;
        private const int MExtracranial_POS = 12;

        private const int SEPERATOR2_POS = 13;

        private const int MIntracranialUnilateral_POS = 14;
        private const int MIntracranialBilateral_POS = 15;
        private const int PROBE1MHZ_POS = 16;
        private const int MPeripheral_POS = 17;
        private const int SEPERATOR3_POS = 18;

        private const int MMultifrequencyTwoProbes_POS = 19;
        private const int MIntraoperative_POS = 20;
        private const int PROBE16MHZ_POS = 21;
        private const int VMRCO2Reactivity_POS = 22;
        private const int SEPERATOR4_POS = 23;

        private const int VMRDiamoxtest_POS = 24;
        private const int MonitoringOftPA_POS = 25;
        private const int EvokedflowBilateraltest_POS = 26;
        private const int SerialNumber1_POS = 27;  //4
        private const int VMRBilateralDiamoxtest_POS = 28;
        private const int Dicom_POS = 29;
        private const int PFOTest_POS = 30;
        private const int SerialNumber2_POS = 31; // 5
        private const int Evokedflowtest_POS = 32;
        private const int ProbesNum_POS = 33;
        private const int VALID7_POS = 34;
        private const int VALID8_POS = 35;
        private const int VALID9_POS = 36;

        //		private const int STUDY_1_POS		= 25;
        //		private const int STUDY_2_POS		= 27;
        //		private const int STUDY_3_POS		= 29;

        public AuthorizationMgrFrm()
        {
            InitializeComponent();
        }

        private System.Guid m_sysKey;
        private void buttonGetSysKey_Click(object sender, System.EventArgs e)
        {
            m_sysKey = Guid.NewGuid();
            string str = m_sysKey.ToString();
            string[] strArr = str.Split('-');

            this.textBox6.Text = strArr[0];
            this.textBox7.Text = strArr[1];
            this.textBox8.Text = strArr[2];
            this.textBox9.Text = strArr[3];
            this.textBox10.Text = strArr[4];
        }

        private void buttonGenProductKey_Click(object sender, System.EventArgs e)
        {
            if (textBoxSerialNumber.Text.Equals(""))
            {
                MessageBox.Show("Please add serial number");
                return;
            }

            UInt64 serialNumber = 0;
            try
            {
                serialNumber = System.Convert.ToUInt64(textBoxSerialNumber.Text, 10);
            }
            catch (Exception)
            {
                MessageBox.Show("Wrong serial number");
                return;
            }

            string strSysKey = GetSystemKey();

            int n1 = (int)(serialNumber / 854127) % 10 + 48;
            int n2 = (int)(serialNumber / 54127) % 10 + 48;

            var tmp = new char[36];
            byte validation = 0;

            for (int i = 0; i < strSysKey.Length; i++)
            {
                if (strSysKey[i] != '-')
                {
                    if (strSysKey[i] >= 97 && strSysKey[i] <= 122)		// a..z
                        validation = VALIDATION_a_z;
                    else if (strSysKey[i] >= 65 && strSysKey[i] <= 90)	// A..Z
                        validation = VALIDATION_A_Z;
                    else if (strSysKey[i] >= 48 && strSysKey[i] <= 57)	// 0..1
                        validation = VALIDATION_0_9;

                    if (i == VALID1_POS)
                        tmp[i] = Convert.ToChar(validation - strSysKey[i] + 1);
                    else if (i == VALID7_POS || i == VALID8_POS || i == VALID9_POS)
                        tmp[i] = Convert.ToChar(validation - strSysKey[i]);
                    else
                    {
                        if (i == MONITORING_POS)	// monitoring
                            tmp[i] = Convert.ToChar(validation - strSysKey[i] + (this.checkBoxMonitoring.Checked ? 1 : 0));
                        else if (i == AUTOSCAN_POS)
                            tmp[i] = Convert.ToChar(validation - strSysKey[i] + (this.checkBoxAutoscan.Checked ? 1 : 0));
                        else if (i == HITS_POS)
                            tmp[i] = Convert.ToChar(validation - strSysKey[i] + (this.checkBoxHits.Checked ? 1 : 0));
                        else if (i == PROBE8MHZ_POS)
                            tmp[i] = Convert.ToChar(validation - strSysKey[i] + (this.checkBoxProbe8MHz.Checked ? 1 : 0));
                        else if (i == Extracranial_POS)
                            tmp[i] = Convert.ToChar(validation - strSysKey[i] + (this.checkBoxExtracranial.Checked ? 1 : 0));
                        else if (i == IntracraniaUnilateral_POS)
                            tmp[i] = Convert.ToChar(validation - strSysKey[i] + (this.checkBoxIntracraniaUnilateral.Checked ? 1 : 0));
                        else if (i == IntracranialBilateral_POS)
                            tmp[i] = Convert.ToChar(validation - strSysKey[i] + (this.checkBoxIntracranialBilateral.Checked ? 1 : 0));
                        else if (i == Peripheral_POS)
                            tmp[i] = Convert.ToChar(validation - strSysKey[i] + (this.checkBoxPeripheral.Checked ? 1 : 0));
                        else if (i == MultifrequencyTwoProbes_POS)
                            tmp[i] = Convert.ToChar(validation - strSysKey[i] + (this.checkBoxMultifrequencyTwoProbes.Checked ? 1 : 0));
                        else if (i == Intraoperative_POS)
                            tmp[i] = Convert.ToChar(validation - strSysKey[i] + (this.checkBoxIntraoperative.Checked ? 1 : 0));
                        else if (i == MExtracranial_POS)
                            tmp[i] = Convert.ToChar(validation - strSysKey[i] + (this.checkBoxMExtracranial.Checked ? 1 : 0));
                        else if (i == MIntracranialUnilateral_POS)
                            tmp[i] = Convert.ToChar(validation - strSysKey[i] + (this.checkBoxMIntracranialUnilateral.Checked ? 1 : 0));
                        else if (i == MIntracranialBilateral_POS)
                            tmp[i] = Convert.ToChar(validation - strSysKey[i] + (this.checkBoxMIntracranialBilateral.Checked ? 1 : 0));
                        else if (i == MPeripheral_POS)
                            tmp[i] = Convert.ToChar(validation - strSysKey[i] + (this.checkBoxMPeripheral.Checked ? 1 : 0));
                        else if (i == MMultifrequencyTwoProbes_POS)
                            tmp[i] = Convert.ToChar(validation - strSysKey[i] + (this.checkBoxMMultifrequencyTwoProbes.Checked ? 1 : 0));
                        else if (i == MIntraoperative_POS)
                            tmp[i] = Convert.ToChar(validation - strSysKey[i] + (this.checkBoxMIntraoperative.Checked ? 1 : 0));
                        else if (i == VMRCO2Reactivity_POS)
                            tmp[i] = Convert.ToChar(validation - strSysKey[i] + (this.checkBoxVMRCO2Reactivity.Checked ? 1 : 0));
                        else if (i == VMRDiamoxtest_POS)
                            tmp[i] = Convert.ToChar(validation - strSysKey[i] + (this.checkBoxVMRDiamoxtest.Checked ? 1 : 0));
                        else if (i == MonitoringOftPA_POS)
                            tmp[i] = Convert.ToChar(validation - strSysKey[i] + (this.checkBoxMonitoringOftPA.Checked ? 1 : 0));
                        else if (i == EvokedflowBilateraltest_POS)
                            tmp[i] = Convert.ToChar(validation - strSysKey[i] + (this.checkBoxEvokedflowBilateral.Checked ? 1 : 0));
                        else if (i == VMRBilateralDiamoxtest_POS)
                            tmp[i] = Convert.ToChar(validation - strSysKey[i] + (this.checkBoxVMRBilateralDiamox.Checked ? 1 : 0));
                        else if (i == Dicom_POS)
                            tmp[i] = Convert.ToChar(validation - strSysKey[i] + (this.checkBoxDicom.Checked ? 1 : 0));
                        else if (i == PFOTest_POS)
                            tmp[i] = Convert.ToChar(validation - strSysKey[i] + (this.checkBoxPFOTest.Checked ? 1 : 0));
                        else if (i == Evokedflowtest_POS)
                            tmp[i] = Convert.ToChar(validation - strSysKey[i] + (this.checkBoxEvokedflowtest.Checked ? 1 : 0));
                        else if (i == ProbesNum_POS)
                            tmp[i] = Convert.ToChar(validation - strSysKey[i] + (int)numericUpDown1.Value);
                        else if (i == PROBE1MHZ_POS)
                            tmp[i] = Convert.ToChar(validation - strSysKey[i] + (this.checkBoxProbe1MHz.Checked ? 1 : 0));
                        else if (i == PROBE16MHZ_POS)
                            tmp[i] = Convert.ToChar(validation - strSysKey[i] + (this.checkBoxProbe16MHz.Checked ? 1 : 0));
                        else if (i == SerialNumber1_POS)
                            tmp[i] = Convert.ToChar(n1);
                        else if (i == SerialNumber2_POS)
                            tmp[i] = Convert.ToChar(n2);
                    }
                }
                else
                    tmp[i] = '-';
            }

            var proKey = new string(tmp);

            // write the product key to the controls.
            string[] proKeyArr = proKey.Split('-');

            this.textBox1.Text = proKeyArr[0];
            this.textBox2.Text = proKeyArr[1];
            this.textBox3.Text = proKeyArr[2];
            this.textBox4.Text = proKeyArr[3];
            this.textBox5.Text = proKeyArr[4];
        }

        private void checkBoxMonitoring_CheckedChanged(object sender, System.EventArgs e)
        {
            this.EnableCtrls();
        }

        private void EnableCtrls()
        {
            if (this.checkBoxMonitoring.Checked == false)
            {
                foreach (Control ctr in this.groupBoxMonitoring.Controls)
                {
                    if (ctr is CheckBox)
                    {
                        ((CheckBox)ctr).Checked = false;
                    }
                }
            }
            this.groupBoxMonitoring.Enabled = this.checkBoxMonitoring.Checked;
        }

        private void AuthorizationMgrFrm_Load(object sender, System.EventArgs e)
        {
            this.EnableCtrls();
            this.checkBoxMIntracranialBilateral.Checked = checkBoxMIntracranialBilateral.Enabled = false;
            this.checkBoxVMRBilateralDiamox.Checked = checkBoxVMRBilateralDiamox.Enabled = false;
            this.checkBoxEvokedflowBilateral.Checked = checkBoxEvokedflowBilateral.Enabled = false;

            TopMost = true;
            //this.SetSerialNumber();
        }

        private void buttonGetInfo_Click(object sender, System.EventArgs e)
        {
            if (this.textBoxSerialNumber.Text.Equals(""))
            {
                MessageBox.Show("Please add serial number");
                return;
            }

            UInt64 serialNumber = 0;
            try
            {
                serialNumber = System.Convert.ToUInt64(this.textBoxSerialNumber.Text, 10);
            }
            catch (Exception)
            {
                MessageBox.Show("Wrong serial number");
                return;
            }
            int n1 = (int)(serialNumber / 854127) % 10;
            int n2 = (int)(serialNumber / 54127) % 10;

            // get system key.
            string sysKey = this.GetSystemKey();

            // get product key.
            string proKey = this.GetProductKey();

            // accumulate the sysKey and the product key.
            //			char [] tmp = new char[36];
            byte validation = 0;
            int enable = 0;
            bool serialNumberError = false;

            for (int i = 0; i < sysKey.Length; i++)
            {
                if (sysKey[i] != '-')
                {
                    //					tmp[i] = Convert.ToChar(sysKey[i] + proKey[i]);

                    if (sysKey[i] >= 97 && sysKey[i] <= 122)		// a..z
                        validation = VALIDATION_a_z;
                    else if (sysKey[i] >= 65 && sysKey[i] <= 90)	// A..Z
                        validation = VALIDATION_A_Z;
                    else if (sysKey[i] >= 48 && sysKey[i] <= 57)	// 0..1
                        validation = VALIDATION_0_9;

                    enable = sysKey[i] + proKey[i] - validation;

                    if (i == MONITORING_POS)
                        checkBoxMonitoring.Checked = (enable == 1);
                    else if (i == AUTOSCAN_POS)
                        checkBoxAutoscan.Checked = (enable == 1);
                    else if (i == MONITORING_POS)
                        checkBoxHits.Checked = (enable == 1);
                    else if (i == HITS_POS)
                        checkBoxHits.Checked = (enable == 1);
                    else if (i == PROBE8MHZ_POS)
                        checkBoxProbe8MHz.Checked = (enable == 1);
                    else if (i == PROBE1MHZ_POS)
                        checkBoxProbe1MHz.Checked = (enable == 1);
                    else if (i == PROBE16MHZ_POS)
                        checkBoxProbe16MHz.Checked = (enable == 1);
                    else if (i == Extracranial_POS)
                        checkBoxExtracranial.Checked = (enable == 1);
                    else if (i == IntracraniaUnilateral_POS)
                        checkBoxIntracraniaUnilateral.Checked = (enable == 1);
                    else if (i == IntracranialBilateral_POS)
                        checkBoxIntracranialBilateral.Checked = (enable == 1);
                    else if (i == Peripheral_POS)
                        checkBoxPeripheral.Checked = (enable == 1);
                    else if (i == MultifrequencyTwoProbes_POS)
                        checkBoxMultifrequencyTwoProbes.Checked = (enable == 1);
                    else if (i == Intraoperative_POS)
                        checkBoxIntraoperative.Checked = (enable == 1);
                    else if (i == MExtracranial_POS)
                        checkBoxMExtracranial.Checked = (enable == 1);
                    else if (i == MIntracranialUnilateral_POS)
                        checkBoxMIntracranialUnilateral.Checked = (enable == 1);
                    else if (i == MIntracranialBilateral_POS)
                        checkBoxMIntracranialBilateral.Checked = (enable == 1);
                    else if (i == MPeripheral_POS)
                        checkBoxMPeripheral.Checked = (enable == 1);
                    else if (i == MMultifrequencyTwoProbes_POS)
                        checkBoxMMultifrequencyTwoProbes.Checked = (enable == 1);
                    else if (i == MIntraoperative_POS)
                        checkBoxMIntraoperative.Checked = (enable == 1);
                    else if (i == VMRCO2Reactivity_POS)
                        checkBoxVMRCO2Reactivity.Checked = (enable == 1);
                    else if (i == VMRDiamoxtest_POS)
                        checkBoxVMRDiamoxtest.Checked = (enable == 1);
                    else if (i == MonitoringOftPA_POS)
                        checkBoxMonitoringOftPA.Checked = (enable == 1);
                    else if (i == EvokedflowBilateraltest_POS)
                        checkBoxEvokedflowBilateral.Checked = (enable == 1);
                    else if (i == VMRBilateralDiamoxtest_POS)
                        checkBoxVMRBilateralDiamox.Checked = (enable == 1);
                    else if (i == Dicom_POS)
                        checkBoxDicom.Checked = (enable == 1);
                    else if (i == PFOTest_POS)
                        checkBoxPFOTest.Checked = (enable == 1);
                    else if (i == Evokedflowtest_POS)
                        checkBoxEvokedflowtest.Checked = (enable == 1);
                    else if (i == ProbesNum_POS)
                        numericUpDown1.Value = (sysKey[i] + proKey[i] - validation);
                    else if (i == SerialNumber1_POS)
                    {
                        string str = Convert.ToString(proKey[i]);
                        if (!str.Equals(n1.ToString()))
                            serialNumberError = true;
                    }
                    else if (i == SerialNumber2_POS)
                    {
                        string str = Convert.ToString(proKey[i]);
                        if (!str.Equals(n2.ToString()))
                            serialNumberError = true;
                    }
                }
            }
            if (serialNumberError)
                MessageBox.Show("This license does not match Serial number");
        }

        private void numericUpDown1_ValueChanged(object sender, System.EventArgs e)
        {
            if (((NumericUpDown)sender).Value > 2)
                ((NumericUpDown)sender).Value = 2;
            if (((NumericUpDown)sender).Value < 1)
                ((NumericUpDown)sender).Value = 1;

            //RIMD-458: Authorization Manager changes: disable Bilateral studies for Probe Num = 1
            if (!cbVersion.Checked)
            {
                if (((NumericUpDown)sender).Value == 1)
                {
                    // uncheck and mark out all the 2 probes studies.
                    checkBoxMIntracranialBilateral.Checked = checkBoxMIntracranialBilateral.Enabled = false;
                    checkBoxVMRBilateralDiamox.Checked = checkBoxVMRBilateralDiamox.Enabled = false;
                    checkBoxEvokedflowBilateral.Checked = checkBoxEvokedflowBilateral.Enabled = false;
                }
                else
                {
                    checkBoxMIntracranialBilateral.Enabled = true;
                    checkBoxVMRBilateralDiamox.Enabled = true;
                    checkBoxEvokedflowBilateral.Enabled = true;
                }
            }
        }

        private void menuItemAbout_Click(object sender, System.EventArgs e)
        {
            using (var dlg = new AboutDlg())
            {
                dlg.ShowDialog(this);
            }
        }

        private void menuItemContents_Click(object sender, System.EventArgs e)
        {
            MessageBox.Show("Not implemented yet!!!", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void AuthorizationMgrFrm_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            e.Handled = true;
            switch (e.KeyCode)
            {
                case Keys.C:
                    if (e.Control)
                    {
                        if (textBox6.Focused ||
                            textBox7.Focused ||
                            textBox8.Focused ||
                            textBox9.Focused ||
                            textBox10.Focused)
                        {
                            Clipboard.SetDataObject(this.GetSystemKey(), false);
                        }

                        if (textBox1.Focused ||
                            textBox2.Focused ||
                            textBox3.Focused ||
                            textBox4.Focused ||
                            textBox5.Focused)
                        {
                            Clipboard.SetDataObject(this.GetProductKey(), false);
                        }
                    }
                    break;
                default:
                    break;
            }
        }

        private string GetSystemKey()
        {
            return textBox6.Text + "-" + textBox7.Text + "-" + textBox8.Text + "-" + textBox9.Text + "-" + textBox10.Text;
        }

        private string GetProductKey()
        {
            return textBox1.Text + "-" + textBox2.Text + "-" + textBox3.Text + "-" + textBox4.Text + "-" + textBox5.Text;
        }

        private void btnSystemKeyCopy_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            btnSystemKeyCopy.BorderStyle = BorderStyle.Fixed3D;
        }

        private void btnProductKeyCopy_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            btnProductKeyCopy.BorderStyle = BorderStyle.Fixed3D;
        }

        private void btnSystemKeyCopy_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            btnSystemKeyCopy.BorderStyle = BorderStyle.None;
            Clipboard.SetDataObject(GetSystemKey(), false);
        }

        private void btnProductKeyCopy_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            btnProductKeyCopy.BorderStyle = BorderStyle.None;
            Clipboard.SetDataObject(GetProductKey(), false);
        }

        /// <summary>
        /// This method was added for the correct work with old versions of AM
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cbVersion_CheckedChanged(object sender, System.EventArgs e)
        {
            if (cbVersion.Checked)
            {
                checkBoxDicom.Checked = checkBoxDicom.Enabled = false;
                checkBoxVMRBilateralDiamox.Checked = checkBoxVMRBilateralDiamox.Enabled = false;
                checkBoxEvokedflowBilateral.Checked = checkBoxEvokedflowBilateral.Enabled = false;

                checkBoxMIntracranialBilateral.Enabled = true;
            }
            else
            {
                checkBoxDicom.Enabled = true;

                if (numericUpDown1.Value == 1)
                {
                    // uncheck and mark out all the 2 probes studies.
                    checkBoxMIntracranialBilateral.Checked = checkBoxMIntracranialBilateral.Enabled = false;
                    checkBoxVMRBilateralDiamox.Checked = checkBoxVMRBilateralDiamox.Enabled = false;
                    checkBoxEvokedflowBilateral.Checked = checkBoxEvokedflowBilateral.Enabled = false;
                }
                else
                {
                    checkBoxMIntracranialBilateral.Enabled = true;
                    checkBoxVMRBilateralDiamox.Enabled = true;
                    checkBoxEvokedflowBilateral.Enabled = true;
                }
            }
        }

        private void SetSerialNumber()
        {
            var volumeLabel = new StringBuilder(256);	// Label
            var volumeFlags = new UInt32();
            var fsName = new StringBuilder(256);	// File System Name
            uint serialNumber = 0;
            uint maxCompLength = 0;

            // Attempt to retreive the information
            long ret = GetVolumeInformation(@"C:\", volumeLabel, (UInt32)volumeLabel.Capacity, ref serialNumber, ref maxCompLength, ref volumeFlags, fsName, (UInt32)fsName.Capacity);

            textBoxSerialNumber.Text = serialNumber.ToString();
        }
    }
}
