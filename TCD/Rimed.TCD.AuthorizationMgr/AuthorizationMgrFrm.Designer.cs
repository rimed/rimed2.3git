﻿using Rimed.TCD.Utils;

namespace Rimed.TCD.AuthorizationMgr
{
    partial class AuthorizationMgrFrm
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AuthorizationMgrFrm));
			this.buttonCancel = new System.Windows.Forms.Button();
			this.buttonGetInfo = new System.Windows.Forms.Button();
			this.buttonGenProductKey = new System.Windows.Forms.Button();
			this.buttonGetSysKey = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.textBox6 = new System.Windows.Forms.TextBox();
			this.textBox7 = new System.Windows.Forms.TextBox();
			this.textBox9 = new System.Windows.Forms.TextBox();
			this.textBox8 = new System.Windows.Forms.TextBox();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.textBox4 = new System.Windows.Forms.TextBox();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.labelSperate6 = new System.Windows.Forms.Label();
			this.labelSperate5 = new System.Windows.Forms.Label();
			this.labelSperate4 = new System.Windows.Forms.Label();
			this.labelSperate3 = new System.Windows.Forms.Label();
			this.labelSperate2 = new System.Windows.Forms.Label();
			this.labelSperate1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.textBox10 = new System.Windows.Forms.TextBox();
			this.textBox5 = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.checkBoxIntracraniaUnilateral = new System.Windows.Forms.CheckBox();
			this.checkBoxIntracranialBilateral = new System.Windows.Forms.CheckBox();
			this.checkBoxExtracranial = new System.Windows.Forms.CheckBox();
			this.checkBoxMultifrequencyTwoProbes = new System.Windows.Forms.CheckBox();
			this.checkBoxIntraoperative = new System.Windows.Forms.CheckBox();
			this.checkBoxPeripheral = new System.Windows.Forms.CheckBox();
			this.groupBoxMonitoring = new System.Windows.Forms.GroupBox();
			this.checkBoxMIntracranialUnilateral = new System.Windows.Forms.CheckBox();
			this.checkBoxMIntracranialBilateral = new System.Windows.Forms.CheckBox();
			this.checkBoxMExtracranial = new System.Windows.Forms.CheckBox();
			this.checkBoxMMultifrequencyTwoProbes = new System.Windows.Forms.CheckBox();
			this.checkBoxMIntraoperative = new System.Windows.Forms.CheckBox();
			this.checkBoxMPeripheral = new System.Windows.Forms.CheckBox();
			this.checkBoxVMRDiamoxtest = new System.Windows.Forms.CheckBox();
			this.checkBoxEvokedflowtest = new System.Windows.Forms.CheckBox();
			this.checkBoxVMRBilateralDiamox = new System.Windows.Forms.CheckBox();
			this.checkBoxEvokedflowBilateral = new System.Windows.Forms.CheckBox();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.checkBoxMonitoringOftPA = new System.Windows.Forms.CheckBox();
			this.checkBoxPFOTest = new System.Windows.Forms.CheckBox();
			this.checkBoxVMRCO2Reactivity = new System.Windows.Forms.CheckBox();
			this.checkBoxDicom = new System.Windows.Forms.CheckBox();
			this.checkBoxMonitoring = new System.Windows.Forms.CheckBox();
			this.checkBoxAutoscan = new System.Windows.Forms.CheckBox();
			this.checkBoxProbe8MHz = new System.Windows.Forms.CheckBox();
			this.checkBoxHits = new System.Windows.Forms.CheckBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.checkBoxProbe16MHz = new System.Windows.Forms.CheckBox();
			this.checkBoxProbe1MHz = new System.Windows.Forms.CheckBox();
			this.label5 = new System.Windows.Forms.Label();
			this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
			this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.menuItemContents = new System.Windows.Forms.MenuItem();
			this.menuItemAbout = new System.Windows.Forms.MenuItem();
			this.textBoxSerialNumber = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.btnSystemKeyCopy = new System.Windows.Forms.PictureBox();
			this.btnProductKeyCopy = new System.Windows.Forms.PictureBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.cbVersion = new System.Windows.Forms.CheckBox();
			this.groupBox1.SuspendLayout();
			this.groupBoxMonitoring.SuspendLayout();
			this.groupBox4.SuspendLayout();
			this.groupBox3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnSystemKeyCopy)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProductKeyCopy)).BeginInit();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// buttonCancel
			// 
			this.buttonCancel.BackColor = System.Drawing.SystemColors.InactiveCaption;
			this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.buttonCancel.Location = new System.Drawing.Point(568, 504);
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.Size = new System.Drawing.Size(184, 24);
			this.buttonCancel.TabIndex = 14;
			this.buttonCancel.Text = "Close";
			this.buttonCancel.UseVisualStyleBackColor = false;
			// 
			// buttonGetInfo
			// 
			this.buttonGetInfo.BackColor = System.Drawing.SystemColors.InactiveCaption;
			this.buttonGetInfo.Location = new System.Drawing.Point(376, 151);
			this.buttonGetInfo.Name = "buttonGetInfo";
			this.buttonGetInfo.Size = new System.Drawing.Size(144, 24);
			this.buttonGetInfo.TabIndex = 11;
			this.buttonGetInfo.Text = "Get Info";
			this.buttonGetInfo.UseVisualStyleBackColor = false;
			this.buttonGetInfo.Click += new System.EventHandler(this.buttonGetInfo_Click);
			// 
			// buttonGenProductKey
			// 
			this.buttonGenProductKey.BackColor = System.Drawing.SystemColors.InactiveCaption;
			this.buttonGenProductKey.Location = new System.Drawing.Point(376, 95);
			this.buttonGenProductKey.Name = "buttonGenProductKey";
			this.buttonGenProductKey.Size = new System.Drawing.Size(144, 24);
			this.buttonGenProductKey.TabIndex = 12;
			this.buttonGenProductKey.Text = "Generate Product Key";
			this.buttonGenProductKey.UseVisualStyleBackColor = false;
			this.buttonGenProductKey.Click += new System.EventHandler(this.buttonGenProductKey_Click);
			// 
			// buttonGetSysKey
			// 
			this.buttonGetSysKey.BackColor = System.Drawing.SystemColors.InactiveCaption;
			this.buttonGetSysKey.Location = new System.Drawing.Point(376, 39);
			this.buttonGetSysKey.Name = "buttonGetSysKey";
			this.buttonGetSysKey.Size = new System.Drawing.Size(144, 24);
			this.buttonGetSysKey.TabIndex = 10;
			this.buttonGetSysKey.Text = "Get New System Key";
			this.buttonGetSysKey.UseVisualStyleBackColor = false;
			this.buttonGetSysKey.Click += new System.EventHandler(this.buttonGetSysKey_Click);
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(264, 40);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(8, 16);
			this.label3.TabIndex = 58;
			this.label3.Text = "-";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 72);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(80, 24);
			this.label1.TabIndex = 56;
			this.label1.Text = "Product Key:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// textBox6
			// 
			this.textBox6.Location = new System.Drawing.Point(8, 40);
			this.textBox6.MaxLength = 8;
			this.textBox6.Name = "textBox6";
			this.textBox6.Size = new System.Drawing.Size(88, 20);
			this.textBox6.TabIndex = 0;
			// 
			// textBox7
			// 
			this.textBox7.Location = new System.Drawing.Point(104, 40);
			this.textBox7.MaxLength = 4;
			this.textBox7.Name = "textBox7";
			this.textBox7.Size = new System.Drawing.Size(48, 20);
			this.textBox7.TabIndex = 1;
			// 
			// textBox9
			// 
			this.textBox9.Location = new System.Drawing.Point(216, 40);
			this.textBox9.MaxLength = 4;
			this.textBox9.Name = "textBox9";
			this.textBox9.Size = new System.Drawing.Size(48, 20);
			this.textBox9.TabIndex = 3;
			// 
			// textBox8
			// 
			this.textBox8.Location = new System.Drawing.Point(160, 40);
			this.textBox8.MaxLength = 4;
			this.textBox8.Name = "textBox8";
			this.textBox8.Size = new System.Drawing.Size(48, 20);
			this.textBox8.TabIndex = 2;
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(8, 96);
			this.textBox1.MaxLength = 8;
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(88, 20);
			this.textBox1.TabIndex = 5;
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(104, 96);
			this.textBox2.MaxLength = 4;
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new System.Drawing.Size(48, 20);
			this.textBox2.TabIndex = 6;
			// 
			// textBox4
			// 
			this.textBox4.Location = new System.Drawing.Point(216, 96);
			this.textBox4.MaxLength = 4;
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new System.Drawing.Size(48, 20);
			this.textBox4.TabIndex = 8;
			// 
			// textBox3
			// 
			this.textBox3.Location = new System.Drawing.Point(160, 96);
			this.textBox3.MaxLength = 4;
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new System.Drawing.Size(48, 20);
			this.textBox3.TabIndex = 7;
			// 
			// labelSperate6
			// 
			this.labelSperate6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelSperate6.Location = new System.Drawing.Point(208, 40);
			this.labelSperate6.Name = "labelSperate6";
			this.labelSperate6.Size = new System.Drawing.Size(8, 16);
			this.labelSperate6.TabIndex = 48;
			this.labelSperate6.Text = "-";
			// 
			// labelSperate5
			// 
			this.labelSperate5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelSperate5.Location = new System.Drawing.Point(152, 40);
			this.labelSperate5.Name = "labelSperate5";
			this.labelSperate5.Size = new System.Drawing.Size(8, 16);
			this.labelSperate5.TabIndex = 47;
			this.labelSperate5.Text = "-";
			// 
			// labelSperate4
			// 
			this.labelSperate4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelSperate4.Location = new System.Drawing.Point(96, 40);
			this.labelSperate4.Name = "labelSperate4";
			this.labelSperate4.Size = new System.Drawing.Size(8, 16);
			this.labelSperate4.TabIndex = 46;
			this.labelSperate4.Text = "-";
			// 
			// labelSperate3
			// 
			this.labelSperate3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelSperate3.Location = new System.Drawing.Point(208, 96);
			this.labelSperate3.Name = "labelSperate3";
			this.labelSperate3.Size = new System.Drawing.Size(8, 16);
			this.labelSperate3.TabIndex = 45;
			this.labelSperate3.Text = "-";
			// 
			// labelSperate2
			// 
			this.labelSperate2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelSperate2.Location = new System.Drawing.Point(152, 96);
			this.labelSperate2.Name = "labelSperate2";
			this.labelSperate2.Size = new System.Drawing.Size(8, 16);
			this.labelSperate2.TabIndex = 44;
			this.labelSperate2.Text = "-";
			// 
			// labelSperate1
			// 
			this.labelSperate1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelSperate1.Location = new System.Drawing.Point(96, 96);
			this.labelSperate1.Name = "labelSperate1";
			this.labelSperate1.Size = new System.Drawing.Size(8, 16);
			this.labelSperate1.TabIndex = 43;
			this.labelSperate1.Text = "-";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 16);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(80, 24);
			this.label2.TabIndex = 55;
			this.label2.Text = "System Key:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// textBox10
			// 
			this.textBox10.Location = new System.Drawing.Point(272, 40);
			this.textBox10.MaxLength = 12;
			this.textBox10.Name = "textBox10";
			this.textBox10.Size = new System.Drawing.Size(88, 20);
			this.textBox10.TabIndex = 4;
			// 
			// textBox5
			// 
			this.textBox5.Location = new System.Drawing.Point(272, 96);
			this.textBox5.MaxLength = 12;
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new System.Drawing.Size(88, 20);
			this.textBox5.TabIndex = 9;
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(264, 96);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(8, 16);
			this.label4.TabIndex = 8;
			this.label4.Text = "-";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.checkBoxIntracraniaUnilateral);
			this.groupBox1.Controls.Add(this.checkBoxIntracranialBilateral);
			this.groupBox1.Controls.Add(this.checkBoxExtracranial);
			this.groupBox1.Controls.Add(this.checkBoxMultifrequencyTwoProbes);
			this.groupBox1.Controls.Add(this.checkBoxIntraoperative);
			this.groupBox1.Controls.Add(this.checkBoxPeripheral);
			this.groupBox1.Location = new System.Drawing.Point(176, 192);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(184, 344);
			this.groupBox1.TabIndex = 59;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Diagnostic Studies";
			// 
			// checkBoxIntracraniaUnilateral
			// 
			this.checkBoxIntracraniaUnilateral.Checked = true;
			this.checkBoxIntracraniaUnilateral.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxIntracraniaUnilateral.Location = new System.Drawing.Point(8, 56);
			this.checkBoxIntracraniaUnilateral.Name = "checkBoxIntracraniaUnilateral";
			this.checkBoxIntracraniaUnilateral.Size = new System.Drawing.Size(168, 24);
			this.checkBoxIntracraniaUnilateral.TabIndex = 0;
			this.checkBoxIntracraniaUnilateral.Text = Constants.StudyType.STUDY_INTERCRANIAL_UNILATERAL;
			// 
			// checkBoxIntracranialBilateral
			// 
			this.checkBoxIntracranialBilateral.Location = new System.Drawing.Point(8, 184);
			this.checkBoxIntracranialBilateral.Name = "checkBoxIntracranialBilateral";
			this.checkBoxIntracranialBilateral.Size = new System.Drawing.Size(168, 24);
			this.checkBoxIntracranialBilateral.TabIndex = 1;
			this.checkBoxIntracranialBilateral.Text = Constants.StudyType.STUDY_INTERCRANIAL_BILATERAL;
			this.checkBoxIntracranialBilateral.Visible = false;
			// 
			// checkBoxExtracranial
			// 
			this.checkBoxExtracranial.Checked = true;
			this.checkBoxExtracranial.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxExtracranial.Location = new System.Drawing.Point(8, 24);
			this.checkBoxExtracranial.Name = "checkBoxExtracranial";
			this.checkBoxExtracranial.Size = new System.Drawing.Size(168, 24);
			this.checkBoxExtracranial.TabIndex = 3;
			this.checkBoxExtracranial.Text = Constants.StudyType.STUDY_EXTRACRANIAL;
			// 
			// checkBoxMultifrequencyTwoProbes
			// 
			this.checkBoxMultifrequencyTwoProbes.Location = new System.Drawing.Point(8, 120);
			this.checkBoxMultifrequencyTwoProbes.Name = "checkBoxMultifrequencyTwoProbes";
			this.checkBoxMultifrequencyTwoProbes.Size = new System.Drawing.Size(168, 24);
			this.checkBoxMultifrequencyTwoProbes.TabIndex = 4;
			this.checkBoxMultifrequencyTwoProbes.Text = Constants.StudyType.STUDY_MULTIFREQ_TWO_PROBES;
			// 
			// checkBoxIntraoperative
			// 
			this.checkBoxIntraoperative.Checked = true;
			this.checkBoxIntraoperative.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxIntraoperative.Location = new System.Drawing.Point(8, 152);
			this.checkBoxIntraoperative.Name = "checkBoxIntraoperative";
			this.checkBoxIntraoperative.Size = new System.Drawing.Size(168, 24);
			this.checkBoxIntraoperative.TabIndex = 6;
			this.checkBoxIntraoperative.Text = Constants.StudyType.STUDY_INTRAOPERATIVE;
			// 
			// checkBoxPeripheral
			// 
			this.checkBoxPeripheral.Checked = true;
			this.checkBoxPeripheral.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxPeripheral.Location = new System.Drawing.Point(8, 88);
			this.checkBoxPeripheral.Name = "checkBoxPeripheral";
			this.checkBoxPeripheral.Size = new System.Drawing.Size(168, 24);
			this.checkBoxPeripheral.TabIndex = 7;
			this.checkBoxPeripheral.Text = Constants.StudyType.STUDY_PERIPHERAL;
			// 
			// groupBoxMonitoring
			// 
			this.groupBoxMonitoring.Controls.Add(this.checkBoxMIntracranialUnilateral);
			this.groupBoxMonitoring.Controls.Add(this.checkBoxMIntracranialBilateral);
			this.groupBoxMonitoring.Controls.Add(this.checkBoxMExtracranial);
			this.groupBoxMonitoring.Controls.Add(this.checkBoxMMultifrequencyTwoProbes);
			this.groupBoxMonitoring.Controls.Add(this.checkBoxMIntraoperative);
			this.groupBoxMonitoring.Controls.Add(this.checkBoxMPeripheral);
			this.groupBoxMonitoring.Controls.Add(this.checkBoxVMRDiamoxtest);
			this.groupBoxMonitoring.Controls.Add(this.checkBoxEvokedflowtest);
			this.groupBoxMonitoring.Controls.Add(this.checkBoxVMRBilateralDiamox);
			this.groupBoxMonitoring.Controls.Add(this.checkBoxEvokedflowBilateral);
			this.groupBoxMonitoring.Location = new System.Drawing.Point(368, 192);
			this.groupBoxMonitoring.Name = "groupBoxMonitoring";
			this.groupBoxMonitoring.Size = new System.Drawing.Size(184, 344);
			this.groupBoxMonitoring.TabIndex = 59;
			this.groupBoxMonitoring.TabStop = false;
			this.groupBoxMonitoring.Text = "Monitoring Studies";
			// 
			// checkBoxMIntracranialUnilateral
			// 
			this.checkBoxMIntracranialUnilateral.Location = new System.Drawing.Point(8, 56);
			this.checkBoxMIntracranialUnilateral.Name = "checkBoxMIntracranialUnilateral";
			this.checkBoxMIntracranialUnilateral.Size = new System.Drawing.Size(168, 24);
			this.checkBoxMIntracranialUnilateral.TabIndex = 0;
			this.checkBoxMIntracranialUnilateral.Text = Constants.StudyType.STUDY_INTERCRANIAL_UNILATERAL;
			// 
			// checkBoxMIntracranialBilateral
			// 
			this.checkBoxMIntracranialBilateral.Location = new System.Drawing.Point(8, 88);
			this.checkBoxMIntracranialBilateral.Name = "checkBoxMIntracranialBilateral";
			this.checkBoxMIntracranialBilateral.Size = new System.Drawing.Size(168, 24);
			this.checkBoxMIntracranialBilateral.TabIndex = 1;
			this.checkBoxMIntracranialBilateral.Text = Constants.StudyType.STUDY_INTERCRANIAL_BILATERAL;
			// 
			// checkBoxMExtracranial
			// 
			this.checkBoxMExtracranial.Location = new System.Drawing.Point(8, 24);
			this.checkBoxMExtracranial.Name = "checkBoxMExtracranial";
			this.checkBoxMExtracranial.Size = new System.Drawing.Size(168, 24);
			this.checkBoxMExtracranial.TabIndex = 3;
			this.checkBoxMExtracranial.Text = Constants.StudyType.STUDY_EXTRACRANIAL;
			// 
			// checkBoxMMultifrequencyTwoProbes
			// 
			this.checkBoxMMultifrequencyTwoProbes.Location = new System.Drawing.Point(8, 152);
			this.checkBoxMMultifrequencyTwoProbes.Name = "checkBoxMMultifrequencyTwoProbes";
			this.checkBoxMMultifrequencyTwoProbes.Size = new System.Drawing.Size(168, 24);
			this.checkBoxMMultifrequencyTwoProbes.TabIndex = 4;
			this.checkBoxMMultifrequencyTwoProbes.Text = Constants.StudyType.STUDY_MULTIFREQ_TWO_PROBES;
			// 
			// checkBoxMIntraoperative
			// 
			this.checkBoxMIntraoperative.Location = new System.Drawing.Point(8, 184);
			this.checkBoxMIntraoperative.Name = "checkBoxMIntraoperative";
			this.checkBoxMIntraoperative.Size = new System.Drawing.Size(168, 24);
			this.checkBoxMIntraoperative.TabIndex = 6;
			this.checkBoxMIntraoperative.Text = Constants.StudyType.STUDY_INTRAOPERATIVE;
			// 
			// checkBoxMPeripheral
			// 
			this.checkBoxMPeripheral.Location = new System.Drawing.Point(8, 120);
			this.checkBoxMPeripheral.Name = "checkBoxMPeripheral";
			this.checkBoxMPeripheral.Size = new System.Drawing.Size(168, 24);
			this.checkBoxMPeripheral.TabIndex = 7;
			this.checkBoxMPeripheral.Text = Constants.StudyType.STUDY_PERIPHERAL;
			// 
			// checkBoxVMRDiamoxtest
			// 
			this.checkBoxVMRDiamoxtest.Location = new System.Drawing.Point(8, 216);
			this.checkBoxVMRDiamoxtest.Name = "checkBoxVMRDiamoxtest";
			this.checkBoxVMRDiamoxtest.Size = new System.Drawing.Size(168, 24);
			this.checkBoxVMRDiamoxtest.TabIndex = 0;
            this.checkBoxVMRDiamoxtest.Text = "VMR CO2 Stimulation test";
			// 
			// checkBoxEvokedflowtest
			// 
			this.checkBoxEvokedflowtest.Location = new System.Drawing.Point(8, 248);
			this.checkBoxEvokedflowtest.Name = "checkBoxEvokedflowtest";
			this.checkBoxEvokedflowtest.Size = new System.Drawing.Size(168, 24);
			this.checkBoxEvokedflowtest.TabIndex = 0;
			this.checkBoxEvokedflowtest.Text = "Evoked flow test";
			// 
			// checkBoxVMRBilateralDiamox
			// 
			this.checkBoxVMRBilateralDiamox.Location = new System.Drawing.Point(8, 280);
			this.checkBoxVMRBilateralDiamox.Name = "checkBoxVMRBilateralDiamox";
			this.checkBoxVMRBilateralDiamox.Size = new System.Drawing.Size(168, 24);
			this.checkBoxVMRBilateralDiamox.TabIndex = 0;
            this.checkBoxVMRBilateralDiamox.Text = "VMR CO2 Stimulation Bilateral test";
			// 
			// checkBoxEvokedflowBilateral
			// 
			this.checkBoxEvokedflowBilateral.Location = new System.Drawing.Point(8, 312);
			this.checkBoxEvokedflowBilateral.Name = "checkBoxEvokedflowBilateral";
			this.checkBoxEvokedflowBilateral.Size = new System.Drawing.Size(168, 24);
			this.checkBoxEvokedflowBilateral.TabIndex = 0;
			this.checkBoxEvokedflowBilateral.Text = "Evoked flow Bilateral test";
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.checkBoxMonitoringOftPA);
			this.groupBox4.Controls.Add(this.checkBoxPFOTest);
			this.groupBox4.Controls.Add(this.checkBoxVMRCO2Reactivity);
			this.groupBox4.Location = new System.Drawing.Point(568, 192);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(184, 304);
			this.groupBox4.TabIndex = 61;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Spatial Studies";
			// 
			// checkBoxMonitoringOftPA
			// 
			this.checkBoxMonitoringOftPA.Location = new System.Drawing.Point(8, 56);
			this.checkBoxMonitoringOftPA.Name = "checkBoxMonitoringOftPA";
			this.checkBoxMonitoringOftPA.Size = new System.Drawing.Size(168, 24);
			this.checkBoxMonitoringOftPA.TabIndex = 0;
			this.checkBoxMonitoringOftPA.Text = "Monitoring Of tPA";
			// 
			// checkBoxPFOTest
			// 
			this.checkBoxPFOTest.Location = new System.Drawing.Point(8, 88);
			this.checkBoxPFOTest.Name = "checkBoxPFOTest";
			this.checkBoxPFOTest.Size = new System.Drawing.Size(168, 24);
			this.checkBoxPFOTest.TabIndex = 0;
			this.checkBoxPFOTest.Text = "PFO Test";
			// 
			// checkBoxVMRCO2Reactivity
			// 
			this.checkBoxVMRCO2Reactivity.Location = new System.Drawing.Point(8, 24);
			this.checkBoxVMRCO2Reactivity.Name = "checkBoxVMRCO2Reactivity";
			this.checkBoxVMRCO2Reactivity.Size = new System.Drawing.Size(168, 24);
			this.checkBoxVMRCO2Reactivity.TabIndex = 0;
			this.checkBoxVMRCO2Reactivity.Text = "VMR CO2 Reactivity";
			// 
			// checkBoxDicom
			// 
			this.checkBoxDicom.Location = new System.Drawing.Point(16, 248);
			this.checkBoxDicom.Name = "checkBoxDicom";
			this.checkBoxDicom.Size = new System.Drawing.Size(112, 24);
			this.checkBoxDicom.TabIndex = 0;
			this.checkBoxDicom.Text = "Dicom";
			// 
			// checkBoxMonitoring
			// 
			this.checkBoxMonitoring.BackColor = System.Drawing.SystemColors.Control;
			this.checkBoxMonitoring.Location = new System.Drawing.Point(16, 56);
			this.checkBoxMonitoring.Name = "checkBoxMonitoring";
			this.checkBoxMonitoring.Size = new System.Drawing.Size(112, 24);
			this.checkBoxMonitoring.TabIndex = 0;
			this.checkBoxMonitoring.Text = "Monitoring";
			this.checkBoxMonitoring.UseVisualStyleBackColor = false;
			this.checkBoxMonitoring.CheckedChanged += new System.EventHandler(this.checkBoxMonitoring_CheckedChanged);
			// 
			// checkBoxAutoscan
			// 
			this.checkBoxAutoscan.BackColor = System.Drawing.SystemColors.Control;
			this.checkBoxAutoscan.Checked = true;
			this.checkBoxAutoscan.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxAutoscan.Location = new System.Drawing.Point(16, 88);
			this.checkBoxAutoscan.Name = "checkBoxAutoscan";
			this.checkBoxAutoscan.Size = new System.Drawing.Size(112, 24);
			this.checkBoxAutoscan.TabIndex = 1;
			this.checkBoxAutoscan.Text = "M-mode";
			this.checkBoxAutoscan.UseVisualStyleBackColor = false;
			// 
			// checkBoxProbe8MHz
			// 
			this.checkBoxProbe8MHz.BackColor = System.Drawing.SystemColors.Control;
			this.checkBoxProbe8MHz.Checked = true;
			this.checkBoxProbe8MHz.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxProbe8MHz.Location = new System.Drawing.Point(16, 184);
			this.checkBoxProbe8MHz.Name = "checkBoxProbe8MHz";
			this.checkBoxProbe8MHz.Size = new System.Drawing.Size(112, 24);
			this.checkBoxProbe8MHz.TabIndex = 2;
			this.checkBoxProbe8MHz.Text = "Probe 8 Mhz";
			this.checkBoxProbe8MHz.UseVisualStyleBackColor = false;
			// 
			// checkBoxHits
			// 
			this.checkBoxHits.BackColor = System.Drawing.SystemColors.Control;
			this.checkBoxHits.Checked = true;
			this.checkBoxHits.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxHits.Location = new System.Drawing.Point(16, 120);
			this.checkBoxHits.Name = "checkBoxHits";
			this.checkBoxHits.Size = new System.Drawing.Size(112, 24);
			this.checkBoxHits.TabIndex = 3;
			this.checkBoxHits.Text = "HITS";
			this.checkBoxHits.UseVisualStyleBackColor = false;
			// 
			// groupBox3
			// 
			this.groupBox3.BackColor = System.Drawing.SystemColors.Control;
			this.groupBox3.Controls.Add(this.checkBoxProbe16MHz);
			this.groupBox3.Controls.Add(this.checkBoxProbe1MHz);
			this.groupBox3.Controls.Add(this.label5);
			this.groupBox3.Controls.Add(this.numericUpDown1);
			this.groupBox3.Controls.Add(this.checkBoxMonitoring);
			this.groupBox3.Controls.Add(this.checkBoxAutoscan);
			this.groupBox3.Controls.Add(this.checkBoxProbe8MHz);
			this.groupBox3.Controls.Add(this.checkBoxHits);
			this.groupBox3.Controls.Add(this.checkBoxDicom);
			this.groupBox3.Location = new System.Drawing.Point(8, 192);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(160, 344);
			this.groupBox3.TabIndex = 60;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "General";
			// 
			// checkBoxProbe16MHz
			// 
			this.checkBoxProbe16MHz.BackColor = System.Drawing.SystemColors.Control;
			this.checkBoxProbe16MHz.Checked = true;
			this.checkBoxProbe16MHz.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxProbe16MHz.Location = new System.Drawing.Point(16, 216);
			this.checkBoxProbe16MHz.Name = "checkBoxProbe16MHz";
			this.checkBoxProbe16MHz.Size = new System.Drawing.Size(112, 24);
			this.checkBoxProbe16MHz.TabIndex = 7;
			this.checkBoxProbe16MHz.Text = "Probe 16 Mhz";
			this.checkBoxProbe16MHz.UseVisualStyleBackColor = false;
			// 
			// checkBoxProbe1MHz
			// 
			this.checkBoxProbe1MHz.BackColor = System.Drawing.SystemColors.Control;
			this.checkBoxProbe1MHz.Checked = true;
			this.checkBoxProbe1MHz.CheckState = System.Windows.Forms.CheckState.Checked;
			this.checkBoxProbe1MHz.Location = new System.Drawing.Point(16, 152);
			this.checkBoxProbe1MHz.Name = "checkBoxProbe1MHz";
			this.checkBoxProbe1MHz.Size = new System.Drawing.Size(112, 24);
			this.checkBoxProbe1MHz.TabIndex = 6;
			this.checkBoxProbe1MHz.Text = "Probe 1 Mhz";
			this.checkBoxProbe1MHz.UseVisualStyleBackColor = false;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(64, 24);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(64, 24);
			this.label5.TabIndex = 5;
			this.label5.Text = "Probe Num";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// numericUpDown1
			// 
			this.numericUpDown1.Location = new System.Drawing.Point(16, 24);
			this.numericUpDown1.Name = "numericUpDown1";
			this.numericUpDown1.Size = new System.Drawing.Size(40, 20);
			this.numericUpDown1.TabIndex = 4;
			this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem1});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemContents,
            this.menuItemAbout});
			this.menuItem1.Text = "Help";
			// 
			// menuItemContents
			// 
			this.menuItemContents.Index = 0;
			this.menuItemContents.Text = "Contents...";
			this.menuItemContents.Click += new System.EventHandler(this.menuItemContents_Click);
			// 
			// menuItemAbout
			// 
			this.menuItemAbout.Index = 1;
			this.menuItemAbout.Text = "About Authorization Manage...";
			this.menuItemAbout.Click += new System.EventHandler(this.menuItemAbout_Click);
			// 
			// textBoxSerialNumber
			// 
			this.textBoxSerialNumber.Location = new System.Drawing.Point(8, 152);
			this.textBoxSerialNumber.Name = "textBoxSerialNumber";
			this.textBoxSerialNumber.Size = new System.Drawing.Size(192, 20);
			this.textBoxSerialNumber.TabIndex = 62;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(8, 128);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(192, 24);
			this.label6.TabIndex = 63;
			this.label6.Text = "Disk serial number:";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// btnSystemKeyCopy
			// 
			this.btnSystemKeyCopy.Image = ((System.Drawing.Image)(resources.GetObject("btnSystemKeyCopy.Image")));
			this.btnSystemKeyCopy.Location = new System.Drawing.Point(539, 34);
			this.btnSystemKeyCopy.Name = "btnSystemKeyCopy";
			this.btnSystemKeyCopy.Size = new System.Drawing.Size(32, 32);
			this.btnSystemKeyCopy.TabIndex = 64;
			this.btnSystemKeyCopy.TabStop = false;
			this.btnSystemKeyCopy.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnSystemKeyCopy_MouseDown);
			this.btnSystemKeyCopy.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnSystemKeyCopy_MouseUp);
			// 
			// btnProductKeyCopy
			// 
			this.btnProductKeyCopy.Image = ((System.Drawing.Image)(resources.GetObject("btnProductKeyCopy.Image")));
			this.btnProductKeyCopy.Location = new System.Drawing.Point(538, 90);
			this.btnProductKeyCopy.Name = "btnProductKeyCopy";
			this.btnProductKeyCopy.Size = new System.Drawing.Size(32, 32);
			this.btnProductKeyCopy.TabIndex = 65;
			this.btnProductKeyCopy.TabStop = false;
			this.btnProductKeyCopy.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnProductKeyCopy_MouseDown);
			this.btnProductKeyCopy.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnProductKeyCopy_MouseUp);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.cbVersion);
			this.groupBox2.Location = new System.Drawing.Point(584, 32);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(160, 88);
			this.groupBox2.TabIndex = 66;
			this.groupBox2.TabStop = false;
			this.groupBox2.Visible = false;
			// 
			// cbVersion
			// 
			this.cbVersion.Location = new System.Drawing.Point(8, 16);
			this.cbVersion.Name = "cbVersion";
			this.cbVersion.Size = new System.Drawing.Size(144, 40);
			this.cbVersion.TabIndex = 0;
			this.cbVersion.Text = "Versions  before 1.17.6.0";
			this.cbVersion.Visible = false;
			this.cbVersion.CheckedChanged += new System.EventHandler(this.cbVersion_CheckedChanged);
			// 
			// AuthorizationMgrFrm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(762, 539);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.btnProductKeyCopy);
			this.Controls.Add(this.btnSystemKeyCopy);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.textBoxSerialNumber);
			this.Controls.Add(this.textBox6);
			this.Controls.Add(this.textBox7);
			this.Controls.Add(this.textBox9);
			this.Controls.Add(this.textBox8);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.textBox2);
			this.Controls.Add(this.textBox4);
			this.Controls.Add(this.textBox3);
			this.Controls.Add(this.textBox10);
			this.Controls.Add(this.textBox5);
			this.Controls.Add(this.groupBox4);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.labelSperate6);
			this.Controls.Add(this.labelSperate5);
			this.Controls.Add(this.labelSperate4);
			this.Controls.Add(this.labelSperate3);
			this.Controls.Add(this.labelSperate2);
			this.Controls.Add(this.labelSperate1);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.buttonGetInfo);
			this.Controls.Add(this.buttonCancel);
			this.Controls.Add(this.buttonGenProductKey);
			this.Controls.Add(this.buttonGetSysKey);
			this.Controls.Add(this.groupBoxMonitoring);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.KeyPreview = true;
			this.MaximizeBox = false;
			this.Menu = this.mainMenu1;
			this.MinimizeBox = false;
			this.Name = "AuthorizationMgrFrm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Rimed Digi-ONE / Digi-Lite Authorization Manager";
			this.Load += new System.EventHandler(this.AuthorizationMgrFrm_Load);
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AuthorizationMgrFrm_KeyDown);
			this.groupBox1.ResumeLayout(false);
			this.groupBoxMonitoring.ResumeLayout(false);
			this.groupBox4.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnSystemKeyCopy)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProductKeyCopy)).EndInit();
			this.groupBox2.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonGetInfo;
        private System.Windows.Forms.Button buttonGenProductKey;
        private System.Windows.Forms.Button buttonGetSysKey;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label labelSperate6;
        private System.Windows.Forms.Label labelSperate5;
        private System.Windows.Forms.Label labelSperate4;
        private System.Windows.Forms.Label labelSperate3;
        private System.Windows.Forms.Label labelSperate2;
        private System.Windows.Forms.Label labelSperate1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkBoxIntracraniaUnilateral;
        private System.Windows.Forms.CheckBox checkBoxIntracranialBilateral;
        private System.Windows.Forms.CheckBox checkBoxExtracranial;
        private System.Windows.Forms.CheckBox checkBoxMultifrequencyTwoProbes;
        private System.Windows.Forms.CheckBox checkBoxIntraoperative;
        private System.Windows.Forms.CheckBox checkBoxPeripheral;
        private System.Windows.Forms.CheckBox checkBoxMIntracranialUnilateral;
        private System.Windows.Forms.CheckBox checkBoxMIntracranialBilateral;
        private System.Windows.Forms.CheckBox checkBoxMExtracranial;
        private System.Windows.Forms.CheckBox checkBoxMMultifrequencyTwoProbes;
        private System.Windows.Forms.CheckBox checkBoxMIntraoperative;
        private System.Windows.Forms.CheckBox checkBoxMPeripheral;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox checkBoxMonitoringOftPA;
        private System.Windows.Forms.CheckBox checkBoxDicom;
        private System.Windows.Forms.CheckBox checkBoxPFOTest;
        private System.Windows.Forms.GroupBox groupBoxMonitoring;
        private System.Windows.Forms.CheckBox checkBoxEvokedflowtest;
        private System.Windows.Forms.CheckBox checkBoxVMRCO2Reactivity;
        private System.Windows.Forms.CheckBox checkBoxVMRDiamoxtest;

        private System.Windows.Forms.CheckBox checkBoxMonitoring;
        private System.Windows.Forms.CheckBox checkBoxAutoscan;
        private System.Windows.Forms.CheckBox checkBoxProbe8MHz;
        private System.Windows.Forms.CheckBox checkBoxHits;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.MainMenu mainMenu1;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem menuItemContents;
        private System.Windows.Forms.MenuItem menuItemAbout;
        private System.Windows.Forms.CheckBox checkBoxProbe1MHz;
        private System.Windows.Forms.CheckBox checkBoxProbe16MHz;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxSerialNumber;
        private System.Windows.Forms.CheckBox checkBoxVMRBilateralDiamox;
        private System.Windows.Forms.CheckBox checkBoxEvokedflowBilateral;
        private System.Windows.Forms.PictureBox btnSystemKeyCopy;
        private System.Windows.Forms.PictureBox btnProductKeyCopy;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox cbVersion;
        private System.ComponentModel.IContainer components;
    }
}