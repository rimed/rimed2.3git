using System.Reflection;

//
// General Information about the solution
//
[assembly: AssemblyCompany("Rimed ltd.")]
[assembly: AssemblyProduct("TCD Rev6")]
[assembly: AssemblyCopyright("Copyright � Rimed ltd. 2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

//
// General Information about the assembly
//
[assembly: AssemblyTitle("Rimed.TCD.AuthorizationMgr")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]

//
// Version information for an assembly consists of the following four values: Major.Minor.Build.Revision
//
[assembly: AssemblyFileVersion("2.0.0.1")]

// For AssemblyVersionAttribute specify all or default the Revision and Build Numbers by using the '*'.
[assembly: AssemblyVersion("2.0.*")]

