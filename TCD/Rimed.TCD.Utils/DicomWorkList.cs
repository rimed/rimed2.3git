﻿using rzdcxLib;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using Rimed.Framework.Common;

namespace Rimed.TCD.Utils
{
    public class DicomWorkList : Disposable
    {
  		private readonly DCXAPP m_dicomLog;
		private readonly string m_localAETitle;
		private readonly string m_targetAETitle;
		private readonly string m_host;
		private readonly ushort m_port;


        public string DateRange { set; get; }

        public string TimeRange { set; get; }

        public string StationName { set; get; }

        public string Modality { set; get; }
        


        public DicomWorkList()
		{
			m_dicomLog = new DCXAPP {NewInstanceUIDPolicy = new NEW_INSTANCE_CREATION_POLICY()};
		}

        public DicomWorkList(string localAETitle, string targetAETitle, string host, short port)
			: this()
        {
            m_localAETitle = localAETitle;
            m_targetAETitle = targetAETitle;
            m_host = host;
            m_port = (ushort)port;

            // for tests without DICOM server
            //m_host = "www.dicomserver.co.uk"; //213.165.94.158
            //m_port = 104;
        }

        public List<PatientDICOMInfo> BuildQuery()
        {
            DCXOBJIterator it = null;
            DCXREQ req = null;
            DCXOBJ rp = null;
            DCXOBJ sps = null;
            DCXELM el = null;
            DCXOBJIterator spsIt = null; 
            
            try
            {
                rp = new DCXOBJ();
                el = new DCXELM();
                sps = new DCXOBJ();

                // Build the Scheduled procedure Step (SPS) item
                el.Init((int)DICOM_TAGS_ENUM.ScheduledStationAETitle);
                el.Value = StationName;
                sps.insertElement(el);

                el.Init((int)DICOM_TAGS_ENUM.ScheduledProcedureStepStartDate);
                el.Value = DateRange;
                sps.insertElement(el);

                el.Init((int)DICOM_TAGS_ENUM.ScheduledProcedureStepStartTime);
                el.Value = TimeRange;
                sps.insertElement(el);

                el.Init((int)DICOM_TAGS_ENUM.Modality);
                el.Value = Modality;
                sps.insertElement(el);

                // Ask for SPS description
                el.Init((int)DICOM_TAGS_ENUM.ScheduledProcedureStepDescription);
                sps.insertElement(el);

                // Now we put it as an item to sequence
                spsIt = new DCXOBJIterator();
                spsIt.Insert(sps);

                // and add the sequence Scheduled Procedure Step Sequence to the requested procedure (parent) object
                el.Init((int)DICOM_TAGS_ENUM.ScheduledProcedureStepSequence);
                el.Value = spsIt;
                rp.insertElement(el);



                /// Add the Requested Procedure attributes that we would like to get
                el.Init((int)DICOM_TAGS_ENUM.studyInstanceUID);
                rp.insertElement(el);

                el.Init((int)DICOM_TAGS_ENUM.PatientsName);
                rp.insertElement(el);

                el.Init((int)DICOM_TAGS_ENUM.patientID);
                rp.insertElement(el);

                el.Init((int)DICOM_TAGS_ENUM.PatientSex);
                rp.insertElement(el);

                el.Init((int)DICOM_TAGS_ENUM.PatientAddress);
                rp.insertElement(el);

                el.Init((int)DICOM_TAGS_ENUM.PatientsBirthDate);
                rp.insertElement(el);

                // added by Alex Bug #835 v 2.2.3.21 10/02/2016 
                el.Init((int)DICOM_TAGS_ENUM.AccessionNumber);
                rp.insertElement(el);


                // Create the requester object and connect it's callback to our method
                req = new DCXREQ();
                req.OnQueryResponseRecieved += new IDCXREQEvents_OnQueryResponseRecievedEventHandler(OnQueryResponseRecievedAction);

                //rp.Dump("query.txt");

                // send the query command
                it = req.Query(m_localAETitle,
                               m_targetAETitle,
                               m_host,
                               m_port,
                               "1.2.840.10008.5.1.4.31",  /// Modality Worklist SOP Class
                               rp);

                return LoadResultToClass(it);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Query failed: " + ex.Message);
            }
            finally
            {
                ReleaseComObject(req);
                ReleaseComObject(rp);
                ReleaseComObject(it);
                ReleaseComObject(el);
                m_dicomLog.StopLogging();
            }
            return null;
        }

        private List<PatientDICOMInfo> LoadResultToClass(DCXOBJIterator it)
        {
            DCXOBJ currObj = null;
            List<PatientDICOMInfo> result = new List<PatientDICOMInfo>();

            try
            {
                // Iterate over the query results
                for (; !it.AtEnd(); it.Next())
                {
                    PatientDICOMInfo patientInfo = new PatientDICOMInfo();

                    currObj = it.Get();
                    patientInfo.ID = TryGetString(currObj, DICOM_TAGS_ENUM.patientID);
                    string[] pn = TryGetString(currObj, DICOM_TAGS_ENUM.patientName).Split('^');

                    patientInfo.LastName = pn.Length > 0 ? pn[0] : "";
                    patientInfo.FirstName = pn.Length > 1 ? pn[1] : "";
                    patientInfo.MiddleName = pn.Length > 2 ? pn[2] : "";
                    patientInfo.Title = pn.Length > 3 ? pn[3] : "";

                    patientInfo.Sex = TryGetString(currObj, DICOM_TAGS_ENUM.PatientSex);
                    if (TryGetDateTime(currObj, DICOM_TAGS_ENUM.PatientsBirthDate) != null)
                        patientInfo.DofB = TryGetDateTime(currObj, DICOM_TAGS_ENUM.PatientsBirthDate);
                    patientInfo.Address = TryGetString(currObj, DICOM_TAGS_ENUM.PatientAddress);
                    // added by Alex Bug #835 v 2.2.3.21 10/02/2016 
                    patientInfo.AccessionNumber = TryGetString(currObj, DICOM_TAGS_ENUM.AccessionNumber);

                    result.Add(patientInfo);
                }
            }
            finally
            {
                ReleaseComObject(currObj);
            }
            return result;
        }

        private void ReleaseComObject(object o)
        {
            if (o != null)
                Marshal.ReleaseComObject(o);
        }

        public void OnQueryResponseRecievedAction(DCXOBJ obj)
        {
            // Do something
            ReleaseComObject(obj);
        }

        private string TryGetString(DCXOBJ obj, DICOM_TAGS_ENUM tag)
        {
            try
            {
                DCXELM e = obj.getElementByTag((int)tag);
                if (e != null && e.Value != null)
                    return obj.getElementByTag((int)tag).Value.ToString();
                else
                    return "";
            }
            catch (COMException)
            {
                return "";
            }
        }

        private DateTime? TryGetDateTime(DCXOBJ obj, DICOM_TAGS_ENUM tag)
        {
            try
            {
                DCXELM e = obj.getElementByTag((int)tag);
                if (e != null && e.Value != null)
                {
                    return DateTime.ParseExact(obj.getElementByTag((int) tag).Value.ToString(), "yyyy-MM-dd",
                        System.Globalization.CultureInfo.InvariantCulture);

                }
                else
                    return null;
            }
            catch (COMException)
            {
                return DateTime.Now;
            }
        }
    }

    public class PatientDICOMInfo
    {
        public string ID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string Title { get; set; }
        public string Sex { get; set; }
        public DateTime? DofB { get; set; }
        public string Address { get; set; }
        // added by Alex Bug #835 v 2.2.3.21 10/02/2016 
        public string AccessionNumber { get; set; }
    }

    public static class DCXFunctions
    {
        /// <summary>
        /// This has to be called for all the com objects to release the memory!!!
        /// </summary>
        /// <param name="o"></param>

        /// <summary>
        /// Insert empty element into DCXOBJ
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="tag"></param>
        public static void InsertEmptyElement(DCXOBJ obj, DICOM_TAGS_ENUM tag)
        {
            DCXELM el = new DCXELM();

            el.Init((int)tag);

            obj.insertElement(el);

        }

        /// <summary>
        /// Build date range (if IsDate parameter is true) or time range
        /// by given parameters for using in the query
        /// </summary>
        /// <param name="checkDateExact"></param>
        /// <param name="dateExact"></param>
        /// <param name="checkDateFrom"></param>
        /// <param name="dateStartDate"></param>
        /// <param name="checkDateTo"></param>
        /// <param name="dateEndDate"></param>
        /// <param name="IsDate"></param>
        /// <returns></returns>
        public static string BuildDateTimeRange(bool checkDateExact, DateTime dateExact,
                                            bool checkDateFrom, DateTime dateStartDate,
                                            bool checkDateTo, DateTime dateEndDate,
                                            bool IsDate)
        {
            string ResRange = String.Empty;

            string FormatStr = IsDate ? "yyyyMMdd" : "HHmmss";

            if (checkDateExact)
            {
                ResRange = dateExact.ToString(FormatStr);
            }
            else
            {
                if (checkDateTo && checkDateFrom)
                {
                    string dateRange = dateStartDate.ToString(FormatStr) + "-" + dateEndDate.ToString(FormatStr);
                    ResRange = dateRange;
                }
                else if (checkDateFrom && !checkDateTo)
                {
                    string dateRange = dateStartDate.ToString(FormatStr) + "-";
                    ResRange = dateRange;
                }
                else if (!checkDateTo && checkDateFrom)
                {
                    string dateRange = "-" + dateEndDate.ToString(FormatStr);
                    ResRange = dateRange;
                }
            }

            return ResRange;
        }

        /// <summary>
        /// Retrive from DCXOBJ instance given DICOM tag value as string
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="ElementTag"></param>
        /// <returns></returns>
        //public static string GetElementValueAsString(DCXOBJ obj, int ElementTag)
        //{
        //    DCXELM e = obj.GetElement(ElementTag);
        //    if (e == null)
        //    {
        //        //Tag doesn't exist
        //        return String.Empty;
        //    }
        //    if (e.Value != null)
        //        return e.Value.ToString();
        //    else
        //    {
        //        return String.Empty;
        //    }
        //}
    }

}
