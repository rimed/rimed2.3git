using System.Collections.Generic;
using System.Text;
using Rimed.Framework.Common;

namespace Rimed.TCD.Utils
{
    public static class LoggerUserActions
    {
        private const string PREFIX_USER_ACTION						= "USER_ACTION ";

        private const string FMT_USER_ACTION_BV_SELECT				= PREFIX_USER_ACTION + "[select]: {0} BV";
        private const string FMT_USER_ACTION_SPECTRUM_SELECT		= PREFIX_USER_ACTION + "[select]: {0} spectrum";

        private const string FMT_USER_ACTION_CLICK					= PREFIX_USER_ACTION + "[click]: {0}.{1}.";
        private const string FMT_USER_ACTION_DBL_CLICK				= PREFIX_USER_ACTION + "[double-click]: {0}.{1}.";
        private const string FMT_USER_ACTION_DRAG					= PREFIX_USER_ACTION + "[Drag.{0}]: {1}.{2}";
        private const string FMT_USER_ACTION_POP_MENU_SELECT		= PREFIX_USER_ACTION + "[select]: {0}.PopupMenu.{1}";

        private const string FMT_USER_ACTION_CBOX_VAL_CHANGED		= PREFIX_USER_ACTION + "[select]: {0}.{1}.ComboBox.Value = '{2}'";
		private const string FMT_USER_ACTION_SELECT_VAL_CHANGED		= PREFIX_USER_ACTION + "[select]: {0}.{1} Value changed.";


        public class LogField
        {
            public readonly string FieldName;
            public readonly string FieldValue;

            public LogField(string fldName, string fldValue)
            {
                FieldName = fldName;
                FieldValue = fldValue;
            }
        }

        public static FiledLists GetFilesList()
        {
            return new FiledLists();
        }

        public class FiledLists
        {
            private readonly List<LogField> m_list = new List<LogField>();

            public void Add(string name, string value)
            {
                var fld = new LogField(name, value);
                m_list.Add(fld);
            }

            public List<LogField> GetList()
            {
                return m_list;
            }
        }

        public static void MouseClick(string name, string frmName, FiledLists fields = null)
        {
            if (!Logger.IsInfoEnabled)
                return;

            var sb = new StringBuilder(string.Format(FMT_USER_ACTION_CLICK, frmName, name));
            if (fields != null && fields.GetList().Count > 0)
            {
                sb.Append(" Fields:");
                foreach (var t in fields.GetList())
                {
                    sb.Append(string.Format("('{0}'='{1}') ", t.FieldName, t.FieldValue));
                }
            }

            Logger.LogInfo(sb.ToString());
        }

        public static void MouseDoubleClick(string name, string frmName)
        {
            Logger.LogInfo(FMT_USER_ACTION_DBL_CLICK, frmName, name);
        }

        public static void MouseDrag(string name, string frmName, bool isStart)
        {
            Logger.LogInfo(FMT_USER_ACTION_DRAG, (isStart ? "START" : "END"), frmName, name);
        }

        public static void ContextMenuClick(string name, string frmName)
        {
            Logger.LogInfo(FMT_USER_ACTION_POP_MENU_SELECT, frmName, name);
        }

        public static void SelectBloodVesel(string name)
        {
            Logger.LogInfo(FMT_USER_ACTION_BV_SELECT, name);
        }

        public static void ComboBoxSelectedValueChanged(string name, string frmName, string text)
        {
            Logger.LogInfo(FMT_USER_ACTION_CBOX_VAL_CHANGED, frmName, name, text);
        }
        
		public static void SelectedValueChanged(string name, string frmName, string text = null)
		{
			var s = string.Format(FMT_USER_ACTION_SELECT_VAL_CHANGED, frmName, name);
			if (!string.IsNullOrWhiteSpace(s))
			s += text;

			Logger.LogInfo(s);
        }

		public static void SelectSpectrum(string name)
        {
            Logger.LogInfo(FMT_USER_ACTION_SPECTRUM_SELECT, name);
        }
    }
}
