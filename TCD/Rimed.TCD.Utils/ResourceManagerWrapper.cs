﻿using System;
using System.Resources;
using Rimed.Framework.Common;

namespace Rimed.TCD.Utils
{
    public class ResourceManagerWrapper
    {
        private readonly ResourceManager m_resourceManager;

        private ResourceManagerWrapper(ResourceManager mgr)
        {
            m_resourceManager = mgr;
        }

        public string GetString(string key, bool isReturnKeyOnNull = true)
        {
            if (key == null)
                return string.Empty;

            if (m_resourceManager == null)
                return isReturnKeyOnNull ? key : null;
            try
            {
                var val = m_resourceManager.GetString(key);
                if (val == null && isReturnKeyOnNull)
                    return key;

                return val;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                if (isReturnKeyOnNull)
                    return key;
            }

            return null;
        }


        public static ResourceManagerWrapper GetWrapper(ResourceManager mgr)
        {
            return new ResourceManagerWrapper(mgr);
        }
    }
}
