using System;
using System.Windows.Forms;

namespace Rimed.TCD.Utils
{
    /// <summary>Summary description for IndexerPanels.</summary>
    public class PanelsArr
    {
        private readonly Panel[] m_panelsArray;

        public PanelsArr(Panel panel0, Panel panel1)
        {
            m_panelsArray = new Panel[2];
            m_panelsArray[0] = panel0;
            m_panelsArray[1] = panel1;
        }

        public Panel this[int i]
        {
            get
            {
                return m_panelsArray[i % m_panelsArray.Length];
            }
        }

		public int Count { get { return m_panelsArray.Length; } }
    }

    public class DoubleListView : Tuple<ListView, ListView>
    {
        public DoubleListView(ListView list1, ListView list2)
            : base(list1, list2)
        {
        }

        public ListView this[int i]
        {
            get { return (i%2 == 0) ? Item1 : Item2; }
        }

        public ListView FirstList { get { return Item1; } }
        public ListView SecondList { get { return Item2; } }
    }
}
