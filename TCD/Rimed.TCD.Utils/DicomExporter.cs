using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using Rimed.Framework.Common;
using rzdcxLib;

namespace Rimed.TCD.Utils
{
	/// <summary>Summary description for DicomExporter.</summary>
	public class DicomExporter : Disposable
	{
		public const string		DICOM_FILE_EXT				= ".DCM";

		private const string	DEFAULT_STUDY_DESCRIPTION	= "Rimed TCD Bilateral.";

		#region static members
		private static DateTime s_seriesDate = DateTime.Now;
		private static int		s_seriesInstanceNo	= 0;

		private static string	generateUid(UID_TYPE uidType)
		{
			var dcxuid = new DCXUID();
			var uid = dcxuid.CreateUID(uidType);
			Marshal.ReleaseComObject(dcxuid);

			return uid;
		}
		private static void		setNewStudyUid()
		{
			var uid = generateUid(UID_TYPE.UID_TYPE_STUDY);
			SetStudyUid(uid);
		}


		public static int		EchoRetryCount { get; set; }
		public static string	ModelName { get; set; }
		public static string	StudyDescription { get; set; }
		public static string	StudyUid { get; private set; }
		public static int		StudyId { get; private set; }
		public static string	SeriesUid { get; private set; }
		public static int		SeriesNo { get; private set; }

		public static string	GetNewStudyUid()
		{
			string uid;
			try
			{
				uid = generateUid(UID_TYPE.UID_TYPE_STUDY);
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);
				uid = string.Empty;
			}

			return uid;
		}

		public static void		SetNewSeriesUid()
		{
			var uid = generateUid(UID_TYPE.UID_TYPE_SERIES);

			SeriesUid			= uid;
			SeriesNo			= SeriesUid.GetHashCode() & 0x0FFFFFFF;
			s_seriesDate		= DateTime.Now;
			s_seriesInstanceNo	= 0;
		}

		public static void		SetStudyUid(string uid, string studyDesc = null)
		{
			if (string.IsNullOrWhiteSpace(uid))
				return;

			if (string.Compare(uid, StudyUid, StringComparison.InvariantCultureIgnoreCase) == 0)
				return;

			//Logger.Trace("SetStudyUid(): current={0}, New={1}", StudyUid, uid);

			StudyUid			= uid;
			StudyId				= StudyUid.GetHashCode() & 0x0FFFFFFF;
			StudyDescription	= studyDesc;
			SetNewSeriesUid();
		}

		static DicomExporter()
		{
		}
		#endregion


		private DCXAPP m_dicomLog;

		private readonly string m_localAETitle;
		private readonly string m_targetAETitle;
		private readonly string m_host;
		private readonly short m_port;

		public DicomExporter()
		{
			// DICOM logs instance
			m_dicomLog = new DCXAPP();
			m_dicomLog.NewInstanceUIDPolicy = new NEW_INSTANCE_CREATION_POLICY();


			//// Study Instance
			//m_studyUid = new DCXELM();
			//m_studyUid.Init((int) DICOM_TAGS_ENUM.studyInstanceUID);
			//if (string.IsNullOrWhiteSpace(StudyUid))
			//    setNewStudyUid();

			//m_studyUid.Value	= StudyUid;


			//// Series Instance
			//m_seriesUid = new DCXELM();
			//m_seriesUid.Init((int) DICOM_TAGS_ENUM.seriesInstanceUID);
			//if (string.IsNullOrWhiteSpace(SeriesUid))
			//    SetNewSeriesUid();

			//m_seriesUid.Value = SeriesUid;	
		}

		public DicomExporter(string localAETitle, string targetAETitle, string host, short port)
			: this()
		{
			m_localAETitle	= localAETitle;
			m_targetAETitle = targetAETitle;
			m_host			= host;
			m_port			= port;
		}

		/// <summary>Convert the bitmap file into a secondary capture file All files</summary>
		/// <returns>full name of DICOM file</returns>
		public string ConvertAndSave(string bmpFileName, string patientId, string patientName, string patientSex, DateTime patientBirthDate, string patientRefferedBy, DateTime studyDate, string accessionNumber)
		{
			string dcmFile;
			try
			{
				dcmFile = convertAndSave(bmpFileName, patientId, patientName, patientSex, patientBirthDate, patientRefferedBy, studyDate, accessionNumber);
			}
			catch (Exception ex)
			{
				Logger.LogError(ex, string.Format("Image file name = {0}", bmpFileName));
				dcmFile = string.Empty;
			}

			return dcmFile;
		}

		/// <summary>Send list of DICOM files to the server</summary>
		/// <param name="filesToSend">list of DICOM files to send</param>
		/// <param name="localAETitle">local AE title</param>
		/// <param name="targetAETitle">target AE title</param>
		/// <param name="host">server host</param>
		/// <param name="port">server port</param>
		/// <param name="logPath"></param>
		/// <returns>list of failed files</returns>
		public string Send(string filesToSend, string localAETitle, string targetAETitle, string host, short port, string logPath)
		{
			if (string.IsNullOrWhiteSpace(filesToSend))
				return string.Empty;

			string failedList;
			var isLoggingStarted = false;
			DCXREQ dcxReq = null;
			try
			{
				if (!string.IsNullOrWhiteSpace(logPath))
				{
					m_dicomLog.StartLogging(logPath);
					isLoggingStarted = true;
				}

				dcxReq = new DCXREQ();
				string succeededList;
				dcxReq.Send(localAETitle, targetAETitle, host, Convert.ToUInt16(port), filesToSend, out succeededList, out failedList);
			}
			finally
			{
				if (isLoggingStarted)
					m_dicomLog.StopLogging();

				releaseComObject(dcxReq);
			}

			return failedList;
		}

		/// <summary>Send list of DICOM files to the server</summary>
		/// <param name="filesToSend">list of DICOM files to send</param>
		/// <param name="logPath"></param>
		/// <returns>list of failed files</returns>
		private bool send(string filesToSend, string logPath)
		{
			var failFiles = Send(filesToSend, m_localAETitle, m_targetAETitle, m_host, m_port, logPath);
			return string.IsNullOrWhiteSpace(failFiles);
		}


		private bool pingPacsServer(string address, int timeoutMs = 2500)
		{
			if (string.IsNullOrWhiteSpace(address))
				address = string.Empty;

			IPAddress ip;
			if (!IPAddress.TryParse(address, out ip))
			{
				Logger.LogWarning("PACS Server: Ping({0}) FAIL. Server address is invalid.", address);
				return false;
			}

			return pingPacsServer(ip, timeoutMs);

		}

		private bool pingPacsServer(IPAddress address, int timeoutMs = 2500)
		{
			var msg = string.Format("PACS Server: Ping({0})", address);

            var reply = Network.Ping(address, timeoutMs);
		    int counter = 0;

		    while ((reply == null || reply.Status != IPStatus.Success) && counter < 3)
		    {
                reply = Network.Ping(address, timeoutMs);
                counter++;
                Thread.Sleep(100);
            }

		    if (reply == null || reply.Status != IPStatus.Success)
			{
				msg += " - FAIL";
                if (reply != null)
                    msg = string.Format("({0}). RoundTrip={1}, TTL={2}, Size={3} counter={4}", reply.Status, reply.RoundtripTime, reply.Options.Ttl, reply.Buffer.Length, counter);

				Logger.LogWarning(msg);
				return false;
			}

            Logger.LogInfo("{0} - {1}. RoundTrip={2}, TTL={3}, Size={4} counter={5}", msg, reply.Status, reply.RoundtripTime, reply.Options.Ttl, reply.Buffer.Length, counter);

			return true;
		}	

		/// <summary>Convert the bitmap file into a secondary capture file All files</summary>
		/// <returns>full name of DICOM file</returns>
		public bool ConvertAndSend(string bmpFileName, string patientId, string patientName, string patientSex, DateTime patientBirthDate, string patientRefferedBy, DateTime studyDate, string accessionNumber, string logPath)
		{
			if (!pingPacsServer(m_host))
				throw new PingException(string.Format("{0} PACS server (IP: {1}) ping FAIL.", m_targetAETitle, m_host));

			var fileToSend = ConvertAndSave(bmpFileName, patientId, patientName, patientSex, patientBirthDate, patientRefferedBy, studyDate, accessionNumber);
			if (string.IsNullOrWhiteSpace(fileToSend))
				return false;

			var res = send(fileToSend, logPath);

			return res;
		}

		/// <summary>Check PACS server communication.</summary>
		/// <param name="localAETitle"></param>
		/// <param name="targetAETitle">PACS Server Name</param>
		/// <param name="host">PACS Server IP address</param>
		/// <param name="port">PACS Server Port </param>
		/// <param name="retryCount">Retry count, in case of failure/error.</param>
		/// <param name="throwException">Flag throw exception on error (thrown at the last try/retry).</param>
		/// <returns></returns>
		public bool Echo(string localAETitle, string targetAETitle, string host, short port, bool throwException = false, int retryCount = -1)
		{
			if (retryCount < 0)
				retryCount = EchoRetryCount;

			if (!pingPacsServer(host))
				return false;

			if (echo(localAETitle, targetAETitle, host, port, throwException && (retryCount == 0)))
				return true;

			// Retries 
			while (retryCount > 0)
			{
				//NOTE: decrease retryCount MUST precede retry call in order to an exception to be thrown.
				retryCount--;
	
				if (echo(localAETitle, targetAETitle, host, port, throwException && (retryCount == 0)))
					return true;
			} 

			return false;
		}

		public bool Echo()
		{
			return Echo(m_localAETitle, m_targetAETitle, m_host, m_port);
		}

		public bool ConvertToDcmFile(string pdfFile, string dicomDir, int waitTimeout = 60 * 1000)
		{
			if (!File.Exists(pdfFile) || string.IsNullOrWhiteSpace(dicomDir))
				return false;

			var simplename = Path.GetFileNameWithoutExtension(pdfFile);

			var result = false;

			disposeDcmConvertResetEvent();
			m_dcmConvertResetEvent = new ManualResetEvent(false);

			try
			{
				var convertProcess = new Process();
				convertProcess.StartInfo.FileName = Constants.GHOST_SCRIPT_PATH + "gswin32c.exe";
				convertProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
				Cursor.Current = Cursors.WaitCursor;
                // jPEG
				convertProcess.StartInfo.Arguments = string.Format(@"-dBATCH -dNOPAUSE -dNOPLATFONTS -sDEVICE=jpeg -dJPEGQ=100 -sFONTPATH=C:\Windows\Fonts -r300x300 -sOutputFile={0}\{1}_page-%d.jpg {2}", dicomDir, simplename, pdfFile);
                //BMP
                //convertProcess.StartInfo.Arguments = string.Format(@"-dBATCH -dNOPAUSE -dNOPLATFONTS -sDEVICE=bmp16m -sFONTPATH=C:\Windows\Fonts -r300x300 -sOutputFile={0}\{1}_page-%d.bmp {2}",
                //                                                    dicomDir, simplename, pdfFile);
				convertProcess.EnableRaisingEvents = true;
				convertProcess.Exited += new EventHandler(convertToDCMProcess_Exited);
				convertProcess.Start();

				//Wait for convertProcess to terminate
				result = m_dcmConvertResetEvent.WaitOne(waitTimeout);
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);
			}
			finally
			{
				disposeDcmConvertResetEvent();
			}

			return result;
		}

		private ManualResetEvent	m_dcmConvertResetEvent;
		private readonly object		m_lockResetEvent = new object();

		private void disposeDcmConvertResetEvent()
		{
			lock (m_lockResetEvent)
			{
				if (m_dcmConvertResetEvent == null)
					return;

				m_dcmConvertResetEvent.Set();
				m_dcmConvertResetEvent.Dispose();

				m_dcmConvertResetEvent = null;
			}
		}

		private void convertToDCMProcess_Exited(object sender, EventArgs e)
		{
			lock (m_lockResetEvent)
			{
				if (m_dcmConvertResetEvent == null)
					return;

				m_dcmConvertResetEvent.Set();
			}
		}

		private bool echo(string localAETitle, string targetAETitle, string host, short port, bool throwException = false)
		{
			DCXREQ dcxReq = null;
			try
			{
				dcxReq = new DCXREQ();
				dcxReq.Echo(localAETitle, targetAETitle, host, Convert.ToUInt16(port));

				return true;
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);
				if (throwException)
					throw;

				return false;
			}
			finally
			{
				releaseComObject(dcxReq);
				dcxReq = null;
			}
		}

		private string convertAndSave(string bmpFileName, string patientId, string patientName, string patientSex, DateTime patientBirthDate, string patientRefferedBy, DateTime studyDate, string accessionNumber)
		{

			var dcmFileName = bmpFileName + DICOM_FILE_EXT;
			DCXOBJ dcxObj = null;
			DCXELM dcxElement = null;
			try
			{
				dcxObj		= new DCXOBJ();
				dcxElement	= new DCXELM();

				// ===============================================================
				// Insert the image
				// ===============================================================
				dcxObj.SetJpegFrames(bmpFileName);
                //dcxObj.SetBMPFrames(bmpFileName); 

				// ===============================================================
				// Manufacturer Info.
				// ===============================================================

				// Manufacturer
				dcxElement.Init((int)DICOM_TAGS_ENUM.Manufacturer);
				dcxElement.Value = "RIMED LTD";
				dcxObj.insertElement(dcxElement);

				dcxElement.Init((int)DICOM_TAGS_ENUM.ManufacturerModelName);
				dcxElement.Value = string.Format("Rimed {0} Transcranial Doppler System", ModelName); 
				dcxObj.insertElement(dcxElement);

				// SOP Instance UID - The unique id of the image
				dcxElement.Init((int)DICOM_TAGS_ENUM.sopInstanceUID);
				dcxElement.Value = generateUid(UID_TYPE.UID_TYPE_INSTANCE);	
				dcxObj.insertElement(dcxElement);

				// Instance Number - can be zero length but we will number them by the order of the report
				dcxElement.Init((int)DICOM_TAGS_ENUM.InstanceNumber);
				dcxElement.Value = s_seriesInstanceNo++;	//Increment series instance No // intanceNum;
				dcxObj.insertElement(dcxElement);

				// ===============================================================
				// Patient info
				// ===============================================================

				// Patient name
				dcxElement.Init((int)DICOM_TAGS_ENUM.patientName);
				dcxElement.Value = patientName;
				dcxObj.insertElement(dcxElement);

				// Patient ID
				dcxElement.Init((int)DICOM_TAGS_ENUM.patientID);
				dcxElement.Value = patientId;
				dcxObj.insertElement(dcxElement);

				// patient sex
				// Can be M - Male/F - Female/O - Other
				dcxElement.Init((int)DICOM_TAGS_ENUM.PatientSex);
				if (string.Compare("Male", patientSex, StringComparison.InvariantCultureIgnoreCase) == 0)
					dcxElement.Value = "M";
				else if (string.Compare("Female", patientSex, StringComparison.InvariantCultureIgnoreCase) == 0)
					dcxElement.Value = "F";
				else
					dcxElement.Value = "O";
				dcxObj.insertElement(dcxElement);

				// patient birth date
				dcxElement.Init((int)DICOM_TAGS_ENUM.PatientBirthDate);
				dcxElement.Value = patientBirthDate.ToString("yyyyMMdd"); // Format is YYYYMMDD
				dcxObj.insertElement(dcxElement);


				// ===============================================================
				// Study info
				// ===============================================================

				// Study Instance UID
				//dcxObj.insertElement(m_studyUid);

				if (string.IsNullOrWhiteSpace(StudyUid))
					setNewStudyUid();
				dcxElement.Init((int)DICOM_TAGS_ENUM.studyInstanceUID);
				dcxElement.Value = StudyUid;
				dcxObj.insertElement(dcxElement);

				// Study Date
				dcxElement.Init((int)DICOM_TAGS_ENUM.StudyDate);
				dcxElement.Value = studyDate.ToString("yyyyMMdd");
				dcxObj.insertElement(dcxElement);

				// Study Time
				dcxElement.Init((int)DICOM_TAGS_ENUM.StudyTime);
				dcxElement.Value = studyDate.ToString("HHmm");
				dcxObj.insertElement(dcxElement);

				// Study Description
				dcxElement.Init((int)DICOM_TAGS_ENUM.StudyDescription);
				dcxElement.Value = (string.IsNullOrWhiteSpace(StudyDescription) ? DEFAULT_STUDY_DESCRIPTION : StudyDescription);
				dcxObj.insertElement(dcxElement);

				// Study ID - can be zero length (""). We put 1. You can number it as you like.
				dcxElement.Init((int)DICOM_TAGS_ENUM.StudyID);
				dcxElement.Value = StudyId.ToString();		// "1";
				dcxObj.insertElement(dcxElement);

				// This number comes from the RIS. You can put it "" if you don't have the value When integrating with Modality Work list, you will have it
				dcxElement.Init((int)DICOM_TAGS_ENUM.AccessionNumber);
				dcxElement.Value = accessionNumber;
				dcxObj.insertElement(dcxElement);

				// If you know it, put it in.
				dcxElement.Init((int)DICOM_TAGS_ENUM.ReferringPhysicianName);
				dcxElement.Value = patientRefferedBy;
				dcxObj.insertElement(dcxElement);


				// ===============================================================
				// Series info
				// ===============================================================
				// Series Instance UID
				if (string.IsNullOrWhiteSpace(SeriesUid))
					SetNewSeriesUid();
				dcxElement.Init((int)DICOM_TAGS_ENUM.seriesInstanceUID);
				dcxElement.Value = SeriesUid;
				dcxObj.insertElement(dcxElement);


				dcxElement.Init((int)DICOM_TAGS_ENUM.Modality);
				dcxElement.Value = "US"; // Other (was "OT" in the Roni's example)
				dcxObj.insertElement(dcxElement);

				dcxElement.Init((int)DICOM_TAGS_ENUM.ConversionType);
				dcxElement.Value = "DRW";			//DRW   = Drawing
				dcxObj.insertElement(dcxElement);

				dcxElement.Init((int)DICOM_TAGS_ENUM.PatientOrientation);
				dcxElement.Value = "";				// No value
				dcxObj.insertElement(dcxElement);

				dcxElement.Init((int)DICOM_TAGS_ENUM.SeriesNumber);
				dcxElement.Value = SeriesNo;	// s_seriesNumber.ToString();	// Can be with no value. 
				dcxObj.insertElement(dcxElement);

				//Series Date is the same as Study Date
				dcxElement.Init((int)DICOM_TAGS_ENUM.SeriesDate);
				dcxElement.Value = s_seriesDate.ToString("yyyyMMdd");	//studyDate.ToString("yyyyMMdd");
				dcxObj.insertElement(dcxElement);

				//Setting the some other necessary dates
				dcxElement.Init((int)DICOM_TAGS_ENUM.AcquisitionDate);
				dcxElement.Value = studyDate.ToString("yyyyMMdd");
				dcxObj.insertElement(dcxElement);

				dcxElement.Init((int)DICOM_TAGS_ENUM.ContentDate);
				dcxElement.Value = DateTime.Now.ToString("yyyyMMdd");	// studyDate.ToString("yyyyMMdd");
				dcxObj.insertElement(dcxElement);
				//////////////////////////////////////////////
				// Save the file
				//////////////////////////////////////////////

				if (!Directory.Exists(Constants.DICOM_FILES_PATH))
					Directory.CreateDirectory(Constants.DICOM_FILES_PATH);

				dcxObj.saveFile(dcmFileName);
			}
			finally 
			{
				releaseComObject(dcxElement);
				releaseComObject(dcxObj);
			}

			return dcmFileName;
		}

		private void releaseComObject(object comObj)
		{
			if (comObj != null)
				Marshal.ReleaseComObject(comObj);
		}


		protected override void DisposeUnmanagedResources()
		{
			releaseComObject(m_dicomLog);
			m_dicomLog = null;
				
			base.DisposeUnmanagedResources();
		}

	}
}
