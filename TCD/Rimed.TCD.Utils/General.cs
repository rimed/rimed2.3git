﻿using System;

namespace Rimed.TCD.Utils
{
    public static class General
    {
		public enum EApplicationSize
		{
			NONE = 0,
			SCREEN,		//Full screen
			AUTO,
			CUSTOM,
		}

		public enum EAppOperationMode
		{
			DEFAULT = 0,
			PRODUCTION = 1,
			DEBUG = 9,
		}

		public enum EAppProduct
		{
			TCD = 0,
			DIGI_LITE = 1,
			DIGI_ONE = 2,
			REVIEW_STATION = 3
		}

		public static string AppProductName(EAppProduct appProduct)
		{
			if (appProduct == EAppProduct.DIGI_LITE)
				return "Digi-Lite™";

			if (appProduct == EAppProduct.DIGI_ONE)
				return "Digi-One™";

			if (appProduct == EAppProduct.REVIEW_STATION)
				return "ReviewStation™";

			return "TCD™";
		}
		
		public static string GetImageFileNameSuffix(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                return string.Empty;

            var suffix = "_M";

            if (name.EndsWith("-L", StringComparison.InvariantCultureIgnoreCase))
                suffix = "_L";
            else if (name.EndsWith("-R", StringComparison.InvariantCultureIgnoreCase))
                suffix = "_R";

            return suffix;
        }
    }
}
