using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Collections.Generic;

namespace Rimed.TCD.Utils
{
    /// <summary>Summary description for ShellFileOperation.</summary>
    public class ShellFileOperation
    {
        public enum EFileOperations
        {
            FO_MOVE = 0x0001,		// Move the files specified in pFrom to the location specified in pTo. 
            FO_COPY = 0x0002,		// Copy the files specified in the pFrom member to the location specified in the pTo member. 
            FO_DELETE = 0x0003,		// Delete the files specified in pFrom. 
            FO_RENAME = 0x0004		// Rename the file specified in pFrom. You cannot use this flag to rename multiple files with a single function call. Use FO_MOVE instead. 
        }

        private readonly Framework.WinAPI.Shell32.EShellFileOperationFlags m_operationFlags;
        private readonly string m_progressTitle;

        public EFileOperations Operation;
        public IntPtr OwnerWindow;
        public List<string> SourceFiles;
        public List<string> DestFiles;

        public ShellFileOperation()
        {
            // set default properties
            Operation = EFileOperations.FO_COPY;
            OwnerWindow = IntPtr.Zero;
            m_operationFlags = Framework.WinAPI.Shell32.EShellFileOperationFlags.FOF_ALLOWUNDO | Framework.WinAPI.Shell32.EShellFileOperationFlags.FOF_MULTIDESTFILES | Framework.WinAPI.Shell32.EShellFileOperationFlags.FOF_NO_CONNECTED_ELEMENTS | Framework.WinAPI.Shell32.EShellFileOperationFlags.FOF_WANTNUKEWARNING;
            m_progressTitle = "";
        }

        public bool DoOperation()
        {
            var fileOpStruct = new Framework.WinAPI.Shell32.SHFILEOPSTRUCT();

            fileOpStruct.hwnd = OwnerWindow;
            fileOpStruct.wFunc = (uint)Operation;

            var multiSource     = stringArrayToMultiString(SourceFiles);
            var multiDest       = stringArrayToMultiString(DestFiles);
            fileOpStruct.pFrom  = Marshal.StringToHGlobalUni(multiSource);
            fileOpStruct.pTo    = Marshal.StringToHGlobalUni(multiDest);

            fileOpStruct.fFlags = (ushort)m_operationFlags;
            fileOpStruct.lpszProgressTitle = m_progressTitle;
            fileOpStruct.fAnyOperationsAborted = 0;
            fileOpStruct.hNameMappings = IntPtr.Zero;

            int retVal = Framework.WinAPI.Shell32.SHFileOperation(ref fileOpStruct);

            Framework.WinAPI.Shell32.SHChangeNotify((uint)Framework.WinAPI.Shell32.EShellChangeNotificationEvents.SHCNE_ALLEVENTS, (uint)Framework.WinAPI.Shell32.EShellChangeNotificationFlags.SHCNF_DWORD, IntPtr.Zero, IntPtr.Zero);

            if (retVal != 0)
                return false;

            if (fileOpStruct.fAnyOperationsAborted != 0)
                return false;

            return true;
        }

        private string stringArrayToMultiString(List<string> stringArray)
        {
            var multiString = new StringBuilder(string.Empty);

            if (stringArray == null)
                return "";

            for (int i = 0; i < stringArray.Count; i++)
                multiString.Append(stringArray[i] + '\0');

            multiString.Append('\0');

            return multiString.ToString();
        }
    }
}
