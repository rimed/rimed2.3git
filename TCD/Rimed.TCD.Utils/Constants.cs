﻿using Rimed.Framework.Assemblies;

namespace Rimed.TCD.Utils
{
    public static class Constants
    {
		public const int DIGI_LITE_SCREEN_HEIGHT	= 768;//745;
		public const int DIGI_LITE_SCREEN_WIDTH		= 1024;

		private const string APP_DATA_ROOT_PATH = @"C:\Rimed\SRC\TCD2003\";

		public const string APP_LOGS_PATH		= APP_DATA_ROOT_PATH + @"GUI\Logs\";

		public const string RAW_DATA_FOLDER			= @"RawData\";
		public const string GATES_IMAGE_FOLDER		= @"GateImg\";
	    public const string SUMMARY_IMAGE_FOLDER	= @"SummaryImg\";
	    public const string TRENDS_IMAGE_FOLDER		= @"Trends\";
		public const string TEMP_IMAGE_FOLDER		= @"TmpImg\";
		public const string REPLAY_IMAGE_FOLDER		= @"ReplayImg\";
		public const string IP_IMAGE_FOLDER			= @"IpImages\";

		public const string DATA_PATH			= APP_DATA_ROOT_PATH + @"DATA\";

        //Added by Alex Feature #672 v.2.2.3.02 02/07/2015
        public const string VIDEO_PATH = APP_DATA_ROOT_PATH;
        
        public const string IP_IMAGES_PATH		= DATA_PATH + IP_IMAGE_FOLDER;
		public const string GATES_IMAGE_PATH	= DATA_PATH + GATES_IMAGE_FOLDER;
		public const string RAW_DATA_PATH		= DATA_PATH + RAW_DATA_FOLDER;
		public const string SUMMARY_IMG_PATH	= DATA_PATH + SUMMARY_IMAGE_FOLDER;
		public const string TEMP_IMG_PATH		= DATA_PATH + TEMP_IMAGE_FOLDER;
		public const string TRENDS_PATH			= DATA_PATH + TRENDS_IMAGE_FOLDER;
		public const string REPLAY_IMG_PATH		= DATA_PATH + REPLAY_IMAGE_FOLDER;
        public const string DICOM_FILES_PATH    = DATA_PATH + @"DICOM_Export_Files\";

		public static readonly string APP_PATH						= AssemblyInformation.EntryAssembly.Directory;	
		public static readonly string GHOST_SCRIPT_PATH				= APP_PATH + @"\Ghostscript\";
		public static readonly string USER_MANUAL_FILE				= APP_PATH + @"\UserManual\UserManual.htm";
		public static readonly string CONFIG_PATH					= APP_PATH + @"\Images\";

		public static readonly string RIMED_TCD_GUI_EXECUTABLE		= APP_PATH + @"\Rimed.TCD.GUI.exe";
		public static readonly string RIMED_STARTMENU_EXECUTABLE	= APP_PATH + @"\Rimed.TCD.StartMenu.exe";

		public const string NEW_LINE = "\r\n";

		public const string ARCHIVE_FILE_7Z_EXTENTION = ".R7Z";
		public const string ARCHIVE_FILE_ZIP_EXTENTION = ".tmp";

		public static class StudyType
		{
			public const string STUDY_TPA									= "tPA";
			public const string STUDY_AUTOREGULATION						= "Autoregulation";
			public const string STUDY_INTRAOPERATIVE						= "Intraoperative";
			public const string STUDY_EXTRACRANIAL							= "Extracranial";
			public const string STUDY_PERIPHERAL							= "Peripheral";

			private const string STUDY_INTERCRANIAL							= "Intracranial";
			public const string STUDY_INTERCRANIAL_BILATERAL				= STUDY_INTERCRANIAL + " Bilateral";
			public const string STUDY_INTERCRANIAL_UNILATERAL				= STUDY_INTERCRANIAL + " Unilateral";
			public const string STUDY_INTERCRANIAL_FOUR_PROBES				= STUDY_INTERCRANIAL + " Four Probes";

			private const string STUDY_MONITORING							= "Monitoring";
			public const string STUDY_MONITORING_INTRACRANIAL				= STUDY_MONITORING + " Intracranial";
			public const string STUDY_MONITORING_INTRACRANIAL_UNILATERAL	= STUDY_MONITORING + " Intracranial Unilateral";
			public const string STUDY_MONITORING_INTRACRANIAL_BILATERAL		= STUDY_MONITORING + " Intracranial Bilateral";

			public const string STUDY_EVOKED_FLOW							= "Evoked Flow";
			public const string STUDY_EVOKED_FLOW_UNILATERAL				= STUDY_EVOKED_FLOW + " Unilateral";
			public const string STUDY_EVOKED_FLOW_BILATERAL					= STUDY_EVOKED_FLOW + " Bilateral";
			
			public const string STUDY_VMR									= "VMR";
			public const string STUDY_VMR_UNIILATERAL						= STUDY_VMR + " Unilateral";
			public const string STUDY_VMR_BILATERAL							= STUDY_VMR + " Bilateral";

			private const string STUDY_MULTIFREQ							= "Multifrequency";
			public const string STUDY_MULTIFREQ_TWO_PROBES					= STUDY_MULTIFREQ +" Two Probes";
			public const string STUDY_MULTIFREQ_FOUR_PROBES					= STUDY_MULTIFREQ + " Four Probes";
		}
    }
}
