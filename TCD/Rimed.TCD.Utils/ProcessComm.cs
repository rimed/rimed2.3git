using System;
using System.Diagnostics;
using Rimed.Framework.Common;
using Rimed.Framework.WinAPI;

namespace Rimed.TCD.Utils
{
	/// <summary>Summary description for ProcessComm.</summary>
	public static class ProcessComm
	{
		private const string REG_WINDOW_MESSAGE_RIMEDMAINMENU_RESTORE	= "Rimed:MainMenuApplication.Restore";
		private const string REG_WINDOW_MESSAGE_RIMEDMAINMENU_EXIT		= "Rimed:MainMenuApplication.Exit";

		static ProcessComm()
		{
			WinMsgMainMenuApplicationRestore	= User32.RegisterWindowMessage(REG_WINDOW_MESSAGE_RIMEDMAINMENU_RESTORE);
			WinMsgMainMenuApplicationExit		= User32.RegisterWindowMessage(REG_WINDOW_MESSAGE_RIMEDMAINMENU_EXIT);
		}

		public static uint WinMsgMainMenuApplicationRestore { get; private set; }
		public static uint WinMsgMainMenuApplicationExit { get; private set; }


		public static void MainMenuApplicationRestore()
		{
			Logger.LogDebug("ProcessComm.MainMenuApplicationRestore()");
			
			//Send notification message to Rimed.MainMenu application
			User32.SendNotifyMessage(new IntPtr(-1), WinMsgMainMenuApplicationRestore, 0, 0);
		}

		public static void MainMenuApplicationExit()
		{
			Logger.LogDebug("ProcessComm.MainMenuApplicationExit()");

			//Send notification message to Rimed.MainMenu application
			User32.SendNotifyMessage(new IntPtr(-1), WinMsgMainMenuApplicationExit, 0, 0);
		}

        public static void ShutdownWindows()
        {
			Logger.LogDebug("ProcessComm.ShutdownWindows()");
            
            //Determine an OS version
            var os = Environment.OSVersion;
            if (os.Platform == PlatformID.Win32NT)
            {
                switch (os.Version.Major)
                {
					case 5:
		                {
			                //Windows 2000 / Windows XP / Windows Server 2003
			                var a = new Shell32.ShellClass();
			                a.ShutdownWindows();
			                break;
		                }
					default:		//case 6:
		                {
							
			                //Windows Vista / Windows 7
			                Process.Start("shutdown.exe", "-s -t 00");
			                break;
		                }
                }
            }
        }
	}
}
