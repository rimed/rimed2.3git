using System;
using System.Diagnostics;
using System.IO;
using Rimed.Framework.Common;

namespace Rimed.TCD.Utils
{
	/// <summary>7Zip command-line utility wrapper.</summary>
	/// <remarks>
	/// http://www.7-zip.org/
	/// 7-Zip is open source software. 
	/// 7-Zip can be used on any computer, including a computer in a commercial organization. You don't need to register or pay for 7-Zip.
	/// </remarks>
	public static class SevenZip
	{
		private const string FMT_FILE_ADD		= @" a -t7z -m0=lzma -mx=1 -mmt=off ""{0}"" ""{1}"" ";
		private const string FMT_FILE_EXTRACT	= @" e ""{0}"" -o""{1}"" -y -aoa";

		private static readonly string SEVENZIP_EXECUTABLE = Path.Combine(Constants.APP_PATH, "7za.exe");

		public static bool Unpack(string archiveFile, string targetPath, int timeout = 10000)
		{
			Logger.LogDebug("7Z.Unpack('{0}', '{1}', {2})", archiveFile, targetPath, timeout);

			if (string.IsNullOrWhiteSpace(archiveFile) || !File.Exists(archiveFile) || string.IsNullOrWhiteSpace(targetPath))
				return false;

			bool isCompleated = false;
			using (var proc = new Process())
			{
				try
				{
					proc.StartInfo.FileName		= SEVENZIP_EXECUTABLE;			
					proc.StartInfo.WindowStyle	= ProcessWindowStyle.Hidden;
					proc.StartInfo.Arguments	= string.Format(FMT_FILE_EXTRACT, archiveFile, targetPath);
					proc.Start();
					proc.WaitForExit(timeout);

					if (!proc.HasExited)
						Logger.LogError(string.Format("7Zip.Unpack: Execution TIMEOUT ({0} ms). Cmd: {1} {2}", timeout, proc.StartInfo.FileName, proc.StartInfo.Arguments));
					else if (proc.ExitCode != 0)
						Logger.LogError(string.Format("7Zip.Unpack: Execution FAIL (Exit code = {0}). Cmd: {1} {2}", proc.ExitCode, proc.StartInfo.FileName, proc.StartInfo.Arguments));
					else
						isCompleated = true;

				}
				catch (Exception ex)
				{
					Logger.LogError(ex);

					isCompleated = false;
				}

				killProcess(proc);
			}

			return isCompleated;
		}

		public static bool Pack(string source, string target, int timeout = 10000)
		{
			Logger.LogDebug("7Z.Pack('{0}', '{1}', {2})", source, target, timeout);

			if (string.IsNullOrWhiteSpace(source) || !File.Exists(source) || string.IsNullOrWhiteSpace(target))
				return false;

			bool isCompleated = false;

			using (var proc = new Process())
			{
				try
				{
					proc.StartInfo.FileName		= SEVENZIP_EXECUTABLE;			
					proc.StartInfo.WindowStyle	= ProcessWindowStyle.Hidden;
					proc.StartInfo.Arguments	= string.Format(FMT_FILE_ADD, target, source);
					proc.Start();
					proc.WaitForExit(timeout);

					if (!proc.HasExited)
						Logger.LogError(string.Format("7Zip.Add: Execution TIMEOUT ({0} ms). Cmd: {1} {2}", timeout, proc.StartInfo.FileName, proc.StartInfo.Arguments));
					else if (proc.ExitCode != 0)
						Logger.LogError(string.Format("7Zip.Add: Execution FAIL (Exit code = {0}). Cmd: {1} {2}", proc.ExitCode, proc.StartInfo.FileName, proc.StartInfo.Arguments));
					else
						isCompleated = true;
				}
				catch (Exception ex)
				{
					Logger.LogError(ex);

					isCompleated = false;
				}

				killProcess(proc);
			}

			return isCompleated;
		}

		private static void killProcess(Process proc)
		{
			if (proc == null || proc.HasExited)
				return;

			try
			{
				proc.Kill();
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);
			}
		}
	}
}
