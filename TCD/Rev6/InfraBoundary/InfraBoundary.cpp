#include "InfraBoundary.h"

using namespace System::Diagnostics;
using namespace System::Configuration;
using namespace System::Reflection;
using namespace System::Threading;
using namespace System::Runtime::InteropServices; // for class Marshal
using namespace Rimed::TCD::Infrastructure;
using namespace Rimed::TCD::ManagedInfrastructure;

CInfraBoundary::CInfraBoundary(CDebugBoundary ^pDebugBoundary)
{
	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );

	IntPtr iptr = pDebugBoundary->RmdDebugGetNativeObj();
	m_pRmdDebug = (CRmdDebug*)iptr.ToPointer();
	
	m_pInfra = new Infra(m_pRmdDebug);
	DBG_ASSERT(m_pRmdDebug, m_pInfra!=NULL);
	disposed = false;
}

CInfraBoundary::~CInfraBoundary(void)
{
	this->!CInfraBoundary();
	disposed = true;
}

CInfraBoundary::!CInfraBoundary(void)
{
	if(!disposed)
	{
		if (m_pInfra!=NULL) 
		{
			delete m_pInfra;
			m_pInfra = NULL;
		}

		//if (m_pRmdDebug!=NULL) 
		//{
		//	delete m_pRmdDebug;
		//	m_pRmdDebug = NULL;
		//}
	}
}

IntPtr CInfraBoundary::InfraGetNativeObj()
{
	return IntPtr(m_pInfra);
}
