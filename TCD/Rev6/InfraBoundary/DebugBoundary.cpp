/*
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdio.h>
*/

#include "DebugBoundary.h"
using namespace System::Diagnostics;
using namespace System::Configuration;
using namespace System::Reflection;
using namespace System::Threading;
using namespace System::Runtime::InteropServices; // for class Marshal
using namespace Rimed::TCD::Infrastructure;
using namespace Rimed::TCD::ManagedInfrastructure;

CDebugBoundary::CDebugBoundary(System::String ^FileName)
{
	m_pRmdDebug = new CRmdDebug();
	assert(m_pRmdDebug!=NULL);
	disposed  = false;
	DebugBoundaryTraceStart(FileName);
}

CDebugBoundary::~CDebugBoundary()
{
	this->!CDebugBoundary();
	disposed = true;
}

CDebugBoundary::!CDebugBoundary()
{
	if(!disposed)
	{
		if (m_pRmdDebug != NULL) 
			delete m_pRmdDebug;

		m_pRmdDebug = NULL;

		//if(m_pRmdDebug!=NULL) 
		//{			
		//	delete m_pRmdDebug; // Debug delete will be done in infra after trace memory leaks
		//	m_pRmdDebug = NULL;
		//}
	}
}

IntPtr CDebugBoundary::RmdDebugGetNativeObj()
{
	return IntPtr(m_pRmdDebug);
}

void CDebugBoundary::DebugBoundaryTraceStart(System::String ^FileName)
{
	const char *pcFileName;
	IntPtr ipFileName = Marshal::StringToHGlobalAnsi(FileName);
	pcFileName= (char*)ipFileName.ToPointer();
	DBG_ASSERT(m_pRmdDebug,pcFileName != NULL);
	//m_pRmdDebug->DebugTraceStart(pcFileName);
	Marshal::FreeHGlobal( ipFileName ); 
}

//void CDebugBoundary::DebugBoundaryTraceStop()
//{
//	m_pRmdDebug->DebugTraceStop();
//}

unsigned int CDebugBoundary::DebugBoundaryTraceGetLevel()
{
	return m_pRmdDebug->GetDebugLevel();
}

void CDebugBoundary::DebugBoundaryTraceSetLevel(unsigned int val)
{
	m_pRmdDebug->SetDebugLevel(val);
}

void CDebugBoundary::DebugBoundaryTrace(unsigned int Level, String ^Str)
{
	const char* NativeStr;
	NativeStr= (char*)(void*)Marshal::StringToHGlobalAnsi(Str);
	m_pRmdDebug->DbgTrace(Level, NativeStr);
	Marshal::FreeHGlobal((System::IntPtr)(void*)NativeStr);
}

CManagedRmdException::CManagedRmdException(Int32 error)
	:Win32Exception(error)
{
	_originalErrorCode = error;
	InitMessage(); 
}

CManagedRmdException::CManagedRmdException(void)
	:Win32Exception()
{
	InitMessage();
}

CManagedRmdException::CManagedRmdException(UInt32 errorCode, UInt32 originalCode, String^ file, UInt32 line)
	:Win32Exception(errorCode)
{     
	Win32Exception::Source = String::Format("{0}:{1}", file, line);      
	_originalErrorCode =originalCode;
	InitMessage();  
}


CManagedRmdException::CManagedRmdException(CRmdException& RmdExc)
	:Win32Exception(RmdExc.m_dwErrorCode)
{
	Win32Exception::Source = String::Format("{0}:{1}", gcnew String(RmdExc.m_acFileName), RmdExc.m_dwLine);      
	_originalErrorCode = RmdExc.m_dwOriginalCode;
	InitMessage();
}

CManagedRmdException::~CManagedRmdException(void)
{
	;
}

Int32 CManagedRmdException::OriginalErrorCode::get(void)
{
	return _originalErrorCode;
}

String^ CManagedRmdException::Message::get(void)
{
	return _RmdMessage;
}

void CManagedRmdException::InitMessage(void)
{  
	_RmdMessage =  String::Format("(0x{0:X}).", _originalErrorCode);
}

