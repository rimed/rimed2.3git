#pragma once

// IMPORTANT: 
// DO NOT include this file in a managed client program (\clr). For managed client you only need to
// add a reference to the assembly and include the using namespace 'Com::...::Managed'.

#include "RmdDebug.h"

using namespace System;
using namespace System::ComponentModel;
using namespace Rimed::TCD::Infrastructure;

namespace Rimed
{
	namespace TCD 
{ 
	namespace ManagedInfrastructure 
	{
		public ref class CDebugBoundary : IDisposable
		{
		public:
			literal unsigned int MGD_ERROR_TRC = ERROR_TRC;
			literal unsigned int MGD_INFO_TRC = INFO_TRC;
			literal unsigned int MGD_LOUD_TRC = LOUD_TRC;
			literal unsigned int MGD_LOW_LEVEL_TRC_LAST = LOW_LEVEL_TRC_LAST;

			CDebugBoundary(System::String ^FileName);
			~CDebugBoundary();
			!CDebugBoundary();
			void DebugBoundaryTraceStart(System::String ^FileName);
			//void DebugBoundaryTraceStop();
			unsigned int DebugBoundaryTraceGetLevel();
			void DebugBoundaryTraceSetLevel(unsigned int val);
			void DebugBoundaryTrace(unsigned int Level, String ^Str);
			IntPtr RmdDebugGetNativeObj(); // Call only from a boundary object
		private:
			CRmdDebug *m_pRmdDebug;
			bool disposed;
		};

		[Serializable]
		public ref class CManagedRmdException : public Win32Exception
		{
		internal:
			// For this assembly only.
			CManagedRmdException(CRmdException& RmdExc);

		public:
			// For external assemblies.
			CManagedRmdException(UInt32 errorCode, UInt32 originalCode, String^ file, UInt32 line);
			CManagedRmdException(void);
			CManagedRmdException(Int32 error);

			virtual ~CManagedRmdException(void);

			virtual property String^ Message
			{
				String^ get() override; 
			}

			virtual property Int32 OriginalErrorCode
			{
				Int32 get(); 
			}

		protected:
			String^ _RmdMessage;
			Int32 _originalErrorCode;
			void InitMessage(void);
		};
	}
}
}