#pragma once

#include "Infra.h"
#include "DebugBoundary.h"

using namespace System;
using namespace Rimed::TCD::Infrastructure;

namespace Rimed
{
	namespace TCD
{
	namespace ManagedInfrastructure 
	{
		public enum class eRmdErrorCode {
			eRMD_SUCCESS						= RMD_SUCCESS,
			eRMD_FAILED							= RMD_FAILED,
			eRMD_THREAD_TERMINATION_TIMEOUT		= RMD_THREAD_TERMINATION_TIMEOUT,
			eRMD_ERR_FREE_NATIVE_RESOURCE		= RMD_ERR_FREE_NATIVE_RESOURCE,
			eRMD_DOUBLE_FAULT					= RMD_DOUBLE_FAULT,
			eRMD_NOT_ENOUGH_SPACE_ALLOCATION	= RMD_NOT_ENOUGH_SPACE_ALLOCATION,
			eRMD_WRONG_STATE					= RMD_WRONG_STATE,
			eRMD_DEVICE_NOT_READY				= RMD_DEVICE_NOT_READY,
			eRMD_FILE_ERROR						= RMD_FILE_ERROR,
			eRMD_FILE_NOT_FOUND					= RMD_FILE_NOT_FOUND,
			eRMD_WAIT_TIMEOUT_ERR				= RMD_WAIT_TIMEOUT_ERR,
			eRMD_OBJ_NOT_INITIALIZED			= RMD_OBJ_NOT_INITIALIZED,
			eRMD_LIST_EMPTY						= RMD_LIST_EMPTY,
			eRMD_DEVICE_UNKNOWN_TYPE			= RMD_DEVICE_UNKNOWN_TYPE,
			eRMD_INVALID_PARAM					= RMD_INVALID_PARAM,
			eRMD_MORE_DATA						= RMD_MORE_DATA,
			eRMD_DISCARD_UNKNOWN_MESSAGE		= RMD_DISCARD_UNKNOWN_MESSAGE,
			eRMD_INPUT_BUFF_TOO_LARGE			= RMD_INPUT_BUFF_TOO_LARGE,
			eRMD_SM_UNKNOWN_EVENT				= RMD_SM_UNKNOWN_EVENT,
			eRMD_SM_WRONG_EVENT4STATE			= RMD_SM_WRONG_EVENT4STATE,
			eRMD_ABNORMAL_KERNEL_EXECUTION		= RMD_ABNORMAL_KERNEL_EXECUTION,
			eRMD_MESSAGE_NOT_SUPPORTED			= RMD_MESSAGE_NOT_SUPPORTED,
			eRMD_DATA_NOT_READY					= RMD_DATA_NOT_READY,
		};

		public ref class CInfraBoundary : IDisposable
		{
		public:
			CInfraBoundary(CDebugBoundary ^pDebugBoundary);
			~CInfraBoundary();
			!CInfraBoundary();
			IntPtr InfraGetNativeObj();
		private:
			unsigned int m_uLevel;
			bool disposed;
			Infra *m_pInfra;
			CRmdDebug* m_pRmdDebug;
		};
	};
}
}
		