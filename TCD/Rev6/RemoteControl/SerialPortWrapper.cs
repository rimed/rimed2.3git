using System;
using System.Windows.Forms;
using System.Drawing;
using Rimed.Framework.WinAPI;

namespace Rimed.TCD.RemoteControl
{
	public delegate void SerialOperationDelegate(byte operationId);

	public sealed class SerialPortWrapper
    {
        #region Static members
        private static SerialPortWrapper s_serialPortWrapper = null;

        public static bool IsInitiated { get; private set; }

        //ATTENTION: Use on-first-use-creation to enable usage of IsInitiated property when singleton was not initiated
        static public SerialPortWrapper Instance
        {
            get
            {
                if (s_serialPortWrapper == null)
                    s_serialPortWrapper = new SerialPortWrapper();

                return s_serialPortWrapper;
            }
        }
        #endregion

		#region Member fields
		private SerialPort                      m_serialPort;

        private event SerialOperationDelegate   serialOperationEvent;
        #endregion

        public enum EKeys
        {
            DEPTH_UP	    = 65,
            DEPTH_DOWN	    = 66,
            POWER_DOWN      = 68,

            HITS            = 72,
            VOL_UP          = 73,
            FREEZE          = 74,
            WIDTH_UP        = 75,
            RVER            = 76,   // flow direction.
            VOL_DOWN        = 77,
            WIDTH_DOWN      = 79,

            POWER_UP        = 80,
            ESC             = 81,
            GAIN_DOWN       = 82,
            RANGE_DOWN      = 83,
            FUNCTION_4      = 84,
            RETURN          = 85,
            GAIN_UP         = 86,
            RANGE_UP	    = 87,
            FUNCTION_2      = 88,
            PROBE           = 89,

            ZEROLINE_DOWN   = 90,
            FUNCTION_3      = 91,
            FUNCTION_1      = 92,
            CHANNEL         = 93,	// selected gate.
            ZEROLINE_UP     = 94,
        }

		#region Constant
		public const int CURSOR_STEP	= 10;

		public const byte CURSOR_UP	= 117;
		public const byte CURSOR_DOWN	= 100;
		public const byte CURSOR_LEFT	= 108;
		public const byte CURSOR_RIGHT = 114;
		public const byte CURSOR_LEFT_CLICK	= 99;
		public const byte CURSOR_RIGHT_CLICK	= 78;
		public const byte CURSOR_LEFT_CLICK_UP= 67;

		public const byte THUMP_DOWN	= 200;
		public const byte THUMP_UP	= 61;
		#endregion

        #region Ctor(s)
        private SerialPortWrapper()
        {
            //Init();
        }
        #endregion

        #region Public methods
        public void Init(SerialOperationDelegate serialOperationHandler = null)
        {
            if (m_serialPort != null)
                m_serialPort.Dispose();

            m_serialPort = new SerialPort();
            m_serialPort.SerialPortRxCharEvent += serialPortRecive;
            var isOpened = m_serialPort.Activate();

            if (serialOperationHandler != null)
                serialOperationEvent += serialOperationHandler;

			IsInitiated = isOpened;
        }

        public void Close()
		{
            if (m_serialPort != null)
            {
                m_serialPort.Dispose();
                m_serialPort = null;
            }

            IsInitiated = false;
		}

	    public string   Port
	    {
	        get
	        {
	            if (m_serialPort == null)
	                return string.Empty;

	            return m_serialPort.Port;
	        }
	    }

	    public bool     IsOpen
	    {
	        get
	        {
                if (m_serialPort == null)
                    return false;
                
                return m_serialPort.IsOpen;
	        }
	    }

		public bool		IsEnabled
		{
			get { return ! "NOCOM".Equals(Instance.Port, StringComparison.InvariantCultureIgnoreCase); }
		}
        #endregion

        #region Private methods
        private void    serialPortRecive(byte ch)
        {
            var pt = new Point(0, 0);

            switch (ch)
            {
                case CURSOR_UP:
                    pt.Y -= CURSOR_STEP;
                    moveMouse(pt);
                    break;
                case CURSOR_DOWN:
                    pt.Y += CURSOR_STEP;
                    moveMouse(pt);
                    break;
                case CURSOR_LEFT:
                    pt.X -= CURSOR_STEP;
                    moveMouse(pt);
                    break;
                case CURSOR_RIGHT:
                    pt.X += CURSOR_STEP;
                    moveMouse(pt);
                    break;
                case CURSOR_LEFT_CLICK:
                    sendMouseLeftButtonDownMessage();
                    break;
                case CURSOR_LEFT_CLICK_UP:
                    sendMouseLeftButtonUpMessage();
                    break;

                case CURSOR_RIGHT_CLICK:
                    sendMouseRightButtonClickMessage();
                    break;
            }
            if (serialOperationEvent != null)
                serialOperationEvent(ch);
        }

        private void    moveMouse(Point aPt)
        {
            var pt = Cursor.Position;
            pt.X += aPt.X;
            pt.Y += aPt.Y;
            if (pt.X <= 1024 && pt.X >= 0 && pt.Y >= 0 && pt.Y <= 768)
                Cursor.Position = pt;
        }

        private void    sendMouseLeftButtonDownMessage()
        {
            var pt      = Cursor.Position;
            var hWnd    = User32.WindowFromPoint(pt);

            if (hWnd != 0)
            {
                User32.ScreenToClient(hWnd, ref pt);
                var lParam = (uint)(pt.Y * 0x10000 + pt.X);
                User32.PostMessage(hWnd, User32.WinMessage.WM_LBUTTONDOWN, 0, lParam);
				User32.PostMessage(hWnd, User32.WinMessage.WM_LBUTTONUP, 0, lParam);
            }
        }

        private void    sendMouseLeftButtonUpMessage()
		{
			var pt      = Cursor.Position;
			var hWnd    = User32.WindowFromPoint(pt);

            if (hWnd != 0)
            {
                var ctrl = Control.FromHandle((System.IntPtr)hWnd);
                var parent = ctrl.Parent;

                var lParam = (uint)(pt.Y << 16 + pt.X);
				User32.SendMessage(hWnd, User32.WinMessage.WM_LBUTTONUP, User32.WinMessage.WMParam.MK_LBUTTON, lParam);
				User32.SendMessage((uint)parent.Handle, User32.WinMessage.WM_LBUTTONUP, User32.WinMessage.WMParam.MK_LBUTTON, lParam);
            }
		}

		private void    sendMouseRightButtonClickMessage()
		{
			var pt      = Cursor.Position;
            var hWnd    = User32.WindowFromPoint(pt);

            if (hWnd != 0)
			{
                User32.ScreenToClient(hWnd, ref pt);

                var lParam = (uint)(pt.Y * 0x10000 + pt.X);
				User32.PostMessage(hWnd, User32.WinMessage.WM_RBUTTONDOWN, 0, lParam);
				User32.PostMessage(hWnd, User32.WinMessage.WM_RBUTTONUP, 0, lParam);
			}
		}
		#endregion
	}
}
