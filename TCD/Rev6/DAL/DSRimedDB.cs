﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Data.SqlClient;
using Rimed.Framework.Common;

namespace Rimed.TCD.DAL 
{
    public partial class DSRimedDB 
	{
		public const string RIMED_DB_CONNECTION_STRING = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Rimed\SRC\TCD2003\DATA\RimedDB.mdb;Persist Security Info=False";


		private static readonly DSRimedDB	s_dsRimedDB;
        private static readonly dsConfiguation s_dsConfiguation;
        private static readonly dsExamination s_dsExamination1;
        private static readonly dsEvents s_dsEvents;

		private static Version										s_dbVersion = null;

		public static tb_GeneralSettingsDataTable					GeneralSettingsTable { get { return s_dsRimedDB.tb_GeneralSettings; } }
		public static tb_PredefinedStatementsDataTable				PredefinedStatementsDataTable { get { return s_dsRimedDB.tb_PredefinedStatements; } }
        public static dsConfiguation.tb_BasicConfigurationDataTable BasicConfiguration { get { return s_dsConfiguation.tb_BasicConfiguration; } }
        public static dsExamination.tb_ExaminationDataTable Examination { get { return s_dsExamination1.tb_Examination; } }
        public static dsEvents.tb_EventsDataTable Events { get { return s_dsEvents.tb_Events; } }
 

		public static bool		ExeNonQuery(string sql)
		{
			if (string.IsNullOrWhiteSpace(sql))
				return false;

			try
			{
				using (var con = new OleDbConnection(RIMED_DB_CONNECTION_STRING))
				{
					con.Open();
					using (var cmd = new OleDbCommand())
					{
						cmd.Connection = con;
						cmd.CommandText = sql;
						cmd.ExecuteNonQuery();
					}

					con.Close();
				}

				return true;
			}
			catch (Exception ex)
			{
				Logger.LogError(ex, "SQL: " + sql);
				return false;
			}
		}
		
        public static string	ExecuteScalar(string sql)
		{
			if (string.IsNullOrWhiteSpace(sql))
				return null;

			try
			{
				object dbVal;
				using (var con = new OleDbConnection(RIMED_DB_CONNECTION_STRING))
				{
					con.Open();
					using (var cmd = new OleDbCommand())
					{
						cmd.Connection = con;
						cmd.CommandText = sql;
						dbVal = cmd.ExecuteScalar();
					}

					con.Close();
				}

				if (dbVal == null)
					return null;

				return dbVal.ToString();
			}
			catch (Exception ex)
			{
				Logger.LogError(ex, "SQL: " + sql);
				return null;
			}
		}

        //Added by Alex CR #846 v.2.2.3.29 14/04/2016
        public static List<Diamox> ExecuteReader(string sql)
        {
            if (string.IsNullOrWhiteSpace(sql))
                return null;

            try
            {
                object dbVal;
                List<Diamox> res = new List<Diamox>();
                using (var con = new OleDbConnection(RIMED_DB_CONNECTION_STRING))
                {
                    con.Open();
                    using (var cmd = new OleDbCommand())
                    {
                        cmd.Connection = con;
                        cmd.CommandText = sql;
                        using (OleDbDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                res.Add(new Diamox(reader["Study_Index"].ToString(), reader["IndexInStudy"].ToString()));
                            }
                        }
                    }
                    con.Close();
                }

                if (res.Count == 0)
                    return null;

                return res;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "SQL: " + sql);
                return null;
            }
        }
		
	
		static DSRimedDB()
		{
			s_dsRimedDB = new DSRimedDB();
            //Added by Alex v.2.2.2.05 18/12/2014
            s_dsConfiguation = new dsConfiguation();
            //Added by Alex Feature #836 v.2.2.3.24 13/03/2016
            s_dsExamination1 = new dsExamination();
            //Added by Alex CR #846 v.2.2.3.29 14/04/2016
            s_dsEvents = new dsEvents();

			upgradeDB();
		}

        //Added by Alex v.2.2.2.05 18/12/2014
        public static void Init()
        {

        }

        public static Version	DBVersion { get { return getDBVersion(); } }

	    public static string	DBVersionDate
	    {
		    get
		    {
			    var dt =  getDBVersionDate();
			    if (dt.HasValue)
				    return dt.Value.ToString("yyyy.MM.dd");

				return string.Empty;
		    }
	    }


		public static tb_PredefinedStatementsRow[] GetCategoryStatements(int categoryId)
		{
			var filter = string.Format("categoryId={0}", categoryId);
			var orderBy = "Alias";
			var result = PredefinedStatementsDataTable.Select(filter, orderBy);


			var sql = string.Format("SELECT {0}.* FROM {0} WHERE [categoryId]='{1}'", PredefinedStatementsDataTable.TableName, categoryId);
			try
			{
				var s = ExecuteScalar(sql);
				s_dbVersion = string.IsNullOrWhiteSpace(s) ? TableWrapperGeneralSettings.EmptyVersion : new Version(s);
			}
			catch (Exception ex)
			{
				Logger.LogWarning(ex, "DB version is unclear and set to v0.0.0.0.");
				s_dbVersion = TableWrapperGeneralSettings.EmptyVersion;
			}



			return result as tb_PredefinedStatementsRow[];
		}


		private static void		upgradeDB()
		{

			#region DB v2.0.8.0 - Initial version
			if (!IsTableExists(GeneralSettingsTable.TableName))
			{
				createGeneralSettingsTable();
				setDBVersion(new Version(2, 0, 8, 0));		//Corresponded to first app. release version supporting new table (rever number reserved for internal iterations).
			}
			#endregion

			#region DB v2.0.8.1
			var newVer = new Version(2, 0, 8, 1);
			if (getDBVersion() < newVer)
			{
				if (updateBVProbsIndexInBV())
					setDBVersion(newVer);
				else
				{
					LoggedDialog.ShowNotification(string.Format("Rimed DB upgrade to version v{0} FAIL.", newVer), "DB update.");
					return;
				}
			}
			#endregion

			#region DB v2.1.6.0
			newVer = new Version(2, 1, 6, 0);			//Corresponded to first app. release version supporting this change (v2.01.06) (rever number reserved for internal iterations).
			if (getDBVersion() < newVer)
			{
				if (createPredefinedStatementsTable())
					setDBVersion(newVer);
				else
				{
					LoggedDialog.ShowNotification(string.Format("Rimed DB upgrade to version v{0} FAIL.", newVer), "DB update.");
					return;
				}
			}
			#endregion

            #region DB v2.1.7.2
            newVer = new Version(2, 1, 7, 2);
            if (getDBVersion() < newVer)
            {
                if (addExaminationDicomStudyUidColomn())
                    setDBVersion(newVer);
                else
                {
                    LoggedDialog.ShowNotification(string.Format("Rimed DB upgrade to version v{0} FAIL.", newVer), "DB update.");
                    return;
                }
            }
            #endregion

            //Added by Alex v.2.2.2.05 18/12/2014
            #region DB v2.1.7.3
            newVer = new Version(2, 1, 7, 3);
            if (getDBVersion() < newVer)
            {
                if (addAverageCPColomn())
                    setDBVersion(newVer);
                else
                {
                    LoggedDialog.ShowNotification(string.Format("Rimed DB upgrade to version v{0} FAIL.", newVer), "DB update.");
                    return;
                }
            }
            #endregion

            //Added by Alex v.2.2.2.05 18/12/2014
            #region DB v2.1.7.4
            newVer = new Version(2, 1, 7, 4);
            if (getDBVersion() < newVer)
            {
                if (addAutoMuteEnableColomn())
                    setDBVersion(newVer);
                else
                {
                    LoggedDialog.ShowNotification(string.Format("Rimed DB upgrade to version v{0} FAIL.", newVer), "DB update.");
                    return;
                }
            }
            #endregion

            //Added by Alex Feature #836 v.2.2.3.24 13/03/2016
            #region DB v2.1.7.5
            newVer = new Version(2, 1, 7, 5);
            if (getDBVersion() < newVer)
            {
                if (addAccessionNumber())
                    setDBVersion(newVer);
                else
                {
                    LoggedDialog.ShowNotification(string.Format("Rimed DB upgrade to version v{0} FAIL.", newVer), "DB update.");
                    return;
                }
            }
            #endregion

            //Added by Alex CR #846 v.2.2.3.29 14/04/2016
            #region DB v2.1.7.6
            newVer = new Version(2, 1, 7, 6);
            if (getDBVersion() < newVer)
            {
                if (ChangeDiamox())
                    setDBVersion(newVer);
                else
                {
                    LoggedDialog.ShowNotification(string.Format("Rimed DB upgrade to version v{0} FAIL.", newVer), "DB update.");
                    return;
                }
            }
            #endregion

            #region DB v2.1.7.7
            newVer = new Version(2, 1, 7, 7);
            if (getDBVersion() < newVer)
            {
                if (UpgradeDBTo2_1_7_7())
                    setDBVersion(newVer);
                else
                {
                    LoggedDialog.ShowNotification(string.Format("Rimed DB upgrade to version v{0} FAIL.", newVer), "DB update.");
                    return;
                }
            }
            #endregion
            #region DB v2.1.7.8
            newVer = new Version(2, 1, 7, 8);
            if (getDBVersion() < newVer)
            {
                if (UpgradeDBTo2_1_7_8())
                    setDBVersion(newVer);
                else
                {
                    LoggedDialog.ShowNotification(string.Format("Rimed DB upgrade to version v{0} FAIL.", newVer), "DB update.");
                    return;
                }
            }
            #endregion
            #region DB v2.1.7.9
            newVer = new Version(2, 1, 7, 9);
            if (getDBVersion() < newVer)
            {
                if (UpgradeDBTo2_1_7_9())
                    setDBVersion(newVer);
                else
                {
                    LoggedDialog.ShowNotification(string.Format("Rimed DB upgrade to version v{0} FAIL.", newVer), "DB update.");
                    return;
                }
            }

            #endregion
        }

		public static bool IsTableExists(string tableName)
		{
			if (string.IsNullOrWhiteSpace(tableName))
				return false;

			var isExists = false;
			using (var con = new OleDbConnection(RIMED_DB_CONNECTION_STRING))
			{
				con.Open();

				try
				{
					var tables = con.GetSchema("tables").Select(string.Format("TABLE_NAME='{0}'", tableName));	//SELECT COUNT(*) as Exists from MsysObjects WHERE type = 1 AND name = <tableName>
					isExists = (tables.Length > 0);
				}
				catch (Exception ex)
				{
					Logger.LogError(ex);
					return false;
				}
				con.Close();
			}

			return isExists;
		}

		public static bool IsTableColumnExists(string tableName, string columnName)
		{
			if (!IsTableExists(tableName))
				return false;

			bool isExists;
			using (var con = new OleDbConnection(RIMED_DB_CONNECTION_STRING))
			{
				con.Open();

				try
				{
					var cols = con.GetSchema("COLUMNS").Select(string.Format("TABLE_NAME='{0}' AND COLUMN_NAME='{1}'", tableName, columnName));	
					isExists = (cols.Length > 0);
				}
				catch (Exception ex)
				{
					Logger.LogError(ex);
					return false;
				}
				con.Close();
			}

			return isExists;
		}

		#region DB v2.0.8.0
		public static bool			IsGeneralsettingsTableExists()
		{
			if (IsTableExists(GeneralSettingsTable.TableName))
				return true;

			createGeneralSettingsTable();

			return IsTableExists(GeneralSettingsTable.TableName);
		}

		/// <summary>Verify GeneralSettings table exists in DB. If not create it, inserting default values.</summary>
		private static void			createGeneralSettingsTable()
		{
			var tableName = GeneralSettingsTable.TableName;

			Logger.LogInfo("{0} Table is missing. Creating table.", tableName);
			var sql = string.Format("CREATE TABLE {0}([Key] Text(32) NOT NULL PRIMARY KEY, KeyValue Text(32), Description Text(255) )", tableName);
			if (!ExeNonQuery(sql))
			{
				Logger.LogWarning("Fail to create {0} Table.", tableName);
				return;
			}

			// Insert DB version
			setDBVersion(new Version(2, 0, 8, 0));

			// Insert default rows
			sql = string.Format("INSERT INTO {0} ([Key] ,KeyValue, Description) VALUES ('{1}', '{2}', '{3}')", tableName, "ExportInterval", "30", "Interval, in seconds, for clinical parameters export to csv-files");
			ExeNonQuery(sql);

			sql = string.Format("INSERT INTO {0} ([Key] ,KeyValue, Description) VALUES ('{1}', '{2}', '{3}')", tableName, "ExportSeparator", ",", "Column seperator for exported CSV files");
			ExeNonQuery(sql);

			sql = string.Format("INSERT INTO {0} ([Key] ,KeyValue, Description) VALUES ('{1}', '{2}', '{3}')", tableName, "IpImagesPerPage", "0", "Number of IP images per report''s page. Valid values: 0, 1, 2, 4");
			ExeNonQuery(sql);

			sql = string.Format("INSERT INTO {0} ([Key] ,KeyValue, Description) VALUES ('{1}', '{2}', '{3}')", tableName, "OnePageReport", "false", "Simple (vs. full) patient report. Valid value: true, false");
			ExeNonQuery(sql);

			sql = string.Format("INSERT INTO {0} ([Key] ,KeyValue, Description) VALUES ('{1}', '{2}', '{3}')", tableName, "ShowHitsEvent", "true", "Show HITS events on the trend diagram. Valid value: true, false");
			ExeNonQuery(sql);

			sql = string.Format("INSERT INTO {0} ([Key] ,KeyValue, Description) VALUES ('{1}', '{2}', '{3}')", tableName, "StartPlayEvent", "2", "Play start offset, in seconds, from selected event");
			ExeNonQuery(sql);
		}

		private static Version		getDBVersion()
		{
			if (s_dbVersion != null && !TableWrapperGeneralSettings.EmptyVersion.Equals(s_dbVersion))
				return s_dbVersion;

			if (!IsGeneralsettingsTableExists())
			{
				s_dbVersion = TableWrapperGeneralSettings.EmptyVersion;
				return s_dbVersion;
			}

			var sql = string.Format("SELECT KeyValue FROM {0} WHERE [Key]='{1}'", GeneralSettingsTable.TableName, TableWrapperGeneralSettings.KEY_DB_VERSION);
			try
			{
				var s		= ExecuteScalar(sql);
				s_dbVersion = string.IsNullOrWhiteSpace(s) ? TableWrapperGeneralSettings.EmptyVersion : new Version(s);
			}
			catch (Exception ex)
			{
				Logger.LogWarning(ex, "DB version is unclear and set to v0.0.0.0.");
				s_dbVersion = TableWrapperGeneralSettings.EmptyVersion;
			}

			return s_dbVersion;
		}

		private static DateTime?	getDBVersionDate()
		{
			if (!IsGeneralsettingsTableExists())
				return null;

			var sql = string.Format("SELECT KeyValue FROM {0} WHERE [Key]='{1}'", GeneralSettingsTable.TableName, TableWrapperGeneralSettings.KEY_DB_VERSION_DATE);
			var s = ExecuteScalar(sql);
			DateTime dt;
			if (DateTime.TryParse(s, out dt))
				return dt;

			return null; 
		}

		private static void			setDBVersion(Version ver)
		{
			if (DBVersion.Equals(ver))
				return;

			var verDate = DateTime.UtcNow.ToString("yyyy.MM.dd HH:mm:ss");

			var sqlVer = string.Format("UPDATE {0} SET KeyValue='{1}' WHERE [Key]='{2}'", GeneralSettingsTable.TableName, ver, TableWrapperGeneralSettings.KEY_DB_VERSION);
			var sqlVerDate = string.Format("UPDATE {0} SET KeyValue='{1}' WHERE [Key]='{2}'", GeneralSettingsTable.TableName, verDate, TableWrapperGeneralSettings.KEY_DB_VERSION_DATE);
			if (TableWrapperGeneralSettings.EmptyVersion.Equals(DBVersion))
			{
				sqlVer = string.Format("INSERT INTO {0} ([Key] ,KeyValue, Description) VALUES ('{1}', '{2}', '{3}')", GeneralSettingsTable.TableName, TableWrapperGeneralSettings.KEY_DB_VERSION, ver, "Current DB version.");
				sqlVerDate = string.Format("INSERT INTO {0} ([Key] ,KeyValue, Description) VALUES ('{1}', '{2}', '{3}')", GeneralSettingsTable.TableName, TableWrapperGeneralSettings.KEY_DB_VERSION_DATE, verDate, "DB Version update date (UTC)");
			}

			if (ExeNonQuery(sqlVer))
			{
				s_dbVersion = TableWrapperGeneralSettings.EmptyVersion;
				ExeNonQuery(sqlVerDate);

				Logger.LogInfo("DB Version v{0} Registered.", ver);
			}
			else
			{
				Logger.LogWarning("Fail to register DB Version (v{0}).", ver);

			}
		}
		#endregion

		#region DB v2.0.8.1
		private static bool			updateBVProbsIndexInBV()
		{
			var indexInBV = 0;
			var values = new List<Tuple<int, string>>();
			values.Add(Tuple.Create(indexInBV++, "1f25be83-9e59-404b-9daf-c36ae99827eb"));	//ACA
			values.Add(Tuple.Create(indexInBV++, "b9f68bec-3dcc-49df-ae16-846af4d59ff5"));	//MCA
			values.Add(Tuple.Create(indexInBV++, "05d6e684-841f-4475-802f-91f00465a767"));	//Bifurc
			values.Add(Tuple.Create(indexInBV++, "9396fc67-26a0-49c6-a765-effc5c98e694"));	//PCA
			values.Add(Tuple.Create(indexInBV++, "a5da81cc-98b1-481d-9c3d-295e78a9fd4a"));	//AComA
			values.Add(Tuple.Create(indexInBV++, "23f49ddb-7d19-4b36-aa77-4a2b35650985"));	//PComA
			values.Add(Tuple.Create(indexInBV++, "87b6f444-81de-4650-ad98-54e3e65c9356"));	//Opht
			values.Add(Tuple.Create(indexInBV++, "4fc67dcc-5331-487f-8645-88512df09a9b"));	//Siphon
			values.Add(Tuple.Create(indexInBV++, "9bc9368a-4efd-4597-a159-32e51753d040"));	//VA
			values.Add(Tuple.Create(indexInBV++, "527ed7b1-047e-4708-9f5c-9dd706fffeb5"));	//BA
			values.Add(Tuple.Create(indexInBV++, "c40dc74f-aaa7-44f0-806f-c53db6e654f1"));	//CCA
			values.Add(Tuple.Create(indexInBV++, "6787e1ef-3aea-4858-a7d6-b60962793aa0"));	//ICA
			values.Add(Tuple.Create(indexInBV++, "b5f2094b-f5c0-4438-9917-9aacb244d9ae"));	//ECA
			values.Add(Tuple.Create(indexInBV++, "ecc4473b-aada-41a8-b6fd-a591fc73137f"));	//Subclav
			values.Add(Tuple.Create(indexInBV++, "58581338-29f7-474a-9308-c277f9e2af3e"));	//Femoral
			values.Add(Tuple.Create(indexInBV++, "ea90c848-2354-4925-9e97-40f10ce58496"));	//Popliteal
			values.Add(Tuple.Create(indexInBV++, "654520bb-bdc8-4a22-9a75-9d06256dffca"));	//Tibial
			values.Add(Tuple.Create(indexInBV++, "a3806204-2c93-4929-8105-c4558dafbaa9"));	//Dorsalis Pedis
			values.Add(Tuple.Create(indexInBV++, "bbd321da-36f3-4eb8-a5dd-50cfc495925a"));	//Digit
			values.Add(Tuple.Create(indexInBV++, "2215236b-af99-46d8-be64-e6657df5ca76"));	//Radial
			values.Add(Tuple.Create(indexInBV++, "27d7efba-fb0f-4c93-ab3e-27537cc86f86"));	//Penis
			values.Add(Tuple.Create(indexInBV++, "dbc66c3a-af42-41ba-813b-103bd6e8aca7"));	//Ulnar
			values.Add(Tuple.Create(indexInBV++, "106cb77f-36ec-42ab-955c-674595a155a6"));	//Brachial

			//DEBUG: Name=ACA, Index=1f25be83-9e59-404b-9daf-c36ae99827eb, Order=0
			//DEBUG: Name=MCA, Index=b9f68bec-3dcc-49df-ae16-846af4d59ff5, Order=2
			//DEBUG: Name=Bifurc, Index=05d6e684-841f-4475-802f-91f00465a767, Order=4
			//DEBUG: Name=PCA, Index=9396fc67-26a0-49c6-a765-effc5c98e694, Order=6
			//DEBUG: Name=AComA, Index=a5da81cc-98b1-481d-9c3d-295e78a9fd4a, Order=8
			//DEBUG: Name=PComA, Index=23f49ddb-7d19-4b36-aa77-4a2b35650985, Order=10
			//DEBUG: Name=Opht, Index=87b6f444-81de-4650-ad98-54e3e65c9356, Order=12
			//DEBUG: Name=Siphon, Index=4fc67dcc-5331-487f-8645-88512df09a9b, Order=14
			//DEBUG: Name=VA, Index=9bc9368a-4efd-4597-a159-32e51753d040, Order=16
			//DEBUG: Name=BA, Index=527ed7b1-047e-4708-9f5c-9dd706fffeb5, Order=18
			//DEBUG: Name=CCA, Index=c40dc74f-aaa7-44f0-806f-c53db6e654f1, Order=19
			//DEBUG: Name=ICA, Index=6787e1ef-3aea-4858-a7d6-b60962793aa0, Order=21
			//DEBUG: Name=ECA, Index=b5f2094b-f5c0-4438-9917-9aacb244d9ae, Order=23
			//DEBUG: Name=Subclav, Index=ecc4473b-aada-41a8-b6fd-a591fc73137f, Order=25
			//DEBUG: Name=Femoral, Index=58581338-29f7-474a-9308-c277f9e2af3e, Order=27
			//DEBUG: Name=Popliteal, Index=ea90c848-2354-4925-9e97-40f10ce58496, Order=29
			//DEBUG: Name=Tibial, Index=654520bb-bdc8-4a22-9a75-9d06256dffca, Order=31
			//DEBUG: Name=Dorsalis Pedis, Index=a3806204-2c93-4929-8105-c4558dafbaa9, Order=33
			//DEBUG: Name=Digit, Index=bbd321da-36f3-4eb8-a5dd-50cfc495925a, Order=35
			//DEBUG: Name=Radial, Index=2215236b-af99-46d8-be64-e6657df5ca76, Order=37
			//DEBUG: Name=Penis, Index=27d7efba-fb0f-4c93-ab3e-27537cc86f86, Order=39
			//DEBUG: Name=Ulnar, Index=dbc66c3a-af42-41ba-813b-103bd6e8aca7, Order=40
			//DEBUG: Name=Brachial, Index=106cb77f-36ec-42ab-955c-674595a155a6, Order=42

			var res = true;
			foreach (var tuple in values)
			{
				var sql = string.Format("UPDATE tb_BV_Probes SET IndexInBV={0} WHERE Blood_Vessel_Index='{{{1}}}'", tuple.Item1, tuple.Item2.ToUpper());
				res &= ExeNonQuery(sql);
			}

			return res;
		}
		#endregion

		#region DB v2.1.6.0
		/// <summary>Verify GeneralSettings table exists in DB. If not create it, inserting default values.</summary>
		private static bool createPredefinedStatementsTable()
		{
			var tableName = PredefinedStatementsDataTable.TableName;
			var ver = DBVersion;
			if (IsTableExists(tableName))
			{
				Logger.LogWarning("DB v{0}: RimedDB version mismatch. {1} Table already exists.", ver, tableName);
				return true;
			}

			Logger.LogInfo("DB v{0}: RimedDB upgrade. Creating {1} table.", ver, tableName);
			var sql = string.Format("CREATE TABLE [{0}] ([Alias] TEXT(128)  NOT NULL, [CategoryId] INTEGER  NOT NULL, [Statement] MEMO NOT NULL, Primary Key ([Alias], [CategoryId]))", tableName);
			if (ExeNonQuery(sql))
				return true;

			Logger.LogWarning("DB v{0}: Action fail. SQL={1}", ver, sql);
			return false;
		}
		#endregion

		#region DB v2.1.7.2
		/// <summary>Verify GeneralSettings table exists in DB. If not create it, inserting default values.</summary>
		private static bool addExaminationDicomStudyUidColomn()
		{
			const string COLUMN_NAME = "DicomStudyUid";

            // Changed by Alex 26/02/2015 fixed bug #698 v v.2.2.2.20
		    var tableName = "tb_Examination";// RimedDal.Instance.ExaminationsDS.tb_Examination.TableName;
			var ver = DBVersion;
			if (!IsTableExists(tableName))
			{
				Logger.LogWarning("DB v{0}: RimedDB version mismatch. {1} Table is missing.", ver, tableName);
				return false;
			}

			if (IsTableColumnExists(tableName, COLUMN_NAME))
			{
				Logger.LogWarning("DB v{0}: RimedDB version mismatch. {1}.{2} column exists.", ver, tableName, COLUMN_NAME);
				return true;
			}
			
			Logger.LogInfo("DB v{0}: RimedDB upgrade. add {1} column to {2} table.", ver, COLUMN_NAME, tableName);
			var sql = string.Format("ALTER TABLE {0} ADD COLUMN {1} TEXT(255) NULL", tableName, COLUMN_NAME);
			if (ExeNonQuery(sql))
				return true;

			Logger.LogWarning("DB v{0}: Action fail. SQL={1}", ver, sql);
			return false;
		}
        #endregion

        //Added by Alex v.2.2.2.05 18/12/2014
        #region DB v2.1.7.3 
        /// <summary>Verify GeneralSettings table exists in DB. If not create it, inserting default values.</summary>
        private static bool addAverageCPColomn()
        {
            const string COLUMN_NAME = "AveragingCalc";

            var tableName =  BasicConfiguration.TableName;
            var ver = DBVersion;
            if (!IsTableExists(tableName))
            {
                Logger.LogWarning("DB v{0}: RimedDB version mismatch. {1} Table is missing.", ver, tableName);
                return false;
            }

            if (IsTableColumnExists(tableName, COLUMN_NAME))
            {
                Logger.LogWarning("DB v{0}: RimedDB version mismatch. {1}.{2} column exists.", ver, tableName, COLUMN_NAME);
                return true;
            }

            Logger.LogInfo("DB v{0}: RimedDB upgrade. add {1} column to {2} table.", ver, COLUMN_NAME, tableName);
            var sql = string.Format("ALTER TABLE {0} ADD COLUMN {1} BYTE", tableName, COLUMN_NAME);
            if (ExeNonQuery(sql))
            {
                sql = string.Format("UPDATE {0} SET {1} = 1", tableName, COLUMN_NAME);
                ExecuteScalar(sql);
                return true;
            }

            Logger.LogWarning("DB v{0}: Action fail. SQL={1}", ver, sql);
            return false;
        }
        #endregion

        //Added by Alex Feature #755 v.2.2.3.19 04/01/2016
        #region DB v2.1.7.4
        /// <summary>Verify GeneralSettings table exists in DB. If not create it, inserting default values.</summary>
        private static bool addAutoMuteEnableColomn()
        {
            const string COLUMN_NAME = "AutoMuteEnable";

            var tableName = BasicConfiguration.TableName;
            var ver = DBVersion;
            if (!IsTableExists(tableName))
            {
                Logger.LogWarning("DB v{0}: RimedDB version mismatch. {1} Table is missing.", ver, tableName);
                return false;
            }

            if (IsTableColumnExists(tableName, COLUMN_NAME))
            {
                Logger.LogWarning("DB v{0}: RimedDB version mismatch. {1}.{2} column exists.", ver, tableName, COLUMN_NAME);
                return true;
            }

            Logger.LogInfo("DB v{0}: RimedDB upgrade. add {1} column to {2} table.", ver, COLUMN_NAME, tableName);
            var sql = string.Format("ALTER TABLE {0} ADD COLUMN {1} BYTE", tableName, COLUMN_NAME);
            if (ExeNonQuery(sql))
            {
                sql = string.Format("UPDATE {0} SET {1} = 0", tableName, COLUMN_NAME);
                ExecuteScalar(sql);
                return true;
            }

            Logger.LogWarning("DB v{0}: Action fail. SQL={1}", ver, sql);
            return false;
        }

        #endregion

        //Added by Alex Feature #836 v.2.2.3.24 13/03/2016
        #region DB v2.1.7.5
        private static bool addAccessionNumber()
        {
            const string COLUMN_NAME = "AccessionNumber";
            const string COLUMN_NAME1 = "AccessionNumberDICOM";

            var tableName = Examination.TableName;
            var ver = DBVersion;
            if (!IsTableExists(tableName))
            {
                Logger.LogWarning("DB v{0}: RimedDB version mismatch. {1} Table is missing.", ver, tableName);
                return false;
            }

            if (IsTableColumnExists(tableName, COLUMN_NAME))
            {
                Logger.LogWarning("DB v{0}: RimedDB version mismatch. {1}.{2} column exists.", ver, tableName, COLUMN_NAME);
                return true;
            }

            if (IsTableColumnExists(tableName, COLUMN_NAME1))
            {
                Logger.LogWarning("DB v{0}: RimedDB version mismatch. {1}.{2} column exists.", ver, tableName, COLUMN_NAME1);
                return true;
            }

            Logger.LogInfo("DB v{0}: RimedDB upgrade. add {1} columns to {2} table.", ver, COLUMN_NAME + ", " + COLUMN_NAME1, tableName);
            var sql = string.Format("ALTER TABLE {0} ADD COLUMN {1} VARCHAR(20) NULL, {2} VARCHAR(20) NULL", tableName, COLUMN_NAME, COLUMN_NAME1);
            if (ExeNonQuery(sql))
            {
                return true;
            }

            Logger.LogWarning("DB v{0}: Action fail. SQL={1}", ver, sql);
            return false;
        }

        #endregion


        //Added by Alex CR #846 v.2.2.3.29 14/04/2016
        #region DB v2.1.7.6

        public class Diamox
        {
            public Diamox(string study_Index, string index)
            {
                Study_Index = study_Index;
                Index = index;
            }

            public string Study_Index { get; set; }
            public string Index { get; set; }
        }

        private static bool ChangeDiamox()
        {
            var tableName = Events.TableName;
            List<Diamox> listDiamox;
            string s1 = "";

            try
            {
                var sql = string.Format("SELECT {0}.Study_Index, {0}.IndexInStudy FROM {0} WHERE LCase(Name) = 'diamox'", tableName);
                listDiamox = ExecuteReader(sql);
                //Added by Alex CR #846 v.2.2.3.30 18/04/2016
                if (listDiamox == null || listDiamox.Count == 0) // manual changed
                    return true;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, ex.Message);
                return false;
            }

            try
            {
                var sql = string.Format("UPDATE {0} SET {0}.Valid = 0 WHERE LCase(Name) = 'diamox'", tableName);
                var s = ExecuteScalar(sql);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, ex.Message);
                return false;
            }


            try
            {
                foreach (var item in listDiamox)
                {
                    string event_Index = Guid.NewGuid().ToString();
                    var sql = string.Format("INSERT INTO {0} VALUES('{{{1}}}','{{{2}}}','CO2 Stimulation',1,{3},'',-1)", tableName, event_Index, item.Study_Index, item.Index);
                    var s = ExecuteScalar(sql);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, ex.Message);
                return false;
            }

            return true;
        }

        //Added by Brigitte
        #region DB v2.1.7.7
        // update for Lindegaard version
        private static bool UpgradeDBTo2_1_7_7()
        {
             // change "Intracranial Unilateral" study BVs defaults
            if (!RimedDal.Instance.RestoreStudyBvDefaults(RimedDal.IntracranialUnilateral_STUDY_ID))
            {
                return false;
            }

            // change default BV for ICA-R and ICA-L to 2MHz(a1)
            string IcaRInbex = "6787e1ef-3aea-4858-a7d6-b60962793aa0";
            string IcaLInbex = "b7c84c6d-638d-4ee6-8003-466a30bd2487";
            string ProbIndex = "f68faf33-5809-4e8d-9397-e8f02e143b24";
            Logger.LogInfo("DB v{0}: RimedDB upgrade. Update ICA BV default prob in tb_BloodVesselSetup.", DBVersion);
            var sql = string.Format("UPDATE tb_BloodVesselSetup SET Probe='{{{0}}}' WHERE Blood_Vessel_Index='{{{1}}}'", ProbIndex.ToUpper(), IcaRInbex.ToUpper());
            if (!ExeNonQuery(sql))
                return false;

            sql = string.Format("UPDATE tb_BloodVesselSetup SET Probe='{{{0}}}' WHERE Blood_Vessel_Index='{{{1}}}'", ProbIndex.ToUpper(), IcaLInbex.ToUpper());
            if (!ExeNonQuery(sql))
                return false;

            Logger.LogInfo("DB v{0}: RimedDB upgrade. Add Lindegaard Ratio fields to tb_Examination.", DBVersion);
            sql = "ALTER TABLE tb_Examination ADD COLUMN LR_McaLMean DOUBLE DEFAULT 0, LR_McaRMean DOUBLE DEFAULT 0, LR_IcaLMean DOUBLE DEFAULT 0, LR_IcaRMean DOUBLE DEFAULT 0, LR_ManualCalc YESNO DEFAULT 0";
            if (!ExeNonQuery(sql))
            {
                return false;
            }

            return true;
        }

        #endregion
        #region DB v2.1.7.8

        private static bool UpgradeDBTo2_1_7_8()
        {
            // fix 2_1_7_7 "Intracranial Unilateral" study BVs defaults order
            if (!RimedDal.Instance.RestoreStudyBvDefaults(RimedDal.IntracranialUnilateral_STUDY_ID))
            {
                return false;
            }
            return true;
        }
        #endregion
        #region DB v2.1.7.9
        private static bool UpgradeDBTo2_1_7_9()
        {
            // add ReportVersion to tb_GateExamination with default value of 2 (new data)
            Logger.LogInfo("DB v{0}: RimedDB upgrade. Report version to tb_GateExamination.", DBVersion);
            var sql = "ALTER TABLE tb_GateExamination ADD COLUMN ReportVersion INTEGER NOT NULL DEFAULT '1'";
            if (!ExeNonQuery(sql))
            {
                return false;
            }
            // set value of current data to 1
            sql = "UPDATE tb_GateExamination SET ReportVersion='1'";
            if (!ExeNonQuery(sql))
                return false;

            return true;
        }

 /*       private static bool AddCp2EventExamination()
        {
            Logger.LogInfo("DB v{0}: RimedDB upgrade. Add clinical parameters to tb_ExaminationEvent.", DBVersion);
            var sql = "ALTER TABLE tb_ExaminationEvent ADD COLUMN PeakTopVal DOUBLE DEFAULT 0, PeakBottomVal DOUBLE DEFAULT 0, MeanTopVal DOUBLE DEFAULT 0, MeanBottomVal DOUBLE DEFAULT 0, DVTopVal DOUBLE DEFAULT 0, DVBottomVal DOUBLE DEFAULT 0, PITopVal DOUBLE DEFAULT 0, PIBottomVal DOUBLE DEFAULT 0,  SWTopVal DOUBLE DEFAULT 0, SWBottomVal DOUBLE DEFAULT 0, SDTopVal DOUBLE DEFAULT 0, SDBottomVal DOUBLE DEFAULT 0, RITopVal DOUBLE DEFAULT 0, RIBottomVal DOUBLE DEFAULT 0, ModeTopVal DOUBLE DEFAULT 0, ModeBottomVal DOUBLE DEFAULT 0, AvrgTopVal DOUBLE DEFAULT 0, AvrgBottomVal DOUBLE DEFAULT 0, HitsTopVal DOUBLE DEFAULT 0, HitsBottomVal DOUBLE DEFAULT 0, HITrTopVal DOUBLE DEFAULT 0, HITrBottomVal DOUBLE DEFAULT 0";
            if (!ExeNonQuery(sql))
            {
                return false;
            }
            // set value of current data to 0
            sql = "UPDATE tb_ExaminationEvent SET PeakTopVal='0', PeakBottomVal='0', MeanTopVal='0', MeanBottomVal='0', DVTopVal='0', DVBottomVal='0', PITopVal='0', PIBottomVal='0',  SWTopVal='0', SWBottomVal='0', SDTopVal='0', SDBottomVal='0', RITopVal='0', RIBottomVal='0', ModeTopVal='0', ModeBottomVal='0', AvrgTopVal='0', AvrgBottomVal='0', HitsTopVal='0', HitsBottomVal='0', HITrTopVal='0', HITrBottomVal='0'";
            if (!ExeNonQuery(sql))
                return false;

            return true;
        }
  * */
        #endregion

        #endregion

    }
}


namespace Rimed.TCD.DAL.DSRimedDBTableAdapters {
    
    
    public partial class tb_PredefinedStatementsTableAdapter {
    }
}
