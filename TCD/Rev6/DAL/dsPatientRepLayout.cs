﻿//------------------------------------------------------------------------------
// <autogenerated>
//     This code was generated by a tool.
//     Runtime Version: 1.1.4322.2407
//
//     Changes to this file may cause incorrect behavior and will be lost if 
//     the code is regenerated.
// </autogenerated>
//------------------------------------------------------------------------------

namespace TCD2003.DB {
    using System;
    using System.Data;
    using System.Xml;
    using System.Runtime.Serialization;
    
    
    [Serializable()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Diagnostics.DebuggerStepThrough()]
    [System.ComponentModel.ToolboxItem(true)]
    public class dsPatientRepLayout : DataSet {
        
        private tb_PatientRepLayoutDataTable tabletb_PatientRepLayout;
        
        public dsPatientRepLayout() {
            this.InitClass();
            System.ComponentModel.CollectionChangeEventHandler schemaChangedHandler = new System.ComponentModel.CollectionChangeEventHandler(this.SchemaChanged);
            this.Tables.CollectionChanged += schemaChangedHandler;
            this.Relations.CollectionChanged += schemaChangedHandler;
        }
        
        protected dsPatientRepLayout(SerializationInfo info, StreamingContext context) {
            string strSchema = ((string)(info.GetValue("XmlSchema", typeof(string))));
            if ((strSchema != null)) {
                DataSet ds = new DataSet();
                ds.ReadXmlSchema(new XmlTextReader(new System.IO.StringReader(strSchema)));
                if ((ds.Tables["tb_PatientRepLayout"] != null)) {
                    this.Tables.Add(new tb_PatientRepLayoutDataTable(ds.Tables["tb_PatientRepLayout"]));
                }
                this.DataSetName = ds.DataSetName;
                this.Prefix = ds.Prefix;
                this.Namespace = ds.Namespace;
                this.Locale = ds.Locale;
                this.CaseSensitive = ds.CaseSensitive;
                this.EnforceConstraints = ds.EnforceConstraints;
                this.Merge(ds, false, System.Data.MissingSchemaAction.Add);
                this.InitVars();
            }
            else {
                this.InitClass();
            }
            this.GetSerializationData(info, context);
            System.ComponentModel.CollectionChangeEventHandler schemaChangedHandler = new System.ComponentModel.CollectionChangeEventHandler(this.SchemaChanged);
            this.Tables.CollectionChanged += schemaChangedHandler;
            this.Relations.CollectionChanged += schemaChangedHandler;
        }
        
        [System.ComponentModel.Browsable(false)]
        [System.ComponentModel.DesignerSerializationVisibilityAttribute(System.ComponentModel.DesignerSerializationVisibility.Content)]
        public tb_PatientRepLayoutDataTable tb_PatientRepLayout {
            get {
                return this.tabletb_PatientRepLayout;
            }
        }
        
        public override DataSet Clone() {
            dsPatientRepLayout cln = ((dsPatientRepLayout)(base.Clone()));
            cln.InitVars();
            return cln;
        }
        
        protected override bool ShouldSerializeTables() {
            return false;
        }
        
        protected override bool ShouldSerializeRelations() {
            return false;
        }
        
        protected override void ReadXmlSerializable(XmlReader reader) {
            this.Reset();
            DataSet ds = new DataSet();
            ds.ReadXml(reader);
            if ((ds.Tables["tb_PatientRepLayout"] != null)) {
                this.Tables.Add(new tb_PatientRepLayoutDataTable(ds.Tables["tb_PatientRepLayout"]));
            }
            this.DataSetName = ds.DataSetName;
            this.Prefix = ds.Prefix;
            this.Namespace = ds.Namespace;
            this.Locale = ds.Locale;
            this.CaseSensitive = ds.CaseSensitive;
            this.EnforceConstraints = ds.EnforceConstraints;
            this.Merge(ds, false, System.Data.MissingSchemaAction.Add);
            this.InitVars();
        }
        
        protected override System.Xml.Schema.XmlSchema GetSchemaSerializable() {
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            this.WriteXmlSchema(new XmlTextWriter(stream, null));
            stream.Position = 0;
            return System.Xml.Schema.XmlSchema.Read(new XmlTextReader(stream), null);
        }
        
        internal void InitVars() {
            this.tabletb_PatientRepLayout = ((tb_PatientRepLayoutDataTable)(this.Tables["tb_PatientRepLayout"]));
            if ((this.tabletb_PatientRepLayout != null)) {
                this.tabletb_PatientRepLayout.InitVars();
            }
        }
        
        private void InitClass() {
            this.DataSetName = "dsPatientRepLayout";
            this.Prefix = "";
            this.Namespace = "http://tempuri.org/dsPatientRepLayout.xsd";
            this.Locale = new System.Globalization.CultureInfo("en-US");
            this.CaseSensitive = false;
            this.EnforceConstraints = true;
            this.tabletb_PatientRepLayout = new tb_PatientRepLayoutDataTable();
            this.Tables.Add(this.tabletb_PatientRepLayout);
        }
        
        private bool ShouldSerializetb_PatientRepLayout() {
            return false;
        }
        
        private void SchemaChanged(object sender, System.ComponentModel.CollectionChangeEventArgs e) {
            if ((e.Action == System.ComponentModel.CollectionChangeAction.Remove)) {
                this.InitVars();
            }
        }
        
        public delegate void tb_PatientRepLayoutRowChangeEventHandler(object sender, tb_PatientRepLayoutRowChangeEvent e);
        
        [System.Diagnostics.DebuggerStepThrough()]
        public class tb_PatientRepLayoutDataTable : DataTable, System.Collections.IEnumerable {
            
            private DataColumn columnIndex;
            
            private DataColumn columnDateDisplay;
            
            private DataColumn columnHospitalDetails;
            
            private DataColumn columnHospitalLogo;
            
            private DataColumn columnTitleDisplay;
            
            private DataColumn columnPatientDetails;
            
            private DataColumn columnPatientHistory;
            
            private DataColumn columnExaminationHistory;
            
            private DataColumn columnLayout;
            
            private DataColumn columnNormalValues;
            
            private DataColumn columnIndication;
            
            private DataColumn columnInterpretation;
            
            private DataColumn columnSignature;
            
            private DataColumn columnPreview;
            
            internal tb_PatientRepLayoutDataTable() : 
                    base("tb_PatientRepLayout") {
                this.InitClass();
            }
            
            internal tb_PatientRepLayoutDataTable(DataTable table) : 
                    base(table.TableName) {
                if ((table.CaseSensitive != table.DataSet.CaseSensitive)) {
                    this.CaseSensitive = table.CaseSensitive;
                }
                if ((table.Locale.ToString() != table.DataSet.Locale.ToString())) {
                    this.Locale = table.Locale;
                }
                if ((table.Namespace != table.DataSet.Namespace)) {
                    this.Namespace = table.Namespace;
                }
                this.Prefix = table.Prefix;
                this.MinimumCapacity = table.MinimumCapacity;
                this.DisplayExpression = table.DisplayExpression;
            }
            
            [System.ComponentModel.Browsable(false)]
            public int Count {
                get {
                    return this.Rows.Count;
                }
            }
            
            internal DataColumn IndexColumn {
                get {
                    return this.columnIndex;
                }
            }
            
            internal DataColumn DateDisplayColumn {
                get {
                    return this.columnDateDisplay;
                }
            }
            
            internal DataColumn HospitalDetailsColumn {
                get {
                    return this.columnHospitalDetails;
                }
            }
            
            internal DataColumn HospitalLogoColumn {
                get {
                    return this.columnHospitalLogo;
                }
            }
            
            internal DataColumn TitleDisplayColumn {
                get {
                    return this.columnTitleDisplay;
                }
            }
            
            internal DataColumn PatientDetailsColumn {
                get {
                    return this.columnPatientDetails;
                }
            }
            
            internal DataColumn PatientHistoryColumn {
                get {
                    return this.columnPatientHistory;
                }
            }
            
            internal DataColumn ExaminationHistoryColumn {
                get {
                    return this.columnExaminationHistory;
                }
            }
            
            internal DataColumn LayoutColumn {
                get {
                    return this.columnLayout;
                }
            }
            
            internal DataColumn NormalValuesColumn {
                get {
                    return this.columnNormalValues;
                }
            }
            
            internal DataColumn IndicationColumn {
                get {
                    return this.columnIndication;
                }
            }
            
            internal DataColumn InterpretationColumn {
                get {
                    return this.columnInterpretation;
                }
            }
            
            internal DataColumn SignatureColumn {
                get {
                    return this.columnSignature;
                }
            }
            
            internal DataColumn PreviewColumn {
                get {
                    return this.columnPreview;
                }
            }
            
            public tb_PatientRepLayoutRow this[int index] {
                get {
                    return ((tb_PatientRepLayoutRow)(this.Rows[index]));
                }
            }
            
            public event tb_PatientRepLayoutRowChangeEventHandler tb_PatientRepLayoutRowChanged;
            
            public event tb_PatientRepLayoutRowChangeEventHandler tb_PatientRepLayoutRowChanging;
            
            public event tb_PatientRepLayoutRowChangeEventHandler tb_PatientRepLayoutRowDeleted;
            
            public event tb_PatientRepLayoutRowChangeEventHandler tb_PatientRepLayoutRowDeleting;
            
            public void Addtb_PatientRepLayoutRow(tb_PatientRepLayoutRow row) {
                this.Rows.Add(row);
            }
            
            public tb_PatientRepLayoutRow Addtb_PatientRepLayoutRow(bool DateDisplay, bool HospitalDetails, bool HospitalLogo, bool TitleDisplay, int PatientDetails, bool PatientHistory, bool ExaminationHistory, System.Byte Layout, bool NormalValues, bool Indication, bool Interpretation, bool Signature, bool Preview) {
                tb_PatientRepLayoutRow rowtb_PatientRepLayoutRow = ((tb_PatientRepLayoutRow)(this.NewRow()));
                rowtb_PatientRepLayoutRow.ItemArray = new object[] {
                        null,
                        DateDisplay,
                        HospitalDetails,
                        HospitalLogo,
                        TitleDisplay,
                        PatientDetails,
                        PatientHistory,
                        ExaminationHistory,
                        Layout,
                        NormalValues,
                        Indication,
                        Interpretation,
                        Signature,
                        Preview};
                this.Rows.Add(rowtb_PatientRepLayoutRow);
                return rowtb_PatientRepLayoutRow;
            }
            
            public tb_PatientRepLayoutRow FindByIndex(int Index) {
                return ((tb_PatientRepLayoutRow)(this.Rows.Find(new object[] {
                            Index})));
            }
            
            public System.Collections.IEnumerator GetEnumerator() {
                return this.Rows.GetEnumerator();
            }
            
            public override DataTable Clone() {
                tb_PatientRepLayoutDataTable cln = ((tb_PatientRepLayoutDataTable)(base.Clone()));
                cln.InitVars();
                return cln;
            }
            
            protected override DataTable CreateInstance() {
                return new tb_PatientRepLayoutDataTable();
            }
            
            internal void InitVars() {
                this.columnIndex = this.Columns["Index"];
                this.columnDateDisplay = this.Columns["DateDisplay"];
                this.columnHospitalDetails = this.Columns["HospitalDetails"];
                this.columnHospitalLogo = this.Columns["HospitalLogo"];
                this.columnTitleDisplay = this.Columns["TitleDisplay"];
                this.columnPatientDetails = this.Columns["PatientDetails"];
                this.columnPatientHistory = this.Columns["PatientHistory"];
                this.columnExaminationHistory = this.Columns["ExaminationHistory"];
                this.columnLayout = this.Columns["Layout"];
                this.columnNormalValues = this.Columns["NormalValues"];
                this.columnIndication = this.Columns["Indication"];
                this.columnInterpretation = this.Columns["Interpretation"];
                this.columnSignature = this.Columns["Signature"];
                this.columnPreview = this.Columns["Preview"];
            }
            
            private void InitClass() {
                this.columnIndex = new DataColumn("Index", typeof(int), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnIndex);
                this.columnDateDisplay = new DataColumn("DateDisplay", typeof(bool), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnDateDisplay);
                this.columnHospitalDetails = new DataColumn("HospitalDetails", typeof(bool), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnHospitalDetails);
                this.columnHospitalLogo = new DataColumn("HospitalLogo", typeof(bool), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnHospitalLogo);
                this.columnTitleDisplay = new DataColumn("TitleDisplay", typeof(bool), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnTitleDisplay);
                this.columnPatientDetails = new DataColumn("PatientDetails", typeof(int), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnPatientDetails);
                this.columnPatientHistory = new DataColumn("PatientHistory", typeof(bool), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnPatientHistory);
                this.columnExaminationHistory = new DataColumn("ExaminationHistory", typeof(bool), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnExaminationHistory);
                this.columnLayout = new DataColumn("Layout", typeof(System.Byte), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnLayout);
                this.columnNormalValues = new DataColumn("NormalValues", typeof(bool), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnNormalValues);
                this.columnIndication = new DataColumn("Indication", typeof(bool), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnIndication);
                this.columnInterpretation = new DataColumn("Interpretation", typeof(bool), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnInterpretation);
                this.columnSignature = new DataColumn("Signature", typeof(bool), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnSignature);
                this.columnPreview = new DataColumn("Preview", typeof(bool), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnPreview);
                this.Constraints.Add(new UniqueConstraint("dsPatientRepLayoutKey1", new DataColumn[] {
                                this.columnIndex}, true));
                this.columnIndex.AutoIncrement = true;
                this.columnIndex.AllowDBNull = false;
                this.columnIndex.Unique = true;
            }
            
            public tb_PatientRepLayoutRow Newtb_PatientRepLayoutRow() {
                return ((tb_PatientRepLayoutRow)(this.NewRow()));
            }
            
            protected override DataRow NewRowFromBuilder(DataRowBuilder builder) {
                return new tb_PatientRepLayoutRow(builder);
            }
            
            protected override System.Type GetRowType() {
                return typeof(tb_PatientRepLayoutRow);
            }
            
            protected override void OnRowChanged(DataRowChangeEventArgs e) {
                base.OnRowChanged(e);
                if ((this.tb_PatientRepLayoutRowChanged != null)) {
                    this.tb_PatientRepLayoutRowChanged(this, new tb_PatientRepLayoutRowChangeEvent(((tb_PatientRepLayoutRow)(e.Row)), e.Action));
                }
            }
            
            protected override void OnRowChanging(DataRowChangeEventArgs e) {
                base.OnRowChanging(e);
                if ((this.tb_PatientRepLayoutRowChanging != null)) {
                    this.tb_PatientRepLayoutRowChanging(this, new tb_PatientRepLayoutRowChangeEvent(((tb_PatientRepLayoutRow)(e.Row)), e.Action));
                }
            }
            
            protected override void OnRowDeleted(DataRowChangeEventArgs e) {
                base.OnRowDeleted(e);
                if ((this.tb_PatientRepLayoutRowDeleted != null)) {
                    this.tb_PatientRepLayoutRowDeleted(this, new tb_PatientRepLayoutRowChangeEvent(((tb_PatientRepLayoutRow)(e.Row)), e.Action));
                }
            }
            
            protected override void OnRowDeleting(DataRowChangeEventArgs e) {
                base.OnRowDeleting(e);
                if ((this.tb_PatientRepLayoutRowDeleting != null)) {
                    this.tb_PatientRepLayoutRowDeleting(this, new tb_PatientRepLayoutRowChangeEvent(((tb_PatientRepLayoutRow)(e.Row)), e.Action));
                }
            }
            
            public void Removetb_PatientRepLayoutRow(tb_PatientRepLayoutRow row) {
                this.Rows.Remove(row);
            }
        }
        
        [System.Diagnostics.DebuggerStepThrough()]
        public class tb_PatientRepLayoutRow : DataRow {
            
            private tb_PatientRepLayoutDataTable tabletb_PatientRepLayout;
            
            internal tb_PatientRepLayoutRow(DataRowBuilder rb) : 
                    base(rb) {
                this.tabletb_PatientRepLayout = ((tb_PatientRepLayoutDataTable)(this.Table));
            }
            
            public int Index {
                get {
                    return ((int)(this[this.tabletb_PatientRepLayout.IndexColumn]));
                }
                set {
                    this[this.tabletb_PatientRepLayout.IndexColumn] = value;
                }
            }
            
            public bool DateDisplay {
                get {
                    try {
                        return ((bool)(this[this.tabletb_PatientRepLayout.DateDisplayColumn]));
                    }
                    catch (InvalidCastException e) {
                        throw new StrongTypingException("Cannot get value because it is DBNull.", e);
                    }
                }
                set {
                    this[this.tabletb_PatientRepLayout.DateDisplayColumn] = value;
                }
            }
            
            public bool HospitalDetails {
                get {
                    try {
                        return ((bool)(this[this.tabletb_PatientRepLayout.HospitalDetailsColumn]));
                    }
                    catch (InvalidCastException e) {
                        throw new StrongTypingException("Cannot get value because it is DBNull.", e);
                    }
                }
                set {
                    this[this.tabletb_PatientRepLayout.HospitalDetailsColumn] = value;
                }
            }
            
            public bool HospitalLogo {
                get {
                    try {
                        return ((bool)(this[this.tabletb_PatientRepLayout.HospitalLogoColumn]));
                    }
                    catch (InvalidCastException e) {
                        throw new StrongTypingException("Cannot get value because it is DBNull.", e);
                    }
                }
                set {
                    this[this.tabletb_PatientRepLayout.HospitalLogoColumn] = value;
                }
            }
            
            public bool TitleDisplay {
                get {
                    try {
                        return ((bool)(this[this.tabletb_PatientRepLayout.TitleDisplayColumn]));
                    }
                    catch (InvalidCastException e) {
                        throw new StrongTypingException("Cannot get value because it is DBNull.", e);
                    }
                }
                set {
                    this[this.tabletb_PatientRepLayout.TitleDisplayColumn] = value;
                }
            }
            
            public int PatientDetails {
                get {
                    try {
                        return ((int)(this[this.tabletb_PatientRepLayout.PatientDetailsColumn]));
                    }
                    catch (InvalidCastException e) {
                        throw new StrongTypingException("Cannot get value because it is DBNull.", e);
                    }
                }
                set {
                    this[this.tabletb_PatientRepLayout.PatientDetailsColumn] = value;
                }
            }
            
            public bool PatientHistory {
                get {
                    try {
                        return ((bool)(this[this.tabletb_PatientRepLayout.PatientHistoryColumn]));
                    }
                    catch (InvalidCastException e) {
                        throw new StrongTypingException("Cannot get value because it is DBNull.", e);
                    }
                }
                set {
                    this[this.tabletb_PatientRepLayout.PatientHistoryColumn] = value;
                }
            }
            
            public bool ExaminationHistory {
                get {
                    try {
                        return ((bool)(this[this.tabletb_PatientRepLayout.ExaminationHistoryColumn]));
                    }
                    catch (InvalidCastException e) {
                        throw new StrongTypingException("Cannot get value because it is DBNull.", e);
                    }
                }
                set {
                    this[this.tabletb_PatientRepLayout.ExaminationHistoryColumn] = value;
                }
            }
            
            public System.Byte Layout {
                get {
                    try {
                        return ((System.Byte)(this[this.tabletb_PatientRepLayout.LayoutColumn]));
                    }
                    catch (InvalidCastException e) {
                        throw new StrongTypingException("Cannot get value because it is DBNull.", e);
                    }
                }
                set {
                    this[this.tabletb_PatientRepLayout.LayoutColumn] = value;
                }
            }
            
            public bool NormalValues {
                get {
                    try {
                        return ((bool)(this[this.tabletb_PatientRepLayout.NormalValuesColumn]));
                    }
                    catch (InvalidCastException e) {
                        throw new StrongTypingException("Cannot get value because it is DBNull.", e);
                    }
                }
                set {
                    this[this.tabletb_PatientRepLayout.NormalValuesColumn] = value;
                }
            }
            
            public bool Indication {
                get {
                    try {
                        return ((bool)(this[this.tabletb_PatientRepLayout.IndicationColumn]));
                    }
                    catch (InvalidCastException e) {
                        throw new StrongTypingException("Cannot get value because it is DBNull.", e);
                    }
                }
                set {
                    this[this.tabletb_PatientRepLayout.IndicationColumn] = value;
                }
            }
            
            public bool Interpretation {
                get {
                    try {
                        return ((bool)(this[this.tabletb_PatientRepLayout.InterpretationColumn]));
                    }
                    catch (InvalidCastException e) {
                        throw new StrongTypingException("Cannot get value because it is DBNull.", e);
                    }
                }
                set {
                    this[this.tabletb_PatientRepLayout.InterpretationColumn] = value;
                }
            }
            
            public bool Signature {
                get {
                    try {
                        return ((bool)(this[this.tabletb_PatientRepLayout.SignatureColumn]));
                    }
                    catch (InvalidCastException e) {
                        throw new StrongTypingException("Cannot get value because it is DBNull.", e);
                    }
                }
                set {
                    this[this.tabletb_PatientRepLayout.SignatureColumn] = value;
                }
            }
            
            public bool Preview {
                get {
                    try {
                        return ((bool)(this[this.tabletb_PatientRepLayout.PreviewColumn]));
                    }
                    catch (InvalidCastException e) {
                        throw new StrongTypingException("Cannot get value because it is DBNull.", e);
                    }
                }
                set {
                    this[this.tabletb_PatientRepLayout.PreviewColumn] = value;
                }
            }
            
            public bool IsDateDisplayNull() {
                return this.IsNull(this.tabletb_PatientRepLayout.DateDisplayColumn);
            }
            
            public void SetDateDisplayNull() {
                this[this.tabletb_PatientRepLayout.DateDisplayColumn] = System.Convert.DBNull;
            }
            
            public bool IsHospitalDetailsNull() {
                return this.IsNull(this.tabletb_PatientRepLayout.HospitalDetailsColumn);
            }
            
            public void SetHospitalDetailsNull() {
                this[this.tabletb_PatientRepLayout.HospitalDetailsColumn] = System.Convert.DBNull;
            }
            
            public bool IsHospitalLogoNull() {
                return this.IsNull(this.tabletb_PatientRepLayout.HospitalLogoColumn);
            }
            
            public void SetHospitalLogoNull() {
                this[this.tabletb_PatientRepLayout.HospitalLogoColumn] = System.Convert.DBNull;
            }
            
            public bool IsTitleDisplayNull() {
                return this.IsNull(this.tabletb_PatientRepLayout.TitleDisplayColumn);
            }
            
            public void SetTitleDisplayNull() {
                this[this.tabletb_PatientRepLayout.TitleDisplayColumn] = System.Convert.DBNull;
            }
            
            public bool IsPatientDetailsNull() {
                return this.IsNull(this.tabletb_PatientRepLayout.PatientDetailsColumn);
            }
            
            public void SetPatientDetailsNull() {
                this[this.tabletb_PatientRepLayout.PatientDetailsColumn] = System.Convert.DBNull;
            }
            
            public bool IsPatientHistoryNull() {
                return this.IsNull(this.tabletb_PatientRepLayout.PatientHistoryColumn);
            }
            
            public void SetPatientHistoryNull() {
                this[this.tabletb_PatientRepLayout.PatientHistoryColumn] = System.Convert.DBNull;
            }
            
            public bool IsExaminationHistoryNull() {
                return this.IsNull(this.tabletb_PatientRepLayout.ExaminationHistoryColumn);
            }
            
            public void SetExaminationHistoryNull() {
                this[this.tabletb_PatientRepLayout.ExaminationHistoryColumn] = System.Convert.DBNull;
            }
            
            public bool IsLayoutNull() {
                return this.IsNull(this.tabletb_PatientRepLayout.LayoutColumn);
            }
            
            public void SetLayoutNull() {
                this[this.tabletb_PatientRepLayout.LayoutColumn] = System.Convert.DBNull;
            }
            
            public bool IsNormalValuesNull() {
                return this.IsNull(this.tabletb_PatientRepLayout.NormalValuesColumn);
            }
            
            public void SetNormalValuesNull() {
                this[this.tabletb_PatientRepLayout.NormalValuesColumn] = System.Convert.DBNull;
            }
            
            public bool IsIndicationNull() {
                return this.IsNull(this.tabletb_PatientRepLayout.IndicationColumn);
            }
            
            public void SetIndicationNull() {
                this[this.tabletb_PatientRepLayout.IndicationColumn] = System.Convert.DBNull;
            }
            
            public bool IsInterpretationNull() {
                return this.IsNull(this.tabletb_PatientRepLayout.InterpretationColumn);
            }
            
            public void SetInterpretationNull() {
                this[this.tabletb_PatientRepLayout.InterpretationColumn] = System.Convert.DBNull;
            }
            
            public bool IsSignatureNull() {
                return this.IsNull(this.tabletb_PatientRepLayout.SignatureColumn);
            }
            
            public void SetSignatureNull() {
                this[this.tabletb_PatientRepLayout.SignatureColumn] = System.Convert.DBNull;
            }
            
            public bool IsPreviewNull() {
                return this.IsNull(this.tabletb_PatientRepLayout.PreviewColumn);
            }
            
            public void SetPreviewNull() {
                this[this.tabletb_PatientRepLayout.PreviewColumn] = System.Convert.DBNull;
            }
        }
        
        [System.Diagnostics.DebuggerStepThrough()]
        public class tb_PatientRepLayoutRowChangeEvent : EventArgs {
            
            private tb_PatientRepLayoutRow eventRow;
            
            private DataRowAction eventAction;
            
            public tb_PatientRepLayoutRowChangeEvent(tb_PatientRepLayoutRow row, DataRowAction action) {
                this.eventRow = row;
                this.eventAction = action;
            }
            
            public tb_PatientRepLayoutRow Row {
                get {
                    return this.eventRow;
                }
            }
            
            public DataRowAction Action {
                get {
                    return this.eventAction;
                }
            }
        }
    }
}
