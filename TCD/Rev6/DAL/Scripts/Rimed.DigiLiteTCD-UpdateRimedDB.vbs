'==============================================================
'This script updates the RIMED DB With changes 
'that have been made from the Current version on the machine        
'to the installed version Database                                      
'Acoording to the Compersion of the Old and the new version
' of the DB the script commits the update
' 
'==============================================================                   

'==============================================================                   
'   HISTORY
'==============================================================                   
' 2013.12.22 v1.0.0.9   Added Generalsettings table (and values)




const DBVersion = "1.0.0.9"


Const adOpenStatic = 3
Const adLockOptimistic = 3
Const adOpenKeyset =1
Const adCmdText=1

' Enable error handling
'On Error Resume Next

Set objConnection = CreateObject("ADODB.Connection")
Set objRecordSet = CreateObject("ADODB.Recordset")

objConnection.Open "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Rimed\Src\TCD2003\DATA\RimedDB.mdb;User ID=Admin;"
       
'Hide Intracranial Bilateral Mode (added by Natalie at 10.31.2008)'
objConnection.Execute "UPDATE tb_Study SET AuthorizationMgr=false WHERE Name='Intracranial Bilateral'"

'Create the new Table that will holds just one value - the DB version
'Wscript.Echo "Creating the DBVersion table"
        
CreateTableDBVersion

VMRBilateralWasExist = false
if not CheckIfDBVersionValueExist Then
    
  'If we have just creat now the DBVersion Table (Upgrading 15.1.0.9 version) 
  Wscript.Echo "The System needs to update some tables and fields in your DB, for compliance with the new version. Click �OK� to continue."
  Wscript.Echo "Upgrade to DB version '" + DBVersion +"'"
  
  AddUseEvelopeFieldToBloodVesselSetupTable
  AddDicomFieldsToHospitalDetailsTable
  InsertVMRstudy
  CreateTableStudyRecordingSetup
  CreateTableExaminationEvent
  CreateTableEvents
  InsertRecordsForVMRstudy

  UpdateDBto2       'Update DB from 1.0.0.1 to 1.0.0.2
  UpdateDBto3       'Update DB from 1.0.0.2 to 1.0.0.3
  UpdateDBto4       'Update DB from 1.0.0.3 to 1.0.0.4
  UpdateDBto5       'Update DB from 1.0.0.4 to 1.0.0.5
  UpdateDBto6       'Update DB from 1.0.0.5 to 1.0.0.6
  UpdateDBto7       'Update DB from 1.0.0.6 to 1.0.0.7
  UpdateDBto8       'Update DB from 1.0.0.7 to 1.0.0.8
  UpdateDBto9       'Update DB from 1.0.0.8 to 1.0.0.9

  UpdateDBVersion(DBVersion)    'Update DBVersion On DB
  
' There is already an old value of the DbVersion value and not all of the updates from version 1.15.0.9 are needed 
else  
	s = GetDBVersion
	if CompareVersions(DBVersion, s) > 0 then
		Wscript.Echo "The System needs to update some tables and fields in your DB, for compliance with the new version. Click �OK� to continue."
		Wscript.Echo "Upgrade to DB version '" + DBVersion +"'"

        if s < "1.0.0.2" then UpdateDBto2
        if s < "1.0.0.3" then UpdateDBto3
        if s < "1.0.0.4" then UpdateDBto4
        if s < "1.0.0.5" then UpdateDBto5
        if s < "1.0.0.6" then UpdateDBto6
        if s < "1.0.0.7" then UpdateDBto7
        if s < "1.0.0.8" then UpdateDBto8
        if s < "1.0.0.9" then UpdateDBto9

'		select case s
'			case "1.0.0.1"
'				UpdateDBto2
'				UpdateDBto3
'				UpdateDBto4
'				UpdateDBto5
'				UpdateDBto6
'				UpdateDBto7
'				UpdateDBto8
'                UpdateDBto9
'			case "1.0.0.2"
'				UpdateDBto3
'				UpdateDBto4
'				UpdateDBto5
'				UpdateDBto6
'				UpdateDBto7
'				UpdateDBto8
'                UpdateDBto9
'			case "1.0.0.3"
'				UpdateDBto4
'				UpdateDBto5
'				UpdateDBto6
'				UpdateDBto7
'				UpdateDBto8
'                UpdateDBto9
'			case "1.0.0.4"
'				UpdateDBto5
'				UpdateDBto6
'				UpdateDBto7
'				UpdateDBto8
'                UpdateDBto9
'			case "1.0.0.5"
'				UpdateDBto6
'				UpdateDBto7
'				UpdateDBto8
'                UpdateDBto9
'			case "1.0.0.6"
'				UpdateDBto7
'				UpdateDBto8
'                UpdateDBto9
'			case "1.0.0.7"
'				UpdateDBto8
'                UpdateDBto9
'			case "1.0.0.8"
'                UpdateDBto9
'		end select

		UpdateDBVersion(DBVersion)
	else
		s=""
	end if

end if

if s="" then
	Wscript.Echo "Current DB version (v"+ GetDBVersion +") is the last version."
else
	Wscript.Echo "DB Upgraded (from v"+ s +" to v"+ DBVersion +") compleated successfully."
end if

set objRecordSet = nothing
objConnection.Close

'============================
' Functions and Subs
'============================

'=======================================================================================================
'This Function inserts the necessary rows for the VMR study
'Includes - One BV (MCA_L), Two events (BASELINE and TEST), Clinical Parameters (also
' insert the new clinical paramter VMR to tb_ClinicalParam and to tb_Study_ClinicalParam
'=======================================================================================================
sub  InsertRecordsForVMRStudy()
	
	'1) Insert new clinical paramter named VMR
	s="INSERT INTO tb_ClinicalParam  (Name,Units) VALUES ('VMR','cm/sec')" 
	objConnection.Execute s
	
	objRecordSet.Open "SELECT * FROM tb_Study Where Name='VMR'", _
    objConnection, adOpenStatic, adLockOptimistic
    Set objRecordSet2 = CreateObject("ADODB.Recordset")	
    objRecordSet2.Open "SELECT Blood_Vessel_Index FROM tb_BloodVesselSetup WHERE Name = 'MCA'and Side =0", _
    objConnection, adOpenStatic, adLockOptimistic 
    
    ' 2) Insert the MCA-L bv to the VMR study
    if objRecordSet2.RecordCount>0 Then
		 s= 	"INSERT INTO tb_Study_BV  (Study_Index,Blood_Vessel_Index,IndexInStudy,Valid) VALUES (" +objRecordSet.Fields("Study_Index").Value + "," +objRecordSet2.Fields("Blood_Vessel_Index").Value + ",0,1)"
		 objConnection.Execute s
	end if
	
	'3) Insert the relevant CP to the VMR study (The VMR CP And the PEAK CP)
	objRecordSet2.Close
	objRecordSet2.Open "SELECT Index FROM tb_ClinicalParam WHERE Name = 'VMR'", _
    objConnection, adOpenStatic, adLockOptimistic 
	
	
	s= 	"INSERT INTO tb_Study_ClinicalParam  (ClinicalParam_Index,Study_Index,IndexInStudy,Valid) VALUES (" +objRecordSet2.Fields("Index").Value + "," +objRecordSet.Fields("Study_Index").Value + ",0,1)"
	objConnection.Execute s
	
	objRecordSet2.Close
	objRecordSet2.Open "SELECT Index FROM tb_ClinicalParam WHERE Name = 'Peak'", _
    objConnection, adOpenStatic, adLockOptimistic 
	
	
	s= 	"INSERT INTO tb_Study_ClinicalParam  (ClinicalParam_Index,Study_Index,IndexInStudy,Valid) VALUES (" +objRecordSet2.Fields("Index").Value + "," +objRecordSet.Fields("Study_Index").Value + ",1,1)"
	objConnection.Execute s
	
	'4) Insert the relevants events to the VMR Study (Baseline DIAMOX and TEST)
	s= "INSERT INTO tb_Events (Study_Index, Name,IndexInStudy, EventType, Valid ) VALUES(" +objRecordSet.Fields("Study_Index").Value + ",'BaseLine Velocity',0,1,1)"
	objConnection.Execute s
	s= "INSERT INTO tb_Events (Study_Index, Name,IndexInStudy, EventType, Valid ) VALUES(" +objRecordSet.Fields("Study_Index").Value + ",'DIAMOX',1,1,1)"
	objConnection.Execute s
	s= "INSERT INTO tb_Events (Study_Index, Name,IndexInStudy, EventType, Valid ) VALUES(" +objRecordSet.Fields("Study_Index").Value + ",'Test Velocity',2,1,1)"
	objConnection.Execute s
	
	
    set objRecordSet2 =Nothing
    objRecordSet.Close
    
end Sub


Function CheckIfDBVersionValueExist()
    objRecordSet.Open "SELECT * FROM tb_DBVersion", _
    objConnection, adOpenStatic, adLockOptimistic

    If objRecordSet.RecordCount<1 Then
       objConnection.Execute "INSERT INTO tb_DBVersion ([DB_Version]) VALUES ('1.0.0.0')"
       CheckIfDBVersionValueExist = false		
    else 
	   CheckIfDBVersionValueExist = true
    End If

    objRecordSet.Close
	
End Function

Function CompareVersions(newVersion, oldVersion)
    CompareVersions = 0
    newVersionArr = Split(newVersion, ".")
    oldVersionArr = Split(oldVersion, ".")
    newVersionLen = UBound(newVersionArr)
    oldVersionLen = UBound(oldVersionArr)
    minLen = newVersionLen

    if oldVersionLen < minLen then
        minLen = oldVersionLen
    end if

    for i = 0 to minLen
        if CInt(newVersionArr(i)) < CInt(oldVersionArr(i)) then
            CompareVersions = -1
            exit for
        elseif CInt(newVersionArr(i)) > CInt(oldVersionArr(i)) then
            CompareVersions = 1
            exit for
        end if
    next

    if CompareVersions = 0 and newVersionLen <> oldVersionLen then
        if newVersionLen > oldVersionLen then
            CompareVersions = 1
        else
            CompareVersions = -1
        end if
    end if
End Function

Function GetDBVersion()
    objRecordSet.Open "SELECT * FROM tb_DBVersion", _
    objConnection, adOpenStatic, adLockOptimistic

	GetDBVersion= objRecordSet.Fields("DB_Version").value
    
    objRecordSet.Close
	
End Function

Sub  UpdateDBVersion( NewDbVerssion )
	objRecordSet.Open "SELECT * FROM tb_DBVersion", _
    objConnection, adOpenStatic, adLockOptimistic
    objRecordSet.Fields("DB_Version").value = NewDbVerssion
    objRecordSet.update	
    objRecordSet.Close
	
End Sub 

Sub CreateTableDBVersion()
   On Error Resume Next
   objConnection.Execute "CREATE TABLE tb_DBVersion(DB_Version TEXT(50))"
End Sub

Sub CreateTableStudyRecordingSetup()
    objConnection.Execute "CREATE TABLE tb_StudyRecordingSetup(" & _
   "Study_Index GUID DEFAULT GenGUID() NOT NULL PRIMARY KEY," & _
   "SaveForwardPeak YesNo ,"& _ 
   "SaveBackPeak YesNo ,"& _ 
   "SaveForwardMode YesNo ,"& _ 
   "SaveBackMode YesNo ,"& _ 
   "SaveContinuously YesNo ,"& _ 
   "TimeAroundEventsInSec int ,"& _ 
   "SaveSpectrumAroundHits YesNo ,"& _ 
   "SaveSpectrumAroundAlarms YesNo ,"& _ 
   "SaveSpectrumAroundPredefinedEvents YesNo ,"& _ 
   "SaveSpectrumAroundEachTimeInterval YesNo ,"& _ 
   "TimeIntervalInMinutes int )"' ," & _
   '"primary key (Study_Index))" 
   
End Sub

Sub CreateTableExaminationEvent()
    objConnection.Execute "CREATE TABLE tb_ExaminationEvent(" & _
   "Event_Index GUID DEFAULT GenGUID() NOT NULL PRIMARY KEY ," & _
   "Examination_Index GUID ," & _
   "Gate_Index GUID  ," & _
   "FileIndex smallint," & _
   "IndexInFile int ," & _
   "EventTime datetime ," & _
   "Deleted YesNo ,"& _ 
   "EventType smallint  ," & _
   "EventName TEXT(50)," & _
   "HitType smallint ," & _
   "HitVelocity double ," & _
   "HitEnergy double ," & _
   "AlarmValue double ," & _
   "ExtraInfo1 double ," & _
   "ExtraInfo2 double ," & _
   "ExtraInfo3 double ," & _
   "ExtraInfo4 int ," & _
   "ExtraInfo5 smallint ," & _
   "ExtraInfo6 TEXT(50) ," & _
   "ExtraInfo7 TEXT(50) )"'," & _
   '"primary key (Event_Index))" 
    objConnection.Execute "CREATE INDEX Examination_Index_Index ON tb_ExaminationEvent (Examination_Index)"
End Sub

Sub CreateTableEvents()
   objConnection.Execute "CREATE TABLE tb_Events(" & _
   "Event_Index GUID DEFAULT GenGUID() NOT NULL PRIMARY KEY," & _
   "Study_Index GUID  ," & _
   "Name TEXT(100) ," & _
   "IndexInStudy smallint," & _
   "EventType smallint ," & _
   "Description memo ," & _
   "Valid YesNo )"
   objConnection.Execute "CREATE INDEX Study_Index_Index ON tb_Events (Study_Index)"
End Sub

Sub AddDicomFieldsToHospitalDetailsTable()
	objConnection.Execute "ALTER TABLE tb_HospitalDetails ADD IP_Address TEXT(50) NULL" 
	objConnection.Execute "ALTER TABLE tb_HospitalDetails ADD Port_Number smallint NULL"
	objConnection.Execute "ALTER TABLE tb_HospitalDetails ADD AE_Title TEXT(50) NULL"
End Sub

Sub CreateTableGeneralSettings()
    objConnection.Execute "CREATE TABLE tb_GeneralSettings([Key] Text(32) NOT NULL PRIMARY KEY, KeyValue Text(32), Description Text(255) )"

	s="INSERT INTO tb_GeneralSettings  ([Key] ,KeyValue, Description) VALUES ('ExportInterval', '30', 'Interval, in seconds, for clinical parameters export to csv-files')" 
	objConnection.Execute s
	
	s="INSERT INTO tb_GeneralSettings  ([Key] ,KeyValue, Description) VALUES ('ExportSeparator', ',', 'Column seperator for exported CSV files')" 
	objConnection.Execute s
	
	s="INSERT INTO tb_GeneralSettings  ([Key] ,KeyValue, Description) VALUES ('IpImagesPerPage', '0', 'Number of IP images per report's page. Valid values: 0, 1, 2, 4')" 
	objConnection.Execute s
	
	s="INSERT INTO tb_GeneralSettings  ([Key] ,KeyValue, Description) VALUES ('OnePageReport', 'true', 'Simple (vs. full) patient report. Valid value: true, false')" 
	objConnection.Execute s
	
	s="INSERT INTO tb_GeneralSettings  ([Key] ,KeyValue, Description) VALUES ('ShowHitsEvent', 'true', 'Show HITS events on the trend diagram. Valid value: true, false')" 
	objConnection.Execute s
	
	s="INSERT INTO tb_GeneralSettings  ([Key] ,KeyValue, Description) VALUES ('StartPlayEvent', '2', 'Play start offset, in seconds, from selected event')" 
	objConnection.Execute s
End Sub


'=======================================================================================================
'This Function adds SaveOnlySummaryImages field to tb_BasicConfiguration and tb_BVExamination tables
'=======================================================================================================
Sub AddFieldsToSupportSaveOnlySummaryImages ()
	objConnection.Execute "ALTER TABLE tb_BasicConfiguration ADD SaveOnlySummaryImages YesNo NULL DEFAULT 0" 
	objConnection.Execute "ALTER TABLE tb_BVExamination ADD SaveOnlySummaryImages YesNo NULL DEFAULT 0" 
	
End Sub


Sub AddUseEvelopeFieldToBloodVesselSetupTable()
	objConnection.Execute "ALTER TABLE tb_BloodVesselSetup ADD UseEnvelopesForRCPDisplay YesNo NULL DEFAULT 1" 
	Set objRecordSet2 = CreateObject("ADODB.Recordset")	
	objRecordSet2.Open "SELECT UseEnvelopesForRCPDisplay FROM tb_BloodVesselSetup", _
    objConnection, adOpenStatic, adLockOptimistic 
    'objRecordSet2.MoveFirst
    'Setting the defualt value of UseEnvelopesForRCPDisplay to TRUE
    While Not objRecordSet2.EOF
		objRecordSet2.Fields("UseEnvelopesForRCPDisplay").value =1
		objRecordSet2.MoveNext
    Wend
    objRecordSet2.MoveFirst
    objRecordSet2.Update
    objRecordSet2.Close
    set objRecordSet2 = Nothing
    
End Sub

Sub InsertVMRstudy()

    objRecordSet.Open "SELECT * FROM tb_Study Where Name='VMR'", _
    objConnection, adOpenStatic, adLockOptimistic
    Set objRecordSet2 = CreateObject("ADODB.Recordset")	
    objRecordSet2.Open "SELECT Name,SerializedData FROM tb_Study WHERE Name = 'Monitoring Intracranial Unilateral'", _
    objConnection, adOpenStatic, adLockOptimistic 
    
    
  	'If the VMR Study Does not exist Yet	
    If objRecordSet.RecordCount<1 Then
        s= 	"INSERT INTO tb_Study  (Name,DefaultStudy,BasedOn,NumOfProbes,Monitoring,AuthorizationMgr,SpecialInfo ) VALUES ('VMR',1,'00000000-0000-0000-0000-000000000000',1,1,1,'6')"
	    objConnection.Execute s
        objRecordSet.Close
        objRecordSet.Open "SELECT * FROM tb_Study Where Name='VMR'", _
	    objConnection, adOpenStatic, adLockOptimistic 
        objRecordSet.MoveFirst
        ' Copying the OLE object (The save Layout class ) from  'Monitoring Intracranial Unilateral'
        objRecordSet.Fields("SerializedData").value =objRecordSet2.Fields("SerializedData").value
       
	    objRecordSet.update	
        set objRecordSet2 = nothing	
        
    End If

    objRecordSet.Close 
    Set objRecordSet2 = nothing
End sub

'=======================================================================================================
'This Function inserts the necessary rows for the VMR-Bilateral study
'Includes - One BV (MCA_L), Two events (BASELINE and TEST), Clinical Parameters (also
' insert the new clinical paramter VMR to tb_ClinicalParam and to tb_Study_ClinicalParam
'=======================================================================================================
sub  InsertRecordsForVMRBilateralStudy()
	
    objRecordSet.Open "SELECT * FROM tb_Study Where Name='VMR Bilateral'", _
    objConnection, adOpenStatic, adLockOptimistic
    Set objRecordSet2 = CreateObject("ADODB.Recordset")	
'If the VMR Study Does not exist Yet	
    If VMRBilateralWasExist <> true Then
    objRecordSet2.Open "SELECT Blood_Vessel_Index FROM tb_BloodVesselSetup WHERE Name = 'MCA'and Side =0", _
    objConnection, adOpenStatic, adLockOptimistic 
    
    Set objRecordSet3 = CreateObject("ADODB.Recordset")
    ' 2.1) Insert the MCA-L bv to the VMRBilateral study
    if objRecordSet2.RecordCount>0 Then
		 objRecordSet3.Open "SELECT * FROM tb_Study_BV WHERE Study_Index =" + objRecordSet.Fields("Study_Index").Value + _
			" and Blood_Vessel_Index =" + objRecordSet2.Fields("Blood_Vessel_Index").Value, _
			objConnection, adOpenStatic, adLockOptimistic
		 if not objRecordSet3.RecordCount>0	then
			s= 	"INSERT INTO tb_Study_BV  (Study_Index,Blood_Vessel_Index,IndexInStudy,Valid) VALUES (" +objRecordSet.Fields("Study_Index").Value + "," +objRecordSet2.Fields("Blood_Vessel_Index").Value + ",0,1)"
			objConnection.Execute s
		 end if
		 objRecordSet3.Close
	end if
	' 2.2) Insert the MCA-R bv to the VMRBilateral study
	objRecordSet2.Close
	objRecordSet2.Open "SELECT Blood_Vessel_Index FROM tb_BloodVesselSetup WHERE Name = 'MCA'and Side =1", _
    	objConnection, adOpenStatic, adLockOptimistic 
	if objRecordSet2.RecordCount>0 Then
		 objRecordSet3.Open "SELECT * FROM tb_Study_BV WHERE Study_Index =" + objRecordSet.Fields("Study_Index").Value + _
			" and Blood_Vessel_Index =" + objRecordSet2.Fields("Blood_Vessel_Index").Value, _
			objConnection, adOpenStatic, adLockOptimistic
		 if not objRecordSet3.RecordCount>0	then
			s= 	"INSERT INTO tb_Study_BV  (Study_Index,Blood_Vessel_Index,IndexInStudy,Valid) VALUES (" +objRecordSet.Fields("Study_Index").Value + "," +objRecordSet2.Fields("Blood_Vessel_Index").Value + ",0,1)"
			objConnection.Execute s
		 end if
		 objRecordSet3.Close
	end if
	'3) Insert the relevant CP to the VMR Bilateral study (The VMR CP And the PEAK CP)
	objRecordSet2.Close
	objRecordSet2.Open "SELECT Index FROM tb_ClinicalParam WHERE Name = 'VMR'", _
    objConnection, adOpenStatic, adLockOptimistic 
	
	objRecordSet3.Open "SELECT * FROM tb_Study_ClinicalParam WHERE Study_Index =" + objRecordSet.Fields("Study_Index").Value + _
		" and ClinicalParam_Index =" + objRecordSet2.Fields("Index").Value, _
		objConnection, adOpenStatic, adLockOptimistic
	if not objRecordSet3.RecordCount>0	then
		s= 	"INSERT INTO tb_Study_ClinicalParam  (ClinicalParam_Index,Study_Index,IndexInStudy,Valid) VALUES (" +objRecordSet2.Fields("Index").Value + "," +objRecordSet.Fields("Study_Index").Value + ",0,1)"
		objConnection.Execute s
	end if
	objRecordSet3.Close
	
	objRecordSet2.Close
	objRecordSet2.Open "SELECT Index FROM tb_ClinicalParam WHERE Name = 'Peak'", _
    objConnection, adOpenStatic, adLockOptimistic 
	
	objRecordSet3.Open "SELECT * FROM tb_Study_ClinicalParam WHERE Study_Index =" + objRecordSet.Fields("Study_Index").Value + _
		" and ClinicalParam_Index =" + objRecordSet2.Fields("Index").Value, _
		objConnection, adOpenStatic, adLockOptimistic
	if not objRecordSet3.RecordCount>0	then
		s= 	"INSERT INTO tb_Study_ClinicalParam  (ClinicalParam_Index,Study_Index,IndexInStudy,Valid) VALUES (" +objRecordSet2.Fields("Index").Value + "," +objRecordSet.Fields("Study_Index").Value + ",1,1)"
		objConnection.Execute s
	end if
	objRecordSet2.Close
	objRecordSet3.Close
	
	'4) Insert the relevants events to the VMR Bilateral Study (Baseline DIAMOX and TEST)
	objRecordSet3.Open "SELECT * FROM tb_Events WHERE Study_Index =" + objRecordSet.Fields("Study_Index").Value + " and Name = 'BaseLine Velocity'", _
    objConnection, adOpenStatic, adLockOptimistic 
    if objRecordSet3.RecordCount=0 then
		s= "INSERT INTO tb_Events (Study_Index, Name,IndexInStudy, EventType, Valid ) VALUES(" +objRecordSet.Fields("Study_Index").Value + ",'BaseLine Velocity',0,1,1)"
		objConnection.Execute s
	end if
	objRecordSet3.Close
	objRecordSet3.Open "SELECT * FROM tb_Events WHERE Study_Index =" + objRecordSet.Fields("Study_Index").Value + " and Name = 'DIAMOX'", _
	objConnection, adOpenStatic, adLockOptimistic
	if objRecordSet3.RecordCount=0 then
		objRecordSet2.Open "SELECT * FROM tb_Events WHERE Study_Index =" + objRecordSet.Fields("Study_Index").Value + " and Name = 'DIMOX'", _
		objConnection, adOpenStatic, adLockOptimistic
		if objRecordSet2.RecordCount=0 then
			s= "INSERT INTO tb_Events (Study_Index, Name,IndexInStudy, EventType, Valid ) VALUES(" +objRecordSet.Fields("Study_Index").Value + ",'DIAMOX',1,1,1)"
			objConnection.Execute s
		else
			objRecordSet2.Fields("Name").Value ="DIAMOX"
			objRecordSet2.update
		end if
	end if
	objRecordSet3.Close
	objRecordSet3.Open "SELECT * FROM tb_Events WHERE Study_Index =" + objRecordSet.Fields("Study_Index").Value + " and Name = 'Test Velocity'", _
	objConnection, adOpenStatic, adLockOptimistic
	if objRecordSet3.RecordCount=0 then
		s= "INSERT INTO tb_Events (Study_Index, Name,IndexInStudy, EventType, Valid ) VALUES(" +objRecordSet.Fields("Study_Index").Value + ",'Test Velocity',2,1,1)"
		objConnection.Execute s
	end if
	objRecordSet3.Close
	End If	
	
    set objRecordSet2 =Nothing
    objRecordSet.Close
    
end Sub

Sub InsertVMRBilateralstudy()

    objRecordSet.Open "SELECT * FROM tb_Study Where Name='VMR Bilateral'", _
    objConnection, adOpenStatic, adLockOptimistic
    Set objRecordSet2 = CreateObject("ADODB.Recordset")	
    objRecordSet2.Open "SELECT Name,SerializedData FROM tb_Study WHERE Name = 'Monitoring Intracranial Bilateral'", _
    objConnection, adOpenStatic, adLockOptimistic 
    
    
  	'If the VMR Study Does not exist Yet	
    If objRecordSet.RecordCount<1 Then
        s= 	"INSERT INTO tb_Study  (Name,DefaultStudy,BasedOn,NumOfProbes,Monitoring,AuthorizationMgr,SpecialInfo ) VALUES ('VMR Bilateral',1,'00000000-0000-0000-0000-000000000000',2,1,1,'7')"
	    objConnection.Execute s
        objRecordSet.Close
        objRecordSet.Open "SELECT * FROM tb_Study Where Name='VMR Bilateral'", _
	    objConnection, adOpenStatic, adLockOptimistic 
        objRecordSet.MoveFirst
        ' Copying the OLE object (The save Layout class ) from  'Monitoring Intracranial Bilateral'
        objRecordSet.Fields("SerializedData").value =objRecordSet2.Fields("SerializedData").value
       
	    objRecordSet.update	
        set objRecordSet2 = nothing	
    Else
		VMRBilateralWasExist = true
        
    End If

    objRecordSet.Close 
    Set objRecordSet2 = nothing
End sub

'=======================================================================================================
'Some additional functions for nice code: Update DB from 1 to 2 version
'=======================================================================================================

Sub UpdateDBto2()

	'Wscript.Echo "InsertVMRBilateralstudy"
	InsertVMRBilateralstudy
	'Wscript.Echo "InsertRecordsForVMRBilateralStudy"
	InsertRecordsForVMRBilateralStudy
	'Wscript.Echo "AddFieldsToSupportSaveOnlySummaryImages"
	AddFieldsToSupportSaveOnlySummaryImages

End Sub

'=======================================================================================================
'Some additional functions for nice code: Update DB from 2 to 3 version
'=======================================================================================================

Sub UpdateDBto3()
	'Wscript.Echo "AddFieldsToChannelTable"
	AddFieldsToChannelTable
	'Wscript.Echo "CorrectChannelTableFields"
	CorrectChannelTableFields
	'Wscript.Echo "CreateTableStudy_Channel"
	CreateTableStudy_Channel
End Sub

'=======================================================================================================
'Some additional functions for nice code: Update DB from 3 to 4 version
'=======================================================================================================

Sub UpdateDBto4()
	AddBackupFlagToExaminationTable
	RenameInterpretionFieldInPatientRepLayoutTable
'	AddDefaultEvents 
End Sub

'=======================================================================================================
'Some additional functions for nice code: Update DB from 4 to 5 version
'=======================================================================================================

Sub UpdateDBto5()
	RenameDIMOXEvent
	InsertEvokedFlowUnilateralStudy
	InsertEvokedFlowBilateralStudy
	InsertRecordsForEvokedFlowStudies
End Sub

'=======================================================================================================
'Some additional functions for nice code: Update DB from 5 to 6 version
'=======================================================================================================

Sub UpdateDBto6()
	AddOnSummaryScreenIndexColumnToGateExaminationTable
End Sub

'=======================================================================================================
'Some additional functions for nice code: Update DB from 6 to 7 version
'=======================================================================================================

Sub UpdateDBto7()
	AddDicomColumnToAuthorizationMgrTable
	AddLocalAEColumnToHospitalDetailsTable
	ChangeLayoutRepNumColumnInBasicConfigurationTable
	AddAccessionNumberColumnToPatientTable
End Sub

'=======================================================================================================
'Add IsTemporary column: Update DB from 7 to 8 version
'=======================================================================================================

Sub UpdateDBto8()
	AddTemporaryColumnToExaminationTable
End Sub

'=======================================================================================================
'Add GeneralSettings table: Update DB from 8 to 9 version
'=======================================================================================================
Sub UpdateDBto8()
    CreateTableGeneralSettings()
End Sub



'=======================================================================================================
'This Function inserts the tb_study_channel table to DB
'=======================================================================================================
Sub CreateTableStudy_Channel()
    objConnection.Execute "CREATE TABLE tb_Study_Channel(" & _
	"Study_Index GUID NOT NULL ," & _
	"Channel_Index GUID NOT NULL ," & _ 
	"IndexInStudy long DEFAULT 0," & _ 
	"Valid YesNo ," & _ 
	"CONSTRAINT tb_Study_tb_Study_Channel FOREIGN KEY (Study_Index) REFERENCES tb_Study ON DELETE CASCADE ," & _
	"CONSTRAINT tb_Channel_tb_Study_Channel FOREIGN KEY (Channel_Index) REFERENCES tb_Channel(Channel_Index) ON DELETE CASCADE )"
	objConnection.Execute "CREATE INDEX Study_Index ON tb_Study_Channel (Channel_Index)"
	objConnection.Execute "CREATE INDEX StudyIndex ON tb_Study_Channel (Study_Index)"
	objConnection.Execute "CREATE UNIQUE INDEX PrimaryKey ON tb_Study_Channel (Study_Index, Channel_Index) WITH PRIMARY"
	
	Set objRecordSetMIU = CreateObject("ADODB.Recordset")	
	Set objRecordSetMIB = CreateObject("ADODB.Recordset")	
	Set objRecordSetVMR = CreateObject("ADODB.Recordset")	
	Set objRecordSetVMRB = CreateObject("ADODB.Recordset")	
	Set objRecordSetPeak = CreateObject("ADODB.Recordset")	
	Set objRecordSetMean = CreateObject("ADODB.Recordset")	
	Set objRecordSetPI = CreateObject("ADODB.Recordset")	
	Set objRecordSetDV = CreateObject("ADODB.Recordset")	
	
	objRecordSetMIU.Open "SELECT Study_Index FROM tb_Study Where Name='Monitoring Intracranial Unilateral'", _
    objConnection, adOpenStatic, adLockOptimistic
    objRecordSetMIB.Open "SELECT Study_Index FROM tb_Study Where Name='Monitoring Intracranial Bilateral'", _
    objConnection, adOpenStatic, adLockOptimistic
    objRecordSetVMR.Open "SELECT Study_Index FROM tb_Study Where Name='VMR'", _
    objConnection, adOpenStatic, adLockOptimistic
    objRecordSetVMRB.Open "SELECT Study_Index FROM tb_Study Where Name='VMR Bilateral'", _
    objConnection, adOpenStatic, adLockOptimistic
    
    objRecordSetPeak.Open "SELECT Channel_Index FROM tb_Channel Where Name='Peak'", _
    objConnection, adOpenStatic, adLockOptimistic
    objRecordSetMean.Open "SELECT Channel_Index FROM tb_Channel Where Name='Mean'", _
    objConnection, adOpenStatic, adLockOptimistic
    objRecordSetPI.Open "SELECT Channel_Index FROM tb_Channel Where Name='P.I.'", _
    objConnection, adOpenStatic, adLockOptimistic
    objRecordSetDV.Open "SELECT Channel_Index FROM tb_Channel Where Name='DV'", _
    objConnection, adOpenStatic, adLockOptimistic
    
	objConnection.Execute "INSERT INTO tb_Study_Channel(Study_Index, Channel_Index, IndexInStudy, Valid)" & _
		"VALUES(" + objRecordSetMIU.Fields("Study_Index").Value + "," + objRecordSetPeak.Fields("Channel_Index").Value + ", 0, -1)"
	objConnection.Execute "INSERT INTO tb_Study_Channel(Study_Index, Channel_Index, IndexInStudy, Valid)" & _
		"VALUES(" + objRecordSetMIU.Fields("Study_Index").Value + "," + objRecordSetMean.Fields("Channel_Index").Value + ", 1, -1)"
	objConnection.Execute "INSERT INTO tb_Study_Channel(Study_Index, Channel_Index, IndexInStudy, Valid)" & _
		"VALUES(" + objRecordSetMIU.Fields("Study_Index").Value + "," + objRecordSetPI.Fields("Channel_Index").Value + ", 2, -1)"
	objConnection.Execute "INSERT INTO tb_Study_Channel(Study_Index, Channel_Index, IndexInStudy, Valid)" & _
		"VALUES(" + objRecordSetMIU.Fields("Study_Index").Value + "," + objRecordSetDV.Fields("Channel_Index").Value + ", 3, -1)"
		
	objConnection.Execute "INSERT INTO tb_Study_Channel(Study_Index, Channel_Index, IndexInStudy, Valid)" & _
		"VALUES(" + objRecordSetMIB.Fields("Study_Index").Value + "," + objRecordSetPeak.Fields("Channel_Index").Value + ", 0, -1)"
	objConnection.Execute "INSERT INTO tb_Study_Channel(Study_Index, Channel_Index, IndexInStudy, Valid)" & _
		"VALUES(" + objRecordSetMIB.Fields("Study_Index").Value + "," + objRecordSetMean.Fields("Channel_Index").Value + ", 1, -1)"
	objConnection.Execute "INSERT INTO tb_Study_Channel(Study_Index, Channel_Index, IndexInStudy, Valid)" & _
		"VALUES(" + objRecordSetMIB.Fields("Study_Index").Value + "," + objRecordSetPI.Fields("Channel_Index").Value + ", 2, -1)"
	objConnection.Execute "INSERT INTO tb_Study_Channel(Study_Index, Channel_Index, IndexInStudy, Valid)" & _
		"VALUES(" + objRecordSetMIB.Fields("Study_Index").Value + "," + objRecordSetDV.Fields("Channel_Index").Value + ", 3, -1)"
		
	objConnection.Execute "INSERT INTO tb_Study_Channel(Study_Index, Channel_Index, IndexInStudy, Valid)" & _
		"VALUES(" + objRecordSetVMR.Fields("Study_Index").Value + "," + objRecordSetPeak.Fields("Channel_Index").Value + ", 0, -1)"
	objConnection.Execute "INSERT INTO tb_Study_Channel(Study_Index, Channel_Index, IndexInStudy, Valid)" & _
		"VALUES(" + objRecordSetVMR.Fields("Study_Index").Value + "," + objRecordSetMean.Fields("Channel_Index").Value + ", 1, -1)"
	objConnection.Execute "INSERT INTO tb_Study_Channel(Study_Index, Channel_Index, IndexInStudy, Valid)" & _
		"VALUES(" + objRecordSetVMR.Fields("Study_Index").Value + "," + objRecordSetPI.Fields("Channel_Index").Value + ", 2, -1)"
	objConnection.Execute "INSERT INTO tb_Study_Channel(Study_Index, Channel_Index, IndexInStudy, Valid)" & _
		"VALUES(" + objRecordSetVMR.Fields("Study_Index").Value + "," + objRecordSetDV.Fields("Channel_Index").Value + ", 3, -1)"
		
	objConnection.Execute "INSERT INTO tb_Study_Channel(Study_Index, Channel_Index, IndexInStudy, Valid)" & _
		"VALUES(" + objRecordSetVMRB.Fields("Study_Index").Value + "," + objRecordSetPeak.Fields("Channel_Index").Value + ", 0, -1)"
	objConnection.Execute "INSERT INTO tb_Study_Channel(Study_Index, Channel_Index, IndexInStudy, Valid)" & _
		"VALUES(" + objRecordSetVMRB.Fields("Study_Index").Value + "," + objRecordSetMean.Fields("Channel_Index").Value + ", 1, -1)"
	objConnection.Execute "INSERT INTO tb_Study_Channel(Study_Index, Channel_Index, IndexInStudy, Valid)" & _
		"VALUES(" + objRecordSetVMRB.Fields("Study_Index").Value + "," + objRecordSetPI.Fields("Channel_Index").Value + ", 2, -1)"
	objConnection.Execute "INSERT INTO tb_Study_Channel(Study_Index, Channel_Index, IndexInStudy, Valid)" & _
		"VALUES(" + objRecordSetVMRB.Fields("Study_Index").Value + "," + objRecordSetDV.Fields("Channel_Index").Value + ", 3, -1)"
		
	objRecordSetMIU.Close
	objRecordSetMIB.Close
	objRecordSetVMR.Close
	objRecordSetVMRB.Close
	objRecordSetPeak.Close
	objRecordSetMean.Close
	objRecordSetPI.Close
	objRecordSetDV.Close
End Sub

'=======================================================================================================
'This Function adds needed fields to tb_Channel table
'=======================================================================================================

Sub AddFieldsToChannelTable()
	objConnection.Execute "ALTER TABLE tb_Channel ADD Color Long DEFAULT 0"
	objConnection.Execute "ALTER TABLE tb_Channel ADD ActiveAlarm YesNo"
	objConnection.Execute "ALTER TABLE tb_Channel ADD LinearA Real DEFAULT 0"
	objConnection.Execute "ALTER TABLE tb_Channel ADD LinearB Real DEFAULT 0"
	objConnection.Execute "ALTER TABLE tb_Channel ADD TrendWindowNumber Byte DEFAULT 0"
	objConnection.Execute "ALTER TABLE tb_Channel ADD ExternalChannelNumber Byte DEFAULT 0"
End Sub

Sub CorrectChannelTableFields()
	objConnection.Execute "DELETE FROM tb_Channel"
	objConnection.Execute "INSERT INTO tb_Channel (Channel_Index, Name, IsExternal, YMin, YMax, AlarmLow, AlarmHigh, Color, LinearA, ExternalChannelNumber)" & _ 
	  "VALUES('2649A25A-709D-4177-94C7-3B0A4A467274', 'Ext7', 1, -100, 100, 0, 0, -16744448, 1, 7)"
	objConnection.Execute "INSERT INTO tb_Channel (Channel_Index, Name, IsExternal, YMin, YMax, AlarmLow, AlarmHigh, Color, LinearA, ExternalChannelNumber)" & _ 
	  "VALUES('43230A94-A0D4-4F29-B1A5-55469B3435E2', 'Peak', 0, -130, 130, 0, 0, -16776961, 0, 255)"
	objConnection.Execute "INSERT INTO tb_Channel (Channel_Index, Name, IsExternal, YMin, YMax, AlarmLow, AlarmHigh, Color, LinearA, ExternalChannelNumber)" & _ 
	  "VALUES('4626CDBA-5665-43E9-8C5D-0601F32EF918', 'Ext8', 1, -100, 100, 0, 0, -16744448, 1, 8)"
	objConnection.Execute "INSERT INTO tb_Channel (Channel_Index, Name, IsExternal, YMin, YMax, AlarmLow, AlarmHigh, Color, LinearA, ExternalChannelNumber)" & _ 
	  "VALUES('521BCEAE-1A30-4E8A-92BD-FDCEAD1B80EF', 'Mean', 0, -130, 130, 1, 67, -65536, 0, 255)"
    objConnection.Execute "INSERT INTO tb_Channel (Channel_Index, Name, IsExternal, YMin, YMax, AlarmLow, AlarmHigh, Color, LinearA, ExternalChannelNumber)" & _ 
	  "VALUES('53348867-1FFE-49D9-882C-E0F3138340EE', 'Ext5', 1, -100, 100, 0, 0, -16744448, 1, 5)"
	objConnection.Execute "INSERT INTO tb_Channel (Channel_Index, Name, IsExternal, YMin, YMax, AlarmLow, AlarmHigh, Color, LinearA, ExternalChannelNumber)" & _ 
	  "VALUES('8CE1DFE7-C57A-44F4-9993-DBB30DA11B05', 'Ext1', 1, -100, 100, 0, 0, -16744448, 1, 1)"
	objConnection.Execute "INSERT INTO tb_Channel (Channel_Index, Name, IsExternal, YMin, YMax, AlarmLow, AlarmHigh, Color, LinearA, ExternalChannelNumber)" & _ 
	  "VALUES('8E21D33E-34EF-4D02-89F4-853E6298606B', 'P.I.', 0, 0, 4, 0, 0, -16181, 0, 255)"
	objConnection.Execute "INSERT INTO tb_Channel (Channel_Index, Name, IsExternal, YMin, YMax, AlarmLow, AlarmHigh, Color, LinearA, ExternalChannelNumber)" & _ 
	  "VALUES('A344F856-7E56-4317-A0D6-B03AA32E0271', 'Ext2', 1, -100, 100, 0, 0, -16744448, 1, 2)"
	objConnection.Execute "INSERT INTO tb_Channel (Channel_Index, Name, IsExternal, YMin, YMax, AlarmLow, AlarmHigh, Color, LinearA, ExternalChannelNumber)" & _ 
	  "VALUES('C2CF4542-0402-4E2C-8339-FCE0C8443B55', 'Ext3', 1, -100, 100, 0, 0, -16744448, 1, 3)"
	objConnection.Execute "INSERT INTO tb_Channel (Channel_Index, Name, IsExternal, YMin, YMax, AlarmLow, AlarmHigh, Color, LinearA, ExternalChannelNumber)" & _ 
	  "VALUES('C66C460F-43DD-4EAB-B9B0-07CA28549BF3', 'Ext4', 1, -100, 100, 0, 0, -16744448, 1, 4)"
	objConnection.Execute "INSERT INTO tb_Channel (Channel_Index, Name, IsExternal, YMin, YMax, AlarmLow, AlarmHigh, Color, LinearA, ExternalChannelNumber)" & _ 
	  "VALUES('CB3C9E39-D3E5-4487-8057-4C7A409FE3F4', 'Ext6', 1, -100, 100, 0, 0, -16744448, 1, 6)"
	objConnection.Execute "INSERT INTO tb_Channel (Channel_Index, Name, IsExternal, YMin, YMax, AlarmLow, AlarmHigh, Color, LinearA, ExternalChannelNumber)" & _ 
	  "VALUES('F4194C31-C66C-4457-844C-7E40416D8856', 'DV', 0, -130, 130, 0, 0, -256, 0, 255)"
End Sub

'=======================================================================================================
'This Function adds flag of backup to tb_Examination table
'=======================================================================================================

Sub AddBackupFlagToExaminationTable()
	objConnection.Execute "ALTER TABLE tb_Examination ADD IsBackup YesNo"
End Sub

Sub RenameInterpretionFieldInPatientRepLayoutTable()
	objConnection.Execute "ALTER TABLE tb_PatientRepLayout ADD Interpretation YesNo"
	objConnection.Execute "UPDATE tb_PatientRepLayout SET Interpretation = Interpretion"
	objConnection.Execute "ALTER TABLE tb_PatientRepLayout DROP COLUMN Interpretion"
End Sub

'=======================================================================================================
'This Function inserts the 3 default events to all VMR and Monitoring studies
'=======================================================================================================
'Sub AddDefaultEvents()
	'Adding default events to the VMR study
'	objRecordSet.Open "SELECT * FROM tb_Study Where Name='VMR'", _
'    objConnection, adOpenStatic, adLockOptimistic
    'If the VMR Study exist	
'    If objRecordSet.RecordCount>0 Then
'		Set objRecordSetMax = CreateObject("ADODB.Recordset")
'		objRecordSetMax.Open "SELECT MAX(IndexInStudy) AS maxIndex FROM tb_Events Where Study_Index=" + objRecordSet.Fields("Study_Index").Value, _
'		objConnection, adOpenStatic, adLockOptimistic
'		Set lastEventNumber = objRecordSetMax.Fields("maxIndex").Value
    
'        s= 	"INSERT INTO tb_Events (Study_Index,Name,IndexInStudy,EventType,Description,Valid) VALUES (objRecordSet.Fields("Study_Index").Value, 'Default 1', lastEventNumber+1, 1, '', -1)"
'	    objConnection.Execute s
'	    s= 	"INSERT INTO tb_Events (Study_Index,Name,IndexInStudy,EventType,Description,Valid) VALUES (objRecordSet.Fields("Study_Index").Value, 'Default 2', lastEventNumber+2, 1, '', -1)"
'	    objConnection.Execute s
'	    s= 	"INSERT INTO tb_Events (Study_Index,Name,IndexInStudy,EventType,Description,Valid) VALUES (objRecordSet.Fields("Study_Index").Value, 'Default 3', lastEventNumber+3, 1, '', -1)"
'	    objConnection.Execute s
'   End If
'    objRecordSet.Close
    
    
'End Sub
'=======================================================================================================
'These functions generates two new Evoked Flow studies (Unilateral and Bilateral)
'=======================================================================================================
Sub InsertEvokedFlowUnilateralStudy()

    objRecordSet.Open "SELECT * FROM tb_Study Where Name='Evoked Flow Unilateral'", _
    objConnection, adOpenStatic, adLockOptimistic
    
  	'If the Evoked Flow Unilateral Study Does not exist Yet	
    If objRecordSet.RecordCount<1 Then
		Set objRecordSet2 = CreateObject("ADODB.Recordset")	
		objRecordSet2.Open "SELECT Name, SerializedData FROM tb_Study WHERE Name = 'Monitoring Intracranial Unilateral'", _
		objConnection, adOpenStatic, adLockOptimistic 
		
        objConnection.Execute "INSERT INTO tb_Study  (Name,DefaultStudy,BasedOn,NumOfProbes,Monitoring,AuthorizationMgr,SpecialInfo ) VALUES ('Evoked Flow Unilateral', 1, '00000000-0000-0000-0000-000000000000', 1, 1, 0, '8')"
        objRecordSet.Close
        objRecordSet.Open "SELECT * FROM tb_Study Where Name='Evoked Flow Unilateral'", _
	    objConnection, adOpenStatic, adLockOptimistic 
        objRecordSet.MoveFirst
        ' Copying the OLE object (The save Layout class ) from  'Monitoring Intracranial Unilateral'
        objRecordSet.Fields("SerializedData").value =objRecordSet2.Fields("SerializedData").value
	    objRecordSet.update
        Set objRecordSet2 = nothing
    End If
   objRecordSet.Close
    
End sub

Sub InsertEvokedFlowBilateralStudy()

    objRecordSet.Open "SELECT * FROM tb_Study Where Name='Evoked Flow Bilateral'", _
    objConnection, adOpenStatic, adLockOptimistic
    
  	'If the Evoked Flow Bilateral Study Does not exist Yet	
    If objRecordSet.RecordCount<1 Then
		Set objRecordSet2 = CreateObject("ADODB.Recordset")	
		objRecordSet2.Open "SELECT Name, SerializedData FROM tb_Study WHERE Name = 'Monitoring Intracranial Bilateral'", _
		objConnection, adOpenStatic, adLockOptimistic 
	
        objConnection.Execute "INSERT INTO tb_Study (Name,DefaultStudy,BasedOn,NumOfProbes,Monitoring,AuthorizationMgr,SpecialInfo ) VALUES ('Evoked Flow Bilateral', 1, '00000000-0000-0000-0000-000000000000', 2, 1, 0, '9')"
	    objRecordSet.Close
        objRecordSet.Open "SELECT * FROM tb_Study Where Name='Evoked Flow Bilateral'", _
	    objConnection, adOpenStatic, adLockOptimistic 
        objRecordSet.MoveFirst
        ' Copying the OLE object (The save Layout class ) from  'Monitoring Intracranial Bilateral'
        objRecordSet.Fields("SerializedData").value =objRecordSet2.Fields("SerializedData").value
	    objRecordSet.update
        Set objRecordSet2 = nothing
    End If
   objRecordSet.Close
    
End sub

sub  InsertRecordsForEvokedFlowStudies()
	
	objRecordSet.Open "SELECT * FROM tb_Study Where Name='Evoked Flow Unilateral'", _
    objConnection, adOpenStatic, adLockOptimistic
    
    Set objRecordSet1 = CreateObject("ADODB.Recordset")
    objRecordSet1.Open "SELECT * FROM tb_Study Where Name='Evoked Flow Bilateral'", _
    objConnection, adOpenStatic, adLockOptimistic
	
	Set objRecordSet2 = CreateObject("ADODB.Recordset")	
    'objRecordSet2.Open "SELECT Blood_Vessel_Index FROM tb_BloodVesselSetup WHERE Name = 'MCA' or Name = 'PCA'", _
    'objConnection, adOpenStatic, adLockOptimistic
    'int i = 0
    'Do Until objRecordSet2.EOF
    
		'objConnection.Execute "INSERT INTO tb_Study_BV  (Study_Index,Blood_Vessel_Index,IndexInStudy,Valid) VALUES (" +objRecordSet.Fields("Study_Index").Value + "," +objRecordSet2.Fields("Blood_Vessel_Index").Value + "," + i + ", 1)"
		'objConnection.Execute "INSERT INTO tb_Study_BV  (Study_Index,Blood_Vessel_Index,IndexInStudy,Valid) VALUES (" +objRecordSet1.Fields("Study_Index").Value + "," +objRecordSet2.Fields("Blood_Vessel_Index").Value + "," + i.Value + ", 1)"
		'i = i + 1
    
    'Loop 
    
    '1) Insert the relevant BVs to the Evoked Flow Unilateral&Bilateral Studies (MCA, PCA)
    objRecordSet2.Open "SELECT Blood_Vessel_Index FROM tb_BloodVesselSetup WHERE Name = 'MCA' and Side = 0", _
    objConnection, adOpenStatic, adLockOptimistic
    objConnection.Execute "INSERT INTO tb_Study_BV  (Study_Index,Blood_Vessel_Index,IndexInStudy,Valid) VALUES (" +objRecordSet.Fields("Study_Index").Value + "," +objRecordSet2.Fields("Blood_Vessel_Index").Value + ",0, 1)"
    objConnection.Execute "INSERT INTO tb_Study_BV  (Study_Index,Blood_Vessel_Index,IndexInStudy,Valid) VALUES (" +objRecordSet1.Fields("Study_Index").Value + "," +objRecordSet2.Fields("Blood_Vessel_Index").Value + ",0, 1)"
    objRecordSet2.Close
    objRecordSet2.Open "SELECT Blood_Vessel_Index FROM tb_BloodVesselSetup WHERE Name = 'PCA' and Side = 0", _
    objConnection, adOpenStatic, adLockOptimistic
    objConnection.Execute "INSERT INTO tb_Study_BV  (Study_Index,Blood_Vessel_Index,IndexInStudy,Valid) VALUES (" +objRecordSet.Fields("Study_Index").Value + "," +objRecordSet2.Fields("Blood_Vessel_Index").Value + ",1, 1)"
    objConnection.Execute "INSERT INTO tb_Study_BV  (Study_Index,Blood_Vessel_Index,IndexInStudy,Valid) VALUES (" +objRecordSet1.Fields("Study_Index").Value + "," +objRecordSet2.Fields("Blood_Vessel_Index").Value + ",2, 1)"
    objRecordSet2.Close
    objRecordSet2.Open "SELECT Blood_Vessel_Index FROM tb_BloodVesselSetup WHERE Name = 'MCA' and Side = 1", _
    objConnection, adOpenStatic, adLockOptimistic
    objConnection.Execute "INSERT INTO tb_Study_BV  (Study_Index,Blood_Vessel_Index,IndexInStudy,Valid) VALUES (" +objRecordSet1.Fields("Study_Index").Value + "," +objRecordSet2.Fields("Blood_Vessel_Index").Value + ",1, 1)"
    objRecordSet2.Close
    objRecordSet2.Open "SELECT Blood_Vessel_Index FROM tb_BloodVesselSetup WHERE Name = 'PCA' and Side = 1", _
    objConnection, adOpenStatic, adLockOptimistic
    objConnection.Execute "INSERT INTO tb_Study_BV  (Study_Index,Blood_Vessel_Index,IndexInStudy,Valid) VALUES (" +objRecordSet1.Fields("Study_Index").Value + "," +objRecordSet2.Fields("Blood_Vessel_Index").Value + ",3, 1)"
    objRecordSet2.Close
     
    '2) Insert the relevant clinical parameters
    Set objRecordSetPeak = CreateObject("ADODB.Recordset")	
	Set objRecordSetMean = CreateObject("ADODB.Recordset")	
	Set objRecordSetPI = CreateObject("ADODB.Recordset")
	Set objRecordSetHITS = CreateObject("ADODB.Recordset")	
	Set objRecordSetDV = CreateObject("ADODB.Recordset")
	
	objRecordSetPeak.Open "SELECT Index FROM tb_ClinicalParam Where Name='Peak'", _
    objConnection, adOpenStatic, adLockOptimistic
    objRecordSetMean.Open "SELECT Index FROM tb_ClinicalParam Where Name='Mean'", _
    objConnection, adOpenStatic, adLockOptimistic
    objRecordSetPI.Open "SELECT Index FROM tb_ClinicalParam Where Name='P.I.'", _
    objConnection, adOpenStatic, adLockOptimistic
    objRecordSetHITS.Open "SELECT Index FROM tb_ClinicalParam Where Name='HITS'", _
    objConnection, adOpenStatic, adLockOptimistic
    objRecordSetDV.Open "SELECT Index FROM tb_ClinicalParam Where Name='DV'", _
    objConnection, adOpenStatic, adLockOptimistic
    
    objConnection.Execute "INSERT INTO tb_Study_ClinicalParam  (ClinicalParam_Index,Study_Index,IndexInStudy,Valid)" & _
		"VALUES (" +objRecordSetPeak.Fields("Index").Value + "," +objRecordSet.Fields("Study_Index").Value + ",0,1)"
	objConnection.Execute "INSERT INTO tb_Study_ClinicalParam  (ClinicalParam_Index,Study_Index,IndexInStudy,Valid)" & _
		"VALUES (" +objRecordSetPeak.Fields("Index").Value + "," +objRecordSet1.Fields("Study_Index").Value + ",0,1)"
	objConnection.Execute "INSERT INTO tb_Study_ClinicalParam  (ClinicalParam_Index,Study_Index,IndexInStudy,Valid)" & _
		"VALUES (" +objRecordSetMean.Fields("Index").Value + "," +objRecordSet.Fields("Study_Index").Value + ",1,1)"
	objConnection.Execute "INSERT INTO tb_Study_ClinicalParam  (ClinicalParam_Index,Study_Index,IndexInStudy,Valid)" & _
		"VALUES (" +objRecordSetMean.Fields("Index").Value + "," +objRecordSet1.Fields("Study_Index").Value + ",1,1)"
	objConnection.Execute "INSERT INTO tb_Study_ClinicalParam  (ClinicalParam_Index,Study_Index,IndexInStudy,Valid)" & _
		"VALUES (" +objRecordSetPI.Fields("Index").Value + "," +objRecordSet.Fields("Study_Index").Value + ",2,1)"
	objConnection.Execute "INSERT INTO tb_Study_ClinicalParam  (ClinicalParam_Index,Study_Index,IndexInStudy,Valid)" & _
		"VALUES (" +objRecordSetPI.Fields("Index").Value + "," +objRecordSet1.Fields("Study_Index").Value + ",2,1)"
	objConnection.Execute "INSERT INTO tb_Study_ClinicalParam  (ClinicalParam_Index,Study_Index,IndexInStudy,Valid)" & _
		"VALUES (" +objRecordSetHITS.Fields("Index").Value + "," +objRecordSet.Fields("Study_Index").Value + ",3,1)"
	objConnection.Execute "INSERT INTO tb_Study_ClinicalParam  (ClinicalParam_Index,Study_Index,IndexInStudy,Valid)" & _
		"VALUES (" +objRecordSetHITS.Fields("Index").Value + "," +objRecordSet1.Fields("Study_Index").Value + ",3,1)"
	objConnection.Execute "INSERT INTO tb_Study_ClinicalParam  (ClinicalParam_Index,Study_Index,IndexInStudy,Valid)" & _
		"VALUES (" +objRecordSetDV.Fields("Index").Value + "," +objRecordSet.Fields("Study_Index").Value + ",4,1)"
	objConnection.Execute "INSERT INTO tb_Study_ClinicalParam  (ClinicalParam_Index,Study_Index,IndexInStudy,Valid)" & _
		"VALUES (" +objRecordSetDV.Fields("Index").Value + "," +objRecordSet1.Fields("Study_Index").Value + ",4,1)"
	
	objRecordSetPeak.Close
	objRecordSetMean.Close
	objRecordSetPI.Close
	objRecordSetHITS.Close
	objRecordSetDV.Close
	
	
	'3) Insert the relevant channels
	
	objRecordSetPeak.Open "SELECT Channel_Index FROM tb_Channel Where Name='Peak'", _
    objConnection, adOpenStatic, adLockOptimistic
    objRecordSetMean.Open "SELECT Channel_Index FROM tb_Channel Where Name='Mean'", _
    objConnection, adOpenStatic, adLockOptimistic
    objRecordSetPI.Open "SELECT Channel_Index FROM tb_Channel Where Name='P.I.'", _
    objConnection, adOpenStatic, adLockOptimistic
    objRecordSetDV.Open "SELECT Channel_Index FROM tb_Channel Where Name='DV'", _
    objConnection, adOpenStatic, adLockOptimistic
    
	objConnection.Execute "INSERT INTO tb_Study_Channel(Study_Index, Channel_Index, IndexInStudy, Valid)" & _
		"VALUES(" + objRecordSet.Fields("Study_Index").Value + "," + objRecordSetPeak.Fields("Channel_Index").Value + ", 0, -1)"
	objConnection.Execute "INSERT INTO tb_Study_Channel(Study_Index, Channel_Index, IndexInStudy, Valid)" & _
		"VALUES(" + objRecordSet.Fields("Study_Index").Value + "," + objRecordSetMean.Fields("Channel_Index").Value + ", 1, -1)"
	objConnection.Execute "INSERT INTO tb_Study_Channel(Study_Index, Channel_Index, IndexInStudy, Valid)" & _
		"VALUES(" + objRecordSet.Fields("Study_Index").Value + "," + objRecordSetPI.Fields("Channel_Index").Value + ", 2, -1)"
	objConnection.Execute "INSERT INTO tb_Study_Channel(Study_Index, Channel_Index, IndexInStudy, Valid)" & _
		"VALUES(" + objRecordSet.Fields("Study_Index").Value + "," + objRecordSetDV.Fields("Channel_Index").Value + ", 3, -1)"
		
	objConnection.Execute "INSERT INTO tb_Study_Channel(Study_Index, Channel_Index, IndexInStudy, Valid)" & _
		"VALUES(" + objRecordSet1.Fields("Study_Index").Value + "," + objRecordSetPeak.Fields("Channel_Index").Value + ", 0, -1)"
	objConnection.Execute "INSERT INTO tb_Study_Channel(Study_Index, Channel_Index, IndexInStudy, Valid)" & _
		"VALUES(" + objRecordSet1.Fields("Study_Index").Value + "," + objRecordSetMean.Fields("Channel_Index").Value + ", 1, -1)"
	objConnection.Execute "INSERT INTO tb_Study_Channel(Study_Index, Channel_Index, IndexInStudy, Valid)" & _
		"VALUES(" + objRecordSet1.Fields("Study_Index").Value + "," + objRecordSetPI.Fields("Channel_Index").Value + ", 2, -1)"
	objConnection.Execute "INSERT INTO tb_Study_Channel(Study_Index, Channel_Index, IndexInStudy, Valid)" & _
		"VALUES(" + objRecordSet1.Fields("Study_Index").Value + "," + objRecordSetDV.Fields("Channel_Index").Value + ", 3, -1)"
		
	objRecordSetPeak.Close
	objRecordSetMean.Close
	objRecordSetPI.Close
	objRecordSetDV.Close
	
    
    '4) Insert the relevant events to the Unilateral&Bilateral Studies (Start Stimulation, Stop Stimulation)
	s= "INSERT INTO tb_Events (Study_Index, Name,IndexInStudy, EventType, Valid ) VALUES(" +objRecordSet.Fields("Study_Index").Value + ",'Start Stimulation',0,1,1)"
	objConnection.Execute s
	s= "INSERT INTO tb_Events (Study_Index, Name,IndexInStudy, EventType, Valid ) VALUES(" +objRecordSet.Fields("Study_Index").Value + ",'Stop Stimulation',1,1,1)"
	objConnection.Execute s
	s= "INSERT INTO tb_Events (Study_Index, Name,IndexInStudy, EventType, Valid ) VALUES(" +objRecordSet1.Fields("Study_Index").Value + ",'Start Stimulation',0,1,1)"
	objConnection.Execute s
	s= "INSERT INTO tb_Events (Study_Index, Name,IndexInStudy, EventType, Valid ) VALUES(" +objRecordSet1.Fields("Study_Index").Value + ",'Stop Stimulation',1,1,1)"
	objConnection.Execute s
	
    objRecordSet1.Close
    objRecordSet.Close
    
end Sub

Sub RenameDIMOXEvent()
	Set objRecordSet1 = CreateObject("ADODB.Recordset")
	objRecordSet1.Open "SELECT * FROM tb_Events WHERE Name = 'DIMOX'", _
	objConnection, adOpenStatic, adLockOptimistic
	Do Until objRecordSet1.EOF
		objRecordSet1.Fields("Name").Value = "DIAMOX"
		objRecordSet1.MoveNext
    Loop
	objRecordSet1.Close
	objRecordSet1.Open "SELECT * FROM tb_ExaminationEvent WHERE EventName = 'DIMOX'", _
	objConnection, adOpenStatic, adLockOptimistic
	Do Until objRecordSet1.EOF
		objRecordSet1.Fields("EventName").Value = "DIAMOX"
		objRecordSet1.MoveNext
    Loop
    objRecordSet1.Close
End Sub

'=======================================================================================================
'This Function adds flag of OnSummaryScreenIndex to tb_GateExamination table
'=======================================================================================================

Sub AddOnSummaryScreenIndexColumnToGateExaminationTable()
	objConnection.Execute "ALTER TABLE tb_GateExamination ADD OnSummaryScreenIndex Long"
End Sub


'=======================================================================================================
'This Function adds Dicom Export flag to tb_AuthorizationMgr table
'=======================================================================================================

Sub AddDicomColumnToAuthorizationMgrTable()
	objConnection.Execute "ALTER TABLE tb_AuthorizationMgr ADD Dicom YesNo"
End Sub

'=======================================================================================================
'This Function adds local AE Title flag to tb_HospitalDetails table
'=======================================================================================================

Sub AddLocalAEColumnToHospitalDetailsTable()
	objConnection.Execute "ALTER TABLE tb_HospitalDetails ADD localAE_Title TEXT(16) NOT NULL DEFAULT 'RIMED'"
End Sub

'=======================================================================================================
'This Function set LayoutRepNum field in tb_BasicConfiguration table to 1 (1 image per page)
'=======================================================================================================

Sub ChangeLayoutRepNumColumnInBasicConfigurationTable()
	objConnection.Execute "ALTER TABLE tb_BasicConfiguration ALTER LayoutRepNum Long NOT NULL DEFAULT '1'"
	objConnection.Execute "UPDATE tb_BasicConfiguration SET LayoutRepNum=1"
End Sub

'=======================================================================================================
'This Function adds Accession Number field to tb_Patient table (for DICOM documents)
'=======================================================================================================

Sub AddAccessionNumberColumnToPatientTable()
	objConnection.Execute "ALTER TABLE tb_Patient ADD AccessionNumber TEXT(64)"
End Sub

'=======================================================================================================
'This Function adds IsTemporary flag to tb_Examination table
'=======================================================================================================

Sub AddTemporaryColumnToExaminationTable()
    fieldExists = false
    
    set rs = CreateObject("ADODB.Recordset")
    rs.Open "SELECT * FROM tb_Examination WHERE 1 = 2", objConnection, adOpenStatic, adLockOptimistic
    For Each fld In rs.Fields
        If fld.Name = strField Then
            FieldExists = True
            Exit For
        End If
    Next

    rs.Close

    If not fieldExists Then 
	    objConnection.Execute "ALTER TABLE tb_Examination ADD IsTemporary YesNo"
    End if
End Sub