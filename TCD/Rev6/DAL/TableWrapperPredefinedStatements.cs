﻿using System;
using Rimed.Framework.Common;
using Rimed.TCD.DAL.DSRimedDBTableAdapters;

namespace Rimed.TCD.DAL
{
	public class TableWrapperPredefinedStatements
	{

		/// <summary>A bridge between the GeneralSettings object and MSAccess database. An intermediary object that populates an table object with data retrieved from a MSAccess database and updates the database to reflect the changes made to the data by using the DataTable object</summary>
		/// <seealso cref="http://support.microsoft.com/kb/308055"/>
		private tb_PredefinedStatementsTableAdapter m_tableAdapter;


		public TableWrapperPredefinedStatements()
		{
			reload();
		}
		

		private bool		reload()
		{
			try
			{
				initTableAdapter();

				DSRimedDB.PredefinedStatementsDataTable.Clear();
				m_tableAdapter.Fill(DSRimedDB.PredefinedStatementsDataTable);

				return true;
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);
				return false;
			}
		}
		
		private void		initTableAdapter()
		{
			m_tableAdapter.TryDispose();
			m_tableAdapter = new tb_PredefinedStatementsTableAdapter();
		}

		public DSRimedDB.tb_PredefinedStatementsRow[] GetRows(int categoryId)
		{
			try
			{
				var rows = DSRimedDB.PredefinedStatementsDataTable.Select(string.Format("{0}={1}", DSRimedDB.PredefinedStatementsDataTable.CategoryIdColumn.ColumnName, categoryId), DSRimedDB.PredefinedStatementsDataTable.AliasColumn.ColumnName);
				if (rows.Length == 0)
					return null;

				return rows as DSRimedDB.tb_PredefinedStatementsRow[];
			}
			catch (Exception ex)
			{
				Logger.LogError(ex, string.Format("PredefinedStatements DB Row load (categoryId={0}) fail.", categoryId));

				return null;
			}

		}

		public void AcceptChanges()
		{
			m_tableAdapter.Update(DSRimedDB.PredefinedStatementsDataTable);
			DSRimedDB.PredefinedStatementsDataTable.AcceptChanges();
		}
		public bool AddRow(string statement, string alias, int categoryId)
		{
			try
			{
				m_tableAdapter.Insert(alias, categoryId, statement);
				return true;
			}
			catch (Exception ex)
			{
				Logger.LogError(ex, string.Format("PredefinedStatements INSERT fail. Alias={0}, CategoryId={1}.", alias, categoryId));
			}

			return false;
		}
		public bool UpdateRow(string statement, string orgAlias, int orgCategoryId)
		{
			try
			{
				m_tableAdapter.Update(statement, orgAlias, orgCategoryId);
				return true;
			}
			catch (Exception ex)
			{
				Logger.LogError(ex, string.Format("PredefinedStatements UPDATE fail. Alias={0}, CategoryId={1}.", orgAlias, orgCategoryId));
			}

			return false;
		}
		public bool deleteRow(string orgAlias, int orgCategoryId)
		{
			try
			{
				m_tableAdapter.Delete(orgAlias, orgCategoryId);
				return true;
			}
			catch (Exception ex)
			{
				Logger.LogError(ex, string.Format("PredefinedStatements DELETE fail. Alias={0}, CategoryId={1}.", orgAlias, orgCategoryId));
			}

			return false;
		}
	}
}
