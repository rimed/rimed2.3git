﻿using System;
using System.Data;
using System.Data.OleDb;
using Rimed.Framework.Common;
using Rimed.TCD.DAL.DSRimedDBTableAdapters;

namespace Rimed.TCD.DAL
{
	public class TableWrapperGeneralSettings
	{
		public static readonly Version EmptyVersion = new Version(0, 0, 0, 0);

		public const string		KEY_DB_VERSION = "DBVersion";
		public const string		KEY_DB_VERSION_DATE = "DBVersionDate";

		private const string	KEY_EXPORT_INTERVAL = "ExportInterval";
		private const int		KEY_EXPORT_INTERVAL_DEFAULT = 30;

		private const string KEY_EXPORT_SEPARATOR = "ExportSeparator";
		private const string KEY_EXPORT_SEPARATOR_DEFAULT = ",";

		private const string KEY_IP_IMAGES_PER_PAGE = "IpImagesPerPage";
		private const int KEY_IP_IMAGES_PER_PAGE_DEFAULT = 0;

		private const string KEY_ONE_PAGE_REPORT = "OnePageReport";
		private const bool KEY_ONE_PAGE_REPORT_DEFAULT = false;

		private const string KEY_SHOW_HITS_EVENT = "ShowHitsEvent";
		private const bool KEY_SHOW_HITS_EVENT_DEFAULT = true;

		private const string KEY_START_PLAY_EVENT = "StartPlayEvent";
		private const int KEY_START_PLAY_EVENT_DEFAULT = 2;

		/// <summary>A bridge between the GeneralSettings object and MSAccess database. An intermediary object that populates an table object with data retrieved from a MSAccess database and updates the database to reflect the changes made to the data by using the DataTable object</summary>
		/// <seealso cref="http://support.microsoft.com/kb/308055"/>
		private tb_GeneralSettingsTableAdapter m_tableAdapter;


		public TableWrapperGeneralSettings()
		{
			reload();
		}
		

		public int		ExportInterval
		{
			get { return getRowValue(KEY_EXPORT_INTERVAL, KEY_EXPORT_INTERVAL_DEFAULT); }
			set { setKeyValue(KEY_EXPORT_INTERVAL, value); }
		}

		public string	ExportSeparator
		{
			get { return getRowValue(KEY_EXPORT_SEPARATOR, KEY_EXPORT_SEPARATOR_DEFAULT); }
			set { setKeyValue(KEY_EXPORT_SEPARATOR, value); }
		}

		public int		IpImagesPerPage
		{
			get { return getRowValue(KEY_IP_IMAGES_PER_PAGE, KEY_IP_IMAGES_PER_PAGE_DEFAULT); }
			set { setKeyValue(KEY_IP_IMAGES_PER_PAGE, value); }
		}

		public bool		IsOnePageReport
		{
			get { return getRowValue(KEY_ONE_PAGE_REPORT, KEY_ONE_PAGE_REPORT_DEFAULT); }
			set { setKeyValue(KEY_ONE_PAGE_REPORT, value); }
		}

		public bool		IsShowHitsEvent
		{
			get { return getRowValue(KEY_SHOW_HITS_EVENT, KEY_SHOW_HITS_EVENT_DEFAULT); }
			set { setKeyValue(KEY_SHOW_HITS_EVENT, value); }
		}

		public int		StartPlayEvent
		{
			get { return getRowValue(KEY_START_PLAY_EVENT, KEY_START_PLAY_EVENT_DEFAULT); }
			set { setKeyValue(KEY_START_PLAY_EVENT, value); }
		}


		public Version	GetDBVersion()
		{
			var dbVer = getRowValue(KEY_DB_VERSION);
			try
			{
				return new Version(dbVer);
			}
			catch (Exception)
			{
				return EmptyVersion;
			}
		}

		public DateTime? GetDBVersionDate()
		{
			var dbVerDate = getRowValue(KEY_DB_VERSION_DATE);
			DateTime dt;
			if (DateTime.TryParse(dbVerDate, out dt))
				return dt;

			return null;
		}


		private bool		reload()
		{
			try
			{
				if (!DSRimedDB.IsGeneralsettingsTableExists())
					return false;

				initNewGeneralSettingsTableAdapter();

				DSRimedDB.GeneralSettingsTable.Clear();
				m_tableAdapter.Fill(DSRimedDB.GeneralSettingsTable);

				return true;
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);
				return false;
			}
		}
		
		private void initNewGeneralSettingsTableAdapter()
		{
			m_tableAdapter.TryDispose();
			m_tableAdapter = new tb_GeneralSettingsTableAdapter();

			// Update command
			m_tableAdapter.Adapter.UpdateCommand.CommandText = "UPDATE tb_GeneralSettings SET KeyValue=? WHERE [Key]=?;";
			m_tableAdapter.Adapter.UpdateCommand.Parameters.Add("@KeyValue", OleDbType.VarWChar, 32, "KeyValue");
			m_tableAdapter.Adapter.UpdateCommand.Parameters.Add("@Key", OleDbType.VarWChar, 32, "[Key]").SourceVersion = DataRowVersion.Original;
		}

		private DSRimedDB.tb_GeneralSettingsRow getRow(string key)
		{
			try
			{
				var rows = DSRimedDB.GeneralSettingsTable.Select(string.Format("Key='{0}'", key));
				if (rows.Length != 1)
					return null;

				return rows[0] as DSRimedDB.tb_GeneralSettingsRow;
			}
			catch (Exception ex)
			{
				Logger.LogError(ex, string.Format("GeneralSettings DB Row load (Key={0}) fail.", key));

				return null;
			}

		}

		private string getRowValue(string key, string defValue = "")
		{
			var row = getRow(key);
			if (row == null || row.KeyValue == null)
				return defValue;

			return row.KeyValue;
		}

		private int getRowValue(string key, int defValue)
		{
			var s = getRowValue(key, defValue.ToString());

			if (string.IsNullOrWhiteSpace(s))
				return defValue;

			int val;
			if (int.TryParse(s, out val))
				return val;

			return defValue;
		}

		private bool getRowValue(string key, bool defValue)
		{
			var s = getRowValue(key, defValue.ToString());

			if (string.IsNullOrWhiteSpace(s))
				return defValue;

			bool val;
			if (bool.TryParse(s, out val))
				return val;

			return defValue;
		}


		private bool setKeyValue(string key, string value)
		{
			if (value == null)
				value = string.Empty;

			var row = getRow(key);
			if (row == null || row.KeyValue == null)
			{
				Logger.LogWarning(string.Format("Key not found. Fail to update key ({0}) value ({1}).", key, value));
				return false;
			}

			try
			{
				row.KeyValue = value;
				m_tableAdapter.Update(row);

				return true;
			}
			catch (Exception ex)
			{
				Logger.LogWarning(ex, string.Format("Fail to update key ({0}) value ({1}).", key, value));

				return false;
			}
		}

		private bool setKeyValue(string key, bool value)
		{
			return setKeyValue(key, value.ToString());
		}

		private bool setKeyValue(string key, int value)
		{
			return setKeyValue(key, value.ToString());
		}
	}
}
