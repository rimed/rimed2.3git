﻿//------------------------------------------------------------------------------
// <autogenerated>
//     This code was generated by a tool.
//     Runtime Version: 1.1.4322.573
//
//     Changes to this file may cause incorrect behavior and will be lost if 
//     the code is regenerated.
// </autogenerated>
//------------------------------------------------------------------------------

namespace TCD2003.DB {
    using System;
    using System.Data;
    using System.Xml;
    using System.Runtime.Serialization;
    
    
    [Serializable()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Diagnostics.DebuggerStepThrough()]
    [System.ComponentModel.ToolboxItem(true)]
    public class dsStudyTrends : DataSet {
        
        private tb_Study_TrendsDataTable tabletb_Study_Trends;
        
        public dsStudyTrends() {
            this.InitClass();
            System.ComponentModel.CollectionChangeEventHandler schemaChangedHandler = new System.ComponentModel.CollectionChangeEventHandler(this.SchemaChanged);
            this.Tables.CollectionChanged += schemaChangedHandler;
            this.Relations.CollectionChanged += schemaChangedHandler;
        }
        
        protected dsStudyTrends(SerializationInfo info, StreamingContext context) {
            string strSchema = ((string)(info.GetValue("XmlSchema", typeof(string))));
            if ((strSchema != null)) {
                DataSet ds = new DataSet();
                ds.ReadXmlSchema(new XmlTextReader(new System.IO.StringReader(strSchema)));
                if ((ds.Tables["tb_Study_Trends"] != null)) {
                    this.Tables.Add(new tb_Study_TrendsDataTable(ds.Tables["tb_Study_Trends"]));
                }
                this.DataSetName = ds.DataSetName;
                this.Prefix = ds.Prefix;
                this.Namespace = ds.Namespace;
                this.Locale = ds.Locale;
                this.CaseSensitive = ds.CaseSensitive;
                this.EnforceConstraints = ds.EnforceConstraints;
                this.Merge(ds, false, System.Data.MissingSchemaAction.Add);
                this.InitVars();
            }
            else {
                this.InitClass();
            }
            this.GetSerializationData(info, context);
            System.ComponentModel.CollectionChangeEventHandler schemaChangedHandler = new System.ComponentModel.CollectionChangeEventHandler(this.SchemaChanged);
            this.Tables.CollectionChanged += schemaChangedHandler;
            this.Relations.CollectionChanged += schemaChangedHandler;
        }
        
        [System.ComponentModel.Browsable(false)]
        [System.ComponentModel.DesignerSerializationVisibilityAttribute(System.ComponentModel.DesignerSerializationVisibility.Content)]
        public tb_Study_TrendsDataTable tb_Study_Trends {
            get {
                return this.tabletb_Study_Trends;
            }
        }
        
        public override DataSet Clone() {
            dsStudyTrends cln = ((dsStudyTrends)(base.Clone()));
            cln.InitVars();
            return cln;
        }
        
        protected override bool ShouldSerializeTables() {
            return false;
        }
        
        protected override bool ShouldSerializeRelations() {
            return false;
        }
        
        protected override void ReadXmlSerializable(XmlReader reader) {
            this.Reset();
            DataSet ds = new DataSet();
            ds.ReadXml(reader);
            if ((ds.Tables["tb_Study_Trends"] != null)) {
                this.Tables.Add(new tb_Study_TrendsDataTable(ds.Tables["tb_Study_Trends"]));
            }
            this.DataSetName = ds.DataSetName;
            this.Prefix = ds.Prefix;
            this.Namespace = ds.Namespace;
            this.Locale = ds.Locale;
            this.CaseSensitive = ds.CaseSensitive;
            this.EnforceConstraints = ds.EnforceConstraints;
            this.Merge(ds, false, System.Data.MissingSchemaAction.Add);
            this.InitVars();
        }
        
        protected override System.Xml.Schema.XmlSchema GetSchemaSerializable() {
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            this.WriteXmlSchema(new XmlTextWriter(stream, null));
            stream.Position = 0;
            return System.Xml.Schema.XmlSchema.Read(new XmlTextReader(stream), null);
        }
        
        internal void InitVars() {
            this.tabletb_Study_Trends = ((tb_Study_TrendsDataTable)(this.Tables["tb_Study_Trends"]));
            if ((this.tabletb_Study_Trends != null)) {
                this.tabletb_Study_Trends.InitVars();
            }
        }
        
        private void InitClass() {
            this.DataSetName = "dsStudyTrends";
            this.Prefix = "";
            this.Namespace = "http://tempuri.org/dsStudyTrends.xsd";
            this.Locale = new System.Globalization.CultureInfo("en-US");
            this.CaseSensitive = false;
            this.EnforceConstraints = true;
            this.tabletb_Study_Trends = new tb_Study_TrendsDataTable();
            this.Tables.Add(this.tabletb_Study_Trends);
        }
        
        private bool ShouldSerializetb_Study_Trends() {
            return false;
        }
        
        private void SchemaChanged(object sender, System.ComponentModel.CollectionChangeEventArgs e) {
            if ((e.Action == System.ComponentModel.CollectionChangeAction.Remove)) {
                this.InitVars();
            }
        }
        
        public delegate void tb_Study_TrendsRowChangeEventHandler(object sender, tb_Study_TrendsRowChangeEvent e);
        
        [System.Diagnostics.DebuggerStepThrough()]
        public class tb_Study_TrendsDataTable : DataTable, System.Collections.IEnumerable {
            
            private DataColumn columnStudy_Index;
            
            private DataColumn columnTrend_Index;
            
            private DataColumn columnIndexInStudy;
            
            private DataColumn columnValid;
            
            internal tb_Study_TrendsDataTable() : 
                    base("tb_Study_Trends") {
                this.InitClass();
            }
            
            internal tb_Study_TrendsDataTable(DataTable table) : 
                    base(table.TableName) {
                if ((table.CaseSensitive != table.DataSet.CaseSensitive)) {
                    this.CaseSensitive = table.CaseSensitive;
                }
                if ((table.Locale.ToString() != table.DataSet.Locale.ToString())) {
                    this.Locale = table.Locale;
                }
                if ((table.Namespace != table.DataSet.Namespace)) {
                    this.Namespace = table.Namespace;
                }
                this.Prefix = table.Prefix;
                this.MinimumCapacity = table.MinimumCapacity;
                this.DisplayExpression = table.DisplayExpression;
            }
            
            [System.ComponentModel.Browsable(false)]
            public int Count {
                get {
                    return this.Rows.Count;
                }
            }
            
            internal DataColumn Study_IndexColumn {
                get {
                    return this.columnStudy_Index;
                }
            }
            
            internal DataColumn Trend_IndexColumn {
                get {
                    return this.columnTrend_Index;
                }
            }
            
            internal DataColumn IndexInStudyColumn {
                get {
                    return this.columnIndexInStudy;
                }
            }
            
            internal DataColumn ValidColumn {
                get {
                    return this.columnValid;
                }
            }
            
            public tb_Study_TrendsRow this[int index] {
                get {
                    return ((tb_Study_TrendsRow)(this.Rows[index]));
                }
            }
            
            public event tb_Study_TrendsRowChangeEventHandler tb_Study_TrendsRowChanged;
            
            public event tb_Study_TrendsRowChangeEventHandler tb_Study_TrendsRowChanging;
            
            public event tb_Study_TrendsRowChangeEventHandler tb_Study_TrendsRowDeleted;
            
            public event tb_Study_TrendsRowChangeEventHandler tb_Study_TrendsRowDeleting;
            
            public void Addtb_Study_TrendsRow(tb_Study_TrendsRow row) {
                this.Rows.Add(row);
            }
            
            public tb_Study_TrendsRow Addtb_Study_TrendsRow(System.Guid Study_Index, System.Guid Trend_Index, int IndexInStudy, bool Valid) {
                tb_Study_TrendsRow rowtb_Study_TrendsRow = ((tb_Study_TrendsRow)(this.NewRow()));
                rowtb_Study_TrendsRow.ItemArray = new object[] {
                        Study_Index,
                        Trend_Index,
                        IndexInStudy,
                        Valid};
                this.Rows.Add(rowtb_Study_TrendsRow);
                return rowtb_Study_TrendsRow;
            }
            
            public tb_Study_TrendsRow FindByStudy_IndexTrend_Index(System.Guid Study_Index, System.Guid Trend_Index) {
                return ((tb_Study_TrendsRow)(this.Rows.Find(new object[] {
                            Study_Index,
                            Trend_Index})));
            }
            
            public System.Collections.IEnumerator GetEnumerator() {
                return this.Rows.GetEnumerator();
            }
            
            public override DataTable Clone() {
                tb_Study_TrendsDataTable cln = ((tb_Study_TrendsDataTable)(base.Clone()));
                cln.InitVars();
                return cln;
            }
            
            protected override DataTable CreateInstance() {
                return new tb_Study_TrendsDataTable();
            }
            
            internal void InitVars() {
                this.columnStudy_Index = this.Columns["Study_Index"];
                this.columnTrend_Index = this.Columns["Trend_Index"];
                this.columnIndexInStudy = this.Columns["IndexInStudy"];
                this.columnValid = this.Columns["Valid"];
            }
            
            private void InitClass() {
                this.columnStudy_Index = new DataColumn("Study_Index", typeof(System.Guid), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnStudy_Index);
                this.columnTrend_Index = new DataColumn("Trend_Index", typeof(System.Guid), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnTrend_Index);
                this.columnIndexInStudy = new DataColumn("IndexInStudy", typeof(int), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnIndexInStudy);
                this.columnValid = new DataColumn("Valid", typeof(bool), null, System.Data.MappingType.Element);
                this.Columns.Add(this.columnValid);
                this.Constraints.Add(new UniqueConstraint("dsStudyTrendsKey1", new DataColumn[] {
                                this.columnStudy_Index,
                                this.columnTrend_Index}, true));
                this.columnStudy_Index.AllowDBNull = false;
                this.columnTrend_Index.AllowDBNull = false;
            }
            
            public tb_Study_TrendsRow Newtb_Study_TrendsRow() {
                return ((tb_Study_TrendsRow)(this.NewRow()));
            }
            
            protected override DataRow NewRowFromBuilder(DataRowBuilder builder) {
                return new tb_Study_TrendsRow(builder);
            }
            
            protected override System.Type GetRowType() {
                return typeof(tb_Study_TrendsRow);
            }
            
            protected override void OnRowChanged(DataRowChangeEventArgs e) {
                base.OnRowChanged(e);
                if ((this.tb_Study_TrendsRowChanged != null)) {
                    this.tb_Study_TrendsRowChanged(this, new tb_Study_TrendsRowChangeEvent(((tb_Study_TrendsRow)(e.Row)), e.Action));
                }
            }
            
            protected override void OnRowChanging(DataRowChangeEventArgs e) {
                base.OnRowChanging(e);
                if ((this.tb_Study_TrendsRowChanging != null)) {
                    this.tb_Study_TrendsRowChanging(this, new tb_Study_TrendsRowChangeEvent(((tb_Study_TrendsRow)(e.Row)), e.Action));
                }
            }
            
            protected override void OnRowDeleted(DataRowChangeEventArgs e) {
                base.OnRowDeleted(e);
                if ((this.tb_Study_TrendsRowDeleted != null)) {
                    this.tb_Study_TrendsRowDeleted(this, new tb_Study_TrendsRowChangeEvent(((tb_Study_TrendsRow)(e.Row)), e.Action));
                }
            }
            
            protected override void OnRowDeleting(DataRowChangeEventArgs e) {
                base.OnRowDeleting(e);
                if ((this.tb_Study_TrendsRowDeleting != null)) {
                    this.tb_Study_TrendsRowDeleting(this, new tb_Study_TrendsRowChangeEvent(((tb_Study_TrendsRow)(e.Row)), e.Action));
                }
            }
            
            public void Removetb_Study_TrendsRow(tb_Study_TrendsRow row) {
                this.Rows.Remove(row);
            }
        }
        
        [System.Diagnostics.DebuggerStepThrough()]
        public class tb_Study_TrendsRow : DataRow {
            
            private tb_Study_TrendsDataTable tabletb_Study_Trends;
            
            internal tb_Study_TrendsRow(DataRowBuilder rb) : 
                    base(rb) {
                this.tabletb_Study_Trends = ((tb_Study_TrendsDataTable)(this.Table));
            }
            
            public System.Guid Study_Index {
                get {
                    return ((System.Guid)(this[this.tabletb_Study_Trends.Study_IndexColumn]));
                }
                set {
                    this[this.tabletb_Study_Trends.Study_IndexColumn] = value;
                }
            }
            
            public System.Guid Trend_Index {
                get {
                    return ((System.Guid)(this[this.tabletb_Study_Trends.Trend_IndexColumn]));
                }
                set {
                    this[this.tabletb_Study_Trends.Trend_IndexColumn] = value;
                }
            }
            
            public int IndexInStudy {
                get {
                    try {
                        return ((int)(this[this.tabletb_Study_Trends.IndexInStudyColumn]));
                    }
                    catch (InvalidCastException e) {
                        throw new StrongTypingException("Cannot get value because it is DBNull.", e);
                    }
                }
                set {
                    this[this.tabletb_Study_Trends.IndexInStudyColumn] = value;
                }
            }
            
            public bool Valid {
                get {
                    try {
                        return ((bool)(this[this.tabletb_Study_Trends.ValidColumn]));
                    }
                    catch (InvalidCastException e) {
                        throw new StrongTypingException("Cannot get value because it is DBNull.", e);
                    }
                }
                set {
                    this[this.tabletb_Study_Trends.ValidColumn] = value;
                }
            }
            
            public bool IsIndexInStudyNull() {
                return this.IsNull(this.tabletb_Study_Trends.IndexInStudyColumn);
            }
            
            public void SetIndexInStudyNull() {
                this[this.tabletb_Study_Trends.IndexInStudyColumn] = System.Convert.DBNull;
            }
            
            public bool IsValidNull() {
                return this.IsNull(this.tabletb_Study_Trends.ValidColumn);
            }
            
            public void SetValidNull() {
                this[this.tabletb_Study_Trends.ValidColumn] = System.Convert.DBNull;
            }
        }
        
        [System.Diagnostics.DebuggerStepThrough()]
        public class tb_Study_TrendsRowChangeEvent : EventArgs {
            
            private tb_Study_TrendsRow eventRow;
            
            private DataRowAction eventAction;
            
            public tb_Study_TrendsRowChangeEvent(tb_Study_TrendsRow row, DataRowAction action) {
                this.eventRow = row;
                this.eventAction = action;
            }
            
            public tb_Study_TrendsRow Row {
                get {
                    return this.eventRow;
                }
            }
            
            public DataRowAction Action {
                get {
                    return this.eventAction;
                }
            }
        }
    }
}
