/************************************************************************/
/*                                                                      */
/*           Copyright 2009 - 2011, Valensum Ltd                        */
/*                       ALL RIGHTS RESERVED                            */
/*                                                                      */
/* DATE CREATED : Sep 15, 2010 BY : MarinaV                             */
/*                                                                      */
/************************************************************
************/
//SOCKET PASCAL FAR socket ( int af, int type, int protocol); 

#include "RmdSocket.h"

using namespace Rimed::TCD::CommunicationLayer::RmdSocket;
using namespace Rimed::TCD::Infrastructure;

CRmdSocket::CRmdSocket(Infra *pInfra, CRmdDebug *pRmdDebug)
{
	m_pInfra =pInfra;
	m_pRmdDebug = pRmdDebug;
	NativeSock = INVALID_SOCKET;
	protocol = RMD_INVALID_PROT;
	RcvBufSize = 0;
	SndBufSize = 0;

	OutputDebugStringA("[RIMED] CRmdSocket::CRmdSocket()");
}

CRmdSocket::~CRmdSocket(void)
{
	OutputDebugStringA("[RIMED] CRmdSocket::~CRmdSocket()");

	if(NativeSock != INVALID_SOCKET) 
		RmdSockClose();
}

RMD_ERROR_CODE CRmdSocket::RmdSockCreate(E_RMD_IP_PROTOCOL ProtocolVal)
{
	DBG_TRACE(m_pRmdDebug, INFO_TRC, "CRmdSocket::RmdSockCreate()");

	int type;
	BOOL SoBroadcastVal = 1;

	switch(ProtocolVal)
	{
		case RMD_TCP_PROT:
				DBG_TRACE(m_pRmdDebug, INFO_TRC, "CRmdSocket::RmdSockCreate(): RMD_TCP_PROT");
				type = SOCK_STREAM;
			break;
		case RMD_UDP_PROT:
				DBG_TRACE(m_pRmdDebug, INFO_TRC, "CRmdSocket::RmdSockCreate(): RMD_UDP_PROT");
				type = SOCK_DGRAM;
			break;
		default:
				DBG_TRACE(m_pRmdDebug, INFO_TRC, "CRmdSocket::RmdSockCreate(): default");
			return RMD_INVALID_PARAM;
	}

	protocol = ProtocolVal;
	NativeSock = socket(PF_INET, type, 0);
	if(NativeSock == INVALID_SOCKET)
	{
		int err = WSAGetLastError();

		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdSocket::RmdSockCreate(): INVALID_SOCKET Socket Create failed (err=%i) -> RMD_ABNORMAL_KERNEL_EXECUTION.", err);

		//throw CRmdException(RMD_ABNORMAL_KERNEL_EXECUTION, err,__LINE__, __FILE__);	//Ofer, 2013.12.29
		return RMD_ABNORMAL_KERNEL_EXECUTION;
	}

	if(ProtocolVal == RMD_UDP_PROT) 
	{
		int err = setsockopt (NativeSock,  SOL_SOCKET, SO_BROADCAST, (const char*)(&SoBroadcastVal), sizeof(BOOL)); 
		if(err!=0) 
		{
			err = WSAGetLastError();

			DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdSocket::RmdSockCreate(): Setup setsockopt SO_BROADCAST failed (err=%i).", err);

			//throw CRmdException(RMD_ABNORMAL_KERNEL_EXECUTION, err,__LINE__, __FILE__);	//Ofer, 2013.12.29
			return RMD_ABNORMAL_KERNEL_EXECUTION;
		}
	}
	/*
	else {
		struct timeval timeout;
		timeout.tv_sec = 20;
		timeout.tv_usec = 0;
		setsockopt( NativeSock, SOL_SOCKET, SO_RCVTIMEO, (char *)(&timeout), sizeof( timeout ) );
	}
	*/

	return RMD_SUCCESS;
}

// Check what happens if the state is not established
RMD_ERROR_CODE CRmdSocket::RmdSockShutdown(void)
{
	DBG_TRACE(m_pRmdDebug, INFO_TRC, "CRmdSocket::RmdSockShutdown()");

	int iRet;
	if(NativeSock == INVALID_SOCKET) 
	{
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdSocket::RmdSockShutdown(): INVALID_SOCKET -> RMD_OBJ_NOT_INITIALIZED");
		return RMD_OBJ_NOT_INITIALIZED;
	}

	iRet = shutdown(NativeSock,2 /*Send FIN, don't allow packets transmit/receive*/);
	if(iRet != 0) 
	{
		int err = WSAGetLastError();

		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdSocket::RmdSockShutdown(): socket shutdown fail (iRet=%i, err=%i).", iRet, err);

		return RMD_ABNORMAL_KERNEL_EXECUTION;
	}

	return RMD_SUCCESS;
}

RMD_ERROR_CODE CRmdSocket::RmdSockSetup(int RcvBufSizeVal, int SndBufSizeVal)
{
	DBG_TRACE(m_pRmdDebug, INFO_TRC, "CRmdSocket::RmdSockSetup()");

	if(NativeSock == INVALID_SOCKET) 
	{
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "RmdSocket::RmdSockSetup(): INVALID_SOCKET -> RMD_OBJ_NOT_INITIALIZED");
		//throw CRmdException(RMD_OBJ_NOT_INITIALIZED, __LINE__, __FILE__);		//Ofer, 2013.12.29
		return RMD_OBJ_NOT_INITIALIZED;
	}

	int iRet = setsockopt(NativeSock,  SOL_SOCKET, SO_RCVBUF, (const char*)(&RcvBufSizeVal), sizeof(int)); 
	if(iRet != 0) 
	{
		int err = WSAGetLastError();

		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdSocket::RmdSockSetup(): Set socket option SO_RCVBUF fail (iRet=%i, err=%i).", iRet, err);

		//throw CRmdException(RMD_ABNORMAL_KERNEL_EXECUTION, err,__LINE__, __FILE__);		//Ofer, 2013.12.29
		return RMD_ABNORMAL_KERNEL_EXECUTION;
	}
	
	RcvBufSize = RcvBufSizeVal;
	
	iRet = setsockopt (NativeSock,  SOL_SOCKET, SO_SNDBUF, (const char*)(&SndBufSizeVal), sizeof(int)); 
	if(iRet!=0) 
	{
		int err = WSAGetLastError();

		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdSocket::RmdSockSetup(): Set socket option SO_SNDBUF fail (iRet=%i, err=%i).", iRet, err);

		//throw CRmdException(RMD_ABNORMAL_KERNEL_EXECUTION, err,__LINE__, __FILE__);		//Ofer, 2013.12.29
		return RMD_ABNORMAL_KERNEL_EXECUTION;
	}
	
	SndBufSize = SndBufSizeVal;

	return RMD_SUCCESS;
}

RMD_ERROR_CODE CRmdSocket::RmdSockClose(void)
{
	DBG_TRACE(m_pRmdDebug, INFO_TRC, "CRmdSocket::RmdSockClose()");

	if(NativeSock == INVALID_SOCKET) 
	{
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdSocket::RmdSockClose(): INVALID_SOCKET -> RMD_OBJ_NOT_INITIALIZED");
		return RMD_OBJ_NOT_INITIALIZED;
	}
	
	int iRet = closesocket(NativeSock);
	if(iRet != 0) 
	{
		int err = WSAGetLastError();

		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdSocket::RmdSockClose(): closesocket failed (iRet=%1, err=%i).", iRet, err);

		return RMD_ABNORMAL_KERNEL_EXECUTION;
	}

	NativeSock = INVALID_SOCKET;

	return RMD_SUCCESS;
}

RMD_ERROR_CODE CRmdSocket::RmdSockBind(unsigned short Port, unsigned long Addr)
{
	DBG_TRACE(m_pRmdDebug, INFO_TRC, "CRmdSocket::RmdSockBind()");

	if(NativeSock == INVALID_SOCKET) 
	{
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdSocket::RmdSockBind(): INVALID_SOCKET -> RMD_OBJ_NOT_INITIALIZED");

		//throw CRmdException(RMD_OBJ_NOT_INITIALIZED, __LINE__, __FILE__);		2013.12.29
		return RMD_OBJ_NOT_INITIALIZED;
	}

	SOCKADDR_IN *addr = (SOCKADDR_IN *)(&RmdSockAddr.NativeAddr); // The address structure for a TCP socket

    addr->sin_family = AF_INET;      // Address family
	addr->sin_port = htons (Port);   // Assign port to this socket

    //Accept a connection from any IP using INADDR_ANY equal to pass inet_addr("0.0.0.0"). If you want only to watch for a connection from a specific IP, specify it instead.
    addr->sin_addr.s_addr = htonl (Addr);  

	RmdSockAddr.addrlen =sizeof(SOCKADDR_IN);
    int iRet = bind(NativeSock, &RmdSockAddr.NativeAddr, RmdSockAddr.addrlen);
    if(iRet!=0)
	 {
		int err = WSAGetLastError();

		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdSocket::RmdSockBind(): Socket bind failed (iRet=%1, err=%i).", iRet, err);

		//throw CRmdException(RMD_ABNORMAL_KERNEL_EXECUTION, err,__LINE__, __FILE__);	2013.12.29
		return RMD_ABNORMAL_KERNEL_EXECUTION;
    }

	return RMD_SUCCESS;
}

RMD_ERROR_CODE CRmdSocket::RmdSockTcpServerListen(int BackLog)
{
	if(NativeSock == INVALID_SOCKET) 
	{
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdSocket::RmdSockTcpServerListen(): INVALID_SOCKET -> RMD_OBJ_NOT_INITIALIZED");

		//throw CRmdException(RMD_OBJ_NOT_INITIALIZED, __LINE__, __FILE__);		Ofer, 2013.12.29
		return RMD_OBJ_NOT_INITIALIZED;
	}

	int iRet = listen(NativeSock, BackLog);
	if(iRet != 0) 
	{
		int err = WSAGetLastError();

		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdSocket::RmdSockTcpServerListen(): set socket to listen state fail (iRet=%i, err=%i).", iRet, err);

		//throw CRmdException(RMD_ABNORMAL_KERNEL_EXECUTION, err,__LINE__, __FILE__);	Ofer, 2013.12.29
		return RMD_ABNORMAL_KERNEL_EXECUTION;
	}
	return RMD_SUCCESS;
}

void CRmdSocket::RmdSockGetAddr(RMD_SOCK_ADDR *DestAddr)
{
	memcpy(DestAddr, &RmdSockAddr, sizeof(RMD_SOCK_ADDR));
}

RMD_ERROR_CODE CRmdSocket::RmdSockTcpServerAccept(CRmdSocket *pListenSocket)
{
	RmdSockAddr.addrlen	= sizeof(NATIVE_SOCK_ADDR);
	int iAddrLen		= RmdSockAddr.addrlen;
	NativeSock			= accept(pListenSocket->NativeSock, &RmdSockAddr.NativeAddr, &iAddrLen);
	RmdSockAddr.addrlen = iAddrLen;
	if(NativeSock == INVALID_SOCKET) 
	{
		int iRet = WSAGetLastError();

		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdSocket::RmdSockTcpServerAccept(): Socket accept error (err=%i).", iRet);

		//throw CRmdException(RMD_ABNORMAL_KERNEL_EXECUTION, iRet,__LINE__, __FILE__);
		return RMD_ABNORMAL_KERNEL_EXECUTION;
	}

	return RMD_SUCCESS;
}

RMD_ERROR_CODE CRmdSocket::UdpSend(RMD_SOCK_ADDR *DestAddr, char *pData, int DataLen)
{
	if(NativeSock == INVALID_SOCKET) 
	{
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdSocket::UdpSend(): INVALID_SOCKET -> RMD_OBJ_NOT_INITIALIZED");

		return RMD_OBJ_NOT_INITIALIZED;
	}

	int iRet = sendto(NativeSock, pData, DataLen, 0, &DestAddr->NativeAddr,	DestAddr->addrlen);
	if(iRet != DataLen) //if(iRet != 0)
	{
		int err = WSAGetLastError();
		
		if (err != 0)
		{
			DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdSocket::UdpSend(): socket sendTo fail (DataLen=%i, iRet=%i, err=%i).", DataLen, iRet, err);
			//throw CRmdException(RMD_ABNORMAL_KERNEL_EXECUTION, err,__LINE__, __FILE__);	Ofer, 2013.12.29

			return RMD_ABNORMAL_KERNEL_EXECUTION;
		}
	}	

	return RMD_SUCCESS;
}

void CRmdSocket::SnprintfNativeAddr(RMD_SOCK_ADDR *RmdSockAddr, char *buf, const size_t len)
{
	SOCKADDR_IN *addr = (SOCKADDR_IN *)(&RmdSockAddr->NativeAddr);
	sprintf_s(buf, len, "%pI4, port=%u", &addr->sin_addr, ntohs(addr->sin_port));
}

void CRmdSocket::RmdSockAddrInit(RMD_SOCK_ADDR *RmdSockAddr, USHORT Port, ULONG val)
{
	SOCKADDR_IN *addr = (SOCKADDR_IN *)(&(RmdSockAddr->NativeAddr));
	addr->sin_family = AF_INET;      // Address family
	addr->sin_port = htons(Port);   // Assign port to this socket

//Accept a connection from any IP using INADDR_ANY
//You could pass inet_addr("0.0.0.0") instead to accomplish the 
//same thing. If you want only to watch for a connection from a 
//specific IP, specify that //instead.
	addr->sin_addr.s_addr = htonl (val);  
	RmdSockAddr->addrlen =sizeof(SOCKADDR_IN);
}

RMD_ERROR_CODE CRmdSocket::RmdSockRecvFrom(char *pData, int DataLen, RMD_SOCK_ADDR *pFromAddr, int *piRxLen)
{
	pFromAddr->addrlen = sizeof(SOCKADDR_IN);
	int addrLen = pFromAddr->addrlen;
	if(NativeSock==INVALID_SOCKET) 
	{
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdSocket::RmdSockRecvFrom(): INVALID_SOCKET -> RMD_OBJ_NOT_INITIALIZED");

		//throw CRmdException(RMD_OBJ_NOT_INITIALIZED, __LINE__, __FILE__);		Ofer, 2013.12.29
		return RMD_OBJ_NOT_INITIALIZED;
	}

	int iRet = recvfrom(NativeSock, pData, DataLen, /*MSG_WAITALL*/0, &pFromAddr->NativeAddr, &addrLen);
	if(iRet<0) 
	{
		int err = WSAGetLastError();

		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdSocket::RmdSockRecvFrom(): socket recvFrom error (iRet=%i, err=%i).", iRet, err);

		//throw CRmdException(RMD_ABNORMAL_KERNEL_EXECUTION, err,__LINE__, __FILE__);	Ofer, 2013.12.29
		return RMD_ABNORMAL_KERNEL_EXECUTION;
	}	
	else if(iRet==0) 
	{
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdSocket::RmdSockRecvFrom(): RMD_SOCKET_CLOSED");

		//throw CRmdException(RMD_SOCKET_CLOSED, iRet,__LINE__, __FILE__);	Ofer, 2013.12.29
		return RMD_SOCKET_CLOSED;
	}	

	pFromAddr->addrlen = addrLen;
	*piRxLen = iRet;
	return RMD_SUCCESS;
}

RMD_ERROR_CODE CRmdSocket::RmdSockRecv(char *pData, int DataLen, int *piRxLen)
{
	if(NativeSock == INVALID_SOCKET) 
	{
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdSocket::RmdSockRecv(): INVALID_SOCKET -> RMD_OBJ_NOT_INITIALIZED");

		//throw CRmdException(RMD_OBJ_NOT_INITIALIZED, __LINE__, __FILE__);	Ofer, 2013.12.29
		return RMD_OBJ_NOT_INITIALIZED;
	}

	int iRet = recv(NativeSock, pData, DataLen, /*MSG_WAITALL*/0);	
	if(iRet < 0) 
	{
		int err = WSAGetLastError();

		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdSocket::RmdSockRecv(): socket recv error (iRet=%i, err=%i).", iRet, err);

		//throw CRmdException(RMD_ABNORMAL_KERNEL_EXECUTION, err,__LINE__, __FILE__); 
		return RMD_SOCKET_RECEIVE_ERROR;
	}	
	else if (iRet == 0) 
	{
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdSocket::RmdSockRecv(): RMD_SOCKET_CLOSED");

		//throw CRmdException(RMD_SOCKET_CLOSED, iRet,__LINE__, __FILE__);	Ofer, 2013.12.29
		return RMD_SOCKET_CLOSED;
	}	

	*piRxLen = iRet;
	return RMD_SUCCESS;
}

RMD_ERROR_CODE CRmdSocket::RmdSockSend(char *pData, int DataLen)
{
	if(NativeSock==INVALID_SOCKET) 
	{
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdSocket::RmdSockSend(): INVALID_SOCKET -> RMD_OBJ_NOT_INITIALIZED");

		//throw CRmdException(RMD_OBJ_NOT_INITIALIZED, __LINE__, __FILE__);	Ofer, 2013.12.29
		return RMD_OBJ_NOT_INITIALIZED;
	}

	int iRet = send(NativeSock, pData, DataLen, /*MSG_WAITALL*/0);
	if(iRet == SOCKET_ERROR) 
	{
		int err = WSAGetLastError();

		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdSocket::RmdSockSend(): Socket send error (iRet=%i, err=%i).", iRet, err);

		//throw CRmdException(RMD_ABNORMAL_KERNEL_EXECUTION, err,__LINE__, __FILE__);	Ofer, 2013.12.29
		return RMD_ABNORMAL_KERNEL_EXECUTION;
	}	

	return RMD_SUCCESS;
}

RMD_ERROR_CODE CRmdSocket::RmdSockSendTo(char *pData, int DataLen, const char *DestAddrStr)
{
	SOCKADDR_IN DestAddr;
	SOCKADDR_IN *InternalAddr = (SOCKADDR_IN *)(&RmdSockAddr.NativeAddr);

	DestAddr.sin_family = InternalAddr->sin_family;
	DestAddr.sin_port = InternalAddr->sin_port;
	DestAddr.sin_addr.s_addr = inet_addr(DestAddrStr);

	if(NativeSock==INVALID_SOCKET) 
	{
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdSocket::RmdSockSendTo(): INVALID_SOCKET -> RMD_OBJ_NOT_INITIALIZED");

		//throw CRmdException(RMD_OBJ_NOT_INITIALIZED, __LINE__, __FILE__);	Ofer, 2013.12.29
		return RMD_OBJ_NOT_INITIALIZED;
	}

	int iRet = sendto(NativeSock, pData, DataLen,/*MSG_WAITALL*/0,(SOCKADDR *)(&DestAddr), sizeof(DestAddr));
	if(iRet == SOCKET_ERROR) 
	{
		int err = WSAGetLastError();

		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdSocket::RmdSockSendTo(): socket sendTo error (iRet=%i, err=%i).", iRet, err);

		//throw CRmdException(RMD_ABNORMAL_KERNEL_EXECUTION, err,__LINE__, __FILE__);	Ofer, 2013.12.29
		return RMD_ABNORMAL_KERNEL_EXECUTION;
	}	

	return RMD_SUCCESS;
}

RMD_ERROR_CODE CRmdSocket::RmdSockTcpClientConnect( RMD_SOCK_ADDR *pServerAddr)
{
	if(NativeSock == INVALID_SOCKET) 
	{
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdSocket::RmdSockTcpClientConnect(): INVALID_SOCKET -> RMD_OBJ_NOT_INITIALIZED");

		//throw CRmdException(RMD_OBJ_NOT_INITIALIZED, __LINE__, __FILE__);	Ofer, 2013.12.29
		return RMD_OBJ_NOT_INITIALIZED;
	}

	int iRet = connect(NativeSock, &pServerAddr->NativeAddr, pServerAddr->addrlen);
	if(iRet!=0) 
	{
		int err = WSAGetLastError();

		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdSocket::RmdSockTcpClientConnect(): socket connect error (iRet=%i, err=%i).", iRet, err);

		//throw CRmdException(RMD_ABNORMAL_KERNEL_EXECUTION, err,__LINE__, __FILE__);	Ofer, 2013.12.29
		return RMD_ABNORMAL_KERNEL_EXECUTION;
	}	

	return RMD_SUCCESS;
}

void RmdSockAddrInit(RMD_SOCK_ADDR *RmdSockAddr, USHORT port, ULONG val)
{
	SOCKADDR_IN *addr = (SOCKADDR_IN *)(&RmdSockAddr->NativeAddr); // The address structure for a TCP socket
	addr->sin_family = AF_INET;      // Address family
	addr->sin_port = htons (port);   // Assign port to this socket
	addr->sin_addr.s_addr = val;
	RmdSockAddr->addrlen = sizeof(SOCKADDR_IN);
}

#if 0
void RmdSockAddrCpy(struct RMD_SOCK_ADDR *vl_dest_addr, struct RMD_SOCK_ADDR *vl_src_addr)
{
	vl_dest_addr->addrlen = vl_src_addr->addrlen;
	memcpy(&vl_dest_addr->NativeAddr, &vl_src_addr->NativeAddr, vl_src_addr->addrlen);
}

vl_status_t RmdSockAddrSetPort(struct RMD_SOCK_ADDR *vl_addr, unsigned short port)
{
	struct sockaddr_in *sin;
	struct sockaddr_in6 *sin6;
	int expected_len;

	if(vl_addr->addrlen < sizeof(short)){
		return RMD_BAD_INPUT;
	}

	switch(vl_addr->NativeAddr.ss_family) {
		case PF_INET:
			expected_len = sizeof(struct sockaddr_in);
			if(vl_addr->addrlen < expected_len) {
				//v2.0.5.1, Ofer DBG_TRACE(m_pRmdDebug, DL_TRACE, "Bad address len=%ld, expected=%d", vl_addr->addrlen, expected_len);
				return RMD_BAD_INPUT;
			}
			sin = (struct sockaddr_in *) (&vl_addr->NativeAddr);
			sin->sin_port = htons(port);
			//v2.0.5.1, Ofer DBG_TRACE(m_pRmdDebug, DL_TRACE, "Port=%hu, sin_port=%hu",port, sin->sin_port)
			break;
		case PF_INET6:
			expected_len = sizeof(struct sockaddr_in6);
			if(vl_addr->addrlen < expected_len)
				return RMD_BAD_INPUT;
			sin6 = (struct sockaddr_in6 *) (&vl_addr->NativeAddr);
			sin6->sin6_port = htons(port);
			break;
		default:
			return RMD_BAD_INPUT;
	}
	return RMD_SUCCESS;
}

vl_status_t vl_ip_sock_addr_u2k_cfg_set(struct RMD_SOCK_ADDR *vl_addr, char *buff, size_t len)
{
	struct sockaddr_in *sin;
	struct sockaddr_in6 *sin6;
	unsigned short *port;
	int buff_offset = 0;
	int field_size = sizeof(short);
	int expected_len;

	if((len < field_size) || (len > 0 && buff == NULL)) {
		vl_addr->addrlen = 0;
		return RMD_BAD_INPUT;
	}

	if(buff != NULL) {
		memcpy(&vl_addr->NativeAddr.ss_family, buff, field_size);
		buff_offset += field_size;
		switch(vl_addr->NativeAddr.ss_family) {
			case PF_INET:
				expected_len = sizeof(struct sockaddr_in);
				if(len < expected_len) {
					//v2.0.5.1, Ofer DBG_TRACE(m_pRmdDebug, DL_TRACE, "Bad address len=%ld, expected=%d", len, expected_len);
					return RMD_BAD_INPUT;
				}
				port = (unsigned short *) (&buff[buff_offset]);
				sin = (struct sockaddr_in *) (&vl_addr->NativeAddr);
				sin->sin_port = htons(*port);
				//v2.0.5.1, Ofer DBG_TRACE(m_pRmdDebug, DL_TRACE, "Port=%hu, sin_port=%hu",*port, sin->sin_port);
				buff_offset += field_size;
				field_size = len - buff_offset;
				memcpy(&sin->sin_addr, &buff[buff_offset], field_size);
				break;
			case PF_INET6:
				expected_len = sizeof(struct sockaddr_in6);
				if(len < expected_len)
					return RMD_BAD_INPUT;
				port = (unsigned short *) (&buff[buff_offset]);
				sin6 = (struct sockaddr_in6 *) (&vl_addr->NativeAddr);
				sin6->sin6_port = htons(*port);
				buff_offset += field_size;
				field_size = sizeof(struct in6_addr);
				memcpy(&sin6->sin6_addr, &buff[buff_offset], field_size);
				break;
			default:
				return RMD_BAD_INPUT;
		}
	}
	vl_print_NativeAddr_full(&vl_addr->NativeAddr);
	vl_addr->addrlen = buff_offset + field_size;
	return RMD_SUCCESS;
}

NATIVE_SOCK_ADDR *vl_sock_get_NativeAddr(struct RMD_SOCK_ADDR *vl_addr, size_t *len)
{
	*len = vl_addr->addrlen;
	return &vl_addr->NativeAddr;
}

bool vl_is_NativeAddr_equal_sock_addr(NATIVE_SOCK_ADDR *NativeAddr1, size_t len, struct RMD_SOCK_ADDR* sock_addr2)
{
	int i;
	if(sock_addr2->addrlen != len) {
		//v2.0.5.1, Ofer DBG_TRACE(m_pRmdDebug, DL_LOUD_TRACE,"sock_addr2->addrlen=%ld, len=%ld",sock_addr2->addrlen, len );
		return false;
	}
	if(NativeAddr1->ss_family != sock_addr2->NativeAddr.ss_family) {
		//v2.0.5.1, Ofer DBG_TRACE(m_pRmdDebug, DL_LOUD_TRACE,"sock_addr2->NativeAddr.family=%d, NativeAddr1.ss_family=%d",sock_addr2->NativeAddr.ss_family, NativeAddr1->ss_family);
		return false;
	}
	if(memcmp(&(NativeAddr1->__data[2]), &(sock_addr2->NativeAddr.__data[2]), len - 4) == 0)
		return true;

	//v2.0.5.1, Ofer for(i = 2; i < len - 2; i++) {
	//v2.0.5.1, Ofer 	DBG_TRACE(m_pRmdDebug, DL_LOUD_TRACE,"sock_addr2[%d]=%d, NativeAddr1=%d", i, sock_addr2->NativeAddr.__data[i], NativeAddr1->__data[i]);
	//v2.0.5.1, Ofer }
	return false;
}

bool vl_is_sock_addrs_equal(struct RMD_SOCK_ADDR *addr1, struct RMD_SOCK_ADDR* addr2)
{
	return vl_is_NativeAddr_equal_sock_addr(&addr1->NativeAddr, addr1->addrlen, addr2);
}

bool vl_is_sock_ports_equal(struct RMD_SOCK_ADDR *addr1, struct RMD_SOCK_ADDR* addr2)
{
	if (memcmp(addr1->NativeAddr.__data, addr2->NativeAddr.__data, 2) == 0)
		return true;
	return false;
}

void vl_print_NativeAddr_full(NATIVE_SOCK_ADDR *addr)
{
	unsigned short *port = (unsigned short *) addr->__data;
	int i;
	//v2.0.5.1, Ofer DBG_TRACE(m_pRmdDebug, DL_TRACE, "family=%hu, port=%hu, ", addr->ss_family, *port);
	//v2.0.5.1, Ofer for(i = 2; i < 6; i++)
	//v2.0.5.1, Ofer 	DBG_TRACE(m_pRmdDebug, DL_TRACE, ".addr[%d] = %d ",i,addr->__data[i]);
}

char *vl_sock_snprintf_vl_addr(struct RMD_SOCK_ADDR *vl_addr, char *buf, const size_t len)
{
	return vl_sock_snprintf_NativeAddr(&vl_addr->NativeAddr, buf, len);
}

char *vl_sock_snprintf_name(struct vl_socket *vl_sock, char *buf, const size_t len)
{
	return vl_sock_snprintf_NativeAddr((struct __kernel_sockaddr_storage *)&vl_sock->sock_name, buf, len);
}

void vl_sock_restore_callbacks(struct vl_socket *vl_sock)
{
	struct sock *sk = vl_sock->native_sock->sk;
	//v2.0.5.1, Ofer DBG_TRACE(m_pRmdDebug, DL_TRACE,"---->");
	if(vl_sock->sk_o_callbacks_valid) {
		sk->sk_state_change = vl_sock->sk_ostate;
		sk->sk_data_ready = vl_sock->sk_odata;
		sk->sk_write_space = vl_sock->sk_owspace;
		sk->sk_error_report = vl_sock->sk_oerror_report;
		sk->sk_user_data = NULL;
		vl_sock->sk_o_callbacks_valid = false;
	}
	//v2.0.5.1, Ofer DBG_TRACE(m_pRmdDebug, DL_TRACE,"<----");
}

void vl_sock_tcp_ip_server_conn_shutdown(struct vl_socket *vl_sock)
{
	struct sock *sk;
	//v2.0.5.1, Ofer DBG_TRACE(m_pRmdDebug, DL_TRACE,"---->");
	if(vl_sock->native_sock == NULL) {
		//v2.0.5.1, Ofer DBG_TRACE(m_pRmdDebug, DL_TRACE,"<----Socket is not inititalized");
		return;
	}
	sk = vl_sock->native_sock->sk;
	vl_sock_tcp_ip_server_shutdown(vl_sock);
	kernel_sock_shutdown(sk->sk_socket, SHUT_RDWR);
	//v2.0.5.1, Ofer DBG_TRACE(m_pRmdDebug, DL_TRACE,"<----");
}

vl_status_t vl_sock_ip_client_bind(struct vl_socket *vl_sock,
                                   short min_port,
                                   short max_port,
                                   short *out_port)
{
	short port;
	int err = -EADDRINUSE;
	vl_status_t status = RMD_FAIL;

	if(vl_sock->native_sock == NULL)
		return RMD_NOT_INITIALIZED;

	for(port = min_port; port<max_port && err==-EADDRINUSE ; port++) {
		err = RMD_SOCK_ADDRany_bind(vl_sock, port);
		if(err==0) {
			//v2.0.5.1, Ofer DBG_TRACE(m_pRmdDebug, DL_TRACE, "bind port=%hd",port);
			*out_port = port;
			status = RMD_SUCCESS;
		}
		//v2.0.5.1, Ofer else
		//v2.0.5.1, Ofer 	DBG_TRACE(m_pRmdDebug, DL_TRACE, "RMD_SOCK_ADDRany_bind failed err=%d port=%hd",err, port);
	}
	//v2.0.5.1, Ofer DBG_TRACE(m_pRmdDebug, DL_TRACE, "<----status=%ld, min port=%hd max port=%hd",status, min_port, max_port);
	return status;
}


void vl_sock_tcp_ip_client_print_window(struct vl_socket *vl_sock)
{
	struct sock *inet;
	inet = vl_sock->native_sock->sk;
	//v2.0.5.1, Ofer DBG_TRACE(m_pRmdDebug, DL_TRACE, "window_clamp=%u snd_wnd=%u rcv_ssthresh=%u advmss=%u\nsndbuf=%d rcvbuf=%d", tcp_sk(inet)->window_clamp, tcp_sk(inet)->snd_wnd, tcp_sk(inet)->rcv_ssthresh, tcp_sk(inet)->advmss, inet->sk_sndbuf, inet->sk_rcvbuf);
}

vl_status_t vl_sock_ip_client_setup(struct vl_socket *vl_sock,
                                   E_RMD_SOCK_STATE_change_cb_t sk_state_change,
                                   struct RMD_SOCK_ADDR *vl_addr)
{

	struct sock *sk;
	struct sockaddr *s_addr = (struct sockaddr *) &vl_addr->NativeAddr;

	if(vl_sock->native_sock == NULL)
		return RMD_NOT_INITIALIZED;
	sk = vl_sock->native_sock->sk;
	write_lock_bh(&sk->sk_callback_lock);
	vl_sock->state_change = sk_state_change;

	vl_sock->sock_name.sa_family = s_addr->sa_family;
	memcpy(&vl_sock->sock_name.sa_data, s_addr->sa_data, vl_addr->addrlen-sizeof(sa_family_t));
	vl_sock->sock_name_len = vl_addr->addrlen;

	sk->sk_data_ready = vl_sock_tcp_ip_client_data_ready;
	sk->sk_state_change = vl_sock_tcp_ip_client_state_change;
	sk->sk_write_space = vl_sock_tcp_ip_client_write_space;
	//	sk->sk_error_report = vl_sock_error_report;
	sk->sk_allocation = GFP_ATOMIC;

	/* socket options */
	sk->sk_userlocks |= SOCK_BINDPORT_LOCK;
	sock_reset_flag(sk, SOCK_LINGER);
	tcp_sk(sk)->linger2 = 0;
	write_unlock_bh(&sk->sk_callback_lock);
	return RMD_SUCCESS;
}

#endif