/************************************************************************/
/*                                                                      */
/*           Copyright 2009 - 2011, Valensum Ltd                        */
/*                       ALL RIGHTS RESERVED                            */
/*                                                                      */
/* DATE CREATED : Sep 15, 2010 BY : MarinaV                             */
/*                                                                      */
/************************************************************************/

#pragma once

#include "Infra.h"
#include <winsock2.h>

typedef struct sockaddr NATIVE_SOCK_ADDR;

#define NATIVE_ADDR_STR_LEN 10
#define NATIVE_ADDR_MAX_BUFLEN	(63U)

typedef struct {
	NATIVE_SOCK_ADDR	NativeAddr;	/* peer address */
	size_t			      addrlen;
} RMD_SOCK_ADDR;

typedef enum {
   RMD_TCP_PROT, /* Transmission Control Protocol.  */
   RMD_UDP_PROT,
	RMD_INVALID_PROT
} E_RMD_IP_PROTOCOL;

using namespace Rimed::TCD::Infrastructure;

namespace Rimed
{
	namespace TCD
{
	namespace CommunicationLayer
	{
		namespace RmdSocket
		{
			class CRmdSocket {
			public:
				CRmdSocket(Infra *pInfra, CRmdDebug *pRmdDebug);
				~CRmdSocket();

				RMD_ERROR_CODE RmdSockCreate(E_RMD_IP_PROTOCOL protocol);
				RMD_ERROR_CODE RmdSockShutdown(void);
				RMD_ERROR_CODE RmdSockSetup(int RcvBufSizeVal, int SndBufSizeVal);
				RMD_ERROR_CODE RmdSockClose(void);
				RMD_ERROR_CODE RmdSockBind(unsigned short Port, unsigned long Addr);
				RMD_ERROR_CODE RmdSockTcpServerListen(int BackLog);
				RMD_ERROR_CODE RmdSockTcpServerAccept(CRmdSocket *pListenSocket);
				RMD_ERROR_CODE UdpSend(RMD_SOCK_ADDR *DestAddr, char *pData, int DataLen);
				RMD_ERROR_CODE RmdSockRecv(char *pData, int DataLen, int *piRxLen);
				RMD_ERROR_CODE RmdSockRecvFrom(char *pData, int DataLen, RMD_SOCK_ADDR *pFromAddr, int *piRxLen);
				RMD_ERROR_CODE RmdSockSend(char *pData, int DataLen);
				RMD_ERROR_CODE RmdSockSendTo(char *pData, int DataLen, const char *DestAddrStr);
				RMD_ERROR_CODE RmdSockTcpClientConnect( RMD_SOCK_ADDR *pServerAddr);
				void RmdSockGetAddr(RMD_SOCK_ADDR *DestAddr);
				void SnprintfNativeAddr(RMD_SOCK_ADDR *RmdSockAddr, char *buf, const size_t len);
				void RmdSockAddrInit(RMD_SOCK_ADDR *RmdSockAddr, USHORT port, ULONG val);
			private:
				Infra *m_pInfra;
				CRmdDebug *m_pRmdDebug;
				E_RMD_IP_PROTOCOL protocol;
				SOCKET NativeSock;
				RMD_SOCK_ADDR RmdSockAddr;
				int SndBufSize;
				int RcvBufSize;
			};
		};
	};
}
}

#if 0
vl_status_t vl_sock_create(struct vl_socket *vl_sock,
                           ,
									struct RMD_SOCK_ADDR *vl_addr,
									void *vl_sock_user_data,
									//void(*sk_state_change)(void *param),
                           vl_sock_data_ready_cb_t sk_data_ready,
                           vl_sock_write_space_cb_t sk_write_space,
                           unsigned int bufsize);
typedef enum {
	RMD_TCP_ESTABLISHED = TCP_ESTABLISHED,
	RMD_TCP_SYN_SENT = TCP_SYN_SENT,
	RMD_TCP_SYN_RECV = TCP_SYN_RECV,
	RMD_TCP_FIN_WAIT1 = TCP_FIN_WAIT1,
	RMD_TCP_FIN_WAIT2 = TCP_FIN_WAIT2,
	RMD_TCP_TIME_WAIT = TCP_TIME_WAIT,
	RMD_TCP_CLOSE = TCP_CLOSE,
	RMD_TCP_CLOSE_WAIT = TCP_CLOSE_WAIT,
	RMD_TCP_LAST_ACK = TCP_LAST_ACK,
	RMD_TCP_LISTEN = TCP_LISTEN,
	RMD_TCP_CLOSING = TCP_CLOSING,	/* Now a valid state */
	RMD_TCP_MAX_STATES = TCP_MAX_STATES	/* Leave at the end! */
} E_RMD_SOCK_STATE;

typedef void (*E_RMD_SOCK_STATE_change_cb_t)(void *param, enum E_RMD_SOCK_STATE state);
typedef void (*vl_sock_data_ready_cb_t)(void *param, int bytes);
typedef void (*vl_sock_write_space_cb_t)(void *param);

struct vl_socket {
	struct socket	*native_sock;	/* berkeley socket layer */
	int		family;
	struct sockaddr sock_name;
	int	sock_name_len;
	void (*sk_ostate)(struct sock *);
	void (*sk_odata)(struct sock *, int bytes);
	void (*sk_owspace)(struct sock *);
	void (*sk_oerror_report)(struct sock *sk);
	bool sk_o_callbacks_valid;
	E_RMD_SOCK_STATE_change_cb_t state_change;
	vl_sock_data_ready_cb_t data_ready;
	vl_sock_write_space_cb_t write_space;
	void *cb_param;
	/* private TCP part */
	/* u32			sk_reclen;	 length of record */
	/* u32			sk_tcplen;	 current read length */
	unsigned int max_mesg;
};

#define RMD_SOCK_DEFAULTS	(0U)
#define RMD_SOCK_ANONYMOUS	(1U << 0)	/* don't register with pmap */
#define RMD_SOCK_TEMPORARY	(1U << 1)	/* flag socket as temporary */

void RmdSockAddrInit(struct RMD_SOCK_ADDR *vl_addr,
							  NATIVE_SOCK_ADDR  *NativeAddr,
							  size_t			       len);
void RmdSockAddrCpy(struct RMD_SOCK_ADDR *vl_dest_addr, struct RMD_SOCK_ADDR *vl_src_addr);
vl_status_t RmdSockAddrSetPort(struct RMD_SOCK_ADDR *vl_addr, unsigned short port);
vl_status_t vl_ip_sock_addr_u2k_cfg_set(struct RMD_SOCK_ADDR *vl_addr,
                                        char                *buff,
													 size_t			     len);
NATIVE_SOCK_ADDR *vl_sock_get_NativeAddr(struct RMD_SOCK_ADDR *vl_addr, size_t *len);
bool vl_is_NativeAddr_equal_sock_addr(NATIVE_SOCK_ADDR *NativeAddr1, size_t len, struct RMD_SOCK_ADDR* addr2);
bool vl_is_sock_ports_equal(struct RMD_SOCK_ADDR *addr1, struct RMD_SOCK_ADDR* addr2);
bool vl_is_sock_addrs_equal(struct RMD_SOCK_ADDR *addr1, struct RMD_SOCK_ADDR* addr2);
void vl_sprintf_NativeAddr(NATIVE_SOCK_ADDR *addr, char *str);
void vl_print_NativeAddr_full(NATIVE_SOCK_ADDR *addr);
char *vl_sock_snprintf_NativeAddr(NATIVE_SOCK_ADDR *addr, char *buf, const size_t len);
char *vl_sock_snprintf_name(struct vl_socket *vl_sock, char *buf, const size_t len);
char *vl_sock_snprintf_vl_addr(struct RMD_SOCK_ADDR *vl_addr, char *buf, const size_t len);
void vl_sock_shutdown(struct vl_socket *vl_sock);
void vl_sock_reclaim(struct vl_socket *vl_sock);
void vl_sock_reset(struct vl_socket *vl_sock);

vl_status_t vl_sock_tcp_ip_server_bind(struct vl_socket *vl_sock, struct RMD_SOCK_ADDR *vl_addr);
vl_status_t vl_sock_tcp_ip_server_listen(struct vl_socket *vl_sock);
vl_status_t vl_sock_tcp_ip_server_accept(struct vl_socket *vl_sock_listen,
                                         struct vl_socket *vl_sock_new_conn,
                                         void *vl_sock_user_data,
                                         E_RMD_SOCK_STATE_change_cb_t sk_state_change,
                                         vl_sock_data_ready_cb_t sk_data_ready,
                                         vl_sock_write_space_cb_t sk_write_space,
                                         struct RMD_SOCK_ADDR *out_peer_addr);
void vl_sock_tcp_ip_server_conn_shutdown(struct vl_socket *vl_sock);
void vl_sock_tcp_ip_server_shutdown(struct vl_socket *vl_sock);

int vl_sock_send_page(struct vl_socket *vl_sock, vl_kernel_page_t *native_page, int offset, size_t size, int flags);
int vl_sock_rx_packet(struct vl_socket *vl_sock,
                      vl_mem_vec_t *mem_vec,
                      size_t mem_vec_len,
                      size_t total_mem_len);

vl_status_t vl_sock_ip_client_bind(struct vl_socket *vl_sock,
                                   short min_port,
                                   short max_port,
                                   short *out_port);
vl_status_t vl_sock_tcp_ip_client_connect(struct vl_socket *vl_sock);
vl_status_t vl_sock_ip_client_setup(struct vl_socket *vl_sock,
                                   E_RMD_SOCK_STATE_change_cb_t sk_state_change,
                                   struct RMD_SOCK_ADDR *vl_addr);

void vl_sock_tcp_ip_client_shutdown(struct vl_socket *vl_sock);
enum E_RMD_SOCK_STATE vl_sock_tcp_ip_state_get(struct vl_socket *vl_sock);
socket_state vl_socket_state_get(struct vl_socket *vl_sock);
int vl_sock_tx_packet(struct vl_socket *vl_sock, struct RMD_SOCK_ADDR *vl_addr, vl_mem_vec_t *mem_vec, size_t mem_vec_len, size_t total_mem_len, unsigned flags);
void vl_sock_tx_notify_no_more(struct vl_socket *vl_sock,struct RMD_SOCK_ADDR *vl_addr);

#endif