#pragma once

#include "RmdTcpServer.h"
#include "Frame.h"
#include "RmdSync.h"
#include "UdpConn.h"

#define RMD_ADVERTISE_MSG_STR "Rimed Digilite Init"
#define RMD_ADVERTISE_MSG_LEN 50
#define RMD_ADVERTISE_PORT_DEFAULT 1030
#define RMD_TCP_SERVER_PORT_DEFAULT 1045
#define RMD_UDP_PORT_DEFAULT 1046
#define RMD_TCP_MAX_MSG_LEN (4096-28)
//#define RMD_UDP_MAX_MSG_LEN 1400
#define RMD_UDP_MAX_MSG_LEN 1464
#define RMD_TCP_MAX_RX_MSGS_LIST_SIZE 5

namespace Rimed
{
	namespace TCD
	{
		using namespace Infrastructure;
		namespace CommunicationLayer
		{
			using namespace RmdTcpServer;
			class CEthMgr
			{
			public:
				CEthMgr(Infra *pInfra, CRmdDebug *pRmdDebug);
				~CEthMgr();
				virtual RMD_ERROR_CODE EthMgrStart(USHORT usPort, ULONG ulServerAddr);
				virtual RMD_ERROR_CODE EthMgrStop(void);
				RMD_ERROR_CODE EthMgrFrameRx(char* pData, int iMaxDataLen, USHORT usMsgType, int *pOutDataLen);
				virtual RMD_ERROR_CODE EthMgrRxHwInfoResponse(char* pData, int iMaxDataLen, USHORT usMsgType, int *pOutDataLen);
				bool EthMgrFrameRxReady(USHORT usMsgType);
				virtual RMD_ERROR_CODE EthMgrFrameTx(char* pData, int iDataLen, USHORT usMsgType);
				virtual int TcpServerGetNumOfClients(void);
				void ClearRxFramesFifo();

			protected:
				Infra *m_pInfra;
				CRmdDebug *m_pRmdDebug;
				//CFrame *m_pRxDsp2PcFrameReady;

				std::list <CFrame*> m_FifoRxDsp2PcFrameReady;
				CFrame *m_pRxDsp2PcFramePending;
				CDspMsg *m_pRxHwInfoMsg;
				BOOL m_bStop;

				void EthMgrProcessMsg(CDspMsg* pDspMsg);
				virtual RMD_ERROR_CODE EthMgrAllRx(void);
			//private:
				static DWORD WINAPI AdvertiseThreadFunc(LPVOID lpThreadParameter);
			
				CRmdTcpServer *m_pRmdTcpServer;
				CUdpConn *m_UdpConn;

				CRmdThread *m_pAdvertiseThread;
				CRmdSocket *m_pRmdAdvertiseSocket;
				USHORT m_AdvertisePort;
				unsigned int m_TxFrameNum;
			};
		}
	}
}