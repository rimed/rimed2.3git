#pragma once

#include "RmdSocket.h"
#include "ListDspMsgs.h"
#include "RmdSync.h"

using namespace Rimed::TCD::CommunicationLayer::RmdSocket;

namespace Rimed
{
	namespace TCD
	{
		namespace CommunicationLayer
		{
			// This class provides only peer2peer implementation for working with RimedDSP
			// 

			namespace RmdTcpServer
			{
				class CRmdTcpServer {
				public:
					CRmdTcpServer(Infra *pInfra, CRmdDebug *pRmdDebug);
					~CRmdTcpServer();

					RMD_ERROR_CODE RmdTcpServerStart(int BackLog, USHORT Port, ULONG ulBindAddr, int iMaxMsgSize, unsigned int uiMaxNumOfRxMsgs);
					RMD_ERROR_CODE RmdTcpServerStop(void);

					int GetNumOfConnections();

					RMD_ERROR_CODE TcpServerMsgRx(CDspMsg** ppDspMsg);
					RMD_ERROR_CODE TcpServerMsgTx(CDspMsg* pDspMsg);

					void TcpServerGetClientAddr(RMD_SOCK_ADDR *DestAddr);
				private:
					static DWORD WINAPI RxThreadFunc(LPVOID lpThreadParameter);
					static DWORD WINAPI TxThreadFunc(LPVOID lpThreadParameter);
	#define RMD_MSG_TCP_SERVER_BASE RMD_MSG_INFRA_MAX+100

					enum {
						RMD_MSG_TCP_SERVER_LISTEN = RMD_MSG_TCP_SERVER_BASE,	//=1225 (= RMD_MSG_INFRA_MAX + 100)
						RMD_MSG_TCP_SERVER_SEND,								//=1226
						RMD_MSG_TCP_SERVER_SEND_DISCONNECT,						//=1227
						RMD_MSG_TCP_SERVER_RECEIVE,								//=1228
						RMD_MSG_TCP_SERVER_MAX									//=1229
					};
	#if RMD_MSG_TCP_SERVER_MAX > RMD_MSG_MAX
	#error "Messages ID overflow -review definitions"
	#endif
					Infra *m_pInfra;
					CRmdDebug *m_pRmdDebug;
					CRmdSocket *m_pRmdListenSocket;
					CRmdSocket *m_pRmdConnectionSocket;
					//CRmdMutex *m_pSmMutex;
					CRmdThread *m_pRxThread;
					CRmdThread *m_pTxThread;
					USHORT m_usPort;
					ULONG m_ulBindAddr;
					int m_iBackLog;
					int m_iNumOfConnections;
					int m_iMaxMsgSize;
					unsigned int m_uiMaxNumOfRxMsgs;
					CListDspMsgsSafe *m_ListOfRxMsgs;
					CListDspMsgsSafe *m_ListOfTxMsgs;

					enum E_TCP_SERVER_STATE{
						TCP_SERVER_IDLE_STATE,
						TCP_SERVER_LISTEN_STATE,
						TCP_SERVER_CONNECTED_STATE,
						TCP_SERVER_FINISH_STATE,
					};
					enum E_TCP_SERVER_EVENT{
						TCP_SERVER_INIT_EVENT,
						TCP_SERVER_ACCEPT_EVENT,
						TCP_SERVER_CLOSE_EVENT,
						TCP_SERVER_CLOSE_CONFIRM_EVENT,
						TCP_SERVER_TX_EVENT,
						TCP_SERVER_RX_EVENT,
					};
					E_TCP_SERVER_STATE TcpServerState;
					RMD_ERROR_CODE RmdTcpServerSMGo(E_TCP_SERVER_EVENT event);
					RMD_ERROR_CODE RmdTcpServerSMGoIdleState(E_TCP_SERVER_EVENT TcpServerEvent);
					RMD_ERROR_CODE RmdTcpServerSMGoListenState(E_TCP_SERVER_EVENT TcpServerEvent);
					RMD_ERROR_CODE RmdTcpServerSMGoConnectedState(E_TCP_SERVER_EVENT TcpServerEvent);
					RMD_ERROR_CODE RmdTcpServerSMGoFinishState(E_TCP_SERVER_EVENT TcpServerEvent);
					RMD_ERROR_CODE RxMessageFunc (unsigned int uMsg, unsigned int uParam, unsigned long ulParam);
					RMD_ERROR_CODE TxMessageFunc (unsigned int uMsg, unsigned int uParam, unsigned long ulParam);

					CRITICAL_SECTION m_cs;
					int m_id;
					static int s_Counter;
				};
			};
		};
	}
}