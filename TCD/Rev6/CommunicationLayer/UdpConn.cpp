#include "UdpConn.h"

using namespace Rimed::TCD::Infrastructure;
using namespace Rimed::TCD::CommunicationLayer;
using namespace RmdSocket;

#define RMD_UDP_THREAD_FINALIZE_TIMEOUT 100
#define RMD_UDP_MTX_TIMEOUT 100

#define RMD_UDP_RX_BUFFER_SIZE	66796
#define RMD_UDP_TX_BUFFER_SIZE	65536

#define UDP_TX_DEST_ADDR "10.0.0.40"

CUdpConn::CUdpConn(Infra *pInfra, CRmdDebug *pRmdDebug)
{
	m_pInfra = pInfra;
	m_pRmdDebug = pRmdDebug;
	m_pRmdSocket = NULL;
	m_ListOfRxMsgs = NULL;
	m_ListOfTxMsgs = NULL;
	m_RxThread = NULL;
	m_TxThread = NULL;

//	m_uiRxMsgCounter = 0;

	INFRA_NEW_OBJ(m_pInfra, m_ListOfRxMsgs, CListDspMsgsSafe, m_pInfra, m_pRmdDebug);
	INFRA_NEW_OBJ(m_pInfra, m_ListOfTxMsgs, CListDspMsgsSafe, m_pInfra, m_pRmdDebug);

	DBG_TRACE(m_pRmdDebug, INFO_TRC, "CUdpConn::CUdpConn()");
}

CUdpConn::~CUdpConn()
{
	DBG_TRACE(m_pRmdDebug, INFO_TRC, "CUdpConn::~CUdpConn()");

	//DBG_ASSERT(m_pRmdDebug,m_pRmdSocket==NULL);	Ofer, 2013.10.03
	INFRA_DELETE_OBJ(m_pInfra,  m_ListOfRxMsgs, CListDspMsgsSafe);
	INFRA_DELETE_OBJ(m_pInfra,  m_ListOfTxMsgs, CListDspMsgsSafe);
}

RMD_ERROR_CODE CUdpConn::UdpConnCreate(USHORT BindPort, ULONG ulBindAddr, int iMaxMsgSize, unsigned int MaxNumOfRxMsgs)
{
	DBG_TRACE(m_pRmdDebug, INFO_TRC, "CUdpConn::UdpConnCreate()");

	RMD_ERROR_CODE RmdErr;
	DWORD ThreadId;

	m_iMaxMsgSize = iMaxMsgSize;
	m_ulBindAddr = ulBindAddr;
	m_BindPort = BindPort;
	m_uiMaxNumOfRxMsgs = MaxNumOfRxMsgs;

	try 
	{
		RmdErr= m_pInfra->SocketLibStart();
		if (RmdErr != RMD_SUCCESS)		//Ofer, 2013.12.29
			return RmdErr;

		INFRA_NEW_OBJ(m_pInfra, m_pRmdSocket, CRmdSocket, m_pInfra, m_pRmdDebug);
		m_pRmdSocket->RmdSockCreate(RMD_UDP_PROT);
		if (RmdErr != RMD_SUCCESS)		//Ofer, 2013.12.29
			return RmdErr;

		RmdErr = m_pRmdSocket->RmdSockBind(m_BindPort, INADDR_ANY);
		if (RmdErr != RMD_SUCCESS)		//Ofer, 2013.12.29
			return RmdErr;

		RmdErr = m_pRmdSocket->RmdSockSetup(RMD_UDP_RX_BUFFER_SIZE,RMD_UDP_TX_BUFFER_SIZE);		
		if (RmdErr != RMD_SUCCESS)		//Ofer, 2013.12.29
			return RmdErr;
	}
	catch(CRmdException &RmdExc)
	{
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CUdpConn::UdpConnCreate().EXCEPTION (err=%i, Inner=%i, File=%s, Line=%i).", RmdExc.m_dwErrorCode, RmdExc.m_dwOriginalCode, RmdExc.m_acFileName, RmdExc.m_dwLine);
	}

	INFRA_NEW_OBJ(m_pInfra, m_ListOfRxMsgs, CListDspMsgsSafe, m_pInfra, m_pRmdDebug);
	INFRA_NEW_OBJ(m_pInfra, m_ListOfTxMsgs, CListDspMsgsSafe, m_pInfra, m_pRmdDebug);

	INFRA_NEW_OBJ(m_pInfra, m_RxThread, CRmdThread, m_pInfra, m_pRmdDebug);
	INFRA_NEW_OBJ(m_pInfra, m_TxThread, CRmdThread, m_pInfra, m_pRmdDebug);

	m_RxThread->RmdThreadStart(RxThreadFunc, this, &ThreadId);
	DBG_TRACE(m_pRmdDebug, INFO_TRC, "CUdpConn::UdpConnCreate(): RxThread Id=%i.", ThreadId);

	m_TxThread->RmdThreadStart(TxThreadFunc, this, &ThreadId);
	DBG_TRACE(m_pRmdDebug, INFO_TRC, "CUdpConn::UdpConnCreate(): TxThread Id=%i.", ThreadId);

	return RMD_SUCCESS;
}

RMD_ERROR_CODE CUdpConn::UdpConnDelete(void)
{
	DBG_TRACE(m_pRmdDebug, INFO_TRC, "CUdpConn::UdpConnDelete()");

	RMD_ERROR_CODE RmdErr;
	if(m_pRmdSocket!=NULL) 
	{
		RmdErr = m_pRmdSocket->RmdSockClose();
	}

	if(m_RxThread!=NULL) 
	{
		m_RxThread->RmdThreadStop();
		m_RxThread->RmdThreadFinalize(RMD_UDP_THREAD_FINALIZE_TIMEOUT);
		INFRA_DELETE_OBJ(m_pInfra, m_RxThread, CRmdThread);
		m_RxThread=NULL;
	}

	if(m_TxThread!=NULL) 
	{
		m_TxThread->RmdThreadStop();
		m_TxThread->RmdThreadFinalize(RMD_UDP_THREAD_FINALIZE_TIMEOUT);
		INFRA_DELETE_OBJ(m_pInfra, m_TxThread, CRmdThread);
		m_TxThread=NULL;
	}

	m_pRmdSocket = NULL;
	m_ListOfRxMsgs->ListDspMsgsClear();
	m_ListOfTxMsgs->ListDspMsgsClear();

	return RMD_SUCCESS;
}

void CUdpConn::RxThread()
{
	OutputDebugStringA("[RIMED] CUdpConn::RxThread(): THREAD START");

	 unsigned int uMsg, uParam;
	 unsigned long ulParam;
	 int RxLen = 0;

	 unsigned int prevFrameNum = 0;
	 unsigned int prevPacketNum = 0;

	 BOOL boMsg;
	 E_RMD_THREAD_STATUS eThreadStatus = m_RxThread->RmdRunTimeFn(INFINITE);
	 CDspMsg *pDspMsg = NULL;
	 RMD_ERROR_CODE RmdErr;

	 INFRA_NEW_OBJ(m_pInfra, pDspMsg, CDspMsg, m_pInfra, m_pRmdDebug);
	 RmdErr = pDspMsg->DspMsgInit(m_iMaxMsgSize);

	 while(eThreadStatus == RMD_THREAD_RUNNING)
	 {
		boMsg = m_RxThread->RmdPeekMsg (&uMsg,&uParam, &ulParam);
		if(boMsg && uMsg==RMD_MSG_KILL)
		{
			OutputDebugStringA("[RIMED] CUdpConn::RxThread(): RMD_MSG_KILL message recieved. THREAD ABORT");
			break;	//return;
		}

		try 
		{
			RxLen  = 0;
			RmdErr = m_pRmdSocket->RmdSockRecv(pDspMsg->m_pcMem,pDspMsg->m_iMemSz, &RxLen);
			if (RmdErr == RMD_SOCKET_RECEIVE_ERROR)
			{
				OutputDebugStringA("[RIMED] CUdpConn::RxThread(): RMD_SOCKET_RECEIVE_ERROR (-37) recieved. THREAD ABORT");
				break;	
			}

			if (RmdErr != RMD_SUCCESS)
				continue;

			m_ListOfRxMsgs->ListDspMsgsPushBackSafe(pDspMsg, RMD_UDP_MTX_TIMEOUT);

			//if (pDspMsg->m_pDspMsgHeader->ucPacketNum == 0)
			//{
			//	if (m_uiRxMsgCounter % 1000 == 0)
			//	{
			//		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "DSP-to-PC UDP Msg: %i, %i, %i, %i"
			//										, m_uiRxMsgCounter
			//										, pDspMsg->m_pDspMsgHeader->FrameNum
			//										, pDspMsg->m_pDspMsgHeader->ucPacketNum
			//										, pDspMsg->m_pDspMsgHeader->uiTimestamp
			//			 );
			//	}
	
			//	m_uiRxMsgCounter++;
			//}

			//if (pDspMsg->m_pDspMsgHeader->ucPacketNum == 0 || pDspMsg->m_pDspMsgHeader->ucPacketNum == pDspMsg->m_pDspMsgHeader->ucTotalPackets -1)
			//{
			//	DBG_TRACE(m_pRmdDebug, ERROR_TRC, "DSP-to-PC UDP Msg: Type=%i, TS=%i, Size=%i, Frame=%i, Packet=%i/%i."
			//										, pDspMsg->m_pDspMsgHeader->usMsgType
			//										, pDspMsg->m_pDspMsgHeader->uiTimestamp
			//										, pDspMsg->m_pDspMsgHeader->usEffectivePacketSize
			//										, pDspMsg->m_pDspMsgHeader->FrameNum
			//										, pDspMsg->m_pDspMsgHeader->ucPacketNum
			//										, pDspMsg->m_pDspMsgHeader->ucTotalPackets
			//			);
			//}

			//if (prevPacketNum > 0 && prevPacketNum < pDspMsg->m_pDspMsgHeader->ucTotalPackets -1 &&  prevFrameNum != pDspMsg->m_pDspMsgHeader->FrameNum && prevPacketNum != pDspMsg->m_pDspMsgHeader->ucPacketNum)
			//{
			//	DBG_TRACE(m_pRmdDebug, ERROR_TRC, "DSP-to-PC UDP Msg: Frame=%i, Packet=%i. Prv.Frame=%d, Prev.Packet=%d.<<<<<< FRAME MISMATCH"
			//										, pDspMsg->m_pDspMsgHeader->FrameNum
			//										, pDspMsg->m_pDspMsgHeader->ucPacketNum
			//										, prevFrameNum
			//										, prevPacketNum
			//			);
			//}
			//if (prevFrameNum == pDspMsg->m_pDspMsgHeader->FrameNum && prevPacketNum +1 != pDspMsg->m_pDspMsgHeader->ucPacketNum)
			//{
			//	DBG_TRACE(m_pRmdDebug, ERROR_TRC, "DSP-to-PC UDP Msg: Frame=%i, Packet=%i, Prev.Packet=%d.<<<<<< PACKET MISMATCH"
			//										, pDspMsg->m_pDspMsgHeader->FrameNum
			//										, pDspMsg->m_pDspMsgHeader->ucPacketNum
			//										, prevPacketNum
			//			);
			//}

			//prevFrameNum	= pDspMsg->m_pDspMsgHeader->FrameNum;
			//prevPacketNum	= pDspMsg->m_pDspMsgHeader->ucPacketNum;

			INFRA_NEW_OBJ(m_pInfra, pDspMsg, CDspMsg, m_pInfra, m_pRmdDebug);
			RmdErr = pDspMsg->DspMsgInit(m_iMaxMsgSize);

			eThreadStatus = m_RxThread->RmdRunTimeFn(0);
		}
		catch(CRmdException &RmdExc)
		{
			DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CUdpConn::RxThread().EXCEPTION (err=%i, Inner=%i, File=%s, Line=%i) thread aborted.", RmdExc.m_dwErrorCode, RmdExc.m_dwOriginalCode, RmdExc.m_acFileName, RmdExc.m_dwLine); //Added 2013.12.30 

			break;	//return;
			//eThreadStatus = m_RxThread->RmdRunTimeFn(INFINITE);
		  }
	 }

	OutputDebugStringA("[RIMED] CUdpConn::RxThread(): THREAD END");
}

void CUdpConn::TxThread()
{
	OutputDebugStringA("[RIMED] CUdpConn::TxThread(): THREAD START");

	 unsigned int uMsg, uParam;
	 unsigned long ulParam;
	 int RxLen = 0;
	 BOOL boMsg;
	 E_RMD_THREAD_STATUS eThreadStatus = m_TxThread->RmdRunTimeFn(INFINITE);
	 CDspMsg *pDspMsg = NULL;
	 RMD_ERROR_CODE RmdErr;

	 //sockaddr_in RecvAddr;

	 INFRA_NEW_OBJ(m_pInfra, pDspMsg, CDspMsg, m_pInfra, m_pRmdDebug);
	 RmdErr =pDspMsg->DspMsgInit(m_iMaxMsgSize);
	 while(eThreadStatus == RMD_THREAD_RUNNING)
	 {
		boMsg = m_TxThread->RmdPeekMsg (&uMsg,&uParam, &ulParam);
		if(boMsg && uMsg==RMD_MSG_KILL)
		{
			OutputDebugStringA("[RIMED] UdpConn::TxThread(): RMD_MSG_KILL message recieved. THREAD ABORT.");
			break;	//return;
		}

		try 
		{
			pDspMsg = m_ListOfTxMsgs->ListDspMsgsPopFrontSafe(RMD_UDP_MTX_TIMEOUT);
			while(pDspMsg != NULL) 
			{
				RmdErr = m_pRmdSocket->RmdSockSendTo(pDspMsg->m_pcMem, pDspMsg->m_iDataLen, UDP_TX_DEST_ADDR);
				if (RmdErr == RMD_SUCCESS)		//Ofer, 2013.12.29
				{
					INFRA_DELETE_OBJ(m_pInfra, pDspMsg, CDspMsg);

					//Send blocking
					pDspMsg = m_ListOfTxMsgs->ListDspMsgsPopFrontSafe(RMD_UDP_MTX_TIMEOUT);
				}
			}
		}
		catch(CRmdException &RmdExc)
		{
			DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CUdpConn::TxThread().EXCEPTION (err=%i, Inner=%i, File=%s, Line=%i).", RmdExc.m_dwErrorCode, RmdExc.m_dwOriginalCode, RmdExc.m_acFileName, RmdExc.m_dwLine);

			 break;		//return;
			// eThreadStatus = m_TxThread->RmdRunTimeFn(INFINITE);
		}
	 }

	 OutputDebugStringA("[RIMED] CUdpConn::TxThread(): THREAD END");
}

DWORD WINAPI CUdpConn::RxThreadFunc(LPVOID lpThreadParameter)
{
	CUdpConn *pThis = (CUdpConn*)lpThreadParameter;
	pThis->RxThread();
	return 0;
}

DWORD WINAPI CUdpConn::TxThreadFunc(LPVOID lpThreadParameter)
{
	CUdpConn *pThis = (CUdpConn*)lpThreadParameter;
	pThis->TxThread();
	return 0;
}


RMD_ERROR_CODE CUdpConn::UdpConnMsgRx(CDspMsg** ppDspMsg)
{
	RMD_ERROR_CODE RmdErr;
	*ppDspMsg = m_ListOfRxMsgs->ListDspMsgsPopFrontSafe(RMD_UDP_MTX_TIMEOUT);
	if(*ppDspMsg==NULL)
		RmdErr = RMD_LIST_EMPTY;
	else
		RmdErr = RMD_SUCCESS;

	m_RxThread->RmdPostMsg(RMD_MSG_EMPTY, 0,0);
	return RmdErr;
}

RMD_ERROR_CODE CUdpConn::UdpConnMsgTx(CDspMsg* pDspMsg)
 {
	 m_ListOfTxMsgs->ListDspMsgsPushBackSafe(pDspMsg, RMD_UDP_MTX_TIMEOUT);
	 m_TxThread->RmdPostMsg(RMD_MSG_EMPTY, 0,0);  
	 return RMD_SUCCESS;
 }