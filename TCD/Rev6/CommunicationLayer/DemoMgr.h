#pragma once

#include "EthMgr.h"
#include "Frame.h"
#include "RmdSync.h"
#include "TCD_DSP2PC.H"

namespace Rimed
{
	namespace TCD
	{
		using namespace Rimed::TCD::DspBlock;
		using namespace Infrastructure;
		namespace CommunicationLayer
		{
			class CDemoMgr : public CEthMgr
			{
			public:
				CDemoMgr(Infra *pInfra, CRmdDebug *pRmdDebug, R_Auto_Scan_Column *AutoScanColArr, int AutoScanColArrLength);
				~CDemoMgr();

				virtual RMD_ERROR_CODE EthMgrStart(USHORT usPort, ULONG ulServerAddr);
				virtual RMD_ERROR_CODE EthMgrStop(void);
				virtual RMD_ERROR_CODE EthMgrFrameTx(char* pData, int iDataLen, USHORT usMsgType);
				virtual int TcpServerGetNumOfClients(void);
				virtual RMD_ERROR_CODE EthMgrRxHwInfoResponse(char* pData, int iMaxDataLen, USHORT usMsgType, int *pOutDataLen);
			protected:
				virtual RMD_ERROR_CODE EthMgrAllRx(void);

			private:		
				CRmdThread *m_pThread;
				static DWORD WINAPI DemoThreadFunc(LPVOID lpThreadParameter);
				DWORD DemoThread();
				CListDspMsgsSafe *m_ListOfRxMsgs;

				RMD_ERROR_CODE DemoMsgRx(CDspMsg** ppDspMsg);
				int m_FileIndex;
				int m_NumOfBuffersInFile;
				int m_BufferIndexInFile;
				FILE *m_RawDataFile;
				BOOL m_IsOnline;
				BOOL m_HwInfoReq;
				//void FillBuffer(CDspMsg *pDspMsg);
				//R_Auto_Scan_Column *m_AutoScanColArr;
				unsigned int m_Numerator;
				unsigned int m_FrameId;
				//int m_AutoScanColArrLength;
			};
		}
	}
}