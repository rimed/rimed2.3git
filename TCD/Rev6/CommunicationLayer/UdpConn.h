#include "RmdSocket.h"
#include "ListDspMsgs.h"
#include "RmdSync.h"


namespace Rimed
{
	namespace TCD
{
	namespace CommunicationLayer
	{
		using namespace RmdSocket;
		class CUdpConn {
		public:
			CUdpConn(Infra *pInfra, CRmdDebug *pRmdDebug);
			~CUdpConn();
			RMD_ERROR_CODE UdpConnCreate(USHORT BindPort, ULONG ulBindAddr, int iMaxMsgSize, unsigned int MaxNumOfRxMsgs);
			RMD_ERROR_CODE UdpConnDelete(void);

			RMD_ERROR_CODE UdpConnMsgRx(CDspMsg** ppDspMsg);
			RMD_ERROR_CODE UdpConnMsgTx(CDspMsg* pDspMsg);
		private:
			CRmdSocket *m_pRmdSocket;
			Infra *m_pInfra;
			CRmdDebug *m_pRmdDebug;

			USHORT m_BindPort;
			ULONG m_ulBindAddr;
			//USHORT m_PeerPort;
			//ULONG m_PeerAddr;
			
			RMD_SOCK_ADDR m_RmdAddrPeer;

			CRmdThread *m_RxThread;
			CRmdThread *m_TxThread;

			int m_iMaxMsgSize;
			unsigned int m_uiMaxNumOfRxMsgs;
			//unsigned int m_uiRxMsgCounter;
			CListDspMsgsSafe *m_ListOfRxMsgs;
			CListDspMsgsSafe *m_ListOfTxMsgs;

			static DWORD WINAPI RxThreadFunc(LPVOID lpThreadParameter);
			static DWORD WINAPI TxThreadFunc(LPVOID lpThreadParameter);

			void RxThread(void);
			void TxThread(void);
		};
	};
}
}