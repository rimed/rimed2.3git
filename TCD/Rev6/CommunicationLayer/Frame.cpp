#include "Frame.h"

using namespace Rimed::TCD::CommunicationLayer;
using namespace std;
using namespace Rimed::TCD::Infrastructure;

unsigned long CFrame::s_framesCounter = 0;

CFrame::CFrame(Infra *pInfra, CRmdDebug *pRmdDebug)
{
	m_pInfra =pInfra;
	m_pRmdDebug = pRmdDebug;
	m_eFrameState = FRAME_STATE_PENDING;
	m_iTotalDataLen = 0;
	INFRA_NEW_OBJ(m_pInfra, m_ListOfDspMsgs, CListDspMsgs, m_pInfra, m_pRmdDebug);
}

CFrame::~CFrame()
{
	m_ListOfDspMsgs->ListDspMsgsClear();
	INFRA_DELETE_OBJ(m_pInfra, m_ListOfDspMsgs, CListDspMsgs);
	m_ListOfDspMsgs = NULL;
}

void CFrame::Reset()
{
	m_ListOfDspMsgs->ListDspMsgsClear();
	m_eFrameState = FRAME_STATE_PENDING;
	m_iTotalDataLen = 0;
}

RMD_ERROR_CODE CFrame::InsertMsg(CDspMsg *pDspMsg)
{
	unsigned long frameId = InterlockedIncrement(&s_framesCounter);

	//DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CFrame::InsertMsg: InsertMsg(). frameId=%d. FrameNum=%d, ucPacketNum=%d, ucTotalPackets=%d, uiTimestamp=%d", frameId, pDspMsg->m_pDspMsgHeader->FrameNum, pDspMsg->m_pDspMsgHeader->ucPacketNum, pDspMsg->m_pDspMsgHeader->ucTotalPackets, pDspMsg->m_pDspMsgHeader->uiTimestamp );

	CDspMsg *pPrevDspMsg = NULL;

	if (m_eFrameState == FRAME_STATE_DISASSEMBLED)
	{
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CFrame::InsertMsg: InsertMsg fail (0). m_eFrameState == FRAME_STATE_DISASSEMBLE. frameId=%i.", frameId);
		return RMD_ERR_FRAME_DISASSEMBLED;
	}

	if(pPrevDspMsg != NULL) 
	{
		// Prev.Packet 
		if(pPrevDspMsg->m_pDspMsgHeader->ucPacketNum +1 >= pPrevDspMsg->m_pDspMsgHeader->ucTotalPackets) 
		{
			DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CFrame::InsertMsg: InsertMsg fail (1). frameId=%i, FrameNum=%d, PrevMsg packetNum=%u, PrevMsg TotalPackets=%u.", frameId, pPrevDspMsg->m_pDspMsgHeader->FrameNum, pPrevDspMsg->m_pDspMsgHeader->ucPacketNum,pPrevDspMsg->m_pDspMsgHeader->ucTotalPackets);
			
			return RMD_INVALID_PARAM;
		}

		if(pPrevDspMsg->m_pDspMsgHeader->ucPacketNum +1 != pDspMsg->m_pDspMsgHeader->ucPacketNum) 
		{
			DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CFrame::InsertMsg: InsertMsg fail (2). frameId=%i, FrameNum=%d, PrevMsg packetNum=%u, Msg PacketNum=%u.", frameId, pPrevDspMsg->m_pDspMsgHeader->FrameNum, pPrevDspMsg->m_pDspMsgHeader->ucPacketNum,pDspMsg->m_pDspMsgHeader->ucPacketNum);

			return RMD_INVALID_PARAM;
		}

		if(pPrevDspMsg->m_pDspMsgHeader->usMsgType != pDspMsg->m_pDspMsgHeader->usMsgType) 
		{
			DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CFrame::InsertMsg: InsertMsg fail (3). frameId=%i, FrameNum=%d, PrevMsg usMsgType=%u, Msg usMsgType=%u.", frameId, pPrevDspMsg->m_pDspMsgHeader->FrameNum, pPrevDspMsg->m_pDspMsgHeader->usMsgType,pDspMsg->m_pDspMsgHeader->usMsgType);

			return RMD_INVALID_PARAM;
		}

		if(pPrevDspMsg->m_pDspMsgHeader->ucTotalPackets != pDspMsg->m_pDspMsgHeader->ucTotalPackets) 
		{
			DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CFrame::InsertMsg: InsertMsg fail (4). frameId=%i, FrameNum=%d, PrevMsg ucTotalPackets=%u, Msg ucTotalPackets=%u.", frameId, pPrevDspMsg->m_pDspMsgHeader->FrameNum, pPrevDspMsg->m_pDspMsgHeader->ucTotalPackets,pDspMsg->m_pDspMsgHeader->ucTotalPackets);

			return RMD_INVALID_PARAM;
		}
	}

	if(pDspMsg->m_pDspMsgHeader->ucPacketNum == pDspMsg->m_pDspMsgHeader->ucTotalPackets-1)
		m_eFrameState = FRAME_STATE_FULL;

	m_ListOfDspMsgs->ListDspMsgsPushBack(pDspMsg);
	m_iTotalDataLen+=pDspMsg->m_pDspMsgHeader->usEffectivePacketSize;

	return RMD_SUCCESS;
}

BOOL CFrame::IsFrameFull()
{
	if(m_eFrameState==FRAME_STATE_FULL)
		return TRUE;
	return FALSE;
}

CDspMsg *CFrame::FramePopFront(void)
{
	CDspMsg *pDspMsg;
	m_eFrameState = FRAME_STATE_DISASSEMBLED;
	pDspMsg = m_ListOfDspMsgs->ListDspMsgsPopFront();
	return pDspMsg;
}

RMD_ERROR_CODE CFrame::PopData(char* pData, int iMaxDataLen, int *pOutDataLen)
{
	if(iMaxDataLen > m_iTotalDataLen) 
	{
		//DBG_TRACE(m_pRmdDebug, INFO_TRC, "RMD_DATA_NOT_READY: CFrame::PopData: iMaxDataLen (%d) > m_iTotalDataLen (%d). list size=%d. Return %d", iMaxDataLen, m_iTotalDataLen, m_ListOfDspMsgs->ListDspMsgsSize(), RMD_DATA_NOT_READY);
		return  RMD_DATA_NOT_READY;
	}

	CDspMsg *pDspMsg = m_ListOfDspMsgs->ListDspMsgsFront();
	int iOffset		 = 0;

	while ((pDspMsg != NULL) && (iOffset+pDspMsg->m_pDspMsgHeader->usEffectivePacketSize<=iMaxDataLen)) 
	{
		m_ListOfDspMsgs->ListDspMsgsPopFront();
		memcpy(&pData[iOffset], pDspMsg->m_pcData, pDspMsg->m_pDspMsgHeader->usEffectivePacketSize);
		iOffset += pDspMsg->m_pDspMsgHeader->usEffectivePacketSize;
		INFRA_DELETE_OBJ(m_pInfra, pDspMsg, CDspMsg);
		pDspMsg = m_ListOfDspMsgs->ListDspMsgsFront();
	}

	*pOutDataLen = iOffset;
	return RMD_SUCCESS;
}

RMD_ERROR_CODE CFrame::PushData(char* pData, int iDataLen, unsigned int FrameNum, USHORT usMsgType, int MsgLen)
{
	if (MsgLen <= 0)
		return RMD_FAILED;

	unsigned char MsgNum	= 0;
	unsigned char NumOfMsgs = iDataLen / MsgLen;	//CDspMsg::DSP_MSG_MAX_LEN;
	if((iDataLen % MsgLen) != 0)					//CDspMsg::DSP_MSG_MAX_LEN)!=0)
		NumOfMsgs++;

	CDspMsg *pDspMsg;
	CDspMsg::DSP_MSG_HEADER *pDspMsgHeader;
	unsigned int Len;

	RMD_ERROR_CODE RmdErr = RMD_SUCCESS;
	int Offset = 0;
	while(Offset < iDataLen) 
	{
		if(iDataLen - Offset<MsgLen)	//CDspMsg::DSP_MSG_MAX_LEN)
			Len = iDataLen-Offset;
		else
			Len = MsgLen;				//CDspMsg::DSP_MSG_MAX_LEN;

		if(Len == 0 || Offset + Len > iDataLen) 
		{
			DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CFrame::PushData: No data or data length mismatch [Offset=%d + Len=%d <= iDataLen=%d].", Offset, Len, iDataLen);
			break;
		}

		INFRA_NEW_OBJ(m_pInfra, pDspMsg, CDspMsg, m_pInfra, m_pRmdDebug);
		pDspMsg->DspMsgInit(Len);
		
		pDspMsgHeader = pDspMsg->m_pDspMsgHeader;
		pDspMsgHeader->FrameNum = FrameNum;
		pDspMsgHeader->ucPacketNum = MsgNum;
		pDspMsgHeader->ucTotalPackets = NumOfMsgs;
		pDspMsgHeader->usOffset = Offset;
		pDspMsgHeader->usEffectivePacketSize = Len;
		pDspMsgHeader->usMsgType = usMsgType;
		//if(Len > 0) 
		//{
		//	DBG_ASSERT(m_pRmdDebug, Offset+Len<=iDataLen);
		//	memcpy(pDspMsg->m_pcData, &pData[Offset], Len);
		//}

		memcpy(pDspMsg->m_pcData, &pData[Offset], Len);

		RmdErr = InsertMsg(pDspMsg);
		//DBG_ASSERT(m_pRmdDebug,RmdErr==RMD_SUCCESS);
		if (RmdErr == RMD_SUCCESS)
		{
			MsgNum++;
			Offset+=Len;
		}
	}
	
	if (Offset == 0)
		return RMD_FAILED;

	return RMD_SUCCESS;
}