#include "ListDspMsgs.h"
#include "DspMsg.h"

using namespace Rimed::TCD::CommunicationLayer;
using namespace Rimed::TCD::Infrastructure;

CListDspMsgs::CListDspMsgs(Infra *pInfra, CRmdDebug *pRmdDebug)
{
	m_pInfra =pInfra;
	m_pRmdDebug = pRmdDebug;
	InitializeCriticalSection(&m_cs);
}

CListDspMsgs::~CListDspMsgs()
{
	DeleteCriticalSection(&m_cs);
}

void CListDspMsgs::ListDspMsgsClear()
{
	EnterCriticalSection(&m_cs);
	__try
	{
		CDspMsg *pDspMsg;
		while(!m_ListDspMsgs.empty()) 
		{
			pDspMsg = m_ListDspMsgs.front();
			m_ListDspMsgs.pop_front();

			//DBG_ASSERT(m_pRmdDebug, pDspMsg!=NULL);
			if (pDspMsg!=NULL)
				INFRA_DELETE_OBJ(m_pInfra, pDspMsg, CDspMsg)
			else
				DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CListDspMsgs::ListDspMsgsClear(): ERROR pDspMsg == NULL.");
		}
	}
	__finally
	{
		LeaveCriticalSection(&m_cs);
	}
}

CDspMsg *CListDspMsgs::ListDspMsgsFront()
{
	EnterCriticalSection(&m_cs);
	__try
	{
		if(m_ListDspMsgs.empty())
			return NULL;				//TODO: FIX warning C6242: A jump out of this try block forces local unwind. This incurs a severe performance penalty

		return m_ListDspMsgs.front();	//TODO: FIX warning C6242: A jump out of this try block forces local unwind. This incurs a severe performance penalty
	}
	__finally
	{
		LeaveCriticalSection(&m_cs);
	}
}

CDspMsg *CListDspMsgs::ListDspMsgsBack()
{
	EnterCriticalSection(&m_cs);
	__try
	{
		if(m_ListDspMsgs.empty())
			return NULL;				//TODO: FIX warning C6242: A jump out of this try block forces local unwind. This incurs a severe performance penalty
		
		return m_ListDspMsgs.back();	//TODO: FIX warning C6242: A jump out of this try block forces local unwind. This incurs a severe performance penalty
	}
	__finally
	{
		LeaveCriticalSection(&m_cs);
	}
}

void CListDspMsgs::ListDspMsgsPushBack(CDspMsg *pDspMsg)
{
	EnterCriticalSection(&m_cs);
	__try
	{
		m_ListDspMsgs.push_back(pDspMsg);
	}
	__finally
	{
		LeaveCriticalSection(&m_cs);
	}
}

CDspMsg *CListDspMsgs::ListDspMsgsPopFront()
{
	EnterCriticalSection(&m_cs);
	__try
	{
		CDspMsg *pDspMsg = ListDspMsgsFront();
		if(pDspMsg!=NULL)
			m_ListDspMsgs.pop_front();	

		return pDspMsg;		//TODO: FIX warning C6242: A jump out of this try block forces local unwind. This incurs a severe performance penalty
	}
	__finally
	{
		LeaveCriticalSection(&m_cs);
	}
}

size_t CListDspMsgs::ListDspMsgsSize()
{
	EnterCriticalSection(&m_cs);
	__try
	{
		return m_ListDspMsgs.size();	//TODO: FIX warning C6242: A jump out of this try block forces local unwind. This incurs a severe performance penalty
	}
	__finally
	{
		LeaveCriticalSection(&m_cs);
	}
}


int CListDspMsgsSafe::s_Counter = 0;
CListDspMsgsSafe::CListDspMsgsSafe(Infra *pInfra, CRmdDebug *pRmdDebug) : CListDspMsgs(pInfra, pRmdDebug)
{
	m_id = s_Counter++;
	InitializeCriticalSection(&m_cs);

	//v2.0.5.1, Ofer DBG_TRACE(m_pRmdDebug, INFO_TRC, "CListDspMsgsSafe::Ctor[id=%d]", m_id);
	//int MutexLabel = 0;
	//INFRA_NEW_OBJ(m_pInfra, m_pMutex, CRmdMutex, m_pInfra, m_pRmdDebug);
	//m_pMutex->RmdMutexCreate(&MutexLabel);
	//DBG_TRACE(m_pRmdDebug, INFO_TRC, "CListDspMsgsSafe: Mutex id=%d", MutexLabel);
}

CListDspMsgsSafe::~CListDspMsgsSafe()
{
	DeleteCriticalSection(&m_cs);
	//v2.0.5.1, Ofer DBG_TRACE(m_pRmdDebug, INFO_TRC, "CListDspMsgsSafe::Dtor[id=%d]", m_id);
	//INFRA_DELETE_OBJ(m_pInfra, m_pMutex, CRmdMutex)
}


void CListDspMsgsSafe::ListDspMsgsPushBackSafe(CDspMsg *pDspMsg, DWORD dwMilliseconds)
{
	//OFER, 2013.04.11, Add try...finally block
	//m_pMutex->RmdMutexAcquire(dwMilliseconds);
	EnterCriticalSection(&m_cs);

	__try
	{
		ListDspMsgsPushBack(pDspMsg);
	}
	__finally
	{
		LeaveCriticalSection(&m_cs);
		//m_pMutex->RmdMutexRelease();
	}
}

CDspMsg *CListDspMsgsSafe::ListDspMsgsPopFrontSafe(DWORD dwMilliseconds)
{
	CDspMsg *pRetMsg;

	//OFER, 2013.04.11, Add try...finally block
	//m_pMutex->RmdMutexAcquire(dwMilliseconds);
	EnterCriticalSection(&m_cs);

	__try
	{
		pRetMsg = ListDspMsgsPopFront();
	}
	__finally
	{
		LeaveCriticalSection(&m_cs);
		//m_pMutex->RmdMutexRelease();
	}

	return pRetMsg;
}