#include <stdio.h>
#include "DemoMgr.h"
#include "TCD_DSP2PC.H"

using namespace Rimed::TCD::CommunicationLayer;
using namespace Rimed::TCD::DspBlock;

#define DEMO_THREAD_FINALIZE_TIMEOUT 100
#define DEMO_THREAD_RX_LIST_TIMEOUT 200

CDemoMgr::CDemoMgr(Infra *pInfra, CRmdDebug *pRmdDebug, R_Auto_Scan_Column *AutoScanColArr, int AutoScanColArrLength) : CEthMgr(pInfra, pRmdDebug)
{
	m_RawDataFile = NULL;
	m_pThread = NULL;
	m_pInfra = pInfra;
	INFRA_NEW_OBJ(m_pInfra, m_ListOfRxMsgs, CListDspMsgsSafe, m_pInfra, m_pRmdDebug);
//	m_AutoScanColArr = AutoScanColArr;
	//m_AutoScanColArrLength = AutoScanColArrLength;
	m_Numerator = 0;
	m_FrameId = 0;

	m_FileIndex = 0;
	m_NumOfBuffersInFile = 0;
	m_BufferIndexInFile = 0;
	m_IsOnline = false;
	m_HwInfoReq = false;
}

CDemoMgr::~CDemoMgr()
{
	INFRA_DELETE_OBJ(m_pInfra,  m_ListOfRxMsgs, CListDspMsgsSafe);
	m_ListOfRxMsgs = NULL;
}

RMD_ERROR_CODE CDemoMgr::EthMgrStart(USHORT usPort, ULONG ulServerAddr) 
{
	DBG_TRACE(m_pRmdDebug, INFO_TRC, "CDemoMgr::EthMgrStart(port=%d, addr=%d)", usPort, ulServerAddr);

	DWORD dwThreadId;
	m_bStop = FALSE;

	INFRA_NEW_OBJ(m_pInfra, m_pThread, CRmdThread, m_pInfra, m_pRmdDebug);
	m_pThread->RmdThreadStart(CDemoMgr::DemoThreadFunc,this,&dwThreadId);

	DBG_TRACE(m_pRmdDebug, INFO_TRC, "CDemoMgr::EthMgrStart(): Thread Id=%i.", dwThreadId);

	return RMD_SUCCESS;
}

#if 0
R_Auto_Scan_Column *GetColumn(double time, R_Auto_Scan_Column *ColumnArr, int ArrLen)
{
	int tick = (int)(time / 0.008);
   if (tick < 0)
		return NULL;

   if (ArrLen > 0)
		return &ColumnArr[tick % ArrLen];
   else
      return NULL;
}


void CDemoMgr::FillBuffer(CDspMsg *pDspMsg)
{
	//(((::R_DSP2PC *)m_pRawData)+1)->Numerator=1;
	////((::R_DSP2PC *)m_pRawData[1])->Numerator=2;

	//::R_DSP2PC* p = ((::R_DSP2PC*)&m_pRawData[0]);
	//p->Numerator = 1;
	//(p+1)->Numerator = 2;

	double GeneralTime = 0;

	int counter = 0;
	//for each on of the DSP blocks (which is 3 spectrum columns)
	//for (int i = 0; i < NUM_OF_DSP2PC_BLOCKS; i++)
	{
		//get a ptr to the relevan block
		pDspMsg->m_pDspMsgHeader->FrameNum = m_FrameId;
		pDspMsg->m_pDspMsgHeader->ucPacketNum = 0;
		pDspMsg->m_pDspMsgHeader->ucTotalPackets = 1;
		pDspMsg->m_pDspMsgHeader->usEffectivePacketSize = (USHORT)sizeof(R_DSP2PC);
		pDspMsg->m_pDspMsgHeader->usOffset = 0;
		pDspMsg->m_pDspMsgHeader->usMsgType = DSP2PC_MSG;
		m_FrameId++;
		R_DSP2PC* p = (R_DSP2PC*)&pDspMsg->m_pcData[0];

		p->Numerator = m_Numerator;
		m_Numerator++;
		//add some data to the debug block. The data in this section can be saved
		//using the debug menu option - save debug data.
		if (m_FrameId == 10)
		{
			for (int ii = 0; ii < 100; ii++)
			{
				p->Control_Flags_and_Debug_Block.TBD[ii] = ii;
			}
			p->Control_Flags_and_Debug_Block.Size = 100;
		}
		else
		{
			p->Control_Flags_and_Debug_Block.Size = 0;
		}


		//fill the autoscan data 
		for(int probe=0; probe<2; probe++)
		{
			double dLocalTime=GeneralTime;

			for(int col=0; col<3; col++, dLocalTime+=0.008)
			{
				R_Auto_Scan_Column* AutoScanCol = GetColumn(dLocalTime, m_AutoScanColArr, m_AutoScanColArrLength);
				for(int pixel=0; pixel<NUM_OF_PIXELS_IN_AUTOSCAN_COLMN; pixel++)
				{
					p->Auto_Scan_Block.R_Probe[probe].Column[col].Pixel[pixel] = AutoScanCol->Pixel[pixel];
				}
			}
		}

		//fill the spectrum data for each gate in the card
		for (int Gate=0; Gate<(GATES_PER_PROBE * NUMBER_OF_PROBES); Gate++)
		{
			double dLocalTime=GeneralTime;
			for (int Column=0;Column<3;Column++)
			{
				//get the relevant data from the dummy data object with an offset for each gate
				//so that each gate will have a different picture.
				SingleCycle ^SC=FFTDummyData::theFFTData->GetData(dLocalTime+Gate*10*0.008);
				//fill the 256 FFT values and the envelopes
				for (int Index=0;Index<256;Index++)
				{
					BYTE *b=&p->FFT_Spectrum_Block.Gate[Gate].Column[Column].Index[Index];
					*b=SC->FFT[Index];
				}
				p->FFT_Envelopes_Block.Peak.Gate[Gate].Column[Column].Forward=(BYTE)SC->Peak_fwd_envelope;
				p->FFT_Envelopes_Block.Peak.Gate[Gate].Column[Column].Reverse=(BYTE)SC->Peak_bck_envelope;
				p->FFT_Envelopes_Block.Mode.Gate[Gate].Column[Column].Forward=(BYTE)SC->Mode_fwd_envelope;
				p->FFT_Envelopes_Block.Mode.Gate[Gate].Column[Column].Reverse=(BYTE)SC->Mode_bck_envelope;

				//clinical parameters
				p->Clinical_Parameters_Block.Gate[Gate].Peak.Forward=SC->Peak_fwd;
				BYTE *b=(BYTE *)&p->Clinical_Parameters_Block.Gate[Gate].Peak.Forward;
				p->Clinical_Parameters_Block.Gate[Gate].Peak.Reverse=SC->Peak_bck;
				p->Clinical_Parameters_Block.Gate[Gate].Mean.Forward=SC->Mean_fwd;
				p->Clinical_Parameters_Block.Gate[Gate].Mean.Reverse=SC->Mean_bck;
				p->Clinical_Parameters_Block.Gate[Gate].Mode.Forward=SC->Mode_fwd;
				p->Clinical_Parameters_Block.Gate[Gate].Mode.Reverse=SC->Mode_bck;
				p->Clinical_Parameters_Block.Gate[Gate].SW.Forward=SC->SW_fwd;
				p->Clinical_Parameters_Block.Gate[Gate].SW.Reverse=SC->SW_bck;


				//advance the time to the next cokumn
				dLocalTime+=NATIVE_DSP_INT_CYCLE_SEC/3.0;
			}


			////Time Domain
			//double TDTime=GeneralTime;
			//for (int TDCounter=0;TDCounter<(int)(0.024/0.000125);TDCounter++)
			//{
			//	TDSingleCycle *TDSC = TimeDomainDummyData::get_Instance()->GetData(TDTime+0.000125*TDCounter);
			//	p->Time_Domain_Block.Gate[Gate].Value[TDCounter].I=TDSC->I;
			//	p->Time_Domain_Block.Gate[Gate].Value[TDCounter].Q=TDSC->Q;
			//}

		}
		//advance the time to hte next block
		GeneralTime+=NATIVE_DSP_INT_CYCLE_SEC;

		// add some random data to hits table
		int random = (int)(GeneralTime*3652);
		if ((random%40)==0)
			p->HITS_Data.m_Gate_sa[0].m_Column_sa[0].Energy = random%250;
		else
			p->HITS_Data.m_Gate_sa[0].m_Column_sa[0].Energy = 0;
		p->HITS_Data.m_Gate_sa[0].m_Column_sa[0].Energy = 0;
	}
}
#endif

DWORD CDemoMgr::DemoThread()
{
	DBG_TRACE(m_pRmdDebug, INFO_TRC, "CDemoMgr::DemoThread() -------> START.");

	int i;
	BOOL boMsg;
	unsigned int uMsg, uParam;
	unsigned long ulParam;
	CDspMsg *pDspMsg = NULL;
	RMD_ERROR_CODE RmdErr;
	char RawDataFileName[256];
	CFrame Frame(m_pInfra, m_pRmdDebug);
	R_DSP2PC *Dsp2Pc;
	int ReadNumOfElements;
	E_RMD_THREAD_STATUS eThreadStatus = m_pThread->RmdRunTimeFn(24);
	errno_t err;

	Dsp2Pc = (R_DSP2PC *)m_pInfra->InfraMalloc (sizeof(R_DSP2PC));

	while(eThreadStatus == RMD_THREAD_RUNNING)
	{
		boMsg = m_pThread->RmdPeekMsg (&uMsg,&uParam, &ulParam);
		if(boMsg && (uMsg==RMD_MSG_KILL))
			break;

		if(!m_IsOnline)
		{
			if(m_RawDataFile!=NULL) 
			{
				fclose(m_RawDataFile);
				m_RawDataFile=NULL;
				m_BufferIndexInFile = 0;
				m_NumOfBuffersInFile = 0;
			}
		}

		if(m_IsOnline && m_ListOfRxMsgs->ListDspMsgsSize()<RMD_TCP_MAX_RX_MSGS_LIST_SIZE) 
		{
					//////////////////////////////////
			if(m_BufferIndexInFile>=m_NumOfBuffersInFile) 
			{
				if(m_RawDataFile!=NULL) 
				{
					fclose(m_RawDataFile);
					m_RawDataFile=NULL;
				}
				m_FileIndex++;
			}

			if(m_RawDataFile==NULL) 
			{
				m_BufferIndexInFile = 0;
				m_NumOfBuffersInFile = 0;
				sprintf_s(RawDataFileName, ".\\DemoRawData\\%d.dat", m_FileIndex);	//sprintf_s(RawDataFileName, "C:\\Rimed\\SRC\\TCD2003\\GUI\\BIN\\DEBUG\\DemoRawData\\%d.dat", m_FileIndex);
				
				err = fopen_s(&m_RawDataFile, RawDataFileName, "rb");
				DBG_TRACE(m_pRmdDebug, INFO_TRC, "CDemoMgr::DemoThread(): RawDataFileName=%s, err=%d.", RawDataFileName, err);
			}

			if(m_RawDataFile==NULL) 
			{
				if(m_FileIndex>0)
					m_FileIndex = 0; // Go back to beginning
			}
			else 
			{
				if(m_BufferIndexInFile>=m_NumOfBuffersInFile) 
				{
					ReadNumOfElements = fread(&m_NumOfBuffersInFile, sizeof(int), 1, m_RawDataFile);
					if(ReadNumOfElements!=1) 
					{
						m_NumOfBuffersInFile = 0;
						fclose(m_RawDataFile);
						m_RawDataFile=NULL;
					}

					m_BufferIndexInFile = 0;
				}
			}

			if(m_RawDataFile != NULL) 
			{
				ReadNumOfElements = fread(Dsp2Pc, sizeof(R_DSP2PC), 1, m_RawDataFile);
				if(ReadNumOfElements==1) 
				{
					RmdErr = Frame.PushData((char*)Dsp2Pc, sizeof(R_DSP2PC), m_FrameId, DSP2PC_MSG, CDspMsg::DSP_MSG_MAX_LEN);
					if (RmdErr == RMD_SUCCESS)
					{
						pDspMsg = Frame.FramePopFront(); 
						while(pDspMsg != NULL) 
						{
							m_ListOfRxMsgs->ListDspMsgsPushBackSafe(pDspMsg, DEMO_THREAD_RX_LIST_TIMEOUT);
							pDspMsg =Frame.FramePopFront(); 
						}

						Frame.Reset();
						m_BufferIndexInFile++;
					}
					else
					{
						DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CDemoMgr::DemoThread(): Frame.PushData FAIL. RmdErr=%d.", RawDataFileName, RmdErr);
					}
				}
				else 
				{
					m_BufferIndexInFile = m_NumOfBuffersInFile;
				}
			}
		}
		eThreadStatus = m_pThread->RmdRunTimeFn(24);	
	}
	m_pInfra->InfraFree (Dsp2Pc, sizeof(R_DSP2PC));

	DBG_TRACE(m_pRmdDebug, INFO_TRC, "CDemoMgr::DemoThread() <------- END.");

	return 0;
}

DWORD CDemoMgr::DemoThreadFunc(LPVOID lpThreadParameter)
{
	CDemoMgr *pThis = (CDemoMgr*)lpThreadParameter;
	return pThis->DemoThread();
}


RMD_ERROR_CODE CDemoMgr::EthMgrStop(void)
{
	DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CDemoMgr::EthMgrStop().");
	if(m_pThread!=NULL) 
	{
		m_pThread->RmdThreadStop();
		m_pThread->RmdThreadFinalize(DEMO_THREAD_FINALIZE_TIMEOUT);
		INFRA_DELETE_OBJ(m_pInfra, m_pThread, CRmdThread);
		m_pThread=NULL;
	}
	m_ListOfRxMsgs->ListDspMsgsClear();

	if(m_RawDataFile!=NULL) 
	{
		fclose(m_RawDataFile);
		m_RawDataFile = NULL;
	}
	m_bStop = TRUE;
	return RMD_SUCCESS;
}

RMD_ERROR_CODE CDemoMgr::DemoMsgRx(CDspMsg** ppDspMsg)
{
	 RMD_ERROR_CODE RmdErr;
	 *ppDspMsg = m_ListOfRxMsgs->ListDspMsgsPopFrontSafe(DEMO_THREAD_RX_LIST_TIMEOUT);
	 if(*ppDspMsg==NULL)
		 RmdErr = RMD_LIST_EMPTY;
	 else
		 RmdErr = RMD_SUCCESS;
	 return RmdErr;
}

RMD_ERROR_CODE CDemoMgr::EthMgrAllRx(void)
{
	RMD_ERROR_CODE RmdErr;
	CDspMsg* pDspMsg;

	RmdErr = DemoMsgRx(&pDspMsg);
	while(RmdErr==RMD_SUCCESS) {
		EthMgrProcessMsg(pDspMsg);
	
		RmdErr = DemoMsgRx(&pDspMsg);
	}
	return RMD_SUCCESS;
}


//public enum TSystemModes { Offline = 0, Online = 1, Replay = 2, FPGADownload = 3 }; // Numbers from dsp protocol!
RMD_ERROR_CODE CDemoMgr::EthMgrFrameTx(char* pData, int iDataLen, USHORT usMsgType)
{
	R_PC2DSP *Pc2Dsp;
	switch(usMsgType) 
	{
		case PC2DSP_MSG:
				Pc2Dsp		= (R_PC2DSP *)pData;
				m_IsOnline	= (Pc2Dsp->m_GenCardInfo_s.m_OperationMode == OPERATION_MODE_ONLINE);
			break;
		case HW_INFO_REQ:
				m_HwInfoReq = true;
			break;
		case REPLAY_DSP2PC:
				//RMD_ERROR_CODE RmdErr = RMD_SUCCESS;
				CDspMsg *pDspMsg;
				CFrame Frame(m_pInfra, m_pRmdDebug);
		
				RMD_ERROR_CODE RmdErr = Frame.PushData(pData, iDataLen, 1, usMsgType, RMD_UDP_MAX_MSG_LEN);
				if (RmdErr == RMD_SUCCESS)
				{
					pDspMsg = Frame.FramePopFront();
					while(pDspMsg != NULL && RmdErr == RMD_SUCCESS) 
					{
						INFRA_DELETE_OBJ(m_pInfra, pDspMsg, CDspMsg);			 
						pDspMsg = Frame.FramePopFront();
					}
				}
			break;
	}

	return RMD_SUCCESS;
}

int CDemoMgr::TcpServerGetNumOfClients(void)
{
	return 1;
}

RMD_ERROR_CODE CDemoMgr::EthMgrRxHwInfoResponse(char* pData, int iMaxDataLen, USHORT usMsgType, int *pOutDataLen)
{
	R_IP_HARDWARE_INFO *HwInfo = (R_IP_HARDWARE_INFO *)pData;
	if(m_HwInfoReq==TRUE) 
	{
		HwInfo->dspVersionFormat = 10;
		HwInfo->dspVersionNum = -1;
		HwInfo->fpgaMainVersion = -1;
		HwInfo->fpgaSubVersion = -1;

		*pOutDataLen = sizeof(R_IP_HARDWARE_INFO);
		return RMD_SUCCESS;
	}
	else
		return RMD_FAILED;
}
