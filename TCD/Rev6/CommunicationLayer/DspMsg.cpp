#include "DspMsg.h"

using namespace Rimed::TCD::CommunicationLayer;
using namespace Rimed::TCD::Infrastructure;

CDspMsg::CDspMsg(Infra *pInfra, CRmdDebug *pRmdDebug)
{
	m_pInfra =pInfra;
	m_pRmdDebug = pRmdDebug;
	DspMsgReset();
}

CDspMsg::~CDspMsg()
{
	//DBG_ASSERT(m_pRmdDebug, m_iMemSz>0);	Ofer, 2013.10.03
	if(m_iMemSz > 0) 
	{
		m_pInfra->InfraFree (m_pcMem, m_iMemSz);
		DspMsgReset();
	}
}

void CDspMsg::DspMsgReset(void)
{
	m_pcData = NULL;
	m_iDataLen = 0;
	m_iMemSz = 0;
	m_pcMem = NULL;
	m_pDspMsgHeader = NULL;
}

RMD_ERROR_CODE CDspMsg::DspMsgInit(int iDataLen)
{
	DBG_ASSERT(m_pRmdDebug,m_pcData == NULL /*&& m_uiDataLen==0*/ && m_iMemSz==0);

	m_pcMem = (char*)m_pInfra->InfraMalloc(iDataLen+sizeof(DSP_MSG_HEADER));
	if(m_pcMem==NULL) 
	{
		throw(CRmdException(RMD_NOT_ENOUGH_SPACE_ALLOCATION, __LINE__, __FILE__));
		return RMD_NOT_ENOUGH_SPACE_ALLOCATION;
	}

	m_iDataLen = iDataLen+sizeof(DSP_MSG_HEADER);
	m_iMemSz = iDataLen+sizeof(DSP_MSG_HEADER);
	m_pDspMsgHeader = (DSP_MSG_HEADER *)m_pcMem;
	m_pcData = &(m_pcMem[sizeof(DSP_MSG_HEADER)]);
	memset(m_pcMem, 0, m_iMemSz);
	
	return RMD_SUCCESS;
}

CDspMsg *CDspMsg::SplitMsg(int TailOffset, int TailLen)
{
	RMD_ERROR_CODE RmdErr;
	CDspMsg *TailMsg;
	INFRA_NEW_OBJ(m_pInfra, TailMsg, CDspMsg, m_pInfra, m_pRmdDebug);
	RmdErr =TailMsg->DspMsgInit(TailLen);
	memcpy(TailMsg->m_pcMem, &m_pcMem[TailOffset], TailLen);

	m_iDataLen = TailOffset;
	return TailMsg;
}