#pragma once

#include "ListDspMsgs.h"

namespace Rimed
{
	namespace TCD
	{
		namespace CommunicationLayer
		{
			class CFrame 
			{
			public:
				CFrame(Infra *pInfra, CRmdDebug *pRmdDebug);
				~CFrame();
				RMD_ERROR_CODE InsertMsg(CDspMsg *pDspMsg);
				BOOL IsFrameFull(void);
				CDspMsg *FramePopFront(void);
				RMD_ERROR_CODE PopData(char* pData, int iMaxDataLen, int *pOutDataLen);
				RMD_ERROR_CODE PushData(char* pData, int iDataLen, unsigned int FrameNum, USHORT usMsgType, int MsgLen);
				void Reset();
			private:
				enum E_FRAME_STATE 
				{
					FRAME_STATE_PENDING,
					FRAME_STATE_FULL,
					FRAME_STATE_DISASSEMBLED
				};
				E_FRAME_STATE m_eFrameState;
				CListDspMsgs *m_ListOfDspMsgs;
				int m_iTotalDataLen;
				Infra *m_pInfra;
				CRmdDebug *m_pRmdDebug;
				static unsigned long s_framesCounter;
			};
		}
	}
}