#include "EthMgr.h"
#include "TCD_DSP2PC.h"

using namespace Rimed::TCD::CommunicationLayer;
using namespace Rimed::TCD::DspBlock;

#define RMD_SERVER_BACKLOG 1
#define RMD_ADVERTISE_THREAD_FINALIZE_TIMEOUT 50
#define RMD_ADVERTISE_INTERVAL 50

CEthMgr::CEthMgr(Infra *pInfra, CRmdDebug *pRmdDebug)
{
	m_pInfra =pInfra;
	m_pRmdDebug = pRmdDebug;
	m_pRmdTcpServer = NULL;
	m_pAdvertiseThread = NULL;
	m_AdvertisePort = RMD_ADVERTISE_PORT_DEFAULT;
	m_pRxDsp2PcFramePending = NULL;
	m_pRxHwInfoMsg = NULL;
	m_bStop = TRUE;
	m_TxFrameNum = 0;
	m_UdpConn = NULL;

	DBG_TRACE(m_pRmdDebug, INFO_TRC, "CEthMgr::CEthMgr()");
}

CEthMgr::~CEthMgr()
{
	DBG_TRACE(m_pRmdDebug, INFO_TRC, "CEthMgr::~CEthMgr()");

	if (!m_bStop)
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CEthMgr::~CEthMgr(): m_bStop != true.");	
}

RMD_ERROR_CODE CEthMgr::EthMgrStart(USHORT usPort, ULONG ulServerAddr)
{
	DBG_TRACE(m_pRmdDebug, INFO_TRC, "CEthMgr::EthMgrStart(port=%d, addr=%d)", usPort, ulServerAddr);

	RMD_ERROR_CODE RmdErr;
	DWORD dwThreadId;
	m_bStop = FALSE;

	if(m_pRmdTcpServer!=NULL) 
	{
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CEthMgr::EthMgrStart(): RMD_WRONG_STATE");
		return RMD_WRONG_STATE;
	}
	try 
	{
		RmdErr= m_pInfra->SocketLibStart();
		if (RmdErr != RMD_SUCCESS)		//Ofer, 2013.12.29
		{
			EthMgrStop();
			return RmdErr;
		}

		INFRA_NEW_OBJ(m_pInfra, m_pRmdTcpServer, CRmdTcpServer, m_pInfra, m_pRmdDebug);
		RmdErr = m_pRmdTcpServer->RmdTcpServerStart(RMD_SERVER_BACKLOG, usPort, ulServerAddr, RMD_TCP_MAX_MSG_LEN, RMD_TCP_MAX_RX_MSGS_LIST_SIZE);
		if (RmdErr != RMD_SUCCESS)		//Ofer, 2013.12.29
		{
			EthMgrStop();
			return RmdErr;
		}
		
		INFRA_NEW_OBJ(m_pInfra, m_pRmdAdvertiseSocket, CRmdSocket, m_pInfra, m_pRmdDebug);
		m_pRmdAdvertiseSocket->RmdSockCreate(RMD_UDP_PROT);
		if (RmdErr != RMD_SUCCESS)		//Ofer, 2013.12.29
		{
			EthMgrStop();
			return RmdErr;
		}

		RmdErr = m_pRmdAdvertiseSocket->RmdSockBind(RMD_ADVERTISE_PORT_DEFAULT, INADDR_ANY);
		if (RmdErr != RMD_SUCCESS)		//Ofer, 2013.12.29
		{
			EthMgrStop();
			return RmdErr;
		}
	}
	catch(CRmdException &RmdExc) 
	{
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdTcpServer::EthMgrStart().EXCEPTION (err=%i, Inner=%i, File=%s, Line=%i).", RmdExc.m_dwErrorCode, RmdExc.m_dwOriginalCode, RmdExc.m_acFileName, RmdExc.m_dwLine);
		EthMgrStop();
	}

	INFRA_NEW_OBJ(m_pInfra, m_pAdvertiseThread, CRmdThread, m_pInfra, m_pRmdDebug);
	m_pAdvertiseThread->RmdThreadStart(AdvertiseThreadFunc, this, &dwThreadId);

	DBG_TRACE(m_pRmdDebug, INFO_TRC, "CEthMgr::EthMgrStart(): Thread Id=%i.", dwThreadId);

	return RmdErr;
}

void CEthMgr::ClearRxFramesFifo()
{
	CFrame *pFrame;
	while(!m_FifoRxDsp2PcFrameReady.empty()) 
	{
		pFrame = m_FifoRxDsp2PcFrameReady.front();
		m_FifoRxDsp2PcFrameReady.pop_front();
		if (pFrame != NULL)
			INFRA_DELETE_OBJ(m_pInfra, pFrame, CFrame)
	}
}

RMD_ERROR_CODE CEthMgr::EthMgrStop(void)
{
	DBG_TRACE(m_pRmdDebug, INFO_TRC, "CEthMgr::EthMgrStop()");

	RMD_ERROR_CODE RmdErr;

	CFrame *pFrame;
	while(!m_FifoRxDsp2PcFrameReady.empty()) 
	{
		pFrame = m_FifoRxDsp2PcFrameReady.front();
		m_FifoRxDsp2PcFrameReady.pop_front();
	
		//DBG_ASSERT(m_pRmdDebug, pFrame!=NULL);
		if (pFrame != NULL)
			INFRA_DELETE_OBJ(m_pInfra, pFrame, CFrame)
		else
			DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CEthMgr::EthMgrStop(): ERROR pFrame == NULL");
	}

	if(m_pAdvertiseThread!=NULL) 
	{
		DBG_TRACE(m_pRmdDebug, INFO_TRC, "CEthMgr::EthMgrStop(): m_pAdvertiseThread->RmdThreadStop()");

		m_pAdvertiseThread->RmdThreadStop();
	}

	if(m_pRmdAdvertiseSocket!=NULL) 
	{
		RmdErr = m_pRmdAdvertiseSocket->RmdSockClose();
		if(RmdErr != RMD_SUCCESS)
			DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CEthMgr::EthMgrStop(): Advertise Socket Close failed (err=%i).", RmdErr);
	}

	if(m_pAdvertiseThread!=NULL) 
	{
		DBG_TRACE(m_pRmdDebug, INFO_TRC, "CEthMgr::EthMgrStop(): m_pAdvertiseThread->RmdThreadStop()");

		m_pAdvertiseThread->RmdThreadStop();
		m_pAdvertiseThread->RmdThreadFinalize(RMD_ADVERTISE_THREAD_FINALIZE_TIMEOUT);
		INFRA_DELETE_OBJ(m_pInfra, m_pAdvertiseThread, CRmdThread);
	}

	if(m_pRmdAdvertiseSocket!=NULL)
		INFRA_DELETE_OBJ(m_pInfra, m_pRmdAdvertiseSocket, CRmdSocket);

	if(m_pRmdTcpServer == NULL) 
	{
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CEthMgr::EthMgrStop(): m_pRmdTcpServer Not started.");
	}
	else 
	{
		RmdErr = m_pRmdTcpServer->RmdTcpServerStop();
		if(RmdErr != RMD_SUCCESS) 
			DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CEthMgr::EthMgrStop(): RmdTcpServerStop failed (err=%i).", RmdErr);

		INFRA_DELETE_OBJ(m_pInfra, m_pRmdTcpServer, CRmdTcpServer);
		m_pRmdTcpServer = NULL;
	}

	if(m_UdpConn!=NULL) 
	{
		m_UdpConn->UdpConnDelete();
		INFRA_DELETE_OBJ(m_pInfra, m_UdpConn, CUdpConn);
	}

	RmdErr = m_pInfra->SocketLibStop();
	if(RmdErr!=RMD_SUCCESS) 
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CEthMgr::EthMgrStop(): SocketLibStart failed (err=%i).", RmdErr);

	if(m_pRxDsp2PcFramePending!=NULL) 
	{
		INFRA_DELETE_OBJ(m_pInfra, m_pRxDsp2PcFramePending, CFrame);
		m_pRxDsp2PcFramePending=NULL;
	}

	if(m_pRxHwInfoMsg!=NULL)
	{
		INFRA_DELETE_OBJ(m_pInfra, m_pRxHwInfoMsg, CDspMsg);
		m_pRxHwInfoMsg=NULL;
	}

	m_bStop = TRUE;

	return RmdErr;
}

void CEthMgr::EthMgrProcessMsg(CDspMsg* pDspMsg)
{
	RMD_ERROR_CODE RmdErr;	
	CDspMsg *MsgDelete = NULL;

		if(pDspMsg->m_iDataLen<sizeof(CDspMsg::DSP_MSG_HEADER)) 
		{
			INFRA_DELETE_OBJ(m_pInfra, pDspMsg, CDspMsg);
		}
		else 
		{
			switch(pDspMsg->m_pDspMsgHeader->usMsgType) 
			{
			case DSP2PC_MSG:
				if(m_pRxDsp2PcFramePending == NULL)
					INFRA_NEW_OBJ(m_pInfra, m_pRxDsp2PcFramePending, CFrame, m_pInfra, m_pRmdDebug);

				RmdErr = m_pRxDsp2PcFramePending->InsertMsg(pDspMsg);
				if(RmdErr != RMD_SUCCESS) 
				{
					DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CEthMgr::EthMgrProcessMsg(): Sm_pRxDsp2PcFramePending->InsertMsg failed, drop msg and frame (err=%i).", RmdErr);

					INFRA_DELETE_OBJ(m_pInfra, pDspMsg, CDspMsg);
					INFRA_DELETE_OBJ(m_pInfra, m_pRxDsp2PcFramePending, CFrame);
					m_pRxDsp2PcFramePending = NULL;
				}
				else if(m_pRxDsp2PcFramePending->IsFrameFull()) 
				{
					m_FifoRxDsp2PcFrameReady.push_back(m_pRxDsp2PcFramePending);
					m_pRxDsp2PcFramePending = NULL;
				}
				break;
			case HW_INFO_RESPONSE:
				if(m_pRxHwInfoMsg != NULL) 
				{
					DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CEthMgr::EthMgrProcessMsg(): Unexpected HW_INFO_MSG received, drop it!");
					MsgDelete = m_pRxHwInfoMsg;
				}

				m_pRxHwInfoMsg = pDspMsg;
				if(MsgDelete != NULL)
					INFRA_DELETE_OBJ(m_pInfra, MsgDelete,CDspMsg);
				
				break;
			case HANDSHAKE_REQ:
				INFRA_DELETE_OBJ(m_pInfra, pDspMsg,CDspMsg);
				
				if(m_pRxDsp2PcFramePending != NULL) 
				{
					DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CEthMgr::EthMgrProcessMsg: INIT_HANDSHAKE_MSG received in the middle of Dsp2Pc frame");
					INFRA_DELETE_OBJ(m_pInfra, m_pRxDsp2PcFramePending, CFrame);
					m_pRxDsp2PcFramePending = NULL;
				}
				
				break;
			case DEBUG_INFO_REQUEST:
				//m_pRmdDebug->DebugDspLog(pDspMsg->m_pcData, pDspMsg->m_pDspMsgHeader->usEffectivePacketSize);
				INFRA_DELETE_OBJ(m_pInfra, pDspMsg,CDspMsg);
				break;
			default:
					DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CEthMgr::EthMgrProcessMsg: Unknown message type (%d).", pDspMsg->m_pDspMsgHeader->usMsgType);
				return;		// DBG_ASSERT(m_pRmdDebug,0); Ofer, 2013.10.03
			}
		}
}

RMD_ERROR_CODE CEthMgr::EthMgrAllRx(void)
{
	RMD_ERROR_CODE RmdErr;
	CDspMsg* pDspMsg;

	//RmdErr = m_pRmdTcpServer->TcpServerMsgRx(&pDspMsg);
	RmdErr = m_UdpConn->UdpConnMsgRx(&pDspMsg);
	while(RmdErr==RMD_SUCCESS) 
	{
		EthMgrProcessMsg(pDspMsg);
		RmdErr = m_UdpConn->UdpConnMsgRx(&pDspMsg);
	}

	return RMD_SUCCESS;
}

bool CEthMgr::EthMgrFrameRxReady(USHORT usMsgType)
{
	try
	{
		EthMgrAllRx();
		switch(usMsgType) 
		{
			case DSP2PC_MSG:
				if(m_FifoRxDsp2PcFrameReady.empty())
					return false;
				break;
			default:
					DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CEthMgr::EthMgrFrameRxReady(usMsgType=%u): Unidentified message type. Return false.", usMsgType);
				return false;	// DBG_ASSERT(m_pRmdDebug,0);	
		}

		return true;
	}
	catch(CRmdException &RmdExc) 
	{
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CEthMgr::EthMgrFrameRxReady(usMsgType=%u).EXCEPTION (err=%i, Inner=%i, File=%s, Line=%i).", usMsgType, RmdExc.m_dwErrorCode, RmdExc.m_dwOriginalCode, RmdExc.m_acFileName, RmdExc.m_dwLine);
		return false;
	}
}

RMD_ERROR_CODE CEthMgr::EthMgrFrameRx(char* pData, int iMaxDataLen, USHORT usMsgType, int *pOutDataLen)
{
	RMD_ERROR_CODE RmdErr = RMD_FAILED;
	CFrame *pFrame;

	switch(usMsgType) 
	{
		case DSP2PC_MSG	:
			if(m_FifoRxDsp2PcFrameReady.empty()) 
			{
				*pOutDataLen = 0;
				return RMD_LIST_EMPTY;	
			}
			else 
			{
				pFrame = m_FifoRxDsp2PcFrameReady.front();
				m_FifoRxDsp2PcFrameReady.pop_front();
				
				RmdErr = pFrame->PopData(pData, iMaxDataLen, pOutDataLen);
				INFRA_DELETE_OBJ(m_pInfra, pFrame, CFrame);
			}
			break;
		default:
			return RmdErr;	// DBG_ASSERT(m_pRmdDebug,0);	Ofer, 2013.10.03
	}

	return RmdErr;
}


RMD_ERROR_CODE CEthMgr::EthMgrRxHwInfoResponse(char* pData, int iMaxDataLen, USHORT usMsgType, int *pOutDataLen)
{
	RMD_ERROR_CODE RmdErr = RMD_FAILED;
	CDspMsg* pDspMsg;
	RmdErr = m_pRmdTcpServer->TcpServerMsgRx(&pDspMsg);
	if(RmdErr == RMD_SUCCESS) 
	{
		if(pDspMsg->m_pDspMsgHeader->usMsgType != HW_INFO_RESPONSE) 
		{
			DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CEthMgr::EthMgrRxHwInfoResponse: Message type (%d) != HW_INFO_RESPONSE.", pDspMsg->m_pDspMsgHeader->usMsgType);

			INFRA_DELETE_OBJ(m_pInfra, pDspMsg, CDspMsg);
			return RMD_FAILED;
		}
		
		if(pDspMsg->m_pDspMsgHeader->usEffectivePacketSize != sizeof(R_IP_HARDWARE_INFO)) 
		{
			DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CEthMgr::EthMgrRxHwInfoResponse: invalid message size (Expected=%d, Recieved=%d).", sizeof(R_IP_HARDWARE_INFO), pDspMsg->m_pDspMsgHeader->usEffectivePacketSize);

			INFRA_DELETE_OBJ(m_pInfra, pDspMsg, CDspMsg);
			return RMD_FAILED;
		}

		memcpy(pData, pDspMsg->m_pcData, pDspMsg->m_pDspMsgHeader->usEffectivePacketSize);
		*pOutDataLen = pDspMsg->m_pDspMsgHeader->usEffectivePacketSize;
		INFRA_DELETE_OBJ(m_pInfra, pDspMsg, CDspMsg);
	}

	return RmdErr;
}

RMD_ERROR_CODE CEthMgr::EthMgrFrameTx(char* pData, int iDataLen, USHORT usMsgType)
{
	if (usMsgType != PC2DSP_MSG && usMsgType != HW_INFO_REQ && usMsgType != REPLAY_DSP2PC)
	{
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CEthMgr::EthMgrFrameTx: usMsgType=&d is not supported, return = %i.", usMsgType, RMD_MESSAGE_NOT_SUPPORTED);
		return RMD_MESSAGE_NOT_SUPPORTED;
	}


	CFrame Frame(m_pInfra, m_pRmdDebug);
	if(usMsgType == PC2DSP_MSG) 
		R_PC2DSP *Pc2Dsp = (R_PC2DSP *)pData;

	CDspMsg *pDspMsg;
	RMD_ERROR_CODE RmdErr = Frame.PushData(pData, iDataLen, m_TxFrameNum, usMsgType, RMD_TCP_MAX_MSG_LEN);
	if (RmdErr == RMD_SUCCESS)	//DBG_ASSERT(m_pRmdDebug,RmdErr==RMD_SUCCESS);
	{
		m_TxFrameNum++;

		pDspMsg = Frame.FramePopFront();
		while(pDspMsg != NULL && RmdErr == RMD_SUCCESS) 
		{
			RmdErr = m_pRmdTcpServer->TcpServerMsgTx(pDspMsg);
			pDspMsg = Frame.FramePopFront();
		}
	}
	else
	{
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CEthMgr::EthMgrFrameTx: usMsgType=&d Frame.PushData return = %i.", usMsgType, RmdErr);
	}

	//switch(usMsgType) 
	//{
	//	case PC2DSP_MSG:
	//	case HW_INFO_REQ:
	//		RmdErr = Frame.PushData(pData, iDataLen, m_TxFrameNum, usMsgType, RMD_TCP_MAX_MSG_LEN);
	//		if (RmdErr == RMD_SUCCESS)	//DBG_ASSERT(m_pRmdDebug,RmdErr==RMD_SUCCESS);
	//		{
	//			m_TxFrameNum++;

	//			pDspMsg = Frame.FramePopFront();
	//			while(pDspMsg != NULL && RmdErr == RMD_SUCCESS) 
	//			{
	//				RmdErr = m_pRmdTcpServer->TcpServerMsgTx(pDspMsg);
	//				pDspMsg = Frame.FramePopFront();
	//			}
	//		}
	//		else
	//		{
	//			DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CEthMgr::EthMgrFrameTx: HW_INFO_REQ Frame.PushData return = %i.", RmdErr);
	//		}
	//		break;
	//	case REPLAY_DSP2PC:
	//		RmdErr = Frame.PushData(pData, iDataLen, m_TxFrameNum, usMsgType, RMD_TCP_MAX_MSG_LEN);
	//		if (RmdErr == RMD_SUCCESS)	//DBG_ASSERT(m_pRmdDebug,RmdErr==RMD_SUCCESS);
	//		{
	//			m_TxFrameNum++;

	//			pDspMsg = Frame.FramePopFront();
	//			while(pDspMsg != NULL && RmdErr == RMD_SUCCESS) 
	//			{
	//				 RmdErr = m_pRmdTcpServer->TcpServerMsgTx(pDspMsg);
	//				 pDspMsg = Frame.FramePopFront();
	//			}
	//		}
	//		else
	//		{
	//			DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CEthMgr::EthMgrFrameTx: REPLAY_DSP2PC Frame.PushData return = %i.", RmdErr);
	//		}

	//		break;
	//}

	return RmdErr;
}

DWORD WINAPI CEthMgr::AdvertiseThreadFunc(LPVOID lpThreadParameter)
{
	OutputDebugStringA("[RIMED] CEthMgr::AdvertiseThreadFunc(). THREAD START");

	CEthMgr *pThis = (CEthMgr*)lpThreadParameter;
	CRmdThread *pThisThread = pThis->m_pAdvertiseThread;
	unsigned int uMsg, uParam;
	unsigned long ulParam;
	RMD_SOCK_ADDR BroadcastAddr;
	RMD_ERROR_CODE RmdErr;
	char acAdvertiseMsg[RMD_ADVERTISE_MSG_LEN];
	BOOL boMsg;
	E_RMD_THREAD_STATUS eThreadStatus = pThisThread->RmdRunTimeFn(RMD_ADVERTISE_INTERVAL);
	while(eThreadStatus == RMD_THREAD_RUNNING)
	{
		boMsg = pThisThread->RmdPeekMsg (&uMsg,&uParam, &ulParam);
		if(boMsg && (uMsg == RMD_MSG_KILL))
		{
			OutputDebugStringA("[RIMED] CEthMgr::AdvertiseThreadFunc(): RMD_MSG_KILL message recieved. THREAD ABORT.");
			break;	//return 0;
		}

		if(pThis->m_pRmdTcpServer->GetNumOfConnections()==0) 
		{
			pThis->m_pRmdAdvertiseSocket->RmdSockAddrInit(&BroadcastAddr, pThis->m_AdvertisePort, INADDR_BROADCAST);
			sprintf_s(acAdvertiseMsg, RMD_ADVERTISE_MSG_LEN, RMD_ADVERTISE_MSG_STR);
			RmdErr = pThis->m_pRmdAdvertiseSocket->UdpSend(&BroadcastAddr, acAdvertiseMsg, RMD_ADVERTISE_MSG_LEN);
			if(RmdErr != RMD_SUCCESS) 
			{
				OutputDebugStringA("[RIMED] CEthMgr::AdvertiseThreadFunc(): ATTENTION - UdpSend() fail. THREAD ABORT.");
				break;	//return 0;
			}
		}
		else 
		{
			OutputDebugStringA("[RIMED] CEthMgr::AdvertiseThreadFunc(): Connection established, stop advertise. THREAD ABORT.");
			break;	//return 0;
		}
	
		eThreadStatus = pThisThread->RmdRunTimeFn(RMD_ADVERTISE_INTERVAL);
	}
	
	OutputDebugStringA("[RIMED] CEthMgr::AdvertiseThreadFunc(). THREAD END");

	return 0;	
}

int CEthMgr::TcpServerGetNumOfClients(void)
{
	RMD_ERROR_CODE RmdErr;
	if(m_pRmdTcpServer->GetNumOfConnections()==1) 	
	{
		if(m_UdpConn==NULL) 
		{
			INFRA_NEW_OBJ(m_pInfra, m_UdpConn, CUdpConn, m_pInfra, m_pRmdDebug);
			RmdErr = m_UdpConn->UdpConnCreate(RMD_UDP_PORT_DEFAULT, INADDR_ANY, RMD_UDP_MAX_MSG_LEN, RMD_TCP_MAX_RX_MSGS_LIST_SIZE);
		}
	}

	return m_pRmdTcpServer->GetNumOfConnections();
}