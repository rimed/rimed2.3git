#pragma once

#include <list>
#include "DspMsg.h"
#include "RmdSync.h"

using namespace std;
namespace Rimed
{
	namespace TCD
	{
		namespace CommunicationLayer
		{
			class CListDspMsgs
			{
			public:
				CListDspMsgs(Infra *pInfra, CRmdDebug *pRmdDebug);
				~CListDspMsgs();
				void ListDspMsgsClear();
				CDspMsg *ListDspMsgsFront();
				CDspMsg *ListDspMsgsBack();
				void ListDspMsgsPushBack(CDspMsg *pDspMsg);
				CDspMsg *ListDspMsgsPopFront();
				size_t ListDspMsgsSize();
			protected:
				std::list <CDspMsg*> m_ListDspMsgs;
				Infra *m_pInfra;
				CRmdDebug *m_pRmdDebug;
			private:
				CRITICAL_SECTION m_cs;
			};

			class CListDspMsgsSafe : public CListDspMsgs
			{
			public:
				CListDspMsgsSafe(Infra *pInfra, CRmdDebug *pRmdDebug);
				~CListDspMsgsSafe();
				void ListDspMsgsPushBackSafe(CDspMsg *pDspMsg, DWORD dwMilliseconds);
				CDspMsg *ListDspMsgsPopFrontSafe(DWORD dwMilliseconds);

			private:
				//CRmdMutex *m_pMutex;
				CRITICAL_SECTION m_cs;
				int m_id;
				static int s_Counter;
			};
		}
	}
}