#pragma once

#include "Infra.h"
using namespace Rimed::TCD::Infrastructure;

namespace Rimed
{
	namespace TCD
	{
		namespace CommunicationLayer
		{
			enum E_DSP_MSG_TYPE{
				 HANDSHAKE_REQ = 1,// I am alive request to the DSP
				 LOAD_DSP_REQ =2,	// Command to the DSP to load its program from the Flash memory
				 PC2DSP_MSG = 3,	// Configuration control command, used to send user commands to the DSP through PC2DSP structur
				 REPLAY_DSP2PC = 4,	// Send the recorded data to the DSP through DSP2PC structure
				 HW_INFO_REQ = 5,	// Request Hardware info (DSP and FPGA versions)
				 BURN_DSP_REQ = 6,	// Command to burn the Flash memory with a new DSP program (followed by the new DSP program file)
				 BURN_FPGA_REQ = 7,
				 RESET_DSP_REQ = 8,
			 
				 HANDSHAKE_RESPONSE = 101,	// Response to HANDSHAKE_REQ (used to indicate that the DSP is alive)
				 LOAD_DSP_RESPONSE =102, 	// Status of DSP at the end of loading DSP program process
				 DSP2PC_MSG = 103,	// sed to send data to the PC in unfreeze mode through DSP2PC structure (as described at 6.2.1)
				 HW_INFO_RESPONSE =105,	// status message that send the board hardware information to the PC (DSP and FPGA versions)
				 BURN_DSP_RESPONSE = 106,	// Status of DSP at the end of burning DSP program to Flash memory process
				 BURN_FPGA_RESPONSE = 107,	// Status of DSP at the end of burning FPGA firmware to Flash memory process
				 DEBUG_INFO_REQUEST = 108
			};
			//class CFrame;
			class CDspMsg 	{
			public:
	#pragma pack(1)
				typedef struct MsgHeader {
					USHORT usMsgType;
					USHORT usEffectivePacketSize;
					unsigned int FrameNum;
					USHORT	ucPacketNum;
					USHORT	ucTotalPackets;
					unsigned int	usOffset;
					unsigned int uiTimestamp;
					unsigned int Reserved;
					unsigned int Reserved1;
				} DSP_MSG_HEADER;
	#pragma pack()

				static const int DSP_MSG_MAX_LEN = /*INT_MAX*/0x8000/* - sizeof(DSP_MSG_HEADER)*/;

				CDspMsg(Infra *pInfra, CRmdDebug *pRmdDebug);
				~CDspMsg();
				RMD_ERROR_CODE DspMsgInit(int iDataLen);
				DSP_MSG_HEADER *m_pDspMsgHeader;
				char *m_pcMem;
				char *m_pcData;

				int m_iDataLen;
				int m_iMemSz;

				CDspMsg *SplitMsg(int TailOffset, int TailLen);
			private:
				Infra *m_pInfra;
				CRmdDebug *m_pRmdDebug;
				void DspMsgReset(void);
			};
		}
	}
}