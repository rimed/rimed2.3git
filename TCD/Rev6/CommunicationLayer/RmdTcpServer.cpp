#include "RmdTcpServer.h"

using namespace Rimed::TCD::CommunicationLayer;
using namespace RmdTcpServer;
using namespace RmdSocket;
using namespace Rimed::TCD::Infrastructure;

#define RMD_TCP_SERVER_THREAD_FINALIZE_TIMEOUT 100
#define RMD_TCP_SERVER_SM_MTX_TIMEOUT 100

int CRmdTcpServer::s_Counter = 0;
CRmdTcpServer::CRmdTcpServer(Infra *pInfra, CRmdDebug *pRmdDebug)
{
	m_pInfra				= pInfra;
	m_pRmdDebug				= pRmdDebug;

	m_pRmdListenSocket		= NULL;
	m_pRmdConnectionSocket	= NULL;
	TcpServerState			= TCP_SERVER_IDLE_STATE;

	m_id = s_Counter++;
	InitializeCriticalSection(&m_cs);

	DBG_TRACE(m_pRmdDebug, INFO_TRC, "CRmdTcpServer::CRmdTcpServer(). Id=%i", m_id);

	INFRA_NEW_OBJ(m_pInfra, m_ListOfRxMsgs, CListDspMsgsSafe, m_pInfra, m_pRmdDebug);
	INFRA_NEW_OBJ(m_pInfra, m_ListOfTxMsgs, CListDspMsgsSafe, m_pInfra, m_pRmdDebug);

	m_pRxThread = NULL;
	m_pTxThread = NULL;
	m_iBackLog = 0;
	m_iNumOfConnections = 0;
	m_iMaxMsgSize = 0;
	m_uiMaxNumOfRxMsgs = 0;
}

CRmdTcpServer::~CRmdTcpServer(void)
{
	DBG_TRACE(m_pRmdDebug, INFO_TRC, "CRmdTcpServer::~CRmdTcpServer(). Id=%i", m_id);

	//	DBG_TRACE(m_pRmdDebug, INFO_TRC," CRmdTcpServer::~CRmdTcpServer TcpServerState=%d <---- ", TcpServerState);
	//DBG_ASSERT(m_pRmdDebug,m_pRmdListenSocket == NULL && m_pRmdConnectionSocket==NULL);	Ofer, 2013.10.03
	//DBG_ASSERT(m_pRmdDebug,((TcpServerState == TCP_SERVER_FINISH_STATE) || (TcpServerState == TCP_SERVER_IDLE_STATE)));	Ofer, 2013.10.03

	INFRA_DELETE_OBJ(m_pInfra,  m_ListOfRxMsgs, CListDspMsgsSafe);
	INFRA_DELETE_OBJ(m_pInfra,  m_ListOfTxMsgs, CListDspMsgsSafe);

	DeleteCriticalSection(&m_cs);
}

RMD_ERROR_CODE CRmdTcpServer::RmdTcpServerStart(int BackLog, USHORT Port, ULONG ulBindAddr, int iMaxMsgSize, unsigned int uiMaxNumOfRxMsgs)
{
	DBG_TRACE(m_pRmdDebug, INFO_TRC, "CRmdTcpServer::RmdTcpServerStart(). Id=%i", m_id);

	m_iBackLog = BackLog;
	m_usPort = Port;
	m_iMaxMsgSize = iMaxMsgSize;
	m_uiMaxNumOfRxMsgs = uiMaxNumOfRxMsgs;
	m_ulBindAddr = ulBindAddr;
	RMD_ERROR_CODE RmdErr = RmdTcpServerSMGo(TCP_SERVER_INIT_EVENT);
	if(RmdErr != RMD_SUCCESS)
	{
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdTcpServer::RmdTcpServerStart(). Id=%i. Error=%i.", m_id, RmdErr);
		//throw CRmdException(RmdErr, __LINE__, __FILE__);	//Ofer, 2013.12.29
	}

	return RmdErr;
}

RMD_ERROR_CODE CRmdTcpServer::RmdTcpServerStop(void)
{
	DBG_TRACE(m_pRmdDebug, INFO_TRC, "CRmdTcpServer::RmdTcpServerStop()");

	RMD_ERROR_CODE RmdErr = RmdTcpServerSMGo(TCP_SERVER_CLOSE_EVENT);
	if(m_pRmdListenSocket!=NULL) 
	{
		RmdErr = m_pRmdListenSocket->RmdSockClose();
		if(RmdErr!=RMD_SUCCESS)
			DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdTcpServer::RmdTcpServerCloseSocket(): Listen Socket Close failed (err=%i).", RmdErr);
			
		m_pRmdListenSocket = NULL;
	}

	if(m_pRmdConnectionSocket!=NULL) 
	{
		RmdErr = m_pRmdConnectionSocket->RmdSockClose();
		if(RmdErr!=RMD_SUCCESS)
			DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdTcpServer::RmdTcpServerCloseSocket(): Connection Socket Close failed (err=%i).", RmdErr);

		m_pRmdConnectionSocket = NULL;
	}

	if(m_pRxThread!=NULL) 
	{
		m_pRxThread->RmdThreadFinalize(RMD_TCP_SERVER_THREAD_FINALIZE_TIMEOUT);
		INFRA_DELETE_OBJ(m_pInfra, m_pRxThread, CRmdThread);
		m_pRxThread=NULL;
	}

	if(m_pTxThread!=NULL) 
	{
		m_pTxThread->RmdThreadFinalize(RMD_TCP_SERVER_THREAD_FINALIZE_TIMEOUT);
		INFRA_DELETE_OBJ(m_pInfra, m_pTxThread, CRmdThread);
		m_pTxThread=NULL;
	}
	
	if(m_pRmdListenSocket!=NULL) 
	{
		INFRA_DELETE_OBJ(m_pInfra, m_pRmdListenSocket, CRmdSocket);
		m_pRmdListenSocket = NULL;
	}

	if(m_pRmdConnectionSocket!=NULL) 
	{
		INFRA_DELETE_OBJ(m_pInfra, m_pRmdConnectionSocket, CRmdSocket);
		m_pRmdConnectionSocket = NULL;
	}

	m_ListOfRxMsgs->ListDspMsgsClear();
	m_ListOfTxMsgs->ListDspMsgsClear();

	return RMD_SUCCESS;
}

RMD_ERROR_CODE CRmdTcpServer::RmdTcpServerSMGoIdleState(E_TCP_SERVER_EVENT TcpServerEvent)
{
	RMD_ERROR_CODE RmdErr = RMD_SUCCESS;
	DWORD dwThreadId;
	switch(TcpServerEvent) 
	{
		case TCP_SERVER_TX_EVENT:
			//v2.0.5.1, Ofer DBG_TRACE(m_pRmdDebug, ERROR_TRC,"CRmdTcpServer::RmdTcpServerSMGoIdleState hold packets until connection");
			/*
			while(!m_ListOfTxMsgs->empty()) {
				pDspMsg = m_ListOfTxMsgs->pop_front();
				INFRA_DELETE_OBJ(m_pInfra, pDspMsg, CDspMsg);
			}
			*/
			return RMD_SUCCESS;
			break;
	
		case TCP_SERVER_RX_EVENT:
			return RMD_SUCCESS;
			break;
		case TCP_SERVER_INIT_EVENT:
			if(m_pRmdListenSocket != NULL) 
			{
				DBG_TRACE(m_pRmdDebug, ERROR_TRC,"CRmdTcpServer::RmdTcpServerSMGoIdleState() <---- m_pRmdSocket != NULL state=%d event=%d", TcpServerState, TcpServerEvent);
				return RMD_FAILED;
			}

			INFRA_NEW_OBJ(m_pInfra, m_pRmdListenSocket, CRmdSocket, m_pInfra, m_pRmdDebug);
			try 
			{
				RmdErr = m_pRmdListenSocket->RmdSockCreate(RMD_TCP_PROT);
				if (RmdErr != RMD_SUCCESS)		//Ofer, 2013.12.29
				{
					m_pRmdListenSocket->RmdSockClose();
					INFRA_DELETE_OBJ(m_pInfra, m_pRmdListenSocket, CRmdSocket);
					m_pRmdListenSocket = NULL;	

					return RmdErr;
				}

				//RmdErr = m_pRmdListenSocket->RmdSockBind(m_usPort, INADDR_ANY);
				//RmdErr = m_pRmdListenSocket->RmdSockBind(m_usPort, INADDR_LOOPBACK);
				RmdErr = m_pRmdListenSocket->RmdSockBind(m_usPort, m_ulBindAddr);
				if (RmdErr != RMD_SUCCESS)		//Ofer, 2013.12.29
				{
					m_pRmdListenSocket->RmdSockClose();
					INFRA_DELETE_OBJ(m_pInfra, m_pRmdListenSocket, CRmdSocket);
					m_pRmdListenSocket = NULL;	

					return RmdErr;
				}

				RmdErr = m_pRmdListenSocket->RmdSockSetup	(65536,65536);
				if (RmdErr != RMD_SUCCESS)		//Ofer, 2013.12.29
				{
					m_pRmdListenSocket->RmdSockClose();
					INFRA_DELETE_OBJ(m_pInfra, m_pRmdListenSocket, CRmdSocket);
					m_pRmdListenSocket = NULL;	

					return RmdErr;
				}
			}
			catch(CRmdException &RmdExc)
			{
				DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdTcpServer::RmdTcpServerSMGoIdleState().EXCEPTION: Connection Socket Close failed (err=%i, Inner=%i, File=%s, Line=%i).", RmdExc.m_dwErrorCode, RmdExc.m_dwOriginalCode, RmdExc.m_acFileName, RmdExc.m_dwLine);

				m_pRmdListenSocket->RmdSockClose();
				INFRA_DELETE_OBJ(m_pInfra, m_pRmdListenSocket, CRmdSocket);
				m_pRmdListenSocket = NULL;	

				return RmdExc.m_dwErrorCode;
			}

			TcpServerState = TCP_SERVER_LISTEN_STATE;
			INFRA_NEW_OBJ(m_pInfra, m_pRxThread, CRmdThread, m_pInfra, m_pRmdDebug);
			m_pRxThread->RmdThreadStart(CRmdTcpServer::RxThreadFunc,this,&dwThreadId);
			
			DBG_TRACE(m_pRmdDebug, INFO_TRC, "CRmdTcpServer::RmdTcpServerSMGoIdleState(): Thread Id=%i.", dwThreadId);
			
			m_pRxThread->RmdPostMsg(RMD_MSG_TCP_SERVER_LISTEN, 0,0);

			return RMD_SUCCESS;

		default:
			DBG_TRACE(m_pRmdDebug, ERROR_TRC,"CRmdTcpServer::RmdTcpServerSMGoIdleState() <---- Server is not initialized event=%d", TcpServerEvent);
			return RMD_SM_WRONG_EVENT4STATE;
	}
}

RMD_ERROR_CODE CRmdTcpServer::RmdTcpServerSMGoListenState(E_TCP_SERVER_EVENT TcpServerEvent)
{
	RMD_ERROR_CODE RmdErr;
	DWORD dwThreadId;
	switch(TcpServerEvent) 
	{
	case TCP_SERVER_ACCEPT_EVENT:
			DBG_TRACE(m_pRmdDebug, COMM_LAYER_TRC, "CRmdTcpServer::RmdTcpServerSMGoListenState(): TCP_SERVER_ACCEPT_EVENT");
			m_iNumOfConnections++;
			
			INFRA_NEW_OBJ(m_pInfra, m_pTxThread, CRmdThread, m_pInfra, m_pRmdDebug);
			m_pTxThread->RmdThreadStart(CRmdTcpServer::TxThreadFunc, this,&dwThreadId);
			DBG_TRACE(m_pRmdDebug, INFO_TRC, "CRmdTcpServer::RmdTcpServerSMGoListenState(): TxThread Id=%i.", dwThreadId);

			TcpServerState = TCP_SERVER_CONNECTED_STATE;
		break;
	case TCP_SERVER_CLOSE_EVENT:
			DBG_TRACE(m_pRmdDebug, INFO_TRC, "CRmdTcpServer::RmdTcpServerSMGoListenState(): m_pRxThread->RmdThreadStop()");
			m_pRxThread->RmdThreadStop();
			RmdErr = m_pRmdListenSocket->RmdSockShutdown();
			// Ignore bad status. If connection is broken this call was obsolete
			TcpServerState = TCP_SERVER_FINISH_STATE;
		break;
	case TCP_SERVER_TX_EVENT:
		//v2.0.5.1, Ofer DBG_TRACE(m_pRmdDebug, ERROR_TRC,"CRmdTcpServer::RmdTcpServerSMGoListenState hold packets until connection");
		/*
		while(!m_ListOfTxMsgs->empty()) {
			pDspMsg = m_ListOfTxMsgs->pop_front();
			INFRA_DELETE_OBJ(m_pInfra, pDspMsg, CDspMsg);
		}
		*/
		break;
	case TCP_SERVER_RX_EVENT:
		break;

	case TCP_SERVER_INIT_EVENT:
	default:
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdTcpServer::RmdTcpServerSMGoListenState: Bad state=%d,event=%d", TcpServerState, TcpServerEvent);
		return RMD_SM_WRONG_EVENT4STATE;
	}
	return RMD_SUCCESS;
}

RMD_ERROR_CODE CRmdTcpServer::RmdTcpServerSMGoConnectedState(E_TCP_SERVER_EVENT TcpServerEvent)
{
	//RMD_ERROR_CODE RmdErr;
	switch(TcpServerEvent) 
	{
	case TCP_SERVER_TX_EVENT:
		m_pTxThread->RmdPostMsg(RMD_MSG_TCP_SERVER_SEND, 0,0);
		break;
	case TCP_SERVER_RX_EVENT:
		m_pRxThread->RmdPostMsg(RMD_MSG_TCP_SERVER_RECEIVE, 0,0);
		break;
	case TCP_SERVER_CLOSE_EVENT:
		/*
		m_pTxThread->RmdPostMsg(RMD_MSG_TCP_SERVER_SEND_DISCONNECT, 0,0);
		break;
	case TCP_SERVER_CLOSE_CONFIRM_EVENT:
	*/
		DBG_TRACE(m_pRmdDebug, INFO_TRC, "CRmdTcpServer::RmdTcpServerSMGoConnectedState(): m_pRxThread->RmdThreadStop()");

		m_iNumOfConnections = 0;
		m_pRxThread->RmdThreadStop();
		m_pTxThread->RmdThreadStop();

		// Ignore bad status. If connection is broken it was obsolete
		TcpServerState = TCP_SERVER_FINISH_STATE;
		break;
	case TCP_SERVER_ACCEPT_EVENT:
	case TCP_SERVER_INIT_EVENT:
	default:
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdTcpServer::RmdTcpServerSMGoConnectedState(): Bad state=%d,event=%d", TcpServerState, TcpServerEvent);
		return RMD_SM_WRONG_EVENT4STATE;
	}
	return RMD_SUCCESS;
}

RMD_ERROR_CODE CRmdTcpServer::RmdTcpServerSMGoFinishState(E_TCP_SERVER_EVENT TcpServerEvent)
{
	switch(TcpServerEvent) 
	{
	case TCP_SERVER_ACCEPT_EVENT:
		break;
	case TCP_SERVER_CLOSE_EVENT:
			DBG_TRACE(m_pRmdDebug, COMM_LAYER_TRC, "CRmdTcpServer::RmdTcpServerSMGoFinishState(): TCP_SERVER_CLOSE_EVENT");
		break;
	case TCP_SERVER_TX_EVENT:
			//v2.0.5.1, Ofer DBG_TRACE(m_pRmdDebug, COMM_LAYER_TRC, "CRmdTcpServer::RmdTcpServerSMGoFinishState(): TCP_SERVER_TX_EVENT Flush packets");
		m_ListOfTxMsgs->ListDspMsgsClear();
		break;
	case TCP_SERVER_RX_EVENT:
		break;
	default:
			DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdTcpServer::RmdTcpServerSMGoFinishState(): Bad event=%d, delete object before renew", TcpServerEvent);
		return RMD_SM_WRONG_EVENT4STATE;
	}
	return RMD_SUCCESS;
}

RMD_ERROR_CODE CRmdTcpServer::RmdTcpServerSMGo(E_TCP_SERVER_EVENT TcpServerEvent)
{
	RMD_ERROR_CODE RmdErr;

	//OFER, 2013.04.11:
	//	1. Add try...finally block
	//  2. Replace Mutex with CriticalSection

	EnterCriticalSection(&m_cs);

	//	DBG_TRACE(m_pRmdDebug, COMM_LAYER_TRC, "CRmdTcpServer::RmdTcpServerSMGo: ---->State=%d, TcpServerEvent=%d", TcpServerState, TcpServerEvent);
	__try 
	{
		switch (TcpServerState) 
		{
			case TCP_SERVER_IDLE_STATE:
				RmdErr = RmdTcpServerSMGoIdleState(TcpServerEvent);
				break;
			case TCP_SERVER_LISTEN_STATE:
				RmdErr = RmdTcpServerSMGoListenState(TcpServerEvent);
				break;
			case TCP_SERVER_CONNECTED_STATE:
				RmdErr =RmdTcpServerSMGoConnectedState(TcpServerEvent);
				break;
			case TCP_SERVER_FINISH_STATE:
				RmdErr =RmdTcpServerSMGoFinishState(TcpServerEvent);
				break;
			default:
				//v2.0.5.1, Ofer DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdTcpServer::RmdTcpServerSMGo: Bad state=%d", TcpServerState);
				RmdErr = RMD_FAILED;
				break;
		}
	}
	__finally
	{
		//DBG_TRACE(m_pRmdDebug, COMM_LAYER_TRC, "CRmdTcpServer::RmdTcpServerSMGo: <----State=%d, TcpServerEvent=%d", TcpServerState, TcpServerEvent);
		LeaveCriticalSection(&m_cs);
	}
	
	return RmdErr;
}

 DWORD CRmdTcpServer::RxThreadFunc(LPVOID lpThreadParameter)
 {
	 OutputDebugStringA("[RIMED] CRmdTcpServer::RxThreadFunc(): THREAD START");

 	 CRmdTcpServer *pThis = (CRmdTcpServer*)lpThreadParameter;
	 CRmdThread *pRxThread = pThis->m_pRxThread;
	 unsigned int uMsg, uParam;
	 unsigned long ulParam;
	 BOOL isMsgAvailable;
	 RMD_ERROR_CODE RmdErr = RMD_SUCCESS;
	 E_RMD_THREAD_STATUS eThreadStatus = pRxThread->RmdRunTimeFn(INFINITE);

	 //while(eThreadStatus == RMD_THREAD_RUNNING)
	 //{
		// isMsgAvailable = pRxThread->RmdPeekMsg (&uMsg,&uParam, &ulParam);
		// 
 	//	 if(isMsgAvailable) 
		// {
		//	if (uMsg==RMD_MSG_KILL)
		//	{
		//		OutputDebugStringA("[RIMED] CRmdTcpServer::RxThreadFunc(): RMD_MSG_KILL message recieved. THREAD ABORT");
		//		break;	//return 0;
		//	}
		//	else 
		//	{
		//		RmdErr = pThis->RxMessageFunc(uMsg,uParam,ulParam);
		//		if (RmdErr != RMD_SUCCESS)
		//		{
		//			OutputDebugStringA("[RIMED] CRmdTcpServer::RxThreadFunc(). RcvMessage Error. THREAD ABORT");
		//			break;	//return 0;
		//		}
		//		
		//		eThreadStatus = pRxThread->RmdRunTimeFn(0);
		//}
		//else 
		//{
		//	eThreadStatus = pRxThread->RmdRunTimeFn(INFINITE);
		//}
		//
	 //}

	 while(eThreadStatus == RMD_THREAD_RUNNING)
	 {
		isMsgAvailable = pRxThread->RmdPeekMsg(&uMsg,&uParam, &ulParam);
		if (!isMsgAvailable)
		{
			eThreadStatus = pRxThread->RmdRunTimeFn(INFINITE);
			continue;
		}

		if (uMsg == RMD_MSG_KILL)
		{
			OutputDebugStringA("[RIMED] CRmdTcpServer::RxThreadFunc(): RMD_MSG_KILL message recieved. THREAD ABORT");
			break;	
		}

		RmdErr = pThis->RxMessageFunc(uMsg,uParam,ulParam);
		if (RmdErr == RMD_SUCCESS)
			eThreadStatus = pRxThread->RmdRunTimeFn(0);			
		else
		{
			OutputDebugStringA("[RIMED] CRmdTcpServer::RxThreadFunc()->RxMessageFunc(...): Return error.");
			eThreadStatus = pRxThread->RmdRunTimeFn(INFINITE);
		}
	 }

	 OutputDebugStringA("[RIMED] CRmdTcpServer::RxThreadFunc(): THREAD END");

	 return 0;
 }

 void CRmdTcpServer::TcpServerGetClientAddr(RMD_SOCK_ADDR *DestAddr)
 {
	 
 }

 RMD_ERROR_CODE CRmdTcpServer::RxMessageFunc (unsigned int uMsg, unsigned int uParam, unsigned long ulParam)
 {
	RMD_ERROR_CODE RmdErr = RMD_SUCCESS;
	CDspMsg *pDspMsg = NULL;
	CDspMsg *TailMsg = NULL;
	int RxLen;
	try 
	{
		switch(uMsg)
		{
			case RMD_MSG_TCP_SERVER_LISTEN:
					RmdErr = m_pRmdListenSocket->RmdSockTcpServerListen(m_iBackLog);
					if (RmdErr != RMD_SUCCESS)
						break;
					 
					//v2.0.5.1, Ofer DBG_TRACE(m_pRmdDebug, COMM_LAYER_TRC, "CRmdTcpServer::RxMessageFunc: RmdSockTcpServerListen ret=%d",RmdErr);
					INFRA_NEW_OBJ(m_pInfra, m_pRmdConnectionSocket, CRmdSocket, m_pInfra, m_pRmdDebug);
					RmdErr = m_pRmdConnectionSocket->RmdSockTcpServerAccept(m_pRmdListenSocket);
					if (RmdErr != RMD_SUCCESS)
						break;

					//v2.0.5.1, Ofer DBG_TRACE(m_pRmdDebug, COMM_LAYER_TRC, "CRmdTcpServer::RxMessageFunc: RmdSockTcpServerAccept ret=%d",RmdErr);
					RmdErr = RmdTcpServerSMGo(TCP_SERVER_ACCEPT_EVENT);
				 break;
			 
			case RMD_MSG_TCP_SERVER_RECEIVE:
					INFRA_NEW_OBJ(m_pInfra, pDspMsg, CDspMsg, m_pInfra, m_pRmdDebug);
					if (pDspMsg == NULL)								//Ofer, 2014.01.30
						break;											//Ofer, 2014.01.30

					RmdErr = pDspMsg->DspMsgInit(m_iMaxMsgSize);
					if (RmdErr != RMD_SUCCESS)
					{
						INFRA_DELETE_OBJ(m_pInfra, pDspMsg, CDspMsg);	//Ofer, 2014.01.30
						break;
					}

					RmdErr = m_pRmdConnectionSocket->RmdSockRecv(pDspMsg->m_pcMem,pDspMsg->m_iMemSz,&RxLen);
					if (RmdErr != RMD_SUCCESS)
					{
						INFRA_DELETE_OBJ(m_pInfra, pDspMsg, CDspMsg);	//Ofer, 2014.01.30	
						break;
					}

					if(m_ListOfRxMsgs->ListDspMsgsSize() >= m_uiMaxNumOfRxMsgs) 
						INFRA_DELETE_OBJ(m_pInfra, pDspMsg, CDspMsg)
					else
						m_ListOfRxMsgs->ListDspMsgsPushBackSafe(pDspMsg, RMD_TCP_SERVER_SM_MTX_TIMEOUT);
				
					m_pRxThread->RmdPostMsg(RMD_MSG_TCP_SERVER_RECEIVE, 0, 0);
				break;

			 default:
				break;	//return RmdErr; //DBG_ASSERT(m_pRmdDebug,0);	Ofer, 2013.10.03
		}
	}
	catch(CRmdException &RmdExc) 
	{
		//DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdTcpServer::RxMessageFunce().EXCEPTION (err=%i, Inner=%i, File=%s, Line=%i).", RmdExc.m_dwErrorCode, RmdExc.m_dwOriginalCode, RmdExc.m_acFileName, RmdExc.m_dwLine);

		if(pDspMsg != NULL)
			INFRA_DELETE_OBJ(m_pInfra, pDspMsg, CDspMsg);

		//RmdTcpServerSMGo(TCP_SERVER_CLOSE_EVENT);					//Ofer, 2014.01.30

		return RmdExc.m_dwErrorCode;
	}
	
	return RmdErr;
 }

 DWORD CRmdTcpServer::TxThreadFunc(LPVOID lpThreadParameter)
 {
	 OutputDebugStringA("[RIMED] CRmdTcpServer::TxThreadFunc(): THREAD START");

	 RMD_ERROR_CODE RmdErr;
	 CRmdTcpServer *pThis = (CRmdTcpServer*)lpThreadParameter;
	 CRmdThread *pTxThread = pThis->m_pTxThread;
	 unsigned int uMsg, uParam;
	 unsigned long ulParam;
	 BOOL boMsg;
	 E_RMD_THREAD_STATUS eThreadStatus = pTxThread->RmdRunTimeFn(INFINITE);
	 while(eThreadStatus == RMD_THREAD_RUNNING)
	 {
		 boMsg = pTxThread->RmdPeekMsg (&uMsg,&uParam, &ulParam);
		 if(!boMsg) 
		 {
			 eThreadStatus = pTxThread->RmdRunTimeFn(INFINITE);
		 }
		 else 
		 {
			 if(uMsg == RMD_MSG_KILL)
			 {
				OutputDebugStringA("[RIMED] CRmdTcpServer::TxThreadFunc(): RMD_MSG_KILL message recieved. THREAD ABORT");
				break;	//return 0;
			 }
			 else 
			 {
				 RmdErr = pThis->TxMessageFunc (uMsg, uParam, ulParam);
				 if (RmdErr == RMD_SUCCESS)
					eThreadStatus = pTxThread->RmdRunTimeFn(0);
			 }
		 }
	 }

	 OutputDebugStringA("[RIMED] CRmdTcpServer::TxThreadFunc(): THREAD END");

	 return 0;
 }


 RMD_ERROR_CODE CRmdTcpServer::TxMessageFunc (unsigned int uMsg, unsigned int uParam, unsigned long ulParam)
 {
	//RMD_ERROR_CODE RmdErr;
	//CDspMsg *pDspMsg = NULL;
	//try 
	//{
	//	switch(uMsg)
	//	{
	//		case RMD_MSG_TCP_SERVER_SEND:
	//				pDspMsg = m_ListOfTxMsgs->ListDspMsgsPopFrontSafe(RMD_TCP_SERVER_SM_MTX_TIMEOUT);
	//				if(pDspMsg == NULL)
	//					return RMD_SUCCESS;
	//			 
	//				 RmdErr = m_pRmdConnectionSocket->RmdSockSend(pDspMsg->m_pcMem, pDspMsg->m_iDataLen);

	//				 if (RmdErr != RMD_SUCCESS)						//Ofer, 2013.12.29
	//					 return RmdErr;

	//				 INFRA_DELETE_OBJ(m_pInfra, pDspMsg, CDspMsg);

	//				 //Send blocking
	//				 m_pTxThread->RmdPostMsg(RMD_MSG_TCP_SERVER_SEND, 0, 0);
	//			 break;
	//		case RMD_MSG_TCP_SERVER_SEND_DISCONNECT:															//2013.10.14 Ofer - Comment out since code executed but invokation location is unknown / unclear.
	//				 //INFRA_NEW_OBJ(m_pInfra, pDspMsg, CDspMsg, m_pInfra, m_pRmdDebug);						//2013.10.14 Ofer
	//				 //RmdErr =pDspMsg->DspMsgInit(4);
	//				 //pDspMsg->m_pDspMsgHeader->ucPacketNum = 0;
	//				 //pDspMsg->m_pDspMsgHeader->ucTotalPackets = 1;
	//				 //pDspMsg->m_pDspMsgHeader->usEffectivePacketSize = 4;
	//				 //pDspMsg->m_pDspMsgHeader->usMsgType = RESET_DSP_REQ;
	//				 //RmdErr = m_pRmdConnectionSocket->RmdSockSend(pDspMsg->m_pcMem, pDspMsg->m_iDataLen);
	//				 //RmdTcpServerSMGo(TCP_SERVER_CLOSE_CONFIRM_EVENT);
	//				DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdTcpServer::TxMessageFunc(): Unintended execution - RMD_MSG_TCP_SERVER_SEND_DISCONNECT");	//2013.10.14 Ofer	
	//				//throw CRmdException(RMD_UNINTENDED_EXECUTION, __LINE__, __FILE__);								//Ofer, 2013.12.29
	//			 break;
	//		 default:
	//			 break;	//return RMD_SUCCESS; //DBG_ASSERT(m_pRmdDebug,0);	Ofer, 2013.10.03
	//	}
	//}
	//catch(CRmdException &RmdExc)
	//{
	//	//DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdTcpServer::TxMessageFunc().EXCEPTION (err=%i, Inner=%i, File=%s, Line=%i).", RmdExc.m_dwErrorCode, RmdExc.m_dwOriginalCode, RmdExc.m_acFileName, RmdExc.m_dwLine);

	//	if(pDspMsg!=NULL)
	//		INFRA_DELETE_OBJ(m_pInfra, pDspMsg, CDspMsg);
	//	
	//	RmdTcpServerSMGo(TCP_SERVER_CLOSE_EVENT);						

	//	return RmdExc.m_dwErrorCode;
	//}

	if (uMsg != RMD_MSG_TCP_SERVER_SEND)
		return RMD_SUCCESS;

	RMD_ERROR_CODE RmdErr;
	CDspMsg *pDspMsg = NULL;
	try 
	{
		pDspMsg = m_ListOfTxMsgs->ListDspMsgsPopFrontSafe(RMD_TCP_SERVER_SM_MTX_TIMEOUT);
		if (pDspMsg == NULL)
			return RMD_SUCCESS;
				 
		RmdErr = m_pRmdConnectionSocket->RmdSockSend(pDspMsg->m_pcMem, pDspMsg->m_iDataLen);
		INFRA_DELETE_OBJ(m_pInfra, pDspMsg, CDspMsg);
		if (RmdErr != RMD_SUCCESS)						//Ofer, 2013.12.29
			return RmdErr;

		//Send blocking
		m_pTxThread->RmdPostMsg(RMD_MSG_TCP_SERVER_SEND, 0, 0);
	}
	catch(CRmdException &RmdExc)
	{
		if (pDspMsg != NULL)
			INFRA_DELETE_OBJ(m_pInfra, pDspMsg, CDspMsg);
		
		//RmdTcpServerSMGo(TCP_SERVER_CLOSE_EVENT);							//Ofer, 2014.01.30

		return RmdExc.m_dwErrorCode;
	}

	return RMD_SUCCESS;
 }


 int CRmdTcpServer::GetNumOfConnections(void)
 {
	 return m_iNumOfConnections;
 }

 RMD_ERROR_CODE CRmdTcpServer::TcpServerMsgRx(CDspMsg** ppDspMsg)
 {
	 RMD_ERROR_CODE RmdErr;
	 *ppDspMsg = m_ListOfRxMsgs->ListDspMsgsPopFrontSafe(RMD_TCP_SERVER_SM_MTX_TIMEOUT);
	 if(*ppDspMsg==NULL)
		 RmdErr = RMD_LIST_EMPTY;
	 else
		 RmdErr = RMD_SUCCESS;

	 RmdTcpServerSMGo(TCP_SERVER_RX_EVENT);

	 return RmdErr;
 }

 RMD_ERROR_CODE CRmdTcpServer::TcpServerMsgTx(CDspMsg* pDspMsg)
 {
	 m_ListOfTxMsgs->ListDspMsgsPushBackSafe(pDspMsg, RMD_TCP_SERVER_SM_MTX_TIMEOUT);
	 return RmdTcpServerSMGo(TCP_SERVER_TX_EVENT);
 }
