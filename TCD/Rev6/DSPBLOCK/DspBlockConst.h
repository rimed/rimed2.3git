#pragma once
#include "TCD_DSP2PC.H"
#include "TCD_PC2DSP.H"

using namespace Rimed::TCD::DspBlock;

namespace Rimed
{
	namespace TCD
{
	namespace DspBlock
	{
		public ref class DspBlockConst 
		{
		public:
			literal int NUM_OF_PROBES = NUMBER_OF_PROBES;
			literal int NUM_OF_PIXELS_IN_COLMN = 256;			
			literal int NUM_OF_PIXELS_IN_AUTOSCAN_COLMN = DspBlock::NUM_OF_PIXELS_IN_AUTOSCAN_COLMN;
			literal int NUM_OF_COLUMNS = 3;

			literal double COLUMN_DURATION     = 7.86432;

			literal int SIZE_OF_ENVELOPE = sizeof(R_Envelope);
			literal int SIZE_OF_ENVELOPE_GATE = sizeof(R_Envelope_Gate);
			literal int SIZE_OF_ENVELOPE_INDEXES = sizeof(R_Envelope_Indexes);
			literal int SIZE_OF_FFT_COLMN = sizeof(struct R_FFT_Column_s);
			literal int SIZE_OF_FFT_SPECTRUM_GATE = sizeof(struct R_FFT_Spectrum_Gate_s);
			//literal int NUM_OF_SPECTRUM_GATES = 16;
			literal int SIZE_OF_FFT_SPECTRUM = sizeof(struct R_FFT_Spectrum_s);
			literal int SIZE_OF_FFT_ENVELOPES = sizeof(struct FFT_Envelopes_s);
			literal int SIZE_OF_HITS_Det = sizeof(struct HITS_Det_s);
			literal int SIZE_OF_HITS_GATE = sizeof(struct HITS_Gate_s);
			literal int SIZE_OF_HITS_DATA = sizeof(struct HITS_Data_s);
			literal int SIZE_OF_CLINICAL_PARAMETERS_VALUE = sizeof(struct Clinical_Parameters_Value_s);
			literal int SIZE_OF_CLINICAL_PARAMETERS_GATE = sizeof(struct Clinical_Parameters_Gate_s);
			literal int SIZE_OF_CLINICAL_PARAMETERS = sizeof(struct Clinical_Parameters_s);
			literal int SIZE_OF_R_AUTO_SCAN_PROBE = sizeof(struct Auto_Scan_Probe_s);
			literal int SIZE_OF_R_AUTO_SCAN_COLUMN = sizeof(struct Auto_Scan_Column_s);
			literal int SIZE_OF_AUTO_SCAN = sizeof(struct Auto_Scan_s);
			literal int SIZE_OF_TIME_DOMAIN = sizeof(struct Time_Domain_s);
			literal int SIZE_OF_TIME_DOMAIN_GATE = sizeof(struct Time_Domain_Gate_s);
			literal int SIZE_OF_TIME_DOMAIN_VAL = sizeof(struct Time_Domain_Value_s);

			literal int SIZE_OF_EX_CHANNEL = sizeof(struct Ex_Channel_s);
			literal int SIZE_OF_R_DSP2PC = sizeof(struct DSP2PC_s);			
			literal int SIZE_OF_R_PC2DSP = sizeof(struct R_PC2DSP_str);
			literal int SIZE_OF_R_GATE_INFO = sizeof(struct R_Gate_Info_str);
			literal int SIZE_OF_R_CARD_INFO = sizeof(struct R_Card_Info_str);
			literal int SIZE_OF_R_PROBE_INFO = sizeof(struct R_Probe_Info_str);
			literal int SIZE_OF_EX_COLUMN_CHANNEL = sizeof(struct Colum_Channel_s);
			
			literal unsigned short FFT_SIZE64 = 6;
			literal unsigned short FFT_SIZE128 = 7;
			literal unsigned short FFT_SIZE256 = 8;

			literal int SIZE_OF_PC2DSP_ECHO = sizeof(struct PC2DSP_Echo_s);

			literal int NUM_OF_GATES_PER_PROBE = GATES_PER_PROBE;
		};
	}
}
}
