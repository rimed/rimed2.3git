#include "DspBlockConst.h"
#include "UtBoundaryDsp2Pc.h"

using namespace System;
using namespace Rimed::TCD::DspBlock;
using namespace Rimed::TCD::ManagedInfrastructure;
using namespace Rimed::TCD::Infrastructure;

CDsp2PcDataDebug::CDsp2PcDataDebug(CDebugBoundary ^pDebugBoundary)
{
	m_pRawData	= NULL;
	m_arrDsp2Pc = NULL;

	IntPtr iptr;
	iptr		= pDebugBoundary->RmdDebugGetNativeObj();

	m_pRmdDebug = (CRmdDebug*)iptr.ToPointer();
	m_Dsp2Pc	= (R_DSP2PC*)malloc(sizeof(R_DSP2PC));
}

CDsp2PcDataDebug::~CDsp2PcDataDebug()
{
	free(m_Dsp2Pc);
}

#if 0
	void CDsp2PcDataDebug::PrintDsp2PcBlock(unsigned int Index, char *pRawData)
	{
		m_pRawData	= pRawData;
		m_arrDsp2Pc = (R_DSP2PC *)pRawData;
	}
#endif

void CDsp2PcDataDebug::InitManagedHits(array<BYTE>^ pData)
{	
	pin_ptr<BYTE> dest;
	dest	= &pData[0];

	m_Dsp2Pc->HITS_Data.m_Gate_sa[3].m_Column_sa[2].Energy = 20;
	int FieldOffset = (char*)(&(m_Dsp2Pc->HITS_Data.m_Gate_sa[3].m_Column_sa[2].Energy))-(char*)(m_Dsp2Pc);
	
	//v2.0.5.1, Ofer DBG_TRACE(m_pRmdDebug, INFO_TRC, "Forward offset=%d", FieldOffset);
	
	memcpy(dest, m_Dsp2Pc, sizeof(R_DSP2PC));
}

void CDsp2PcDataDebug::InitManagedClinicalParamsBlock(array<BYTE>^ pData)
{
	pin_ptr<BYTE> dest;
	dest = &pData[0];

	m_Dsp2Pc->Clinical_Parameters_Block.Heart_Rate = 20;
	int FieldOffset = (char*)(&(m_Dsp2Pc->Clinical_Parameters_Block.Heart_Rate))-(char*)(m_Dsp2Pc);
	
	//v2.0.5.1, Ofer DBG_TRACE(m_pRmdDebug, INFO_TRC, "Forward offset=%d", FieldOffset);
	
	memcpy(dest, m_Dsp2Pc, sizeof(R_DSP2PC));
}

void CDsp2PcDataDebug::InitManagedDsp2PcBlock(array<BYTE>^ pData)
{
	pin_ptr<BYTE> dest;
	dest = &pData[0];

	int ii	= 0;
	BYTE bi = 0;
	
	int GateNum;
	int ProbeNum;
	int ColNum;
	int FieldOffset;
	int PixNum;
	
	for(GateNum=0; GateNum<GATES_PER_PROBE * NUMBER_OF_PROBES; GateNum++) 
	{
		for(ColNum=0;ColNum<NUM_OF_COLUMNS; ColNum++) 
		{
			m_Dsp2Pc->FFT_Envelopes_Block.Peak.Gate[GateNum].Column[ColNum].Forward = bi+100;
			FieldOffset = (char*)(&(m_Dsp2Pc->FFT_Envelopes_Block.Peak.Gate[GateNum].Column[ColNum].Forward))-(char*)(m_Dsp2Pc);
			
			//v2.0.5.1, Ofer DBG_TRACE(m_pRmdDebug, INFO_TRC, "Forward offset=%d", FieldOffset);
			
			bi++;
			m_Dsp2Pc->FFT_Envelopes_Block.Peak.Gate[GateNum].Column[ColNum].Reverse = bi;
			FieldOffset = (char*)(&(m_Dsp2Pc->FFT_Envelopes_Block.Peak.Gate[GateNum].Column[ColNum].Reverse))-(char*)(m_Dsp2Pc);
			
			//v2.0.5.1, Ofer DBG_TRACE(m_pRmdDebug, INFO_TRC, "Reverse offset=%d", FieldOffset);
			
			bi++;
		}
	}

	bi = 0;
	for(GateNum=0; GateNum<GATES_PER_PROBE * NUMBER_OF_PROBES; GateNum++) 
	{
		for(ColNum=0;ColNum<NUM_OF_COLUMNS; ColNum++) 
		{
			for(PixNum=0;PixNum<256;PixNum++) 
			{
				m_Dsp2Pc->FFT_Spectrum_Block.Gate[GateNum].Column[ColNum].Index[PixNum] = bi;
				bi++;
			}
		}
	}

	bi = 0;
	for(ProbeNum=0; ProbeNum<NUMBER_OF_PROBES; ProbeNum++) 
	{
		for(ColNum=0;ColNum<NUM_OF_COLUMNS; ColNum++) 
		{
			for(PixNum=0;PixNum<NUM_OF_PIXELS_IN_AUTOSCAN_COLMN;PixNum++) 
			{
				m_Dsp2Pc->Auto_Scan_Block.R_Probe[ProbeNum].Column[ColNum].Pixel[PixNum] = bi;
				bi++;
			}
		}
	}

	memcpy(dest, m_Dsp2Pc, sizeof(R_DSP2PC));
}

void CDsp2PcDataDebug::InitManagedEchoPc2DspBlock(array<BYTE>^ pData)
{
	pin_ptr<BYTE>	dest;
	dest	= &pData[0];

	int ii	= 0;
	BYTE bi = 0;
	
	int	FieldOffset;
	FieldOffset = (char*)(&(m_Dsp2Pc->PC2DSP_Echo.m_ProbeInfo_sa[0].m_Gate_sa[1].m_Depth))-(char*)(m_Dsp2Pc);
	
	//v2.0.5.1, Ofer DBG_TRACE(m_pRmdDebug, INFO_TRC, "Depth offset=%d", FieldOffset);

	m_Dsp2Pc->PC2DSP_Echo.m_ProbeInfo_sa[1].m_NumOfGates = 16;
	FieldOffset = (char*)(&(m_Dsp2Pc->PC2DSP_Echo.m_ProbeInfo_sa[1].m_NumOfGates))-(char*)(m_Dsp2Pc);
	
	//v2.0.5.1, Ofer DBG_TRACE(m_pRmdDebug, INFO_TRC, "NumOfGates offset=%d", FieldOffset);
	
	memcpy(dest, m_Dsp2Pc, sizeof(R_DSP2PC));
}

void CDsp2PcDataDebug::PrintTimeDomainOffset()
{
	int FieldOffset;
	FieldOffset = (char*)(&(m_Dsp2Pc->Time_Domain_Block.Gate[8]))-(char*)(m_Dsp2Pc);
	
	//v2.0.5.1, Ofer DBG_TRACE(m_pRmdDebug, INFO_TRC, "Orig time domain gate offset=%d", FieldOffset);
}