#pragma once

#include "TCD_DSP2PC.H"
#include "RmdDebug.h"

using namespace Rimed::TCD::DspBlock;
using namespace Rimed::TCD::ManagedInfrastructure;
using namespace Rimed::TCD::Infrastructure;

namespace Rimed
{
	namespace TCD
	{
		namespace DspBlock
		{
			public ref class CDsp2PcDataDebug 
			{
				public:
					CDsp2PcDataDebug(CDebugBoundary ^pDebugBoundary);
					~CDsp2PcDataDebug();
					void InitManagedDsp2PcBlock(array<BYTE>^ pData);
					void InitManagedEchoPc2DspBlock(array<BYTE>^ pData);
					void InitManagedClinicalParamsBlock(array<BYTE>^ pData);
					void InitManagedHits(array<BYTE>^ pData);
					void PrintTimeDomainOffset();
	#if 0
					void PrintDsp2PcBlock(char *pRawData);
					void PrintManagedDsp2PcBlock(array<BYTE>^ %pData);
	#endif
			private:
					char *m_pRawData;
					R_DSP2PC *m_arrDsp2Pc;
					CRmdDebug *m_pRmdDebug;
					R_DSP2PC *m_Dsp2Pc;
			};
		}
	}
}