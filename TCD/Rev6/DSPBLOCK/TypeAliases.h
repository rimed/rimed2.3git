/****************************************************************************************************
* File Name		:	TypeAliases.h																	*
* Project		:	DSPBlock																		*
* Subsystem		:	DSP module																		*
* Description	:																					*
*																									*
* Date			:	2013.03.29																		*
* Programmer	:	Ofer Bester																		*
* Revision Notes:																					*
*																									*
* Notes			:																					*
* Platform		:	                            													*
****************************************************************************************************/
#pragma once

namespace Rimed
{
	namespace TCD
	{
		//namespace DspBlock
		//{
			#ifndef Uint8
				#define Uint8	unsigned	char
				#define Int8				char
				#define Uint16	unsigned	short
				#define Int16				short
				#define Uint32	unsigned	int
				#define Int32				int
			#endif
		//}
	}
}
/*#######################################  End Of TypeAliases.h File  ###########################################*/



