#include "DspBlockConst.h"
#include "UtBoundaryPc2Dsp.h"

using namespace System;
using namespace Rimed::TCD::DspBlock;
using namespace Rimed::TCD::ManagedInfrastructure;
using namespace Rimed::TCD::Infrastructure;

CPc2DspDataDebug::CPc2DspDataDebug(CDebugBoundary ^pDebugBoundary)
{
	IntPtr iptr;
	iptr = pDebugBoundary->RmdDebugGetNativeObj();

	m_pRmdDebug = (CRmdDebug*)iptr.ToPointer();
}

CPc2DspDataDebug::~CPc2DspDataDebug()
{

}

void CPc2DspDataDebug::Pc2DspPrintProbeInfo(array<BYTE>^ pData)
{
	R_PC2DSP Pc2Dsp;
	pin_ptr<BYTE> src;
	src = &pData[0];

	memcpy(&Pc2Dsp, src, sizeof(R_PC2DSP));
	int FieldOffset = (char*)(&(Pc2Dsp.m_ProbeInfo_sa[0].m_Gate_sa[0].m_Depth))-(char*)(&Pc2Dsp);
	
	//v2.0.5.1, Ofer DBG_TRACE(m_pRmdDebug, INFO_TRC, "Depth offset=%d", FieldOffset);
}