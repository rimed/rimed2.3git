#pragma once
#include "TCD_PC2DSP.H"
#include "RmdDebug.h"

using namespace Rimed::TCD::DspBlock;
using namespace Rimed::TCD::ManagedInfrastructure;
using namespace Rimed::TCD::Infrastructure;

namespace Rimed
{
	namespace TCD
	{
		namespace DspBlock
		{
			public ref class CPc2DspDataDebug 
			{
				public:
					CPc2DspDataDebug(CDebugBoundary ^pDebugBoundary);
					~CPc2DspDataDebug();
					void Pc2DspPrintProbeInfo(array<BYTE>^ pData);

				private:
					CRmdDebug *m_pRmdDebug;
			};
		}
	}
}