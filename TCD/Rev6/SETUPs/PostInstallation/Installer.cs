using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using Microsoft.Win32;
using Rimed.TCD.Utils;

namespace Rimed.TCD.PostInstallation
{
    [RunInstaller(true)]
    public class Installer : System.Configuration.Install.Installer
    {
        private const string TRACE_LINE_PREFIX = "[Rimed] Setup: ";
        private const string TRACE_CATEGORY = "Rimed.DigiLite.PostInstallation";

        private readonly System.ComponentModel.Container components = null;

        private void traceWriteLine(string msg)
        {
            Trace.WriteLine(TRACE_LINE_PREFIX + msg, TRACE_CATEGORY);
        }

        public Installer()
        {
            traceWriteLine("Installer.ctor().START");

            InitializeComponent();

            traceWriteLine("Installer.ctor().END");
        }

        protected override void Dispose(bool disposing)
        {
            traceWriteLine("Installer.Dispose().START");

            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);

            traceWriteLine("Installer.Dispose().END");
        }

        #region Component Designer generated code
        /// <summary>Required method for Designer support - do not modify the contents of this method with the code editor.</summary>
        private void InitializeComponent()
        {

        }
        #endregion

        #region Override methods
        public override void Install(IDictionary stateSaver)
        {
            traceWriteLine("Install(...).START");

            base.Install(stateSaver);

            createDir();

			traceWriteLine("Setting product configuration values....");

			updateStartMenuAppConfigKey("Application:OperationMode", "PRODUCTION");
			updateStartMenuAppConfigKey("Application:Window.Size", "Screen");

			updateGuiAppConfigKey("Application:OperationMode", "PRODUCTION");
			updateGuiAppConfigKey("Application:Window.Size", "Screen");

			var product = Context.Parameters["Product"];

			var isDigiLite = (string.Compare(General.EAppProduct.DIGI_LITE.ToString(), product, StringComparison.InvariantCultureIgnoreCase) == 0);
			if (isDigiLite)
		        configAsDigiLite();
	        else if (string.Compare(General.EAppProduct.REVIEW_STATION.ToString(), product, StringComparison.InvariantCultureIgnoreCase) == 0)
		        configAsReviewStation();
	        else
		        configAsDigiOne();

			updateGuiAppConfigKey("RemoteCom", (isDigiLite ? "COM2" : "AUTO"));

            execDicomRegistration();

            traceWriteLine("Install(...).END");
        }

        //public override void Commit(IDictionary savedState)
        //{
        //    base.Commit(savedState);
        //}

        //public override void Rollback(IDictionary savedState)
        //{
        //    base.Rollback(savedState);
        //}

        public override void Uninstall(IDictionary savedState)
        {
            traceWriteLine("Uninstall(...).START");

            var myKey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Rimed", true);
            if (myKey != null)
            {
                if (myKey.GetValue("OnePageReport") != null)
                    myKey.DeleteValue("OnePageReport");
            }

            base.Uninstall(savedState);

            traceWriteLine("Uninstall(...).END");
        }
        #endregion

		private void updateConfigKey(string key, string value, string configFile)
		{
			if (string.IsNullOrWhiteSpace(key) || string.IsNullOrWhiteSpace(configFile))
				return;

			if (value == null)
				value = string.Empty;

			try
			{
				traceWriteLine("Update configuration: " + key + "='" + value + "'.");
				var config = ConfigurationManager.OpenExeConfiguration(configFile);
				var element = config.AppSettings.Settings[key];
				if (element != null)
				{
					element.Value = value;
					config.Save(ConfigurationSaveMode.Modified);
				}

			}
			catch (Exception ex)
			{
				traceWriteLine("ERROR: Fail to update configuration file ('" + configFile + "'). Key='" + key + "', Value='" + value + "'.");
				traceWriteLine("EXCEPTION: "+ ex);
			}
		}

		private void updateGuiAppConfigKey(string key, string value)
		{
			updateConfigKey(key, value, Constants.RIMED_TCD_GUI_EXECUTABLE);
		}

		private void updateStartMenuAppConfigKey(string key, string value)
		{
			updateConfigKey(key, value, Constants.RIMED_STARTMENU_EXECUTABLE);
		}

		private void configAsDigiLite()
		{
			traceWriteLine("Configure as Digi-Lite.");

			updateStartMenuAppConfigKey("Application:Product", General.EAppProduct.DIGI_LITE.ToString());

			updateGuiAppConfigKey("Application:Product", General.EAppProduct.DIGI_LITE.ToString());
            // changed by Alex 30/11/2015 feature #672 v 2.2.3.14
            updateGuiAppConfigKey("Audio:PCSpeakers.Enable", "true"); 
			updateGuiAppConfigKey("Audio:Noise.Mute.Enabled", "false");
		}

		private void configAsDigiOne()
		{
			traceWriteLine("Configure as Digi-One.");

			updateStartMenuAppConfigKey("Application:Product", General.EAppProduct.DIGI_ONE.ToString());

			updateGuiAppConfigKey("Application:Product", General.EAppProduct.DIGI_ONE.ToString());
			updateGuiAppConfigKey("Audio:PCSpeakers.Enable", "true");
			updateGuiAppConfigKey("Audio:Noise.Mute.Enabled", "false");
		}

		private void configAsReviewStation()
		{
			traceWriteLine("Configure as ReviewStation.");

			updateStartMenuAppConfigKey("Application:Product", General.EAppProduct.REVIEW_STATION.ToString());

			updateGuiAppConfigKey("Application:Product", General.EAppProduct.REVIEW_STATION.ToString());
			updateGuiAppConfigKey("Audio:PCSpeakers.Enable", "true");
			updateGuiAppConfigKey("Audio:Noise.Mute.Enabled", "false");
		}
		
		private void createDir()
        {
            traceWriteLine("CreateDir().START");

            Directory.CreateDirectory(Constants.GATES_IMAGE_PATH);
            Directory.CreateDirectory(Constants.RAW_DATA_PATH);
            Directory.CreateDirectory(Constants.SUMMARY_IMG_PATH);
            Directory.CreateDirectory(Constants.TEMP_IMG_PATH);

            traceWriteLine("CreateDir().END");
        }

        private bool execDicomRegistration()
        {
            traceWriteLine("execDICOMRegistration().START");

            var cmdpArams = " "+ Constants.APP_PATH + "rzdcx.dll /s";
            traceWriteLine("Register DICOM COM DLL: " + cmdpArams);
            var res = false;
            try
            {
                //RIMD-358: COM exception in Dicom report
                using (var p = Process.Start("regsvr32", cmdpArams))
                {
                    p.WaitForExit(60000);
                }

                res = true;
            }
            catch (Exception ex)
            {
                traceWriteLine("execDICOMRegistration().EXCEPTION "+ ex);
                MessageBox.Show("Rimed DICOM server registratin fail.\n"+ ex.Message, "Rimed ltd.");
            }

            traceWriteLine("execDICOMRegistration().END");

            return res;
        }        
    }
}
