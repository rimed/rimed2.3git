﻿namespace Rimed.TCD.PostInstallation
{
	partial class SelectComPortForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectComPortForm));
			this.comboBoxComPort = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.checkBoxRemoteControl = new System.Windows.Forms.CheckBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.button1 = new System.Windows.Forms.Button();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// comboBoxComPort
			// 
			this.comboBoxComPort.Enabled = false;
			this.comboBoxComPort.FormattingEnabled = true;
			this.comboBoxComPort.Location = new System.Drawing.Point(69, 47);
			this.comboBoxComPort.Name = "comboBoxComPort";
			this.comboBoxComPort.Size = new System.Drawing.Size(213, 21);
			this.comboBoxComPort.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 50);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(57, 13);
			this.label1.TabIndex = 1;
			this.label1.Text = "Serial port:";
			// 
			// checkBoxRemoteControl
			// 
			this.checkBoxRemoteControl.AutoSize = true;
			this.checkBoxRemoteControl.Location = new System.Drawing.Point(9, 19);
			this.checkBoxRemoteControl.Name = "checkBoxRemoteControl";
			this.checkBoxRemoteControl.Size = new System.Drawing.Size(135, 17);
			this.checkBoxRemoteControl.TabIndex = 2;
			this.checkBoxRemoteControl.Text = "Enable Remore-Control";
			this.checkBoxRemoteControl.UseVisualStyleBackColor = true;
			this.checkBoxRemoteControl.CheckedChanged += new System.EventHandler(this.checkBoxRemoteControl_CheckedChanged);
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.checkBoxRemoteControl);
			this.groupBox1.Controls.Add(this.comboBoxComPort);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(2, -3);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(292, 132);
			this.groupBox1.TabIndex = 3;
			this.groupBox1.TabStop = false;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(103, 97);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 4;
			this.button1.Text = "OK";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// SelectComPortForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(297, 132);
			this.ControlBox = false;
			this.Controls.Add(this.button1);
			this.Controls.Add(this.groupBox1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "SelectComPortForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Rimed TCD System Remote-Control";
			this.TopMost = true;
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.ComboBox comboBoxComPort;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.CheckBox checkBoxRemoteControl;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button button1;
	}
}