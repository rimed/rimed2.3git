﻿using System;
using System.Collections.Generic;
using System.Management;
using System.Windows.Forms;

namespace Rimed.TCD.PostInstallation
{
	public partial class SelectComPortForm : Form
	{
		private class ComPortInfo
		{
			public ComPortInfo(string id, string name)
			{
				Id = id;
				Name = name;
			}

			public string Id { get; private set; }
			public string Name { get; private set; }

			public override string ToString()
			{
				return Name;
			}
		}


		public static string GetRemoteControlComPort(string defPort)
		{
			using (var dlg = new SelectComPortForm(defPort))
			{
				if (dlg.ShowDialog() == DialogResult.OK && dlg.SelectedPort != null)
					defPort = dlg.SelectedPort.Id;
			}

			return defPort;
		}

		private SelectComPortForm(string defPort)
		{
			InitializeComponent();

			DisabledPort = new ComPortInfo("NoCOM", "Disabled");
			SelectedPort = DisabledPort;

			loadMachineComPorts(defPort);
		}

		private ComPortInfo SelectedPort { get; set; }
		private ComPortInfo DisabledPort { get; set; }
		private readonly List<ComPortInfo> m_comPorts = new List<ComPortInfo>();


		private void loadMachineComPorts(string defPort)
		{
			m_comPorts.Add(DisabledPort);
			try
			{
				var searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_SerialPort");

				foreach (ManagementObject queryObj in searcher.Get())
				{
					var id = queryObj["DeviceID"];
					var name = queryObj["Caption"];
					if (id != null && name != null)
					{
						var com = new ComPortInfo(id.ToString(), name.ToString());
						m_comPorts.Add(com);
					}
				}

				searcher = new ManagementObjectSearcher("root\\WMI","SELECT * FROM MSSerial_PortName");
			    foreach (ManagementObject queryObj in searcher.Get())
				{
					var id = queryObj["PortName"] as string;
					var name = queryObj["InstanceName"] as string;
					//If the serial port's instance name contains USB it must be a USB to serial device
					if (id != null && name != null && name.Contains("USB"))
					{
						var com = new ComPortInfo(id, string.Format("USB.{0}", id));
						m_comPorts.Add(com);
					}
				}
			}
			catch (Exception ex)
			{
				System.Diagnostics.Trace.WriteLine(ex);
			}

			var selected = -1;
			foreach (var port in m_comPorts)
			{
				var ix = comboBoxComPort.Items.Add(port);
				if (string.Compare(defPort, port.Id, StringComparison.InvariantCultureIgnoreCase) == 0)
					selected = ix;
			}

			if (selected >= 0 && selected < m_comPorts.Count)
				comboBoxComPort.SelectedIndex = selected;
		}

		private void checkBoxRemoteControl_CheckedChanged(object sender, EventArgs e)
		{
			comboBoxComPort.Enabled = m_comPorts.Count > 0 && checkBoxRemoteControl.Checked;
			if (!comboBoxComPort.Enabled)
				SelectedPort = DisabledPort;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			SelectedPort = DisabledPort;
			if (checkBoxRemoteControl.Checked)
			{
				var port = comboBoxComPort.SelectedItem as ComPortInfo;
				if (port != null)
					SelectedPort = port;
			}

			DialogResult = DialogResult.OK;
		}

	}
}
