@ECHO OFF

::
:: Script parameters
::
SET WinRarEXE="C:\Program Files\WinRAR\WinRAR.EXE"

SET OutputFileName="..\..\Rimed TCD ReviewStation Rev.6 v2.02.05.0X.EXE"

ECHO.
ECHO =============================================================================================================
ECHO Setup wrapper batch (%0)
ECHO -------------------------------------------------------------------------------------------------------------
ECHO - Store current work folder (%CD%)
POPD %CD%

ECHO - Change current work folder....
CD /d %~dp0
ECHO   Current work folder:  %CD%

ECHO.
ECHO - Copying files....
COPY /Y /V ..\Output\Setup.*											.\FILEs\

ECHO.
ECHO - Input files:
DIR %CD%\FILEs\* /A-D /B /D /ON /4

ECHO.
ECHO - Output file:	
ECHO %OutputFileName%

ECHO.
ECHO Delete existing output file...
DEL /F /Q %OutputFileName%

ECHO - Wrapping....... 
::
:: 	WinRAR command line switches in use
::
:: 	-t		Test archive
:: 	-o+ 	Overwrite all (default for updating archived files); 
:: 	-sfx	Create self-extract archive
:: 	-rr5	Add recovery record (5%)
:: 	-r		Add recursive
:: 	-z		Add comment
%WinRarEXE% a -sfx -rr5p -t -o+ -r -ep -iicon%CD%\Rimed.ico -iimg%CD%\Rimed.LOGO.bmp -zCreateRarSfx.Comment %OutputFileName% .\FILEs\*

ECHO - Wrapping compleated.

ECHO.
ECHO - Clean up....
DEL /F /Q .\FILEs\Setup.*
DEL /F /Q ..\Output\Setup.*	

ECHO - Restore previous work folder
PUSHD

ECHO.
ECHO - Open product folder
%SystemRoot%\explorer.exe "%~dp0\..\..\..\"

ECHO.
ECHO DONE.
ECHO =============================================================================================================
ECHO.

