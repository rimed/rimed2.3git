﻿using System;

namespace Rimed.TCD.ManagedInfrastructure
{
	public class CSmEvent<TEnumEvent>
	{
		public CSmEvent(TEnumEvent smEventNum)
		{
			SmEventNum = smEventNum;
		}

		public CSmEvent(TEnumEvent dspSmEventNum, object[] paramsArr)
		{
			SmEventNum          = dspSmEventNum;
			SmEventParamsArr    = paramsArr;
		}

		public TEnumEvent SmEventNum { get; private set; }
		public object[] SmEventParamsArr { get; private set; }
	}

	public class CSmEventsList<TEnumEvent> : RmdListSafe<CSmEvent<TEnumEvent>>
	{
		public void AddEvent(TEnumEvent smEventNum, string strParam)
		{
			object[] eventParamsArr = new string[1];
			eventParamsArr[0] = strParam;
			var smEvent = new CSmEvent<TEnumEvent>(smEventNum, eventParamsArr);
			AddSafe(smEvent);
		}

		public void AddEvent(TEnumEvent smEventNum)
		{
			var smEvent = new CSmEvent<TEnumEvent>(smEventNum, null);
			AddSafe(smEvent);
		}

		public void AddEvent(TEnumEvent smEventNum, object[] eventParamsArr)
		{
			var smEvent = new CSmEvent<TEnumEvent>(smEventNum, eventParamsArr);
			AddSafe(smEvent);
		}
	}
}
