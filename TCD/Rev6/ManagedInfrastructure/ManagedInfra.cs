﻿using System;
using System.Collections.Generic;

namespace Rimed.TCD.ManagedInfrastructure
{
	[SerializableAttribute]
	public class RmdListSafe<T> : List<T> 
	{
		public void AddSafe(T item)
		{
            Add(item);
		}

		public void RemoveRangeSafe(int index, int count)
		{
            RemoveRange(index, count);
		}

        public new bool Remove(T item)
        {
			lock (m_locker)
				return base.Remove(item);
        }

        public new void RemoveAt(int index)
        {
			lock (m_locker)
            {
                //Range check
                if (index < 0 || index >= Count)
                    return;

                base.RemoveAt(index);
            }
        }

        public new void Clear()
        {
			lock (m_locker)
                base.Clear();
        }

		public new T this[int index]
		{
		    get
		    {
				lock (m_locker)
                {
					//Range check
					if (index < 0 || index >= Count)
						return default(T);

					return base[index];
                }
            }
		    set
		    {
				lock (m_locker)
                {
                    //Range check
                    if (index < 0 || index >= Count)
                        return;

                    base[index] = value;
                }
            }
		}

        public new void Add(T item)
        {
			lock (m_locker)
                base.Add(item);
        }

        public new void RemoveRange(int index, int count)
        {
			lock (m_locker)
            {
                //Range & parameter check
                if (count <= 0 || index < 0 || index + count >= Count)
                    return;

                base.RemoveRange(index, count);
            }
        }

		public new int Capacity
		{
		    get
		    {
				lock (m_locker)
		            return base.Capacity;
		    }
		    set
		    {
				lock (m_locker)
		            base.Capacity = value;
		    }
		}

		private readonly object m_locker = new object();
	}
}