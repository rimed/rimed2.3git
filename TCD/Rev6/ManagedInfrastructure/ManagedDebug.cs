﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace TCD
{
	namespace ManagedInfrastructure
	{
		public class CManagedDebug
		{
			public const uint AUTOSCAN_TRC = CDebugBoundary.MGD_LOW_LEVEL_TRC_LAST;
			public const uint SPECTRUM_TRC = AUTOSCAN_TRC<<1;
			public const uint FILE_MGR_TRC = SPECTRUM_TRC << 1;
			public const uint DSP_MGR_TRC = FILE_MGR_TRC << 1;
			public const uint DSP_DATA_TRC = DSP_MGR_TRC << 1;

			public CManagedDebug(String FileName, uint uiLevel)
			{
				m_DebugBoundary = new CDebugBoundary(FileName);
				m_uLevel = uiLevel;
				m_DebugBoundary.DebugBoundaryTraceSetLevel(m_uLevel);
			}

			//[Conditional("DEBUG")]
			public void ManagedDbgTrace(uint Level, String strFormat, params Object[] FormatParams)
			{
				if ((m_uLevel & Level) == Level)
				{
					String s = String.Format(strFormat, FormatParams);
					m_DebugBoundary.DebugBoundaryTrace(Level, s);
				}
			}

			//[Conditional("DEBUG")]
			public void ManagedDebugAssert(bool condition)
			{
				if (!condition) {
					//StackTrace st = new StackTrace(new StackFrame(true));
					StackTrace st = new StackTrace(new StackFrame(1, true));
					ManagedDbgTrace(CDebugBoundary.MGD_ERROR_TRC, "ASSERTION!\n Stack trace for current level: {0}", st.ToString());
					StackFrame sf = st.GetFrame(0);
					ManagedDbgTrace(CDebugBoundary.MGD_ERROR_TRC, " File: {0} Method: {1} Line Number: {2}",
						sf.GetFileName(), sf.GetMethod().Name, sf.GetFileLineNumber());
					m_DebugBoundary.DebugBoundaryTraceStop();
				}
				Debug.Assert(condition);
			}

			public CDebugBoundary DebugBoundary { get { return m_DebugBoundary;} }

			private uint m_uLevel;
			/// <summary>
			/// Allocate ourselves. We have a private constructor, so no one else can.
			/// </summary>			
			private CDebugBoundary m_DebugBoundary;
		}
	}
}
