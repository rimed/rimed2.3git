﻿using System;
using Rimed.TCD.Utils;

namespace Rimed.TCD.ManagedInfrastructure
{
	public class InfraBoundaryWrapper
	{
	    public static readonly InfraBoundaryWrapper Wrapper = new InfraBoundaryWrapper();

	    public const uint AUTOSCAN_TRC = CDebugBoundary.MGD_LOW_LEVEL_TRC_LAST;
	    public const uint SPECTRUM_TRC = AUTOSCAN_TRC << 1;
	    public const uint FILE_MGR_TRC = SPECTRUM_TRC << 1;
	    public const uint DSP_MGR_TRC = FILE_MGR_TRC << 1;
	    public const uint DSP_DATA_TRC = DSP_MGR_TRC << 1;

	    private const uint TRACE_LEVEL = CDebugBoundary.MGD_INFO_TRC | CDebugBoundary.MGD_ERROR_TRC | DSP_MGR_TRC | FILE_MGR_TRC;

        private InfraBoundaryWrapper(string fileName = null)
        {
            if (string.IsNullOrWhiteSpace(fileName))
                fileName = Constants.APP_LOGS_PATH + @"\" + DateTime.UtcNow.ToString("yyyy.MM.dd") + ".LOG"; 

            DebugBoundar = new CDebugBoundary(fileName);
            DebugBoundar.DebugBoundaryTraceSetLevel(TRACE_LEVEL);

            InfraBoundary = new CInfraBoundary(DebugBoundar);
        }

        public bool IsInitiated 
        { 
            get { return DebugBoundar != null && InfraBoundary != null; }
        }

	    public CDebugBoundary DebugBoundar { get; private set; }
        public CInfraBoundary InfraBoundary { get; private set; }
	}
}

