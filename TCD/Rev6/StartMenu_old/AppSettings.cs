﻿using System;
using System.Windows.Forms;
using Rimed.Framework.Config;
using Rimed.TCD.Utils;

namespace Rimed.TCD.StartMenu
{
	public class AppSettings
	{
		private General.EAppProduct m_appProduct;
		public static AppSettings Active { get; private set; }

		static AppSettings()
		{
			Active = new AppSettings();
		}


		public int							ApplicatioWidth { get; private set; }
		public int							ApplicationHeight { get; private set; }
		public General.EAppOperationMode	AppOperationMode { get; private set; }
		private General.EAppProduct			AppProduct
		{
			get { return m_appProduct; }
			set
			{
				m_appProduct = value;
				ApplicationProductName = General.AppProductName(m_appProduct);
			}
		}

		public string ApplicationProductName { get; private set; }

		public bool ShowApplicationCloseButton
		{
			get
			{
				return EnableApplicationClose && AppConfig.GetConfigValue("ShowApplicationCloseButton", false);
			}
		}

		public bool EnableApplicationClose
		{
			get
			{
				return AppConfig.GetConfigValue("EnableApplicationClose", false);
			}
		}
		public bool ShowWindowsShutdownButton
		{
			get
			{
				return AppConfig.GetConfigValue("ShowWindowsShutdownButton", true);
			}
		}

		private AppSettings()
		{
			load();
		}



		private General.EApplicationSize ApplicationSize { get; set; }

		private void init()
		{
			ApplicationSize		= General.EApplicationSize.NONE;
			ApplicatioWidth		= Constants.DIGI_LITE_SCREEN_WIDTH;
			ApplicationHeight	= Constants.DIGI_LITE_SCREEN_HEIGHT;
			AppOperationMode	= General.EAppOperationMode.PRODUCTION;
			AppProduct			= General.EAppProduct.TCD;
		}

		private void load()
		{
			init();

			var value = AppConfig.GetConfigValue("Application:Window.Size");
			General.EApplicationSize enumVal;
			if (Enum.TryParse(value, true, out enumVal))
				setApplicationSize(enumVal);
			else
				setApplicationSize(General.EApplicationSize.NONE);

			value = AppConfig.GetConfigValue("Application:OperationMode");
			General.EAppOperationMode operationMode;
			if (Enum.TryParse(value, true, out operationMode))
				AppOperationMode = operationMode;

			value = AppConfig.GetConfigValue("Application:Product");
			General.EAppProduct appProduct;
			if (Enum.TryParse(value, true, out appProduct))
				AppProduct = appProduct;
		}

		private void setApplicationSize(General.EApplicationSize resizeMode)
		{
			ApplicationSize = resizeMode;

			if (ApplicationSize == General.EApplicationSize.CUSTOM)
			{
				ApplicatioWidth = AppConfig.GetConfigValue("Application:Window.Size.Custom.Width", ApplicatioWidth);
				ApplicationHeight = AppConfig.GetConfigValue("Application:Window.Size.Custom.Height", ApplicationHeight);
			}
			else if (ApplicationSize == General.EApplicationSize.SCREEN)
			{
				var srn = Screen.PrimaryScreen;
				ApplicatioWidth	= srn.Bounds.Width;
				ApplicationHeight = srn.Bounds.Height;					
			}
			else if (ApplicationSize == General.EApplicationSize.AUTO)
			{
				var srn = Screen.PrimaryScreen;
				ApplicatioWidth = srn.Bounds.Width;
				ApplicationHeight = 800;
			}

			if (ApplicatioWidth < Constants.DIGI_LITE_SCREEN_WIDTH)
				ApplicatioWidth = Constants.DIGI_LITE_SCREEN_WIDTH;

			if (ApplicationHeight < Constants.DIGI_LITE_SCREEN_HEIGHT)
				ApplicationHeight = Constants.DIGI_LITE_SCREEN_HEIGHT;
		}
	}
}
