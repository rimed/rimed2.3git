﻿
using Rimed.TCD.Utils;

namespace Rimed.TCD.StartMenu
{
    partial class MainForm
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.btnTcd = new System.Windows.Forms.Button();
            this.btnCarotid = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnExitWindows = new System.Windows.Forms.Button();
            this.btnExitApplication = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnTcd
            // 
            this.btnTcd.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnTcd.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnTcd.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnTcd.Location = new System.Drawing.Point(328, 240);
            this.btnTcd.Name = "btnTcd";
            this.btnTcd.Size = new System.Drawing.Size(368, 32);
            this.btnTcd.TabIndex = 0;
            this.btnTcd.Text = "TCD application ";
            this.btnTcd.UseVisualStyleBackColor = false;
            this.btnTcd.Click += new System.EventHandler(this.btnTcd_Click);
            // 
            // btnCarotid
            // 
            this.btnCarotid.BackColor = System.Drawing.Color.LightSlateGray;
            this.btnCarotid.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnCarotid.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnCarotid.Location = new System.Drawing.Point(328, 284);
            this.btnCarotid.Name = "btnCarotid";
            this.btnCarotid.Size = new System.Drawing.Size(368, 32);
            this.btnCarotid.TabIndex = 1;
            this.btnCarotid.Text = "Digi-Lite™IP carotid imaging application ";
            this.btnCarotid.UseVisualStyleBackColor = false;
            this.btnCarotid.Click += new System.EventHandler(this.btnCarotid_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1024, 768);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // btnExitWindows
            // 
            this.btnExitWindows.BackColor = System.Drawing.Color.DarkRed;
            this.btnExitWindows.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnExitWindows.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnExitWindows.Location = new System.Drawing.Point(406, 499);
            this.btnExitWindows.Name = "btnExitWindows";
            this.btnExitWindows.Size = new System.Drawing.Size(213, 32);
            this.btnExitWindows.TabIndex = 3;
            this.btnExitWindows.Text = "Windows Shutdown";
            this.btnExitWindows.UseVisualStyleBackColor = false;
            this.btnExitWindows.Click += new System.EventHandler(this.btnExitWindows_Click);
            // 
            // btnExitApplication
            // 
            this.btnExitApplication.BackColor = System.Drawing.Color.Tomato;
            this.btnExitApplication.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnExitApplication.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnExitApplication.Location = new System.Drawing.Point(406, 428);
            this.btnExitApplication.Name = "btnExitApplication";
            this.btnExitApplication.Size = new System.Drawing.Size(213, 32);
            this.btnExitApplication.TabIndex = 2;
            this.btnExitApplication.Text = "Exit Rimed™ menu";
            this.btnExitApplication.UseVisualStyleBackColor = false;
            this.btnExitApplication.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnExitApplication_MouseClick);
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.ControlBox = false;
            this.Controls.Add(this.btnExitApplication);
            this.Controls.Add(this.btnExitWindows);
            this.Controls.Add(this.btnCarotid);
            this.Controls.Add(this.btnTcd);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Main Menu";
            this.TopMost = true;
            this.Closing += new System.ComponentModel.CancelEventHandler(this.mainForm_Closing);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.mainForm_FormClosed);
            this.Load += new System.EventHandler(this.mainForm_Load);
            this.VisibleChanged += new System.EventHandler(this.mainForm_VisibleChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.mainForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnTcd;
        private System.Windows.Forms.Button btnCarotid;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnExitWindows;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
        private System.Windows.Forms.Button btnExitApplication;
    }
}

//Deleted by Alex 07/12/2015  Yosi's request v2.2.3.16
//using Rimed.TCD.Utils;

//namespace Rimed.TCD.StartMenu
//{
//    partial class MainForm
//    {
//        /// <summary>
//        /// Clean up any resources being used.
//        /// </summary>
//        protected override void Dispose(bool disposing)
//        {
//            if (disposing)
//            {
//                if (components != null)
//                {
//                    components.Dispose();
//                }
//            }
//            base.Dispose(disposing);
//        }

//        #region Windows Form Designer generated code

//        /// <summary>
//        /// Required method for Designer support - do not modify
//        /// the contents of this method with the code editor.
//        /// </summary>
//        private void InitializeComponent()
//        {
//            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
//            this.btnTcd = new System.Windows.Forms.Button();
//            this.btnCarotid = new System.Windows.Forms.Button();
//            this.pictureBox1 = new System.Windows.Forms.PictureBox();
//            this.btnExitWindows = new System.Windows.Forms.Button();
//            this.btnExitApplication = new System.Windows.Forms.Button();
//            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
//            this.SuspendLayout();
//            // 
//            // btnTcd
//            // 
//            this.btnTcd.BackColor = System.Drawing.Color.LightSlateGray;
//            this.btnTcd.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
//            this.btnTcd.ForeColor = System.Drawing.SystemColors.ControlLightLight;
//            this.btnTcd.Location = new System.Drawing.Point(63, 233);
//            this.btnTcd.Name = "btnTcd";
//            this.btnTcd.Size = new System.Drawing.Size(368, 256);
//            this.btnTcd.TabIndex = 0;
//            this.btnTcd.Text = "TCD application ";
//            this.btnTcd.UseVisualStyleBackColor = false;
//            this.btnTcd.Click += new System.EventHandler(this.btnTcd_Click);
//            // 
//            // btnCarotid
//            // 
//            this.btnCarotid.BackColor = System.Drawing.Color.LightSlateGray;
//            this.btnCarotid.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
//            this.btnCarotid.ForeColor = System.Drawing.SystemColors.ControlLightLight;
//            this.btnCarotid.Location = new System.Drawing.Point(570, 233);
//            this.btnCarotid.Name = "btnCarotid";
//            this.btnCarotid.Size = new System.Drawing.Size(368, 256);
//            this.btnCarotid.TabIndex = 1;
//            this.btnCarotid.Text = "Digi Lite IP carotid & intracranial imaging application (TCCS)";
//            this.btnCarotid.UseVisualStyleBackColor = false;
//            this.btnCarotid.Click += new System.EventHandler(this.btnCarotid_Click);
//            // 
//            // pictureBox1
//            // 
//            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
//            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
//            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
//            this.pictureBox1.Name = "pictureBox1";
//            this.pictureBox1.Size = new System.Drawing.Size(1024, 768);
//            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
//            this.pictureBox1.TabIndex = 4;
//            this.pictureBox1.TabStop = false;
//            // 
//            // btnExitWindows
//            // 
//            this.btnExitWindows.BackColor = System.Drawing.Color.DarkRed;
//            this.btnExitWindows.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
//            this.btnExitWindows.ForeColor = System.Drawing.SystemColors.ControlLightLight;
//            this.btnExitWindows.Location = new System.Drawing.Point(296, 599);
//            this.btnExitWindows.Name = "btnExitWindows";
//            this.btnExitWindows.Size = new System.Drawing.Size(418, 53);
//            this.btnExitWindows.TabIndex = 3;
//            this.btnExitWindows.Text = "Windows Shutdown";
//            this.btnExitWindows.UseVisualStyleBackColor = false;
//            this.btnExitWindows.Click += new System.EventHandler(this.btnExitWindows_Click);
//            // 
//            // btnExitApplication
//            // 
//            this.btnExitApplication.BackColor = System.Drawing.Color.Tomato;
//            this.btnExitApplication.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
//            this.btnExitApplication.ForeColor = System.Drawing.SystemColors.ControlLightLight;
//            this.btnExitApplication.Location = new System.Drawing.Point(296, 524);
//            this.btnExitApplication.Name = "btnExitApplication";
//            this.btnExitApplication.Size = new System.Drawing.Size(418, 53);
//            this.btnExitApplication.TabIndex = 2;
//            this.btnExitApplication.Text = "Exit Rimed™ menu";
//            this.btnExitApplication.UseVisualStyleBackColor = false;
//            this.btnExitApplication.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnExitApplication_MouseClick);
//            // 
//            // MainForm
//            // 
//            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
//            this.BackColor = System.Drawing.Color.LightSteelBlue;
//            this.ClientSize = new System.Drawing.Size(1024, 768);
//            this.ControlBox = false;
//            this.Controls.Add(this.btnExitApplication);
//            this.Controls.Add(this.btnExitWindows);
//            this.Controls.Add(this.btnCarotid);
//            this.Controls.Add(this.btnTcd);
//            this.Controls.Add(this.pictureBox1);
//            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
//            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
//            this.KeyPreview = true;
//            this.MaximizeBox = false;
//            this.MinimizeBox = false;
//            this.Name = "MainForm";
//            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
//            this.Text = "Main Menu";
//            this.TopMost = true;
//            this.Closing += new System.ComponentModel.CancelEventHandler(this.mainForm_Closing);
//            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
//            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.mainForm_FormClosed);
//            this.Load += new System.EventHandler(this.mainForm_Load);
//            this.VisibleChanged += new System.EventHandler(this.mainForm_VisibleChanged);
//            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.mainForm_KeyDown);
//            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
//            this.ResumeLayout(false);

//        }

//        #endregion

//        private System.Windows.Forms.Button btnTcd;
//        private System.Windows.Forms.Button btnCarotid;
//        private System.Windows.Forms.PictureBox pictureBox1;
//        private System.Windows.Forms.Button btnExitWindows;
//        /// <summary>
//        /// Required designer variable.
//        /// </summary>
//        private System.ComponentModel.Container components = null;
//        private System.Windows.Forms.Button btnExitApplication;
//    }
//}