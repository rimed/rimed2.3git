using System;
using System.Diagnostics;
using System.Drawing;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using Rimed.Framework.Common;
using Rimed.Framework.WinAPI;
using Rimed.Framework.WinInternal;
using Rimed.TCD.Utils;
using Constants = Rimed.TCD.Utils.Constants;
using Network_MedDev;
using Rimed.Framework.Config;

namespace Rimed.TCD.StartMenu
{
    public partial class MainForm : System.Windows.Forms.Form
    {
		const int ERROR_FILE_NOT_FOUND = 2;
        const int ERROR_ACCESS_DENIED = 5;

        private const int WM_QUERYENDSESSION = 0x0011;
		private static bool s_shuttingDown;
		private static bool s_isCloseApplication;

		private const string PROCESS_PATH_DIGI_IMAGER	= "Digi-Imager";
		private const string PROCESS_PATH_DIGI_LITE_IP	= "Digi-Lite tm IP";
	    private const string PROCESS_NAME_ECHOWAVE		= "EchoWave.exe";

	    private const string RIMED_MAUNMENU_TEXT = "Rimed.TCD Start Menu";

		private readonly static WindowsSpecialKeysWatcher s_windowsSpecialKeysWatcher = new WindowsSpecialKeysWatcher();

		/// <summary>The main entry point for the application.</summary>
		[STAThread]
		static void Main()
		{
			var hWnd = User32.GetWindowHandle(RIMED_MAUNMENU_TEXT);
			if (hWnd != IntPtr.Zero)
			{
				User32.WindowRestore(hWnd);
				return;
			}

			try
			{
				if (AppSettings.Active.AppOperationMode != General.EAppOperationMode.DEBUG)
				{
					Logger.LogInfo("Hide Windows TaskBar and Start menu.");
					User32.WindowsTaskBarHide();

					s_windowsSpecialKeysWatcher.Start();
				}

				//Set application process affinity
				Logger.LogInfo("Set Process affinity = 1.");
				Process.GetCurrentProcess().ProcessorAffinity = (IntPtr)1;

				Application.Run(new MainForm());
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);
			}
			finally
			{
				Logger.LogInfo("Restore Windows TaskBar and Start menu.");
				User32.WindowsTaskBarShow();
				s_windowsSpecialKeysWatcher.End();
			}
		}

		private ProcessWrapper m_dlProcess;
		private ProcessWrapper m_diProcess;

		//private string m_ewVersion = PROCESS_PATH_DIGI_LITE_IP;

        private ConnectionInfo connectionInfo;
        private string originalConnection = "";


        private MainForm()
        {
			InitializeComponent();
			Text						= RIMED_MAUNMENU_TEXT;
			btnTcd.Text					= AppSettings.Active.ApplicationProductName + " TCD application";
			btnExitApplication.Visible	= AppSettings.Active.ShowApplicationCloseButton;
	        btnExitWindows.Visible		= AppSettings.Active.ShowWindowsShutdownButton;
            
            // Changed by Alex 02/07/2015 feature #661 v 2.2.3.1
            //btnExitApplication.Visible	= AppSettings.Active.ShowApplicationCloseButton;
            btnExitApplication.Visible = (AppSettings.Active.ApplicationProductName == "Digi-One�" || AppSettings.Active.ApplicationProductName == "ReviewStation�");

            // Added by Alex 13/04/2015 Change request #645 v 2.2.3.1
            btnExitWindows.Visible = AppSettings.Active.ShowWindowsShutdownButton;
            originalConnection = AppConfig.GetConfigValue("OriginalConnection", "");
            string ipDigiOne = AppConfig.GetConfigValue("IPDigiOne", "");
            string maskDigiOne = AppConfig.GetConfigValue("MaskDigiOne", "");

            if (originalConnection != "")
            {
                connectionInfo = NetworkManagement.NetshCommandGetIpAndMask(originalConnection);
                NetworkManagement.NetshCommandSwitchToStatic(ipDigiOne, maskDigiOne, originalConnection);
            }
        }

        // Deleted by Alex 07/12/2015  Yosi's request v2.2.3.16
        //private void mainForm_Load(object sender, EventArgs e)
        //{
        //    Left		= 0;
        //    Top			= 0;
        //    Size		= new Size(AppSettings.Active.ApplicatioWidth, AppSettings.Active.ApplicationHeight);
        //    MaximumSize = Size;
        //    MinimumSize = Size;

        //    // Changed by Alex 08/11/2015 bug #806 v 2.2.3.9
        //    btnTcd.Left = Width / 2 - btnTcd.Width - 20;
        //    btnCarotid.Left = Width / 2 + 20;
        //    btnTcd.Top = (int)(MinimumSize.Height / 3.7) + 20; // 209 768 291 1080
        //    btnCarotid.Top = btnTcd.Top;

        //    btnExitApplication.Left = (Width - btnExitApplication.Width) / 2;
        //    btnExitWindows.Left = btnExitApplication.Left;

        //    btnExitApplication.Top = btnTcd.Top + btnTcd.Height + 20;
        //    btnExitWindows.Top = btnExitApplication.Top + btnExitApplication.Height + 20;
        //    TopMost = true;

        //    exitProcesses();	//killProcesses();
        //}

        private void mainForm_Load(object sender, EventArgs e)
        {
            Left = 0;
            Top = 0;
            Size = new Size(AppSettings.Active.ApplicatioWidth, AppSettings.Active.ApplicationHeight);
            MaximumSize = Size;
            MinimumSize = Size;

            btnTcd.Left = (Width - btnTcd.Width) / 2;
            btnCarotid.Left = (Width - btnCarotid.Width) / 2;
            btnExitApplication.Left = (Width - btnExitApplication.Width) / 2;
            btnExitWindows.Left = (Width - btnExitWindows.Width) / 2;

            TopMost = true;

            exitProcesses();	//killProcesses();
        }

		private void show()
		{
			if (s_isCloseApplication)
				return;

			if (InvokeRequired)
			{
				Invoke((Action)show);
				return;
			}

			s_windowsSpecialKeysWatcher.End();
			s_windowsSpecialKeysWatcher.Start();

			restoreMainMenu();
			User32.WindowRestore(Handle);	
			TopMost = true;
			Cursor = Cursors.Default;
			Focus();
		}

		private void hide()
		{
			if (InvokeRequired)
			{
				Invoke((Action)hide);
				return;
			}

			User32.WindowHide(Handle); //WindowsController.HideWindow(Text);
			TopMost = false;
		}

        private void btnTcd_Click(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick("btnTcd", Name);

            btnTcd.BackColor = Color.LightSteelBlue;

			m_dlProcess.TryDispose();
			m_dlProcess = null;

			m_dlProcess = new ProcessWrapper();
            runApplication(Constants.RIMED_TCD_GUI_EXECUTABLE, false, m_dlProcess);
        }

        private void btnCarotid_Click(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick("btnCarotid", Name);

			btnCarotid.BackColor = Color.LightSteelBlue;
			Cursor = Cursors.WaitCursor;

			if (m_diProcess != null && m_diProcess.HasStarted && !m_diProcess.HasExited)
			{
				m_diProcess.Restore();	//WindowsController.RestoreWindow(m_ewVersion);
				hide();
			}
			else if (!runEchoWaveApplication(Environment.SpecialFolder.ProgramFilesX86))
			{
				show();
				LoggedDialog.Show(this, "DigiLite� IP application was not purchased");
			}
		
			Cursor = Cursors.Default;
		}

        private bool runEchoWaveApplication(Environment.SpecialFolder programFilesFolder)
        {
			var pathFmt = Environment.GetFolderPath(programFilesFolder) + "\\Rimed\\{0}\\" + PROCESS_NAME_ECHOWAVE;
            var path	= string.Format(pathFmt, PROCESS_PATH_DIGI_IMAGER);
			if (!File.Exists(path))
				path = string.Format(pathFmt, PROCESS_PATH_DIGI_LITE_IP);

	        if (!File.Exists(path))
		        return false;

			m_diProcess.TryDispose();
			m_diProcess = null;

			m_diProcess = new ProcessWrapper();

			return runApplication(path, true, m_diProcess);
        }

		private bool runApplication(string fileName, bool isDigiImager, ProcessWrapper appProcess)
        {
            Logger.LogInfo("runApplication(fileName={0}, isDigiImager={1})", fileName, isDigiImager);

			if (appProcess == null)
			{
				show();	
				return false;
			}

            Cursor = Cursors.WaitCursor;

			var ret = true;
            try
            {
                btnTcd.Enabled				= false;
                btnCarotid.Enabled			= false;
                btnExitWindows.Enabled		= false;
				btnExitApplication.Enabled	= false;

                appProcess.OnExited			+= onProcessExited;
				appProcess.OnIconicChanged	+= onProcessIconicChanged;
				if (!appProcess.Start(fileName))
				{
					ret = false;
					show();
					LoggedDialog.ShowError(this, string.Format("Application ({0}) startup fail.", fileName));
				}
            }
            catch (Win32Exception ex)
            {
				ret = false;
				
				if (ex.NativeErrorCode == ERROR_FILE_NOT_FOUND)
					LoggedDialog.Show(this, "DigiLite� IP application was not purchased.", exception: ex);
                else if (ex.NativeErrorCode == ERROR_ACCESS_DENIED)
					LoggedDialog.Show(this, "Missing permission for the DigiLite� Carotid imaging.", exception: ex);
                else
					LoggedDialog.ShowError(this, ex.Message, exception: ex);

	            show();		// restoreMainMenu();
			}
            catch (Exception ex)
            {
				ret = false;

				LoggedDialog.ShowError(this, ex.Message, exception: ex);
				show();		// restoreMainMenu();
            }

			Cursor = Cursors.Default;

			return ret;
		}

		private void btnExitWindows_Click(object sender, System.EventArgs e)
		{
			LoggerUserActions.MouseClick("btnExitWindows", Name);

			if (LoggedDialog.ShowQuestion(this, "Are you sure you want to SHUTDOWN Windows?") == DialogResult.Yes)
				ProcessComm.ShutdownWindows();
		}

		private void onProcessIconicChanged(Process process, bool isIconic)
		{
			Logger.LogDebug("onProcessIconicChanged(process={0}, isIconic={1})", process, isIconic);

			if (isIconic)
				show();
			else if (process != null && !process.HasExited)
				hide();
		}

        private void onProcessExited(object sender, EventArgs e)
        {
			Logger.LogDebug("onProcessExited(sender={0}, e={1})", sender, e);
			show();
		}


		private void killProcesses()
		{
			//Kill Rimed.TCD.GUI application
			var name = Files.GetFileNameWithoutExtention(Constants.RIMED_TCD_GUI_EXECUTABLE);
			var processes = Process.GetProcessesByName(name);
			foreach (var process in processes)
			{
				killProcess(process);
			}

			// Kill eEchoWave processes
			name = Files.GetFileNameWithoutExtention(PROCESS_NAME_ECHOWAVE);
			processes = Process.GetProcessesByName(name);
			foreach (var process in processes)
			{
				killProcess(process);
			}
		}

		private void closeProcesses()
		{
			//Close Rimed.TCD.GUI application
			var name = Files.GetFileNameWithoutExtention(Constants.RIMED_TCD_GUI_EXECUTABLE);
			var processes = Process.GetProcessesByName(name);
			foreach (var process in processes)
			{
				closeProcessMainWindow(process);
			}

			// Kill eEchoWave processes
			name = Files.GetFileNameWithoutExtention(PROCESS_NAME_ECHOWAVE);
			processes = Process.GetProcessesByName(name);
			foreach (var process in processes)
			{
				closeProcessMainWindow(process);
			}
		}

		private void waitForProcessesToTerminate(int timeout = 1000)
		{
			//Close Rimed.TCD.GUI application
			var name = Files.GetFileNameWithoutExtention(Constants.RIMED_TCD_GUI_EXECUTABLE);
			var processes = Process.GetProcessesByName(name);
			foreach (var process in processes)
			{
				if (process != null && !process.HasExited)
					process.WaitForExit(timeout);
			}

			// Kill eEchoWave processes
			name = Files.GetFileNameWithoutExtention(PROCESS_NAME_ECHOWAVE);
			processes = Process.GetProcessesByName(name);
			foreach (var process in processes)
			{
				if (process != null && !process.HasExited)
					process.WaitForExit(timeout);
			}
		}

		private void exitProcesses()
		{
			m_dlProcess.TryDispose();
			m_dlProcess = null;

			m_diProcess.TryDispose();
			m_diProcess = null;

			try
			{
				closeProcesses();

				waitForProcessesToTerminate();

				killProcesses();
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);
			}
		}

		private void closeProcessMainWindow(Process process)
		{
			if (process == null || process.HasExited)
				return;

			try
			{
				process.CloseMainWindow();
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);
			}
		}

		private void killProcess(Process process)
		{
			if (process == null)
				return;

			try
			{
				if (!process.HasExited)
					process.Kill();

				process.Dispose();
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);
			}
		}

		private void restoreMainMenu()
        {
            if (InvokeRequired)
            {
                Invoke((Action)restoreMainMenu);
                return;
            }

			User32.WindowsTaskBarHide();
			Focus();

			btnTcd.Enabled				= true;
            btnCarotid.Enabled			= true;
            btnExitWindows.Enabled		= true;
	        btnExitApplication.Enabled	= true;

            btnTcd.BackColor			= Color.LightSlateGray;
            btnCarotid.BackColor		= Color.LightSlateGray;
        }

        private void mainForm_VisibleChanged(object sender, System.EventArgs e)
        {
			if (Visible)
				show();
			else
				hide();
		}

        //Disable Alt+F4. Added by Natalie on 08/21/2009
        private void mainForm_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
	        if (s_isCloseApplication)
		        return;

            if (!s_shuttingDown)
				e.Cancel = true;
        }

        protected override void WndProc(ref Message m)
        {
	        try
	        {
				if (m.Msg == ProcessComm.WinMsgMainMenuApplicationRestore)
				{
					Logger.LogInfo("RESTORE message received.");
					show();

					return;
				}

				if (m.Msg == ProcessComm.WinMsgMainMenuApplicationExit)
				{
					Logger.LogInfo("EXIT message received.");
					hide();
					s_isCloseApplication = true;
					Close();

					return;
				}
				
				s_shuttingDown = (m.Msg == WM_QUERYENDSESSION);
				base.WndProc(ref m);
			}
	        catch (Exception ex)
	        {
				Logger.LogError(ex);    
	        }
        }

		private void btnExitApplication_MouseClick(object sender, MouseEventArgs e)
		{
			LoggerUserActions.MouseClick("btnExitApplication", Name);
			if (LoggedDialog.ShowQuestion(this, "Are you sure you want to quit Rimed Main menu?") == DialogResult.Yes)
			{
				s_isCloseApplication = true;
				Close();
			}
		}

		private void mainForm_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Alt && e.Control && e.KeyValue == 32 && !e.Handled && AppSettings.Active.EnableApplicationClose)
			{
				btnExitApplication.Visible = !btnExitApplication.Visible;

				e.Handled = true;
			}
		}

		private void mainForm_FormClosed(object sender, FormClosedEventArgs e)
		{
			Logger.LogInfo("Closing");
			exitProcesses();
		}

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Added by Alex 13/04/2015 Change request #645 v 2.2.3.1
            if (originalConnection != "")
                if (connectionInfo.DHCP)
                    NetworkManagement.NetshCommandSwitchToDHCP(originalConnection);
                else
                    NetworkManagement.NetshCommandSwitchToStatic(connectionInfo.IP, connectionInfo.Mask, originalConnection);
        }
    }
}
