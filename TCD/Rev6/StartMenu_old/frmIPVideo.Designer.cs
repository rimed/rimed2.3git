﻿namespace Rimed.TCD.StartMenu
{
    partial class frmIPVideo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmIPVideo));
            this.axwmplayer = new AxWMPLib.AxWindowsMediaPlayer();
            ((System.ComponentModel.ISupportInitialize)(this.axwmplayer)).BeginInit();
            this.SuspendLayout();
            // 
            // axwmplayer
            // 
            this.axwmplayer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axwmplayer.Enabled = true;
            this.axwmplayer.Location = new System.Drawing.Point(0, 0);
            this.axwmplayer.Name = "axwmplayer";
            this.axwmplayer.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axwmplayer.OcxState")));
            this.axwmplayer.Size = new System.Drawing.Size(1014, 739);
            this.axwmplayer.TabIndex = 0;
            this.axwmplayer.MediaError += new AxWMPLib._WMPOCXEvents_MediaErrorEventHandler(this.axwmplayer_MediaError);
            // 
            // frmIPVideo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1014, 739);
            this.Controls.Add(this.axwmplayer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmIPVideo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DigiLite™ IP application was not purchased";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.axwmplayer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private AxWMPLib.AxWindowsMediaPlayer axwmplayer;

    }
}