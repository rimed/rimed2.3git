﻿using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Threading;
using Rimed.Framework.Common;
using Rimed.Framework.Threads;

namespace Rimed.TCD.DSPData
{
    public class CRawDataStorage : Disposable
    {
        private const string CLASS_NAME = "CRawDataStorage";

        private static long s_counter = 0;

        private readonly Queue<DspToPcContainer> m_writeQueue = new Queue<DspToPcContainer>();

        private string m_dirName;
        private int m_fileCounter;
        private BackgroundWorker m_backgroundIo;
        private EventWaitHandle m_backgroundIoCompleted;
		private readonly object	m_whIoCompletedLock = new object();


        #region Constructors
        public CRawDataStorage() 
        {
            Id = Interlocked.Increment(ref s_counter);
                
            m_dirName = null;
            m_fileCounter = 0;

			m_backgroundIo                              = new BackgroundWorker();
            m_backgroundIo.WorkerSupportsCancellation   = true;
            m_backgroundIo.DoWork                       += rawDataStorageBackgroundWork;
            m_backgroundIo.RunWorkerCompleted           += onRunWorkerCompleted;

            m_backgroundIoCompleted                     = new EventWaitHandle(true, EventResetMode.ManualReset);
            ioCompleatWaitHandleSet();
        }
        #endregion Constructors

        public static DspToPcContainer RawDataDirectRead(string readFileName)
        {
            if (File.Exists(readFileName))
            {
                var dsp2Pc              = new DspToPcContainer();
                dsp2Pc.RawDataFileName  = readFileName;
                dsp2Pc.LoadRawDataFromFile();

                return dsp2Pc;
            }

            return null;
        }

        internal enum ECacheEntryStatus
        {
            CACHE_ENTRY_PENDING,
            CACHE_ENTRY_READY,
            CACHE_ENTRY_READ_FAIL
        }

        internal void RawDataRecordStart(string dirName)
        {
            m_dirName = dirName;
            m_fileCounter = 0;
        }

        internal void RawDataStorageCancelIo()
        {
			if (m_backgroundIo != null) 
				m_backgroundIo.CancelAsync();

            lock (m_writeQueue)
                m_writeQueue.Clear();
        }

        internal void RawDataRecordFinish()
        {
            m_dirName = null;
            m_fileCounter = 0;
        }
			
        internal void RawDataWrite(DspToPcContainer dspToPcContainer)
        {
            if (m_dirName == null)
                return;

            lock (m_writeQueue)
                m_writeQueue.Enqueue(dspToPcContainer);

            runWorkerAsync();
        }

        internal ECacheEntryStatus RawDataRead(string readFileName, out DspToPcContainer outDspToPcContainer)
        {
			outDspToPcContainer = new DspToPcContainer();
            outDspToPcContainer.RawDataFileName   = readFileName;
            outDspToPcContainer.LoadRawDataFromFile();

            if (outDspToPcContainer.Count == 0)
                return ECacheEntryStatus.CACHE_ENTRY_READ_FAIL;

            return ECacheEntryStatus.CACHE_ENTRY_READY;
        }

        internal string RawDataStorageCreateFileNameForWrite(ref int fileNum)
        {
            if (m_dirName == null)
                return null;

            fileNum = m_fileCounter;
            m_fileCounter++;

	        var fileName = m_dirName + @"\" + fileNum + RawDataFile.DATA_FILES_EXTENTION;

            return fileName;
        }

        internal void ResetWaitHandels()
        {
            ioCompleatWaitHandleReset();
        }

        internal bool WaitIoCompleate(int waitTimeoutMSec)
        {
			var isSignaled = m_backgroundIoCompleted.WaitOne(waitTimeoutMSec, false);
			if (!isSignaled)
				Logger.LogWarning("{0}.WaitIoCompleate(waitTimeoutMSec={1}) TIMEOUT.", CLASS_NAME, waitTimeoutMSec);
                
            return isSignaled;
        }

        internal long Id { get; private set; }


        /// <summary>Reset (unsignal) io-compleat wait handle blocking calling threads</summary>
        private void ioCompleatWaitHandleReset()
        {
	        lock (m_whIoCompletedLock)
	        {
		        if (m_backgroundIoCompleted == null)
			        return;

		        m_backgroundIoCompleted.Reset();
	        }
        }

        /// <summary>Set (signal) io-compleat wait handle release waiting threads</summary>
        private void ioCompleatWaitHandleSet()
        {
	        lock (m_whIoCompletedLock)
	        {
		        if (m_backgroundIoCompleted == null)
			        return;

		        m_backgroundIoCompleted.Set();
	        }
        }

        private void runWorkerAsync()
        {
            if (m_backgroundIo == null || m_backgroundIo.IsBusy)
                return;

            m_backgroundIo.RunWorkerAsync();
        }


        //TODO, Ofer: Replace with QThread (Add PostWaitObject() - post pbject and wait (with timeout) for it object process end) 
        private void rawDataStorageBackgroundWork(object sender, DoWorkEventArgs e)
        {
            var worker = sender as BackgroundWorker;
            if (worker == null)
            {
                // Signal wait handle - To avoid deadlock 
                ioCompleatWaitHandleSet();

                Logger.LogError("StorageBackgroundWork invoked by non-BackgroundWorker thread. Action aborted");

                return;
            }

            Thread.CurrentThread.SetName("RawDataWriter");

            // Reset io completed wait handle
            ioCompleatWaitHandleReset();

            while (m_writeQueue.Count > 0)
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }

                DspToPcContainer dspToPcContainer;
                lock (m_writeQueue)
                    dspToPcContainer = m_writeQueue.Dequeue();

                if (dspToPcContainer != null)
                    dspToPcContainer.WriteRawDataToStream(worker);
            }

            ioCompleatWaitHandleSet();
        }

        // This event handler deals with the results of the background operation.
        private void onRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Logger.LogTrace(Logger.ETraceLevel.L5, CLASS_NAME, "onRunWorkerCompleted(Canceled={0}).", e.Cancelled);
            ioCompleatWaitHandleSet();
        }


        #region Override base methods
        protected override void DisposeManagedResources()
        {
			if (m_backgroundIo != null)
			{
				m_backgroundIo.CancelAsync();
				m_backgroundIo.TryDispose();
				m_backgroundIo = null;
			}

	        lock (m_whIoCompletedLock)
	        {
		        if (m_backgroundIoCompleted != null)
		        {
			        ioCompleatWaitHandleSet();

			        m_backgroundIoCompleted.TryDispose();
			        m_backgroundIoCompleted = null;
		        }
	        }
        }
        #endregion
    }
}