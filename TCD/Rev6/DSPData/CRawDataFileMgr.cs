﻿using System.Collections.Generic;
using System.Threading;
using Rimed.TCD.ManagedInfrastructure;

namespace Rimed.TCD.DSPData
{
    public abstract class CRawDataFileMgr	//	: Disposable 
    {
        private const string CLASS_NAME = "CRawDataFileMgr";

        private static long s_counter = 0;

        protected CRawDataFileMgr() 
        {
            Id = Interlocked.Increment(ref s_counter);

            RawDataStorage  = new CRawDataStorage(); 
            ListOfDsp2Pc    = new RmdListSafe<DspToPcContainer>();
            ListOfDsp2PcScroll = new RmdListSafe<DspToPcContainer>();
            ReadIndex       = 0;
            LastReadIndex   = int.MaxValue;
            AllDataSize = 0;
        }


        protected abstract void DoRawDataRecordFinish();
        public abstract void RawDataWrite(DspToPcContainer dspToPcContainer);
		public abstract DspToPcContainer RawDataAlloc(int capacity);
		public abstract void RawDataReadStart(string dirName, int readIndex);

        public int AllDataSize { get; private set; }

        public void RawDataRecordFinish()
        {
			DoRawDataRecordFinish();
        }

        public void RawDataRecordStart(string dirName)
        {
            DirName = dirName;
            RawDataStorage.RawDataRecordStart(dirName);
        }
			
        public void RawDataRecordCancel()
        {
	        if (RawDataStorage == null)
		        return;

            RawDataStorage.RawDataStorageCancelIo();
        }

        public DspToPcContainer RawDataReadJumpToFile(int readIndex)
        {
			if (RawDataStorage == null)
				return null;

			var isLast = false;
            ListOfDsp2Pc.Clear();
            RawDataStorage.RawDataStorageCancelIo();
            ReadIndex = readIndex;
            rawDataReadForward(readIndex);

            return RawDataReadNext(ref isLast);
        }

        public void WaitIoCompleate(int waitTimeoutMSec)
        {
			if (RawDataStorage == null)
				return;

			RawDataStorage.WaitIoCompleate(waitTimeoutMSec);
        }

        public void ResetWaitHandels()
        {
			if (RawDataStorage == null)
				return;

			RawDataStorage.ResetWaitHandels();
        }


        public int PackSise { get; private set; }

        private void rawDataReadForward(int readIndex)
        {

			if (RawDataStorage == null)
				return;

            var firstIndex = ListOfDsp2Pc.Count;

            for (var i = firstIndex; i < ListOfDsp2Pc.Capacity; i++)
                ListOfDsp2Pc.Add(null);

            for (var i = 0; i < ListOfDsp2Pc.Capacity; i++) 
            {
                if (ListOfDsp2Pc[i] == null) 
                {
                    var absoluteIndex = readIndex + i;
	                var fileName = DirName + @"\" + absoluteIndex + RawDataFile.DATA_FILES_EXTENTION;
                        
                    DspToPcContainer dspToPcContainer;
                    var cacheEntryStatus = RawDataStorage.RawDataRead(fileName, out dspToPcContainer);
                    if (cacheEntryStatus == CRawDataStorage.ECacheEntryStatus.CACHE_ENTRY_READ_FAIL) 
                    {
                        LastReadIndex = absoluteIndex;
                        break;
                    }
					
                    ListOfDsp2Pc[i] = dspToPcContainer;
                }
            }
        }

        protected void RawDataReadStart(string dirName, int readIndex, int numOfBlocksForward)
        {
            ReadIndex = readIndex;
            DirName = dirName;
            ListOfDsp2Pc = new RmdListSafe<DspToPcContainer>();
            ListOfDsp2Pc.Capacity = numOfBlocksForward;
            
            rawDataReadForward(readIndex);

            AllDataSize = 0;
            foreach (var item in ListOfDsp2Pc)
            {
                if (item != null) // Added by Alex 08/11/2015 Bug #807 v 2.2.3.7
                    AllDataSize += item.Count;
            }
            PackSise = ListOfDsp2Pc[0].Count;
        }

        public void RawDataReadRestart()
        {
			if (RawDataStorage == null)
				return;

			RawDataStorage.RawDataStorageCancelIo();
            ListOfDsp2Pc.Clear();
            ReadIndex = 0;
            rawDataReadForward(0);
        }

        public void RawDataReadStop()
        {
			if (RawDataStorage == null)
				return;

			RawDataStorage.RawDataStorageCancelIo();
            ListOfDsp2Pc.Clear();
            ListOfDsp2Pc.Capacity = 0;
            DirName = null;
            ReadIndex = int.MaxValue;
            LastReadIndex = int.MaxValue;
        }


        public virtual DspToPcContainer RawDataReadNext(ref bool isLast)
        {
            if (ReadIndex >= LastReadIndex)
            {
                isLast = true;
                return null;
            }

            var dsp2Pc = ListOfDsp2Pc[0];
            if (dsp2Pc != null)
            {
                ListOfDsp2Pc.RemoveAt(0);
                ReadIndex++;
            }
				
            if (ReadIndex >= LastReadIndex)
                isLast = true;
            else 
                rawDataReadForward(ReadIndex);

            return dsp2Pc;
        }

        public long Id			{ get; private set; }
		public string DirName	{ get; protected set; }

		//protected override void DisposeManagedResources()
		//{
		//    RawDataStorage.TryDispose();
		//    RawDataStorage = null;
		//    base.DisposeManagedResources();
		//}

        protected readonly CRawDataStorage RawDataStorage;
        protected int ReadIndex;
        protected int LastReadIndex;
        protected RmdListSafe<DspToPcContainer> ListOfDsp2Pc;
        protected RmdListSafe<DspToPcContainer> ListOfDsp2PcScroll;
    }
}