﻿using System;

namespace Rimed.TCD.DSPData
{
    public interface IRawDataFileFrameInfo : IRawDataFileFrameInfoReadOnly
    {
        new int BufferIndex { get; set; }
		//new int MSec { get; set; }
		//new DateTime? Time { get; set; }
    }
}