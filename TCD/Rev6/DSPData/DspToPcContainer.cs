using System;
using System.ComponentModel;
using System.IO;
using System.Threading;
using Rimed.Framework.Common;
using Rimed.TCD.ManagedInfrastructure;

namespace Rimed.TCD.DSPData
{
    public class DspToPcContainer : RmdListSafe<RDspToPc>
    {
        private static long s_counter = 0;

        #region Constructors
	    public DspToPcContainer(int capacity = 0)
		{
            CraeteTime          = DateTime.Now;
            Capacity            = capacity;
            RawDataFileName     = null;
            RawDataFileNum      = -1;

            Id = Interlocked.Increment(ref s_counter);
        }
        #endregion Constructors

        private const string CLASS_NAME = "DspToPcContainer";

        #region Properties

        public string   RawDataFileName
        {
            get { return m_rawDataFileName; }
            set
            {
                var tmp = Files.GetFileNameWithoutExtention(value);
                int fileNum;
                if (!int.TryParse(tmp, out fileNum) || fileNum < 0)
                {
                    m_rawDataFileName   = null;
                    RawDataFileNum      = -1;
                }
                else
                {
                    m_rawDataFileName   = value;
                    RawDataFileNum      = fileNum;
                }
            }
        }

        public int      RawDataFileNum  { get; private set; }

		public long		Id { get; set; }

        private DateTime CraeteTime      { get; set; }
        #endregion Properties

        #region Public Methods
		new public bool Add(RDspToPc dspToPc)
		{
			CurrentPlayData = null;

			if (dspToPc!= null && dspToPc.IsValid)
			{
				AddSafe(dspToPc);
				CurrentPlayData = dspToPc;
				return true;
			}

			return false;
		}


        public RDspToPc CurrentPlayData { get; set; }

        public void LoadRawDataFromFile()
        {
            if (!ExaminationRawData.IsCreated)
                return;

            var fileData = ExaminationRawData.Instance.GetFileData(RawDataFileNum);
            if (fileData == null)
                return;

            AddRange(fileData);
        }

        public void WriteRawDataBlockToStream(int index)
        {
            if (RawDataFileName == null || index < 0 || index >= Capacity) 
                return;

            try 
            {
                using (var writeFs = new FileStream(RawDataFileName, FileMode.Append))
                {
                    var item = base[index];
                    item.RawDataToStream(writeFs);
                    writeFs.Flush();
                    writeFs.Close();
                }
            }
            catch (Exception ex) 
            {
                Logger.LogError(ex);
            }
        }

        public void WriteRawDataToStream(BackgroundWorker worker)
        {
            if (RawDataFileName == null || File.Exists(RawDataFileName)) 
                return;
            try 
            {
                using (var writeFs = new FileStream(RawDataFileName, FileMode.CreateNew))
                {
                    for (var i = 0; i < Count; i++)
                    {
                        if (worker.CancellationPending)
                            break;

                        var item = base[i];
                        item.RawDataToStream(writeFs);
                    }

                    writeFs.Flush();
                    writeFs.Close();
                }
            }
            catch (Exception ex) 
            {
                Logger.LogError(ex);
            }
        }

        //private void compressData()
        //{
        //    var size = 0;
        //    var dataArr = ToArray();

        //    foreach (var item in dataArr)
        //    {
        //        size += item.RawMem.Length;
        //    }

        //    var data = new byte[size]; // We would expect a good compression ratio from an empty array!
        //    var pos = 0;
        //    foreach (var item in dataArr)
        //    {
        //        var len = item.RawMem.Length;
        //        Buffer.BlockCopy(item.RawMem, 0, data, pos, len);
        //        pos += len;
        //    }

        //    var ms = new MemoryStream();
        //    using (Stream ds = new DeflateStream(ms, CompressionMode.Compress))
        //    {
        //        ds.Write(data, 0, data.Length);
        //    }

        //    byte[] compressed = ms.ToArray();
        //    //Console.WriteLine(compressed.Length); // 113

        //    //// Decompress back to the data array:
        //    //ms = new MemoryStream(compressed);
        //    //using (Stream ds = new DeflateStream(ms, CompressionMode.Decompress))
        //    //    for (int i = 0; i < 1000; i += ds.Read(data, i, 1000 - i)) ;            
        //}

		public int CalcAbsoluteIndex(int indexInFile)
        {
			if (indexInFile < 0 || RawDataFileNum < 0)
                return 0;

			return RawDataFile.FILE_MAX_DSP_MESSAGES * RawDataFileNum + indexInFile;
        }

		public override string ToString()
        {
            return string.Format("Id={0}, Count={1}, Capacity={2}, RawDataFileName='{3}', RawDataFileNum={4}", Id, Count, Capacity, RawDataFileName, RawDataFileNum);
        }
        #endregion Public Methods

        #region Private Fields

		private string m_rawDataFileName;

        #endregion Private Fields
    }
}