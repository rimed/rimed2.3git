﻿using System;

namespace Rimed.TCD.DSPData
{
    public class RawDataFileFrameInfo : IRawDataFileFrameInfo
    {
        public const int INVALID = -1;

        public RawDataFileFrameInfo(int framesCount, DateTime? firstFrameTime, int frameIndex, int offset = 0)
        {
            Offset					= offset;

            m_examFirstFrameIndex   = Offset;
            m_examFirstFrameTime    = firstFrameTime.HasValue ? firstFrameTime.Value : DateTime.Now;
            m_examLastFrameIndex    = Offset + framesCount - 1;

            BufferIndex				= frameIndex;
        }

        private readonly int		m_examFirstFrameIndex;
        private readonly DateTime	m_examFirstFrameTime;
        private readonly int		m_examLastFrameIndex;
        private int					m_examFrameIndex = 0;

	    protected int Offset { get; private set; }

        public int BufferIndex
        {
            get { return m_examFrameIndex; }
            set
            {
	            if (value >= m_examFirstFrameIndex && value <= m_examLastFrameIndex)
		            m_examFrameIndex = value;
            }
        }

		public int MSec
        {
            get
            {
	            return indexToMSec(m_examFrameIndex);
            }
        }

        public DateTime? Time
        {
            get
            {
				return m_examFirstFrameTime.AddMilliseconds(MSec);
            }
        }

        public override string ToString()
        {
			return string.Format("BufferIndex={0}, MSec={1}, Time={2}, Offset={3}, Range: {4}...{5}", BufferIndex, MSec, Time, Offset, m_examFirstFrameIndex, m_examLastFrameIndex);
        }

	    private static int indexToMSec(int frameIndex)
        {
            if (frameIndex < 0)
                return INVALID;

			return (int)Math.Round(frameIndex * RawDataFile.DSP_MESSAGE_DURATION_MSEC);
		}
    }
}