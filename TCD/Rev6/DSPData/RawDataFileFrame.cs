﻿using System;
using System.Collections.Generic;

namespace Rimed.TCD.DSPData
{
    public class RawDataFileFrame : RawDataFileFrameInfo, IRawDataFileFrame
    {
        public RawDataFileFrame(List<RDspToPc> frames, DateTime? firstFrameTime, int number, int offset = 0)
            : base(frames.Count, firstFrameTime, number, offset)
        {
            m_frames = frames;
        }

        public RawDataFileFrame(DateTime? firstFrameTime, int number, int offset = 0)
            : this(new List<RDspToPc>(), firstFrameTime, number, offset)
        {
        }

        private readonly List<RDspToPc> m_frames;
        public RDspToPc Data
        {
            get
            {
                if (BufferIndex == INVALID || m_frames == null || m_frames.Count == 0)
                    return null;

                var index = BufferIndex - Offset;
                if (index < 0 || index >= m_frames.Count)
                    return null;

                return m_frames[index];
            }
        }
    }
}