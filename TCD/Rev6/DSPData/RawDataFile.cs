﻿using System;
using System.Collections.Generic;
using System.IO;
using Rimed.Framework.Common;
using Rimed.TCD.DspBlock;

namespace Rimed.TCD.DSPData
{
    public class RawDataFile: IRawDataFileInfo
    {
        private const string CLASS_NAME							= "RawDataFile";

        public const string			DATA_FILES_EXTENTION		= ".DAT";
		public const int			FILE_MAX_DSP_MESSAGES		= 500;
		public const double			DSP_MESSAGE_DURATION_MSEC	= DspBlockConst.COLUMN_DURATION * DspBlockConst.NUM_OF_COLUMNS;
		public const double			DSP_MESSAGE_DURATION_SEC	= DSP_MESSAGE_DURATION_MSEC / 1000.0;
		public const double			FILE_MAX_DURATION_MSEC		= FILE_MAX_DSP_MESSAGES * DSP_MESSAGE_DURATION_MSEC;

		public static readonly int	FILE_MAX_DURATION_SEC		= (int)Math.Round(FILE_MAX_DURATION_MSEC / 1000.0);
		public static readonly int	DSP_MESSAGES_IN_SECOND		= (int)Math.Round(1000.0 / DSP_MESSAGE_DURATION_MSEC);


		//public static int ExtractDataFileNumber(string filePath)
		//{
		//    if (string.IsNullOrWhiteSpace(filePath))
		//        return RawDataFileFrameInfo.INVALID;

		//    if (!File.Exists(filePath))
		//        return RawDataFileFrameInfo.INVALID;

		//    // Check file extension is DATA_FILES_EXTENTION
		//    var s = Files.GetFileExtention(filePath);
		//    if (string.Compare(DATA_FILES_EXTENTION, s, StringComparison.InvariantCultureIgnoreCase) != 0)
		//        return RawDataFileFrameInfo.INVALID;

		//    s = Files.GetFileNameWithoutExtention(filePath);
		//    int fileNum;
		//    if (!int.TryParse(s, out fileNum) || fileNum < 0)
		//        return RawDataFileFrameInfo.INVALID;

		//    return fileNum;
		//}

        private readonly object                     m_locker = new object();

        internal List<RDspToPc> Frames
        {
	        get { return m_frames; }
	        private set
	        {
		        var tmp = m_frames;
		        m_frames = value;

				if (tmp != null)
					tmp.Clear();
	        }
        }

	    #region C'tor
        private RawDataFile()
        {
            Frames          = new List<RDspToPc>();
            FileName        = string.Empty;
            FileNum         = -1;
            BufferOffset    = -1;
            MSecOffset      = -1;
        }

        public RawDataFile(string filePath, DateTime startTime)
            : this()
        {
            m_startTime = startTime;
            validateFile(filePath);
        }
        #endregion

        public void Load()
        {
			if (!IsValid)
            {
                FirstBuffer      = new RawDataFileFrame(Frames, m_startTime, BufferOffset, BufferOffset);
                LastBuffer       = new RawDataFileFrame(Frames, m_startTime, BufferOffset + FrameCount - 1, BufferOffset);
                CurrentBuffer    = new RawDataFileFrame(Frames, m_startTime, -1, BufferOffset);
				
				return;
            }

			Logger.LogDebug("{0}.Load(): FileIndex={1}", CLASS_NAME, FileNum);
			
			lock (m_locker)
            {
                if (IsLoaded)
					return;

				//NOTE File load and buffer properties assignment MUST be an atomic operation - no thread context-switch
                loadFile();
				FirstBuffer		= new RawDataFileFrame(Frames, m_startTime, BufferOffset, BufferOffset);
				LastBuffer		= new RawDataFileFrame(Frames, m_startTime, BufferOffset + FrameCount - 1, BufferOffset);
				CurrentBuffer	= new RawDataFileFrame(Frames, m_startTime, -1, BufferOffset);
			}

        }

        public void Unload()
        {
			if (!IsLoaded)
				return;

			Logger.LogDebug("{0}.Unload(): Index: {1}, {2} frames unloaded. File: '{3}'", CLASS_NAME, FileNum, Frames.Count, FileName);
			Frames = new List<RDspToPc>();	
        }

        public int							InternalBufferIndex
        {
            get
            {
                if (CurrentBuffer.BufferIndex == RawDataFileFrameInfo.INVALID || BufferOffset > CurrentBuffer.BufferIndex)
                    return RawDataFileFrameInfo.INVALID;

                return CurrentBuffer.BufferIndex - BufferOffset;
            }
        }
        public int							BufferOffset { get; private set; }
        public int							MSecOffset { get; private set; }
        public IRawDataFileFrameReadOnly    FirstBuffer { get; private set; }
        public IRawDataFileFrameReadOnly    LastBuffer { get; private set; }
        public IRawDataFileFrame            CurrentBuffer { get; private set; }
		public bool							IsLoaded { get { return FrameCount > 0; } }
        public string						FileName { get; private set; }
        public int							FileNum { get; private set; }
		public int							FrameCount { get { return (Frames == null) ? 0 : Frames.Count; } }

		//public void JumpForward(int steps = DSP_MESSAGES_IN_SECOND)
		//{
		//    if (IsLoaded)
		//        Load();

		//    var newNumber               = CurrentBuffer.BufferIndex + steps;
		//    CurrentBuffer.BufferIndex    = (newNumber > LastBuffer.BufferIndex) ? LastBuffer.BufferIndex : newNumber;
		//}

		//public void JumpBackward(int steps = DSP_MESSAGES_IN_SECOND)
		//{
		//    if (IsLoaded)
		//        Load();

		//    var newNumber               = CurrentBuffer.BufferIndex - steps;
		//    CurrentBuffer.BufferIndex    = (newNumber < FirstBuffer.BufferIndex) ? FirstBuffer.BufferIndex : newNumber;
		//}

		public bool IsValid { get; private set; }

        public RDspToPc this[int index]
        {
            get
            {
                if (IsLoaded)
                    Load();

                if (index < 0 || index >= FrameCount)
                    return null;

                return Frames[index];
            }
        }

        private readonly DateTime m_startTime;
	    private List<RDspToPc> m_frames;

	    private bool validateFile(string filePath)
        {
            if (string.IsNullOrWhiteSpace(filePath))
            {
                Logger.LogError("Raw data file name can NOT be null or empty.");
                return false;
            }

            if (!File.Exists(filePath) || !Files.IsPathValid(filePath))
            {
                Logger.LogError(string.Format("Raw data file '{0}' not found or name is invalid.", filePath));
                return false;
            }

            // Check file extension is DATA_FILES_EXTENTION
            var s = Files.GetFileExtention(filePath);
            if (string.Compare(DATA_FILES_EXTENTION, s, StringComparison.InvariantCultureIgnoreCase) != 0)
            {
                Logger.LogError(string.Format("Raw data file name extension '{0}' is not valid. Expected '{1}' extension.", filePath, DATA_FILES_EXTENTION));
                return false;
            }

            s = Files.GetFileNameWithoutExtention(filePath);
            int fileNum ;
            if (!int.TryParse(s, out fileNum) || fileNum < 0)
            {
                Logger.LogError(string.Format("Raw data file name format '{0}' is not valid. Expected zero or positive numeric value.", filePath));
                return false;
            }

            FileName        = filePath;
            FileNum         = fileNum;
            BufferOffset    = fileNum * FILE_MAX_DSP_MESSAGES;
            MSecOffset      = (int) (BufferOffset * DSP_MESSAGE_DURATION_MSEC);

            IsValid         = true;

            return IsValid;
        }

        private void loadFile()
        {
			if (IsLoaded || !IsValid)
                return;

            var tmp = new List<RDspToPc>();
            try
            {
                using (var readFs = new FileStream(FileName, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    var framesCount = (int)(readFs.Length / DspBlockConst.SIZE_OF_R_DSP2PC);

                    for (var i = 0; i < framesCount; i++)
                    {
                        var buffer = new Byte[DspBlockConst.SIZE_OF_R_DSP2PC];
                        readFs.Read(buffer, 0, DspBlockConst.SIZE_OF_R_DSP2PC);
                        var rDsp2Pc = new RDspToPc(buffer);	//isCheckNumerator = false: Play ALL saved data
						if (rDsp2Pc.IsValid)
							tmp.Add(rDsp2Pc);
                    }

                    readFs.Close();
                }

                Frames = tmp;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                Unload();
            }
        }
    }
}