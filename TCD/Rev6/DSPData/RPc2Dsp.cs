﻿using System;
using System.Security.AccessControl;
using Rimed.TCD.DspBlock;
using Rimed.TCD.ManagedInfrastructure;

namespace Rimed.TCD.DSPData
{
    public class RPC2DSPMemOffset
    {
		public readonly static RPC2DSPMemOffset MemoryMap = new RPC2DSPMemOffset();
		
		public RPC2DSPMemOffset() 
        {
            int offset = 0;
            int i;
            offset += sizeof(UInt32);
            CardInfoMemOffset = new RCardInfoMemOffset(offset);
            offset += DspBlockConst.SIZE_OF_R_CARD_INFO;
            m_arrProbeInfoMemOffset = new RProbeInfoMemOffset[DspBlockConst.NUM_OF_PROBES];
            for (i = 0; i < DspBlockConst.NUM_OF_PROBES; i++) 
            {
                m_arrProbeInfoMemOffset[i] = new RProbeInfoMemOffset(i, offset);
                offset += DspBlockConst.SIZE_OF_R_PROBE_INFO;
            }
        }
        public int NumeratorMemOffset {get {return 0;}}

        public RCardInfoMemOffset CardInfoMemOffset { get; private set; }
        public RProbeInfoMemOffset ProbeInfoMemOffset(int index) { return m_arrProbeInfoMemOffset[index]; }
        private readonly RProbeInfoMemOffset[] m_arrProbeInfoMemOffset;
    }

    public class RGateInfoMemOffset
    {
        public RGateInfoMemOffset(int iRawMemOffset)
        {
            int offset = iRawMemOffset;
            m_depthMemOffset = offset;
            offset += sizeof(ushort);
            m_hitEnableMemOffset = offset;
        }

        public int DepthMemOffset { get { return m_depthMemOffset; }}
        public int HitEnableMemOffset { get { return m_hitEnableMemOffset; } }
        private readonly int m_depthMemOffset;
        private readonly int m_hitEnableMemOffset;
    }

    public class RProbeInfoMemOffset
    {
        public RProbeInfoMemOffset(int probeIndex, int iRawMemOffset)
        {
            int offset = iRawMemOffset;
            int i;

            m_probeIndex = probeIndex;
            ProbeTypeMemOffset = offset;
            offset += sizeof(ushort);
            WidthMemOffset = offset;
            offset += sizeof(ushort);
            PowerMemOffset = offset;
            offset += sizeof(ushort);
            GainMemOffset = offset;
            offset += sizeof(ushort);
            RangeMemOffset = offset;
            offset += sizeof(ushort);
            ThumpMemOffset = offset;
            offset += sizeof(ushort);
            RangeDivisorMemOffset = offset;
            offset += sizeof(ushort);
            FcShiftMemOffset = offset;
            offset += sizeof(ushort);
            NumOfGatesMemOffset = offset;
            offset += sizeof(ushort);
            StartDepthMemOffset = offset;
            offset += sizeof(ushort);
            DepthStepMemOffset = offset;
            offset += sizeof(ushort);
            DirectionSwitchMemOffset = offset;
            offset += sizeof(ushort);
            ReserveMemOffset = offset;
            offset += 2;
            m_gateSaMemOffset = new RGateInfoMemOffset[DspBlockConst.NUM_OF_GATES_PER_PROBE];
            for (i = 0; i < DspBlockConst.NUM_OF_GATES_PER_PROBE; i++) 
            {
                m_gateSaMemOffset[i] = new RGateInfoMemOffset(offset);
                offset += DspBlockConst.SIZE_OF_R_GATE_INFO;
            }
        }

        public RGateInfoMemOffset Gate_saMemOffset(int index) { return m_gateSaMemOffset[index]; }
        public int ProbeTypeMemOffset { get; private set; }
        public int WidthMemOffset { get; private set; }
        public int PowerMemOffset { get; private set; }
        public int GainMemOffset { get; private set; }
        public int RangeMemOffset { get; private set; }
        public int ThumpMemOffset { get; private set; }
        public int RangeDivisorMemOffset { get; private set; }
        public int FcShiftMemOffset { get; private set; }
        public int NumOfGatesMemOffset { get; private set; }
        public int StartDepthMemOffset { get; private set; }
        public int DepthStepMemOffset { get; private set; }
        public int DirectionSwitchMemOffset { get; private set; }
        public int ReserveMemOffset { get; private set; }

        private int m_probeIndex;
        //Sergey : The first one is for Sensitivity and the second to notify if the current BV is Opthalmic or Siphon
        private readonly RGateInfoMemOffset[]	m_gateSaMemOffset; // 14 x 8 = 112
    }

    public class RCardInfoMemOffset
    {
        public RCardInfoMemOffset(int iRawMemOffset)
        {
            int Offset = iRawMemOffset;
            m_operationModeMemOffset = Offset;
            Offset += sizeof(ushort);
            m_soundVolumeMemOffset = Offset;
            Offset += sizeof(ushort);
            m_gate2HearMemOffset = Offset;
            Offset += sizeof(ushort);
            m_fftSizeMemOffset = Offset;
            Offset += sizeof(ushort);
            m_hitThresholdMemOffset = Offset;
            Offset += sizeof(ushort);
            m_speakerOrHeadphoneMemOffset = Offset;
            Offset += sizeof(ushort);
            m_mModeThreshold1MemOffset = Offset;
            Offset += sizeof(byte);
            m_mModeThreshold2MemOffset = Offset;
            Offset += sizeof(byte);
            //Added by Alex Bug #613 v.2.2.2.10 15/01/2015
            Offset += sizeof(byte) + sizeof(byte) + sizeof(byte);
            //Added by Alex Feature #755 v.2.2.3.20 18/01/2016
            m_mAutoMuteEnable = Offset;
            Offset += sizeof(byte);
            m_mAverageCPMemOffset = Offset;
        }
        public int OperationModeMemOffset	{	get {	return m_operationModeMemOffset;	}	}
        public int SoundVolumeMemOffset { get { return m_soundVolumeMemOffset; } }
        public int Gate2HearMemOffset { get { return m_gate2HearMemOffset; } }
        public int FFTSizeMemOffset { get { return m_fftSizeMemOffset; } }
        public int HitThresholdMemOffset { get { return m_hitThresholdMemOffset; } }
        public int SpeakerOrHeadphoneMemOffset { get { return m_speakerOrHeadphoneMemOffset; } }
        public int MModeThreshold1MemOffset { get { return m_mModeThreshold1MemOffset; } }
        public int MModeThreshold2MemOffset { get { return m_mModeThreshold2MemOffset; } }
        //Added by Alex Feature #755 v.2.2.3.20 18/01/2016
        public int MAutoMuteEnable { get { return m_mAutoMuteEnable; } }
        //Added by Alex Bug #613 v.2.2.2.10 15/01/2015
        public int MAverageCPMemOffset { get { return m_mAverageCPMemOffset; } }

        private readonly int m_operationModeMemOffset;
        private readonly int m_soundVolumeMemOffset;
        private readonly int m_gate2HearMemOffset;
        private readonly int m_fftSizeMemOffset;
        private readonly int m_hitThresholdMemOffset;
        private readonly int m_speakerOrHeadphoneMemOffset; // 0 - speaker, 1 - headphone
        private readonly int m_mModeThreshold1MemOffset;
        private readonly int m_mModeThreshold2MemOffset;
        //Added by Alex Feature #755 v.2.2.3.20 18/01/2016
        private readonly int m_mAutoMuteEnable;
        //Added by Alex Bug #613 v.2.2.2.10 15/01/2015
        private readonly int m_mAverageCPMemOffset;
    }

    public class Pc2DspDataFieldVal
    {
        public Pc2DspDataFieldVal(Byte[] fieldVal, int fieldOffset)
        {
            m_fieldOffset = fieldOffset;
            m_fieldVal = fieldVal;
        }

        public Byte[] FieldVal { get { return m_fieldVal; } }
        public int FieldOffset { get { return m_fieldOffset; } }

        private readonly Byte[] m_fieldVal;
        private readonly int m_fieldOffset;
    }

    public class RGateInfo
    {
        public RGateInfo(RmdListSafe<Pc2DspDataFieldVal> listNewFieldVals, RGateInfoMemOffset memOffset)
        {
            m_memOffset = memOffset;
            m_listNewFieldVals = listNewFieldVals;

            m_depth = 255;
            m_hitEnable = 0;
        }
		 
        public UInt16 Depth
        {
            get { return m_depth;}
            set
            {
                if (m_depth == value)   
                    return;

                m_depth = value;
                var newFieldVal = new Pc2DspDataFieldVal(BitConverter.GetBytes(value), m_memOffset.DepthMemOffset);
                m_listNewFieldVals.AddSafe(newFieldVal);
            }
        }

        public UInt16 HitEnable
        {
            get { return m_hitEnable; }
            set
            {
                if (m_hitEnable == value)   
                    return;

                m_hitEnable     = value;
                var newFieldVal = new Pc2DspDataFieldVal(BitConverter.GetBytes(value), m_memOffset.HitEnableMemOffset);
                m_listNewFieldVals.AddSafe(newFieldVal);
            }
        }

        private UInt16 m_depth;
        private UInt16 m_hitEnable;

        private readonly RmdListSafe<Pc2DspDataFieldVal> m_listNewFieldVals;    
        private readonly RGateInfoMemOffset m_memOffset;
    }

    public class RProbeInfo
    {
        public RProbeInfo(RProbeInfoMemOffset memOffset)
        {
            m_memOffset         = memOffset;
            m_listNewFieldVals  = new RmdListSafe<Pc2DspDataFieldVal>();
            m_gateSa            = new RGateInfo[DspBlockConst.NUM_OF_GATES_PER_PROBE];
            for (var i = 0; i < DspBlockConst.NUM_OF_GATES_PER_PROBE; i++) 
            {
                m_gateSa[i] = new RGateInfo(m_listNewFieldVals, m_memOffset.Gate_saMemOffset(i));
            }

            m_reserve = new Byte[2];
            ProbeType       = 255;
            Gain            = 255;
            Range           = 255;
            Power           = 255;
            Width           = 255;
            Thump           = 255;
			NumOfGates		= 63; //TODO: [Ofer] ??? = 64 (GlobalSettings.NUM_OF_GATES_64)
            StartDepth      = 255;
            DepthStep       = 255;
            DirectionSwitch = 255;

            //m_Sensitivity = 2;    // 00 -6-12
            //	m_Ophthalmic = 0;    // 05 6-12-07
        }

        #region Public properties
        public RmdListSafe<Pc2DspDataFieldVal> ListNewFieldVals
        {
            get { return m_listNewFieldVals; }
        }

        public ushort ProbeType 
        {
            get { return m_probeType; }
            set
            {
                if (m_probeType == value)   
                    return;

                m_probeType  = value;
	            var newFieldVal = new Pc2DspDataFieldVal(BitConverter.GetBytes(value), m_memOffset.ProbeTypeMemOffset);
                m_listNewFieldVals.AddSafe(newFieldVal);
            }
        }

        public ushort Width
        {
            get { return m_width; }
            set
            {
                if (m_width == value)      
                    return;

                m_width = value;
	            var newFieldVal = new Pc2DspDataFieldVal(BitConverter.GetBytes(value), m_memOffset.WidthMemOffset);
                m_listNewFieldVals.AddSafe(newFieldVal);
            }
        }

        public ushort Power
        {
            get { return m_power; }
            set
            {
                if (m_power == value)      
                    return;

                m_power = value;
	            var newFieldVal = new Pc2DspDataFieldVal(BitConverter.GetBytes(value), m_memOffset.PowerMemOffset);
                m_listNewFieldVals.AddSafe(newFieldVal);
            }
        }

        public ushort Gain
        {
            get { return m_gain; }
            set
            {
                if (m_gain == value)      
                    return;

                m_gain = value;
	            var newFieldVal = new Pc2DspDataFieldVal(BitConverter.GetBytes(value), m_memOffset.GainMemOffset);
                m_listNewFieldVals.AddSafe(newFieldVal);
            }
        }

        public ushort Range
        {
            get { return m_range; }
            set
            {
                if (m_range == value)      
                    return;

                m_range = value;
                var newFieldVal = new Pc2DspDataFieldVal(BitConverter.GetBytes(value), m_memOffset.RangeMemOffset);
                m_listNewFieldVals.AddSafe(newFieldVal);
			}
        }

        public ushort Thump
        {
            get { return m_thump; }
            set
            {
                if (m_thump == value)      
                    return;

                m_thump = value;
	            var newFieldVal = new Pc2DspDataFieldVal(BitConverter.GetBytes(value), m_memOffset.ThumpMemOffset);
                m_listNewFieldVals.AddSafe(newFieldVal);
            }
        }

        public ushort RangeDivisor
        {
            get { return m_rangeDivisor; }
            set
            {
                if (m_rangeDivisor == value)      
                    return;

                m_rangeDivisor = value;
	            var newFieldVal = new Pc2DspDataFieldVal(BitConverter.GetBytes(value), m_memOffset.RangeDivisorMemOffset);
                m_listNewFieldVals.AddSafe(newFieldVal);
            }
        }

        public ushort FcShift
        {
            get { return m_fcShift; }
            set
            {
                if (m_fcShift == value)      
                    return;

                m_fcShift = value;
	            var newFieldVal = new Pc2DspDataFieldVal(BitConverter.GetBytes(value), m_memOffset.FcShiftMemOffset);
                m_listNewFieldVals.AddSafe(newFieldVal);
            }
        }

        public ushort NumOfGates
        {
            get { return m_numOfGates; }
            set
            {
                if (m_numOfGates == value)      
                    return;

                m_numOfGates    = value;
	            var newFieldVal = new Pc2DspDataFieldVal(BitConverter.GetBytes(value), m_memOffset.NumOfGatesMemOffset);
                m_listNewFieldVals.AddSafe(newFieldVal);
            }
        }

        public ushort StartDepth
        {
            get { return m_startDepth; }
            set
            {
                if (m_startDepth == value)      
                    return;

                m_startDepth    = value;
	            var newFieldVal = new Pc2DspDataFieldVal(BitConverter.GetBytes(value), m_memOffset.StartDepthMemOffset);
                m_listNewFieldVals.AddSafe(newFieldVal);
            }
        }

        public ushort DepthStep
        {
            get { return m_depthStep; }
            set
            {
                if (m_depthStep == value)      
                    return;

                m_depthStep     = value;
	            var newFieldVal = new Pc2DspDataFieldVal(BitConverter.GetBytes(value), m_memOffset.DepthStepMemOffset);
                m_listNewFieldVals.AddSafe(newFieldVal);
            }
        }

        public ushort DirectionSwitch
        {
            get { return m_directionSwitch; }
            set
            {
                if (m_directionSwitch == value)      
                    return;

                m_directionSwitch   = value;
	            var newFieldVal     = new Pc2DspDataFieldVal(BitConverter.GetBytes(value), m_memOffset.DirectionSwitchMemOffset);
                m_listNewFieldVals.AddSafe(newFieldVal);
            }
        }

        public Byte GetReserve(int index)
        {
            return m_reserve[index];
        }

        public void SetReserve(int index, Byte value)
        {
            if (m_reserve[index] == value)      
                return;

            m_reserve[index]    = value;
            var newFieldVal     = new Pc2DspDataFieldVal(m_reserve, m_memOffset.ReserveMemOffset);
            m_listNewFieldVals.AddSafe(newFieldVal);
        }

        public RGateInfo GetMGate(int gateNum)
        {
            return m_gateSa[gateNum];
        }
			
        /*
			public R_GateInfo set_Gate_sa(int GateNum)
			{
				m_ = value;
					Pc2DspDataFieldVal NewFieldVal;
					NewFieldVal = new Pc2DspDataFieldVal(BitConverter.GetBytes(value),
																	 m_MemOffset.MemOffset);
					m_ListNewFieldVals.AddSafe(NewFieldVal);
			}
			*/
        #endregion Public properties

		public override string ToString()
		{
			return string.Format("ProbeType={0}, Width={1}, Power={2}, Gain={3}, Range={4}, Thump={5}, RangeDivisor={6}, FcShift={7}, NumOfGates={8}, StartDepth={9}, DepthStep={10}, DirectionSwitch={11}"
									, ProbeType, Width, Power, Gain, Range, Thump, RangeDivisor, FcShift, NumOfGates, StartDepth, DepthStep, DirectionSwitch
								);
		}

        #region Private fields
        private ushort			m_probeType;
        private ushort			m_width;
        private ushort			m_power;
        private ushort			m_gain;
        private ushort			m_range;
        private ushort			m_thump;
        private ushort			m_rangeDivisor;
        private ushort			m_fcShift;
        private ushort			m_numOfGates;	// 16, 32, 64.
        private ushort			m_startDepth;	// Real value * 4.
        private ushort			m_depthStep;	// 0, 1, 2 - selected index in the combobox.
        private ushort			m_directionSwitch; // 0 - ToProbe, 1 - FromProbe.
        //Sergey : The first one is for Sensitivity and the second to notify if the current BV is Opthalmic or Siphon
        private readonly Byte[]			m_reserve;
        private readonly RGateInfo[]		   m_gateSa; // 14 x 8 = 112

        private readonly RmdListSafe<Pc2DspDataFieldVal> m_listNewFieldVals;
        private readonly RProbeInfoMemOffset m_memOffset;
        #endregion Private fields
    }

    public class RCardInfo
    {
		public const ushort OPERATION_MODE_OFFLINE = 0;
        public const ushort OPERATION_MODE_ONLINE = 1; 
        public const ushort OPERATION_MODE_REPLAY = 2;
        public const ushort OPERATION_MODE_FPGA_DOWNLOAD = 3;
			
        public RCardInfo(RCardInfoMemOffset memOffset)
        {				
            m_memOffset = memOffset;
            m_listNewFieldVals = new RmdListSafe<Pc2DspDataFieldVal>();
            OperationMode = OPERATION_MODE_OFFLINE;
            SoundVolume = 10;
            Gate2Hear = 0;
            FFTSize = 255;
            HitThreshold = 10;
            SpeakerOrHeadphone = 0; // 0 - speaker, 1 - headphone
            MModeThreshold1 = 255;
            MModeThreshold2 = 255;
            //ZeroLinePercent = 0;
            //pcCmd.m_GenCardInfo_s.m_Probe2Hear = 0;
        }

        #region Public properties
        public RmdListSafe<Pc2DspDataFieldVal> ListNewFieldVals
        {
            get { return m_listNewFieldVals; }
        }

        public ushort OperationMode 
        {
            get {return m_operationMode; }
            set
            {
                if (m_operationMode == value)      
                    return;

                m_operationMode = value;
                var newFieldVal = new Pc2DspDataFieldVal(BitConverter.GetBytes(value), m_memOffset.OperationModeMemOffset);
                m_listNewFieldVals.AddSafe(newFieldVal);
            }
        }
        
        public ushort SoundVolume 
        {
            get {return m_soundVolume; }
            set
            {
                if (m_soundVolume == value)      
                    return;

                m_soundVolume = value;
                var newFieldVal = new Pc2DspDataFieldVal(BitConverter.GetBytes(value), m_memOffset.SoundVolumeMemOffset);
                m_listNewFieldVals.AddSafe(newFieldVal);
            }
        }
        
		public void AddDemiAliveMessage()
		{
			var newFieldVal = new Pc2DspDataFieldVal(BitConverter.GetBytes(SoundVolume), m_memOffset.SoundVolumeMemOffset);
			m_listNewFieldVals.AddSafe(newFieldVal);
		}

        public ushort Gate2Hear
        {
            get { return m_gate2Hear; }
            set
            {
                if (m_gate2Hear == value)      
                    return;

                m_gate2Hear = value;
                var newFieldVal = new Pc2DspDataFieldVal(BitConverter.GetBytes(value), m_memOffset.Gate2HearMemOffset);
                m_listNewFieldVals.AddSafe(newFieldVal);
            }
        }

        public ushort FFTSize
        {
            get { return m_fftSize; }
            set
            {
                if (m_fftSize == value)      
                    return;

                m_fftSize = value;
                var newFieldVal = new Pc2DspDataFieldVal(BitConverter.GetBytes(value), m_memOffset.FFTSizeMemOffset);
                m_listNewFieldVals.AddSafe(newFieldVal);
            }
        }

        public ushort HitThreshold
        {
            get { return m_hitThreshold; }
            set
            {
                if (m_hitThreshold == value)      
                    return;

                m_hitThreshold = value;
                var newFieldVal = new Pc2DspDataFieldVal(BitConverter.GetBytes(value), m_memOffset.HitThresholdMemOffset);
                m_listNewFieldVals.AddSafe(newFieldVal);
            }
        }

        public ushort SpeakerOrHeadphone
        {
            get { return m_speakerOrHeadphone; }
            set
            {
                if (m_speakerOrHeadphone == value)      
                    return;

                m_speakerOrHeadphone = value;
                var newFieldVal = new Pc2DspDataFieldVal(BitConverter.GetBytes(value), m_memOffset.SpeakerOrHeadphoneMemOffset);
                m_listNewFieldVals.AddSafe(newFieldVal);
            }
        }

        public byte MModeThreshold1
        {
            get { return m_mModeThreshold1; }
            set
            {
                if (m_mModeThreshold1 == value)      
                    return;

                m_mModeThreshold1 = value;
                var newFieldVal = new Pc2DspDataFieldVal(BitConverter.GetBytes(value), m_memOffset.MModeThreshold1MemOffset);
                m_listNewFieldVals.AddSafe(newFieldVal);
            }
        }

        public byte MModeThreshold2
        {
            get { return m_mModeThreshold2; }
            set
            {
                if (m_mModeThreshold2 == value)      
                    return;

                m_mModeThreshold2 = value;
                var newFieldVal = new Pc2DspDataFieldVal(BitConverter.GetBytes(value), m_memOffset.MModeThreshold2MemOffset);
                m_listNewFieldVals.AddSafe(newFieldVal);
            }
        }

        //Added by Alex Bug #613 v.2.2.2.10 15/01/2015
        public UInt16 AverageCP
        {
            get { return m_avarageCP; }
            set
            {
                m_avarageCP = value;
                var newFieldVal = new Pc2DspDataFieldVal(BitConverter.GetBytes(value), m_memOffset.MAverageCPMemOffset);
                m_listNewFieldVals.AddSafe(newFieldVal);
            }
        }

        //Added by Alex Feature #755 v.2.2.3.19 04/01/2016
        public byte AutoMuteEnable
        {
            get { return m_AutoMuteEna; }
            set
            {
                m_AutoMuteEna = value;
                //Changed by Alex Feature #755 v.2.2.3.20 18/01/2016
                var newFieldVal = new Pc2DspDataFieldVal(new byte[] { value }, m_memOffset.MAutoMuteEnable);
                m_listNewFieldVals.AddSafe(newFieldVal);
            }
        }

        #endregion Public properties

        #region Private fields
        private ushort m_operationMode;
        private ushort m_soundVolume;
        private ushort m_gate2Hear;
        private ushort m_fftSize;
        private ushort m_hitThreshold;
        private ushort m_speakerOrHeadphone; // 0 - speaker, 1 - headphone
        //Added by Alex Feature #755 v.2.2.3.19 04/01/2016
        private Byte m_AutoMuteEna = 0;
        private Byte m_mModeThreshold1;
        private Byte m_mModeThreshold2;
        //private Byte m_ZeroLinePercent;

        private readonly RmdListSafe<Pc2DspDataFieldVal> m_listNewFieldVals;
        private readonly RCardInfoMemOffset m_memOffset;
        private UInt16 m_avarageCP = 1; // 0 - Beat by Beat; 1 - Averaging  
        #endregion Private fields
    }


    public class RPc2Dsp
    {
		private readonly static RPC2DSPMemOffset s_memOffset = new RPC2DSPMemOffset();

		public static RPC2DSPMemOffset MemOffset { get { return s_memOffset; } }

		
		public RPc2Dsp()
        {
            m_numerator = 0;
            m_pRawData = new Byte[DspBlockConst.SIZE_OF_R_PC2DSP];
        }

        public Byte[] RawData { get { return m_pRawData; } }

        public UInt32 Numerator 
        {
            get { return m_numerator; }
            set
            {
                m_numerator = value;
                byte[] valueByteArr = BitConverter.GetBytes(m_numerator);
                Array.Copy(valueByteArr, 0, m_pRawData, (long)s_memOffset.NumeratorMemOffset, sizeof(UInt32));
            }
        }

        public bool MergeRawData(RmdListSafe<Pc2DspDataFieldVal> listNewFieldVals)
        {
            var bNewData = false;
            var numOfFields = listNewFieldVals.Count;
            for (var i = 0; i < numOfFields; i++ ) 
            {
                Array.Copy(listNewFieldVals[i].FieldVal, 0, m_pRawData, (long)listNewFieldVals[i].FieldOffset, listNewFieldVals[i].FieldVal.Length);
            }

            if (numOfFields > 0) 
            {
                bNewData = true;
                listNewFieldVals.RemoveRangeSafe(0, numOfFields);
            }

            return bNewData;
        }

        private readonly Byte[] m_pRawData;
		private UInt32 m_numerator;
    };
}
