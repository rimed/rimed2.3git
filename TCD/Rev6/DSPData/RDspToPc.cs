﻿using System;
using System.IO;
using System.Threading;
using Rimed.Framework.Common;
using Rimed.TCD.DspBlock;

namespace Rimed.TCD.DSPData
{
	#region Struct memory offsets
	public class RDspToPCFieldsOffset
    {
		public readonly static RDspToPCFieldsOffset OffsetMap = new RDspToPCFieldsOffset(0);

        #region Constructors
	    private RDspToPCFieldsOffset(int iRawMemOffset)
        {
            m_iRawMemOffset = iRawMemOffset;
            var offset = iRawMemOffset;

            offset += sizeof(UInt32);
            m_fftSpectrumMemOffset = new RFFTSpectrumMemOffset(offset);
            offset += DspBlockConst.SIZE_OF_FFT_SPECTRUM;
            m_fftEnvelopesMemOffset = new RFFTEnvelopesMemOffset(offset);
            offset += DspBlockConst.SIZE_OF_FFT_ENVELOPES;
            m_hitsDataMemOffset = new RHitsDataMemOffset(offset);
            offset += DspBlockConst.SIZE_OF_HITS_DATA;
            m_clinicalParametersMemOffset = new RClinicalParametersMemOffset(offset);
            offset += DspBlockConst.SIZE_OF_CLINICAL_PARAMETERS;
            m_autoScanMemOffset = new RAutoScanMemOffset(offset);
            offset += DspBlockConst.SIZE_OF_AUTO_SCAN;
            m_timeDomainMemOffset = new RTimeDomainMemOffset(offset);
            offset += DspBlockConst.SIZE_OF_TIME_DOMAIN;
            m_exChannelMemOffset = new RExChannelMemOffset(offset);
            offset += DspBlockConst.SIZE_OF_EX_CHANNEL;
            m_pc2DSPEchoMemOffset = new RPC2DSPEchoMemOffset(offset);
            m_size = offset + DspBlockConst.SIZE_OF_PC2DSP_ECHO;
        }
        #endregion Constructors

        public RFFTSpectrumMemOffset FFTSpectrumMemOffset { get { return m_fftSpectrumMemOffset; } }
        public RFFTEnvelopesMemOffset FFTEnvelopesMemOffset { get { return m_fftEnvelopesMemOffset; } }
        public RHitsDataMemOffset HitsDataMemOffset { get { return m_hitsDataMemOffset; } }
        public RClinicalParametersMemOffset ClinicalParametersMemOffset { get { return m_clinicalParametersMemOffset;}}
        public RAutoScanMemOffset AutoScanMemOffset {	get { return m_autoScanMemOffset;}	}
        public RTimeDomainMemOffset TimeDomainMemOffset { get { return m_timeDomainMemOffset; } }
        public RExChannelMemOffset ExChannelMemOffset { get { return m_exChannelMemOffset; } }
        public RPC2DSPEchoMemOffset PC2DSPEchoMemOffset { get { return m_pc2DSPEchoMemOffset; } }
        public int SizeofDsp2Pc { get { return m_size; } }

        #region Private fields
        //private int m_iIndex;
        private int m_iRawMemOffset;
        private readonly RFFTSpectrumMemOffset m_fftSpectrumMemOffset;
        private readonly RFFTEnvelopesMemOffset m_fftEnvelopesMemOffset;
        private readonly RHitsDataMemOffset m_hitsDataMemOffset;
        private readonly RClinicalParametersMemOffset m_clinicalParametersMemOffset;
        private readonly RAutoScanMemOffset m_autoScanMemOffset;
        private readonly RTimeDomainMemOffset m_timeDomainMemOffset;
        private readonly RExChannelMemOffset m_exChannelMemOffset;
        private readonly RPC2DSPEchoMemOffset m_pc2DSPEchoMemOffset;				// 1.75
        private readonly int m_size;
        #endregion
    }

    public class RFFTSpectrumGateMemOffset
    {
        public RFFTSpectrumGateMemOffset(int iRawMemOffset)
        {
            int offset = iRawMemOffset;
            m_iRawMemOffset = iRawMemOffset;
            m_fftColumnMemOffset = new int[DspBlockConst.NUM_OF_COLUMNS];
            for (var i = 0; i < DspBlockConst.NUM_OF_COLUMNS; i++) 
            {
                m_fftColumnMemOffset[i] = offset;
                offset += DspBlockConst.NUM_OF_PIXELS_IN_COLMN;
            }
        }

        public int GetFFTColumnMemOffset(int index)
        {
            return m_fftColumnMemOffset[index];
        }

        private readonly int[] m_fftColumnMemOffset;
        private int m_iRawMemOffset;
    }

    public class RFFTSpectrumMemOffset
    {
        private const int NUM_OF_GATES = DspBlockConst.NUM_OF_GATES_PER_PROBE * DspBlockConst.NUM_OF_PROBES;
        public RFFTSpectrumMemOffset(int iRawMemOffset)
        {
            m_iRawMemOffset = iRawMemOffset;
            int offset = iRawMemOffset;
            m_fftSpectrumGatesArrMemOffset = new RFFTSpectrumGateMemOffset[NUM_OF_GATES];

            for (var i = 0; i < NUM_OF_GATES; i++) 
            {
                m_fftSpectrumGatesArrMemOffset[i] = new RFFTSpectrumGateMemOffset(offset);
                offset += DspBlockConst.SIZE_OF_FFT_SPECTRUM_GATE;
            }
        }

        public RFFTSpectrumGateMemOffset get_FFT_SpectrumGateMemOffset(int gateNum)
        {
            return m_fftSpectrumGatesArrMemOffset[gateNum];
        }

        private readonly RFFTSpectrumGateMemOffset[] m_fftSpectrumGatesArrMemOffset;
        private int m_iRawMemOffset;
    }

    public class REnvelopeIndexesMemOffset
    {
        public REnvelopeIndexesMemOffset(int iRawMemOffset)
        {
            int offset = iRawMemOffset;
            m_iRawMemOffset = iRawMemOffset;
            m_forwardMemOffset = offset;
            offset++;
            m_reverseMemOffset = offset;
        }

        public int ForwardMemOffset { get { return m_forwardMemOffset; } }
        public int ReverseMemOffset { get { return m_reverseMemOffset; } }
        private readonly int m_forwardMemOffset;
        private readonly int m_reverseMemOffset;
        private int m_iRawMemOffset;
    }

    public class REnvelopeGateMemOffset
    {
        public REnvelopeGateMemOffset(int iRawMemOffset)
        {
            int offset = iRawMemOffset;
            m_iRawMemOffset = iRawMemOffset;
            m_envelopeIndexesColumnsMemOffset = new REnvelopeIndexesMemOffset[DspBlockConst.NUM_OF_COLUMNS];
            for (var i = 0; i < DspBlockConst.NUM_OF_COLUMNS; i++) 
            {
                m_envelopeIndexesColumnsMemOffset[i] = new REnvelopeIndexesMemOffset(offset);
                offset += DspBlockConst.SIZE_OF_ENVELOPE_INDEXES;
            }
        }

        public REnvelopeIndexesMemOffset EnvelopeIndexesColumnMemOffset(int column)
        {
            return m_envelopeIndexesColumnsMemOffset[column];
        }

        private readonly REnvelopeIndexesMemOffset[] m_envelopeIndexesColumnsMemOffset;
        private int m_iRawMemOffset;
    }

    public class REnvelopeMemOffset
    {
        private const int NUM_OF_ENVELOPES = DspBlockConst.NUM_OF_GATES_PER_PROBE * DspBlockConst.NUM_OF_PROBES;
        public REnvelopeMemOffset(int iRawMemOffset)
        {
            int offset = iRawMemOffset;
            m_iRawMemOffset = iRawMemOffset;
            m_envelopeGateMemOffset = new REnvelopeGateMemOffset[NUM_OF_ENVELOPES];
            for (var i = 0; i < NUM_OF_ENVELOPES; i++) 
            {
                m_envelopeGateMemOffset[i] = new REnvelopeGateMemOffset(offset);
                offset += DspBlockConst.SIZE_OF_ENVELOPE_GATE;
            }
        }

        public REnvelopeGateMemOffset GetEnvelopeGateMemOffset(int index)
        {
            return m_envelopeGateMemOffset[index];
        }

        private readonly REnvelopeGateMemOffset[] m_envelopeGateMemOffset;
        private int m_iRawMemOffset;
    }

    public class RFFTEnvelopesMemOffset
    {
        public RFFTEnvelopesMemOffset(int iRawMemOffset)
        {
            int offset = iRawMemOffset;
            m_iRawMemOffset = iRawMemOffset;
            m_peakMemOffset = new REnvelopeMemOffset(offset);
            offset += DspBlockConst.SIZE_OF_ENVELOPE;
            m_modeMemOffset = new REnvelopeMemOffset(offset);
        }

        public REnvelopeMemOffset PeakMemOffset { get {return m_peakMemOffset;}}
        public REnvelopeMemOffset ModeMemOffset { get {return m_modeMemOffset;}}

        private readonly REnvelopeMemOffset m_peakMemOffset;
        private readonly REnvelopeMemOffset m_modeMemOffset;
        private int m_iRawMemOffset;
    }

    public class RHitsDetMemOffset
    {
        public RHitsDetMemOffset(int iRawMemOffset)
        {
            int offset = iRawMemOffset;
            m_typeMemOffset = offset;
            offset++;
            m_energyMemOffset = offset;
            offset++;
            m_speedIndexMemOffset = offset;
            offset += sizeof(UInt16);
            m_durationMemOffset = offset;
            m_iRawMemOffset = iRawMemOffset;
        }

        public int TypeMemOffset { get { return m_typeMemOffset; } }
        public int EnergyMemOffset { get { return m_energyMemOffset; } }
        public int SpeedIndexMemOffset { get { return m_speedIndexMemOffset; } }
        public int DurationMemOffset { get { return m_durationMemOffset; } }

        private readonly int m_typeMemOffset;			// 0 - no HITS, 1 HITS found. TBD - gas or solid
        private readonly int m_energyMemOffset;     	// in dB units
        private readonly int m_speedIndexMemOffset;	// the same like peak units
        private readonly int m_durationMemOffset;       // in 8msec columns
        private int m_iRawMemOffset;
    }

    public class RHitsGateMemOffset
    {
        public RHitsGateMemOffset(int iRawMemOffset)
        {
            int offset = iRawMemOffset;
            m_iRawMemOffset = iRawMemOffset;

            m_hitsDetMemOffsetArr = new RHitsDetMemOffset[DspBlockConst.NUM_OF_COLUMNS];
            for (var i = 0; i < DspBlockConst.NUM_OF_COLUMNS; i++) 
            {
                m_hitsDetMemOffsetArr[i] = new RHitsDetMemOffset(offset);
                offset += DspBlockConst.SIZE_OF_HITS_Det;
            }
        }

        public RHitsDetMemOffset GetHitsDetMemOffset(int ColumnNum)
        {
            return m_hitsDetMemOffsetArr[ColumnNum];
        }

        private readonly RHitsDetMemOffset[] m_hitsDetMemOffsetArr;
        private int m_iRawMemOffset;
    }

    public class RHitsDataMemOffset
    {
        const int NUM_OF_GATES = DspBlockConst.NUM_OF_GATES_PER_PROBE * DspBlockConst.NUM_OF_PROBES;
        public RHitsDataMemOffset(int iRawMemOffset)
        {
            int offset = iRawMemOffset;
            m_iRawMemOffset = iRawMemOffset;
            m_hitsGateMemOffsetArr = new RHitsGateMemOffset[NUM_OF_GATES];
            for (var i = 0; i < NUM_OF_GATES; i++) 
            {
                m_hitsGateMemOffsetArr[i] = new RHitsGateMemOffset(offset);
                offset += DspBlockConst.SIZE_OF_HITS_GATE;
            }
        }
        public RHitsGateMemOffset GetHitsGateMemOffset(int gateNum)
        {
            return m_hitsGateMemOffsetArr[gateNum];
        }

        private readonly RHitsGateMemOffset[] m_hitsGateMemOffsetArr;
        private int m_iRawMemOffset;
    }

    public class RClinicalParametersValueMemOffset 
    {
        public RClinicalParametersValueMemOffset(int iRawMemOffset)
        {
            int offset = iRawMemOffset;
            m_iRawMemOffset = iRawMemOffset;
            m_forwardMemOffset = offset;
            offset+=sizeof(UInt16);
            m_reverseMemOffset = offset;
        }

        public int ForwardMemOffset { get {return m_forwardMemOffset; }}
        public int ReverseMemOffset { get {return m_reverseMemOffset; }}

        private readonly int m_forwardMemOffset;
        private readonly int m_reverseMemOffset;
        private int m_iRawMemOffset;
    }

    public class RClinicalParametersGateMemOffset
    {
        public RClinicalParametersGateMemOffset(int iRawMemOffset)
        {
            int offset = iRawMemOffset;
            m_iRawMemOffset = iRawMemOffset;
            m_peakMemOffset = new RClinicalParametersValueMemOffset(offset);
            offset+=DspBlockConst.SIZE_OF_CLINICAL_PARAMETERS_VALUE;
            m_modeMemOffset = new RClinicalParametersValueMemOffset(offset);
            offset+=DspBlockConst.SIZE_OF_CLINICAL_PARAMETERS_VALUE;
            m_meanMemOffset = new RClinicalParametersValueMemOffset(offset);
            offset+=DspBlockConst.SIZE_OF_CLINICAL_PARAMETERS_VALUE;
            m_averageMemOffset = new RClinicalParametersValueMemOffset(offset);
            offset+=DspBlockConst.SIZE_OF_CLINICAL_PARAMETERS_VALUE;
            m_dvMemOffset = new RClinicalParametersValueMemOffset(offset);
            offset+=DspBlockConst.SIZE_OF_CLINICAL_PARAMETERS_VALUE;
            m_swMemOffset = new RClinicalParametersValueMemOffset(offset);
        }

        public RClinicalParametersValueMemOffset	PeakMemOffset { get { return m_peakMemOffset;}}
        public RClinicalParametersValueMemOffset	ModeMemOffset{ get { return m_modeMemOffset;}}
        public RClinicalParametersValueMemOffset	MeanMemOffset{ get { return m_meanMemOffset; }}
        public RClinicalParametersValueMemOffset	AverageMemOffset{ get { return m_averageMemOffset; }}
        public RClinicalParametersValueMemOffset	DVMemOffset{ get { return m_dvMemOffset; }}
        public RClinicalParametersValueMemOffset	SWMemOffset{ get { return m_swMemOffset;}}

        private readonly RClinicalParametersValueMemOffset	m_peakMemOffset;
        private readonly RClinicalParametersValueMemOffset	m_modeMemOffset;
        private readonly RClinicalParametersValueMemOffset	m_meanMemOffset;
        private readonly RClinicalParametersValueMemOffset	m_averageMemOffset;
        private readonly RClinicalParametersValueMemOffset	m_dvMemOffset;
        private readonly RClinicalParametersValueMemOffset	m_swMemOffset;
        private int m_iRawMemOffset;
    }
		
    public class RClinicalParametersMemOffset 
    {
        private const int NUM_OF_GATES = DspBlockConst.NUM_OF_GATES_PER_PROBE * DspBlockConst.NUM_OF_PROBES;

        public RClinicalParametersMemOffset(int iRawMemOffset)
        {
            int offset = iRawMemOffset;
            m_iRawMemOffset = iRawMemOffset;
            m_flageMemOffset = offset;
            offset+=sizeof(UInt16);
            m_heartRateMemOffset = offset;
            offset+=sizeof(UInt16);
            m_gateArrMemOffset = new RClinicalParametersGateMemOffset[NUM_OF_GATES];
            for(var i=0;i<NUM_OF_GATES;i++)
            {
                m_gateArrMemOffset[i] = new RClinicalParametersGateMemOffset(offset);
                offset+=DspBlockConst.SIZE_OF_CLINICAL_PARAMETERS_GATE;
            }
        }

        public int FlageMemOffset { get { return m_flageMemOffset; } }
        public int Heart_RateMemOffset { get { return m_heartRateMemOffset; } }

        public RClinicalParametersGateMemOffset get_ClinicalParametersGateMemOffset(int gateNum)
        {
            return m_gateArrMemOffset[gateNum];
        }
			
        private readonly int m_flageMemOffset;									//1.4.1
        private readonly int m_heartRateMemOffset;							//1.4.2
        private readonly RClinicalParametersGateMemOffset[] m_gateArrMemOffset;	 	//1.4.3-18
        private int m_iRawMemOffset;
    }

    public class RAutoScanMemOffset
    {
        public RAutoScanMemOffset(int iRawMemOffset)
        {
            int i;
            m_iRawMemOffset = iRawMemOffset;
            m_autoScanProbeMemOffset = new RAutoScanProbeMemOffset[DspBlockConst.NUM_OF_PROBES];
            int offset = iRawMemOffset;
            for (i = 0; i < DspBlockConst.NUM_OF_PROBES; i++)
            {
                m_autoScanProbeMemOffset[i] = new RAutoScanProbeMemOffset(offset);
                offset += DspBlockConst.SIZE_OF_R_AUTO_SCAN_PROBE;
            }
        }
        public RAutoScanProbeMemOffset AutoScanProbeMemOffset(int probeIndex)
        {
            return m_autoScanProbeMemOffset[probeIndex];
        }

        private int m_iRawMemOffset;
        private readonly RAutoScanProbeMemOffset[] m_autoScanProbeMemOffset;
    }

    public class RAutoScanColumnMemOffset
    {
        public RAutoScanColumnMemOffset(int iRawMemOffset)
        {
            m_iRawMemOffset = iRawMemOffset;
        }

        public int GetPixelMemOffset(int index)
        {
            return m_iRawMemOffset+index;
        }
        private readonly int m_iRawMemOffset;
    }

    public class RAutoScanProbeMemOffset
    {
        public RAutoScanProbeMemOffset(int iRawMemOffset)
        {
            int offset = iRawMemOffset;
            m_iRawMemOffset = iRawMemOffset;
				
            m_autoScanColumnMemOffsetArr = new RAutoScanColumnMemOffset[DspBlockConst.NUM_OF_COLUMNS];
            for (var i = 0; i < DspBlockConst.NUM_OF_COLUMNS; i++) 
            {
                m_autoScanColumnMemOffsetArr[i] = new RAutoScanColumnMemOffset(offset);
                offset += DspBlockConst.SIZE_OF_R_AUTO_SCAN_COLUMN;
            }
        }

        public RAutoScanColumnMemOffset get_ColumnMemOffset(int index)
        {
            return m_autoScanColumnMemOffsetArr[index];
        }
        private int m_iRawMemOffset;
        private readonly RAutoScanColumnMemOffset[] m_autoScanColumnMemOffsetArr;
    }

    public class RTimeDomainMemOffset
    {
        private const int NUM_OF_GATES = DspBlockConst.NUM_OF_GATES_PER_PROBE * DspBlockConst.NUM_OF_PROBES;

        public RTimeDomainMemOffset(int iRawMemOffset)
        {
            int offset = iRawMemOffset;
            m_iRawMemOffset = iRawMemOffset;

            m_timeDomainGateMemOffsetArr = new int[NUM_OF_GATES];
            for (var i = 0; i < NUM_OF_GATES; i++) 
            {
                m_timeDomainGateMemOffsetArr[i] = offset;
                offset += DspBlockConst.SIZE_OF_TIME_DOMAIN_GATE;
            }
        }

        public int GetTimeDomainGateMemOffset(int i) { return m_timeDomainGateMemOffsetArr[i]; }
        readonly int[] m_timeDomainGateMemOffsetArr;
        private int m_iRawMemOffset;
    }

    public class RExColumChannelsMemOffset
    {
        public RExColumChannelsMemOffset(int iRawMemOffset)
        {
            m_iRawMemOffset = iRawMemOffset;
        }

        public int get_ValMemOffset(int index)
        {
            int offset = m_iRawMemOffset + index * sizeof(Int16);
            return offset;
        }
				
        private readonly int m_iRawMemOffset;
    }

    public class RExChannelMemOffset
    {
        public RExChannelMemOffset(int iRawMemOffset)
        {
            int offset = iRawMemOffset;
            m_iRawMemOffset = iRawMemOffset;
            m_columnMemOffsetArr = new RExColumChannelsMemOffset[DspBlockConst.NUM_OF_COLUMNS];
            for (var i = 0; i < DspBlockConst.NUM_OF_COLUMNS; i++) 
            {
                m_columnMemOffsetArr[i] = new RExColumChannelsMemOffset(offset);
                offset += DspBlockConst.SIZE_OF_EX_COLUMN_CHANNEL;
            }
        }

        public RExColumChannelsMemOffset get_ColumnMemOffset(int colNum)
        {
            return m_columnMemOffsetArr[colNum];
        }

        private readonly RExColumChannelsMemOffset[] m_columnMemOffsetArr;
        private int m_iRawMemOffset;
    }

    public class RPC2DSPEchoMemOffset
    {
        public RPC2DSPEchoMemOffset(int iRawMemOffset)
        {
            int i;
            int offset = iRawMemOffset;
            m_iRawMemOffset = iRawMemOffset;
            m_cardInfoMemOffset = new RCardInfoMemOffset(offset);
            offset+=DspBlockConst.SIZE_OF_R_CARD_INFO;
            m_probeInfoMemOffsetArr = new RProbeInfoMemOffset[DspBlockConst.NUM_OF_PROBES];
            for (i = 0; i < DspBlockConst.NUM_OF_PROBES; i++) 
            {
                m_probeInfoMemOffsetArr[i] = new RProbeInfoMemOffset(i, offset);
                offset += DspBlockConst.SIZE_OF_R_PROBE_INFO;
            }
        }

        public RCardInfoMemOffset CardInfoMemOffset { get { return m_cardInfoMemOffset; } }
        public RProbeInfoMemOffset GetProInfoMemOffset(int probeIndex) { return m_probeInfoMemOffsetArr[probeIndex]; }

        private readonly RCardInfoMemOffset m_cardInfoMemOffset;
        private readonly RProbeInfoMemOffset[] m_probeInfoMemOffsetArr;
        private int m_iRawMemOffset;
    }
	#endregion


	public class RFFTColumn
    {
        public RFFTColumn(Byte[] rawMem, int iRawMemOffset)
        {
            m_iRawMemOffset = iRawMemOffset;
            m_rawMem		= rawMem;
        }

        public Byte GetIndex(int i)
        {
            return m_rawMem[i+m_iRawMemOffset];
        }

		private readonly Byte[] m_rawMem;
        private readonly int m_iRawMemOffset;
    }

    public class RFFTSpectrumGate
    {
        public RFFTSpectrumGate(Byte[] rawMem, RFFTSpectrumGateMemOffset fftSpectrumGateMemOffset)
        {
            m_fftSpectrumGateMemOffset = fftSpectrumGateMemOffset;
            m_rawMem = rawMem;
            m_fftColumnsArr = null;
        }

        public RFFTColumn GetColumn(int colNum)
        {
            if(m_fftColumnsArr==null) 
            {
                m_fftColumnsArr = new RFFTColumn[DspBlockConst.NUM_OF_COLUMNS];
                for(var i=0;i<DspBlockConst.NUM_OF_COLUMNS;i++) 
                {
                    m_fftColumnsArr[i] = null;
                }
            }

            if(m_fftColumnsArr[colNum]==null)
                m_fftColumnsArr[colNum] = new RFFTColumn(m_rawMem, m_fftSpectrumGateMemOffset.GetFFTColumnMemOffset(colNum));

            return m_fftColumnsArr[colNum]; 
        }

        private RFFTColumn[] m_fftColumnsArr;
        private readonly Byte[] m_rawMem;
        private readonly RFFTSpectrumGateMemOffset m_fftSpectrumGateMemOffset;
    }

    public class RFFTSpectrum
    {
        const int NUM_OF_GATES = DspBlockConst.NUM_OF_GATES_PER_PROBE * DspBlockConst.NUM_OF_PROBES;

        public RFFTSpectrum(Byte[] rawMem, RFFTSpectrumMemOffset fftSpectrumMemOffset)
        {
            m_rawMem = rawMem;
            m_fftSpectrumMemOffset = fftSpectrumMemOffset;
            m_fftSpectrumGatesArr = null;
        }

        public RFFTSpectrumGate GetGate(int gateNum)
        {
            if(m_fftSpectrumGatesArr==null) 
            {
                m_fftSpectrumGatesArr = new RFFTSpectrumGate[NUM_OF_GATES];
                for(var i = 0; i<NUM_OF_GATES; i++)
                    m_fftSpectrumGatesArr[i] = null;
            }

            if(m_fftSpectrumGatesArr[gateNum]==null)
                m_fftSpectrumGatesArr[gateNum] = new RFFTSpectrumGate(m_rawMem, m_fftSpectrumMemOffset.get_FFT_SpectrumGateMemOffset(gateNum));

            return m_fftSpectrumGatesArr[gateNum];
        }

        private RFFTSpectrumGate[] m_fftSpectrumGatesArr;
        private readonly Byte[] m_rawMem;
        private readonly RFFTSpectrumMemOffset m_fftSpectrumMemOffset;
    }

    public class REnvelopeIndexes
    {
        public REnvelopeIndexes(Byte[] rawMem, REnvelopeIndexesMemOffset envelopeIndexesMemOffset)
        {
            m_rawMem = rawMem;
            m_envelopeIndexesMemOffset = envelopeIndexesMemOffset;
        }

        public Byte Forward { get { return m_rawMem[m_envelopeIndexesMemOffset.ForwardMemOffset]; } }
        public Byte Reverse { get { return m_rawMem[m_envelopeIndexesMemOffset.ReverseMemOffset]; } }

        private readonly Byte[] m_rawMem;
        private readonly REnvelopeIndexesMemOffset m_envelopeIndexesMemOffset;
    }

    public class REnvelopeGate
    {
        public REnvelopeGate(Byte[] rawMem, REnvelopeGateMemOffset envelopeGateMemOffset)
        {
            m_envelopeGateMemOffset = envelopeGateMemOffset;
            m_rawMem = rawMem;
            m_columnsArr = null;
        }

        public REnvelopeIndexes GetColumn(int columnNum)
        {
            if(m_columnsArr==null) 
            {
                m_columnsArr = new REnvelopeIndexes[DspBlockConst.NUM_OF_COLUMNS];
                for(var i=0; i<DspBlockConst.NUM_OF_COLUMNS; i++)
                    m_columnsArr[i] = null;
            }

            if(m_columnsArr[columnNum]==null)
                m_columnsArr[columnNum] = new REnvelopeIndexes(m_rawMem, m_envelopeGateMemOffset.EnvelopeIndexesColumnMemOffset(columnNum));

            return m_columnsArr[columnNum];
        }

        private REnvelopeIndexes[] m_columnsArr;

        private readonly REnvelopeGateMemOffset m_envelopeGateMemOffset;
        private readonly Byte[] m_rawMem;
    }

    public class REnvelope
    {
        private const int NUM_OF_GATES = DspBlockConst.NUM_OF_GATES_PER_PROBE*DspBlockConst.NUM_OF_PROBES;
        public REnvelope(Byte[] rawMem, REnvelopeMemOffset envelopeMemOffset)
        {
            m_rawMem = rawMem;
            m_envelopeMemOffset = envelopeMemOffset;
            m_arrGates = null;
        }

        public REnvelopeGate GetGate(int index)
        {
            if(m_arrGates==null) 
            {
                m_arrGates = new REnvelopeGate[NUM_OF_GATES];
                for(var i=0;i<NUM_OF_GATES;i++)
                    m_arrGates[i] = null;
            }

            if(m_arrGates[index] == null) 
                m_arrGates[index] = new REnvelopeGate(m_rawMem, m_envelopeMemOffset.GetEnvelopeGateMemOffset(index));

            return m_arrGates[index];
        }
			
        private REnvelopeGate[] m_arrGates;

        private readonly REnvelopeMemOffset m_envelopeMemOffset;
        private readonly Byte[] m_rawMem;
    }

    public class RFFTEnvelopes
    {
        public RFFTEnvelopes(Byte[] rawMem, RFFTEnvelopesMemOffset fftEnvelopesMemOffset)
        {
            m_rawMem = rawMem;
            m_fftEnvelopesMemOffset = fftEnvelopesMemOffset;
            m_peak = null;
            m_mode = null;
        }

        public REnvelope Peak 
        {
            get
            {
                if(m_peak == null)
                    m_peak = new REnvelope(m_rawMem, m_fftEnvelopesMemOffset.PeakMemOffset);

                return m_peak;
            }
        }

        public REnvelope Mode
        {
            get
            {
                if(m_mode == null)
                    m_mode = new REnvelope(m_rawMem, m_fftEnvelopesMemOffset.ModeMemOffset);

                return m_mode;
            }
        }

        private REnvelope m_peak;
        private REnvelope m_mode;

        private readonly RFFTEnvelopesMemOffset m_fftEnvelopesMemOffset;
        private readonly Byte[] m_rawMem;
    }

    public class RHitsDet
    {
		public enum EHitType
		{
			NO_HIT		= 0,
			DETECTED	= 1,
			TBD			= 2
		}

        public RHitsDet(Byte[] rawMem, RHitsDetMemOffset hitsDetMemOffset)
        {
            m_hitsDetMemOffset  = hitsDetMemOffset;
            m_rawMem            = rawMem;
			SpeedIndex			= BitConverter.ToUInt16(m_rawMem, m_hitsDetMemOffset.SpeedIndexMemOffset);
			Duration			= BitConverter.ToUInt16(m_rawMem, m_hitsDetMemOffset.DurationMemOffset);
			IsHitDetected		= (HitType == EHitType.DETECTED || HitType == EHitType.TBD);
        }

		public override string ToString()
		{
			return string.Format("IsHitDetected={0}, HitType={1}, Energy={2}dB, SpeedIndex={3}, Duration={4}", IsHitDetected, HitType, Energy, SpeedIndex, Duration);
		}

		public EHitType HitType			{ get { return (EHitType) m_rawMem[m_hitsDetMemOffset.TypeMemOffset]; } }					// 0: NO HITs, 1: HITs detected, 2: TBD (gas / solid).
		public Byte		Energy			{ get { return m_rawMem[m_hitsDetMemOffset.EnergyMemOffset];} }     						// in dB units
		public UInt16	SpeedIndex		{ get; private set; }	// the same like peak units
		public UInt16	Duration		{ get; private set; }
        public bool		IsHitDetected   { get; private set; }

        private readonly RHitsDetMemOffset m_hitsDetMemOffset;
        private readonly Byte[] m_rawMem;
    }

    public class RHitsGate
    {
        public RHitsGate(Byte[] rawMem, RHitsGateMemOffset hitsGateMemOffset)
        {
            m_rawMem			= rawMem;
            m_hitsGateMemOffset = hitsGateMemOffset;
            m_hitsDetArr		= null;
        }

        public RHitsDet GetColumn(int columnNum)
        {
            if (m_hitsDetArr == null) 
            {
                m_hitsDetArr = new RHitsDet[DspBlockConst.NUM_OF_COLUMNS];
                for(var i=0; i<DspBlockConst.NUM_OF_COLUMNS; i++)
                    m_hitsDetArr[i] = null;
            }

            if (m_hitsDetArr[columnNum] == null)
                m_hitsDetArr[columnNum] = new RHitsDet(m_rawMem, m_hitsGateMemOffset.GetHitsDetMemOffset(columnNum));

            return m_hitsDetArr[columnNum];
        }

        private RHitsDet[] m_hitsDetArr;
        private readonly RHitsGateMemOffset m_hitsGateMemOffset;
        private readonly Byte[] m_rawMem;
    }

    public class RHitsData
    {
        const int NUM_OF_GATES = DspBlockConst.NUM_OF_GATES_PER_PROBE * DspBlockConst.NUM_OF_PROBES;

        public RHitsData(Byte[] rawMem, RHitsDataMemOffset hitsDataMemOffset)
        {
            m_hitsDataMemOffset = hitsDataMemOffset;
            m_rawMem			= rawMem;
            m_hitsGateArr		= null;
        }

        public RHitsGate GetHitsGate(int gateNum)
        {
            if(m_hitsGateArr == null)
            {
                m_hitsGateArr = new RHitsGate[NUM_OF_GATES];
                for(var i=0; i<NUM_OF_GATES; i++)
                    m_hitsGateArr[i] = null;
            }

            if(m_hitsGateArr[gateNum]==null)
                m_hitsGateArr[gateNum] = new RHitsGate(m_rawMem, m_hitsDataMemOffset.GetHitsGateMemOffset(gateNum));

            return m_hitsGateArr[gateNum];
        }

		RHitsGate[] m_hitsGateArr;
        readonly RHitsDataMemOffset m_hitsDataMemOffset;
        private readonly Byte[] m_rawMem;
    }

    public class RClinicalParametersValue
    {
        public RClinicalParametersValue(Byte[] rawMem, RClinicalParametersValueMemOffset  clinicalParametersValueMemOffset)
        {
            m_clinicalParametersValueMemOffset = clinicalParametersValueMemOffset;
            m_rawMem = rawMem;
        }

        public int Forward { get { return BitConverter.ToUInt16(m_rawMem, m_clinicalParametersValueMemOffset.ForwardMemOffset);} }	// w=0
        public int Reverse { get { return BitConverter.ToUInt16(m_rawMem, m_clinicalParametersValueMemOffset.ReverseMemOffset);} }

        private readonly RClinicalParametersValueMemOffset m_clinicalParametersValueMemOffset;
        private readonly Byte[] m_rawMem;
    }

    public class RClinicalParametersGate
    {
        public RClinicalParametersGate(Byte[] rawMem, RClinicalParametersGateMemOffset  clinicalParametersGateMemOffset)
        {
            m_rawMem = rawMem;
            m_clinicalParametersGateMemOffset = clinicalParametersGateMemOffset;
            m_peak = null;
            m_mode = null;
            m_mean = null;
            m_average = null;
            m_dv = null;
            m_sw = null;

        }

        public RClinicalParametersValue Peak 
        {
            get 
            {
                if(m_peak==null)
                    m_peak = new RClinicalParametersValue(m_rawMem, m_clinicalParametersGateMemOffset.PeakMemOffset);

                return m_peak;
            }
        }
			
        public RClinicalParametersValue Mode
        {
            get 
            {
                if(m_mode==null)
                    m_mode = new RClinicalParametersValue(m_rawMem, m_clinicalParametersGateMemOffset.ModeMemOffset);

                return m_mode;
            }
        }

        public RClinicalParametersValue Mean
        {
            get 
            {
                if(m_mean==null)
                    m_mean = new RClinicalParametersValue(m_rawMem, m_clinicalParametersGateMemOffset.MeanMemOffset);

                return m_mean;
            }
        }

        public RClinicalParametersValue Average
        {
            get 
            {
                if(m_average==null)
                    m_average = new RClinicalParametersValue(m_rawMem, m_clinicalParametersGateMemOffset.AverageMemOffset);

                return m_average;
            }
        }
			
        public RClinicalParametersValue DV
        {
            get 
            {
                if(m_dv==null)
                    m_dv = new RClinicalParametersValue(m_rawMem, m_clinicalParametersGateMemOffset.DVMemOffset);

                return m_dv;
            }
        }

        public RClinicalParametersValue SW
        {
            get 
            {
                if(m_sw == null)
                    m_sw = new RClinicalParametersValue(m_rawMem, m_clinicalParametersGateMemOffset.SWMemOffset);

                return m_sw;
            }
        }

        private RClinicalParametersValue m_peak;		//1.4.3.1+W
        private RClinicalParametersValue m_mode;		//1.4.3.3+W
        private RClinicalParametersValue m_mean;		//1.4.3.5+W
        private RClinicalParametersValue m_average;		//1.4.3.7+W
        private RClinicalParametersValue m_dv;			//1.4.3.9+W
        private RClinicalParametersValue m_sw;			//1.4.3.11+W
        private readonly RClinicalParametersGateMemOffset m_clinicalParametersGateMemOffset;
        private readonly Byte[] m_rawMem;
    }

    public class RClinicalParameters 
    {
        public RClinicalParameters(Byte[] rawMem, RClinicalParametersMemOffset clinicalParametersMemOffset)
        {
            m_rawMem = rawMem;
            m_clinicalParametersMemOffset = clinicalParametersMemOffset;

            m_clinicalParametersGateArr = null;
        }

        public UInt16 Flage { get { return BitConverter.ToUInt16(m_rawMem, m_clinicalParametersMemOffset.FlageMemOffset);} }									//1.4.1
        public UInt16 Heart_Rate{get { return BitConverter.ToUInt16(m_rawMem, m_clinicalParametersMemOffset.Heart_RateMemOffset); } }
			
        public RClinicalParametersGate get_Gate(int gateNum)
        {
            const int NUM_OF_GATES = DspBlockConst.NUM_OF_PROBES * DspBlockConst.NUM_OF_GATES_PER_PROBE;

            if(m_clinicalParametersGateArr == null) 
            {
                m_clinicalParametersGateArr = new RClinicalParametersGate[NUM_OF_GATES];
                for(var i = 0; i < NUM_OF_GATES; i++)
                    m_clinicalParametersGateArr[i] = null;
            }
				
            if(m_clinicalParametersGateArr[gateNum]==null)
                m_clinicalParametersGateArr[gateNum] = new RClinicalParametersGate(m_rawMem, m_clinicalParametersMemOffset.get_ClinicalParametersGateMemOffset(gateNum));
				
            return m_clinicalParametersGateArr[gateNum];
        }
			
        private RClinicalParametersGate[] m_clinicalParametersGateArr;
        private readonly RClinicalParametersMemOffset m_clinicalParametersMemOffset;
        private readonly Byte[] m_rawMem;
    }

    public class RAutoScanColumn
    {
        public RAutoScanColumn(Byte[] rawMem, RAutoScanColumnMemOffset autoScanColumnMemOffset)
        {
            m_autoScanColumnMemOffset = autoScanColumnMemOffset;
            m_rawMem = rawMem;
        }

        public Byte GetPixel(int index)
        {
	        if (m_prevIndex == index)
		        return m_prevPixel;

            var offset	= m_autoScanColumnMemOffset.GetPixelMemOffset(index);
	        m_prevPixel = m_rawMem[offset];
			m_prevIndex = index;

            return m_prevPixel;
        }

        private readonly RAutoScanColumnMemOffset m_autoScanColumnMemOffset;
        private readonly Byte[] m_rawMem;

		//Cache last pixel read.
	    private int m_prevIndex = -1;
	    private byte m_prevPixel = 0;
    }

    public class RAutoScanProbe
    {
        public RAutoScanProbe(Byte[] rawMem, RAutoScanProbeMemOffset autoScanProbeMemOffset)
        {
            m_rawMem = rawMem;
            m_autoScanProbeMemOffset = autoScanProbeMemOffset;
            m_autoScanColumnArr = null;
        }

        public RAutoScanColumn GetColumn(int index)
        {
            if (m_autoScanColumnArr == null)
                m_autoScanColumnArr = new RAutoScanColumn[DspBlockConst.NUM_OF_COLUMNS];
				
            if (m_autoScanColumnArr[index] == null)
                m_autoScanColumnArr[index] = new RAutoScanColumn(m_rawMem, m_autoScanProbeMemOffset.get_ColumnMemOffset(index));
				
            return m_autoScanColumnArr[index];
        }

        private RAutoScanColumn[] m_autoScanColumnArr;
        private readonly RAutoScanProbeMemOffset m_autoScanProbeMemOffset;
        private readonly Byte[] m_rawMem;
    }

    public class RAutoScan
    {
        public RAutoScan(Byte[] rawMem, RAutoScanMemOffset autoScanMemOffset)
        {
            m_rawMem = rawMem;
            m_autoScanMemOffset = autoScanMemOffset;	
        }

        public RAutoScanProbe GetProbe(int probeIndex)
        {
            if (m_arrAutoScanProbes == null)
                m_arrAutoScanProbes = new RAutoScanProbe[DspBlockConst.NUM_OF_PROBES];

            if(m_arrAutoScanProbes[probeIndex] == null) 
                m_arrAutoScanProbes[probeIndex] = new RAutoScanProbe(m_rawMem, m_autoScanMemOffset.AutoScanProbeMemOffset(probeIndex));

            return m_arrAutoScanProbes[probeIndex];
        }

        private readonly RAutoScanMemOffset m_autoScanMemOffset;
        private readonly Byte[] m_rawMem;
        private RAutoScanProbe[] m_arrAutoScanProbes;
    }

    public class RExColumnChannels
    {
        public RExColumnChannels(Byte[] rawMem, RExColumChannelsMemOffset exColumnChannelsMemOffset)
        {
            m_rawMem = rawMem;
            m_exColumnChannelsMemOffset = exColumnChannelsMemOffset;
        }

        public Int16 GetValue(int index)
        {
            var valOffset = m_exColumnChannelsMemOffset.get_ValMemOffset(index);
            return BitConverter.ToInt16(m_rawMem, valOffset);
        }

        private readonly RExColumChannelsMemOffset m_exColumnChannelsMemOffset;
        private readonly Byte[] m_rawMem;
    }

    public class RExChannel
    {
        public RExChannel(Byte[] rawMem, RExChannelMemOffset exChannelMemOffset)
        {
            m_rawMem = rawMem;
            m_columnsArr = null;
            m_exChannelMemOffset = exChannelMemOffset;
        }

        public RExColumnChannels GetColumn(int colNum) 
        {
            if (m_columnsArr == null) 
            {
                m_columnsArr = new RExColumnChannels[DspBlockConst.NUM_OF_COLUMNS];
                for (var i = 0; i < DspBlockConst.NUM_OF_COLUMNS; i++)
                    m_columnsArr[i] = null;
            }

            if (m_columnsArr[colNum] == null)
                m_columnsArr[colNum] = new RExColumnChannels(m_rawMem, m_exChannelMemOffset.get_ColumnMemOffset(colNum));

            return m_columnsArr[colNum];
        }

        private RExColumnChannels[] m_columnsArr;
        private readonly RExChannelMemOffset m_exChannelMemOffset;
        private readonly Byte[] m_rawMem;
    }

    public class RGateInfoEcho
    {
        public RGateInfoEcho(Byte[] rawMem, RGateInfoMemOffset gateInfoMemOffset)
        {
            m_rawMem			= rawMem;
            m_gateInfoMemOffset = gateInfoMemOffset;
			m_value				= BitConverter.ToUInt16(m_rawMem, m_gateInfoMemOffset.DepthMemOffset);
        }

        public UInt16 Depth
        {
			get { return m_value; }// get { return BitConverter.ToUInt16(m_rawMem, m_gateInfoMemOffset.DepthMemOffset); }
		}

        private readonly RGateInfoMemOffset m_gateInfoMemOffset;
        private readonly Byte[] m_rawMem;
	    private readonly UInt16 m_value;
    }

	public class RCardInfoEcho
	{
		private static readonly RCardInfoMemOffset s_memoryMap = RDspToPCFieldsOffset.OffsetMap.PC2DSPEchoMemOffset.CardInfoMemOffset;
		public RCardInfoEcho(Byte[] rawMem)
		{
			m_rawMem		= rawMem;
		}

		public ushort	OperationMode
		{
			get { return BitConverter.ToUInt16(m_rawMem, s_memoryMap.OperationModeMemOffset); }
		}

		public ushort	SoundVolume
		{
			get { return BitConverter.ToUInt16(m_rawMem, s_memoryMap.SoundVolumeMemOffset); }
		}

		public ushort	Gate2Hear
		{
			get { return BitConverter.ToUInt16(m_rawMem, s_memoryMap.Gate2HearMemOffset); }
		}

		public ushort	FFTSize
		{
			get { return BitConverter.ToUInt16(m_rawMem, s_memoryMap.FFTSizeMemOffset); }
		}

		public ushort	HitThreshHold
		{
			get { return BitConverter.ToUInt16(m_rawMem, s_memoryMap.HitThresholdMemOffset); }
		}

		public ushort	SpeakerOrHeadphone // 0 - speaker, 1 - headphone
		{
			get { return BitConverter.ToUInt16(m_rawMem, s_memoryMap.SpeakerOrHeadphoneMemOffset); }
		}

		public byte		MModeThreshould1
		{
			get { return m_rawMem[s_memoryMap.MModeThreshold1MemOffset]; }
		}

		public byte		MModeThreshould2
		{
			get { return m_rawMem[s_memoryMap.MModeThreshold2MemOffset]; }
		}


		private readonly Byte[] m_rawMem;
	}

    public class RProbeInfoEcho
    {
	    public const int NUM_OF_GATES_MIN	= 16;
		public const int NUM_OF_GATES_MAX	= 64;
		public const int DEPTH_START_MAX	= 128;

        public RProbeInfoEcho(Byte[] rawMem, RProbeInfoMemOffset probeInfoMemOffset)
        {
            m_rawMem = rawMem;
            m_probeInfoMemOffset = probeInfoMemOffset;
            m_gateSa = null;

			IsValid = (Width > 0 && Power > 0 && Range > 0 && Thump > 0 && NumOfGates >= NUM_OF_GATES_MIN && NumOfGates <= NUM_OF_GATES_MAX && DephtStart <= DEPTH_START_MAX);
        }

	    public ushort Width
	    {
		    get
		    {
				if (m_width == 0)
					m_width = BitConverter.ToUInt16(m_rawMem, m_probeInfoMemOffset.WidthMemOffset);

			    return m_width;
		    }
	    }

	    public ushort Power
	    {
		    get
		    {
				if (m_power == 0)
					m_power = BitConverter.ToUInt16(m_rawMem, m_probeInfoMemOffset.PowerMemOffset);

				return m_power;
			}
	    }
        
	    public ushort Gain
	    {
		    get
		    {
				if (m_gain == 0)
					m_gain = BitConverter.ToUInt16(m_rawMem, m_probeInfoMemOffset.GainMemOffset);

				return m_gain;
			}
	    }

	    public ushort Range
	    {
		    get
		    {
				if (m_range == 0)
					m_range = BitConverter.ToUInt16(m_rawMem, m_probeInfoMemOffset.RangeMemOffset);

				return m_range;
			}
	    }
        
	    public ushort Thump
	    {
		    get
		    {
				if (m_thump == 0)
					m_thump = BitConverter.ToUInt16(m_rawMem, m_probeInfoMemOffset.ThumpMemOffset);

				return m_thump;
			}
	    }

	    public ushort DirectionSwitch
	    {
		    get
		    {
				if (m_directionSwitch == 0)
					m_directionSwitch = BitConverter.ToUInt16(m_rawMem, m_probeInfoMemOffset.DirectionSwitchMemOffset);

				return m_directionSwitch;
			}
	    }

	    public ushort NumOfGates
	    {
		    get
		    {
				if (m_numOfGates == 0)
					m_numOfGates = BitConverter.ToUInt16(m_rawMem, m_probeInfoMemOffset.NumOfGatesMemOffset);

				return m_numOfGates;
			}
	    }

		public byte Sensitivity
	    {
		    get
		    {
				if (m_sensitivity == 0)
					m_sensitivity = m_rawMem[m_probeInfoMemOffset.ReserveMemOffset];

				return m_sensitivity;
			}
	    }

		public ushort DephtStart
		{
			get
			{
				if (m_dephtStart == 0)
					m_dephtStart = BitConverter.ToUInt16(m_rawMem, m_probeInfoMemOffset.StartDepthMemOffset);

				return m_dephtStart;
			}
		}

		public ushort DephtStep
		{
			get
			{
				if (m_dephtStep == 0)
					m_dephtStep = BitConverter.ToUInt16(m_rawMem, m_probeInfoMemOffset.DepthStepMemOffset);

				return m_dephtStep;
			}
		}

		//public Byte get_RESERVE(int index)
		//{
		//    return m_rawMem[m_probeInfoMemOffset.ReserveMemOffset + index];
		//}

        public RGateInfoEcho GetGate(int gateNum)
        {
            if (m_gateSa == null) 
                m_gateSa = new RGateInfoEcho[DspBlockConst.NUM_OF_GATES_PER_PROBE];

            if (m_gateSa[gateNum] == null) 
                m_gateSa[gateNum] = new RGateInfoEcho(m_rawMem, m_probeInfoMemOffset.Gate_saMemOffset(gateNum));

            return m_gateSa[gateNum];
        }

	    public bool IsValid { get; private set; }

        private readonly RProbeInfoMemOffset m_probeInfoMemOffset;
        private readonly Byte[] m_rawMem;
        private RGateInfoEcho[] m_gateSa; // 14 x 8 = 112

		public override string ToString()
		{
			return string.Format("IsValid={0}, Width={1}, Power={2}, Gain={3}, Range={4}, Thump={5}, DirectionSwitch={6}, NumOfGates={7}, Sensitivity={8}, DephtStart={9}, DephtStep={10}", IsValid, Width, Power, Gain, Range, Thump, DirectionSwitch, NumOfGates, Sensitivity, DephtStart, DephtStep);
		}

	    private ushort m_width				= 0;
		private ushort m_power				= 0;
		private ushort m_gain				= 0;
		private ushort m_range				= 0;
		private ushort m_thump				= 0;
		private ushort m_directionSwitch	= 0;
		private ushort m_numOfGates			= 0;
	    private byte   m_sensitivity		= 0;
		private ushort m_dephtStart			= 0;
		private ushort m_dephtStep			= 0;
	}

    public class RPC2DSPEcho 
    {
        public RPC2DSPEcho(Byte[] rawMem, RPC2DSPEchoMemOffset pc2DSPEchoMemOffset)
        {
            m_pc2DSPEchoMemOffset = pc2DSPEchoMemOffset;
            m_rawMem = rawMem;
            m_probeInfoSa = null;
        }

        public RProbeInfoEcho GetProbeInfo(int probeIndex)
        {
            if(m_probeInfoSa == null) 
                m_probeInfoSa = new RProbeInfoEcho[DspBlockConst.NUM_OF_PROBES];

            if(m_probeInfoSa[probeIndex] == null)
                m_probeInfoSa[probeIndex] = new RProbeInfoEcho(m_rawMem, m_pc2DSPEchoMemOffset.GetProInfoMemOffset(probeIndex));

            return m_probeInfoSa[probeIndex];
        }

		public RCardInfoEcho GetCardInfo()
		{
			if (m_cardInfoEcho == null)
				m_cardInfoEcho = new RCardInfoEcho(m_rawMem);

			return m_cardInfoEcho;
		}

	    private RCardInfoEcho	 m_cardInfoEcho;
        private RProbeInfoEcho[] m_probeInfoSa;

        private readonly RPC2DSPEchoMemOffset m_pc2DSPEchoMemOffset;
        private readonly Byte[] m_rawMem;
    }

    public class RDspToPc
    {
	    private static long s_counter = 0;

		public const int MEM_BYTE_OFFSET_NUMERATOR			= 0;
		public const int MEM_BYTE_OFFSET_FFT_SPECTRUM		= MEM_BYTE_OFFSET_NUMERATOR			+ sizeof(int);
		public const int MEM_BYTE_OFFSET_FFT_ENVELOPES		= MEM_BYTE_OFFSET_FFT_SPECTRUM		+ DspBlockConst.SIZE_OF_FFT_SPECTRUM;
		public const int MEM_BYTE_OFFSET_HITS_DATA			= MEM_BYTE_OFFSET_FFT_ENVELOPES		+ DspBlockConst.SIZE_OF_FFT_ENVELOPES;
		public const int MEM_BYTE_OFFSET_CLINICAL_PARAMS	= MEM_BYTE_OFFSET_HITS_DATA			+ DspBlockConst.SIZE_OF_HITS_DATA;
		public const int MEM_BYTE_OFFSET_AUTO_SCAN			= MEM_BYTE_OFFSET_CLINICAL_PARAMS	+ DspBlockConst.SIZE_OF_CLINICAL_PARAMETERS;
		public const int MEM_BYTE_OFFSET_TIME_DOMAIN		= MEM_BYTE_OFFSET_AUTO_SCAN			+ DspBlockConst.SIZE_OF_AUTO_SCAN;
	    public const int MEM_BYTE_OFFSET_EX_CHANNELS		= MEM_BYTE_OFFSET_TIME_DOMAIN		+ DspBlockConst.SIZE_OF_TIME_DOMAIN;	//RTimeDomain.SIZE_BYTES;	//49152;
		public const int MEM_BYTE_OFFSET_PCTODSP_ECHO		= MEM_BYTE_OFFSET_EX_CHANNELS		+ DspBlockConst.SIZE_OF_EX_CHANNEL;
		public const int MEM_BYTE_OFFSET_CTRL_FLAGS			= MEM_BYTE_OFFSET_PCTODSP_ECHO		+ DspBlockConst.SIZE_OF_PC2DSP_ECHO;
		public const int MEM_BYTE_OFFSET_DUP_NUMERATOR		= MEM_BYTE_OFFSET_CTRL_FLAGS		+ 2268;

		public RDspToPc(Byte[] buffer, bool isCheckNumerator = false)
        {
			m_msgBuffer				= buffer;

            m_fftSpectrumBlock		= null;
            m_fftEnvelopesBlock		= null;
            m_hitsData				= null;
            m_clinicalParameters	= null;
            m_autoScanBlock			= null;
            m_exChannel				= null;
            m_pc2DSPEcho			= null;

			Id						= Interlocked.Increment(ref s_counter);
			extractBufferNumerators();

            IsValid                 = validate(isCheckNumerator);
        }


		public RFFTSpectrum FFTSpectrumBlock
        {
            get
            {
                if (m_fftSpectrumBlock == null)
					m_fftSpectrumBlock = new RFFTSpectrum(m_msgBuffer, RDspToPCFieldsOffset.OffsetMap.FFTSpectrumMemOffset);

                return m_fftSpectrumBlock;
            }
        }

        public RFFTEnvelopes FFTEnvelopesBlock
        {
            get
            {
                if (m_fftEnvelopesBlock == null)
					m_fftEnvelopesBlock = new RFFTEnvelopes(m_msgBuffer, RDspToPCFieldsOffset.OffsetMap.FFTEnvelopesMemOffset);

                return m_fftEnvelopesBlock;
            }
        }

        public RHitsData HitsData
        {
            get
            {
                if (m_hitsData == null)
					m_hitsData = new RHitsData(m_msgBuffer, RDspToPCFieldsOffset.OffsetMap.HitsDataMemOffset);

                return m_hitsData;
            }
        }

        public RClinicalParameters ClinicalParametersBlock
        {
            get
            {
                if (m_clinicalParameters == null)
					m_clinicalParameters = new RClinicalParameters(m_msgBuffer, RDspToPCFieldsOffset.OffsetMap.ClinicalParametersMemOffset);

                return m_clinicalParameters;
            }
        }

        public RAutoScan AutoScanBlock
        {
            get
            {
                if (m_autoScanBlock==null)
					m_autoScanBlock = new RAutoScan(m_msgBuffer, RDspToPCFieldsOffset.OffsetMap.AutoScanMemOffset);

                return m_autoScanBlock;
            }
        }

		//public RTimeDomain	TimeDomain
		//{
		//    get
		//    {
		//        if (m_timeDomain == null)
		//            m_timeDomain = new RTimeDomain(m_msgBuffer, MEM_BYTE_OFFSET_TIME_DOMAIN);

		//        return m_timeDomain;
		//    }
		//}

        public Byte[] GetReplayTimeDomainGateCopy(int gateNum)
        { 
            var timeDomainGate = new Byte[DspBlockConst.SIZE_OF_TIME_DOMAIN_GATE];
			Array.Copy(m_msgBuffer, RDspToPCFieldsOffset.OffsetMap.TimeDomainMemOffset.GetTimeDomainGateMemOffset(gateNum), timeDomainGate, 0, timeDomainGate.Length);

            return timeDomainGate;
        }

		public int GetTimeGateMemStartOffset(int gateNumber)
		{
			return RDspToPCFieldsOffset.OffsetMap.TimeDomainMemOffset.GetTimeDomainGateMemOffset(gateNumber);
		}

        public RExChannel ExChannelsArr
        {
            get
            {
                if (m_exChannel == null)
					m_exChannel = new RExChannel(m_msgBuffer, RDspToPCFieldsOffset.OffsetMap.ExChannelMemOffset);

                return m_exChannel;
            }
        }

        public RPC2DSPEcho PC2DSPEcho 
        {
            get 
            {
                if (m_pc2DSPEcho == null)
					m_pc2DSPEcho = new RPC2DSPEcho(m_msgBuffer, RDspToPCFieldsOffset.OffsetMap.PC2DSPEchoMemOffset);

                return m_pc2DSPEcho;
            }
        }

		//public RAudioSamples AudioSamples
		//{
		//    get
		//    {
		//        if (m_audioSamples == null)
		//            m_audioSamples = new RAudioSamples(m_msgBuffer);

		//        if (m_audioSamples.IsValid)
		//            return m_audioSamples;

		//        return null;
		//    }
		//}

		public int	Numerator { get; private set; }

		public bool IsValid { get; private set; }

		public bool IsNumeratorMatch { get { return Numerator == m_dupNumerator; } }

		public long Id { get; private set; }

		public byte[] Buffer { get { return m_msgBuffer; } }

		public void RawDataToStream(FileStream destStream)
		{
			destStream.Write(m_msgBuffer, 0, m_msgBuffer.Length);
		}

		private void extractBufferNumerators()	
		{
			if (m_msgBuffer != null && m_msgBuffer.Length > BUFFER_NUMERATOR_SIZE * 2)
			{
				Numerator		= BitConverter.ToInt32(m_msgBuffer, 0);
				m_dupNumerator	= BitConverter.ToInt32(m_msgBuffer, m_msgBuffer.Length - BUFFER_NUMERATOR_SIZE);
			}
			else
			{
				Numerator		= BUFFER_NUMERATOR_INVALID;
				m_dupNumerator	= BUFFER_NUMERATOR_INVALID;
			}
		}

		private bool validate(bool isCheckNumerator)
		{
			if (m_msgBuffer == null)
			{
				Logger.LogWarning("DspToPc buffer is NULL.");
				return false;
			}

			if (m_msgBuffer.Length != DspBlockConst.SIZE_OF_R_DSP2PC)
			{
				Logger.LogWarning("DspToPc buffer size mismatch ({0}).", m_msgBuffer.Length);
                return false;
			}

            if (isCheckNumerator)
			{
                if (Numerator == BUFFER_NUMERATOR_INVALID || Numerator < BUFFER_NUMERATOR_MIN)
                {
                    Logger.LogWarning("DspToPc (Id={0}) buffer numerator is invalid: {1}.", Id, Numerator);
                    return false;
                }

                if (!IsNumeratorMatch)
			    {
                    Logger.LogWarning("DspToPc (Id={0}) buffer numerator mismatch ({1} vs. {2}).", Id, Numerator, m_dupNumerator);
                    return false;
			    }
            }

			return true;
		}

		private const int BUFFER_NUMERATOR_SIZE		= sizeof(Int32);
		private const int BUFFER_NUMERATOR_INVALID	= -1;
		private const int BUFFER_NUMERATOR_MIN		= 1;

		private readonly Byte[] m_msgBuffer;

        private RFFTSpectrum m_fftSpectrumBlock;
        private RFFTEnvelopes m_fftEnvelopesBlock;
        private RHitsData m_hitsData;
        private RClinicalParameters m_clinicalParameters;
        private RAutoScan m_autoScanBlock;
		//private RTimeDomain m_timeDomain;
		private RExChannel m_exChannel;
        private RPC2DSPEcho m_pc2DSPEcho;

        //private RAudioSamples	m_audioSamples;
		private int				m_dupNumerator;
	}
}
