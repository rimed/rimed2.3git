﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Rimed.Framework.Common;
using Rimed.Framework.Threads;
using Rimed.TCD.DspBlock;
using Rimed.TCD.Utils;

namespace Rimed.TCD.DSPData
{
    public class ExaminationRawData: Disposable
	{
		#region static members
		private const string CLASS_NAME = "ExaminationRawData";

        /// <summary>'*' + RawDataFile.DATA_FILES_EXTENTION</summary>
        private const string DATA_FILES_SERACH  = "*" + RawDataFile.DATA_FILES_EXTENTION;

		private static int s_fileCacheForward	= 1;
		private static int s_fileCacheBackward	= 0;

		private static int					calculateFrameIndex(int frameMSec)
		{
			var index = (int)Math.Round(frameMSec / RawDataFile.DSP_MESSAGE_DURATION_MSEC);
			return (index < 0 ? 0 : index);
		}


        public static ExaminationRawData    Instance { get; private set; }

		public static bool					IsCreated { get { return Instance != null; } }

		public static void					ReInitInstance(Guid examIndex, DateTime startTime)
        {
			var dataPath = Path.Combine(Constants.RAW_DATA_PATH, examIndex.ToString());
			if (IsCreated && string.Compare(Instance.PathRoot, dataPath, StringComparison.InvariantCultureIgnoreCase) == 0 && Instance.StartTime == startTime)
				return;

			var prev = Instance;
			Instance = (string.IsNullOrWhiteSpace(dataPath) ? null : new ExaminationRawData(dataPath, startTime));

			if (prev != null)
				prev.Dispose();
		}

        public static void					DisposeInstance()
        {
            if (IsCreated)
                Instance.TryDispose();

            Instance = null;
        }

		public static void					Init(int fileCacheForward, int fileCacheBackward)
		{
			s_fileCacheForward	= fileCacheForward;
			s_fileCacheBackward = fileCacheBackward;
		}

		public static int					GetFileIndex(int frameMSec)
		{
			var frameIndex = calculateFrameIndex(frameMSec);
			return frameIndex / RawDataFile.FILE_MAX_DSP_MESSAGES;
		}

		public static int					GetIndexInFile(int frameMSec)
		{
			var fileIndex	= GetFileIndex(frameMSec);
			var frameIndex	= calculateFrameIndex(frameMSec) - fileIndex * RawDataFile.FILE_MAX_DSP_MESSAGES;

			return (frameIndex < 0 ? 0 : frameIndex);
		}

		public static TimeSpan				CalcElapseTime(int fileIndex, int indexInFile)
		{
			var ms = CalcAbsolutIndex(fileIndex, indexInFile) * RawDataFile.DSP_MESSAGE_DURATION_MSEC;
			var ts = TimeSpan.FromMilliseconds(ms);
			return ts;
		}

		public static int					CalcAbsolutIndex(int fileIndex, int indexInFile)
		{
			var ix = fileIndex * RawDataFile.FILE_MAX_DSP_MESSAGES + (indexInFile & RawDataFile.FILE_MAX_DSP_MESSAGES);
			return ix;
		}
		#endregion


		private readonly object					m_listLock	= new object();
		private readonly List<RawDataFile>		m_dataFiles = new List<RawDataFile>();
		private readonly Queue<Tuple<int, int>> m_loadQueue = new Queue<Tuple<int, int>>();
		private readonly Thread					m_trdFileLoader;

		private string							m_pathRoot = string.Empty;
		private IRawDataFileFrame				m_lastCurrentFrame;

		public List<RDspToPc> GetFileData(int fileIndex)
        {
            var file = loadFile(fileIndex);
            return (file == null ? null : file.Frames);
        }

	    public ExaminationRawData(string dataPath, DateTime startTime)
        {
            StartTime	= startTime;
            PathRoot	= dataPath;

            // File loader thread
            m_trdFileLoader                 = new Thread(rawDataLoader);
            m_trdFileLoader.Priority        = ThreadPriority.BelowNormal;
            m_trdFileLoader.IsBackground    = true;
            m_trdFileLoader.SetName("RawDataLoader");

			init();
        }

        public string PathRoot
        {
            get { return m_pathRoot; }
            private set
            {
                if (string.Compare(m_pathRoot, value, StringComparison.InvariantCultureIgnoreCase) == 0)
                    return;

                m_pathRoot = Files.IsPathValid(value) ? value : string.Empty;
            }
        }

        #region File loader thread
        /// <summary>File index of current frame.</summary>
        private int				m_currentFileIndex = -1;

        /// <summary>Flag. Continue thread iterations</summary>
        private bool			m_continueThread        = true;

        private AutoResetEvent	m_whIterationWait = new AutoResetEvent(false);

        private void postFileIndexChangedCheck(int currFileIndex)
        {
			if (m_currentFileIndex == currFileIndex || m_whIterationWait == null)
                return;
            
            lock (m_loadQueue)
            {
	            m_loadQueue.Enqueue(new Tuple<int, int>(m_currentFileIndex, currFileIndex));
				if (m_whIterationWait != null)
					m_whIterationWait.Set();
			}

            m_currentFileIndex = currFileIndex;
        }

        private void rawDataLoader()
        {
			Logger.LogInfo("{0}.rawDataLoader(): START", CLASS_NAME);

            m_continueThread = true;
            while (m_continueThread)
            {
	            try
	            {
					if (m_loadQueue.Count == 0)
					{
						if (m_whIterationWait == null)
							break;

						m_whIterationWait.WaitOne(1000, false);

						continue;
					}

					Tuple<int, int> data;
					lock (m_loadQueue)
						data = m_loadQueue.Dequeue();

		            if (data != null)
			            onFileIndexChanged(data.Item1, data.Item2);
	            }
                catch (ThreadAbortException)
                {
					Logger.LogInfo("{0}.rawDataLoader(): ABORT", CLASS_NAME);
                    m_continueThread = false;
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex);
                }
            }

			Logger.LogInfo("{0}.rawDataLoader(): END", CLASS_NAME);
        }

        private void onFileIndexChanged(int prevFileIndex, int newFileIndex)
        {
            if (prevFileIndex == newFileIndex)
                return;

			Logger.LogDebug("{0}.onFileIndexChanged({1}, {2})", CLASS_NAME, prevFileIndex, newFileIndex);

			var startFileIndex = newFileIndex - s_fileCacheBackward;	
            if (startFileIndex < 0)
                startFileIndex = 0;

			var endFileIndex = newFileIndex + s_fileCacheForward;		
            if (endFileIndex > m_dataFiles.Count - 1)
                endFileIndex = m_dataFiles.Count - 1;

            for (int i = 0; i < m_dataFiles.Count; i++)
            {
                if (i >= startFileIndex && i <= endFileIndex)
                    m_dataFiles[i].Load();
                else
                    m_dataFiles[i].Unload();
            }
        }

        private IRawDataFileFrameReadOnly loadCurrentFrame()
        {
            if (m_lastCurrentFrame.BufferIndex == CurrentFrame.BufferIndex)
                return m_lastCurrentFrame;

            lock (m_listLock)
            {
                var index = frameIndexToFileIndex(CurrentFrame.BufferIndex);     // (CurrentBuffer.Number - FirstBuffer.Number) / RawDataFile.FILE_MAX_DSP_MESSAGES;
                var file = loadFile(index);
                if (file == null)
                    return null;

                file.CurrentBuffer.BufferIndex  = CurrentFrame.BufferIndex;
                m_lastCurrentFrame              = file.CurrentBuffer;

                return (m_lastCurrentFrame == null) ? null : m_lastCurrentFrame;
            }
        }

        private RawDataFile loadFile(int index)
        {
			//System.Diagnostics.Debug.WriteLine("OFER.DEBUG> loadFile({0})", index);
			
			if (index < 0 || index >= m_dataFiles.Count)
                return null;

            var file    = m_dataFiles[index];

            // If jump out of cache .... load on current thread and continue.
            if (!file.IsLoaded)
                file.Load();

            //Check if File index changed. Load/Unload file if needed.
            postFileIndexChangedCheck(index);

            return file;
        }
        #endregion


        protected override void DisposeManagedResources()
        {
            m_continueThread = false;

			lock (m_loadQueue)
				m_loadQueue.Clear();

			lock (m_listLock)
			{
				foreach (var dataFile in m_dataFiles)
				{
					dataFile.Unload();
				}

				m_dataFiles.Clear();
			}

	        var wh = m_whIterationWait;
			m_whIterationWait = null;
			if (wh != null)
			{
				wh.Set();
				wh.Dispose();
			}

	        base.DisposeManagedResources();
        }


		public int FilesCount { get { return m_dataFiles.Count; } }


		public IRawDataFileInfo CurrentFile
		{
			get
			{
				var index = frameIndexToFileIndex(CurrentFrame.BufferIndex);
				if (index < 0 || index >= m_dataFiles.Count)
					return null;

				return m_dataFiles[index];
			}
		}

		public DateTime StartTime { get; set; }

		/// <summary>Examination first frame.</summary>
		public IRawDataFileFrameInfoReadOnly FirstFrame { get; set; }

		/// <summary>Examination selected frame's data.</summary>
		public IRawDataFileFrameReadOnly CurrentFrameData
		{
			get
			{
				return loadCurrentFrame();
			}
		}

		/// <summary>Examination selected frame.</summary>
		public IRawDataFileFrameInfo CurrentFrame { get; set; }

		/// <summary>Examination last frame.</summary>
		public IRawDataFileFrameInfoReadOnly LastFrame { get; set; }

		public IRawDataFileFrameReadOnly Next()
		{
			return move(1);
		}

		public IRawDataFileFrameReadOnly Previous()
		{
			return move(-1);
		}

		public IRawDataFileFrameReadOnly JumpForward(int steps = 0)
		{
			if (steps == 0)
				steps = RawDataFile.DSP_MESSAGES_IN_SECOND;

			return move(steps);
		}

		public IRawDataFileFrameReadOnly JumpBackward(int steps = 0)
		{
			if (steps == 0)
				steps = RawDataFile.DSP_MESSAGES_IN_SECOND;

			return move(-steps);
		}


		private void init()
		{
			m_trdFileLoader.Start();

			// Load, validate and initiate data files from data folder. Sorted by data file numeric name. (Files data NOT loaded)  
			intValidateAndPopulate();

			// Calculate total frames
			var lastFileFrameCount = 0;
			if (m_dataFiles.Count > 0)
			{
				m_dataFiles[0].Load();
				postFileIndexChangedCheck(0);

				var fi = Files.GetFileInfo(m_dataFiles[m_dataFiles.Count - 1].FileName);
				if (fi != null)
					lastFileFrameCount = (int)(fi.Length / DspBlockConst.SIZE_OF_R_DSP2PC);
			}

			var frameCount = (m_dataFiles.Count - 1) * RawDataFile.FILE_MAX_DSP_MESSAGES + lastFileFrameCount;
			FirstFrame = new RawDataFileFrameInfo(frameCount, StartTime, 0);
			LastFrame = new RawDataFileFrameInfo(frameCount, StartTime, frameCount - 1);
			m_lastCurrentFrame = ((m_dataFiles.Count > 0) ? m_dataFiles[0].CurrentBuffer : new RawDataFileFrame(StartTime, 0));
			CurrentFrame = new RawDataFileFrameInfo(frameCount, StartTime, 0);

			Logger.LogDebug("{0}.Initiated. {1} files identified.", CLASS_NAME, m_dataFiles.Count);
		}

		/// <summary>Load, validate and initiate data files from data folder. Sorted by data file numeric name. (Files data NOT loaded)  </summary>
		/// <returns># files found.</returns>
		private int intValidateAndPopulate()
		{
			//TODO, Ofer: Add support for archived files.
			//      XZipManager.theXZip.UnzipFile(archiveName, cacheEntry.DSP2PCData.RawDataFileName);
			//      cacheEntry.DSP2PCData.LoadRawDataFromFile(worker);
			//      Files.Delete(archiveName);

			var pathFiles = new List<string>();
			if (!string.IsNullOrWhiteSpace(PathRoot))
				pathFiles = new List<string>(Directory.GetFiles(PathRoot, DATA_FILES_SERACH));

			var dic = new SortedDictionary<int, RawDataFile>();
			foreach (var file in pathFiles)
			{
				if (string.IsNullOrWhiteSpace(file))
					continue;

				// Check file extension is DATA_FILES_EXTENTION
				var s = Files.GetFileExtention(file);
				if (string.Compare(RawDataFile.DATA_FILES_EXTENTION, s, StringComparison.InvariantCultureIgnoreCase) != 0)
					continue;

				// Check file name is numeric
				s = Files.GetFileNameWithoutExtention(file);
				int fileNum;
				if (int.TryParse(s, out fileNum))
				{
					var fileStartTime = StartTime.AddMilliseconds(fileNum * RawDataFile.FILE_MAX_DSP_MESSAGES);
					var dataFile = new RawDataFile(file, fileStartTime);
					if (dataFile.IsValid)
						dic[fileNum] = dataFile;
				}
			}

			m_dataFiles.AddRange(dic.Values);

			return m_dataFiles.Count;
		}

		private int frameIndexToFileIndex(int frmaeIndex)
		{
			if (FirstFrame.BufferIndex == RawDataFileFrameInfo.INVALID)
				return RawDataFileFrameInfo.INVALID;

			return (frmaeIndex - FirstFrame.BufferIndex) / RawDataFile.FILE_MAX_DSP_MESSAGES;
		}

		/// <summary>Move examination selected frame relative to current frame.</summary>
		private IRawDataFileFrameReadOnly		move(int frames)
		{
			if (CurrentFrame == null)
				return null;

			var frm = getFrameByIndex(CurrentFrame.BufferIndex + frames);
			CurrentFrame = frm as IRawDataFileFrameInfo;
			return frm;
		}

		private IRawDataFileFrameReadOnly		getFrameByIndex(int frameIndex)
        {
            if (frameIndex < FirstFrame.BufferIndex || frameIndex > LastFrame.BufferIndex)
                return null;

            if (frameIndex == CurrentFrame.BufferIndex)
                return loadCurrentFrame();

			//var index = frameIndexToFileIndex(CurrentFrame.BufferIndex);
			var index = frameIndexToFileIndex(frameIndex);
			var file = loadFile(index);
	        if (file == null)
		        return null;

			file.CurrentBuffer.BufferIndex = frameIndex;
			return file.CurrentBuffer;
        }
    }
}
