﻿namespace Rimed.TCD.DSPData
{
    public interface IRawDataFileFrameReadOnly : IRawDataFileFrameInfoReadOnly
    {
        RDspToPc Data { get; }
    }
}