﻿using System.IO;

namespace Rimed.TCD.DSPData
{
    public class CDiagnosticFileMgr : CRawDataFileMgr
    {
        private const string CLASS_NAME = "CDiagnosticFileMgr";

        public CDiagnosticFileMgr()
        {
            ListOfDsp2Pc.Capacity = NUM_OF_BLOCKS_IN_FILE_DIAGNOSTIC;
        }

        public override void RawDataWrite(DspToPcContainer dspToPcContainer)
        {
            if (ListOfDsp2Pc.Count >= NUM_OF_BLOCKS_IN_FILE_DIAGNOSTIC)				//Bug fix: save between 24 and 36 sec for diagnostic, each file = 12 sec.
                ListOfDsp2Pc.RemoveAt(0);

            ListOfDsp2Pc.AddSafe(dspToPcContainer);
        }

        public override DspToPcContainer RawDataAlloc(int capacity)
        {
            var ret = new DspToPcContainer(capacity);
            return ret; 
        }

        protected override void DoRawDataRecordFinish() 
        {
            int fileNum = -1;
            if (DirName != null)
            {
                if (!Directory.Exists(DirName))
                    Directory.CreateDirectory(DirName);

				for (int i = 0; i < ListOfDsp2Pc.Count; i++)
                {
                    DspToPcContainer dspToPcContainer = ListOfDsp2Pc[i];
                    dspToPcContainer.RawDataFileName = RawDataStorage.RawDataStorageCreateFileNameForWrite(ref fileNum);
                    RawDataStorage.RawDataWrite(dspToPcContainer);
                }
			}

			ListOfDsp2Pc.Clear();
			RawDataStorage.RawDataRecordFinish();
			DirName = null;
        }

        public override void RawDataReadStart(string dirName, int readIndex)
        {
            RawDataReadStart(dirName, 0, NUM_OF_BLOCKS_IN_FILE_DIAGNOSTIC);
        }

        private const int NUM_OF_BLOCKS_IN_FILE_DIAGNOSTIC = 3;
    }
}