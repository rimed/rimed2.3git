﻿using System;

namespace Rimed.TCD.DSPData
{
    public interface IRawDataFileFrame : IRawDataFileFrameReadOnly
    {
        new int BufferIndex { get; set; }
        //new int MSec { get; set; }
        //new DateTime? Time { get; set; }
    }
}