﻿using System;
using System.Threading;

namespace Rimed.TCD.DSPData
{
    public class BmpRawData
    {
        private static long s_counter = 0;

		private int m_eventStartPointAbsoluteIndex;

        public BmpRawData()
        {
            Id = Interlocked.Increment(ref s_counter);
            reset();
        }

        private void reset()
        {
            DspToPcContainer				= null;
            EventStartPointAbsoluteIndex	= 0;
            LastDrawnIndex                     = -1;
            LastAbsoluteIndex				= -1;
            EventReplayPointAbsoluteIndex   = 0;
        }

        public override string		ToString()
        {
			return string.Format("Id={0}, LastDrawnIndex={1}, EventStartPointAbsoluteIndex={2}, LastAbsoluteIndex={3}, EventReplayPointAbsoluteIndex={4}, DspToPcContainer=({5})", Id, LastDrawnIndex, EventStartPointAbsoluteIndex, LastAbsoluteIndex, EventReplayPointAbsoluteIndex, DspToPcContainer);
        }

        public DspToPcContainer		DspToPcContainer;
		public int					EventStartPointAbsoluteIndex
        {
	        get { return m_eventStartPointAbsoluteIndex; }
	        set
	        {
		        m_eventStartPointAbsoluteIndex		= value;
				EventReplayPointAbsoluteIndex		= CalcAbsoluteIndex(m_eventStartPointAbsoluteIndex);
	        }
        }
		public int					EventReplayPointAbsoluteIndex;
        public int					LastDrawnIndex;
        public int					LastAbsoluteIndex;
	    public long					Id { get; private set; }

		public int CalcAbsoluteIndex(int indexInFile)
        {
            if (DspToPcContainer == null)
                return -1;

			return DspToPcContainer.CalcAbsoluteIndex(indexInFile); 
        }

		public double CalcElapsedMilliseconds(int indexInFile)
		{
			var time = DspToPcContainer.CalcAbsoluteIndex(indexInFile) * RawDataFile.DSP_MESSAGE_DURATION_SEC;
			return time;	
		}

		/// <summary>The time from the start of online mode</summary>
		public TimeSpan OnlineTime()
		{
			var ts = TimeSpan.Zero;
			if (LastDrawnIndex < 0)
				return ts;

			double secs;
			if (EventStartPointAbsoluteIndex != 0)
			{
				if (LastAbsoluteIndex < EventStartPointAbsoluteIndex || EventStartPointAbsoluteIndex < 0)
					return ts;

				secs = EventReplayPointAbsoluteIndex*RawDataFile.DSP_MESSAGE_DURATION_SEC;
			}
			else
				secs = LastAbsoluteIndex * RawDataFile.DSP_MESSAGE_DURATION_SEC;

			ts = TimeSpan.FromSeconds(secs);
			return ts;
		}

		public RDspToPc LastDrawnDspMsg 
		{
			get
			{
				if (DspToPcContainer == null || LastDrawnIndex < 0 || LastDrawnIndex >= DspToPcContainer.Count)
					return null;

				var dspToPc = DspToPcContainer[LastDrawnIndex];
				return dspToPc;
			}
		}
    }
}