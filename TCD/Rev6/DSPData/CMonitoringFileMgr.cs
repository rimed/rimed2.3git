﻿namespace Rimed.TCD.DSPData
{
    public class CMonitoringFileMgr : CRawDataFileMgr
    {
        private const string CLASS_NAME = "CMonitoringFileMgr";
            
        public override void RawDataWrite(DspToPcContainer dspToPcContainer)
        {
            RawDataStorage.RawDataWrite(dspToPcContainer);
        }

        public override DspToPcContainer RawDataAlloc(int capacity)
        {
            var ret = new DspToPcContainer(capacity);
            var fileNum = -1;
            ret.RawDataFileName = RawDataStorage.RawDataStorageCreateFileNameForWrite(ref fileNum);

            return ret;
        }

        protected override void DoRawDataRecordFinish()
        {
            RawDataStorage.RawDataRecordFinish();
            DirName = null;
        }

        public override void RawDataReadStart(string dirName, int readIndex)
        {
            RawDataReadStart(dirName, readIndex, NUM_OF_BLOCKS_READ_FORWARD);
        }

        public override DspToPcContainer RawDataReadNext(ref bool isLast)
        {
			//Logger.LogTrace(Logger.ETraceLevel.ENTER, CLASS_NAME, "RawDataReadNext(isLast={0}): ENTER", isLast);

            var dsp2Pc = base.RawDataReadNext(ref isLast);
            if (dsp2Pc == null && !isLast)
            {
                for (var i = ListOfDsp2Pc.Count; i < NUM_OF_BLOCKS_READ_FORWARD; i++)	
                {
                    var absoluteIndex = ReadIndex + i;
						
                    if (absoluteIndex > LastReadIndex)
                        break;

	                var fileName = DirName + @"\" + absoluteIndex + RawDataFile.DATA_FILES_EXTENTION;
                    var cacheEntryStatus = RawDataStorage.RawDataRead(fileName, out dsp2Pc);
                    if (cacheEntryStatus == CRawDataStorage.ECacheEntryStatus.CACHE_ENTRY_READ_FAIL)
                        LastReadIndex = absoluteIndex;

                    ListOfDsp2Pc[i] = dsp2Pc;
                }
            }

			//Logger.LogTrace(Logger.ETraceLevel.LEAVE, CLASS_NAME, "RawDataReadNext(isLast={0}): LEAVE", isLast);

            return dsp2Pc;
        }

        private const int NUM_OF_BLOCKS_READ_FORWARD = 1;
    }
}
