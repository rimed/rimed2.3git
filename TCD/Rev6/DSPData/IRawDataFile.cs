﻿namespace Rimed.TCD.DSPData
{
    public interface IRawDataFileInfo
    {
        string                      FileName { get; }
        int                         FileNum { get; }

        /// <summary>File first buffer mSec offset from examination start time</summary>
        int                         MSecOffset { get; }

        /// <summary>File first buffer offset first examination buffer index (0)</summary>
        int                         BufferOffset { get; }

        /// <summary># of buffers in file</summary>
        int                         FrameCount { get; }

        /// <summary>File first buffer.</summary>
        IRawDataFileFrameReadOnly   FirstBuffer { get; }

        /// <summary>When loaded: File current buffer. Null if not loaded or current buffer is out of file scope.</summary>
        IRawDataFileFrame           CurrentBuffer { get; }

        /// <summary>File last buffer.</summary>
        IRawDataFileFrameReadOnly   LastBuffer { get; }

        /// <summary>Is file buffers loaded.</summary>
        bool IsLoaded { get; }

        int InternalBufferIndex { get; }
    }
}