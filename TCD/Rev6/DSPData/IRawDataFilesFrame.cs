﻿namespace Rimed.TCD.DSPData
{
    public interface IRawDataFilesFrame : IRawDataFileFrame
    {
        int FileNumber { get; }
        int FileFrameNumber { get; }
    }
}