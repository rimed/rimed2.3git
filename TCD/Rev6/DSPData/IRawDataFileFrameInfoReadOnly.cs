﻿using System;

namespace Rimed.TCD.DSPData
{
    public interface IRawDataFileFrameInfoReadOnly
    {
        int BufferIndex { get; }
        int MSec { get; }
        DateTime? Time { get; }
    }
}