﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Rimed.Framework.Common;
using Rimed.Framework.Config;
using Rimed.Tools.BinFileWrapper;

namespace Rimed.TCD.Tools.FWUpgrader
{
    public class DSPComm: Disposable
    {
        #region CONSTANTS
        private const string CLASS_NAME                     = "DSPComm";

        /// <summary>= 1045</summary>
        private const int TCP_LISTENER_PORT					= 1045;

        /// <summary>= 10.0.0.17</summary>
        private const string TCP_LISTENER_IP				= "10.0.0.17";

        /// <summary>= 4096 byte</summary>
        public const int TCP_MAX_PACKET_SIZE                = 4096;

	    /// <summary>30 * 1000 = 30 Sec</summary>
	    private const int DSP_FILE_BURN_ACK_TIMEOUT_MSEC	= 30 * 1000;

        /// <summary>5 * 1000 = 5 Sec</summary>
        private const int DSP_RESTART_WAIT_INTERVAL_MSEC	= 5 * 1000;

	    /// <summary>15 * 1000 = 15 Sec</summary>
	    private const int DSP_STOP_TIMEOUT_MSEC				= 15 * 1000;


        /// <summary>Wait interval between two file packets: 50 mSec</summary>
        private const int SEND_FILE_WAIT_INTERVAL_MSEC      = 50;

		public static int TimeoutMultiplier { get; private set; }

	    #endregion

		static DSPComm()
		{
			TimeoutMultiplier = AppConfig.GetConfigValue("TimeoutMultiplier", 1);
			if (TimeoutMultiplier < 0)
				TimeoutMultiplier = 1;
		}


        private readonly TcpListener    m_tcpServer;
        private bool                    m_isServerStarted;
        private EStatus                 m_status        = EStatus.UNKNOWN;

        private NetStreamQThread        m_qtrdNetStream;
		private Files.Version			m_dspVersion;
		private Files.Version			m_fpgaVersion;


        public enum EStatus
        {
            UNKNOWN         = 0,

            STARTING        = 1,
            STARTED         = 2,
            
            CONNECTING      = 3,
            CONNECTED       = 4,
            
            DISCONNECTED    = 5,
            
            STOPING         = 6,
            STOPPED         = 7,
        }

		public Files.Version DspVersion
        {
            get { return m_dspVersion; }
        }

		public Files.Version FpgaVersion
        {
            get { return m_fpgaVersion; }
        }

        public bool         IsClientConnected
        {
            get { return (m_qtrdNetStream != null && m_qtrdNetStream.IsClientStreamEnabled); }
        }

	    private DSPComm(string ip, int port)
        {
            MainForm.LogTrace("DSPComm(ip={0}, port={1})",ip, port);
            try
            {
                var localAddr   = IPAddress.Parse(ip);
                m_tcpServer     = new TcpListener(localAddr, port);
            }
            catch (Exception ex)
            {
                MainForm.LogError(ex);    
                throw;
            }
        }

        public DSPComm() : this(TCP_LISTENER_IP, TCP_LISTENER_PORT)
        {
        }

        
        public void Start()
        {
            MainForm.LogTrace("DSPComm.Start()");

            if (m_tcpServer == null || m_isServerStarted)
                return;

            try
            {
                Status = EStatus.STARTING;
                m_tcpServer.Start();
                m_isServerStarted = true;
                Status = EStatus.STARTED;

                MainForm.Progress += 20;

                Status = EStatus.CONNECTING;
                m_qtrdNetStream = new NetStreamQThread(m_tcpServer);
                m_qtrdNetStream.Start();

                MainForm.Progress += 10;

                if (doHandshake())
                    Status = EStatus.CONNECTED;
                else
                    MainForm.LogError("Init communication: Handshake Fail.");
            }
            catch (Exception ex)
            {
                Status = EStatus.UNKNOWN;
                MainForm.LogError(ex);
            }
        }

        public bool GetHWVersions(int timeoutMsec = NetStreamQThread.SYNC_SEND_RECIEVE_DEFAULT_TIMEOUT_MSEC)
        {
            MainForm.LogTrace("DSPComm.GetHWVersions(timeoutMsec={0})", timeoutMsec);

            if (!IsClientConnected)
            {
                MainForm.LogTrace("DSPComm.GetHWVersions(...): Client NOT connected");
                return false;
            }

            var buffer = m_qtrdNetStream.Send(Protocol.PcToDsp.GetHWInfoReqBuffer(), true);
            if (buffer != null && Protocol.DspToPc.GetVersionResponse(buffer, ref m_dspVersion, ref m_fpgaVersion))
                return true;

            return false;
        }

        public void Stop()
        {
            MainForm.LogTrace("DSPComm.Stop()");

            if (m_tcpServer == null || !m_isServerStarted)
                return;

            m_isServerStarted = false;

            Status = EStatus.STOPING;

            try
            {
                if (IsClientConnected)
                {
                    m_qtrdNetStream.Purge();
                    m_qtrdNetStream.Send(Protocol.PcToDsp.GetResetReqBuffer(), true, 500);

	                var timeout = DSP_STOP_TIMEOUT_MSEC*(TimeoutMultiplier - 1);
	                if (timeout <= 0)
		                timeout = DSP_STOP_TIMEOUT_MSEC;
					if (!m_qtrdNetStream.PostStopJoin(timeout))
						m_qtrdNetStream.StopJoin(DSP_STOP_TIMEOUT_MSEC);
                }

                m_tcpServer.Stop();
                Status = EStatus.STOPPED;
            }
            catch (Exception ex)
            {
                Status = EStatus.UNKNOWN;
                MainForm.LogError(ex);
            }

            m_qtrdNetStream.TryDispose();
        }

        public void Restart(int wait = DSP_RESTART_WAIT_INTERVAL_MSEC)
        {
            MainForm.LogTrace("DSPComm.Restart(wait={0}mSec)", wait);

            Stop();

            Thread.Sleep(wait * TimeoutMultiplier);

            Start();
        }

        public EStatus Status
        {
            get { return m_status; }
            private set
            {
                MainForm.LogMessage("Status changed: {0}  ->  {1}.", m_status, value);
                m_status = value;
            }
        }

        public bool BurnFile(string file, EFileType fileType, int retryCount = 2)
        {
            var sendInterval = AppConfig.GetConfigValue("SendFilePacketsWaitInterval_mSec", SEND_FILE_WAIT_INTERVAL_MSEC);

			MainForm.LogTrace("DSPComm.BurnFile(fileType={0}, retryCount={1}, file={2}): Packet send interval {3} mSec.", fileType, retryCount, file, sendInterval);
			for (var i = 0; i < retryCount; i++)
            {
                Protocol.DspToPc.EBurnFileReturnCode returnCode;
                var isBurned = burnFile(file, fileType, sendInterval, out returnCode);
                if (isBurned)
                    return true;

                MainForm.LogError(string.Format("{0} file {1} burn attempt #{2}: error {3} (#{4}).", fileType, file, i, returnCode, (int)returnCode));
                if (returnCode != Protocol.DspToPc.EBurnFileReturnCode.NACK_BURN_FAILED)
                    return false;
            }

            return false;
        }


        protected override void DisposeManagedResources()
        {
            Stop();

            base.DisposeManagedResources();
        }

        private bool doHandshake(int timeoutMsec = NetStreamQThread.SYNC_SEND_RECIEVE_DEFAULT_TIMEOUT_MSEC)
        {
            MainForm.LogTrace("DSPComm.DoHandshake(timeoutMsec={0})", timeoutMsec);

            if (!IsClientConnected)
            {
                MainForm.LogTrace("DSPComm.DoHandshake(...): Client NOT connected");
                return false;
            }

            // Send request
            var buffer = m_qtrdNetStream.Send(Protocol.PcToDsp.GetHandshakeReqBuffer(), true);
            if (buffer != null && Protocol.DspToPc.IsHandshakeResponseOk(buffer))
                return true;

            return false;
        }


        private bool burnFile(string file, EFileType fileType, int sendInterval, out Protocol.DspToPc.EBurnFileReturnCode returnCode)
        {
            MainForm.LogTrace("DSPComm.burnFile(fileType={0}, file={1}, sendInterval={2})", fileType, file, sendInterval);

            returnCode = Protocol.DspToPc.EBurnFileReturnCode.INVALID;

            if (string.IsNullOrWhiteSpace(file) || fileType != EFileType.DSP && fileType != EFileType.FPGA)
            {
                MainForm.LogTrace("DSPComm.burnFile(...): Unsupported File type {0}", fileType);
                return false;
            }

            if (!IsClientConnected)
            {
                MainForm.LogTrace("DSPComm.burnFile(...): Client NOT connected");
                return false;
            }

            var tcpPacketMaxDataSize = TCP_MAX_PACKET_SIZE - Protocol.RimedMessageHeader.Size - Protocol.PcToDsp.BurnFileInitHeader.Size;
            var rxMsgType = (fileType == EFileType.DSP ? Protocol.EDigiLiteMessageType.BURN_DSP_RESPONSE : Protocol.EDigiLiteMessageType.BURN_FPGA_RESPONSE);

            Protocol.RimedMessage response;
            MainForm.Progress = 0;
			Files.Version imageVer;
            using (var reader = new WrapperFwdReader(file, tcpPacketMaxDataSize))
            {
                if (!reader.Open())
                {
                    MainForm.LogError("Error opening {0} image file.", fileType);
                    return false;
                }

                imageVer = reader.Header.FileVer;
                var fmtState = string.Format("Sending {0} v{1} image...", fileType, imageVer) +"{0}%";
                MainForm.ProgressState = string.Format(fmtState, 0);
                MainForm.LogTrace("DSPComm.burnFile(...): Burn {0} image file ('{1}' v{2}): FileSize={3}, BlockSize={4}, BlockCount={5}", fileType, file, imageVer, reader.SourceFileSize, reader.BlockSize, reader.BlockCount);

                var fileBlock   = reader.ReadFirst();
                var initMsg     = Protocol.PcToDsp.BurnFileGetInitReqBuffer(fileType, reader.SourceFileSize, reader.Header.GetFileSignature(), fileBlock, (ushort)reader.BlockCount);
                MainForm.LogTrace("DSPComm.burnFile(...): {0} image First/Initiation packet: {1}", fileType, initMsg);
                if (!m_qtrdNetStream.PostSend(initMsg))
                {
                    MainForm.LogError("Burn file initialization packet PostSend FAIL.");
                    return false;
                }

                var currFrame = Protocol.LastFrameNumber;

                while (IsClientConnected)
                {
                    if (reader.BlockCount > 0)
                    {
                        var percentage = 100 * reader.CurrentBlock / reader.BlockCount;
                        MainForm.Progress = percentage;
                        MainForm.ProgressState = string.Format(fmtState, percentage);
                    }

                    // Check RxQueue for file burn response message
                    response = m_qtrdNetStream.DequeueRxMessage(rxMsgType);
                    if (response != null)
                    {
                        returnCode = (Protocol.DspToPc.EBurnFileReturnCode)BitConverter.ToInt32(response.DataBuffer, 0);
                        if (returnCode != Protocol.DspToPc.EBurnFileReturnCode.ACK_OK)
                            return false;

                        returnCode = Protocol.DspToPc.EBurnFileReturnCode.INVALID;
                    }

                    if (sendInterval >= 0)
                        Thread.Sleep(sendInterval);

                    fileBlock = reader.ReadNext();
                    if (fileBlock == null || fileBlock.Length == 0 || reader.CurrentBlockStart < 0)
                        break;

                    var blockMsg = Protocol.PcToDsp.BurnFileGetNextFileBlock(fileType, fileBlock, currFrame, (ushort)reader.CurrentBlock, (ushort)reader.BlockCount, (uint)(reader.CurrentBlockStart + Protocol.PcToDsp.BurnFileInitHeader.Size));
                    MainForm.LogTrace("DSPComm.burnFile(...): {0} image next packet: {1}", fileType, blockMsg);
                    if (!m_qtrdNetStream.PostSend(blockMsg))
                    {
                        MainForm.LogError("Burn file packet #{0} PostSend FAIL.", reader.BlockCount);
                        return false;
                    }
                }

                reader.Close();
            }

            MainForm.Progress       = 100;
            MainForm.IsBlink        = true;
			MainForm.ProgressState = "   - Burning image  -  DO NOT TURN OFF the device";

            // Pending for DSP burn file Ack / NAck
            response = m_qtrdNetStream.WaitForRxMessage(rxMsgType, DSP_FILE_BURN_ACK_TIMEOUT_MSEC * TimeoutMultiplier, 200);

            MainForm.IsBlink        = false;

            if (response == null)
            {
                MainForm.LogError("{0} Wait response timeout.", rxMsgType);
                return false;
            }

            returnCode = (Protocol.DspToPc.EBurnFileReturnCode)BitConverter.ToInt32(response.DataBuffer, 0);

            MainForm.Progress       = 0;
            MainForm.ProgressState  = string.Empty;

            return (returnCode == Protocol.DspToPc.EBurnFileReturnCode.ACK_OK);
        }
    }
}
