﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using Rimed.Framework.Common;
using Rimed.Tools.BinFileWrapper;

namespace Rimed.TCD.Tools.FWUpgrader
{
    public static class Protocol
    {
        private static long                 s_sequentialTxId    = 0;
        private static readonly DateTime    s_initTime          = DateTime.UtcNow;

        public static uint LastFrameNumber { get; private set; }
        public static uint LastPacketNumber { get; private set; }

        public static uint GetTimestamp()
        {
            return (uint) DateTime.UtcNow.Subtract(s_initTime).TotalMilliseconds;
        }

        public static uint GetSequentialTxId()
        {
            return (uint)System.Threading.Interlocked.Increment(ref s_sequentialTxId);
        }

        public interface IDigiLiteMessage
        {
            byte[]  GetData();
            void    SetData(byte[] buffer);
            byte[]  GetMessageBuffer();
        }

        public enum EDigiLiteMessageType : ushort
        {
            UNKNOWN             = 0,

            HANDSHAKE_REQ       = 1,    // Handshake / alive mesaage request
            LOAD_DSP_REQ        = 2,    // Command to the DSP to load its program from the Flash memory
            CONTROL             = 3,    // Configuration control command, used to send user commands to the DSP through PC2DSP structur
            REPLAY_DATA         = 4,    // Send the recorded data to the DSP through DSP2PC structure
            HW_INFO_REQ         = 5,    // Request Hardware info (DSP and FPGA versions)
            BURN_DSP_REQ        = 6,    // Command to burn the Flash memory with a new DSP program (followed by the new DSP program file)
            BURN_FPGA_REQ       = 7,            
            RESET_DSP_REQ       = 8,

            HANDSHAKE_RESPONSE  = 101,	// Response to EPcToDspMessageType.HANDSHAKE_REQ (used to indicate that the DSP is alive)
            LOAD_DSP_RESPONSE   = 102, 	// Status of DSP at the end of loading DSP program process
            PLAY_DATA           = 103,	// sed to send data to the PC in unfreeze mode through DSP2PC structure (as described at 6.2.1)
            HW_INFO_RESPONSE    = 105,	// status message that send the board hardware information to the PC (DSP and FPGA versions)
            BURN_DSP_RESPONSE   = 106,	// Status of DSP at the end of burning DSP program to Flash memory process
            BURN_FPGA_RESPONSE  = 107,	// Status of DSP at the end of burning FPGA firmware to Flash memory process
            DEBUG_INFO_REQUEST  = 108,
        };

        [StructLayout(LayoutKind.Explicit, Pack = 1, Size = SIZE, CharSet = CharSet.Ansi)]
        public unsafe struct RimedMessageHeader : IDigiLiteMessage
        {
            public const int SIZE = 28;
            /// <summary>Message type: EPcToDspMessageType</summary>
            [FieldOffset(0)]    public EDigiLiteMessageType       MsgType;                    // 0 ..  1  message type

            /// <summary>Size of data within packet</summary>
            [FieldOffset(2)]    public ushort       EffectivePacketSize;        // 2 ..  3  size of data within packet

            /// <summary>Serial number of frame</summary>
            [FieldOffset(4)]    public uint         FrameNum;                   // 4 ..  7  serial number of frame

            /// <summary>serial number of IP packet within frame</summary>
            [FieldOffset(8)]    public ushort       PacketNum;                  // 8 ..  9  serial number of IP packet within  frame

            /// <summary>Total number of IP packets within frame</summary>
            [FieldOffset(10)]   public ushort       TotalPackets;               //10 .. 11  total number of IP packets within  frame

            /// <summary>offset in bytes of data within frame</summary>
            [FieldOffset(12)]   public uint         Offset;                     //12 .. 15  offset in bytes of data within  frame

            /// <summary>DSP timestamp (usecs)</summary>
            [FieldOffset(16)]   public uint         Timestamp;                  //16 .. 19  DSP timestamp (usecs)
            [FieldOffset(20)]   public uint         Reserved0;                  //20 .. 23
            [FieldOffset(24)]   public uint         Reserved1;                  //24 .. 27
            
            /// <summary>Byte array union</summary>
            [FieldOffset(0)]    public fixed byte Data[SIZE];                   // 0 .. 27


            public static int Size { get { return sizeof(RimedMessageHeader); } }


            public RimedMessageHeader(EDigiLiteMessageType msgType = EDigiLiteMessageType.HANDSHAKE_REQ, ushort effectivePacketSize = 0, uint frameNumber = 0, ushort packetNum = 0, ushort totalPackets = 1, uint offset = 0)
            {
                
                MsgType             = msgType;
                EffectivePacketSize = effectivePacketSize;
                FrameNum            = (frameNumber == 0 ? GetSequentialTxId() : frameNumber);
                PacketNum           = packetNum;
                TotalPackets        = totalPackets;
                Offset              = offset;
                Timestamp            = GetTimestamp();
                Reserved0           = 0;
                Reserved1           = 0;

                LastFrameNumber     = FrameNum;
                LastPacketNumber    = PacketNum;
            }

            public  RimedMessageHeader(byte[] buffer): this()
            {
                SetData(buffer);
            }

            
            public byte[]   GetData()
            {
                var res = new byte[28];
                fixed (byte* ptr = Data)
                {
                    Marshal.Copy((IntPtr)ptr, res, 0, res.Length);
                }

                return res;
            }

            public void     SetData(byte[] buffer)
            {
                if (buffer == null)
                    throw new ArgumentNullException("buffer");

                var size = buffer.Length;
                if (size > 28)
                    size = 28;

                fixed (byte* ptr = Data)
                {
                    Marshal.Copy(buffer, 0, (IntPtr)ptr, size);
                }
            }

            public byte[]   GetMessageBuffer()
            {
                return GetData();
            }

            public override string ToString()
            {
                return string.Format("MsgType={0}, EffectivePacketSize={1}, FrameNum={2}, PacketNum={3}, TotalPackets={4}, Offset={5}, Timestam={6}, Reserved0={7}, Reserved1={8}"
                                    , MsgType    , EffectivePacketSize    , FrameNum    , PacketNum    , TotalPackets    , Offset    , Timestamp    , Reserved0    , Reserved1);
            }
        }

        public class RimedMessage : IDigiLiteMessage
        {
            public RimedMessage(EDigiLiteMessageType msgType, byte[] dataBuffer, uint frameNum = 0, ushort packetNum = 1, ushort totalPackets = 1, uint offset = 0)
            {
                DataBuffer  = dataBuffer;
                Header      = new RimedMessageHeader(msgType, (ushort)(dataBuffer == null ? 0 : dataBuffer.Length), frameNum, packetNum, totalPackets,offset);
                Size        = RimedMessageHeader.Size + Header.EffectivePacketSize;
            }

            public RimedMessage(byte[] buffer)
            {
                if (buffer == null)
                    throw new ArgumentNullException("buffer");

                if (buffer.Length < RimedMessageHeader.SIZE)
                    throw new ArgumentException("Buffer size is smaller the message header size.");

                Header = new RimedMessageHeader(buffer);
                Size = RimedMessageHeader.Size + Header.EffectivePacketSize;

                if (Header.EffectivePacketSize > 0)
                {
                    DataBuffer = new byte[Header.EffectivePacketSize];

                    if (DataBuffer.Length > 0)
                        Array.Copy(buffer, RimedMessageHeader.SIZE, DataBuffer, 0, DataBuffer.Length);
                }

                //var dataSize = buffer.Length - RimedMessageHeader.SIZE;
                //if (dataSize <= 0)
                //    dataSize = 0;

                //DataBuffer = new byte[dataSize];
                //if (DataBuffer.Length > 0)
                //    Array.Copy(buffer, RimedMessageHeader.SIZE, DataBuffer, 0, DataBuffer.Length);
            }


            public RimedMessageHeader   Header      { get; private set; }
            public byte[]               DataBuffer  { get; private set; }
            public int                  Size        { get; private set; }

            public byte[] GetData()
            {
                var headerBuffer = Header.GetData();
                if (DataBuffer == null || DataBuffer.Length == 0)
                    return headerBuffer;

                var buffer = new byte[Size];
                Array.Copy(headerBuffer, buffer, headerBuffer.Length);
                Array.Copy(DataBuffer, 0, buffer, headerBuffer.Length, DataBuffer.Length);

                return buffer;
            }

            public void SetData(byte[] buffer)
            {
                throw new NotImplementedException();
            }

            public byte[]               GetMessageBuffer()
            {
                return GetData();
                //var headerBuffer = Header.GetData();
                //if (DataBuffer == null || DataBuffer.Length == 0)
                //    return headerBuffer;

                //var buffer = new byte[Size];
                //Array.Copy(headerBuffer, buffer, headerBuffer.Length);
                //Array.Copy(DataBuffer, 0, buffer, headerBuffer.Length, DataBuffer.Length);

                //return buffer;
            }

            public override string      ToString()
            {
                return string.Format("[{0}][DataBuffer.Len={1}, Size={2}]", Header, DataBuffer == null ? -1 : DataBuffer.Length, Size);
            }
        }

        public static class PcToDsp
        {
            private const string CLASS_NAME = "PcToDsp";
            [StructLayout(LayoutKind.Explicit, Pack = 1, Size = 24, CharSet = CharSet.Ansi)]
            public unsafe struct BurnFileInitHeader : IDigiLiteMessage
            {
                private const int SIGNATURE_SIZE        = 16;
                private const int BURN_TIMEOUT_MAX      = 15 * 60 * 1000;
                private const int BURN_TIMEOUT_NONE     = 0;

                private const int BURN_TIMEOUT_DEFAULT = BURN_TIMEOUT_NONE;
                
                [FieldOffset(0)]    public uint         FileSize;                       // 0 ... 3
                [FieldOffset(4)]    public uint         BurnTimeout;                    // 4 ... 7 0: NO Timeout
                [FieldOffset(8)]    public fixed byte   FileSignature[SIGNATURE_SIZE];  // 8 ... 23
                [FieldOffset(0)]    public fixed byte   Data[24];                       // 0 ... 23

                public static int Size { get { return sizeof(BurnFileInitHeader); } }


                public BurnFileInitHeader(uint fileSize, byte[] fileSignature, uint burnTimeoutMSec = BURN_TIMEOUT_DEFAULT)
                    : this()
                {
                    FileSize    = fileSize;
                    BurnTimeout = burnTimeoutMSec;
                    setSignature(fileSignature);
                }


                public byte[] GetData()
                {
                    var res = new byte[Size];
                    fixed (byte* ptr = Data)
                    {
                        Marshal.Copy((IntPtr)ptr, res, 0, res.Length);
                    }

                    return res;
                }

                public void SetData(byte[] buffer)
                {
                    if (buffer == null)
                        throw new ArgumentNullException("buffer");

                    var size = buffer.Length;
                    if (size > Size)
                        size = Size;

                    fixed (byte* ptr = Data)
                    {
                        Marshal.Copy(buffer, 0, (IntPtr)ptr, size);
                    }
                }

                public byte[] GetMessageBuffer()
                {
                    return GetData();
                }

                private void setSignature(byte[] buffer)
                {
                    if (buffer == null)
                        throw new ArgumentNullException("buffer");

                    if (buffer.Length != SIGNATURE_SIZE)
                        throw new ArgumentOutOfRangeException("buffer", string.Format("File signature size must be {0} bytes long.", SIGNATURE_SIZE));

                    fixed (byte* ptr = FileSignature)
                    {
                        Marshal.Copy(buffer, 0, (IntPtr)ptr, SIGNATURE_SIZE);
                    }
                }

                public override string ToString()
                {
                    return string.Format("FileSize={0}, BurnTimeout={1}mSec", FileSize, BurnTimeout);
                }
            }

            public class BurnFileInitRequestMessage : IDigiLiteMessage
            {
                public BurnFileInitRequestMessage(EFileType fileType, uint fileSize, byte[] fileSignature, byte[] dataBlock, ushort packetsCount = 1)
                {
                    if (fileType != EFileType.DSP && fileType != EFileType.FPGA)
                        throw new InvalidEnumArgumentException("fileType");

                    var msgType = (fileType == EFileType.DSP ? EDigiLiteMessageType.BURN_DSP_REQ : EDigiLiteMessageType.BURN_FPGA_REQ);
                    Header      = new RimedMessageHeader(msgType, (ushort)(BurnFileInitHeader.Size + dataBlock.Length), totalPackets: packetsCount);
                    InitHeader  = new BurnFileInitHeader(fileSize, fileSignature);
                    DataBlock   = dataBlock;

                    Size        = RimedMessageHeader.SIZE + Header.EffectivePacketSize;
                }

                public RimedMessageHeader       Header      { get; private set; }
                public BurnFileInitHeader       InitHeader  { get; private set; }
                public byte[]                   DataBlock   { get; private set; }
                public int Size { get; private set; }

                public byte[] GetData()
                {
                    var headerBuffer = Header.GetData();
                    if (Header.EffectivePacketSize == 0)
                        return headerBuffer;

                    var initBuffer = InitHeader.GetData();

                    var buffer = new byte[Size];
                    Array.Copy(headerBuffer, buffer, headerBuffer.Length);
                    Array.Copy(initBuffer, 0, buffer, headerBuffer.Length, initBuffer.Length);
                    Array.Copy(DataBlock, 0, buffer, headerBuffer.Length + initBuffer.Length, DataBlock.Length);

                    return buffer;
                }

                public void SetData(byte[] buffer)
                {
                    throw new NotImplementedException();
                }

                public byte[] GetMessageBuffer()
                {
                    return GetData();
                    //var headerBuffer = Header.GetData();
                    //if (Header.EffectivePacketSize == 0)
                    //    return headerBuffer;

                    //var initBuffer = InitHeader.GetData();

                    //var buffer = new byte[Size];
                    //Array.Copy(headerBuffer, buffer, headerBuffer.Length);
                    //Array.Copy(initBuffer, 0, buffer, headerBuffer.Length, initBuffer.Length);
                    //Array.Copy(DataBlock, 0, buffer, headerBuffer.Length + initBuffer.Length, DataBlock.Length);

                    //return buffer;
                }

                public override string ToString()
                {
                    return string.Format("[{0}][{1}][DataLen={2}]", Header, InitHeader, DataBlock == null ? -1 : DataBlock.Length);
                }
            }


            public static byte[] GetResetReqBuffer()
            {
                var msg     = new RimedMessageHeader(EDigiLiteMessageType.RESET_DSP_REQ);
                return msg.GetData();
            }

            public static byte[] GetHandshakeReqBuffer()
            {
                var msg     = new RimedMessageHeader(EDigiLiteMessageType.HANDSHAKE_REQ);
                return msg.GetData();
            }

            public static byte[] GetHWInfoReqBuffer()
            {
                var msg = new RimedMessageHeader(EDigiLiteMessageType.HW_INFO_REQ);
                return msg.GetData();
            }

            public static BurnFileInitRequestMessage BurnFileGetInitReqBuffer(EFileType fileType, uint fileSize, byte[] fileSignature, byte[] dataBlock, ushort packetsCount = 1)
            {
                var msg = new BurnFileInitRequestMessage(fileType, fileSize, fileSignature, dataBlock, packetsCount);
                return msg;
            }

            public static RimedMessage BurnFileGetNextFileBlock(EFileType fileType, byte[] dataBlock, uint frameNum, ushort packetNum, ushort totalPacket, uint offset)
            {
                if (fileType != EFileType.DSP && fileType != EFileType.FPGA)
                    throw new InvalidEnumArgumentException("fileType");

                var msgType = (fileType == EFileType.DSP ? EDigiLiteMessageType.BURN_DSP_REQ : EDigiLiteMessageType.BURN_FPGA_REQ);

                var msg = new RimedMessage(msgType, dataBlock, frameNum, packetNum, totalPacket, offset);
                return msg;
            }
        }

        public static class DspToPc
        {
            public enum EBurnFileReturnCode
            {
                ACK_OK                      = 0,
                NACK_GENERAL_FAILURE		= -1,
                NACK_INCOSTINTANT_PACKETNUM	= -2,
                NACK_INCOSTINTANT_FRAMENUM	= -3,
                NACK_INVALID_PACKET_SIZE	= -4,
                NACK_INVALID_PACKET_OFFSET	= -5,
                NACK_INVALID_IMAGESIZE		= -6,
                NACK_EXPIRED_TIMEOUT		= -7,
                NACK_CHECKSUM_CALC_FAILED	= -8,
                NACK_CHECKSUM_MISMATCH		= -9,
                NACK_BURN_FAILED			= -10,
                NACK_INVALID_AISFILE_FORMAT	= -11,

                INVALID                     = int.MinValue,
            }

            [StructLayout(LayoutKind.Explicit, Pack = 1, Size = 44, CharSet = CharSet.Ansi)]
            public unsafe struct HardWareVersionResponse : IDigiLiteMessage
            {
                [FieldOffset(0)]    public RimedMessageHeader Header;                    // 0 .. 27
                [FieldOffset(28)]   public uint FpgaSubVersion;                 //28 .. 31
                [FieldOffset(32)]   public uint FpgaMainVersion;                //32 .. 35
                [FieldOffset(36)]   public uint DspVersionNum;                  //36 .. 39
                [FieldOffset(40)]   public uint DspVersionFormat;               //40 .. 43
                [FieldOffset(0)]    public fixed byte Data[44];                 // 0 .. 43

                public int Size { get { return sizeof(HardWareVersionResponse); } }


                public HardWareVersionResponse(byte[] buffer)
                    : this()
                {
                    SetData(buffer);
                }

                public bool IsValid
                {
                    get { return Header.MsgType == EDigiLiteMessageType.HW_INFO_RESPONSE; }
                }


                public byte[] GetData()
                {
                    var res = new byte[44];
                    fixed (byte* ptr = Data)
                    {
                        Marshal.Copy((IntPtr)ptr, res, 0, res.Length);
                    }

                    return res;
                }

                public void SetData(byte[] buffer)
                {
                    if (buffer == null)
                        throw new ArgumentNullException("buffer");

                    var size = buffer.Length;
                    if (size > 44)
                        size = 44;

                    fixed (byte* ptr = Data)
                    {
                        Marshal.Copy(buffer, 0, (IntPtr)ptr, size);
                    }
                }

                public byte[] GetMessageBuffer()
                {
                    return GetData();
                }

				public Files.Version FpgaVersion
                {
                    get
                    {
						var ver = new Files.Version();
                        ver.Major = (ushort) FpgaMainVersion;
                        ver.Minor = (ushort) FpgaSubVersion;

                        return ver;
                    }
                }

				public Files.Version DspVersion
                {
                    get
                    {
						var ver = new Files.Version();

                        var dspVersionNum = (int)DspVersionNum;
                        if (DspVersionFormat == 1)
                        {
                            ver.Major       = (ushort) (dspVersionNum / 1000);
                            dspVersionNum   = dspVersionNum % 1000;
                            ver.Minor       = (ushort) (dspVersionNum / 100);
                            ver.Reversion   = (ushort) (dspVersionNum % 100);
                        }
                        else if (DspVersionFormat == 2)
                        {
                            ver.Major = (ushort) (dspVersionNum / 1000000);
                            dspVersionNum = dspVersionNum % 1000000;
                            ver.Minor = (ushort) (dspVersionNum / 10000);
                            dspVersionNum = dspVersionNum % 10000;
                            ver.Reversion = (ushort) (dspVersionNum / 100);
                            ver.Build = (ushort) (dspVersionNum % 100);
                        }

                        return ver;
                    }
                }
            }

            [StructLayout(LayoutKind.Explicit, Pack = 1, Size = 42, CharSet = CharSet.Ansi)]
            public unsafe struct HandshakeResponse : IDigiLiteMessage
            {
                [FieldOffset(0)]    public RimedMessageHeader Header;        // 0 .. 27
                [FieldOffset(28)]   public uint Reserver0;          //28 .. 31
                [FieldOffset(32)]   public uint Reserver1;          //32 .. 35
                [FieldOffset(36)]   public ushort Reserver2;        //36 .. 37

                [FieldOffset(0)]    public fixed byte Data[42];     // 0 .. 41

                public int Size { get { return sizeof(HandshakeResponse); } }

                public HandshakeResponse(byte[] buffer)
                    : this()
                {
                    SetData(buffer);
                }

                public bool IsValid
                {
                    get { return Header.MsgType == EDigiLiteMessageType.HANDSHAKE_RESPONSE; }
                }

                public byte[] GetData()
                {
                    var res = new byte[42];
                    fixed (byte* ptr = Data)
                    {
                        Marshal.Copy((IntPtr)ptr, res, 0, res.Length);
                    }

                    return res;
                }

                public void SetData(byte[] buffer)
                {
                    if (buffer == null)
                        throw new ArgumentNullException("buffer");

                    var size = buffer.Length;
                    if (size > 42)
                        size = 42;

                    fixed (byte* ptr = Data)
                    {
                        Marshal.Copy(buffer, 0, (IntPtr)ptr, size);
                    }
                }

                public byte[] GetMessageBuffer()
                {
                    return GetData();
                }
            }

            public static bool IsHandshakeResponseOk(byte[] buffer)
            {
                var handshake = new HandshakeResponse(buffer);
                Logger.LogInfo("IsHandshakeResponseOK(...): {0}", handshake.Header);
                return handshake.IsValid;
            }

			public static bool GetVersionResponse(byte[] buffer, ref Files.Version dspVer, ref Files.Version fpgaVer)
            {
                dspVer.Value  = 0;
                fpgaVer.Value = 0;

                var hwInfo   = new HardWareVersionResponse(buffer);
                Logger.LogInfo("GetVersionResponse(...): {0}", hwInfo.Header);
                if (hwInfo.IsValid)
                {
                    dspVer.Value = hwInfo.DspVersion.Value;
                    fpgaVer.Value = hwInfo.FpgaVersion.Value;
                }

                return hwInfo.IsValid;
                
            }
        }
    }
}
