﻿using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using Rimed.Framework.Common;
using Rimed.Framework.Config;
using Rimed.Framework.Threads;
using Rimed.Tools.BinFileWrapper;

namespace Rimed.TCD.Tools.FWUpgrader
{
    public partial class MainForm : Form
    {
        public const string APP_TITLE                       = "Rimed TCD Rev6 Firmware upgrader";

        private const string CLI_DELIMITER = "/";
        private const string FMT_CLI_PARAMETER              = CLI_DELIMITER + "{0}=";
        private const string FMT_CLI_SWITCH                 = CLI_DELIMITER + "{0}";

        private const string PARAM_DSP_FILE                 = "DSPFile";
        private const string PARAM_FPGAP_FILE               = "FPGAFile";
        private const string SWITCH_FORCE                   = "Force";
        private const string SWITCH_DOWNGRADE               = "EnableDowngrade";
        private const string SWITCH_DSP_PERMANENT_DOWNGRADE = "EnableDSPPermanentDowngrade";
		private const string SWITCH_STARTUP_RESTART			= "ResetDSPOnStartup";
        private const string SWITCH_VERIFY_BURN_IMAGES      = "VerifyBurnedImages";

		private static readonly Files.Version s_firstVersionSupportingFirmwareWUpgarde = new Files.Version(3, 1);

        private readonly DSPComm m_dspComm = new DSPComm();
        private WrapperHeader m_dspFileHeader;
        private WrapperHeader m_fpgaFileHeader;

        //private static MainForm s_this;

        public static void LogTrace(string format, params object[] args)
        {
            if (args != null && args.Length > 0)
                format = string.Format(format, args);

            Logger.LogInfo(format);
        }

        public static void LogMessage(string format, params object[] args)
        {
            if (args != null && args.Length > 0)
                format = string.Format(format, args);

            Logger.LogInfo(format);
            Console.WriteLine(format);
        }

	    private static void logWarninge(string format, params object[] args)
        {
            if (args != null && args.Length > 0)
                format = string.Format(format, args);

            Logger.LogWarning(format);
            Console.WriteLine(format);
        }

        public static void LogError(Exception ex)
        {
            if (ex == null)
                return;

            Logger.LogError(ex);
            Console.WriteLine(ex.Message);
        }

        public static void LogError(string format, params object[] args)
        {
            if (args != null && args.Length > 0)
                format = string.Format(format, args);

            Logger.LogError(format);
            Console.WriteLine(format);
        }

	    private Color m_panalinitialColor;
        public MainForm()
        {
            InitializeComponent();

			m_panalinitialColor = panelMain.BackColor;
            //s_this      = this;
            //s_this.Text = APP_TITLE;
            //label1.Text = APP_TITLE;

			Text = APP_TITLE;
			Text = APP_TITLE;

			loadSettings();
        }


        public bool Init()
        {
            Progress = 0;
            updateProgress();
            
            var isInitiated = init();
            
            Progress = isInitiated ? 100 : 0;
            updateProgress();

            return isInitiated;
        }

        public bool IsFilesProvided
        {
            get { return !(string.IsNullOrWhiteSpace(FPGAFile) && string.IsNullOrWhiteSpace(DSPFile)); }
        }

		//private bool IsDSPSupportUpgrade
		//{
		//    get
		//    {
		//        if (m_dspComm == null || !m_dspComm.IsClientConnected)
		//        {
		//            LogError("DSP is not connected. Firmware upgrade support is undetermined.");
		//            return false;
		//        }

		//        if (m_dspComm.DspVersion.Value < s_dspPermanentDowngradeVersion.Value)
		//        {
		//            LogMessage("Firmware upgrade is NOT supported on DSP versions earlier than v{0} (Current DSP version: v{1}).", s_dspPermanentDowngradeVersion, m_dspComm.DspVersion);
		//            return false;
		//        }

		//        return true;
		//    }
		//}

        public void Start()
        {
			var trd = new Thread(threadStartWrapper);
            trd.IsBackground = true;
            trd.SetName("FWImageBurner");
            trd.Start();
        }


        #region Settings
        public string DSPFile { get; private set; }
        public string FPGAFile { get; private set; }

        public bool IsForceUpgrade { get; private set; }
        public bool IsEnabledDowngrade { get; private set; }
        public bool IsEnabledDSPPermanentDowngrade { get; private set; }

        public bool IsResetDSPOnStartup { get; private set; }
        public bool IsVerifyBurnedImages { get; private set; }
        
        private void loadSettings()
        {
            ProgressState = "Loading settings...";

            DSPFile                         = loadFileNameParam(PARAM_DSP_FILE);
            FPGAFile                        = loadFileNameParam(PARAM_FPGAP_FILE);

            IsForceUpgrade                  = loadSwitchValue(SWITCH_FORCE);
            IsEnabledDowngrade              = loadSwitchValue(SWITCH_DOWNGRADE);
            IsEnabledDSPPermanentDowngrade  = loadSwitchValue(SWITCH_DSP_PERMANENT_DOWNGRADE);
            IsResetDSPOnStartup               = loadSwitchValue(SWITCH_STARTUP_RESTART);
            IsVerifyBurnedImages            = loadSwitchValue(SWITCH_VERIFY_BURN_IMAGES, true);

            LogMessage("Image file(s):");
            LogMessage("- DSP : '{0}'.", string.IsNullOrWhiteSpace(DSPFile) ? "NONE" : DSPFile);
            LogMessage("- FPGA: '{0}'.", string.IsNullOrWhiteSpace(FPGAFile) ? "NONE" : FPGAFile);
            LogMessage("Switches:");
            LogMessage("- Force same version upgrade: {0}", IsForceUpgrade ? "YES" : "No");
            LogMessage("- Enabled version downgrade: {0}", IsEnabledDowngrade ? "YES" : "No");
            LogMessage("- Enabled DSP Permanent downgrade: {0}", IsEnabledDSPPermanentDowngrade ? "YES" : "No");
            LogMessage("- Startup restart: {0}", IsResetDSPOnStartup ? "YES" : "No");
            LogMessage("- Verify burned images: {0}", IsVerifyBurnedImages ? "YES" : "No");

			LogMessage("Other:");
			LogMessage("- Timeout multiplier: {0}", DSPComm.TimeoutMultiplier);
			

            ProgressState = "Loading settings...Done";
        }

        private string loadFileNameParam(string paramName)
        {
            var val = loadParamValue(paramName).Replace("\"", string.Empty);
            if (string.IsNullOrWhiteSpace(val))
                return string.Empty;

            return Files.GetAbsolutPath(val);
        }

        private string loadParamValue(string paramName)
        {
            var val = getCmdLineParamValue(string.Format(FMT_CLI_PARAMETER, paramName));
            if (string.IsNullOrWhiteSpace(val))
                val = AppConfig.GetConfigValue(paramName, string.Empty);

            return val;
        }

        private bool loadSwitchValue(string switchName, bool defValue = false)
        {
            var cliSwitch = string.Format(FMT_CLI_SWITCH, switchName);
            var val = (Environment.CommandLine.IndexOf(cliSwitch, 0, StringComparison.Ordinal) >= 0);
            if (!val)
                val = AppConfig.GetConfigValue(switchName, defValue);

            return val;
        }

        private string getCmdLineParamValue(string param)
        {
            var start = Environment.CommandLine.IndexOf(param, 0, StringComparison.InvariantCultureIgnoreCase);
            if (start < 0 || start + param.Length >= Environment.CommandLine.Length)
                return string.Empty;

            start += param.Length;
            var end = Environment.CommandLine.IndexOf(CLI_DELIMITER, start, StringComparison.InvariantCultureIgnoreCase);
            if (end == -1)
                return Environment.CommandLine.Substring(start);

            if (end - start <= 0)
                return string.Empty;

            return Environment.CommandLine.Substring(start, end - start);
        }


        private delegate void SyncLoggedDialogDelegate(string msg, bool isError);
        private void syncLoggedDialog(string msg, bool isError = true)
        {
            if (InvokeRequired)
            {
                Invoke((SyncLoggedDialogDelegate)syncLoggedDialog, msg, isError);
                return;
            }

            Hide();
            if (isError)
				LoggedDialog.ShowError(this, msg);
            else
				LoggedDialog.ShowNotification(this, msg);
        }

        private bool init()
        {
            LogMessage("Verify image file(s)....");

            //
            // DSP image wrapper file
            //
            updateProgress(-1, "Verify DSP image file....");
            if (!verifyWrappedFile(EFileType.DSP, ref m_dspFileHeader))
                return false;

            updateProgress(10);

            // Confirm DSP downgrade
			if (m_dspFileHeader.IsValid && m_dspFileHeader.FileVer.Value < s_firstVersionSupportingFirmwareWUpgarde.Value)
            {
                if (!IsEnabledDSPPermanentDowngrade)
                {
                    LogError("Burning DSP v{0} will prohibit future upgrades. Action aborted.", m_dspFileHeader.FileVer);
                    return false;
                }

                updateProgress(30, "ATTENTION: PERMANENT DOWNGRADE.");
                var msg = string.Format("\t- A T E N T I O N -\n\nBurning DSP v{0} will prohibit future hardware upgrades.\n\nClick <Yes> to CONTINUE with DSP image burn.", m_dspFileHeader.FileVer);
				if (LoggedDialog.ShowConfirmation(this, msg, "Permanent Downgrade", MessageBoxButtons.YesNo, MessageBoxDefaultButton.Button2) != DialogResult.OK)
                    return false;
            }


            //
            // FPGA image wrapper file
            //
            updateProgress(30, "Verify FPGA image file....");

            // Verify FPGA upgrade file 
            if (!verifyWrappedFile(EFileType.FPGA, ref m_fpgaFileHeader))
                return false;

            return true;
        }
        #endregion

	    private bool m_wkrThreadEnd;

		private void threadStartWrapper()
		{
			try
			{
				start();
				syncClose();
			}
			catch (Exception ex)
			{
				LogError(ex);
			}
			finally
			{
				m_wkrThreadEnd = true;
			}
		}

        private void start()
        {
            if (!initCommModule())
            {
                LogError("PC-DSP Communication initiation fail");
                syncLoggedDialog("Hardware connection FAIL.\n\nPlease check log file for more details.");

                return;
            }

			if (m_dspComm == null || !m_dspComm.IsClientConnected)
			{
				syncLoggedDialog("DSP is not connected.\n\nFirmware upgrade support is undetermined.");	//Bug FIX
				return;	
			}
	        
			// Check if DSP support image file upgrade (v3.1 and above.)
			if (m_dspComm.DspVersion.Value < s_firstVersionSupportingFirmwareWUpgarde.Value)
			{
				var msg = string.Format("DSP v{0} dose not support firmware upgrade.\n\nFirmware upgrades supported on DSP v{1} and above.", m_dspComm.DspVersion, s_firstVersionSupportingFirmwareWUpgarde);
				syncLoggedDialog(msg);			//Bug FIX
				return;
			}

            var isDSPBurnOk = false;
            if (compareImageAndCurrentVersions(DSPFile, ref m_dspFileHeader, EFileType.DSP, m_dspComm.DspVersion.Value) && m_dspFileHeader.IsValid)
            {
                // Burn DSP image file
                isDSPBurnOk = m_dspComm.BurnFile(DSPFile, EFileType.DSP);
                if (!isDSPBurnOk)
                {
                    syncLoggedDialog("DSP Firmware upgrade FAIL.\n\nCheck application log for details.");
                    return;
                }
            }

            var isFpgaBurnOk = false;
            if (compareImageAndCurrentVersions(FPGAFile, ref m_fpgaFileHeader, EFileType.FPGA, m_dspComm.FpgaVersion.Value) && m_fpgaFileHeader.IsValid)
            {
                // Burn FPGA image file
                isFpgaBurnOk = m_dspComm.BurnFile(FPGAFile, EFileType.FPGA);
                if (!isFpgaBurnOk)
                {
					syncLoggedDialog("FPGA Firmware upgrade FAIL.\n\nCheck application log for details.");
                    
					// If DSP NOT burned exit - hardware reset is not required.
                    if (!isDSPBurnOk)
                        return;
                }
            }

            // Verify burned images
            if (isDSPBurnOk || isFpgaBurnOk)
            {
				if (!IsVerifyBurnedImages)
				{
					syncLoggedDialog("Operation completed.\n\nNOTE: Burned Firmware images were NOT loaded and verified.");
					return;
				}

				var prevDSPVer = m_dspComm.DspVersion;
				var prevFpgaVer = m_dspComm.FpgaVersion;

				string msg;
				var isVerified = verifyBurnedFiles();
				if (isVerified)
					msg = "Firmware image(s) burn completed Successfully.\n\n" + getResultDesc(EFileType.DSP, prevDSPVer, m_dspComm.DspVersion) + "\n" + getResultDesc(EFileType.FPGA, prevFpgaVer, m_dspComm.FpgaVersion);
				else
					msg = string.Format("Firmware image(s) burn FAIL.\n\nCurrent versions: DSP v{0}, FPGA v{1}.\n\nPlease check log file for more details.", m_dspComm.DspVersion, m_dspComm.FpgaVersion);

				syncLoggedDialog(msg, !isVerified);
            }
        }

		private void syncClose()
		{
			mainTimer.Enabled = false;

			if (InvokeRequired)
				BeginInvoke((Action)(Close));
			else
				Close();
		}

		private string getResultDesc(EFileType fileType, Files.Version prevVer, Files.Version currVer)
        {
            // Identical version
            if (prevVer.Value == currVer.Value)
            {
                if (IsForceUpgrade) 
                    return string.Format("- Re-burned {0} version v{1}.", fileType, currVer);

                return string.Format("- {0} version unchanged (v{1}).", fileType, currVer);
            }

            return string.Format("- {0} version {1} from v{2} to v{3}.", fileType, (prevVer.Value < currVer.Value) ? "Upgraded" : "DOWNGRADED", prevVer, currVer);
        }

	    private int m_wrapProgressBar = 0;
        private bool verifyBurnedFiles()
        {
            Progress = 0;
            ProgressState = "Verifying burned images...";

            var expectedDspVer = m_dspComm.DspVersion;
            if (m_dspFileHeader.IsValid)
                expectedDspVer = m_dspFileHeader.FileVer;

            var expectedFpgaVer = m_dspComm.FpgaVersion;
            if (m_fpgaFileHeader.IsValid)
                expectedFpgaVer = m_fpgaFileHeader.FileVer;

            LogMessage("Verify Firmware burned images:");
            LogMessage(" - Current versions:  DSP v{0}, FPGA v{1}", m_dspComm.DspVersion, m_dspComm.FpgaVersion);
            LogMessage(" - Expected versions: DSP v{0}, FPGA v{1}", expectedDspVer, expectedFpgaVer);

            ProgressState = "Restarting hardware...";

			m_wrapProgressBar = 1;
            m_dspComm.Restart();
			m_wrapProgressBar = 0;

			ProgressState = "Hardware started, Verifying Firmware versions...";
            m_dspComm.GetHWVersions();
            
            var isVerified = (m_dspComm.DspVersion.Value == expectedDspVer.Value && m_dspComm.FpgaVersion.Value == expectedFpgaVer.Value);
            LogMessage(" - Post restart versions: DSP v{0}, FPGA v{1} {2}", m_dspComm.DspVersion, m_dspComm.FpgaVersion, (isVerified ? "VERIFIED" : "VERSION MISMATCH"));

            ProgressState = "Hardware started. Firmware version(s) " + (isVerified ? "VERIFIED" : "MISMATCH");


            return isVerified;
        }

        private bool verifyWrappedFile(EFileType fileType, ref WrapperHeader header)
        {
            header.FileType = fileType;

            if (fileType != EFileType.DSP && fileType != EFileType.FPGA)
            {
                LogError("{0} Firmware image type is NOT supported.", fileType);
                return false;
            }

            var file = (fileType == EFileType.DSP ? DSPFile : FPGAFile);
            if (string.IsNullOrWhiteSpace(file))
                return true;

            var verified = Wrapper.GetVerifiedHeader(file, ref header);
            LogMessage("Verify {0} Firmware image file: {1}", fileType, header);
            if (!verified)
            {
                LogError("{0} Firmware image file ({1}) verification fail.", fileType, file);
                return false;
            }

            if (header.FileType != fileType)
            {
                LogError("Firmware image file type mismatch. Expected: {0} vs. Actual: {1} file.", fileType, header.FileType);
                return false;
            }

            return true;
        }

        private bool compareImageAndCurrentVersions(string imageFile, ref WrapperHeader header, EFileType fileType, ulong currentVer)
        {
            if (string.IsNullOrWhiteSpace(imageFile))
                return false;

            if (header.FileVer.Value == currentVer && !IsForceUpgrade)
            {
                logWarninge("Current {0} and Firmware image file versions are equal. Force upgrade flag is OFF. {0} burn skipped.", fileType);
                header.FileVer.Value = 0;

                return false;
            }

            if (header.FileVer.Value < currentVer && !IsEnabledDowngrade)
            {
                logWarninge("Current {0} version newer than Firmware image file version. Enabled downgrade flag is OFF. {0} burn skipped.", fileType);
                header.FileVer.Value = 0;

                return false;
            }

            return true;
        }

        private bool initCommModule()
        {
            Progress = 0;
            updateProgress(0, "Connecting....");

            LogMessage("Initiating communication module and DSP connection....");
	        m_wrapProgressBar = 1;
			m_dspComm.Start();
			m_wrapProgressBar = 0;

			if (m_dspComm.Status != DSPComm.EStatus.CONNECTED)
                return false;


            if (IsResetDSPOnStartup)
            {
				updateProgress(1, "Restarting hardware....");
				m_wrapProgressBar = 1;
				m_dspComm.Restart();
				m_wrapProgressBar = 0;

				updateProgress(1, "Get hardware version....");
			}
			else
				updateProgress(10, "Get hardware version....");

            LogMessage("Get hardware version....");
            if (!m_dspComm.GetHWVersions())
                return false;

            LogMessage("- DSP version:  v{0}.", m_dspComm.DspVersion);
            LogMessage("- FPGA version: v{0}.", m_dspComm.FpgaVersion);

            updateProgress(20, string.Format("Current versions: DSP v{0}, FPGA v{1}.", m_dspComm.DspVersion, m_dspComm.FpgaVersion));

            return true;
        }

        #region ProgressBar

        public static bool IsBlink { get; set; }
        public static string ProgressState { get; set; }
        public static int    Progress { get; set; }

        private void mainTimer_Tick(object sender, EventArgs e)
        {
			if (m_wkrThreadEnd) //Monitor worker thread
			{
				syncClose();
				return;
			}
				
			mainTimer.Enabled = false;

            updateProgress();

            mainTimer.Enabled   = true;
        }

        private delegate void UpdateProgressDelegate(int progressInc = -1, string progressState = null);

        private void updateProgress(int progressInc = -1, string progressState = null)
        {
            if (InvokeRequired)
            {
                BeginInvoke((UpdateProgressDelegate)updateProgress,progressInc, progressState);
                return;
            }

            if (progressInc >= 0)
                Progress += progressInc;

            if (progressState != null)
                ProgressState = progressState;

			if (m_wrapProgressBar > 0)
			{
				Progress += m_wrapProgressBar;
				if (Progress > progressBarMain.Maximum)
					Progress = progressBarMain.Minimum;
			}

			label1.Text = string.Format("Rimed Firmware Upgrade: {0}", ProgressState);
            if (IsBlink)
            {
                progressBarMain.Value   = progressBarMain.Maximum;
                progressBarMain.Visible = !progressBarMain.Visible;
				if (progressBarMain.Visible)
					label1.ForeColor = Color.White;
				else
					label1.ForeColor = Color.LightGreen;
			}
            else
            {
                if (Progress > progressBarMain.Maximum)
                    Progress = progressBarMain.Maximum;
                if (Progress < progressBarMain.Minimum)
                    Progress = progressBarMain.Minimum;

				label1.ForeColor = Color.LightGreen;
				progressBarMain.Value = Progress;
	            progressBarMain.Visible = true;
            }
            progressBarMain.Refresh();
        }
        #endregion
    }
}
