﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading;
using Rimed.Framework.Common;
using Rimed.Framework.Threads;

namespace Rimed.TCD.Tools.FWUpgrader
{
    public class NetStreamQThread: QThreadBase
    {
        /// <summary>= 1000</summary>
        private const int QUEUE_MAX_SIZE_DEFAULT = 1000;
        private const int QUEUE_MAX_SIZE_NONE = 0;
        private const int QUEUE_MAX_SIZE_NO_LIMIT = -1;

        /// <summary>20 * 1000 = 20 Sec</summary>
        private const int TCP_CONNECT_TIMEOUT_MSEC   = 30 * 1000;

        /// <summary>= 100 Milliseconds</summary>
        private const int TCP_DEFAULT_READ_TIMEOUT_MSEC = 150;

        /// <summary>= 100 Milliseconds</summary>
        private const int TCP_DEFAULT_WRITE_TIMEOUT_MSEC = 150;

        /// <summary>= 500 Milliseconds</summary>
        private const int UNHANDELED_EXCEPTION_SLEEP_MSEC = 500;

        /// <summary>= 5000 Milliseconds</summary>
        public const int SYNC_SEND_RECIEVE_DEFAULT_TIMEOUT_MSEC = 5000;

        
        private readonly byte[] m_buffer = new byte[DSPComm.TCP_MAX_PACKET_SIZE + 500];
        private readonly Queue<Protocol.RimedMessage> m_rxQueue = new Queue<Protocol.RimedMessage>();
        private readonly object         m_sendLocker    = new object();

        private TcpClient               m_tcpClient;

        private bool                    m_isLastSenderror;

        private int                     m_socketReadTimeout     = TCP_DEFAULT_READ_TIMEOUT_MSEC;
        private int                     m_socketWriteTimeout    = TCP_DEFAULT_WRITE_TIMEOUT_MSEC;

        private readonly SyncSendRecieveBuffer   m_syncSendRecieveBuffer = new SyncSendRecieveBuffer();

        private class SyncSendRecieveBuffer
        {
            public byte[]   SendBuffer
            {
                get { return m_sendBuffer; }
                set
                {
                    m_receiveBuffer = null;
                    m_isSent        = false;
                    m_sendBuffer    = value;
                }
            }

            public bool     IsSent
            {
                get { return m_isSent; }
                set
                {
                    m_sendBuffer    = null;
                    m_isSent        = value;
                }
            }
            public bool     IsWaitRecieve
            {
                get { return RecieveTimeoutMSec != 0; }   
            }
            public int      TimeoutSleepMSec { get; set; }
            public int      TimeoutMSec { get; set; }
            public int      RecieveTimeoutMSec { get; set; }
            public byte[]   ReceiveBuffer
            {
                get
                {
                    var buffer      = m_receiveBuffer;
                    return buffer;
                }
                set
                {
                    m_sendBuffer    = null;
                    m_receiveBuffer = value;
                }
            }

            public bool     IsRecieved      { get { return IsWaitRecieve && !IsSendPending && m_receiveBuffer != null && m_receiveBuffer.Length > 0; } }
            public bool     IsSendPending   { get { return !IsSent && m_sendBuffer != null && m_sendBuffer.Length > 0; } }

            private byte[] m_sendBuffer;
            private byte[] m_receiveBuffer;
            private bool m_isSent;

            public override string ToString()
            {
                return string.Format("SendBuffer.Size={0}, IsSent={1}, IsWaitRecieve={2}, RecieveTimeoutMSec={3}, ReceiveBuffer.Size={4}, IsRecieved={5}, IsSendPending={6}"
                    , (SendBuffer == null ?-1 : SendBuffer.Length), IsSent, IsWaitRecieve, RecieveTimeoutMSec, (ReceiveBuffer == null ? -1 : ReceiveBuffer.Length), IsRecieved, IsSendPending);
            }
        }

        public NetStreamQThread(TcpListener tcpServer, int queueMaxSize = QUEUE_MAX_SIZE_DEFAULT, int recieveTimeoutMsec = TCP_DEFAULT_READ_TIMEOUT_MSEC, int sendTimeoutMsec = TCP_DEFAULT_WRITE_TIMEOUT_MSEC)
        {
            OnEmpty += onEmpty;

            m_tcpClient  = acceptClient(tcpServer);

			m_tcpClient.NoDelay = true;

			getClientStream();

            WaitTimeout     = WAIT_TIMEOUT_NONE;
            QueueMaxSize    = queueMaxSize;
            ReadTimeout     = recieveTimeoutMsec;
            WriteTimeout    = sendTimeoutMsec;
        }


        private int     ReadTimeout
        {
            get
            {
                if (IsClientStreamEnabled)
                    return ClientStream.ReadTimeout;

                return m_socketReadTimeout;
            }
            set
            {
                if (value == 0)
                    return;

                if (value < 0)
                    value = -1;         //Timeout infinit

                m_socketReadTimeout = value;
                if (IsClientStreamEnabled)
                    ClientStream.ReadTimeout = m_socketReadTimeout;
            }
        }

        private int     WriteTimeout
        {
            get
            {
                if (IsClientStreamEnabled)
                    return ClientStream.WriteTimeout;

                return m_socketWriteTimeout;
            }
            set
            {
                if (value == 0)
                    return;

                if (value < 0)
                    value = -1;         //Timeout infinit

                m_socketWriteTimeout = value;
                if (IsClientStreamEnabled)
                    ClientStream.WriteTimeout = m_socketWriteTimeout;
            }
        }

	    private int     QueueMaxSize { get; set; }


        public new void Purge()
        {
            base.Purge();
        }

        //public bool     PostSend(byte[] buffer)
        //{
        //    if (m_isLastSenderror)
        //        return false;

        //    return PostObject(buffer);
        //}

        public bool PostSend(Protocol.IDigiLiteMessage digiLiteMsg)
        {
            if (m_isLastSenderror)
                return false;

            return PostObject(digiLiteMsg);
        }

        public byte[] Send(byte[] buffer, bool isRecieveWait = false, int timeout = SYNC_SEND_RECIEVE_DEFAULT_TIMEOUT_MSEC, int sleep = 100)
        {
            if (!IsClientStreamEnabled)
                return null;

            lock (m_sendLocker)
            {
                // NOTE: Thread synch - Set RecieveTimeoutMSec before SendBuffer to avoid send without recieve
                m_syncSendRecieveBuffer.TimeoutMSec         = timeout;
                m_syncSendRecieveBuffer.TimeoutSleepMSec    = sleep;
                m_syncSendRecieveBuffer.RecieveTimeoutMSec  = isRecieveWait ? TCP_DEFAULT_READ_TIMEOUT_MSEC : 0;      //0: NO RECIEVE

                m_syncSendRecieveBuffer.SendBuffer          = buffer;

                if (IsEmpty)
                    PostObject(null);

                if (!isRecieveWait)
                    return null;

                var timeoutExpired  = DateTime.UtcNow.AddMilliseconds(timeout);
                while (IsClientStreamEnabled && DateTime.UtcNow < timeoutExpired && !m_syncSendRecieveBuffer.IsRecieved)
                {
                    Thread.Sleep(sleep);
                }

                if (m_syncSendRecieveBuffer.IsRecieved)
                {
                    buffer = m_syncSendRecieveBuffer.ReceiveBuffer;
                    m_syncSendRecieveBuffer.ReceiveBuffer = null;

                    return buffer;
                }

                return null;
            }
        }

        /// <summary>Dequeue Rx RimedMessage. Option for message type filtering.</summary>
        /// <remarks>NOTE: If dequeued message message type mismatch requested message type dequeued message discared and null value returned.</remarks>
        /// <returns>RimedMessage / null (no pending message).</returns>
        public Protocol.RimedMessage DequeueRxMessage(Protocol.EDigiLiteMessageType? msgType = null)
        {
            if (m_rxQueue.Count == 0)
                return null;

            Protocol.RimedMessage dspMsg;
            lock (m_rxQueue)
                dspMsg = m_rxQueue.Dequeue();

            if (dspMsg == null)
                return null;

            if (msgType.HasValue && msgType.Value != dspMsg.Header.MsgType)
                return null;

            return dspMsg;
        }

        /// <summary>Pending, with timeout, for Rx RimedMessage. Option for message type filtering.</summary>
        /// <remarks>NOTE: If dequeued message message type mismatch requested message type dequeued message discared and null value returned.</remarks>
        /// <returns>RimedMessage / null (on timeout).</returns>
        public Protocol.RimedMessage WaitForRxMessage(Protocol.EDigiLiteMessageType? msgType = null, int timeoutMsec = 5000, int spinWait = 50)
        {
            MainForm.LogTrace("NetStreamQThread.WaitForRxMessage(msgType={0}, timeoutMsec={1}, spinWait={2})", msgType, timeoutMsec, spinWait);

            Protocol.RimedMessage msg = null;
            var timeout = DateTime.UtcNow.AddMilliseconds(timeoutMsec);
            while (DateTime.UtcNow < timeout)
            {
                Thread.Sleep(spinWait);
                msg = DequeueRxMessage(msgType);
                if (msg != null)
                    break;
            }

            if (msg == null)
                MainForm.LogTrace("NetStreamQThread.WaitForRxMessage(msgType={0}, ...): TIMEOUT", msgType);

            return msg;
        }


        public void     ClearRxQueue()
        {
            MainForm.LogTrace("{0}.ClearRxQueue()", Name);

            lock (m_rxQueue)
                m_rxQueue.Clear();
        }

        protected bool  IsClientEnabled
        {
            get { return m_tcpClient != null && m_tcpClient.Connected; }
        }

        public bool     IsClientStreamEnabled
        {
            get { return IsClientEnabled && ClientStream != null && ClientStream.CanWrite && ClientStream.CanRead; }
        }


        protected override void LoopIteration(object obj)
        {
            // Send sync buffer
            syncSendRecieve();

            // send queued buffer
            var msg = obj as Protocol.IDigiLiteMessage;
            send(msg);
        }

        protected override void HandleException(Exception ex)
        {
            MainForm.LogError(ex);

            Thread.Sleep(UNHANDELED_EXCEPTION_SLEEP_MSEC);
        }

        protected override void MainLoopStarting()
        {
            MainForm.LogTrace("{0}.MainLoopStarting()", Name);
            base.MainLoopStarting();
            
            getClientStream();

            if (IsClientStreamEnabled)
            {
                ClientStream.ReadTimeout    = m_socketReadTimeout;
                ClientStream.WriteTimeout   = m_socketWriteTimeout;
            }
        }

        protected override void MainLoopEnding()
        {
            MainForm.LogTrace("{0}.MainLoopEnding()", Name);

			if (ClientStream != null)
            {
				MainForm.LogTrace("{0}.MainLoopEnding(): Close TcpClientStream.", Name);
				ClientStream.Close();
	            ClientStream.TryDispose();
            }
            ClientStream = null;

			MainForm.LogTrace("{0}.MainLoopEnding(): Close TcpClient.", Name);
			m_tcpClient.Close();

            m_tcpClient.TryDispose();
            m_tcpClient = null;

            base.MainLoopEnding();
        }

        protected NetworkStream ClientStream { get; private set; }


        private TcpClient acceptClient(TcpListener tcpServer, int timeoutMsec = TCP_CONNECT_TIMEOUT_MSEC)
        {
            MainForm.LogTrace("{0}.acceptClient(...)", Name);

            if (tcpServer == null)
                throw new ArgumentNullException("tcpServer");

            TcpClient client = null;
            var timeout = DateTime.UtcNow.AddMilliseconds(timeoutMsec);
            while (timeout > DateTime.UtcNow)
            {
				MainForm.LogTrace("{0}.acceptClient(...): Check pending connection requests....", Name);
				if (tcpServer.Pending())
				{
					MainForm.LogTrace("{0}.acceptClient(...): Accept connection request.", Name);
					client = tcpServer.AcceptTcpClient();
				}

                if (client != null && client.Connected)
                    break;

                Thread.Sleep(500);
            }

            if (client != null && client.Connected)
            {
                client.ReceiveTimeout   = TCP_DEFAULT_READ_TIMEOUT_MSEC;
                client.SendTimeout      = TCP_DEFAULT_READ_TIMEOUT_MSEC;
            }
            else
            {
                throw new Exception("Connection timeout.");
            }

            return client;
        }

        private void syncSendRecieve()
        {
            if (!m_syncSendRecieveBuffer.IsSendPending)
                return;

            if (!IsClientStreamEnabled)
                return;

            try
            {
                m_syncSendRecieveBuffer.IsSent = send(m_syncSendRecieveBuffer.SendBuffer);
            }
            catch (Exception ex)
            {
                m_syncSendRecieveBuffer.SendBuffer = null;
                Logger.LogError(ex);
            }

            if (m_syncSendRecieveBuffer.IsWaitRecieve)
            {
                var timeoutExpired = DateTime.UtcNow.AddMilliseconds(m_syncSendRecieveBuffer.TimeoutMSec);
                Thread.Sleep(m_syncSendRecieveBuffer.TimeoutSleepMSec);
                while (DateTime.UtcNow < timeoutExpired && !m_syncSendRecieveBuffer.IsRecieved)
                {
                    var timeout                             = ReadTimeout;
                    ReadTimeout                             = m_syncSendRecieveBuffer.RecieveTimeoutMSec;

                    var dspMsg                              = recieveDspMessage();
                    m_syncSendRecieveBuffer.ReceiveBuffer   = ((dspMsg == null) ? null : dspMsg.GetMessageBuffer());

                    ReadTimeout                             = timeout;
                }
            }

        }

        private void getClientStream()
        {
            MainForm.LogTrace("{0}.getClientStream()", Name);

            if (IsClientEnabled && ClientStream == null)
                ClientStream = m_tcpClient.GetStream();
        }


        private bool send(byte[] buffer)
        {
            MainForm.LogTrace("{0}.send(byte[])", Name);

            if (buffer == null || buffer.Length == 0)
                return false;

            if (!IsClientStreamEnabled)
                return false;

            MainForm.LogTrace("Send(...): Buffer.Len={0}, Buffer: 0x{1}....", buffer.Length, BitConverter.ToString(buffer, 0, Protocol.RimedMessageHeader.Size).Replace("-", ""));
            try
            {
                ClientStream.Write(buffer, 0, buffer.Length);
                ClientStream.Flush();

                m_isLastSenderror = false;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "ClientStream write error.");
                m_isLastSenderror = true;
            }

            return true;
        }

        private bool send(Protocol.IDigiLiteMessage msg)
        {
            MainForm.LogTrace("{0}.send(IDigiLiteMessage)", Name);

            if (msg == null)
                return false;

            MainForm.LogTrace("NetStreamQThread.send(msg={0})", msg);
            return send(msg.GetMessageBuffer());
        }

        private Protocol.RimedMessage recieveDspMessage()
        {
            if (!IsClientStreamEnabled || !ClientStream.DataAvailable)
                return null;

            var rxCount = ClientStream.Read(m_buffer, 0, m_buffer.Length);
            if (rxCount <= 0)
                return null;

            if (rxCount < Protocol.RimedMessageHeader.Size)
            {
                MainForm.LogTrace("NetStreamQThread.recieveDspMessage(): ABORT Buffer size ({0}) < Header size ({1})", rxCount, Protocol.RimedMessageHeader.Size);
                return null;
            }

            var msg = new Protocol.RimedMessage(m_buffer);
            MainForm.LogTrace("NetStreamQThread.recieveDspMessage(): Message.Header={0}", msg.Header);

            return msg;
        }

        private bool onEmpty()
        {
            Protocol.RimedMessage dspMsg;
            do
            {
                // Send sync buffer
                syncSendRecieve();

                // Recieve data
                dspMsg = recieveDspMessage();
                if (dspMsg != null && QueueMaxSize != QUEUE_MAX_SIZE_NONE)
                {
                    // Queue recieved data
                    lock (m_rxQueue)
                    {
                        m_rxQueue.Enqueue(dspMsg);
                        if (QueueMaxSize != QUEUE_MAX_SIZE_NO_LIMIT && m_rxQueue.Count > QueueMaxSize)
                            m_rxQueue.Dequeue();
                    }
                }
            } while (dspMsg != null && IsEmpty && IsClientStreamEnabled);

            return true;
        }
    }
}
