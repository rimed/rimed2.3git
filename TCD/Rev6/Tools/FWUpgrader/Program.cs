﻿using System;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using Rimed.Framework.Common;
using Rimed.Framework.Threads;

namespace Rimed.TCD.Tools.FWUpgrader
{
    class Program
    {
        /// <summary>The main entry point for the application.</summary>
        [STAThread]
        static void Main(string[] args)
        {
            Thread.CurrentThread.SetName(MainForm.APP_TITLE);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var initPath = Environment.CurrentDirectory;// Directory.GetCurrentDirectory();
            var appParh = Files.GetFilePath(Assembly.GetExecutingAssembly().Location);
            Environment.CurrentDirectory = appParh;

            LoggedDialog.BaseCaption            = MainForm.APP_TITLE;
            LoggedDialog.InformationCaption     = string.Empty;
            LoggedDialog.NotificationCaption    = string.Empty;
            LoggedDialog.QuestionCaption        = string.Empty;
            LoggedDialog.ConfirmationCaption    = string.Empty;

            Logger.LogInfo("");
            Logger.LogInfo("-----------------------------------------------------------------------------------------------------------------------");
            Logger.LogInfo("                                    - " + MainForm.APP_TITLE + " -");
            Logger.LogInfo("-----------------------------------------------------------------------------------------------------------------------");
            Logger.LogInfo("Working directory: {0}", Environment.CurrentDirectory);

            var mainForm = new MainForm();
            MainForm.ProgressState = string.Empty;
            mainForm.Show();
           
            if (!mainForm.IsFilesProvided)
            {
                Logger.LogWarning("No image files provided.");
            }
            else 
            {
                if (!mainForm.Init())
                {
                    MainForm.ProgressState = "Initialization error.";
                    LoggedDialog.ShowError(string.Format("{0} initiation fail.\n\nPlease check log file for more details.", MainForm.APP_TITLE));
                }
                else
                {
                    mainForm.Start();
                    Application.Run(mainForm);
                }
            }

            Environment.CurrentDirectory = initPath;

            Logger.LogInfo("");
            Logger.LogInfo("Application is down.");
        }
    }
}
