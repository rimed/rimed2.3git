using namespace System;
using namespace System::Reflection;
using namespace System::Runtime::CompilerServices;
using namespace System::Runtime::InteropServices;
using namespace System::Security::Permissions;

//
// General Information about the solution
//
[assembly:AssemblyCompanyAttribute("Rimed ltd.")];
[assembly:AssemblyProductAttribute("Rimed TCD Rev6")];
[assembly:AssemblyCopyrightAttribute("Copyright (c) 2016")];
[assembly:AssemblyTrademarkAttribute("")];
[assembly:AssemblyCultureAttribute("")];

//
// 6Version information for an assembly consists of the following four values: Major.Minor.Build.Revision
//
[assembly:AssemblyFileVersionAttribute("2.2.5.2")];

// For AssemblyVersionAttribute specify all or default the Revision and Build Numbers by using the '*'.
[assembly:AssemblyVersionAttribute("2.2.*")];
