using System;

namespace Rimed.TCD.ReportMgr
{
	public sealed class ReportResourceManager
	{
		private string m_languageResourceFileName ;
		public string LanguageResourceFileName
		{
			get{return m_languageResourceFileName;}
		}

		static private readonly ReportResourceManager m_ReportResourceManagerInstance = new ReportResourceManager();
		
		static public ReportResourceManager theResourceManager
		{ 
			get
			{
				return m_ReportResourceManagerInstance;
			} 
		}
		
		public ReportResourceManager()
		{
			SetLangugeResourceFileName();
		}

		public void SetLangugeResourceFileName()
		{
			// Read the Language from the configuration file
			System.Configuration.AppSettingsReader reader = new System.Configuration.AppSettingsReader();
			
			var language = (string)reader.GetValue("Language",typeof(string));
			switch ( language )
			{
				case "English":
					m_languageResourceFileName =  ".ReportResource";
					break;

				case "Chinese":
					m_languageResourceFileName = ".ReportResourceChinese";
					break;

                case "Russian":
                    m_languageResourceFileName = ".ReportResourceRussian";
                    break;

                case "Spanish":
                    m_languageResourceFileName = ".ReportResourceSpanish";
                    break;
                default:
					m_languageResourceFileName = ".ReportResource";
					break;
			}
		}
	}
}
