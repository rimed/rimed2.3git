using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using Rimed.Framework.Common;
using Rimed.TCD.Utils;

namespace Rimed.TCD.ReportMgr
{
    public partial class IPImagesListDlg : Form
    {
        #region Private Fields

        private List<string> m_files = new List<string>();
        private const string IP_DEFAULT_PATH = @"C:\Echo Images";
        private System.Resources.ResourceManager strRes;

        #endregion Private Fields

        #region Constructors

        public IPImagesListDlg()
        {
            InitializeComponent();
            strRes = new System.Resources.ResourceManager(
                GetType().Namespace + ReportResourceManager.theResourceManager.LanguageResourceFileName, GetType().Assembly);
        }

        public IPImagesListDlg(string ordFileName)
            : this()
        {
            if (File.Exists(ordFileName))
            {
                StreamReader file = new StreamReader(ordFileName);
                while (file.Peek() != -1)
                    this.m_files.Add(file.ReadLine());
                this.FillListCtrl();
            }
        }

        #endregion Constructors

        public List<string> ImageList
        {
            get
            {
                return m_files;
            }
        }

        #region Event Handlers

        private void IPImagesListDlg_Load(object sender, EventArgs e)
        {
            this.btnSave.Text = strRes.GetString("Save");
            this.btnCancel.Text = strRes.GetString("Cancel");
            this.lblCaption.Text = strRes.GetString("Manage DigiLite IP Images");
        }

        private void bAdd_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("bAdd", Name);

            if (Directory.Exists(IP_DEFAULT_PATH))
            {
                this.openFileDialog1.InitialDirectory = IP_DEFAULT_PATH;
                if (this.openFileDialog1.ShowDialog(this) == DialogResult.OK)
                {
                    int number = 12 - m_files.Count; //TODO: "12" can be setting
                    number = (number > this.openFileDialog1.FileNames.Length ? this.openFileDialog1.FileNames.Length : number);
                    for (int i = 0; i < number; i++)
                    {
                        if (!this.m_files.Contains(this.openFileDialog1.FileNames[i]))
                        {
                            this.m_files.Add(this.openFileDialog1.FileNames[i]);
                            this.FillListCtrl();
                            this.btnSave.Enabled = true;
                        }
                        else
							LoggedDialog.Show(this, strRes.GetString("Selected IP image has already been added"));
                    }
                }
                this.enableButtons();
            }
            else
				LoggedDialog.Show(this, strRes.GetString("You didn't purchase the DigiLite IP software yet."));
        }

        protected void FillListCtrl()
        {
            if (listBoxImages.Items.Count != 0)
                listBoxImages.Items.Clear();

            for (int i = 0; i < m_files.Count; i++)
            {
                listBoxImages.Items.Add(String.Format("{0}. {1}", i + 1, m_files[i]));
            }
        }

        private void enableButtons()
        {
            btnEdit.Enabled = false;
            btnRemove.Enabled = false;
            btnMoveUp.Enabled = false;
            btnMoveDown.Enabled = false;
        }

        private void bEdit_Click(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick("bEdit", Name);

            if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
            {
                var item = listBoxImages.SelectedItem.ToString();
                item = item.Substring(item.IndexOf(' ') + 1);
                int i = m_files.IndexOf(item);
                if (!m_files.Contains(openFileDialog1.FileName))
                {
                    m_files[i] = openFileDialog1.FileName;
                    FillListCtrl();
                }
                else
					LoggedDialog.Show(this, strRes.GetString("Selected IP image has already been added"));
            }
            enableButtons();
        }

        private void bRemove_Click(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick("bRemove", Name);

            var item = listBoxImages.SelectedItem.ToString();
            item = item.Substring(item.IndexOf(' ') + 1);
            m_files.Remove(item);
            FillListCtrl();
            enableButtons();
        }

        private void listBoxImages_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
	        if (listBoxImages.SelectedItem == null)
		        return;

			btnEdit.Enabled = true;
	        btnRemove.Enabled = true;
	        if (listBoxImages.SelectedIndex > 0)
		        btnMoveUp.Enabled = true;

	        if (listBoxImages.SelectedIndex < listBoxImages.Items.Count - 1)
		        btnMoveDown.Enabled = true;
        }

        private void listBoxImages_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (listBoxImages.SelectedIndex > 0)
                btnMoveUp.Enabled = true;
            else
                btnMoveUp.Enabled = false;

            if (listBoxImages.SelectedIndex < listBoxImages.Items.Count - 1)
                btnMoveDown.Enabled = true;
            else
                btnMoveDown.Enabled = false;
        }

        private void bMoveUp_Click(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick("bMoveUp", Name);

            var i = listBoxImages.SelectedIndex;
            var item = m_files[i - 1];
            m_files[i - 1] = m_files[i];
            m_files[i] = item;
            FillListCtrl();
            listBoxImages.SelectedIndex = i - 1;
        }

        private void bMoveDown_Click(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick("bMoveDown", Name);

            var i = listBoxImages.SelectedIndex;
            var item = m_files[i + 1];
            m_files[i + 1] = m_files[i];
            m_files[i] = item;
            FillListCtrl();
            listBoxImages.SelectedIndex = i + 1;
        }

        #endregion Event Handlers
    }
}
