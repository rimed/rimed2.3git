﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Rimed.TCD.ReportMgr {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class ReportResourceSpanish {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ReportResourceSpanish() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Rimed.TCD.ReportMgr.ReportResourceSpanish", typeof(ReportResourceSpanish).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancelar.
        /// </summary>
        internal static string Cancel {
            get {
                return ResourceManager.GetString("Cancel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Exportación DICOM se completó con éxito!.
        /// </summary>
        internal static string DICOM_Export_was_successfully_completed_ {
            get {
                return ResourceManager.GetString("DICOM Export was successfully completed!", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Transferencia DICOM falló:.
        /// </summary>
        internal static string DICOM_transfer_failed_ {
            get {
                return ResourceManager.GetString("DICOM transfer failed:", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Administrar Imágenes de DigiLite IP.
        /// </summary>
        internal static string Manage_DigiLite_IP_Images {
            get {
                return ResourceManager.GetString("Manage DigiLite IP Images", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Se guardó el informe .
        /// </summary>
        internal static string Report_was_saved {
            get {
                return ResourceManager.GetString("Report was saved", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Guardar.
        /// </summary>
        internal static string Save {
            get {
                return ResourceManager.GetString("Save", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Imagen IP seleccionada ya se ha añadido.
        /// </summary>
        internal static string Selected_IP_image_has_already_been_added {
            get {
                return ResourceManager.GetString("Selected IP image has already been added", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Configurar valores para exportación DITCOM: servidor, puerto, Título AE, Título AE local.
        /// </summary>
        internal static string Set_values_for_DICOM_export__host__port__AE_Title__local_AE_title {
            get {
                return ResourceManager.GetString("Set values for DICOM export: host, port, AE Title, local AE title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Algunos archivos no se enviaron. ¿Quiere intentarlo otra vez?.
        /// </summary>
        internal static string Some_files_were_not_sent__Do_you_want_to_try_again_ {
            get {
                return ResourceManager.GetString("Some files were not sent. Do you want to try again?", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Aún no compraste el software de DigiLite IP.
        /// </summary>
        internal static string You_didn_t_purchase_the_DigiLite_IP_software_yet_ {
            get {
                return ResourceManager.GetString("You didn\'t purchase the DigiLite IP software yet.", resourceCulture);
            }
        }
    }
}
