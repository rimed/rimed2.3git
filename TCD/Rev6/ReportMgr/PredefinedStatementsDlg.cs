﻿using System;
using System.Drawing;
using System.Windows.Forms;
using Rimed.Framework.Common;
using Rimed.Framework.GUI;
using Rimed.TCD.DAL;

namespace Rimed.TCD.ReportMgr
{
	public partial class PredefinedStatementsDlg : Form
	{
		#region static members
		private const string FORM_CAPTION_FMT = "Select statement for {0}";

		public static string GetStatment(Form owner, int categoryId, string caption)
		{
			var statement = string.Empty;

			using (var dlg = new PredefinedStatementsDlg(categoryId, caption))
			{
				dlg.Text = string.Format(FORM_CAPTION_FMT, caption);
				var res = dlg.ShowDialog(owner);

				if (dlg.isChangesExists())
				{
					if (LoggedDialog.ShowQuestion(owner, "There are unsaved changes.\n\nSave unsaved changes?") == DialogResult.Yes)
						dlg.saveAllChanges();
				}

				if (res == DialogResult.OK && dlg.IsSelected)
					statement = dlg.Statement;
			}

			return statement;
		}
		#endregion

		#region Helper class
		private class StatementData
		{
			public enum EStatus
			{
				NONE,
				Loaded,
				New,
				Modified,
				Deleted,
			}
			public StatementData(string alias, string statement, EStatus status)
			{
				Alias				= alias;
				Statement			= statement;
				Status				= status;
				OriginalStatement	= statement;
			}

			public string Alias { get; private set; }
			public string Statement { get; set; }
			public EStatus Status { get; set; }
			public bool IsStatementModified
			{
				get
				{
					return (string.Compare(OriginalStatement, Statement, StringComparison.Ordinal) != 0);
				}
			}
			public string OriginalStatement { get; private set; }

			public void StatementSaved()
			{
				OriginalStatement = Statement;
				Status = EStatus.Loaded;
			}

			public override string ToString()
			{
				if (Status == EStatus.Loaded)
					return Alias;

				return string.Format("[{0}] {1}", Status, Alias);
			}

		}
		#endregion

		#region Instance members
		private readonly TableWrapperPredefinedStatements m_tableWrapperPredefinedStatements = new TableWrapperPredefinedStatements();
		private int m_categoryId = -1;
		private readonly string m_category = string.Empty;


		private PredefinedStatementsDlg(int categoryId, string category)
		{
			InitializeComponent();

			m_categoryId = categoryId;
			m_category	 = category;
			
			loadCategory();
		}

		private void PredefinedStatementsDlg_Load(object sender, EventArgs e)
		{
			Size		= new Size(Size.Width, buttonSave.Top + Height - buttonCancel.Top);
			MinimumSize = Size;
		}

		private void buttonSelect_Click(object sender, EventArgs e)
		{
			Statement = textBoxStatement.Text;
			DialogResult = DialogResult.OK;
		}

		private void buttonCancel_Click(object sender, EventArgs e)
		{
			Statement = null;
			DialogResult = DialogResult.Cancel;
		}


		private void buttonNew_Click(object sender, EventArgs e)
		{
			var alias = InputDialog.GetUserInput(this, string.Format("Add predefined {0} statement", m_category), "Write statement alias");
			if (string.IsNullOrWhiteSpace(alias))
				return;

			foreach (var item in comboBoxAlias.Items)
			{
				var statement = item as StatementData;
				if (statement == null)
					continue;

				if (string.Compare(statement.Alias, alias, StringComparison.InvariantCultureIgnoreCase) == 0)
				{
					LoggedDialog.ShowNotification(this, string.Format("'{0}'\n{1} alias already exists. Duplicate aliases are not supported.", alias, m_category));
					return;
				}
			}

			var newStatement = new StatementData(alias, string.Empty, StatementData.EStatus.New);
			var ix = comboBoxAlias.Items.Add(newStatement);
			comboBoxAliasSetSelectedIndex(ix);			
			buttonEdit_Click(this, null);
		}

		private void buttonEdit_Click(object sender, EventArgs e)
		{
			textBoxStatement.ReadOnly = false;
			textBoxStatement.Focus();
			buttonEdit.Enabled = false;
		}

		private void buttonDelete_Click(object sender, EventArgs e)
		{
			var statement = comboBoxAlias.SelectedItem as StatementData;
			if (statement == null)
				return;

			if (statement.Status == StatementData.EStatus.Deleted)
				return;


			if (statement.Status == StatementData.EStatus.New)
				updateStatement(statement);
			else
				updateStatement(statement, false, StatementData.EStatus.Deleted);
		}

		private void buttonUndo_Click(object sender, EventArgs e)
		{
			var statement = comboBoxAlias.SelectedItem as StatementData;
			undoStatement(statement);
		}

		private void buttonSave_Click(object sender, EventArgs e)
		{
			var statement = comboBoxAlias.SelectedItem as StatementData;
			saveStatement(statement);
		}


		private void comboBoxAlias_SelectedIndexChanged(object sender, EventArgs e)
		{
			var statement			= comboBoxAlias.SelectedItem as StatementData;
			textBoxStatement.Text	= (statement == null) ? string.Empty : statement.Statement;
			enableButtons();
		}

		private void textBoxStatement_Leave(object sender, EventArgs e)
		{
			textBoxStatement.ReadOnly = true;
			var statement = comboBoxAlias.SelectedItem as StatementData;
			if (statement == null)
				return;

			statement.Statement = textBoxStatement.Text;
			if (statement.IsStatementModified && statement.Status == StatementData.EStatus.Loaded)
				updateStatement(statement, false, StatementData.EStatus.Modified);
			else
				enableButtons();
		}



		private string Statement { get; set; }
		private bool IsSelected { get { return !string.IsNullOrWhiteSpace(Statement); } }

		private void enableButtons(StatementData statement = null)
		{
			comboBoxAlias.Focus();

			var isTextExists	= false;
			var isNotLoaded		= false;
			var isNotdeleted	= false;
			var isModified		= false;

			if (statement == null)
				statement = comboBoxAlias.SelectedItem as StatementData;

			if (statement != null)
			{
				isTextExists	= (! string.IsNullOrWhiteSpace(statement.Statement));
				isNotLoaded		= (statement.Status != StatementData.EStatus.Loaded);
				isNotdeleted	= (statement.Status != StatementData.EStatus.Deleted);
				isModified		= isNotLoaded;
				if (statement.Status == StatementData.EStatus.New && !statement.IsStatementModified)
					isModified = false;
			}

			buttonSelect.Enabled	= isTextExists;
			buttonSave.Enabled		= (isTextExists && isNotLoaded);
			buttonUndo.Enabled		= isModified;
			buttonDelete.Enabled	= isNotdeleted;
			buttonEdit.Enabled		= isNotdeleted;
		}

		private bool isChangesExists()
		{
			foreach (var item in comboBoxAlias.Items)
			{
				var statement = item as StatementData;
				if (statement == null || string.IsNullOrWhiteSpace(statement.Statement))
					continue;

				if (statement.Status != StatementData.EStatus.Loaded)
					return true;
			}

			return false;
		}

		private void saveAllChanges()
		{
			if (comboBoxAlias.Items.Count == 0)
				return;

			var items = new object[comboBoxAlias.Items.Count];
			comboBoxAlias.Items.CopyTo(items, 0);
			foreach (var item in items)
			{
				var statement = item as StatementData;
				saveStatement(statement);
			}
		}

		private bool saveStatement(StatementData statement)
		{
			if (!saveDBRow(statement))
				return false;

			if (statement.Status == StatementData.EStatus.Deleted)
				updateStatement(statement);
			else
				updateStatement(statement, false, StatementData.EStatus.Loaded);

			statement.StatementSaved();

			return true;
		}

		private void comboBoxAliasSetSelectedIndex(int index = 0)
		{
			if (index >= 0 && comboBoxAlias.Items.Count > index)
			{
				comboBoxAlias.SelectedIndex = index;
			}
			else if (comboBoxAlias.Items.Count > 0)
			{
				comboBoxAlias.SelectedIndex = 0;
			}
			else
			{
				comboBoxAlias.SelectedIndex = -1;
				comboBoxAlias_SelectedIndexChanged(this, null);
			}
		}

		private void undoStatement(StatementData statement)
		{
			if (statement == null)
				return;

			if (statement.Status == StatementData.EStatus.Loaded)
				return;

			switch (statement.Status)
			{
				case StatementData.EStatus.Modified:
				case StatementData.EStatus.Deleted:
						updateStatement(statement, false, StatementData.EStatus.Loaded, statement.OriginalStatement);
					break;
				case StatementData.EStatus.New:
						statement.Statement = statement.OriginalStatement;
						comboBoxAlias_SelectedIndexChanged(this, null);
					break;
				default:
					break;
			}
		}

		private void updateStatement(StatementData statement, bool isRemove = true, StatementData.EStatus newStatus = StatementData.EStatus.NONE, string newStatment = null)
		{
			if (statement == null)
				return;

			comboBoxAlias.Items.Remove(statement);
			if (isRemove)
			{
				comboBoxAliasSetSelectedIndex();
				return;
			}

			if (string.IsNullOrWhiteSpace(newStatment))
				newStatment = statement.Statement;

			if (newStatus == StatementData.EStatus.NONE)
				newStatus = statement.Status;

			statement.Statement = newStatment;
			statement.Status	= newStatus;
			comboBoxAlias.Items.Add(statement);		
			var ix = comboBoxAlias.Items.IndexOf(statement);
			comboBoxAliasSetSelectedIndex(ix);
		}

		private bool saveDBRow(StatementData statement)
		{
			if (statement == null || string.IsNullOrWhiteSpace(statement.Statement))
				return false;

			if (statement.Status == StatementData.EStatus.New)
				return m_tableWrapperPredefinedStatements.AddRow(statement.Statement, statement.Alias, m_categoryId);

			if (statement.Status == StatementData.EStatus.Modified)
				return m_tableWrapperPredefinedStatements.UpdateRow(statement.Statement, statement.Alias, m_categoryId);

			if (statement.Status == StatementData.EStatus.Deleted)
				return m_tableWrapperPredefinedStatements.deleteRow(statement.Alias, m_categoryId);

			return false;
		}

		private void loadCategory(int categoryId = -1)
		{
			comboBoxAlias.SelectedIndex = -1;
			if (categoryId > -1)
				m_categoryId = categoryId;

			var rows = m_tableWrapperPredefinedStatements.GetRows(m_categoryId);
			if (rows == null)
				return;

			comboBoxAlias.SuspendLayout();
			comboBoxAlias.Items.Clear();
			foreach (var row in rows)
			{
				var statement = new StatementData(row.Alias, row.Statement, StatementData.EStatus.Loaded);
				comboBoxAlias.Items.Add(statement);
			}
			comboBoxAlias.ResumeLayout();

			comboBoxAlias.SelectedIndex = 0;
		}
		#endregion //Instance members

		private void textBoxStatement_KeyUp(object sender, KeyEventArgs e)
		{
			var statTxt = textBoxStatement.Text;
			if (string.IsNullOrWhiteSpace(statTxt))
			{
				buttonSave.Enabled = false;
				buttonUndo.Enabled = false;
				return;
			}

			var statement = comboBoxAlias.SelectedItem as StatementData;
			if (statement == null)
				return;

			var isEnabled		= (string.Compare(statement.OriginalStatement, statTxt, StringComparison.Ordinal) != 0);
			buttonUndo.Enabled	= isEnabled;
			buttonSave.Enabled	= isEnabled;
		}
	}
}
