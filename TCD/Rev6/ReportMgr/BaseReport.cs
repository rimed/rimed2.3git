using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Data;
using System.IO;
using System.Windows.Forms;
using CrystalDecisions.Shared;
using Rimed.TCD.DAL;
using Rimed.TCD.Utils;
using Rimed.Framework.Common;
using CrystalDecisions.CrystalReports.Engine;

namespace Rimed.TCD.ReportMgr
{
    public class BaseReport
    {
        //TODO, Ofer: Use single image format for resource images => Convert all (non-animated) images to the same format (JPG, BMP, GIF, or PNG)

		protected const int PAPER_SIZE_HIGHT		= 15360;
		protected const int PAPER_MARGINS_TOP		= 360;
		protected const int PAPER_MARGINS_BOTTOM	= 360;
        //Added by Alex bug #724 v.2.2.3.19 11/01/2017 
        protected const int ROW_EXAM_IMAGE_HEIGHT   = 1680;

        private string m_currentPatientId;

        protected DataSet												Ds				= null;
        protected Guid													ExamIndex		= Guid.Empty;
        protected string												StudyName		= Constants.StudyType.STUDY_INTERCRANIAL_UNILATERAL;
		protected ReportViewer											ReportViewer	= null;
		protected CrystalDecisions.CrystalReports.Engine.ReportClass	Report;
		protected readonly dsExamination.tb_ExaminationRow				ExamRow			= null;

        protected ResourceManagerWrapper            StringManager { get; private set; }

        #region Ctos
        /// <summary>Constructor</summary>
        protected BaseReport()
        {
            var recource    = new System.Resources.ResourceManager(GetType().Namespace + ReportResourceManager.theResourceManager.LanguageResourceFileName, GetType().Assembly);
            StringManager   = ResourceManagerWrapper.GetWrapper(recource);
        }

        /// <summary>Constructor</summary>
        protected BaseReport(Guid examIndex) : this()
        {
            ExamIndex   = examIndex;
            ExamRow 	= RimedDal.Instance.GetExaminationByIndex(examIndex);
            StudyName   = RimedDal.Instance.GetStudyName(ExamRow.Study_Index);	
        }
        #endregion

        /// <summary>Create report layout and catch it till application run It fixes RIMD-471</summary>
        public static void Initialize()
        {
            // Initializing Crystal Reports
            Logger.SystemInfoLog("Initializing CrestalReport engine...");

            using (var rpt = new crPatientRep())
            {
                rpt.Load();
                rpt.Close();
            }

            Logger.SystemInfoLog("CrestalReport engine initialized.");
        }

	    private static string ClassName { get { return "BaseReport"; } }

		public virtual void Display(bool preview)
		{
			ReportViewer = new ReportViewer(ExamIndex, GetReportTitle());
			ReportViewer.AddText2RepEvent += new AddText2RepDelegate(RepViewerInstance_AddText2RepEvent);
			ReportViewer.AddIpImages2RepEvent += new AddIpImages2RepDelegate(ReportViewer_AddIpImages2RepEvent);
			ReportViewer.SaveIpImagesEvent += new SaveIpImagesDelegate(ReportViewer_SaveIpImagesEvent);

			if (preview)
			{
				ReportViewer.CurrentPatientID = m_currentPatientId;
				ReportViewer.PatientReportClass = Report;
				ReportViewer.TopMost = true;
				ReportViewer.ShowDialog();
				ReportViewer.AddText2RepEvent -= new AddText2RepDelegate(RepViewerInstance_AddText2RepEvent);
				ReportViewer.AddIpImages2RepEvent -= new AddIpImages2RepDelegate(ReportViewer_AddIpImages2RepEvent);
				ReportViewer.SaveIpImagesEvent -= new SaveIpImagesDelegate(ReportViewer_SaveIpImagesEvent);
			}
			else
			{
				try
				{
					Report.PrintToPrinter(1, true, 0, 0);
				}
				catch (Exception ex)    // Nadiya, RIMD-442: in new env exception System.Drawing.Printing.InvalidPrinterException is thrown.
				{
					Logger.LogError(ex);
					LoggedDialog.ShowInformation("Printer was not found!");
				}
			}
		}

		protected string GetReportTitle()	//CR #421
		{
			var name = GetStudyName(StudyName);

			return string.Format("TCD Examination: {0}", name);
		}

		protected string GetStudyName(string name)	//CR #421
		{
			if (string.IsNullOrWhiteSpace(name))
				return string.Empty;

			if (string.Compare(Constants.StudyType.STUDY_INTERCRANIAL_UNILATERAL, name.Trim(), StringComparison.InvariantCultureIgnoreCase) == 0)
				return Constants.StudyType.STUDY_INTERCRANIAL_BILATERAL;

			return name;
		}


	    protected virtual void UpdateGeneralData(DataView dvHospital, DataView dvPatient)
        {
            Ds.Tables["tb_GenralInfo"].Clear();
            Ds.Tables["tb_Patient"].Clear();
            Ds.Tables["tb_HospitalDetails"].Clear();

            // copy hospital details.
            DataRow hosNewRow = Ds.Tables["tb_HospitalDetails"].NewRow();
            hosNewRow.ItemArray = dvHospital[0].Row.ItemArray;
            Ds.Tables["tb_HospitalDetails"].Rows.Add(hosNewRow);

            //Assaf fix (22/11/06) - Adding the data for the SummaryReport (Printing from summaryScreen)
            // add one row to tb_GenralInfo.
            DataRow genInfoNewRow = Ds.Tables["tb_GenralInfo"].NewRow();

            genInfoNewRow["Indication"] = ExamRow["Indication"];
            genInfoNewRow["Interpretation"] = (string)ExamRow["Interpretation"];

			genInfoNewRow["ReportTitle"] = GetReportTitle();		//CR #421

            genInfoNewRow["ExamDate"] = ((DateTime)ExamRow["Date"]).ToShortDateString();

            // generate time string
            var time = ((((int)ExamRow["TimeHours"]) < 10) ? "0" : "") + ((int)ExamRow["TimeHours"]) + ":" + ((((int)ExamRow["TimeMinutes"]) < 10) ? "0" : "") + ((int)ExamRow["TimeMinutes"]);

            genInfoNewRow["ExamTime"] = time;

            genInfoNewRow["Notes"] = RimedDal.Instance.ExaminationsDS.tb_Examination[0].Notes;
            Ds.Tables["tb_GenralInfo"].Rows.Add(genInfoNewRow);

            // copy the current patient.
            DataRow patientNewRow = Ds.Tables["tb_Patient"].NewRow();

            patientNewRow["Patient_Index"] = dvPatient[0]["Patient_Index"];
            patientNewRow["First_Name"] = dvPatient[0]["First_Name"];
            patientNewRow["Middle_Name"] = dvPatient[0]["Middle_Name"];
            patientNewRow["Last_Name"] = dvPatient[0]["Last_Name"];
            patientNewRow["Deleted"] = dvPatient[0]["Deleted"];
            patientNewRow["ID"] = dvPatient[0]["ID"];
            patientNewRow["Sex"] = dvPatient[0]["Sex"];
            patientNewRow["Birth_date"] = dvPatient[0]["Birth_date"];
            patientNewRow["Age"] = dvPatient[0]["Age"];
            patientNewRow["Home_Phone"] = dvPatient[0]["Home_Phone"];
            patientNewRow["Cellular_Phone"] = dvPatient[0]["Cellular_Phone"];
            patientNewRow["Work_Phone"] = dvPatient[0]["Work_Phone"];
            patientNewRow["Address"] = dvPatient[0]["Address"];
            patientNewRow["Reason_For_Examination"] = dvPatient[0]["Reason_For_Examination"];
            patientNewRow["RefferedBy"] = dvPatient[0]["RefferedBy"];
            patientNewRow["Email"] = dvPatient[0]["Email"];
            patientNewRow["Alive"] = dvPatient[0]["Alive"];
            patientNewRow["Smoking"] = (((bool)dvPatient[0]["Smoking"])) ? "Yes" : "No";
            patientNewRow["Hypertension"] = (((bool)dvPatient[0]["Hypertension"])) ? "Yes" : "No";
            patientNewRow["Symptomatic_Carotid_Stenosis"] = (((bool)dvPatient[0]["Symptomatic_Carotid_Stenosis"])) ? "Yes" : "No";
            patientNewRow["Asymptomatic_Carotid_Stenosis"] = (((bool)dvPatient[0]["Asymptomatic_Carotid_Stenosis"])) ? "Yes" : "No";
            patientNewRow["TIA"] = (((bool)dvPatient[0]["TIA"])) ? "Yes" : "No";
            patientNewRow["CVA"] = (((bool)dvPatient[0]["CVA"])) ? "Yes" : "No";
            patientNewRow["Atrial_Fibrilation"] = (((bool)dvPatient[0]["Atrial_Fibrilation"])) ? "Yes" : "No";
            patientNewRow["Prosthetic_Heart_Valves"] = (((bool)dvPatient[0]["Prosthetic_Heart_Valves"])) ? "Yes" : "No";
            patientNewRow["Coronary_By_Pass"] = (((bool)dvPatient[0]["Coronary_By_Pass"])) ? "Yes" : "No";
            patientNewRow["Arrithymia"] = (((bool)dvPatient[0]["Arrithymia"])) ? "Yes" : "No";
            patientNewRow["Title"] = dvPatient[0]["Title"];

            Ds.Tables["tb_Patient"].Rows.Add(patientNewRow);
            m_currentPatientId = dvPatient[0]["Patient_Index"].ToString();
        }

        protected virtual void RepViewerInstance_AddText2RepEvent(string field, string text)
        {
            // do nothing in base.
        }

        protected virtual void ReportViewer_AddIpImages2RepEvent(List<string> files)
        {
            // do nothing in base
        }

        protected virtual void ReportViewer_SaveIpImagesEvent()
        {
            // do nothing in base
        }

        protected byte[] GetReportImage(string fileName, int width, int height)
        {
            if (string.IsNullOrWhiteSpace(fileName))
            {
                Logger.LogError(string.Format("{0}.GetReportImage(...): FileName cannot be null or empty.", ClassName));
                return new byte[0];
            }

            if (!File.Exists(fileName))
            {
                Logger.LogError(string.Format("{0}.GetReportImage(fileName='{1}'): File not found.", ClassName, fileName));
                return new byte[0];
            }

            using (var img = System.Drawing.Image.FromFile(fileName))
            {
                return getReportImage(img, width, height);
            }
        }

        protected byte[] GetReportImage(MemoryStream stream, int width, int height)
        {
            if (stream == null)
            {
                Logger.LogError(string.Format("{0}.GetReportImage(...): MemoryStream cannot be null.", ClassName));
                return new byte[0];
            }

            using (var img = System.Drawing.Image.FromStream(stream))
            {
                return getReportImage(img, width, height);
            }
        }

        private static byte[] getReportImage(System.Drawing.Image img, int width, int height)
        {
            using (var memStream = new MemoryStream())
            {
                if (img.Width > width && img.Height > height)
                {
                    using (var img2 = img.GetThumbnailImage(width, height, null, IntPtr.Zero))
                    {
                        img2.Save(memStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                    }
                }
                else
                {
                    img.Save(memStream, System.Drawing.Imaging.ImageFormat.Jpeg);
                }

                memStream.Position = 0;

                return memStream.ToArray();
            }
        }
    }

    public class LayoutReport : BaseReport
    {
        public enum ELayoutRep { CHART1 = 1, CHART2 = 2, CHART4 = 4, CHART6 = 6 };
        
        private ELayoutRep  m_layoutRep = ELayoutRep.CHART1;
        private int         m_colNum    = 1;
        
        public ELayoutRep TemplateLayoutRep
        {
            set
            {
                if (!isLayoutRepEmpty())
                    printLayoutRep(true);

                m_layoutRep = value;
            }
        }

        static private readonly LayoutReport s_instance = new LayoutReport();
        private LayoutReport()
        {
            Ds = new dsLayoutRep();
        }

        static public LayoutReport Instance
        {
            get
            {
                return s_instance;
            }
        }

        public void AddChart(MemoryStream ms, string notes)
        {
            int chartNum = add2LayoutRep(ms, notes, m_colNum);
            if (chartNum >= ((int)m_layoutRep))
            {
                printLayoutRep(false);
                Ds.Tables["tb_Charts"].Rows.Clear();
            }
        }

	    private void printLayoutRep(bool preview)
        {
            Report.SetDataSource(Ds);
            Display(preview);
        }

	    private bool isLayoutRepEmpty()
        {
            return (Ds.Tables["tb_Charts"].Rows.Count == 0);
        }

	    private int add2LayoutRep(MemoryStream ms, string notes, int colNum)
        {
            //Assad (21/12/06) - I have added the next 2 rows for initiating the report when it's needed
            if (Ds.Tables["tb_Charts"].Rows.Count == 0)
                TemplateLayoutRep = (ELayoutRep)RimedDal.Instance.GetLayoutRepTemplate();

            switch (m_layoutRep)
            {
                case ELayoutRep.CHART1:
                    Report = new crLayoutRep1();
                    m_colNum = 1;
                    break;
                case ELayoutRep.CHART2:
                    Report = new crLayoutRep2();
                    m_colNum = 1;
                    break;
                case ELayoutRep.CHART4:
                    Report = new crLayoutRep4();
                    m_colNum = 2;
                    break;
                case ELayoutRep.CHART6:
                    Report = new crLayoutRep6();
                    m_colNum = 2;
                    break;
            }

            var imgData = ms.ToArray();

            if (colNum == 1)
            {
                var newRow = Ds.Tables["tb_Charts"].NewRow();
                newRow["GraphImg"] = imgData;
                newRow["Notes"] = notes;
                newRow["GraphImg1"] = imgData;
                newRow["Notes1"] = notes;
                Ds.Tables["tb_Charts"].Rows.Add(newRow);

                return Ds.Tables["tb_Charts"].Rows.Count;
            }
            
            if (Ds.Tables["tb_Charts"].Rows.Count == 0)
            {
                var newRow = Ds.Tables["tb_Charts"].NewRow();
                newRow["GraphImg"] = imgData;
                newRow["Notes"] = notes;
                newRow["GraphImg1"] = imgData;
                newRow["Notes1"] = notes;
                Ds.Tables["tb_Charts"].Rows.Add(newRow);

                return 1;
            }
            else
            {
                // retrieve the last row.
                var row = (dsLayoutRep.tb_ChartsRow)Ds.Tables["tb_Charts"].Rows[Ds.Tables["tb_Charts"].Rows.Count - 1];
                if (row.GraphImg == row.GraphImg1 && row.Notes == row.Notes1)
                {
                    row.GraphImg1 = imgData;
                    row.Notes1 = notes;

                    return (Ds.Tables["tb_Charts"].Rows.Count * colNum);
                }
                    
                    
                var newRow = Ds.Tables["tb_Charts"].NewRow();
                newRow["GraphImg"] = imgData;
                newRow["Notes"] = notes;
                newRow["GraphImg1"] = imgData;
                newRow["Notes1"] = notes;
                Ds.Tables["tb_Charts"].Rows.Add(newRow);

                return ((Ds.Tables["tb_Charts"].Rows.Count - 1) * colNum + 1);
            }
        }
    }

    public class PrintScreenReport : BaseReport
    {
        public PrintScreenReport(Guid examIndex, MemoryStream ms, string notes, DataView dvHospital, DataView dvPatient, DataView dvExam)
            : base(examIndex)
        {
            Ds		= new dsPrintScreenRep();
            Report  = new crPrintScreenRep();
            try
            {
                UpdateGeneralData(dvHospital, dvPatient);
                updatePrintScreenRepDs(examIndex, ms, notes);
                Report.SetDataSource(Ds);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
        }

	    private void updatePrintScreenRepDs(System.Guid examIndex, System.IO.MemoryStream ms, string notes)
        {
            Ds.Tables["tb_PrintScreen"].Rows.Clear();

            DataRow newRow = Ds.Tables["tb_PrintScreen"].NewRow();
            newRow["Img"] = GetReportImage(ms, 711, 519);
            newRow["SubExamNotes"] = notes;
            Ds.Tables["tb_PrintScreen"].Rows.Add(newRow);
        }
    }

    public class GenerateExamReport : BaseReport
    {
	    protected enum ERepType { SUMMARY = 0, TABLE = 1, BOTH = 2, NONE = 3 };

        protected readonly dsGateExamination DsGateExam = new dsGateExamination();
        protected readonly dsExamination DsExam = new dsExamination();
        protected List<string> LeftCol = null;
        protected List<string> MiddleCol = null;
        protected List<string> RightCol = null;
        protected ERepType ReportType = ERepType.SUMMARY;

	    protected GenerateExamReport(Guid examIndex, DataView dvHospital, DataView dvPatient, dsExamination dsExam, dsGateExamination dsGateExam)
            : base(examIndex)
        {
            DsExam      = (dsExamination)dsExam.Copy();
            DsGateExam  = (dsGateExamination)dsGateExam.Copy();
        }

        protected void initImageLists()
        {
            if (LeftCol == null)
                LeftCol = new List<string>();

            if (MiddleCol == null)
                MiddleCol = new List<string>();

            if (RightCol == null)
                RightCol = new List<string>();

            LeftCol.Clear();
            MiddleCol.Clear();
            RightCol.Clear();
        }

        protected void FillExamArr()
        {
            initImageLists();

            switch (StudyName)
            {
                case Constants.StudyType.STUDY_EXTRACRANIAL:
                    // insert the examination schema
                    MiddleCol.Add(Path.Combine(Constants.DATA_PATH, "Extracranial1.PNG"));  
                    MiddleCol.Add(Path.Combine(Constants.DATA_PATH, "Extracranial2.PNG"));
                    MiddleCol.Add(Path.Combine(Constants.DATA_PATH, "Extracranial3.PNG"));
                    break;

                case Constants.StudyType.STUDY_PERIPHERAL:
                    // insert the examination schema
                    MiddleCol.Add(Path.Combine(Constants.DATA_PATH, "Periferal1.PNG"));
                    MiddleCol.Add(Path.Combine(Constants.DATA_PATH, "Periferal2.PNG"));
                    MiddleCol.Add(Path.Combine(Constants.DATA_PATH, "Periferal3.PNG"));
                    break;

                default:
                    // insert the examination schema
                    MiddleCol.Add(Path.Combine(Constants.DATA_PATH, "NO_ANIM1.PNG"));
                    MiddleCol.Add(Path.Combine(Constants.DATA_PATH, "NO_ANIM2.PNG"));
                    MiddleCol.Add(Path.Combine(Constants.DATA_PATH, "NO_ANIM3.PNG"));
                    break;
            }

            foreach (dsGateExamination.tb_GateExaminationRow gateExamRow in DsGateExam.tb_GateExamination.Rows)
            {
                string imgPath;
                if (File.Exists(Constants.GATES_IMAGE_PATH + gateExamRow.Gate_Index.ToString() + "_L.jpg"))
                {
                    imgPath = Constants.GATES_IMAGE_PATH + gateExamRow.Gate_Index.ToString() + "_L.jpg";
                    LeftCol.Add(imgPath);
                }
                else if (File.Exists(Constants.GATES_IMAGE_PATH + gateExamRow.Gate_Index.ToString() + "_M.jpg"))
                {
                    imgPath = Constants.GATES_IMAGE_PATH + gateExamRow.Gate_Index.ToString() + "_M.jpg";
                    MiddleCol.Add(imgPath);
                }
                else if (File.Exists(Constants.GATES_IMAGE_PATH + gateExamRow.Gate_Index.ToString() + "_R.jpg"))
                {
                    imgPath = Constants.GATES_IMAGE_PATH + gateExamRow.Gate_Index.ToString() + "_R.jpg";
                    RightCol.Add(imgPath);
                }
            }
        }

        protected void AddEmptyGates()
        {
            // check max array count.
            int maxGV = (LeftCol.Count > MiddleCol.Count) ? LeftCol.Count : MiddleCol.Count;
            maxGV = (RightCol.Count > maxGV) ? RightCol.Count : maxGV;

            string emptyGVPath = Path.Combine(Constants.DATA_PATH, "emptyGV.png");

            for (int i = LeftCol.Count; i < maxGV; i++)
                LeftCol.Add(emptyGVPath);

            for (int i = MiddleCol.Count; i < maxGV; i++)
                MiddleCol.Add(emptyGVPath);

            for (int i = RightCol.Count; i < maxGV; i++)
                RightCol.Add(emptyGVPath);
        }

        protected void UpdateGeneralExamInfo()
        {
            Ds.Tables["tb_GenralInfo"].Rows[0]["Indication"] = ExamRow["Indication"];
            Ds.Tables["tb_GenralInfo"].Rows[0]["Interpretation"] = ExamRow["Interpretation"];
			Ds.Tables["tb_GenralInfo"].Rows[0]["ReportTitle"] = GetReportTitle();	//CR #421
            Ds.Tables["tb_GenralInfo"].Rows[0]["ExamDate"] = ((DateTime)ExamRow["Date"]).ToShortDateString();
            
            // generate time string
            var time = ((((int)ExamRow["TimeHours"]) < 10) ? "0" : "") + ((int)ExamRow["TimeHours"]) + ":" + ((((int)ExamRow["TimeMinutes"]) < 10) ? "0" : "") + ((int)ExamRow["TimeMinutes"]);

            Ds.Tables["tb_GenralInfo"].Rows[0]["ExamTime"] = time;
        }
    }

    public class SummaryExamReport : GenerateExamReport
    {
        public SummaryExamReport(string studyName, Guid examIndex, DataView dvHospital, DataView dvPatient, dsExamination dsExam, dsGateExamination dsGateExam)
            : base(examIndex, dvHospital, dvPatient, dsExam, dsGateExam)
        {
            StudyName = studyName;

            Ds = new dsSummaryRep();

            Report = new crSummaryRep();
            try
            {
                UpdateGeneralData(dvHospital, dvPatient);
                FillExamArr();
                AddEmptyGates();
                updateSummaryDs();

                Report.SetDataSource(Ds);
            }
            catch (System.IndexOutOfRangeException ex)
            {
                Logger.LogError(ex);
            }
        }

        private void updateSummaryDs()
        {
            const int WIDTH = 243;
            const int HEIGHT = 105;

            // update Summary dataset.
            for (int i = 0; i < LeftCol.Count; i++)
            {
                DataRow summaryRow = Ds.Tables["tb_SummaryScreen"].NewRow();

                summaryRow["LeftImg"] = GetReportImage(LeftCol[i], WIDTH, HEIGHT);
                summaryRow["MiddleImg"] = GetReportImage(MiddleCol[i], WIDTH, HEIGHT);
                summaryRow["RightImg"] = GetReportImage(RightCol[i], WIDTH, HEIGHT);

                Ds.Tables["tb_SummaryScreen"].Rows.Add(summaryRow);
            }
        }
    }

    public abstract class FinalReport : GenerateExamReport
    {
        private void fillExamHistory()
        {
            Ds.Tables["tb_ExaminationHistory"].Clear();
			
            foreach (dsExamination.tb_ExaminationRow row in DsExam.tb_Examination.Rows)
            {
	            var examType = "Unknown";	//BugFix #412
				if (! row.IsStudy_IndexNull())
				{
					var studyRow = RimedDal.Instance.GetStudy(row.Study_Index);
					if (studyRow != null && ! studyRow.IsNameNull())
						examType = studyRow.Name;
				}

                var time = ((row.TimeHours < 10) ? "0" : "") + row.TimeHours + ":" + ((row.TimeMinutes < 10) ? "0" : "") + row.TimeMinutes;

                DataRow examHistoryNewRow = Ds.Tables["tb_ExaminationHistory"].NewRow();

                examHistoryNewRow["Examination_Index"]	= row.Examination_Index;
                examHistoryNewRow["Date"]				= row.Date.ToShortDateString();
                examHistoryNewRow["Indication"]			= row.Indication;
                examHistoryNewRow["Interpretation"]		= row.Interpretation;
                examHistoryNewRow["time"]				= time;
				examHistoryNewRow["Type"]				= GetStudyName(examType);	//CR #421
                examHistoryNewRow["Notes"]				= row.Notes;

                Ds.Tables["tb_ExaminationHistory"].Rows.Add(examHistoryNewRow);
            }
        }


        /// <summary>Constructor</summary>
        protected FinalReport(Guid examIndex, DataView dvHospital, DataView dvPatient, dsExamination dsExam, dsGateExamination dsGateExam)
            : base(examIndex, dvHospital, dvPatient, dsExam, dsGateExam)
        {
            // initial report's dataset.
            Ds = new dsPatientRep();
            UpdateGeneralData(dvHospital, dvPatient);
        }


        protected virtual void GenReportData()
        {
            if (updateExamDataWrapper())        //Ofer, v1.18.2.14 Unhandled System.ArgumentException: Value does not fall within the expected range (ErrorCode: 0x80070057), on CrystalDecisions.CrystalReports.Engine
                Report.SetDataSource(Ds);
        }

        protected virtual void UpdateReportDs()
        {
            fillExamHistory();
            updateExamDataWrapper();            //Ofer, v1.18.2.14 Unhandled System.ArgumentException: Value does not fall within the expected range (ErrorCode: 0x80070057), on CrystalDecisions.CrystalReports.Engine
        }

        protected override void RepViewerInstance_AddText2RepEvent(string field, string text)
        {
            //Deleted by Alex bug #812 v.2.2.3.18 28/12/2015 
            //text = "\n" + DateTime.Now.ToString() + " - " + text;
            ReportViewer.SuspendLayout();

            //Changed by Alex bug #812 v.2.2.3.18 28/12/2015 
            // update the main database.
            RimedDal.Instance.RevriteExamination(this.ExamRow, field, text);

            // TODO: add updating the report dataset. 
            GenReportData();
            ReportViewer.RefreshPatientRep();
            ReportViewer.ResumeLayout(false);
        }


        protected abstract void UpdateExamData();


        private bool updateExamDataWrapper()    //Ofer, v1.18.2.14 
        {
            try
            {
                UpdateExamData();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "Examination update error.");
                LoggedDialog.ShowNotification("Examination update fail\n" + ex.Message, "Examination update");

                return false;
            }

            return true;
        }
    }

    public class PatientReport : FinalReport
    {
        #region Private Fields
        private int                     m_rowsFirstPageNum = 0;
        private int                     m_ipImagesPerPage = 0;

        private readonly List<string>   m_imagerIpFiles = new List<string>();
        private string                  m_directoryName = string.Empty;

        private List<DataRowView>       m_gateExamLeft = null;
        private List<DataRowView>       m_gateExamRight = null;
        private List<DataRowView>       m_gateExamMiddle = null;
        private List<string>            m_gateNameContainer = null;
        private List<CP>                m_cpOrder = null;
        #endregion Private Fields

        private struct CP
        {
            public readonly string Name;
            public readonly string Units;
            public CP(string name, string units)
            {
                Name = name;
                Units = units;
            }
        }


        /// <summary>Constructor</summary>
        public PatientReport(Guid examIndex, DataView dvHospital, DataView dvPatient, dsExamination dsExam, dsGateExamination dsGateExam)
            : base(examIndex, dvHospital, dvPatient, dsExam, dsGateExam)
        {
            m_directoryName = Path.Combine(Constants.IP_IMAGES_PATH, ExamIndex.ToString());

			if (RimedDal.GeneralSettings.IsOnePageReport)
				Report = new crNewPatientRepSimple();
 			else
                Report = new crNewPatientRep();

            try
            {
                m_rowsFirstPageNum = genReportLayout();
                if (m_rowsFirstPageNum >= 0)            //Ofer, v1.18.2.9  
                {
                    UpdateReportDs();
                    Report.SetDataSource(Ds);
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
        }


        protected override void GenReportData()
        {
            base.GenReportData();
            updateIpImages();
        }

        protected override void ReportViewer_AddIpImages2RepEvent(List<string> files)
        {
            if (files == null || files.Count == 0)
                return;

            ReportViewer.SuspendLayout();
            fillIpImagesTable(files);
            Report.SetDataSource(Ds);
            fillRimedIpFiles(files);
            ReportViewer.RefreshPatientRep();
            ReportViewer.ResumeLayout(false);
        }

        private void fillRimedIpFiles(List<string> files)
        {
            m_imagerIpFiles.Clear();
            foreach (string fileName in files)
            {
                m_imagerIpFiles.Add(fileName);
            }

            m_directoryName = Path.Combine(Constants.IP_IMAGES_PATH, ExamIndex.ToString());
        }

        /// <summary>Copies selected IP Images from "C:\Echo Images" folder to Rimed folder</summary>
        protected override void ReportViewer_SaveIpImagesEvent()
        {
            if (!Directory.Exists(Constants.IP_IMAGES_PATH))
                Directory.CreateDirectory(Constants.IP_IMAGES_PATH);

            //Find/create new folder for Rimed IP files (in the special Examination folder)
            m_directoryName = Path.Combine(Constants.IP_IMAGES_PATH, ExamIndex.ToString());
            if (!Directory.Exists(this.m_directoryName))
                Directory.CreateDirectory(this.m_directoryName);

            //List of file paths in Rimed folder
            var rimedIpFiles = new List<string>();
            foreach (string fullname in m_imagerIpFiles)
            {
                var fileName = Path.GetFileName(fullname);
                if (!string.IsNullOrWhiteSpace(fileName))
                    rimedIpFiles.Add(Path.Combine(m_directoryName, fileName));
            }

            //Form the file with new paths and write it to HD
            using (var file = new StreamWriter(m_directoryName + @"\files.ord", false))
            {
                foreach (string filename in rimedIpFiles)
                {
                    file.Write(string.Format("{0}\r\n", filename));
                }

                file.Close();
            }

            //Get list of the old files of this Examination
            var rimedOldFiles = Directory.GetFiles(m_directoryName);

            //Make list of new files of this Saving / Delete old files
            foreach (var oldfile in rimedOldFiles)
            {
                var ipFileName = getFileFromIPList(m_imagerIpFiles, oldfile);
                if (Path.GetFileName(oldfile) != "files.ord")
                {
                    if (ipFileName == null)
                        Files.Delete(oldfile);
                    else
                    {
                        int i = m_imagerIpFiles.IndexOf(ipFileName);
                        rimedIpFiles.RemoveAt(i);
                        m_imagerIpFiles.Remove(ipFileName);
                    }
                }
            }

            //Copy files from IP folder to Rimed folder
            for (int i = 0; i < m_imagerIpFiles.Count; i++)
                Files.Copy(m_imagerIpFiles[i], rimedIpFiles[i]);

            MessageBox.Show(StringManager.GetString("Report was saved"));
        }

        protected override void UpdateReportDs()
        {
            base.UpdateReportDs();
            updateIpImages();
        }

        protected override void UpdateExamData()
        {
            Ds.Tables["tb_SummaryScreen0"].Clear();
            Ds.Tables["tb_SummaryScreen1"].Clear();

            Report.ReportDefinition.Sections["SectionLindegaardRatio"].SectionFormat.EnableSuppress = true;

            // update general report info
            UpdateGeneralExamInfo();

            fillCPTable();

            switch (ReportType)
            {
                case ERepType.SUMMARY: // summary.
                    UpdateSummaryData();

                    Report.ReportDefinition.Sections["SectionTableReport"].SectionFormat.EnableSuppress = true;
                    Report.ReportDefinition.Sections["SectionTableReport1"].SectionFormat.EnableSuppress = true;

                    break;

                case ERepType.TABLE: // table.
						fillLeftRightTable();
						fillBottomTable();

						if (!RimedDal.GeneralSettings.IsOnePageReport)
						{
							Report.ReportDefinition.Sections["SectionSummary"].SectionFormat.EnableSuppress = true;
							Report.ReportDefinition.Sections["SectionSummary1"].SectionFormat.EnableSuppress = true;
						}
                    break;

                case ERepType.BOTH:
                    // summary screen
                    UpdateSummaryData();

                    // table.
                    fillLeftRightTable();
                    fillBottomTable();
                    break;

                case ERepType.NONE:
                        Report.ReportDefinition.Sections["SectionTableReport"].SectionFormat.EnableSuppress = true;
                        Report.ReportDefinition.Sections["SectionTableReport1"].SectionFormat.EnableSuppress = true;
                        Report.ReportDefinition.Sections["SectionSummary"].SectionFormat.EnableSuppress = true;
                        Report.ReportDefinition.Sections["SectionSummary1"].SectionFormat.EnableSuppress = true;
                    break;
            }
            fillLindegaardRatio();

        }

        private int genReportLayout()
        {
            if (Report == null)     
                return -1;

            int rowExamHeight = 100;
			var paperSize = PAPER_SIZE_HIGHT - PAPER_MARGINS_TOP - PAPER_MARGINS_TOP;
			if (Report.PrintOptions != null)					
            {
                try
                {
					Report.Refresh();		//Ofer, v1.18.2.9: CR internal exception: Object reference not set to an instance of an object. (ErrorCode: 0x80004003) at CrystalDecisions.CrystalReports.Engine.PrintOptions.get_PageContentHeight()
                    paperSize = Report.PrintOptions.PageContentHeight - Report.PrintOptions.PageMargins.topMargin - Report.PrintOptions.PageMargins.bottomMargin;
                }
                catch
                {
					paperSize = PAPER_SIZE_HIGHT - PAPER_MARGINS_TOP - PAPER_MARGINS_TOP;      //Ofer, v1.18.2.9  workaround		
                }
            }

            var dvRepInfo = new DataView(RimedDal.Instance.PatientRepLayoutsDS.tb_PatientRepLayout);
            var dv = new DataView();

            if (((bool)dvRepInfo[0]["DateDisplay"]) == false && ((bool)dvRepInfo[0]["HospitalDetails"]) == false && ((bool)dvRepInfo[0]["HospitalLogo"]) == false)
            {
                Report.ReportDefinition.Sections["SectionHospitalInfo"].SectionFormat.EnableSuppress = true;
            }
            else
            {
                paperSize -= Report.ReportDefinition.Sections["SectionHospitalInfo"].Height;

                if (((bool)dvRepInfo[0]["DateDisplay"]) == false)
                    Report.ReportDefinition.ReportObjects["FieldDate"].ObjectFormat.EnableSuppress = true;

                // Hospital Details.
                dv.Table = RimedDal.Instance.ConfigDS.tb_HospitalDetails;
                for (int i = 0; i < Report.ReportDefinition.ReportObjects.Count - 1; i++)
                {
                    Debug.WriteLine(Report.ReportDefinition.ReportObjects[i].Name);
                }

                if (((bool)dvRepInfo[0]["HospitalDetails"]) == false)
                {
                    // Changed By Assaf (08/11/06) Change the Visibility of the Hospital Details to FALSE.
                    Report.ReportDefinition.ReportObjects[1].ObjectFormat.EnableSuppress = true;
                    Report.ReportDefinition.ReportObjects[2].ObjectFormat.EnableSuppress = true;
                    Report.ReportDefinition.ReportObjects[3].ObjectFormat.EnableSuppress = true;
                }

                // Hospital Logo
                if (((bool)dvRepInfo[0]["HospitalLogo"]) == false)
                    Report.ReportDefinition.ReportObjects[5].ObjectFormat.EnableSuppress = true;
            }

            // Title
            if (((bool)dvRepInfo[0]["TitleDisplay"]) == false)
                Report.ReportDefinition.Sections["SectionTitle"].SectionFormat.EnableSuppress = true;
            else
                paperSize -= Report.ReportDefinition.Sections["SectionTitle"].Height;

            // Patient Details.
            if (((int)dvRepInfo[0]["PatientDetails"]) == 0)         // minimal patient details.
                Report.ReportDefinition.Sections["SectionPatientDetailFull"].SectionFormat.EnableSuppress = true;
            else                                                    // full patient details.
                paperSize -= Report.ReportDefinition.Sections["SectionPatientDetailFull"].Height;

            //Added by Natalie for Simple Report
            bool isSimple = false;

	        isSimple = RimedDal.GeneralSettings.IsOnePageReport;


            //RIMD-321: United report for DigiLite and IP
            if (!isSimple)
            {
                m_ipImagesPerPage = RimedDal.GeneralSettings.IpImagesPerPage;
                switch (m_ipImagesPerPage)
                {
                    case 0:
							Report.ReportDefinition.Sections["SectionIpImages1"].SectionFormat.EnableSuppress = true;
							Report.ReportDefinition.Sections["SectionIpImages2"].SectionFormat.EnableSuppress = true;
							Report.ReportDefinition.Sections["SectionIpImages4"].SectionFormat.EnableSuppress = true;
                        break;
                    case 2:
							Report.ReportDefinition.Sections["SectionIpImages1"].SectionFormat.EnableSuppress = true;
							Report.ReportDefinition.Sections["SectionIpImages2"].SectionFormat.EnableSuppress = false;
							Report.ReportDefinition.Sections["SectionIpImages4"].SectionFormat.EnableSuppress = true;
                        break;
                    case 4:
							Report.ReportDefinition.Sections["SectionIpImages1"].SectionFormat.EnableSuppress = true;
							Report.ReportDefinition.Sections["SectionIpImages2"].SectionFormat.EnableSuppress = true;
							Report.ReportDefinition.Sections["SectionIpImages4"].SectionFormat.EnableSuppress = false;
                        break;
					default:	//== 1
							Report.ReportDefinition.Sections["SectionIpImages1"].SectionFormat.EnableSuppress = false;
							Report.ReportDefinition.Sections["SectionIpImages2"].SectionFormat.EnableSuppress = true;
							Report.ReportDefinition.Sections["SectionIpImages4"].SectionFormat.EnableSuppress = true;
						break;
				}
            }

            // Examination history.
            if (((bool)dvRepInfo[0]["ExaminationHistory"]) == false)
            {
                if (!isSimple)
                {
                    Report.ReportDefinition.Sections["SectionExamHistory"].SectionFormat.EnableSuppress = true;
                }
                else
                {
                    // fucking work around: i did not had the time to find out how to get the subreport sections height.
                    paperSize -= 1126;	// subReport History examination Header.
                    paperSize -= DsExam.tb_Examination.Rows.Count * 408; //// subReport History examination Details.
                    paperSize -= 225;	// subReport History examination Footer.
                }
            }

            if (((bool)dvRepInfo[0]["PatientHistory"]) == false)
            {
                if (isSimple)
                    paperSize -= 760;
                else
                    Report.ReportDefinition.Sections["SectionPatientHistory"].SectionFormat.EnableSuppress = true;
            }

            // layout.
            ReportType = (ERepType)(byte)dvRepInfo[0]["Layout"];

            if (ReportType == ERepType.SUMMARY || ReportType == ERepType.BOTH)	// summary
            {
                paperSize -= 480;
                rowExamHeight = ROW_EXAM_IMAGE_HEIGHT;
            }

            // Indication
            if (((bool)dvRepInfo[0]["Indication"]) == false)
                Report.ReportDefinition.Sections["SectionIndication"].SectionFormat.EnableSuppress = true;
            else
                paperSize -= Report.ReportDefinition.Sections["SectionIndication"].Height;

            // Interpretation
            if (((bool)dvRepInfo[0]["Interpretation"]) == false)
                Report.ReportDefinition.Sections["SectionInterpretation"].SectionFormat.EnableSuppress = true;

            // Signature
            if (((bool)dvRepInfo[0]["Signature"]) == false)
                Report.ReportDefinition.Sections["SectionSignature"].SectionFormat.EnableSuppress = true;


            // calculate the number of gate that be able to insert at the rest of the report.
            // this is work around the "CR feature" that start a new page in case the section can not be insert.
            int rowNum = 0;
            if (paperSize > 0)
                rowNum = (int)(paperSize / rowExamHeight) - 1;

            // validation.
            if (rowNum == -1)
                rowNum = 0;

            return rowNum;
        }

        /// <summary>Return full name of IP file, if Rimed file's name is the same with it</summary>
        private string getFileFromIPList(List<string> list, string fileName)
        {
            foreach (string file in list)
            {
                if (Path.GetFileName(file) == Path.GetFileName(fileName))
                    return file;
            }

            return null;
        }

        //Fill Ip Images in Report
        private void updateIpImages()
        {
            var path = string.Format(@"{0}\files.ord", m_directoryName);
            if (!File.Exists(path))
                return;

            var files = new List<string>();
            using (var file = new StreamReader(path))
            {
                while (file.Peek() != -1)
                {
                    files.Add(file.ReadLine());
                }

                file.Close();
            }

            fillIpImagesTable(files);
        }

        /// <summary>Fill 3 array with the examination data.</summary>
        private void fillSummaryTable(int startFill, int endFill, int tableNum)
        {
            int width = 243;
            int height = 105;
            for (int i = startFill; i < endFill; i++)
            {
                DataRow summaryRow = Ds.Tables["tb_SummaryScreen" + tableNum.ToString()].NewRow();
                summaryRow["LeftImg"] = GetReportImage(LeftCol[i], width, height);
                summaryRow["MiddleImg"] = GetReportImage(MiddleCol[i], width, height);
                summaryRow["RightImg"] = GetReportImage(RightCol[i], width, height);

                Ds.Tables["tb_SummaryScreen" + tableNum].Rows.Add(summaryRow);
            }
        }

        private void fillIpImagesTable(List<string> files)
        {
            Ds.Tables["tb_IpImages"].Clear();

            for (int i = 0; i < files.Count; i++)
            {
                DataRow imageRow = Ds.Tables["tb_IpImages"].NewRow();

                if (m_ipImagesPerPage == 1)
                    imageRow["LeftImg"] = GetReportImage(files[i], 617, 522);
                else if (m_ipImagesPerPage == 2)
                    imageRow["LeftImg"] = GetReportImage(files[i], 443, 373);
                else if (m_ipImagesPerPage == 4)
                {
                    imageRow["LeftImg"] = GetReportImage(files[i], 311, 262);

                    if (i < files.Count - 1)
                        imageRow["RightImg"] = GetReportImage(files[++i], 311, 262);
                }
                else
                    continue;

                Ds.Tables["tb_IpImages"].Rows.Add(imageRow);
            }
        }

        private void fillCPTable()
        {
            Ds.Tables["tb_ClinicalParam"].Rows.Clear();
            m_cpOrder = new List<CP>();
            DataView dvCP = RimedDal.Instance.GetClinicalParamByStudyIndex((Guid)ExamRow["Study_Index"]);

            int i = 1;
            DataRow newCPRow = Ds.Tables["tb_ClinicalParam"].NewRow();

            foreach (DataRowView dvRow in dvCP)
            {
                string filter = "Index = '" + ((Guid)dvRow["ClinicalParam_Index"]) + "'";
                DataRow[] drCP = RimedDal.Instance.ClinicalParamSetupDS.tb_ClinicalParam.Select(filter);
                CP cp = new CP((string)drCP[0]["Name"], (string)drCP[0]["Units"]);
                m_cpOrder.Add(cp);

                newCPRow["CpName" + i] = cp.Name;
                newCPRow["CpUnits" + i] = cp.Units;
                i++;
            }
            if (i < 10)
            {
                for (; i <= 10; i++)
                {
                    newCPRow["CpName" + i] = "";
                    newCPRow["CpUnits" + i] = "";
                }
            }
            Ds.Tables["tb_ClinicalParam"].Rows.Add(newCPRow);

        }
        
        private int lenOfSubString(string bvName)
        {
            //Ofer, v1.18.2.9: System.ArgumentOutOfRangeException: StartIndex cannot be less than zero. Parameter name: startIndex (ErrorCode: 0x80131502)
            if (string.IsNullOrWhiteSpace(bvName))
                return 0;

            var len = bvName.Length;
            if (len >= 2)
            {
                var s = bvName.Substring(len - 2, 2);
                if (s == "-L" || s == "-R")
                    len = len - 2;
            }

            return len;
        }

        private void initGatesLists()
        {
            if (m_gateExamLeft == null)
                m_gateExamLeft = new List<DataRowView>();
            else
                m_gateExamLeft.Clear();

            if (m_gateExamRight == null)
                m_gateExamRight = new List<DataRowView>();
            else
                m_gateExamRight.Clear();

            if (m_gateExamMiddle == null)
                m_gateExamMiddle = new List<DataRowView>();
            else
                m_gateExamMiddle.Clear();
        }

        private void fillLeftRightTable()
        {
            initGatesLists();

            var gateNames       = new List<string>();
            var copyDvGateExam  = new DataView(DsGateExam.tb_GateExamination);

            //Ofer, v1.18.2.9: System.ArgumentOutOfRangeException: StartIndex cannot be less than zero. Parameter name: startIndex (ErrorCode: 0x80131502)
            foreach (DataRowView item in copyDvGateExam)
            {
                if (item == null || item["Name"] == null)
                    continue;

                var rowName = item["Name"].ToString();

                var subStrLen = lenOfSubString(rowName);
                if (subStrLen == 0)
                    continue;

                var gateName = rowName.Substring(0, subStrLen);
                if (!gateNames.Contains(gateName))
                    gateNames.Add(gateName);

                if (rowName.EndsWith("-L"))
                    m_gateExamLeft.Add(item);
                else if (rowName.EndsWith("-R"))
                    m_gateExamRight.Add(item);
                else
                    m_gateExamMiddle.Add(item);
            }

            m_gateNameContainer = new List<string>();
            Ds.Tables["tb_TableRepLeftRight"].Clear();
			addLeftRightTbRows();	
		}

		private void addLeftRightTbRows()
		{
            var numberOfRowsInTable = m_gateExamRight.Count >= m_gateExamLeft.Count ? m_gateExamRight.Count : m_gateExamLeft.Count;
            for (var i = 1; i <= numberOfRowsInTable; i++)
            {
                DataRowView dvL, dvR;
                if (m_gateExamRight.Count >= i && m_gateExamLeft.Count >= i)
                {
                    dvL = m_gateExamLeft[i - 1];
                    dvR = m_gateExamRight[i - 1];
                    addLeftRightRow(dvL, dvR);
                }
                if (m_gateExamRight.Count >= i && m_gateExamLeft.Count < i)
                {
                    dvR = m_gateExamRight[i - 1];
                    addLeftRightRow(null, dvR);
                }
                if (m_gateExamRight.Count < i && m_gateExamLeft.Count >= i)
                {
                    dvL = m_gateExamLeft[i - 1];
                    addLeftRightRow(dvL, null);
                }
            }
        }

        private void addLeftRightRow(DataRowView dvRowL, DataRowView dvRowR)
        {
            var gateName = string.Empty;
            DataRow newRow = Ds.Tables["tb_TableRepLeftRight"].NewRow();

            if (dvRowL == null)
            {
                newRow["DepthLeft"] = "";
                clearCP(newRow, 1, "Left");
            }
            else
            {
                if (m_gateNameContainer.Contains(gateName) != true)
                {
                    gateName = ((string)dvRowL["Name"]).Substring(0, ((string)dvRowL["Name"]).Length - 2);
                    m_gateNameContainer.Add(gateName);
                }
                newRow["BVName"] = gateName;
                newRow["DepthLeft"] = ((double)dvRowL["Depth"]).ToString();
                insertCP(newRow, "Left", dvRowL);
            }

            if (dvRowR == null)
            {
                newRow["DepthRight"] = "";
                clearCP(newRow, 1, "Right");
            }
            else
            {
                if (m_gateNameContainer.Contains(gateName) != true)
                {
                    gateName = ((string)dvRowR["Name"]).Substring(0, ((string)dvRowR["Name"]).Length - 2);
                    m_gateNameContainer.Add(gateName);
                }
                newRow["BVName"] = gateName;
                newRow["DepthRight"] = ((double)dvRowR["Depth"]).ToString();
                insertCP(newRow, "Right", dvRowR);
            }

            if (dvRowR != null)
                newRow["cpLeftTop9"] = ((string)dvRowR["Name"]).Substring(0, ((string)dvRowR["Name"]).Length - 2);
            else
                if (dvRowL != null)
                    newRow["cpLeftTop9"] = ((string)dvRowL["Name"]).Substring(0, ((string)dvRowL["Name"]).Length - 2);

            Ds.Tables["tb_TableRepLeftRight"].Rows.Add(newRow);
        }
        
        private void clearCP(DataRow row, int startPos, string side)
        {
            for (int i = startPos; i < 11; i++)
            {
                row["Cp" + side + "Top" + i]    = string.Empty;
                row["Cp" + side + "Bottom" + i] = string.Empty;
            }
        }
        
        private void insertCP(DataRow row, string side, DataRowView dvRow)
        {
            for (int i = 0; i < m_cpOrder.Count; i++)
            {
                string cpName = m_cpOrder[i].Name;
                if (cpName == "S/D") cpName = "SD";
                if (cpName == "P.I.") cpName = "PI";

                string cpNameTop = cpName + "TopVal";
                string cpNameBottom = cpName + "BottomVal";

                int cpIndex = i + 1;
                
                //Added by Alex bug #818 v.2.2.3.16 13/12/2015 
                string name = (string)dvRow[5];
                string[] name_arr = name.Split('-');
                if (name_arr.Length == 2)
                    name = name_arr[0];

                bool top = true;
                bool bottom = true;

                var row2 = RimedDal.Instance.GetBVProperties(RimedDal.Instance.GetBVGuid(name));
                if (row2 != null)
                {
                    top = (bool)row2["DisplayPeakFwdEnvelope"];
                    bottom = (bool)row2["DisplayPeakBckEnvelope"];
                }

                //Changed by Alex bug #818 v.2.2.3.16 13/12/2015 
                row["Cp" + side + "Top" + cpIndex] = (((double)dvRow[cpNameTop]) == 0) ? string.Empty : (top ? ((double)dvRow[cpNameTop]).ToString() : string.Empty);
                row["Cp" + side + "Bottom" + cpIndex] = (((double)dvRow[cpNameBottom]) == 0) ? string.Empty : (bottom ? ((double)dvRow[cpNameBottom]).ToString() : string.Empty);
            }
            clearCP(row, m_cpOrder.Count + 1, side);
        }

        private void fillBottomTable()
        {
            Ds.Tables["tb_TableRepMiddle"].Clear();
            foreach (DataRowView dvRow in this.m_gateExamMiddle)
            {
                DataRow newRow = Ds.Tables["tb_TableRepMiddle"].NewRow();
                newRow["BVName"] = (string)dvRow["Name"];
                newRow["Depth"] = ((double)dvRow["Depth"]).ToString();
                insertCP(newRow, "Middle", dvRow);
                Ds.Tables["tb_TableRepMiddle"].Rows.Add(newRow);
            }
        }

        private void fillLindegaardRatio()
        {
            double LeftRation, RightRatio;
            // version 2.2.5.002 fix bug report from load patient do not show LR
            RimedDal.Instance.GetLindegaardRatio(out LeftRation, out RightRatio, ExamRow, DsGateExam);
            if (LeftRation == 0 && RightRatio == 0)
                return;
            Report.ReportDefinition.Sections["SectionLindegaardRatio"].SectionFormat.EnableSuppress = false;
            Ds.Tables["tb_LindegaardRatio"].Clear();
            DataRow newRow = Ds.Tables["tb_LindegaardRatio"].NewRow();
            if (LeftRation != 0)
                newRow["LRleft"] = string.Format("Lindegaard-Ratio Left: {0:N1}", LeftRation);
            else
                newRow["LRleft"] = "";
            if (RightRatio != 0)
                newRow["LRright"] = string.Format("Lindegaard-Ratio Right: {0:N1}", RightRatio);
            else
                newRow["LRright"] = "";
            Ds.Tables["tb_LindegaardRatio"].Rows.Add(newRow);
            
        }

        // The summary section was change in version 2.2.5.0
        // reports of data created before show previous format
        void UpdateSummaryData()
        {
            // find the data report version
            int reportVersion = 1;
            foreach (dsGateExamination.tb_GateExaminationRow gateExamRow in DsGateExam.tb_GateExamination.Rows)
            {
                reportVersion = gateExamRow.ReportVersion;
                break;
            }

            if (reportVersion == 1)
            {
                UpdateSummaryDataV1();
            }
            else
                UpdateSummaryDataV2();
        }

        void UpdateSummaryDataV1()
        {

            FillExamArr();
            AddEmptyGates();
            if (ReportType == ERepType.SUMMARY)
            {
                fillSummaryTable(0, (m_rowsFirstPageNum > LeftCol.Count) ? LeftCol.Count : m_rowsFirstPageNum, 0);
                if ((LeftCol.Count - m_rowsFirstPageNum) > 0)
                {
                    fillSummaryTable(m_rowsFirstPageNum, LeftCol.Count, 1);
                    Report.ReportDefinition.Sections["SectionSummary1"].SectionFormat.EnableSuppress = false;
                }
                else
                {
                    //Added by Natalie for RIMD-129: Error when export to pdf from final patient report
                    //This code hide empty graphic section (SectionSummary1) in case when all images of BVs can go into the SectionSummary
                    Report.ReportDefinition.Sections["SectionSummary1"].SectionFormat.EnableSuppress = true;
                }
            }
            else if (ReportType == ERepType.BOTH)
            {
                //Changed by Alex bug #724 v.2.2.3.19 11/01/2017 
                m_rowsFirstPageNum = (PAPER_SIZE_HIGHT - PAPER_MARGINS_TOP - PAPER_MARGINS_TOP) / ROW_EXAM_IMAGE_HEIGHT;
                fillSummaryTable(0, (m_rowsFirstPageNum > LeftCol.Count) ? LeftCol.Count : m_rowsFirstPageNum, 0);
                if ((LeftCol.Count - m_rowsFirstPageNum) > 0)
                {
                    fillSummaryTable(m_rowsFirstPageNum, LeftCol.Count, 1);
                }
                else
                {
                    //Added by Natalie for RIMD-129: Error when export to pdf from final patient report 
                    //This code hide empty graphic section (SectionSummary1) in case when all images of BVs can go into the SectionSummary
                    Report.ReportDefinition.Sections["SectionSummary1"].SectionFormat.EnableSuppress = true;
                }
            }
        }

        void UpdateSummaryDataV2()
        {
            Report.ReportDefinition.Sections["SectionSummaryWithCP"].SectionFormat.EnableSuppress = false;
            Report.ReportDefinition.Sections["SectionSummary"].SectionFormat.EnableSuppress = true;
            Report.ReportDefinition.Sections["SectionSummary1"].SectionFormat.EnableSuppress = true;

            var copyDvGateExam = new DataView(DsGateExam.tb_GateExamination);
            initGatesLists();
            initImageLists();
            
            foreach (DataRowView item in copyDvGateExam)
            {
                if (item == null || item["Name"] == null)
                    continue;

                var rowName = item["Name"].ToString();
                var subStrLen = lenOfSubString(rowName);
                if (subStrLen == 0)
                    continue;

                var gateName = rowName.Substring(0, subStrLen);
                if (rowName.EndsWith("-L") && File.Exists(string.Format("{0}{1}_L.jpg", Constants.GATES_IMAGE_PATH, item["Gate_Index"])))
                {
                    LeftCol.Add(string.Format("{0}{1}_L.jpg", Constants.GATES_IMAGE_PATH, item["Gate_Index"]));
                    m_gateExamLeft.Add(item);
                }
                else if (rowName.EndsWith("-R") && File.Exists(string.Format("{0}{1}_R.jpg", Constants.GATES_IMAGE_PATH, item["Gate_Index"])))
                {
                    RightCol.Add(string.Format("{0}{1}_R.jpg", Constants.GATES_IMAGE_PATH, item["Gate_Index"]));
                    m_gateExamRight.Add(item);
                }
                else if (File.Exists(string.Format("{0}{1}_M.jpg", Constants.GATES_IMAGE_PATH, item["Gate_Index"])))
                {
                    MiddleCol.Add(string.Format("{0}{1}_M.jpg", Constants.GATES_IMAGE_PATH, item["Gate_Index"]));
                    m_gateExamMiddle.Add(item);
                }
            }
            fillSummaryWithCPTable();

        }

        private void fillSummaryWithCPTable()
        {
            int width = 842;
            int height = 225;
            int ileft=0, iRight=0, iMid=0;
            string emptyGVPath = Path.Combine(Constants.DATA_PATH, "emptyGV2.png");
            while (ileft<m_gateExamLeft.Count || iRight<m_gateExamRight.Count || iMid<m_gateExamMiddle.Count)
            {
                DataRowView dvL=null, dvR=null;
                DataRow summaryRow = Ds.Tables["tb_SummaryV2LeftRight"].NewRow();
                if (ileft < m_gateExamLeft.Count)
                {
                    dvL = m_gateExamLeft[ileft];
                    summaryRow["LeftImg"] = GetReportImage(LeftCol[ileft], width, height);
                    ileft++;
                }
                else if (iMid < m_gateExamMiddle.Count)
                {
                    dvL = m_gateExamMiddle[iMid];
                    summaryRow["LeftImg"] = GetReportImage(MiddleCol[iMid], width, height);
                    iMid++;
                }

                if (iRight < m_gateExamRight.Count)
                {
                    dvR = m_gateExamRight[iRight];
                    summaryRow["RightImg"] = GetReportImage(RightCol[iRight], width, height);
                    iRight++;
                }
                else if (iMid < m_gateExamMiddle.Count)
                {
                    dvR = m_gateExamMiddle[iMid];
                    summaryRow["RightImg"] = GetReportImage(MiddleCol[iMid], width, height);
                    iMid++;
                }
                if (dvL == null)
                {
                    summaryRow["DepthLeft"] = "";
                    clearCP(summaryRow, 1, "Left");
                    summaryRow["LeftImg"] = GetReportImage(emptyGVPath, width, height);
                }
                else
                {
                    summaryRow["DepthLeft"] = ((double)dvL["Depth"]).ToString();
                    insertCP(summaryRow, "Left", dvL);
                }
                if (dvR == null)
                {
                    summaryRow["DepthRight"] = "";
                    clearCP(summaryRow, 1, "Right");
                    summaryRow["RightImg"] = GetReportImage(emptyGVPath, width, height);
                }
                else
                {
                    summaryRow["DepthRight"] = ((double)dvR["Depth"]).ToString();
                    insertCP(summaryRow, "Right", dvR);
                }
                Ds.Tables["tb_SummaryV2LeftRight"].Rows.Add(summaryRow);
            }
        }
    }

    public class MonitoringReport : FinalReport
    {
        #region Private Fields

        private class ReportEvent
        {
            public string EventName { get; private set; }
            public string EventTime { get; private set; }
            public string EventPath { get; private set; }

            public ReportEvent(string eventName, string eventTime, string eventPath)
            {
                EventName = eventName;
                EventTime = eventTime;
                EventPath = eventPath;
            }
        }

        private readonly dsEventExamination m_dsEventExam   = new dsEventExamination();
        private List<ReportEvent>           m_leftEvents    = new List<ReportEvent>();
        private List<ReportEvent>           m_middleEvents  = new List<ReportEvent>();
        private List<ReportEvent>           m_rightEvents   = new List<ReportEvent>();

        #endregion Private Fields

        /// <summary>Constructor</summary>
        public MonitoringReport(Guid examIndex, DataView dvHospital, DataView dvPatient, dsExamination dsExam, dsGateExamination dsGateExam, dsEventExamination dsEventExam)
            : base(examIndex, dvHospital, dvPatient, dsExam, dsGateExam)
        {
            m_dsEventExam = (dsEventExamination)dsEventExam.Copy();

            Report = new crMonitoringRep();

            try
            {
                genReportLayout();
                UpdateReportDs();
                Report.SetDataSource(Ds);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
        }


        protected override void UpdateExamData()
        {
            Ds.Tables["tb_SummaryScreen0"].Clear();

            // update general report info
            UpdateGeneralExamInfo();

            Report.ReportDefinition.Sections["SectionEvents"].SectionFormat.EnableSuppress = true;
            Report.ReportDefinition.Sections["SectionBilateralEvents"].SectionFormat.EnableSuppress = true;
            if (ReportType == ERepType.SUMMARY || ReportType == ERepType.BOTH)
            {
                fillExamArr();
                fillEventTable();
                if ((StudyName.Contains("Unilateral")) || (StudyName == "VMR")) // Changed by Alex 21.12.2015 v. 2.2.3.17 feature # 670
                    Report.ReportDefinition.Sections["SectionEvents"].SectionFormat.EnableSuppress = false;
                else if (StudyName.Contains("Bilateral"))
                    Report.ReportDefinition.Sections["SectionBilateralEvents"].SectionFormat.EnableSuppress = false;
            }
        }


        private void genReportLayout()
        {
            var dvRepInfo	= new DataView(RimedDal.Instance.PatientRepLayoutsDS.tb_PatientRepLayout);
            var dv			= new DataView();

            if (((bool) dvRepInfo[0]["DateDisplay"]) == false && ((bool) dvRepInfo[0]["HospitalDetails"]) == false && ((bool) dvRepInfo[0]["HospitalLogo"]) == false)
            {
	            Report.ReportDefinition.Sections["SectionHospitalInfo"].SectionFormat.EnableSuppress = true;
            }
            else
            {
                if (((bool)dvRepInfo[0]["DateDisplay"]) == false)
                    Report.ReportDefinition.ReportObjects["FieldDate"].ObjectFormat.EnableSuppress = true;

                // Hospital Details.
                dv.Table = RimedDal.Instance.ConfigDS.tb_HospitalDetails;
                for (int i = 0; i < Report.ReportDefinition.ReportObjects.Count - 1; i++)
                {
                    Debug.WriteLine(this.Report.ReportDefinition.ReportObjects[i].Name);
                }

                if (((bool)dvRepInfo[0]["HospitalDetails"]) == false)
                {
                    // Change the Visibility of the Hospital Details to FALSE.
                    Report.ReportDefinition.ReportObjects[1].ObjectFormat.EnableSuppress = true;
                    Report.ReportDefinition.ReportObjects[2].ObjectFormat.EnableSuppress = true;
                    Report.ReportDefinition.ReportObjects[3].ObjectFormat.EnableSuppress = true;
                }

                // Hospital Logo
                if (((bool)dvRepInfo[0]["HospitalLogo"]) == false)
                    Report.ReportDefinition.ReportObjects[5].ObjectFormat.EnableSuppress = true;
            }

            // Title
            if (((bool)dvRepInfo[0]["TitleDisplay"]) == false)
                Report.ReportDefinition.Sections["SectionTitle"].SectionFormat.EnableSuppress = true;

            // Patient Details.
            if (((int)dvRepInfo[0]["PatientDetails"]) == 0)
                // minimal patient details.
                Report.ReportDefinition.Sections["SectionPatientDetailFull"].SectionFormat.EnableSuppress = true;

            // Examination history.
            if (((bool)dvRepInfo[0]["ExaminationHistory"]) == false)
                Report.ReportDefinition.Sections["SectionExamHistory"].SectionFormat.EnableSuppress = true;

            if (((bool)dvRepInfo[0]["PatientHistory"]) == false)
                Report.ReportDefinition.Sections["SectionPatientHistory"].SectionFormat.EnableSuppress = true;

            // layout.
            ReportType = (ERepType)(byte)dvRepInfo[0]["Layout"];

            // Indication
            if (((bool)dvRepInfo[0]["Indication"]) == false)
                Report.ReportDefinition.Sections["SectionIndication"].SectionFormat.EnableSuppress = true;

            // Interpretation
            if (((bool)dvRepInfo[0]["Interpretation"]) == false)
                Report.ReportDefinition.Sections["SectionInterpretation"].SectionFormat.EnableSuppress = true;

            // Signature
            if (((bool)dvRepInfo[0]["Signature"]) == false)
                Report.ReportDefinition.Sections["SectionSignature"].SectionFormat.EnableSuppress = true;
        }

        //NOTE, Ofer: Method name changed (from 'FillExamArr()' to 'fillExamArr()') Previous name hide base method - declared using a 'new' keyword
        private void fillExamArr()  
        {
            if (m_leftEvents == null)
                m_leftEvents = new List<ReportEvent>();
            else
                m_leftEvents.Clear();

            if (m_middleEvents == null)
                m_middleEvents = new List<ReportEvent>();
            else
                m_middleEvents.Clear();

            if (m_rightEvents == null)
                m_rightEvents = new List<ReportEvent>();
            else
                m_rightEvents.Clear();


            if (DsGateExam.tb_GateExamination.Rows == null || DsGateExam.tb_GateExamination.Rows.Count == 0)        
                return;

            string imgPath;

            if ((StudyName.Contains("Unilateral")) || (StudyName == "VMR")) // Changed by Alex 21.12.2015 v. 2.2.3.17 feature # 670
            {
                var row = DsGateExam.tb_GateExamination.Rows[0] as dsGateExamination.tb_GateExaminationRow;     
                if (row == null)
                {
                    Logger.LogWarning("No unilateral GateExamination found.");
                    return;
                }

                var bvSideSuffix    = General.GetImageFileNameSuffix(row.Name);                                     
                if (string.IsNullOrWhiteSpace(bvSideSuffix))
                {
                    Logger.LogWarning("Unilateral {0} GateExamination location missing.", row.Name);
                    return;
                }

                for (var i = 0; i < m_dsEventExam.tb_ExaminationEvent.Rows.Count; i++)
                {
                    var examEventRow = m_dsEventExam.tb_ExaminationEvent.Rows[i] as dsEventExamination.tb_ExaminationEventRow;
                    if (examEventRow == null)
                        continue;

                    imgPath = string.Format("{0}{1}{2}.jpg", Constants.GATES_IMAGE_PATH, examEventRow.Event_Index, bvSideSuffix);
                    if (!File.Exists(imgPath))
                    {
                        Logger.LogWarning("Unilateral {0} GateExamination image '{1}' missing.", row.Name, imgPath);
                        continue;
                    }

                    switch (i % 3)
                    {
                        case 0:
                                m_leftEvents.Add(new ReportEvent(examEventRow.EventName, examEventRow.EventTime.ToString("hh:mm:ss"), imgPath));
                            break;
                        case 1:
                                m_middleEvents.Add(new ReportEvent(examEventRow.EventName, examEventRow.EventTime.ToString("hh:mm:ss"), imgPath));
                            break;
                        case 2:
                                m_rightEvents.Add(new ReportEvent(examEventRow.EventName, examEventRow.EventTime.ToString("hh:mm:ss"), imgPath));
                            break;
                    }
                }
            }
            else if (StudyName.Contains("Bilateral"))
            {
                foreach (dsEventExamination.tb_ExaminationEventRow examEventRow in m_dsEventExam.tb_ExaminationEvent.Rows)
                {
                    if (examEventRow == null)
                        continue;

                    imgPath = string.Format("{0}{1}_L.jpg", Constants.GATES_IMAGE_PATH, examEventRow.Event_Index);
                    if (File.Exists(imgPath))
                        m_leftEvents.Add(new ReportEvent(examEventRow.EventName, examEventRow.EventTime.ToString("hh:mm:ss"), imgPath));

                    imgPath = string.Format("{0}{1}_R.jpg", Constants.GATES_IMAGE_PATH, examEventRow.Event_Index); 
                    if (File.Exists(imgPath))
                        m_rightEvents.Add(new ReportEvent(examEventRow.EventName, examEventRow.EventTime.ToString("hh:mm:ss"), imgPath));
                }
            }
            else if (StudyName.Contains("VMR"))
            {
                
            }
        }

        /// <summary>Fill 3 array with the event screens</summary>
        private void fillEventTable()
        {
            const int WIDTH = 482;
            const int HEIGHT = 204;
            for (var i = 0; i < m_leftEvents.Count; i++)
            {
                DataRow summaryRow = Ds.Tables["tb_SummaryScreen0"].NewRow();
                summaryRow["LeftImg"] = GetReportImage(m_leftEvents[i].EventPath, WIDTH, HEIGHT);
                summaryRow["LeftEventName"] = m_leftEvents[i].EventName;
                summaryRow["LeftEventTime"] = m_leftEvents[i].EventTime;
                if (m_middleEvents.Count > i)
                {
                    summaryRow["MiddleImg"] = GetReportImage(m_middleEvents[i].EventPath, WIDTH, HEIGHT);
                    summaryRow["MiddleEventName"] = m_middleEvents[i].EventName;
                    summaryRow["MiddleEventTime"] = m_middleEvents[i].EventTime;
                }

                if (m_rightEvents.Count > i)
                {
                    summaryRow["RightImg"] = GetReportImage(m_rightEvents[i].EventPath, WIDTH, HEIGHT);
                    summaryRow["RightEventName"] = m_rightEvents[i].EventName;
                    summaryRow["RightEventTime"] = m_rightEvents[i].EventTime;
                }

                Ds.Tables["tb_SummaryScreen0"].Rows.Add(summaryRow);
            }
        }
    }

    //public delegate void ReportTextChangedHandler(Guid examIndex, string field, string text);
}
