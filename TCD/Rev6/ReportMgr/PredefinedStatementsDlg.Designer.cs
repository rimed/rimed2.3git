﻿namespace Rimed.TCD.ReportMgr
{
	partial class PredefinedStatementsDlg
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.comboBoxAlias = new System.Windows.Forms.ComboBox();
			this.labelAlias = new System.Windows.Forms.Label();
			this.textBoxStatement = new System.Windows.Forms.TextBox();
			this.buttonSelect = new System.Windows.Forms.Button();
			this.buttonCancel = new System.Windows.Forms.Button();
			this.buttonNew = new System.Windows.Forms.Button();
			this.buttonDelete = new System.Windows.Forms.Button();
			this.buttonSave = new System.Windows.Forms.Button();
			this.buttonUndo = new System.Windows.Forms.Button();
			this.buttonEdit = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// comboBoxAlias
			// 
			this.comboBoxAlias.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.comboBoxAlias.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxAlias.FormattingEnabled = true;
			this.comboBoxAlias.Location = new System.Drawing.Point(50, 12);
			this.comboBoxAlias.Name = "comboBoxAlias";
			this.comboBoxAlias.Size = new System.Drawing.Size(383, 21);
			this.comboBoxAlias.Sorted = true;
			this.comboBoxAlias.TabIndex = 0;
			this.comboBoxAlias.SelectedIndexChanged += new System.EventHandler(this.comboBoxAlias_SelectedIndexChanged);
			// 
			// labelAlias
			// 
			this.labelAlias.AutoSize = true;
			this.labelAlias.Location = new System.Drawing.Point(12, 15);
			this.labelAlias.Name = "labelAlias";
			this.labelAlias.Size = new System.Drawing.Size(32, 13);
			this.labelAlias.TabIndex = 1;
			this.labelAlias.Text = "Alias:";
			// 
			// textBoxStatement
			// 
			this.textBoxStatement.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.textBoxStatement.Location = new System.Drawing.Point(50, 39);
			this.textBoxStatement.Multiline = true;
			this.textBoxStatement.Name = "textBoxStatement";
			this.textBoxStatement.ReadOnly = true;
			this.textBoxStatement.Size = new System.Drawing.Size(383, 184);
			this.textBoxStatement.TabIndex = 2;
			this.textBoxStatement.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBoxStatement_KeyUp);
			this.textBoxStatement.Leave += new System.EventHandler(this.textBoxStatement_Leave);
			// 
			// buttonSelect
			// 
			this.buttonSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.buttonSelect.Enabled = false;
			this.buttonSelect.Location = new System.Drawing.Point(50, 229);
			this.buttonSelect.Name = "buttonSelect";
			this.buttonSelect.Size = new System.Drawing.Size(75, 23);
			this.buttonSelect.TabIndex = 3;
			this.buttonSelect.Tag = "";
			this.buttonSelect.Text = "Select";
			this.buttonSelect.UseVisualStyleBackColor = true;
			this.buttonSelect.Click += new System.EventHandler(this.buttonSelect_Click);
			// 
			// buttonCancel
			// 
			this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.buttonCancel.Location = new System.Drawing.Point(358, 229);
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.Size = new System.Drawing.Size(75, 23);
			this.buttonCancel.TabIndex = 4;
			this.buttonCancel.Tag = "";
			this.buttonCancel.Text = "Cancel";
			this.buttonCancel.UseVisualStyleBackColor = true;
			this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
			// 
			// buttonNew
			// 
			this.buttonNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonNew.Location = new System.Drawing.Point(439, 10);
			this.buttonNew.Name = "buttonNew";
			this.buttonNew.Size = new System.Drawing.Size(42, 23);
			this.buttonNew.TabIndex = 5;
			this.buttonNew.Tag = "";
			this.buttonNew.Text = "New";
			this.buttonNew.UseVisualStyleBackColor = true;
			this.buttonNew.Click += new System.EventHandler(this.buttonNew_Click);
			// 
			// buttonDelete
			// 
			this.buttonDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonDelete.Enabled = false;
			this.buttonDelete.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
			this.buttonDelete.ForeColor = System.Drawing.Color.Black;
			this.buttonDelete.Location = new System.Drawing.Point(439, 77);
			this.buttonDelete.Name = "buttonDelete";
			this.buttonDelete.Size = new System.Drawing.Size(42, 23);
			this.buttonDelete.TabIndex = 6;
			this.buttonDelete.Tag = "";
			this.buttonDelete.Text = "Del.";
			this.buttonDelete.UseVisualStyleBackColor = true;
			this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
			// 
			// buttonSave
			// 
			this.buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonSave.Enabled = false;
			this.buttonSave.Location = new System.Drawing.Point(439, 159);
			this.buttonSave.Name = "buttonSave";
			this.buttonSave.Size = new System.Drawing.Size(42, 23);
			this.buttonSave.TabIndex = 7;
			this.buttonSave.Tag = "";
			this.buttonSave.Text = "Save";
			this.buttonSave.UseVisualStyleBackColor = true;
			this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
			// 
			// buttonUndo
			// 
			this.buttonUndo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonUndo.Enabled = false;
			this.buttonUndo.Location = new System.Drawing.Point(439, 120);
			this.buttonUndo.Name = "buttonUndo";
			this.buttonUndo.Size = new System.Drawing.Size(42, 23);
			this.buttonUndo.TabIndex = 8;
			this.buttonUndo.Tag = "";
			this.buttonUndo.Text = "Undo";
			this.buttonUndo.UseVisualStyleBackColor = true;
			this.buttonUndo.Click += new System.EventHandler(this.buttonUndo_Click);
			// 
			// buttonEdit
			// 
			this.buttonEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonEdit.Enabled = false;
			this.buttonEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
			this.buttonEdit.ForeColor = System.Drawing.Color.Black;
			this.buttonEdit.Location = new System.Drawing.Point(439, 39);
			this.buttonEdit.Name = "buttonEdit";
			this.buttonEdit.Size = new System.Drawing.Size(42, 23);
			this.buttonEdit.TabIndex = 9;
			this.buttonEdit.Tag = "";
			this.buttonEdit.Text = "Edit";
			this.buttonEdit.UseVisualStyleBackColor = true;
			this.buttonEdit.Click += new System.EventHandler(this.buttonEdit_Click);
			// 
			// PredefinedStatementsDlg
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.buttonCancel;
			this.ClientSize = new System.Drawing.Size(489, 265);
			this.ControlBox = false;
			this.Controls.Add(this.buttonEdit);
			this.Controls.Add(this.buttonUndo);
			this.Controls.Add(this.buttonSave);
			this.Controls.Add(this.buttonDelete);
			this.Controls.Add(this.buttonNew);
			this.Controls.Add(this.buttonCancel);
			this.Controls.Add(this.buttonSelect);
			this.Controls.Add(this.textBoxStatement);
			this.Controls.Add(this.labelAlias);
			this.Controls.Add(this.comboBoxAlias);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "PredefinedStatementsDlg";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "PredefinedStatementsDlg";
			this.Load += new System.EventHandler(this.PredefinedStatementsDlg_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ComboBox comboBoxAlias;
		private System.Windows.Forms.Label labelAlias;
		private System.Windows.Forms.TextBox textBoxStatement;
		private System.Windows.Forms.Button buttonSelect;
		private System.Windows.Forms.Button buttonCancel;
		private System.Windows.Forms.Button buttonNew;
		private System.Windows.Forms.Button buttonDelete;
		private System.Windows.Forms.Button buttonSave;
		private System.Windows.Forms.Button buttonUndo;
		private System.Windows.Forms.Button buttonEdit;
	}
}