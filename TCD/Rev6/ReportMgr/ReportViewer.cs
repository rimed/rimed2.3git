using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Resources;
using System.Windows.Forms;

using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Rimed.Framework.Common;
using Rimed.Framework.GUI;
using Rimed.Framework.WinAPI;
using Rimed.TCD.DAL;
using Rimed.TCD.Utils;
using Constants = Rimed.TCD.Utils.Constants;

namespace Rimed.TCD.ReportMgr
{
    public partial class ReportViewer : Form
    {
		public static bool IsRetainSentDicom;
		public static bool IsRetainSentImage; 

        #region Private Fields

        private ReportClass m_reportClass;
        private readonly Guid m_examIndex = Guid.Empty;
        private string m_dicomDirName = string.Empty;
        private bool m_isSaved = false;
        private readonly IPImagesListDlg m_ipDlg;
        private readonly object m_syncWriterLock = new object();

		private readonly ResourceManagerWrapper m_stringManager;

        private bool m_lockPageChanging = false;
        private int m_currentPageNum = 1;
	    private string m_reportName;
        
        #endregion Private Fields

        public ReportViewer(Guid examIndex, string reportName)
        {
            InitializeComponent();

            var res				= new ResourceManager(GetType().Namespace + ReportResourceManager.theResourceManager.LanguageResourceFileName, GetType().Assembly);
			m_stringManager		= ResourceManagerWrapper.GetWrapper(res);
            DICOMExport.Enabled = RimedDal.Instance.AuthorizationMgrDS.tb_AuthorizationMgr[0].Dicom;
            m_examIndex			= examIndex;
	        m_reportName		= reportName;

            if (Directory.Exists(Path.Combine(Constants.DATA_PATH + "IpImages", m_examIndex.ToString())))
            {
                m_isSaved = true;
                btnSave.Enabled = false;
            }

            var ordFileName = Path.Combine(Path.Combine(Constants.IP_IMAGES_PATH, m_examIndex.ToString()), "files.ord");
            m_ipDlg			= File.Exists(ordFileName) ? new IPImagesListDlg(ordFileName) : new IPImagesListDlg();


        }

        public ReportClass PatientReportClass
        {
            set
            {
                crystalReportViewer1.ReportSource = value;
                crystalReportViewer1.RefreshReport();
            }
        }
            
	    public string CurrentPatientID { get; set; }

	    public void RefreshPatientRep()
        {
            crystalReportViewer1.RefreshReport();
            if (m_currentPageNum != 1)
                crystalReportViewer1.ShowNthPage(m_currentPageNum);
        }

        public event AddText2RepDelegate AddText2RepEvent;
        public event AddIpImages2RepDelegate AddIpImages2RepEvent;
        public event SaveIpImagesDelegate SaveIpImagesEvent;


		private const string EDIT_DIALOG_INDICATION_CAPTION		= "Indication";
		private const int	 EDIT_DIALOG_INDICATION_CATEGORY_ID	= 1;

		private const string EDIT_DIALOG_INTERPERTATION_CAPTION = "Interpretation";
		private const int EDIT_DIALOG_INTERPERTATION_CATEGORY_ID = 2;
		private void crystalReportViewer1_DrillDownSubreport(object source, CrystalDecisions.Windows.Forms.DrillSubreportEventArgs e)
        {
            e.Handled = true;
			if (e.NewSubreportName == EDIT_DIALOG_INDICATION_CAPTION)
            {
                m_lockPageChanging = true;
				using (var dlg = new EditTestDlg(EDIT_DIALOG_INDICATION_CAPTION, EDIT_DIALOG_INDICATION_CATEGORY_ID))
                {
                    //Addeded by Alex bug #812 v.2.2.3.18 28/12/2015 
                    var ExamRow = RimedDal.Instance.GetExaminationByIndex(m_examIndex);
                    dlg.UserText = ExamRow.Indication;
					
                    if (dlg.ShowDialog(this) == DialogResult.OK)
						fireAddText2RepEvent(EDIT_DIALOG_INDICATION_CAPTION, dlg.UserText);
				}
            }
			else if (e.NewSubreportName == EDIT_DIALOG_INTERPERTATION_CAPTION)
            {
                m_lockPageChanging = true;
				using (var dlg = new EditTestDlg(EDIT_DIALOG_INTERPERTATION_CAPTION, EDIT_DIALOG_INTERPERTATION_CATEGORY_ID))
                {
                    //Addeded by Alex bug #812 v.2.2.3.18 28/12/2015 
                    var ExamRow = RimedDal.Instance.GetExaminationByIndex(m_examIndex);
                    dlg.UserText = ExamRow.Interpretation;
                    
                    if (dlg.ShowDialog(this) == DialogResult.OK)
						fireAddText2RepEvent(EDIT_DIALOG_INTERPERTATION_CAPTION, dlg.UserText);
				}
            }
            else if (!m_isSaved && (e.NewSubreportName == "IpImages1" || e.NewSubreportName == "IpImages2" || e.NewSubreportName == "IpImages4"))
            {
                m_lockPageChanging = true;
                if (m_ipDlg.ShowDialog(this) == DialogResult.OK)
                    fireAddIpImages2RepEvent(m_ipDlg.ImageList);

                m_lockPageChanging = false;
            }
        }

        private void fireAddText2RepEvent(string field, string text)
        {
            if (AddText2RepEvent != null)
                AddText2RepEvent(field, text);
        }

        private void fireAddIpImages2RepEvent(List<string> files)
        {
            if (AddIpImages2RepEvent != null)
                AddIpImages2RepEvent(files);
        }

        private void crystalReportViewer1_Navigate(object source, CrystalDecisions.Windows.Forms.NavigateEventArgs e)
        {
            if (m_lockPageChanging)
                m_lockPageChanging = false;
            else
                m_currentPageNum = e.NewPageNumber;
        }

        private string exportAsPDF(string fileName)
        {
            var diskOpts = new DiskFileDestinationOptions();

            //set the export format
            m_reportClass = (ReportClass)crystalReportViewer1.ReportSource;
            m_reportClass.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
            m_reportClass.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;

            //set the disk file options
	        diskOpts.DiskFileName = fileName;	// string.Format@"{0}\Report_{1}.pdf", m_dicomDirName, DateTime.Now.ToString("yyyyMMdd_HHmmss"));
            m_reportClass.ExportOptions.DestinationOptions = diskOpts;
            m_reportClass.Export();

            return diskOpts.DiskFileName;
        }

        private void DICOMExport_Click(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick("DICOMExport", Name);

	        try
	        {
		        using (new CursorSafeChange(this))
		        {
					var studyUid	= string.Empty;

			        var exemRow		= RimedDal.Instance.LoadedExamination();
					if (exemRow != null)
					{
						//If DicomStudyUid is empty create new uid and assign it to the examination
						if (string.IsNullOrWhiteSpace(exemRow.DicomStudyUid))
						{
							studyUid				= DicomExporter.GetNewStudyUid();
							exemRow.DicomStudyUid	= studyUid;
							RimedDal.Instance.UpdateExamination();
						}
						else
						{
							studyUid = exemRow.DicomStudyUid;
						}
					}

					if (!string.IsNullOrWhiteSpace(studyUid))
						DicomExporter.SetStudyUid(studyUid, m_reportName);

					DicomExporter.SetNewSeriesUid();
					using (var dcmExporter = new DicomExporter())
			        {
				        exportToDicom(dcmExporter);
			        }
					DicomExporter.SetNewSeriesUid();
				}
	        }
	        catch (Exception ex)
	        {
		        Logger.LogError(ex);
	        }
        }

		private void exportToDicom(DicomExporter dcmExporter, string requestId = null)
		{
			if (dcmExporter == null)
				return;

			if (string.IsNullOrWhiteSpace(requestId))
				requestId = DateTime.UtcNow.Ticks.ToString("X16");

			Logger.LogInfo("ReportViewer: generating Report (PACS Server Request #{0}).", requestId);

			const string MSGBOX_TITLE = "Export to PACS server";
			m_dicomDirName = Path.Combine(Constants.DICOM_FILES_PATH, CurrentPatientID);
			if (!Directory.Exists(m_dicomDirName))
				Directory.CreateDirectory(m_dicomDirName);

			lock (m_syncWriterLock)
			{
				dsPatient.tb_PatientRow patient = null;
				for (var i = 0; i < RimedDal.Instance.PatientsDS.tb_Patient.Rows.Count; i++)
				{
					var row = RimedDal.Instance.PatientsDS.tb_Patient.Rows[i] as dsPatient.tb_PatientRow;
					if (row != null && string.Compare(CurrentPatientID, row.Patient_Index.ToString(), StringComparison.InvariantCultureIgnoreCase) == 0)
					{
						patient = row;
						break;
					}
				}

				if (patient == null)
				{
					LoggedDialog.ShowError(this, string.Format("Fail to locate patient ({0}).\nAction aborted.", CurrentPatientID), MSGBOX_TITLE);
					return;
				}

				var examination = RimedDal.Instance.GetExaminationByIndex(m_examIndex);
				if (examination == null)
				{
					LoggedDialog.ShowError(this, string.Format("Fail to locate patient ({0}) examination ({1}).\nAction aborted.", CurrentPatientID, m_examIndex), MSGBOX_TITLE);
					return;
				}

                // Added by Alex 03/04/2016 fixed bug #842 v. 2.2.3.27
                if (string.IsNullOrEmpty(examination.AccessionNumber))
                {
                    LoggedDialog.Show(this, "Please save the examination before sending it to a DICOM server.", "DICOM", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }


				var filename = exportAsPDF(string.Format(@"{0}\Report_{1}.pdf", m_dicomDirName, requestId));
				var simplename = Path.GetFileNameWithoutExtension(filename);
				if (!dcmExporter.ConvertToDcmFile(filename, m_dicomDirName))
				{
					LoggedDialog.ShowError("DCM Conversion fail / Timeout.", MSGBOX_TITLE, owner: this);
					return;
				}
				
				//Convert bmp files to DICOM format
				var namepart		= simplename + "_page-*";
				var bmpfiles		= Directory.GetFiles(m_dicomDirName, namepart);
				var patientName		= string.Format("{0} {1}", patient.Last_Name, patient.First_Name); //Bug #420
				var filesToSend		= "";

				for (var i = 0; i < bmpfiles.Length; i++)
				{
                    // Changed by Alex 03/04/2016 fixed bug #842 v. 2.2.3.27
                    var dicomFileName = dcmExporter.ConvertAndSave(bmpfiles[i], patient.ID, patientName, patient.Sex, patient.Birth_date, patient.RefferedBy, examination.Date, examination.AccessionNumber);

					if (!IsRetainSentImage)
						Files.Delete(bmpfiles[i]);

					filesToSend += string.Format("{0}{1};", filesToSend, dicomFileName);	//TODO: [Ofer] ??? - Do we need to cascade file names or just send the last one
				}

				//RIMD-362: Make DICOM sending data to server function
				var subReqId = requestId;
				var wasSent = false;
				while (!wasSent)
				{
					try
					{
						string localAeTitle;
						string aeTitle;
						string ipAddress;
						short portNumber = 0;
						try
						{
							localAeTitle	= RimedDal.Instance.ConfigDS.tb_HospitalDetails[0].localAE_Title;
							aeTitle			= RimedDal.Instance.ConfigDS.tb_HospitalDetails[0].AE_Title;
							ipAddress		= RimedDal.Instance.ConfigDS.tb_HospitalDetails[0].IP_Address;
							portNumber		= RimedDal.Instance.ConfigDS.tb_HospitalDetails[0].Port_Number;
						}
						catch (System.Data.StrongTypingException ex)
						{
							LoggedDialog.ShowError(this, m_stringManager.GetString("Set values for DICOM export: host, port, AE Title, local AE title"), MSGBOX_TITLE, exception: ex);
							return;
						}

						while (true)
						{
							if (dcmExporter.Echo(localAeTitle, aeTitle, ipAddress, portNumber))
								break;

							var msg = string.Format(m_stringManager.GetString("No response from '{0}' PACS Server."), aeTitle)
										+ string.Format("\nServer address: {0}:{1}.", ipAddress, portNumber)
										+"\n\n" + m_stringManager.GetString("Retry?");
							if (LoggedDialog.ShowQuestion(this, msg, m_stringManager.GetString("Communication error."), MessageBoxButtons.RetryCancel) != DialogResult.Retry)
								return;
						}

						var logFileName = String.Format(@"{0}\Rimed.TCD.Rev6.ExportToDicom.{1}.LOG", Constants.APP_LOGS_PATH, subReqId);
						subReqId = DateTime.UtcNow.Ticks.ToString("X16");

					    int counter = 0;
						var failedList = dcmExporter.Send(filesToSend, localAeTitle, aeTitle, ipAddress, portNumber, logFileName);

                        while (!string.IsNullOrEmpty(failedList) && counter < 3)
					    {
                            failedList = dcmExporter.Send(filesToSend, localAeTitle, aeTitle, ipAddress, portNumber, logFileName);
					        counter++;
					    }

					    if (string.IsNullOrEmpty(failedList))
							wasSent = true;
						else
							filesToSend = failedList;
					}
					catch (Exception ex)
					{
						LoggedDialog.Show(this, string.Format("Export to DICOM server (Request #{0}): FAIL.", requestId), MSGBOX_TITLE, exception: ex);
					}

					if (wasSent)
						LoggedDialog.ShowInformation(this, m_stringManager.GetString("Patient report successfully sent to PACS Server!") + "\n\n(Request #" + requestId +").", MSGBOX_TITLE);
					else if (LoggedDialog.ShowQuestion(this, m_stringManager.GetString("Some files were not sent. Do you want to try again?"), MSGBOX_TITLE, MessageBoxButtons.RetryCancel) == DialogResult.Cancel)
						wasSent = true;
				}

				// Delete temporary dicom files
				if (!IsRetainSentDicom)
				{
					Logger.LogInfo("ReportViewer: PACS Server Request #{0} - Removing temporary DCM files.", requestId);

					var dcmFiles = Directory.GetFiles(m_dicomDirName, "*" + DicomExporter.DICOM_FILE_EXT);
					foreach (var dcmFile in dcmFiles)
					{
						Files.Delete(dcmFile);
					}
				}
			}
		}

        private void btnSave_Click(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick("btnSave", Name);

            if (SaveIpImagesEvent != null)
            {
				using (new CursorSafeChange(this))
				{
					SaveIpImagesEvent();
					m_isSaved		= true;
					btnSave.Enabled = false;
				}
            }
        }


        protected override void WndProc(ref Message m)
        {
			// RIMD-496: Digi-Lite and Summary Screen windows can be moved in Windows Aero user scheme
			// This code was added to make Main Form fixed in Aero Themes of Windows 7
			if ((m.Msg == User32.WinMessage.WM_NCLBUTTONDOWN || m.Msg == User32.WinMessage.WM_NCLBUTTONDBLCLK || m.Msg == User32.WinMessage.WM_NCRBUTTONDOWN) && m.WParam == (IntPtr)User32.WinMessage.WMParam.HTCAPTION)
				return;

            try
            {
                base.WndProc(ref m);
            }
            catch (Exception e)
            {
                var ex = new ApplicationException(string.Format("{0}.WndProc({1})", Name, m), e);
                Logger.LogError(ex);
            }
        }
    }

    public delegate void AddText2RepDelegate(string field, string text);
    public delegate void AddIpImages2RepDelegate(List<string> files);
    public delegate void SaveIpImagesDelegate();
}
