using System;
using Rimed.Framework.Common;

namespace Rimed.TCD.ReportMgr
{
	public partial class  EditTestDlg : System.Windows.Forms.Form
	{
		private readonly int m_categoryId;
		private readonly string m_category;

		private EditTestDlg()
		{
			InitializeComponent();

		}
		
        public EditTestDlg(string caption, int categoryId): this()
		{
	        m_category		= caption;
			m_categoryId	= categoryId;
			Text			= m_category;
		}

		public string UserText 
        { 
            get { return richTextBox1.Text; }
		    set { richTextBox1.Text = value; }
		}

		private void buttonStatements_Click(object sender, EventArgs e)
		{
			string statment;
			try
			{
				statment = PredefinedStatementsDlg.GetStatment(this, m_categoryId, m_category);
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);
				statment = null;
			}

			if (string.IsNullOrWhiteSpace(statment))
				return;

			var curr = richTextBox1.Text;
			var pos = richTextBox1.SelectionStart;
			if (pos < 0 || curr.Length <= pos)
				curr += statment;
			else
				curr = curr.Insert(pos, statment);

			richTextBox1.Text = curr;
			richTextBox1.SelectionStart = pos + statment.Length;
			richTextBox1.Focus();
			updateTextSize();
		}

		private void richTextBox1_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			updateTextSize();
		}

		private void richTextBox1_KeyUp(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			updateTextSize();
		}

		private void updateTextSize()
		{
			labelTextSize.Text = string.Format("{0} / {1}", richTextBox1.SelectionStart + richTextBox1.SelectionLength, richTextBox1.Text.Length);
		}
	}
}
