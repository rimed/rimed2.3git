Rimed uses Ghostscript for the PDF to BMP convertation.

Example for the test:

cmd

"C:\Rimed\SRC\TCD2003\Ghostscript\gswin32c.exe" -dBATCH -dNOPAUSE -dNOPLATFONTS -sDEVICE=pngalpha -sFONTPATH="C:\Windows\Fonts" "-r300x300" -sOutputFile=C:\Rimed\SRC\TCD2003\Ghostscript\Ghostscript\page-%d.bmp C:\Rimed\SRC\TCD2003\Ghostscript\Report1.pdf