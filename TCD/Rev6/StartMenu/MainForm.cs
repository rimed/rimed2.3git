using System;
using System.Diagnostics;
using System.Drawing;
using System.ComponentModel;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Rimed.Framework.Common;
using Rimed.Framework.GUI;
using Rimed.Framework.WinAPI;
using Rimed.Framework.WinInternal;
using Rimed.TCD.StartMenu.Properties;
using Rimed.TCD.Utils;
using Constants = Rimed.TCD.Utils.Constants;
using Network_MedDev;
using Rimed.Framework.Config;

namespace Rimed.TCD.StartMenu
{
    public partial class MainForm : System.Windows.Forms.Form
    {
		const int ERROR_FILE_NOT_FOUND = 2;
        const int ERROR_ACCESS_DENIED = 5;

        private const int WM_QUERYENDSESSION = 0x0011;
		private static bool s_shuttingDown;
		private static bool s_isCloseApplication;

		private const string PROCESS_PATH_DIGI_IMAGER	= "Digi-Imager";
		private const string PROCESS_PATH_DIGI_LITE_IP	= "Digi-Lite tm IP";
	    private const string PROCESS_NAME_ECHOWAVE		= "EchoWave.exe";

	    private const string RIMED_MAUNMENU_TEXT = "Rimed.TCD Start Menu";

		private readonly static WindowsSpecialKeysWatcher s_windowsSpecialKeysWatcher = new WindowsSpecialKeysWatcher();

		/// <summary>The main entry point for the application.</summary>
		[STAThread]
		static void Main()
		{
			var hWnd = User32.GetWindowHandle(RIMED_MAUNMENU_TEXT);
			if (hWnd != IntPtr.Zero)
			{
				User32.WindowRestore(hWnd);
				return;
			}

			try
			{
				if (AppSettings.Active.AppOperationMode != General.EAppOperationMode.DEBUG)
				{
					Logger.LogInfo("Hide Windows TaskBar and Start menu.");
					User32.WindowsTaskBarHide();

					s_windowsSpecialKeysWatcher.Start();
				}

				//Set application process affinity
				Logger.LogInfo("Set Process affinity = 1.");
				Process.GetCurrentProcess().ProcessorAffinity = (IntPtr)1;

                Application.Run(new MainForm());
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);
			}
			finally
			{
				Logger.LogInfo("Restore Windows TaskBar and Start menu.");
				User32.WindowsTaskBarShow();
				s_windowsSpecialKeysWatcher.End();
			}
        }

		private ProcessWrapper m_dlProcess;
		private ProcessWrapper m_diProcess;

		//private string m_ewVersion = PROCESS_PATH_DIGI_LITE_IP;

        private ConnectionInfo connectionInfo;
        private string originalConnection = "";

        private void SetImages(string hd)
        {
            string str = (AppSettings.Active.ApplicationProductName == "Digi-One�" ? "ONE" : "LITE");
            btnTcd.DownImage = (Image)Resources.ResourceManager.GetObject("DIGI_" + str + "_but_03" + hd);
            btnTcd.HoverImage = (Image)Resources.ResourceManager.GetObject("DIGI_" + str + "_but_02" + hd);
            btnTcd.NormalImage = (Image)Resources.ResourceManager.GetObject("DIGI_" + str + "_but_01" + hd);
            // Changed by Alex 27/04/2016 bug #848 v 2.2.3.31
            btnCarotid.DownImage = (Image)Resources.ResourceManager.GetObject("DIGI_LITE_IP_but_03" + hd);
            btnCarotid.HoverImage = (Image)Resources.ResourceManager.GetObject("DIGI_LITE_IP_but_02" + hd);
            btnCarotid.NormalImage = (Image)Resources.ResourceManager.GetObject("DIGI_LITE_IP_but_01" + hd);
            btnExitWindows.DownImage = (Image)Resources.ResourceManager.GetObject("EXIT_TO_WINDOWS_03" + hd);
            btnExitWindows.HoverImage = (Image)Resources.ResourceManager.GetObject("EXIT_TO_WINDOWS_02" + hd);
            btnExitWindows.NormalImage = (Image)Resources.ResourceManager.GetObject("EXIT_TO_WINDOWS_01" + hd);
            imageButton4.DownImage = (Image)Resources.ResourceManager.GetObject("Shutdown_03" + hd);
            imageButton4.HoverImage = (Image)Resources.ResourceManager.GetObject("Shutdown_02" + hd);
            imageButton4.NormalImage = (Image)Resources.ResourceManager.GetObject("Shutdown_01" + hd);
        }

        private void SetHDResolution()
        {
            double coeffX = Screen.PrimaryScreen.Bounds.Width / 1024d;
            double coeffY = Screen.PrimaryScreen.Bounds.Height / 768d;
            double coeffW = Screen.PrimaryScreen.Bounds.Width / 1920d;
            double coeffH = Screen.PrimaryScreen.Bounds.Height / 1080d;
            pictureBox1.Image = new Bitmap(Properties.Resources.Rimed_screen_1920_768);

            SetImages("_HD");


            btnTcd.Left = (int)(btnTcd.Left * coeffX);
            btnTcd.Top = (int)(btnTcd.Top * coeffY - 37 * coeffH);
            btnTcd.Width = (int)(btnTcd.NormalImage.Width * coeffW);
            btnTcd.Height = (int)(btnTcd.NormalImage.Height * coeffH); 

            btnCarotid.Left = (int)(btnCarotid.Left * coeffX);
            btnCarotid.Top = (int)(btnCarotid.Top * coeffY - 37 * coeffH);
            btnCarotid.Width = (int)(btnCarotid.NormalImage.Width * coeffW);
            btnCarotid.Height = (int)(btnCarotid.NormalImage.Height * coeffH);

            btnExitWindows.Left = (int)(btnExitWindows.Left * coeffX + 150 * coeffW);
            btnExitWindows.Top = (int)(btnExitWindows.Top * coeffY);
            btnExitWindows.Width = (int)(btnExitWindows.NormalImage.Width * coeffW);
            btnExitWindows.Height = (int)(btnExitWindows.NormalImage.Height * coeffH);

            imageButton4.Left = (int)(imageButton4.Left * coeffX + 60 * coeffW);
            imageButton4.Top = (int)(imageButton4.Top * coeffY);
            imageButton4.Width = (int)(imageButton4.NormalImage.Width * coeffW);
            imageButton4.Height = (int)(imageButton4.NormalImage.Height * coeffH);
        }

        private MainForm()
        {
			InitializeComponent();


            if ((double)Screen.PrimaryScreen.Bounds.Width / Screen.PrimaryScreen.Bounds.Height > 1.5)
                SetHDResolution();
            else
                SetImages("");


            Splash splash = new Splash();
            splash.Show();
            Thread.Sleep(1000); 
            splash.Close();

            pictureBox1.Controls.Add(btnTcd);
            btnTcd.Location = new Point(btnTcd.Location.X, btnTcd.Location.Y);
            btnTcd.BackColor = Color.Transparent;


            pictureBox1.Controls.Add(btnCarotid);
            btnCarotid.Location = new Point(btnCarotid.Location.X, btnCarotid.Location.Y);
            btnCarotid.BackColor = Color.Transparent;

            pictureBox1.Controls.Add(btnExitWindows);
            btnExitWindows.Location = new Point(btnExitWindows.Location.X, btnExitWindows.Location.Y);
            btnExitWindows.BackColor = Color.Transparent;


            pictureBox1.Controls.Add(imageButton4);
            imageButton4.Location = new Point(imageButton4.Location.X, imageButton4.Location.Y);
            imageButton4.BackColor = Color.Transparent;

            pictureBox1.Controls.Add(pictureBox2);
            pictureBox2.Location = new Point(pictureBox2.Location.X, pictureBox2.Location.Y);
            pictureBox2.BackColor = Color.Transparent;

            
            Text						= RIMED_MAUNMENU_TEXT;
			//btnTcd.Text					= AppSettings.Active.ApplicationProductName + " TCD application";
			btnExitWindows.Visible	= AppSettings.Active.ShowApplicationCloseButton;
	        btnExitWindows.Visible		= AppSettings.Active.ShowWindowsShutdownButton;
            
            // Changed by Alex 02/07/2015 feature #661 v 2.2.3.1
            //btnExitApplication.Visible	= AppSettings.Active.ShowApplicationCloseButton;
            btnExitWindows.Visible = (AppSettings.Active.ApplicationProductName == "Digi-One�" || AppSettings.Active.ApplicationProductName == "ReviewStation�");

            // Added by Alex 13/04/2015 Change request #645 v 2.2.3.1
            btnExitWindows.Visible = AppSettings.Active.ShowWindowsShutdownButton;
            originalConnection = AppConfig.GetConfigValue("OriginalConnection", "");
            string ipDigiOne = AppConfig.GetConfigValue("IPDigiOne", "");
            string maskDigiOne = AppConfig.GetConfigValue("MaskDigiOne", "");

            if (originalConnection != "" && AppSettings.Active.ApplicationProductName == "Digi-One�")
            {
                try
                {
                    connectionInfo = NetworkManagement.NetshCommandGetIpAndMask(originalConnection);
                    NetworkManagement.NetshCommandSwitchToStatic(ipDigiOne, maskDigiOne, originalConnection);
                }
                catch (Exception ex)
                {
                    Logger.LogInfo("Change IP for start working with Digi-One fail.");
                }
            }
         }

        // Deleted by Alex 07/12/2015  Yosi's request v2.2.3.16
        //private void mainForm_Load(object sender, EventArgs e)
        //{
        //    Left		= 0;
        //    Top			= 0;
        //    Size		= new Size(AppSettings.Active.ApplicatioWidth, AppSettings.Active.ApplicationHeight);
        //    MaximumSize = Size;
        //    MinimumSize = Size;

        //    // Changed by Alex 08/11/2015 bug #806 v 2.2.3.9
        //    btnTcd.Left = Width / 2 - btnTcd.Width - 20;
        //    btnCarotid.Left = Width / 2 + 20;
        //    btnTcd.Top = (int)(MinimumSize.Height / 3.7) + 20; // 209 768 291 1080
        //    btnCarotid.Top = btnTcd.Top;

        //    btnExitApplication.Left = (Width - btnExitApplication.Width) / 2;
        //    btnExitWindows.Left = btnExitApplication.Left;

        //    btnExitApplication.Top = btnTcd.Top + btnTcd.Height + 20;
        //    btnExitWindows.Top = btnExitApplication.Top + btnExitApplication.Height + 20;
        //    TopMost = true;

        //    exitProcesses();	//killProcesses();
        //}

        private void mainForm_Load(object sender, EventArgs e)
        {
            Left = 0;
            Top = 0;
            Size = new Size(AppSettings.Active.ApplicatioWidth, AppSettings.Active.ApplicationHeight);
            MaximumSize = Size;
            MinimumSize = Size;

            //btnTcd.Left = (Width - btnTcd.Width) / 2;
            //btnCarotid.Left = (Width - btnCarotid.Width) / 2;
            //btnExitWindows.Left = (Width - btnExitWindows.Width) / 2;
            //btnExitWindows.Left = (Width - btnExitWindows.Width) / 2;

            TopMost = true;

            exitProcesses();	//killProcesses();
        }

		private void show()
		{
			if (s_isCloseApplication)
				return;

			if (InvokeRequired)
			{
				Invoke((Action)show);
				return;
			}

			s_windowsSpecialKeysWatcher.End();
			s_windowsSpecialKeysWatcher.Start();

			restoreMainMenu();
			User32.WindowRestore(Handle);	
			TopMost = true;
			Cursor = Cursors.Default;
			Focus();
		}

		private void hide()
		{
			if (InvokeRequired)
			{
				Invoke((Action)hide);
				return;
			}

			User32.WindowHide(Handle); //WindowsController.HideWindow(Text);
            TopMost = false;
		}

        private void btnTcd_Click(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick("btnTcd", Name);

			m_dlProcess.TryDispose();
			m_dlProcess = null;

            // Changed by Alex 01/05/2016 bug #852 v 2.2.3.31
            TopMost = false;
            
            m_dlProcess = new ProcessWrapper();
            runApplication(Constants.RIMED_TCD_GUI_EXECUTABLE, false, m_dlProcess);
        }

        private void btnCarotid_Click(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick("btnCarotid", Name);

			Cursor = Cursors.WaitCursor;

			if (m_diProcess != null && m_diProcess.HasStarted && !m_diProcess.HasExited)
			{
				m_diProcess.Restore();	//WindowsController.RestoreWindow(m_ewVersion);
                //Added by Alex bug #852 v 2.2.3.36 18/05/2016 
                TopMost = false;
				hide();
			}
			else if (!runEchoWaveApplication(Environment.SpecialFolder.ProgramFilesX86))
			{
				show();
                // Changed by Alex 01/05/2016 bug #849 v 2.2.3.31
                ShowMessage("Error", "DigiLite� IP application was not purchased", MessageBoxButtons.OK);
			}
		
			Cursor = Cursors.Default;
		}

        private bool runEchoWaveApplication(Environment.SpecialFolder programFilesFolder)
        {
			var pathFmt = Environment.GetFolderPath(programFilesFolder) + "\\Rimed\\{0}\\" + PROCESS_NAME_ECHOWAVE;
            var path	= string.Format(pathFmt, PROCESS_PATH_DIGI_IMAGER);
			if (!File.Exists(path))
				path = string.Format(pathFmt, PROCESS_PATH_DIGI_LITE_IP);

	        if (!File.Exists(path))
		        return false;

			m_diProcess.TryDispose();
			m_diProcess = null;

			m_diProcess = new ProcessWrapper();

			return runApplication(path, true, m_diProcess);
        }

		private bool runApplication(string fileName, bool isDigiImager, ProcessWrapper appProcess)
        {
            Logger.LogInfo("runApplication(fileName={0}, isDigiImager={1})", fileName, isDigiImager);

			if (appProcess == null)
			{
				show();	
				return false;
			}

            Cursor = Cursors.WaitCursor;

			var ret = true;
            try
            {
                btnTcd.Enabled				= false;
                btnCarotid.Enabled			= false;
                btnExitWindows.Enabled		= false;
				btnExitWindows.Enabled	= false;

                appProcess.OnExited			+= onProcessExited;
				appProcess.OnIconicChanged	+= onProcessIconicChanged;
				if (!appProcess.Start(fileName))
				{
					ret = false;
					show();
                    // Changed by Alex 01/05/2016 bug #849 v 2.2.3.31
                    ShowMessage("Error", string.Format("Application ({0}) startup fail.", fileName), MessageBoxButtons.OK);
				}
            }
            catch (Win32Exception ex)
            {
				ret = false;
				
				if (ex.NativeErrorCode == ERROR_FILE_NOT_FOUND)
                    // Changed by Alex 01/05/2016 bug #849 v 2.2.3.31
                    ShowMessage("Error", "DigiLite� IP application was not purchased.", MessageBoxButtons.OK);
                else if (ex.NativeErrorCode == ERROR_ACCESS_DENIED)
                    // Changed by Alex 01/05/2016 bug #849 v 2.2.3.31
                    ShowMessage("Error", "Missing permission for the DigiLite� Carotid imaging.", MessageBoxButtons.OK);
                else
                    // Changed by Alex 01/05/2016 bug #849 v 2.2.3.31
                    ShowMessage("Error", ex.Message, MessageBoxButtons.OK);
	            show();		// restoreMainMenu();
			}
            catch (Exception ex)
            {
				ret = false;

                // Changed by Alex 01/05/2016 bug #849 v 2.2.3.31
                ShowMessage("Error", ex.Message, MessageBoxButtons.OK);
				show();		// restoreMainMenu();
            }

			Cursor = Cursors.Default;

			return ret;
		}

		private void btnExitWindows_Click(object sender, System.EventArgs e)
		{
			LoggerUserActions.MouseClick("btnExitWindows", Name);

            // Changed by Alex 04/05/2016 CR #NN v 2.2.3.32
            if (ShowMessage("Shut Down", "Are you sure you want to shut down the computer?", MessageBoxButtons.YesNo) == DialogResult.Yes)
				ProcessComm.ShutdownWindows();
		}

		private void onProcessIconicChanged(Process process, bool isIconic)
		{
			Logger.LogDebug("onProcessIconicChanged(process={0}, isIconic={1})", process, isIconic);

			if (isIconic)
				show();
			else if (process != null && !process.HasExited)
				hide();
		}

        private void onProcessExited(object sender, EventArgs e)
        {
			Logger.LogDebug("onProcessExited(sender={0}, e={1})", sender, e);
			show();
		}


		private void killProcesses()
		{
			//Kill Rimed.TCD.GUI application
			var name = Files.GetFileNameWithoutExtention(Constants.RIMED_TCD_GUI_EXECUTABLE);
			var processes = Process.GetProcessesByName(name);
			foreach (var process in processes)
			{
				killProcess(process);
			}

			// Kill eEchoWave processes
			name = Files.GetFileNameWithoutExtention(PROCESS_NAME_ECHOWAVE);
			processes = Process.GetProcessesByName(name);
			foreach (var process in processes)
			{
				killProcess(process);
			}
		}

		private void closeProcesses()
		{
			//Close Rimed.TCD.GUI application
			var name = Files.GetFileNameWithoutExtention(Constants.RIMED_TCD_GUI_EXECUTABLE);
			var processes = Process.GetProcessesByName(name);
			foreach (var process in processes)
			{
				closeProcessMainWindow(process);
			}

			// Kill eEchoWave processes
			name = Files.GetFileNameWithoutExtention(PROCESS_NAME_ECHOWAVE);
			processes = Process.GetProcessesByName(name);
			foreach (var process in processes)
			{
				closeProcessMainWindow(process);
			}
		}

		private void waitForProcessesToTerminate(int timeout = 1000)
		{
			//Close Rimed.TCD.GUI application
			var name = Files.GetFileNameWithoutExtention(Constants.RIMED_TCD_GUI_EXECUTABLE);
			var processes = Process.GetProcessesByName(name);
			foreach (var process in processes)
			{
				if (process != null && !process.HasExited)
					process.WaitForExit(timeout);
			}

			// Kill eEchoWave processes
			name = Files.GetFileNameWithoutExtention(PROCESS_NAME_ECHOWAVE);
			processes = Process.GetProcessesByName(name);
			foreach (var process in processes)
			{
				if (process != null && !process.HasExited)
					process.WaitForExit(timeout);
			}
		}

		private void exitProcesses()
		{
			m_dlProcess.TryDispose();
			m_dlProcess = null;

			m_diProcess.TryDispose();
			m_diProcess = null;

			try
			{
				closeProcesses();

				waitForProcessesToTerminate();

				killProcesses();
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);
			}
		}

		private void closeProcessMainWindow(Process process)
		{
			if (process == null || process.HasExited)
				return;

			try
			{
				process.CloseMainWindow();
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);
			}
		}

		private void killProcess(Process process)
		{
			if (process == null)
				return;

			try
			{
				if (!process.HasExited)
					process.Kill();

				process.Dispose();
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);
			}
		}

		private void restoreMainMenu()
        {
            if (InvokeRequired)
            {
                Invoke((Action)restoreMainMenu);
                return;
            }

			User32.WindowsTaskBarHide();
			Focus();

			btnTcd.Enabled				= true;
            btnCarotid.Enabled			= true;
            btnExitWindows.Enabled		= true;
	        btnExitWindows.Enabled	= true;

            //btnTcd.BackColor			= Color.LightSlateGray;
            //btnCarotid.BackColor		= Color.LightSlateGray;
        }

        private void mainForm_VisibleChanged(object sender, System.EventArgs e)
        {
			if (Visible)
				show();
			else
				hide();
		}

        //Disable Alt+F4. Added by Natalie on 08/21/2009
        private void mainForm_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
	        if (s_isCloseApplication)
		        return;

            if (!s_shuttingDown)
				e.Cancel = true;
        }

        protected override void WndProc(ref Message m)
        {
	        try
	        {
				if (m.Msg == ProcessComm.WinMsgMainMenuApplicationRestore)
				{
					Logger.LogInfo("RESTORE message received.");
					show();

					return;
				}

				if (m.Msg == ProcessComm.WinMsgMainMenuApplicationExit)
				{
					Logger.LogInfo("EXIT message received.");
					hide();
					s_isCloseApplication = true;
					Close();

					return;
				}
				
				s_shuttingDown = (m.Msg == WM_QUERYENDSESSION);
				base.WndProc(ref m);
			}
	        catch (Exception ex)
	        {
				Logger.LogError(ex);    
	        }
        }

        // Added by Alex 01/05/2016 bug #849 v 2.2.3.31
        private DialogResult ShowMessage(string header, string message, MessageBoxButtons buttons)
        {
            var mess = new NewMessageLoger(header, message, buttons);
            mess.ShowDialog();
            return mess.result;
        }

		private void btnExitApplication_MouseClick(object sender, MouseEventArgs e)
		{
            // Changed by Alex 04/05/2016 CR #NN v 2.2.3.32
            if (ShowMessage("Exit", "Are you sure you want to exit the application?", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                s_isCloseApplication = true;
                Close();
            }
		}

		private void mainForm_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Alt && e.Control && e.KeyValue == 32 && !e.Handled && AppSettings.Active.EnableApplicationClose)
			{
				btnExitWindows.Visible = !btnExitWindows.Visible;

				e.Handled = true;
			}
		}

		private void mainForm_FormClosed(object sender, FormClosedEventArgs e)
		{
			Logger.LogInfo("Closing");
			exitProcesses();
		}

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Added by Alex 13/04/2015 Change request #645 v 2.2.3.1
            if (originalConnection != "")
                if (connectionInfo.DHCP)
                    NetworkManagement.NetshCommandSwitchToDHCP(originalConnection);
                else
                    NetworkManagement.NetshCommandSwitchToStatic(connectionInfo.IP, connectionInfo.Mask, originalConnection);
        }
    }
}
