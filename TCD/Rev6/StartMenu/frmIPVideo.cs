﻿using System.Windows.Forms;

namespace Rimed.TCD.StartMenu
{
    public partial class frmIPVideo : Form
    {
        public frmIPVideo()
        {
            InitializeComponent();
        }

        private void axwmplayer_MediaError(object sender, AxWMPLib._WMPOCXEvents_MediaErrorEvent e)
        {
            MessageBox.Show("Cannot play IP media file");
        }
    }
}
