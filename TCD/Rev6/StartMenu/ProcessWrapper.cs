﻿using System;
using System.Diagnostics;
using System.Threading;
using Rimed.Framework.Common;
using Rimed.Framework.Threads;
using Rimed.Framework.WinAPI;

namespace Rimed.TCD.StartMenu
{
	public class ProcessWrapper: Disposable
	{
		private readonly object			m_locker = new object();
		private Process					m_process = new Process();
		private int						m_chkIntervalMSec = 250;
		private Thread					m_thread;

		public delegate void IconicChangedDelegate(Process process, bool isForground);

		public event IconicChangedDelegate OnIconicChanged;
		public event EventHandler OnExited;

		public bool HasStarted { get; private set; }
		public bool HasExited { get { return m_process == null || m_process.HasExited; } }
		public string Name
		{
			get
			{
				if (m_process == null) 
					return "Unknown";

				return m_process.ProcessName;
			}
		}
		public int CheckInterval
		{
			get { return m_chkIntervalMSec; }
			set
			{
				if (value <= 0)
					return;

				m_chkIntervalMSec = value;
			}
		}

		public bool Start(string fileName)
		{
			return start(new ProcessStartInfo(fileName));
		}

		private bool start(ProcessStartInfo startInfo)
		{
			lock (m_locker)
			{
				if (m_thread != null || startInfo == null)
					return false;

				if (HasStarted && !HasExited)
					return false;

				m_process.StartInfo = startInfo;
				m_process.EnableRaisingEvents = true;
				m_process.Exited += doOnExited;

				var ret = m_process.Start();
				if (!ret)
					return false;

				HasStarted = true;

				m_thread = new Thread(processSamplerThreadDelegate);
				m_thread.Priority = ThreadPriority.BelowNormal;
				m_thread.IsBackground = true;
				m_thread.SetName("ProcessWrapper: " + m_process.ProcessName);

				m_thread.Start();

				User32.WindowRestore(m_process.MainWindowHandle);

				return true;
			}
		}

		private void stop()
		{
			lock (m_locker)
			{
				if (HasStarted && !HasExited)
				{
					m_process.CloseMainWindow();
					m_process.WaitForExit(1000);
					if (!HasExited)
						m_process.Kill();
				}

				m_process.TryDispose();
				m_process = null;
			}
		}


		public void Restore()
		{
			lock (m_locker)
			{
				if (!HasStarted || HasExited)
					return;

				User32.WindowRestore(m_process.MainWindowHandle);
			}
		}

		protected override void DisposeManagedResources()
		{
			stop();

			base.DisposeManagedResources();
		}

		private void processSamplerThreadDelegate()
		{
			//Logger.LogDebug();
			bool prevIsIconic	= true;
			bool isVisibleOnce	= false;
			while (HasStarted && !HasExited)
			{
				try
				{
					Thread.Sleep(m_chkIntervalMSec);

					// Wait for the main form to be visible for the first time.
					if (!isVisibleOnce)
					{
						lock (m_locker)
						{
							if (m_process == null)
								break;

							isVisibleOnce = User32.IsWindowVisible(m_process.MainWindowHandle);
						}

						if (!isVisibleOnce)
							continue;
					}

					bool isIconic;
					lock (m_locker)
					{
						if (m_process == null)
							break;
						
						isIconic = User32.IsIconic(m_process.MainWindowHandle);
					}

					if (prevIsIconic != isIconic)
					{
						doOnIconicChanged(isIconic);

						prevIsIconic = isIconic;
					}
				}
				catch (ThreadAbortException)
				{
					return;
				}
				catch (Exception ex)
				{
					Logger.LogError(ex);
				}
			}

			doOnIconicChanged(true);
		}

		private void doOnIconicChanged(bool isIconic)
		{
			if (OnIconicChanged != null)
			{
				lock (m_locker)
					OnIconicChanged(m_process, isIconic);
			}
		}

		private void doOnExited(object sender, EventArgs e)
		{
			if (OnExited != null)
			{
				lock (m_locker)
					OnExited(m_process, e);
			}
		}
	}
}
