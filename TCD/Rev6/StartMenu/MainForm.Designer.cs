﻿
using Rimed.Framework.GUI;
using Rimed.TCD.Utils;

namespace Rimed.TCD.StartMenu
{
    partial class MainForm
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnTcd = new Rimed.Framework.GUI.ImageButton();
            this.imageButton4 = new Rimed.Framework.GUI.ImageButton();
            this.btnExitWindows = new Rimed.Framework.GUI.ImageButton();
            this.btnCarotid = new Rimed.Framework.GUI.ImageButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTcd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExitWindows)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCarotid)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Rimed.TCD.StartMenu.Properties.Resources.Rimed_prog_bar_1024x768;
            this.pictureBox2.Location = new System.Drawing.Point(383, 516);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(298, 15);
            this.pictureBox2.TabIndex = 10;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1024, 768);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // btnTcd
            // 
            this.btnTcd.BackColor = System.Drawing.Color.Transparent;
            this.btnTcd.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnTcd.DownImage = global::Rimed.TCD.StartMenu.Properties.Resources.DIGI_LITE_but_03;
            this.btnTcd.HoverImage = global::Rimed.TCD.StartMenu.Properties.Resources.DIGI_LITE_but_02;
            this.btnTcd.Location = new System.Drawing.Point(359, 292);
            this.btnTcd.Name = "btnTcd";
            this.btnTcd.NormalImage = global::Rimed.TCD.StartMenu.Properties.Resources.DIGI_LITE_but_03;
            this.btnTcd.Size = new System.Drawing.Size(161, 161);
            this.btnTcd.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnTcd.TabIndex = 9;
            this.btnTcd.TabStop = false;
            this.btnTcd.Click += new System.EventHandler(this.btnTcd_Click);
            // 
            // imageButton4
            // 
            this.imageButton4.BackColor = System.Drawing.Color.Transparent;
            this.imageButton4.DialogResult = System.Windows.Forms.DialogResult.None;
            this.imageButton4.DownImage = global::Rimed.TCD.StartMenu.Properties.Resources.Shutdown_03;
            this.imageButton4.HoverImage = global::Rimed.TCD.StartMenu.Properties.Resources.Shutdown_02;
            this.imageButton4.Location = new System.Drawing.Point(777, 688);
            this.imageButton4.Name = "imageButton4";
            this.imageButton4.NormalImage = global::Rimed.TCD.StartMenu.Properties.Resources.Shutdown_03;
            this.imageButton4.Size = new System.Drawing.Size(169, 38);
            this.imageButton4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imageButton4.TabIndex = 8;
            this.imageButton4.TabStop = false;
            this.imageButton4.Click += new System.EventHandler(this.btnExitWindows_Click);
            // 
            // btnExitWindows
            // 
            this.btnExitWindows.BackColor = System.Drawing.Color.Transparent;
            this.btnExitWindows.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnExitWindows.DownImage = global::Rimed.TCD.StartMenu.Properties.Resources.EXIT_TO_WINDOWS_03;
            this.btnExitWindows.HoverImage = global::Rimed.TCD.StartMenu.Properties.Resources.EXIT_TO_WINDOWS_02;
            this.btnExitWindows.Location = new System.Drawing.Point(604, 688);
            this.btnExitWindows.Name = "btnExitWindows";
            this.btnExitWindows.NormalImage = global::Rimed.TCD.StartMenu.Properties.Resources.EXIT_TO_WINDOWS_03;
            this.btnExitWindows.Size = new System.Drawing.Size(169, 38);
            this.btnExitWindows.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnExitWindows.TabIndex = 7;
            this.btnExitWindows.TabStop = false;
            this.btnExitWindows.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnExitApplication_MouseClick);
            // 
            // btnCarotid
            // 
            this.btnCarotid.BackColor = System.Drawing.Color.Transparent;
            this.btnCarotid.DialogResult = System.Windows.Forms.DialogResult.None;
            this.btnCarotid.DownImage = global::Rimed.TCD.StartMenu.Properties.Resources.DIGI_LITE_IP_but_03;
            this.btnCarotid.HoverImage = global::Rimed.TCD.StartMenu.Properties.Resources.DIGI_LITE_IP_but_02;
            this.btnCarotid.Location = new System.Drawing.Point(532, 292);
            this.btnCarotid.Name = "btnCarotid";
            this.btnCarotid.NormalImage = global::Rimed.TCD.StartMenu.Properties.Resources.DIGI_LITE_IP_but_03;
            this.btnCarotid.Size = new System.Drawing.Size(161, 161);
            this.btnCarotid.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnCarotid.TabIndex = 6;
            this.btnCarotid.TabStop = false;
            this.btnCarotid.Click += new System.EventHandler(this.btnCarotid_Click);
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(1024, 768);
            this.ControlBox = false;
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.btnTcd);
            this.Controls.Add(this.imageButton4);
            this.Controls.Add(this.btnExitWindows);
            this.Controls.Add(this.btnCarotid);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Main Menu";
            this.TopMost = true;
            this.TransparencyKey = System.Drawing.Color.Maroon;
            this.Closing += new System.ComponentModel.CancelEventHandler(this.mainForm_Closing);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.mainForm_FormClosed);
            this.Load += new System.EventHandler(this.mainForm_Load);
            this.VisibleChanged += new System.EventHandler(this.mainForm_VisibleChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.mainForm_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnTcd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageButton4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExitWindows)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnCarotid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
        private ImageButton btnCarotid;
        private ImageButton btnExitWindows;
        private ImageButton imageButton4;
        private ImageButton btnTcd;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}

//Deleted by Alex 07/12/2015  Yosi's request v2.2.3.16
//using Rimed.TCD.Utils;

//namespace Rimed.TCD.StartMenu
//{
//    partial class MainForm
//    {
//        /// <summary>
//        /// Clean up any resources being used.
//        /// </summary>
//        protected override void Dispose(bool disposing)
//        {
//            if (disposing)
//            {
//                if (components != null)
//                {
//                    components.Dispose();
//                }
//            }
//            base.Dispose(disposing);
//        }

//        #region Windows Form Designer generated code

//        /// <summary>
//        /// Required method for Designer support - do not modify
//        /// the contents of this method with the code editor.
//        /// </summary>
//        private void InitializeComponent()
//        {
//            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
//            this.btnTcd = new System.Windows.Forms.Button();
//            this.btnCarotid = new System.Windows.Forms.Button();
//            this.pictureBox1 = new System.Windows.Forms.PictureBox();
//            this.btnExitWindows = new System.Windows.Forms.Button();
//            this.btnExitApplication = new System.Windows.Forms.Button();
//            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
//            this.SuspendLayout();
//            // 
//            // btnTcd
//            // 
//            this.btnTcd.BackColor = System.Drawing.Color.LightSlateGray;
//            this.btnTcd.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
//            this.btnTcd.ForeColor = System.Drawing.SystemColors.ControlLightLight;
//            this.btnTcd.Location = new System.Drawing.Point(63, 233);
//            this.btnTcd.Name = "btnTcd";
//            this.btnTcd.Size = new System.Drawing.Size(368, 256);
//            this.btnTcd.TabIndex = 0;
//            this.btnTcd.Text = "TCD application ";
//            this.btnTcd.UseVisualStyleBackColor = false;
//            this.btnTcd.Click += new System.EventHandler(this.btnTcd_Click);
//            // 
//            // btnCarotid
//            // 
//            this.btnCarotid.BackColor = System.Drawing.Color.LightSlateGray;
//            this.btnCarotid.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
//            this.btnCarotid.ForeColor = System.Drawing.SystemColors.ControlLightLight;
//            this.btnCarotid.Location = new System.Drawing.Point(570, 233);
//            this.btnCarotid.Name = "btnCarotid";
//            this.btnCarotid.Size = new System.Drawing.Size(368, 256);
//            this.btnCarotid.TabIndex = 1;
//            this.btnCarotid.Text = "Digi Lite IP carotid & intracranial imaging application (TCCS)";
//            this.btnCarotid.UseVisualStyleBackColor = false;
//            this.btnCarotid.Click += new System.EventHandler(this.btnCarotid_Click);
//            // 
//            // pictureBox1
//            // 
//            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
//            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
//            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
//            this.pictureBox1.Name = "pictureBox1";
//            this.pictureBox1.Size = new System.Drawing.Size(1024, 768);
//            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
//            this.pictureBox1.TabIndex = 4;
//            this.pictureBox1.TabStop = false;
//            // 
//            // btnExitWindows
//            // 
//            this.btnExitWindows.BackColor = System.Drawing.Color.DarkRed;
//            this.btnExitWindows.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
//            this.btnExitWindows.ForeColor = System.Drawing.SystemColors.ControlLightLight;
//            this.btnExitWindows.Location = new System.Drawing.Point(296, 599);
//            this.btnExitWindows.Name = "btnExitWindows";
//            this.btnExitWindows.Size = new System.Drawing.Size(418, 53);
//            this.btnExitWindows.TabIndex = 3;
//            this.btnExitWindows.Text = "Windows Shutdown";
//            this.btnExitWindows.UseVisualStyleBackColor = false;
//            this.btnExitWindows.Click += new System.EventHandler(this.btnExitWindows_Click);
//            // 
//            // btnExitApplication
//            // 
//            this.btnExitApplication.BackColor = System.Drawing.Color.Tomato;
//            this.btnExitApplication.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
//            this.btnExitApplication.ForeColor = System.Drawing.SystemColors.ControlLightLight;
//            this.btnExitApplication.Location = new System.Drawing.Point(296, 524);
//            this.btnExitApplication.Name = "btnExitApplication";
//            this.btnExitApplication.Size = new System.Drawing.Size(418, 53);
//            this.btnExitApplication.TabIndex = 2;
//            this.btnExitApplication.Text = "Exit Rimed™ menu";
//            this.btnExitApplication.UseVisualStyleBackColor = false;
//            this.btnExitApplication.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnExitApplication_MouseClick);
//            // 
//            // MainForm
//            // 
//            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
//            this.BackColor = System.Drawing.Color.LightSteelBlue;
//            this.ClientSize = new System.Drawing.Size(1024, 768);
//            this.ControlBox = false;
//            this.Controls.Add(this.btnExitApplication);
//            this.Controls.Add(this.btnExitWindows);
//            this.Controls.Add(this.btnCarotid);
//            this.Controls.Add(this.btnTcd);
//            this.Controls.Add(this.pictureBox1);
//            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
//            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
//            this.KeyPreview = true;
//            this.MaximizeBox = false;
//            this.MinimizeBox = false;
//            this.Name = "MainForm";
//            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
//            this.Text = "Main Menu";
//            this.TopMost = true;
//            this.Closing += new System.ComponentModel.CancelEventHandler(this.mainForm_Closing);
//            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
//            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.mainForm_FormClosed);
//            this.Load += new System.EventHandler(this.mainForm_Load);
//            this.VisibleChanged += new System.EventHandler(this.mainForm_VisibleChanged);
//            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.mainForm_KeyDown);
//            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
//            this.ResumeLayout(false);

//        }

//        #endregion

//        private System.Windows.Forms.Button btnTcd;
//        private System.Windows.Forms.Button btnCarotid;
//        private System.Windows.Forms.PictureBox pictureBox1;
//        private System.Windows.Forms.Button btnExitWindows;
//        /// <summary>
//        /// Required designer variable.
//        /// </summary>
//        private System.ComponentModel.Container components = null;
//        private System.Windows.Forms.Button btnExitApplication;
//    }
//}