using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Rimed.Framework.Common;
using Syncfusion.Windows.Forms.Tools.XPMenus;
using Rimed.TCD.DAL;
using Rimed.TCD.Utils;

namespace Rimed.TCD.GUI
{
    public partial class StudiesSelectDlg : Form
    {
        /// <summary>Constructor</summary>
        public StudiesSelectDlg()
        {
//            System.Diagnostics.Trace.WriteLine(string.Format("Window handle: {0} created.", Handle));   // Ofer, Force creation of Window handle

            this.InitializeComponent();
            this.BackColor = GlobalSettings.BackgroundDlg;

            var strRes = MainForm.StringManager;
            this.buttonOK.Text = strRes.GetString("OK");
            this.buttonCancel.Text = strRes.GetString("Close");
            this.buttonDelete.Text = strRes.GetString("Delete");
            this.buttonHelp.Text = strRes.GetString("Help");
            this.Text = strRes.GetString("Studies");

            this.SelectedNodeTag = Guid.Empty;
        }

        public Guid SelectedNodeTag { get; private set; }

        public void InitData(BarItems items)
        {
            this.treeViewStudies.Nodes.Clear();
            foreach (ParentBarItem ib in items)
            {
                TreeNode n = new TreeNode(ib.Text);
                n.Tag = ib.Tag;
                this.treeViewStudies.Nodes.Add(n);
                foreach (BarItem bi in ib.Items)
                {
                    TreeNode nn = new TreeNode(bi.Text);
                    nn.Tag = bi.Tag;
                    n.Nodes.Add(nn);
                }
            }
        }

        private void onOk(bool showWarning)
        {
            // Verify that the selected node is a study (a leaf in the tree)
            if (treeViewStudies.SelectedNode != null && treeViewStudies.SelectedNode.Nodes.Count == 0)
            {
                var fields = LoggerUserActions.GetFilesList();
                fields.Add("treeViewStudies", treeViewStudies.SelectedNode.Text);
                LoggerUserActions.MouseClick("buttonOK", Name, fields);

                SelectedNodeTag = (Guid)treeViewStudies.SelectedNode.Tag;
                DialogResult = DialogResult.OK;
                Close();
            }
            else if (showWarning)
            {
                LoggedDialog.ShowError(this, MainForm.StringManager.GetString("Please select a study."));
            }
        }

        private void buttonOK_Click(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick("buttonOK", Name);

            onOk(true);
        }

        private void treeViewStudies_DoubleClick(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseDoubleClick("treeViewStudies", Name);

            this.onOk(false);
        }

        private void buttonDelete_Click(object sender, System.EventArgs e)
        {
            SelectedNodeTag = (Guid)treeViewStudies.SelectedNode.Tag;
            if (treeViewStudies.SelectedNode != null && treeViewStudies.SelectedNode.Nodes.Count == 0 && treeViewStudies.SelectedNode.Text != "Default")
            {
                var strRes = MainForm.StringManager;

                if (LoggedDialog.ShowWarning(this, strRes.GetString("Are you sure you want to delete the selected study?"), buttons: MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    var fields = LoggerUserActions.GetFilesList();
                    fields.Add("treeViewStudies", treeViewStudies.SelectedNode.Text);
                    LoggerUserActions.MouseClick("buttonDelete", Name, fields);

                    if (SelectedNodeTag == MainForm.Instance.CurrentStudyId)
                    {
                        LoggedDialog.ShowError(this, strRes.GetString("In order to delete this study first change the current study"));
                    }
                    else if (RimedDal.Instance.DeleteStudy(this.SelectedNodeTag))
                    {
                        // delete the selected study from the tree.
                        treeViewStudies.Nodes.Remove(this.treeViewStudies.SelectedNode);
                    }
                }
            }
            else
            {
                MessageBox.Show(MainForm.StringManager.GetString("You can not delete default study!"),
                    MainForm.StringManager.GetString("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void treeViewStudies_AfterSelect(object sender, System.Windows.Forms.TreeViewEventArgs e)
        {
            if (this.treeViewStudies.SelectedNode != null
                && this.treeViewStudies.SelectedNode.Index != 0
                && this.treeViewStudies.SelectedNode.FirstNode == null)
            {
                this.buttonDelete.Enabled = true;
            }
            else
            {
                this.buttonDelete.Enabled = false;
            }
        }
    }
}
