using System;
using Rimed.TCD.DSPData;

namespace Rimed.TCD.GUI
{
    public class SingleHistoryEvent
    {
	    public const string HIT_PREFIX = "Hit";

        public SingleHistoryEvent(Guid eventIndex, Guid examinationIndex, int fileIndex, int indexInFile, DateTime eventTime, bool deleted, GlobalTypes.EEventType eventType, String eventName, double vmrPrimary, double vmrSecondary, double trendSiganlIndexAtEventTime, short flowChangePrimary, short flowChangeSecondary, bool evokedFlowEvent, GlobalTypes.EFHitType hitType, DateTime? examStartTime = null)
        {
			EventIndex					= eventIndex;
            EventName					= eventName;
            ExaminationIndex			= examinationIndex;
            FileIndex					= fileIndex;
	        IndexInFile					= (indexInFile % RawDataFile.FILE_MAX_DSP_MESSAGES);	//Override bug #474   -  indexInFile is relative to file start for hits and relative for examination start for other events.
            Deleted						= deleted;
            EventType					= eventType;
            VmrPrimary					= vmrPrimary;
            VmrSecondary				= vmrSecondary;
            TrendSiganlIndexAtEventTime = trendSiganlIndexAtEventTime;
            FlowChangePrimary			= flowChangePrimary;
            FlowChangeSecondary			= flowChangeSecondary;
            EvokedFlowEvent				= evokedFlowEvent;
            HitType						= hitType;
			IsHitEvent					= EventName.Contains(HIT_PREFIX) && (EventType == GlobalTypes.EEventType.HitEvent);
			AbsolutIndex				= ExaminationRawData.CalcAbsolutIndex(FileIndex, IndexInFile);
			ElapseTime					= ExaminationRawData.CalcElapseTime(FileIndex, IndexInFile);

			var calcEventTime			= (examStartTime.HasValue) ? examStartTime.Value.Add(ElapseTime) : eventTime;
			EventTime					= calcEventTime;

			//Logger.Trace(string.Format("SingleHistoryEvent.Ctor(eventIndex={0}, fileIndex={1}, indexInFile={2}, EventTime={3}, examStartTime={4}): Calc.ElapseTime={5}, Calc.EventTime={6}, EventTime calc.-input diff.={7}", EventIndex, FileIndex, IndexInFile, EventTime.ToString("HH:mm:ss"), (examStartTime.HasValue ? examStartTime.Value.ToString("HH:mm:ss") : "NULL"), ElapseTime, calcEventTime.ToString("HH:mm:ss"), calcEventTime.Subtract(eventTime)));
		}

		public bool IsHitEvent { get; private set; }

        public string EventName { get; private set; }

        public Guid EventIndex { get; private set; }

        public Guid ExaminationIndex { get; private set; }

        public Guid GateIndex { get; set; }

        public int FileIndex { get; private set; }

		public int IndexInFile { get; private set; }

		public int AbsolutIndex { get; private set; }

        public DateTime EventTime { get; private set; }

        public bool Deleted { get; set; }               //Ofer, v2.0.4.16: BugFix #094

        public GlobalTypes.EEventType EventType { get; private set; }

        public GlobalTypes.EFHitType HitType { get; private set; }

        public double HitVelocity { get; set; }

        public double HitEnergy { get; set; }

        public double AlarmValue { get; set; }

        public double VmrSecondary { get; private set; }

        public double VmrPrimary { get; private set; }

        public double TrendSiganlIndexAtEventTime { get; private set; }

        public short FlowChangePrimary { get; private set; }

        public short FlowChangeSecondary { get; private set; }

        public bool EvokedFlowEvent { get; private set; }

        public TimeSpan ElapseTime { get; private set; }

        public override string ToString()
        {
            return string.Format("EventName={0}, EventIndex={1}, ExaminationIndex={2}, GateIndex={3}, FileIndex={4}, IndexInFile={5}, EventTime={6}, Deleted={7}, EventType={8},Type={9}"
                                    + ", Velocity={10}, Energy={11}, AlarmValue={12}, VMRSecondary={13}, VMRPrimary={14}, TrendSiganlIndexAtEventTime={15}, FlowChangePrimary={16}, FlowChangeSecondary={17}, EvokedFlowEvent={18}, ElapseTime={19}"
                                    , EventName, EventIndex, ExaminationIndex, GateIndex, FileIndex, IndexInFile, EventTime, Deleted, EventType, HitType,
                                    HitVelocity, HitEnergy, AlarmValue, VmrSecondary, VmrPrimary, TrendSiganlIndexAtEventTime, FlowChangePrimary, FlowChangeSecondary, EvokedFlowEvent, ElapseTime
                                 );
        }
    }
}
