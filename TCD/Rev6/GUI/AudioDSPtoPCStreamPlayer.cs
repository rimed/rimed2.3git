﻿//#define AUDIO_SAMPLES_WRITE

using System;
using System.Collections.Generic;
using System.Threading;
using Rimed.Framework.Audio;
using Rimed.Framework.Config;
using Rimed.Framework.Common;
using Rimed.Framework.Threads;
using Rimed.TCD.DSPData;
using Rimed.TCD.DspBlock;

namespace Rimed.TCD.GUI
{
    public class AudioDSPtoPCStreamPlayer: Disposable
    {
        #region Class / Static members
		private const string					CLASS_NAME = "AudioDSPtoPCStreamPlayer";

        private const byte						DEFAULT_PRF								= 6;

		/// <summary>Ignore posted buffers when this threshold reached. (2 * ~24 = ~48 msec)</summary>
	    private const int						PENDING_PLAY_BUFFERS_IGNORE_THRESHOLD	= 1;

	    private const int						THREAD_WAIT_TIMEOUT_MS = 500;

		/// <summary>Gain multilayer increased as long as the speaker volume (0 .... 1.0) is below this limit. </summary>
	    private const double					GAIN_MULTIPLAIER_TOP_LIMIT				= 0.8;

		private static readonly bool			s_cfgEnablePcSpeakers		= true;
		private static readonly object			s_playerLocker				= new object();
		/// <summary>Thread iteration wait object.</summary>
		private static			AutoResetEvent	s_whIterationWait;
		private static readonly Thread			s_waveOutInvokerThread;
		private static readonly Queue<RDspToPc> s_dspToPcPlaybackQueue		= new Queue<RDspToPc>();
		private static readonly AudioSamplesFIR	s_audioSamplesFIR;

		private static AudioDSPtoPCStreamPlayer	s_player;

		static AudioDSPtoPCStreamPlayer()
		{
		    s_cfgEnablePcSpeakers = AppConfig.GetConfigValue("Audio:PCSpeakers.Enable", s_cfgEnablePcSpeakers);
	        if (!s_cfgEnablePcSpeakers)
		        return;

			try
			{
				s_audioSamplesFIR	= new AudioSamplesFIR(DEFAULT_PRF);
				setPlayer(s_audioSamplesFIR.AudioSampleRate);
			}
			catch (Exception ex)
            {
                Logger.LogError(ex);
                disableSoundPlayer(ex.Message);

	            return;
            }

	        s_waveOutInvokerThread = new Thread(waveOutInvokerThreadDelegate);
	        s_waveOutInvokerThread.IsBackground = true;
	        s_waveOutInvokerThread.SetName("WaveOutInvokerThread");
			s_waveOutInvokerThread.Priority = ThreadPriority.AboveNormal;
	        s_waveOutInvokerThread.Start();
        }

        private static bool		setPlayer(int samplesPerSec, short channels = 2, short bitsPerSample = 32, EWaveFormatTag fmtTag = EWaveFormatTag.PCM)
        {
			Logger.LogDebug("{0}.setPlayer(samplesPerSec={1}, channels={2}, bitsPerSample={3}, Tag={4})", CLASS_NAME, samplesPerSec, channels, bitsPerSample, fmtTag);

			if (samplesPerSec <= 0)
			{
				Logger.LogWarning("{0}.setPlayer(samplesPerSec={1}, channels={2}, bitsPerSample={3}, Tag={4}). ABORT. samplesPerSec can NOT be equal or less then zero.", CLASS_NAME, samplesPerSec, channels, bitsPerSample, fmtTag);
				return false;
			}

			//var samplesPerSecond    = (samplesPerSec > RGateAudioSamples.MAX_SAMPLES_PER_SECOND ? RGateAudioSamples.MAX_SAMPLES_PER_SECOND : samplesPerSec);
			//var fmt					= new WaveFormat(fmtTag, channels, samplesPerSecond, bitsPerSample);
			var fmt = new WaveFormat(fmtTag, channels, samplesPerSec, bitsPerSample);

            var prevPlayer  = s_player;
            var volume      = (prevPlayer == null ? new Volume(1F) : prevPlayer.Volume);
			
			lock (s_playerLocker)
				s_player = new AudioDSPtoPCStreamPlayer(fmt, volume);

			if (prevPlayer != null)
			{
				prevPlayer.reset();
				prevPlayer.TryDispose();
			}

            return s_player.IsOpened;
        }

		private static int s_dropPostedAudioCounter = 0;
		private static int s_abortNoiseCounter = 0;
		private static void waveOutInvokerThreadDelegate()
		{
			Logger.LogInfo("{0}.waveOutInvokerThreadDelegate(): START.", CLASS_NAME);
			s_whIterationWait = new AutoResetEvent(false);
			while (true)
			{
				try
				{
					if (s_dspToPcPlaybackQueue.Count == 0)
					{
						s_whIterationWait.WaitOne(THREAD_WAIT_TIMEOUT_MS);	
						continue;
					}

					// Get next pending message
					RDspToPc dspToPc;
					lock (s_dspToPcPlaybackQueue)
						dspToPc = s_dspToPcPlaybackQueue.Dequeue();

					//Filter noise
					if (AppSettings.Active.AudioNoiseMuteEnabled)
					{
						if (isNoiseDetected(dspToPc))
							s_abortNoiseCounter = AppSettings.Active.AudioNoiseMuteDuration; 

						if (s_abortNoiseCounter > 0)
						{
							s_abortNoiseCounter--;
							continue;
						}
					}

					// Check sample play rate
					if (s_audioSamplesFIR.AudioSampleRate != s_player.WavFormat.SamplesPerSecond)
					{
						if (!setPlayer(s_audioSamplesFIR.AudioSampleRate))
							continue;
					}

					try
					{
						// Convert I & Q data to audio data
						var wavBuffer = getPlaybackBuffer(dspToPc, Gate2Hear);
						if (wavBuffer != null)
						{
							s_player.write(wavBuffer);

							//return buffer to sampler buffer pool
							s_audioSamplesFIR.BufferPoolPush(wavBuffer);
						}
					}
					catch (Exception ex)
					{
						Logger.LogError(ex);
						disableSoundPlayer(ex.Message);

						break;								// Exit thread
					}
				}
				catch (ThreadAbortException)
				{
					Logger.LogInfo("{0}.waveOutInvokerThreadDelegate(): ThreadAbortException.", CLASS_NAME);
					break;									// Exit thread
				}
				catch (Exception ex)
				{
					Logger.LogError(ex);
					Thread.Sleep(THREAD_WAIT_TIMEOUT_MS);
					Reset();
				}
			}

			var wHdl = s_whIterationWait;
			lock (s_dspToPcPlaybackQueue)
			{
				s_dspToPcPlaybackQueue.Clear();
				s_whIterationWait = null;
			}

			// Release ManualResetEvent
			if (wHdl != null)
			{
				wHdl.Close();
				wHdl.Dispose();
			}

			Logger.LogInfo("{0}.waveOutInvokerThreadDelegate(): END.", CLASS_NAME);
		}

		///// <summary>Calculate sample rate based on prf (Samples per column = prf * 16, Samples per second = Samples per column / column duration).</summary>
		///// <param name="pulseRepetitionRate">Pulse repetition rate. Supported values: 1, 2, 3, 4, 6, 8, 12, 24 </param>
		///// <returns>Sample rate = Samples per second</returns>
		///// <remarks>Max samples per second: RGateAudioSamples.MAX_SAMPLES_PER_SECOND</remarks>
		//private static int		prfToSamplesPerSecond(byte pulseRepetitionRate)
		//{
		//    var value = pulseRepetitionRate * 16.0 * 1000.0 / DspBlockConst.COLUMN_DURATION;
		//    if (value > RGateAudioSamples.MAX_SAMPLES_PER_SECOND)
		//        value = RGateAudioSamples.MAX_SAMPLES_PER_SECOND;

		//    return (int)value;
		//}

		//private static void		play(byte[] iqBuffer)
		//{
		//    if (iqBuffer == null || !Enabled)
		//        return;

		//    try
		//    {
		//        var wavBuffer = s_audioSamplesFIR.ConvertIQBuffer(iqBuffer);
		//        if (s_audioSamplesFIR.AudioSampleRate != s_player.WavFormat.SamplesPerSecond)
		//        {
		//            if (!setPlayer(s_audioSamplesFIR.AudioSampleRate))
		//                return;
		//        }

		//        s_player.write(wavBuffer);
		//    }
		//    catch (Exception ex)
		//    {
		//        Logger.LogError(ex);
		//        disableSoundPlayer(ex.Message);
		//    }
		//}

		private static byte[]	getPlaybackBuffer(RDspToPc dspToPc, int gateToHear)
		{
			if (dspToPc == null)
				return null;

			byte[] wavBuffer;
			try
			{
				wavBuffer = s_audioSamplesFIR.ConvertIQBuffer(dspToPc.Buffer, dspToPc.GetTimeGateMemStartOffset(gateToHear));
			}
			catch (Exception ex)
			{
				Logger.LogError(ex, string.Format("{0}.PostPlay(Sample.Numerator={1}, gateToHear={2}) Fail to convert audio play buffer.", CLASS_NAME, dspToPc.Numerator, gateToHear));
				if (gateToHear == 0)
					return null;

				wavBuffer = getPlaybackBuffer(dspToPc, 0);
			}

			#region DEBUG: AUDIO_SAMPLES_WRITE
#if AUDIO_SAMPLES_WRITE
			s_debugAudioSamples.Add(wavBuffer);
			s_debugDspToPc.Add(dspToPc);
			s_datalength += wavBuffer.Length;
#endif
			#endregion

			return wavBuffer;
		}

		private static void		disableSoundPlayer(string msg, bool isShowNotification = true)
		{
			lock (s_playerLocker)
			{
				s_player.TryDispose();
				s_player = null;
			}

			if (isShowNotification)
				LoggedDialog.ShowNotification(MainForm.Instance, string.Format("TCD Audio player error.\n{0}\n\nPlayer is disabled.", msg), "TDC Audio player error");
		}

		private static bool		isNoiseDetected(RDspToPc dspToPc)
		{
			var gateToHear = LayoutManager.CurrentGateId;
			
			var fftGate = dspToPc.FFTSpectrumBlock.GetGate(gateToHear);
			for (var i = 0; i < DspBlockConst.NUM_OF_COLUMNS; i++)
			{
				var fftColumn = fftGate.GetColumn(i);
				if (isNoiseDetected(fftColumn))
					return true;
			}

			return false;
		}

		private static bool		isNoiseDetected(RFFTColumn fftCol)
		{
			var bottomCnt = 1;
			var avgBottom = 0;

			var topCnt = 1;
			var avgTop = 0;

			for (var i = 0; i < AppSettings.Active.AudioNoiseMuteExtent; i++)
			{
				var b = fftCol.GetIndex(i);
				if (SpectrumGraph.IsFFTValueInRange(b))
				{
					avgTop += b;
					topCnt++;
				}

				b = fftCol.GetIndex(DspBlockConst.NUM_OF_PIXELS_IN_COLMN - i);
				if (SpectrumGraph.IsFFTValueInRange(b))
				{
					avgBottom += b;
					bottomCnt++;
				}
			}
			avgTop /= topCnt;
			avgBottom /= bottomCnt;

			return (avgTop > AppSettings.Active.AudioNoiseMuteThreshold && avgBottom > AppSettings.Active.AudioNoiseMuteThreshold);
		}

		private static bool		Enabled { get { return s_player != null && s_whIterationWait != null; } }


		public static int		Gate2Hear { get; set; }

	    public static int		PRF
	    {
			get
			{
				if (!Enabled)
					return DEFAULT_PRF;

				return s_audioSamplesFIR.PRF;
			}
			set
			{
				if (!Enabled)
					return;

				s_audioSamplesFIR.PRF = value;
				Reset();
			}
	    }

		public static void		Reset()
		{
			if (!Enabled)
				return;

			lock (s_dspToPcPlaybackQueue)
				s_dspToPcPlaybackQueue.Clear();

			s_player.reset();
			s_abortNoiseCounter			= 0;
			s_dropPostedAudioCounter	= 0;
		}

		public static bool		PostPlay(RDspToPc dspToPc)
		{
			if (!Enabled)
				return false;

			if (dspToPc == null || !dspToPc.IsValid)
			{
				Logger.LogDebug("{0}.PostPlay(..): dspToPc == null OR dspToPc is NOT valid", CLASS_NAME);
				return false;
			}

			if (s_dspToPcPlaybackQueue.Count >= PENDING_PLAY_BUFFERS_IGNORE_THRESHOLD)
			{
				Logger.LogWarning("{0}.PostPlay() pending queue is full, Posted message ignored. Current pending messages = {1}.", CLASS_NAME, s_dspToPcPlaybackQueue.Count);

				return false;
			}
			

			lock (s_dspToPcPlaybackQueue)
			{
				if (s_whIterationWait == null)
					return false;

				//Drop every 42nd packet (24 msec every 1sec) - Play time exceeded post time => on a few hrs study sound playback is delayed.
				//TODO: Replace workaround with correct calculation of play time
				//s_dropPostedAudioCounter++;
				//if (s_dropPostedAudioCounter > 42)	//TODO: Convert to config param
				//{
				//    s_dropPostedAudioCounter = 0;
				//    return false;
				//}

				s_dspToPcPlaybackQueue.Enqueue(dspToPc);
			}

			s_whIterationWait.Set();

			return true;
		}

		public static void		SetVolume(int value)
		{
			if (!Enabled)
				return;

			if (value > GlobalSettings.VOLUME_MAX)
				value = GlobalSettings.VOLUME_MAX;
			else if (value < GlobalSettings.VOLUME_MIN)
				value = GlobalSettings.VOLUME_MIN;

			var volRatio	=(float)value / (GlobalSettings.VOLUME_MAX - GlobalSettings.VOLUME_MIN);
			try
			{
				lock (s_playerLocker)
				{
					if (s_player != null)
						s_player.Volume = new Volume(volRatio); 
				}
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);
				disableSoundPlayer(ex.Message);
			}

			var multiplier = s_audioSamplesFIR.GainMultiplier;
			if (volRatio < GAIN_MULTIPLAIER_TOP_LIMIT)
			{
				multiplier = ((1 + volRatio) * AppSettings.Active.AudioPCGainMultiplier - AppSettings.Active.AudioPCGainMultiplier) / GAIN_MULTIPLAIER_TOP_LIMIT;
				s_audioSamplesFIR.GainMultiplier = multiplier;
			}

			Logger.LogDebug("{0}.setVolume(value={1}): Volume={2}, Multiplier={3}, GainMultiplayer={4}", CLASS_NAME, value, volRatio, multiplier, s_audioSamplesFIR.GainMultiplier);
		}


		#region DEBUG: AUDIO_SAMPLES_WRITE
#if AUDIO_SAMPLES_WRITE
		public static void LogDebugTests()
		{
			logAudioSamples();
		}

		private struct RTimeDomainValue
		{
			public short I;
			public short Q;
		}
		private const int ARRAY_SIZE = 31;
	    private const int GAIN_MULTIPLAYER = 1;
		private static int s_cylicArrStartIndex = ARRAY_SIZE - 1;
		private static readonly int[] s_hilbertCo = new int[]{111, 0, 192, 0, 440, 0, 922, 0, 1752, 0, 3212, 0, 6343, 0, 20651
																, 0
																, -20651, 0, -6343, 0, -3212, 0, -1752, 0, -922, 0, -440, 0, -192, 0, -111
																
															};
		private static readonly int[] s_lfpCo = new int[] { -34, 56, -90, 132, -167, 164, -85, -103, 426, -884, 1450, -2073, 2678, -3186, 3526
															, 27197 
															, 3526, -3186, 2678, -2073, 1450, -884, 426, -103, -85, 164, -167, 132, -90, 56,-34
														  };

		private static readonly RTimeDomainValue[] s_samplesDataValue = new RTimeDomainValue[ARRAY_SIZE];
		//private static byte[] processIandQ(RTimeDomainValue[] data)
		//{
		//    if (data == null)
		//        return null;

		//    var waveBuffer = new byte[data.Length *2 * 4];	//items * 2 values (Fwd, Rev) * 4 byte (value size)
		//    using (var sw = new MemoryStream(waveBuffer))
		//    {
		//        using (var bw = new BinaryWriter(sw))
		//        {
		//            for (var sample = 0; sample < data.Length; sample++)
		//            {
		//                var fwd = 0;
		//                var rev = 0;
		//                s_samplesDataValue[s_cylicArrStartIndex].I = data[sample].I;
		//                s_samplesDataValue[s_cylicArrStartIndex].Q = data[sample].Q;

		//                for (var i = 0; i <= ARRAY_SIZE / 2; i++)
		//                {
		//                    var ix = (s_cylicArrStartIndex + i) % ARRAY_SIZE;
		//                    var ix2 = (ix + ARRAY_SIZE - i * 2 - 1);
		//                    if (ix2 >= ARRAY_SIZE)
		//                        ix2 = ix2 - ARRAY_SIZE;

		//                    var hilCoef = s_hilbertCo[i];
		//                    var lpfCoef = s_lfpCo[i];
		//                    var item1 = s_samplesDataValue[ix];
		//                    var item2 = s_samplesDataValue[ix2];

		//                    fwd += ((item1.I + item2.I) * lpfCoef + (item1.Q - item2.Q) * hilCoef);
		//                    rev += ((item1.Q + item2.Q) * lpfCoef + (item1.I - item2.I) * hilCoef);
		//                }

		//                s_cylicArrStartIndex--;
		//                if (s_cylicArrStartIndex < 0)
		//                    s_cylicArrStartIndex = ARRAY_SIZE - 1;

		//                fwd = fwd / (2 ^ 15);
		//                rev = rev / (2 ^ 15);
		//                bw.Write(fwd);
		//                bw.Write(rev);
		//            }
		//        }
		//    }

		//    return waveBuffer;
		//}


		private static int[,] processIandQ(RTimeDomainValue[] data)
		{
			//Diff=2200003, count=2098, avg=1048
			//Diff=6100009, count=6404, avg=952

			//if (data == null)
			//    return null;

			var waveBuffer = new int[2,data.Length];	//items * 2 values (Fwd, Rev)
			var pos = 0;
			for (var sample = 0; sample < data.Length; sample++)
			{
				var iLpf = 0;
				var qLpf = 0;

				var iHil = 0;
				var qHil = 0;

				s_samplesDataValue[0].I = data[sample].I;
				s_samplesDataValue[0].Q = data[sample].Q;

				for (var i = 0; i < ARRAY_SIZE; i++)
				{
					var coeffHil = s_hilbertCo[i];
					var coeffLpf = s_lfpCo[i];
					var item1 = s_samplesDataValue[i];

					iLpf += item1.I * coeffLpf;
					qLpf += item1.Q * coeffLpf;

					iHil += item1.I * coeffHil;
					qHil += item1.Q * coeffHil;
				}

				waveBuffer[0, pos] = (iLpf + qHil) * GAIN_MULTIPLAYER;
				waveBuffer[1, pos] = (iHil + qLpf) * GAIN_MULTIPLAYER;
				pos++;


				for (int i = s_samplesDataValue.Length -1; i > 0; i--)
				{
					s_samplesDataValue[i] = s_samplesDataValue[i - 1];
				}
			}

			return waveBuffer;
		}


		//private static byte[] processIandQ_Bytes(RTimeDomainValue[] data)
		//{
		//    if (data == null)
		//        return null;

		//    var waveBuffer = new byte[data.Length * 4 * 2];	//items * 2 values (Fwd, Rev) * sizeof(int)
		//    var pos = 0;
		//    for (var sample = 0; sample < data.Length; sample++)
		//    {
		//        var iLpf = 0;
		//        var qLpf = 0;

		//        var iHil = 0;
		//        var qHil = 0;

		//        s_samplesDataValue[0].I = data[sample].I;
		//        s_samplesDataValue[0].Q = data[sample].Q;

		//        for (var i = 0; i < ARRAY_SIZE; i++)
		//        {
		//            var coeffHil = s_hilbertCo[i];
		//            var coeffLpf = s_lfpCo[i];
		//            var item1 = s_samplesDataValue[i];

		//            iLpf += item1.I * coeffLpf;
		//            qLpf += item1.Q * coeffLpf;

		//            iHil += item1.I * coeffHil;
		//            qHil += item1.Q * coeffHil;
		//        }

		//        var fwd = (iLpf + qHil) * GAIN_MULTIPLAYER;
		//        var rev = (iHil + qLpf) * GAIN_MULTIPLAYER;

		//        waveBuffer[pos++] = (byte)(fwd & 0x000000FF);
		//        waveBuffer[pos++] = (byte)((fwd & 0x0000FF00) >> 8);
		//        waveBuffer[pos++] = (byte)((fwd & 0x00FF0000) >> 16);
		//        waveBuffer[pos++] = (byte)((fwd & 0xFF000000) >> 24);

		//        waveBuffer[pos++] = (byte)(rev & 0x000000FF);
		//        waveBuffer[pos++] = (byte)((rev & 0x0000FF00) >> 8);
		//        waveBuffer[pos++] = (byte)((rev & 0x00FF0000) >> 16);
		//        waveBuffer[pos++] = (byte)((rev & 0xFF000000) >> 24);

		//        for (int i = s_samplesDataValue.Length - 1; i > 0; i--)
		//        {
		//            s_samplesDataValue[i] = s_samplesDataValue[i - 1];
		//        }
		//    }

		//    return waveBuffer;
		//}

		private static bool processIandQ_Bytes(RTimeDomainValue[] data, byte[] waveBuffer)
		{
			//Diff=2000003, count=2098, avg=953
			//Diff=2000003, count=2127, avg=940
			//Diff=2600004, count=2573, avg=1010
			//Diff=1800003, count=1775, avg=1014
			//Diff=5900008, count=6404, avg=921

			//if (data == null || waveBuffer == null)
			//    return false;

			var pos = 0;
			for (var sample = 0; sample < data.Length; sample++)
			{
				var iLpf = 0;
				var qLpf = 0;

				var iHil = 0;
				var qHil = 0;

				//System.Diagnostics.Debug.WriteLine("[RIMED] TEST01: data[{0}]=({1},{2}), Pos={3}", sample, data[sample].I, data[sample].Q, pos);
				s_samplesDataValue[0].I = data[sample].I;
				s_samplesDataValue[0].Q = data[sample].Q;

				for (var i = 0; i < ARRAY_SIZE; i++)
				{
					var coeffHil = s_hilbertCo[i];
					var coeffLpf = s_lfpCo[i];
					var item1 = s_samplesDataValue[i];

					iLpf += item1.I * coeffLpf;
					qLpf += item1.Q * coeffLpf;

					iHil += item1.I * coeffHil;
					qHil += item1.Q * coeffHil;
				}

				var fwd = (iLpf + qHil) * GAIN_MULTIPLAYER;
				var rev = (iHil + qLpf) * GAIN_MULTIPLAYER;
				waveBuffer[pos++] = (byte)(fwd & 0x000000FF);
				waveBuffer[pos++] = (byte)((fwd & 0x0000FF00) >> 8);
				waveBuffer[pos++] = (byte)((fwd & 0x00FF0000) >> 16);
				waveBuffer[pos++] = (byte)((fwd & 0xFF000000) >> 24);

				waveBuffer[pos++] = (byte)(rev & 0x000000FF);
				waveBuffer[pos++] = (byte)((rev & 0x0000FF00) >> 8);
				waveBuffer[pos++] = (byte)((rev & 0x00FF0000) >> 16);
				waveBuffer[pos++] = (byte)((rev & 0xFF000000) >> 24);

				for (int i = s_samplesDataValue.Length - 1; i > 0; i--)
				{
					s_samplesDataValue[i] = s_samplesDataValue[i - 1];
				}
			}

			return true;
		}

		private static bool processIandQ_Bytes2(byte[] iqBuff, int size, ref RTimeDomainValue prev, byte[] resultBuffer)
		{
			//processIandQ_Bytes2: Diff=2500003, count=2573, avg=971
			
			//processIandQ_Bytes3: Diff=1400002, count=1775, avg=788
			//processIandQ_Bytes3: Diff=4700007, count=6404, avg=733
			var readPos = 0;
			var writePos = 0;
			for (int i = 0; i < size; i += 2)
			{
				var currI = BitConverter.ToInt16(iqBuff, readPos);
				var currQ = BitConverter.ToInt16(iqBuff, readPos + 2);
				readPos += 4;

				prev.I = (short)((currI + prev.I) / 2);
				prev.Q = (short)((currQ + prev.Q) / 2);
				writePos = processIandQ_Bytes3(ref prev, resultBuffer, writePos, i);

				prev.I = currI;
				prev.Q = currQ;
				writePos = processIandQ_Bytes3(ref prev, resultBuffer, writePos, i + 1);
			}

			return true;
		}

		//private static int processIandQ_Bytes2(ref RTimeDomainValue item, byte[] resultBuffer, int pos, int sample)
		//{
		//    //System.Diagnostics.Debug.WriteLine("[RIMED] TEST02: data[{0}]=({1},{2}), Pos={3}", sample, item.I, item.Q, pos);

		//    var iLpf = 0;
		//    var qLpf = 0;

		//    var iHil = 0;
		//    var qHil = 0;

		//    s_samplesDataValue[0] = item;
		//    for (var i = 0; i < ARRAY_SIZE; i++)
		//    {
		//        var coeffHil = s_hilbertCo[i];
		//        var coeffLpf = s_lfpCo[i];
		//        var item1 = s_samplesDataValue[i];

		//        iLpf += item1.I * coeffLpf;
		//        qLpf += item1.Q * coeffLpf;

		//        iHil += item1.I * coeffHil;
		//        qHil += item1.Q * coeffHil;
		//    }

		//    var fwd = (iLpf + qHil) * GAIN_MULTIPLAYER;
		//    var rev = (iHil + qLpf) * GAIN_MULTIPLAYER;

		//    resultBuffer[pos++] = (byte)(fwd & 0x000000FF);
		//    resultBuffer[pos++] = (byte)((fwd & 0x0000FF00) >> 8);
		//    resultBuffer[pos++] = (byte)((fwd & 0x00FF0000) >> 16);
		//    resultBuffer[pos++] = (byte)((fwd & 0xFF000000) >> 24);

		//    resultBuffer[pos++] = (byte)(rev & 0x000000FF);
		//    resultBuffer[pos++] = (byte)((rev & 0x0000FF00) >> 8);
		//    resultBuffer[pos++] = (byte)((rev & 0x00FF0000) >> 16);
		//    resultBuffer[pos++] = (byte)((rev & 0xFF000000) >> 24);

		//    for (int i = s_samplesDataValue.Length - 1; i > 0; i--)
		//    {
		//        s_samplesDataValue[i] = s_samplesDataValue[i - 1];
		//    }

		//    return pos;
		//}

		private static int processIandQ_Bytes3(ref RTimeDomainValue item, byte[] resultBuffer, int pos, int sample)
		{
			var iLpf = 0;
			var qLpf = 0;

			var iHil = 0;
			var qHil = 0;

			s_samplesDataValue[0] = item;
			for (var i = 0; i <= ARRAY_SIZE/2; i++)
			{
				var coeffHil = s_hilbertCo[i];
				var coeffLpf = s_lfpCo[i];
				var item1 = s_samplesDataValue[i];
				var item2 = s_samplesDataValue[ARRAY_SIZE -1-i];

				iLpf += (item1.I + item2.I)* coeffLpf;
				qLpf += (item1.Q +item2.Q) * coeffLpf;

				iHil += (item1.I - item2.I)* coeffHil;
				qHil += (item1.Q - item2.Q )* coeffHil;
			}

			var fwd = (iLpf + qHil) * GAIN_MULTIPLAYER;
			var rev = (iHil + qLpf) * GAIN_MULTIPLAYER;

			resultBuffer[pos++] = (byte)(fwd & 0x000000FF);
			resultBuffer[pos++] = (byte)((fwd & 0x0000FF00) >> 8);
			resultBuffer[pos++] = (byte)((fwd & 0x00FF0000) >> 16);
			resultBuffer[pos++] = (byte)((fwd & 0xFF000000) >> 24);

			resultBuffer[pos++] = (byte)(rev & 0x000000FF);
			resultBuffer[pos++] = (byte)((rev & 0x0000FF00) >> 8);
			resultBuffer[pos++] = (byte)((rev & 0x00FF0000) >> 16);
			resultBuffer[pos++] = (byte)((rev & 0xFF000000) >> 24);

			for (int i = s_samplesDataValue.Length - 1; i > 0; i--)
			{
				s_samplesDataValue[i] = s_samplesDataValue[i - 1];
			}

			return pos;
		}


		
		//private static bool processIandQ_Bytes(RTimeDomainValue[] data, byte[] waveBuffer)
		//{
		//    //if (data == null || waveBuffer == null)
		//    //    return false;

		//    var pos = 0;
		//    for (var sample = 0; sample < data.Length; sample++)
		//    {
		//        var iLpf = 0;
		//        var qLpf = 0;

		//        var iHil = 0;
		//        var qHil = 0;

		//        s_samplesDataValue[0].I = data[sample].I;
		//        s_samplesDataValue[0].Q = data[sample].Q;

		//        for (var i = 0; i <= ARRAY_SIZE /2; i++)
		//        {
		//            var coeffHil = s_hilbertCo[i];
		//            var coeffLpf = s_lfpCo[i];
		//            var item1 = s_samplesDataValue[i];
		//            var item2 = s_samplesDataValue[ARRAY_SIZE -1-i];

		//            //System.Diagnostics.Debug.WriteLine("[RIMED] {0} <-> {1}, Hil={2}, Lpf={3}", i, ARRAY_SIZE - 1 - i, coeffHil, coeffLpf);

		//            iLpf += (item1.I+item2.I) * coeffLpf;
		//            qLpf += (item1.Q+item2.Q) * coeffLpf;

		//            iHil += (item1.I-item2.I) * coeffHil;
		//            qHil += (item1.Q-item2.Q) * coeffHil;
		//        }

		//        var fwd = (iLpf + qHil) * GAIN_MULTIPLAYER;
		//        var rev = (iHil + qLpf) * GAIN_MULTIPLAYER;

		//        waveBuffer[pos++] = (byte)(fwd & 0x000000FF);
		//        waveBuffer[pos++] = (byte)((fwd & 0x0000FF00) >> 8);
		//        waveBuffer[pos++] = (byte)((fwd & 0x00FF0000) >> 16);
		//        waveBuffer[pos++] = (byte)((fwd & 0xFF000000) >> 24);

		//        waveBuffer[pos++] = (byte)(rev & 0x000000FF);
		//        waveBuffer[pos++] = (byte)((rev & 0x0000FF00) >> 8);
		//        waveBuffer[pos++] = (byte)((rev & 0x00FF0000) >> 16);
		//        waveBuffer[pos++] = (byte)((rev & 0xFF000000) >> 24);

		//        for (int i = s_samplesDataValue.Length - 1; i > 0; i--)
		//        {
		//            s_samplesDataValue[i] = s_samplesDataValue[i - 1];
		//        }
		//    }

		//    return true;
		//}

		//private static int processIandQ_IQData(RTimeDomainValue[] data, byte[] waveBuffer, int start)
		//{
		//    //if (data == null || waveBuffer == null)
		//    //    return false;

		//    var pos = 0;
		//    for (var sample = 0; sample < data.Length; sample++)
		//    {
		//        var iLpf = 0;
		//        var qLpf = 0;

		//        var iHil = 0;
		//        var qHil = 0;

		//        s_samplesDataValue[start].I = data[sample].I;
		//        s_samplesDataValue[start].Q = data[sample].Q;

		//        for (var i = 0; i < ARRAY_SIZE; i++)
		//        {
		//            var coeffHil = s_hilbertCo[i];
		//            var coeffLpf = s_lfpCo[i];

		//            var ix = (start + i) % ARRAY_SIZE;
		//            var item1 = s_samplesDataValue[ix];

		//            iLpf += item1.I * coeffLpf;
		//            qLpf += item1.Q * coeffLpf;

		//            iHil += item1.I * coeffHil;
		//            qHil += item1.Q * coeffHil;
		//        }

		//        var fwd = (iLpf + qHil) * GAIN_MULTIPLAYER;
		//        var rev = (iHil + qLpf) * GAIN_MULTIPLAYER;

		//        waveBuffer[pos++] = (byte)(fwd & 0x000000FF);
		//        waveBuffer[pos++] = (byte)((fwd & 0x0000FF00) >> 8);
		//        waveBuffer[pos++] = (byte)((fwd & 0x00FF0000) >> 16);
		//        waveBuffer[pos++] = (byte)((fwd & 0xFF000000) >> 24);

		//        waveBuffer[pos++] = (byte)(rev & 0x000000FF);
		//        waveBuffer[pos++] = (byte)((rev & 0x0000FF00) >> 8);
		//        waveBuffer[pos++] = (byte)((rev & 0x00FF0000) >> 16);
		//        waveBuffer[pos++] = (byte)((rev & 0xFF000000) >> 24);

		//        start--;
		//        if (start < 0)
		//            start = ARRAY_SIZE - 1;
		//    }

		//    return start;
		//}

		//private static int processIandQ_IQData(RTimeDomainValue[] data, byte[] waveBuffer, int start)
		//{
		//    //if (data == null || waveBuffer == null)
		//    //    return false;

		//    var pos = 0;
		//    for (var sample = 0; sample < data.Length; sample++)
		//    {
		//        var iLpf = 0;
		//        var qLpf = 0;

		//        var iHil = 0;
		//        var qHil = 0;

		//        s_samplesDataValue[start].I = data[sample].I;
		//        s_samplesDataValue[start].Q = data[sample].Q;

		//        for (var i = 0; i <= ARRAY_SIZE/2; i++)
		//        {
		//            var coeffHil = s_hilbertCo[i];
		//            var coeffLpf = s_lfpCo[i];

		//            var ix = (start + i) % ARRAY_SIZE;
		//            var ix2 = (start + ARRAY_SIZE - 1 - i);
		//            if (ix2 >= ARRAY_SIZE)
		//                ix2 = ix2 - ARRAY_SIZE;

		//            var item1 = s_samplesDataValue[ix];
		//            var item2 = s_samplesDataValue[ix2];

		//            iLpf += (item1.I + item2.I) * coeffLpf;
		//            qLpf += (item1.Q + item2.Q) * coeffLpf;

		//            iHil += (item1.I - item2.I) * coeffHil;
		//            qHil += (item1.Q - item2.Q) * coeffHil;
		//        }

		//        var fwd = (iLpf + qHil) * GAIN_MULTIPLAYER;
		//        var rev = (iHil + qLpf) * GAIN_MULTIPLAYER;

		//        waveBuffer[pos++] = (byte)(fwd & 0x000000FF);
		//        waveBuffer[pos++] = (byte)((fwd & 0x0000FF00) >> 8);
		//        waveBuffer[pos++] = (byte)((fwd & 0x00FF0000) >> 16);
		//        waveBuffer[pos++] = (byte)((fwd & 0xFF000000) >> 24);

		//        waveBuffer[pos++] = (byte)(rev & 0x000000FF);
		//        waveBuffer[pos++] = (byte)((rev & 0x0000FF00) >> 8);
		//        waveBuffer[pos++] = (byte)((rev & 0x00FF0000) >> 16);
		//        waveBuffer[pos++] = (byte)((rev & 0xFF000000) >> 24);

		//        start--;
		//        if (start < 0)
		//            start = ARRAY_SIZE - 1;
		//    }

		//    return start;
		//}

		private static int s_datalength = 0;
        private static int s_sampleRateHz = 0;
		private static List<byte[]> s_debugAudioSamples = new List<byte[]>();
		private static List<RDspToPc> s_debugDspToPc = new List<RDspToPc>();


		private static void writeWaveFileHeader2Channels32Bit(BinaryWriter bw, int dataSize)
		{
				// Set the member data from the parameters.
				const short M_NUM_CHANNELS = 2;
				const int M_BITS_PER_SAMPLE = 32;

				// Write the parameters to the file header.

				// RIFF chunk (12 bytes total)
				// Write the chunk IDD ("RIFF", 4 bytes)
				var buffer = new[] { (byte)'R', (byte)'I', (byte)'F', (byte)'F' };
				bw.Write(buffer, 0, 4);

				// File size size (4 bytes) 
				bw.Write(dataSize + 36);

				buffer = new[] { (byte)'W', (byte)'A', (byte)'V', (byte)'E' };
				bw.Write(buffer, 0, 4);

				// Format chunk (24 bytes total) 
				// "fmt " (ASCII characters)
				buffer = new[] { (byte)'f', (byte)'m', (byte)'t', (byte)' ' };
				bw.Write(buffer, 0, 4);

				// Length of format chunk (always 16, 4 bytes)
				bw.Write(16);

				// 2 bytes (always 1)
				bw.Write((short)1);

				// # of channels (2 bytes)
				bw.Write(M_NUM_CHANNELS);

				// Sample rate (4 bytes)
				bw.Write(s_sampleRateHz);

				// Calculate the # of bytes per sample: 1=8 bit Mono, 2=8 bit Stereo or 16 bit Mono, 4=16 bit Stereo
				const short BYTES_PER_SAMPLE = (short)(M_NUM_CHANNELS * (M_BITS_PER_SAMPLE / 8));

				// Write the # of bytes per second (4 bytes)
				var mBytesPerSec = s_sampleRateHz * BYTES_PER_SAMPLE;
				bw.Write(mBytesPerSec);

				// Write the # of bytes per sample (2 bytes)
				bw.Write(BYTES_PER_SAMPLE);

				// Bits per sample (2 bytes)
				bw.Write((short)M_BITS_PER_SAMPLE);

				// Data chunk
				// "data" (ASCII characters)
				buffer = new[] { (byte)'d', (byte)'a', (byte)'t', (byte)'a' };
				bw.Write(buffer, 0, 4);

				// Length of data to follow (4 bytes) - This will be 0 for now
				bw.Write(dataSize);
		}

        private static void logAudioSamples()
        {
            if (s_datalength < 10000)
                return;

	        var timestamp = DateTime.UtcNow.Ticks;
	        const bool IS_WRITE_AUDI_DATA = true;
	        string wavName;

            #region write Audio samples to wave file

			wavName = string.Format(@"C:\Rimed\{0:X16}.AudioSamples.WAV", timestamp);
			using (var fs = new FileStream(wavName, FileMode.Create))
			{
			    using (var bw = new BinaryWriter(fs))
			    {
			        // Set the member data from the parameters.
			        const short M_NUM_CHANNELS = 2;
			        const int M_BITS_PER_SAMPLE = 16;

			        // Write the parameters to the file header.

			        // RIFF chunk (12 bytes total)
			        // Write the chunk IDD ("RIFF", 4 bytes)
			        var buffer = new[] {(byte) 'R', (byte) 'I', (byte) 'F', (byte) 'F'};
			        bw.Write(buffer, 0, 4);

			        // File size size (4 bytes) 
			        bw.Write(s_datalength + 36);

			        buffer = new[] {(byte) 'W', (byte) 'A', (byte) 'V', (byte) 'E'};
			        bw.Write(buffer, 0, 4);

			        // Format chunk (24 bytes total) 
			        // "fmt " (ASCII characters)
			        buffer = new[] {(byte) 'f', (byte) 'm', (byte) 't', (byte) ' '};
			        bw.Write(buffer, 0, 4);

			        // Length of format chunk (always 16, 4 bytes)
			        bw.Write(M_BITS_PER_SAMPLE);

			        // 2 bytes (always 1)
			        bw.Write((short) 1);

			        // # of channels (2 bytes)
			        bw.Write(M_NUM_CHANNELS);

			        // Sample rate (4 bytes)
			        bw.Write(s_sampleRateHz);

			        // Calculate the # of bytes per sample: 1=8 bit Mono, 2=8 bit Stereo or 16 bit Mono, 4=16 bit Stereo
			        const short BYTES_PER_SAMPLE = (short) (M_NUM_CHANNELS *(M_BITS_PER_SAMPLE/8));

			        // Write the # of bytes per second (4 bytes)
			        var mBytesPerSec = s_sampleRateHz*BYTES_PER_SAMPLE;
			        bw.Write(mBytesPerSec);

			        // Write the # of bytes per sample (2 bytes)
			        bw.Write(BYTES_PER_SAMPLE);

			        // Bits per sample (2 bytes)
			        bw.Write((short) M_BITS_PER_SAMPLE);

			        // Data chunk
			        // "data" (ASCII characters)
			        buffer = new[] {(byte) 'd', (byte) 'a', (byte) 't', (byte) 'a'};
			        bw.Write(buffer, 0, 4);

			        // Length of data to follow (4 bytes) - This will be 0 for now
			        bw.Write(s_datalength);

					foreach (var dsptopc in s_debugDspToPc)
			        {
			            var sample = dsptopc.AudioSamples;
						if (sample != null && IS_WRITE_AUDI_DATA)
							bw.Write(sample.WaveBuffer);
			        }
			    }
			}

			#endregion

			#region write raw data file
			//var rawDataName = string.Format(@"C:\Rimed\{0:X16}.Raw.DAT", timestamp);
			//var revDataName = string.Format(@"C:\Rimed\{0:X16}.Rev.DAT",timestamp);
			//var fwdDataName = string.Format(@"C:\Rimed\{0:X16}.Fwd.DAT", timestamp);

			//using (var raws = new FileStream(rawDataName, FileMode.Create))
			//{
			//    using (var revs = new FileStream(revDataName, FileMode.Create))
			//    {
			//        using (var fwds = new FileStream(fwdDataName, FileMode.Create))
			//        {
			//            using (var rawbw = new BinaryWriter(raws))
			//            {
			//                using (var fwdbw = new BinaryWriter(fwds))
			//                {
			//                    using (var revbw = new BinaryWriter(revs))
			//                    {
			//                        foreach (var dsptopc in list)
			//                        {
			//                            var sample = dsptopc.AudioSamples;

			//                            rawbw.Write(sample.WaveBuffer);
			//                            fwdbw.Write(sample.ForwadBuffer);
			//                            revbw.Write(sample.ReverseBuffer);
			//                        }
			//                    }
			//                }
			//            }
			//        }
			//    }
			//}
			#endregion

			#region write wave file from I and Q data
//[RIMED] C:\Rimed\08D0B985F429C233.IQ.BASE.WAV: Diff=4910281, count=4907, avg=1000
//[RIMED] C:\Rimed\08D0B985F429C233.IQ.TEST.01.WAV: Diff=4820276, count=4907, avg=982
//[RIMED] C:\Rimed\08D0B985F429C233.IQ.TEST.02.WAV: Diff=3770216, count=4907, avg=768
//[RIMED] C:\Rimed\08D0B985F429C233.IQ.TEST.03.WAV: Diff=4320248, count=4907, avg=880
//[RIMED] C:\Rimed\08D0B985F429C233.IQ.TEST.04.WAV: Diff=4310246, count=4907, avg=878

		#region base

	        var buffSize = 0;
			foreach (var buff in s_debugAudioSamples)
			{
				buffSize += buff.Length;
			}

			wavName = string.Format(@"C:\Rimed\{0:X16}.IQ.WAV", timestamp);
			using (var fs = new FileStream(wavName, FileMode.Create))
			{
				using (var bw = new BinaryWriter(fs))
				{
					// Set the member data from the parameters.
					const short M_NUM_CHANNELS = 2;
					const int M_BITS_PER_SAMPLE = 32;

					// Write the parameters to the file header.

					// RIFF chunk (12 bytes total)
					// Write the chunk IDD ("RIFF", 4 bytes)
					var buffer = new[] { (byte)'R', (byte)'I', (byte)'F', (byte)'F' };
					bw.Write(buffer, 0, 4);

					// File size size (4 bytes) 
					bw.Write(buffSize + 36);

					buffer = new[] { (byte)'W', (byte)'A', (byte)'V', (byte)'E' };
					bw.Write(buffer, 0, 4);

					// Format chunk (24 bytes total) 
					// "fmt " (ASCII characters)
					buffer = new[] { (byte)'f', (byte)'m', (byte)'t', (byte)' ' };
					bw.Write(buffer, 0, 4);

					// Length of format chunk (always 16, 4 bytes)
					bw.Write(16);

					// 2 bytes (always 1)
					bw.Write((short)1);

					// # of channels (2 bytes)
					bw.Write(M_NUM_CHANNELS);

					// Sample rate (4 bytes)
					bw.Write(s_sampleRateHz);

					// Calculate the # of bytes per sample: 1=8 bit Mono, 2=8 bit Stereo or 16 bit Mono, 4=16 bit Stereo
					const short BYTES_PER_SAMPLE = (short)(M_NUM_CHANNELS * (M_BITS_PER_SAMPLE / 8));

					// Write the # of bytes per second (4 bytes)
					var mBytesPerSec = s_sampleRateHz * BYTES_PER_SAMPLE;
					bw.Write(mBytesPerSec);

					// Write the # of bytes per sample (2 bytes)
					bw.Write(BYTES_PER_SAMPLE);

					// Bits per sample (2 bytes)
					bw.Write((short)M_BITS_PER_SAMPLE);

					// Data chunk
					// "data" (ASCII characters)
					buffer = new[] { (byte)'d', (byte)'a', (byte)'t', (byte)'a' };
					bw.Write(buffer, 0, 4);

					// Length of data to follow (4 bytes) - This will be 0 for now
					bw.Write(buffSize);

					foreach (var buff in s_debugAudioSamples)
					{
						if (buff != null && IS_WRITE_AUDI_DATA)
							bw.Write(buff);
					}
				}
			}
		#endregion

#endregion
			
			//#region write RollingAvarege wave file
			//wavName = string.Format(@"C:\Rimed\{0:X16}.RollingAvarege.WAV", timestamp);
            //using (var fs = new FileStream(wavName, FileMode.Create))
            //{
            //    using (var bw = new BinaryWriter(fs))
            //    {
            //        // Set the member data from the parameters.
            //        const short M_NUM_CHANNELS = 2;
            //        const int M_BITS_PER_SAMPLE = 16;

            //        // Write the parameters to the file header.

            //        // RIFF chunk (12 bytes total)
            //        // Write the chunk IDD ("RIFF", 4 bytes)
            //        var buffer = new[] { (byte)'R', (byte)'I', (byte)'F', (byte)'F' };
            //        bw.Write(buffer, 0, 4);

            //        // File size size (4 bytes) 
            //        bw.Write(s_datalength + 36);

            //        buffer = new[] { (byte)'W', (byte)'A', (byte)'V', (byte)'E' };
            //        bw.Write(buffer, 0, 4);

            //        // Format chunk (24 bytes total) 
            //        // "fmt " (ASCII characters)
            //        buffer = new[] { (byte)'f', (byte)'m', (byte)'t', (byte)' ' };
            //        bw.Write(buffer, 0, 4);

            //        // Length of format chunk (always 16, 4 bytes)
            //        bw.Write(M_BITS_PER_SAMPLE);

            //        // 2 bytes (always 1)
            //        bw.Write((short)1);

            //        // # of channels (2 bytes)
            //        bw.Write(M_NUM_CHANNELS);

            //        // Sample rate (4 bytes)
            //        bw.Write(s_sampleRateHz);

            //        // Calculate the # of bytes per sample: 1=8 bit Mono, 2=8 bit Stereo or 16 bit Mono, 4=16 bit Stereo
            //        const short BYTES_PER_SAMPLE = (short)((M_BITS_PER_SAMPLE / 8) * 2);

            //        // Write the # of bytes per second (4 bytes)
            //        var mBytesPerSec = s_sampleRateHz * BYTES_PER_SAMPLE;
            //        bw.Write(mBytesPerSec);

            //        // Write the # of bytes per sample (2 bytes)
            //        bw.Write(BYTES_PER_SAMPLE);

            //        // Bits per sample (2 bytes)
            //        bw.Write((short)M_BITS_PER_SAMPLE);

            //        // Data chunk
            //        // "data" (ASCII characters)
            //        buffer = new[] { (byte)'d', (byte)'a', (byte)'t', (byte)'a' };
            //        bw.Write(buffer, 0, 4);

            //        // Length of data to follow (4 bytes) - This will be 0 for now
            //        bw.Write(s_datalength);

            //        foreach (var dsptopc in list)
            //        {
            //            var sample = dsptopc.AudioSamples;
            //            for (var i = 0; i < sample.WaveBuffer.Length; i += 2)
            //            {
            //                var saple = (BitConverter.ToInt16(sample.WaveBuffer, i));
            //                saple = s_rollingAvarege.Add(saple);
            //                sample.WaveBuffer[i] = (byte)(saple);
            //                sample.WaveBuffer[i + 1] = (byte)(saple >> 8);
            //            }

            //            bw.Write(sample.WaveBuffer);
            //        }
            //    }
            //}
            //#endregion

            //#region write RollingAvarege wave file
			//wavName = string.Format(@"C:\Rimed\{0:X16}.XXXX.WAV", timestamp);
            //using (var fs = new FileStream(wavName, FileMode.Create))
            //{
            //    using (var bw = new BinaryWriter(fs))
            //    {
            //        // Set the member data from the parameters.
            //        const short M_NUM_CHANNELS = 2;
            //        const int M_BITS_PER_SAMPLE = 16;

            //        // Write the parameters to the file header.

            //        // RIFF chunk (12 bytes total)
            //        // Write the chunk IDD ("RIFF", 4 bytes)
            //        var buffer = new[] { (byte)'R', (byte)'I', (byte)'F', (byte)'F' };
            //        bw.Write(buffer, 0, 4);

            //        // File size size (4 bytes) 
            //        bw.Write(s_datalength + 36);

            //        buffer = new[] { (byte)'W', (byte)'A', (byte)'V', (byte)'E' };
            //        bw.Write(buffer, 0, 4);

            //        // Format chunk (24 bytes total) 
            //        // "fmt " (ASCII characters)
            //        buffer = new[] { (byte)'f', (byte)'m', (byte)'t', (byte)' ' };
            //        bw.Write(buffer, 0, 4);

            //        // Length of format chunk (always 16, 4 bytes)
            //        bw.Write(M_BITS_PER_SAMPLE);

            //        // 2 bytes (always 1)
            //        bw.Write((short)1);

            //        // # of channels (2 bytes)
            //        bw.Write(M_NUM_CHANNELS);

            //        // Sample rate (4 bytes)
            //        bw.Write(s_sampleRateHz);

            //        // Calculate the # of bytes per sample: 1=8 bit Mono, 2=8 bit Stereo or 16 bit Mono, 4=16 bit Stereo
            //        const short BYTES_PER_SAMPLE = (short)((M_BITS_PER_SAMPLE / 8) * 2);

            //        // Write the # of bytes per second (4 bytes)
            //        var mBytesPerSec = s_sampleRateHz * BYTES_PER_SAMPLE;
            //        bw.Write(mBytesPerSec);

            //        // Write the # of bytes per sample (2 bytes)
            //        bw.Write(BYTES_PER_SAMPLE);

            //        // Bits per sample (2 bytes)
            //        bw.Write((short)M_BITS_PER_SAMPLE);

            //        // Data chunk
            //        // "data" (ASCII characters)
            //        buffer = new[] { (byte)'d', (byte)'a', (byte)'t', (byte)'a' };
            //        bw.Write(buffer, 0, 4);

            //        // Length of data to follow (4 bytes) - This will be 0 for now
            //        bw.Write(s_datalength);

            //        foreach (var dsptopc in list)
            //        {
            //            var sample = dsptopc.AudioSamples;
            //            for (var i = 0; i < sample.WaveBuffer.Length; i += 2)
            //            {
            //                var saple = (BitConverter.ToInt16(sample.WaveBuffer, i));

            //                sample.WaveBuffer[i] = (byte)(saple);
            //                sample.WaveBuffer[i + 1] = (byte)(saple >> 8);
            //            }

            //            bw.Write(sample.WaveBuffer);
            //        }
            //    }
            //}
            //#endregion

            //#region write medianFilter wave file
			//wavName = string.Format(@"C:\Rimed\{0:X16}.MedianFilter.WAV", timestamp);
            //using (var fs = new FileStream(wavName, FileMode.Create))
            //{
            //    using (var bw = new BinaryWriter(fs))
            //    {
            //        // Set the member data from the parameters.
            //        const short M_NUM_CHANNELS = 2;
            //        const int M_BITS_PER_SAMPLE = 16;

            //        // Write the parameters to the file header.

            //        // RIFF chunk (12 bytes total)
            //        // Write the chunk IDD ("RIFF", 4 bytes)
            //        var buffer = new[] { (byte)'R', (byte)'I', (byte)'F', (byte)'F' };
            //        bw.Write(buffer, 0, 4);

            //        // File size size (4 bytes) 
            //        bw.Write(s_datalength + 36);

            //        buffer = new[] { (byte)'W', (byte)'A', (byte)'V', (byte)'E' };
            //        bw.Write(buffer, 0, 4);

            //        // Format chunk (24 bytes total) 
            //        // "fmt " (ASCII characters)
            //        buffer = new[] { (byte)'f', (byte)'m', (byte)'t', (byte)' ' };
            //        bw.Write(buffer, 0, 4);

            //        // Length of format chunk (always 16, 4 bytes)
            //        bw.Write(M_BITS_PER_SAMPLE);

            //        // 2 bytes (always 1)
            //        bw.Write((short)1);

            //        // # of channels (2 bytes)
            //        bw.Write(M_NUM_CHANNELS);

            //        // Sample rate (4 bytes)
            //        bw.Write(s_sampleRateHz);

            //        // Calculate the # of bytes per sample: 1=8 bit Mono, 2=8 bit Stereo or 16 bit Mono, 4=16 bit Stereo
            //        const short BYTES_PER_SAMPLE = (short)((M_BITS_PER_SAMPLE / 8) * 2);

            //        // Write the # of bytes per second (4 bytes)
            //        var mBytesPerSec = s_sampleRateHz * BYTES_PER_SAMPLE;
            //        bw.Write(mBytesPerSec);

            //        // Write the # of bytes per sample (2 bytes)
            //        bw.Write(BYTES_PER_SAMPLE);

            //        // Bits per sample (2 bytes)
            //        bw.Write((short)M_BITS_PER_SAMPLE);

            //        // Data chunk
            //        // "data" (ASCII characters)
            //        buffer = new[] { (byte)'d', (byte)'a', (byte)'t', (byte)'a' };
            //        bw.Write(buffer, 0, 4);

            //        // Length of data to follow (4 bytes) - This will be 0 for now
            //        bw.Write(s_datalength);
            //        foreach (var dsptopc in list)
            //        {
            //            var sample = dsptopc.AudioSamples;
            //            for (int col = 0; col < 3; col++)
            //            {
            //                var c = sample.GetColumn(col);
            //                var fwd = medianFilter(c.FwdSamples, 31);
            //                var rev = medianFilter(c.RevSamples, 31);    
            //                for (var i = 0; i < c.SamplesCount; i++)
            //                {
            //                    bw.Write(fwd[i]);
            //                    bw.Write(rev[i]);
            //                }
            //            }

            //            //var fwd = medianFilter(sample.FwdSamples, 31);
            //            //var rev = medianFilter(sample.RevSamples, 31);    

            //            //for (var i = 0; i < fwd.Length; i++)
            //            //{
            //            //    bw.Write(fwd[i]);
            //            //    bw.Write(rev[i]);
            //            //}

            //        }
            //    }
            //}
            //#endregion

		//#region write data samples
			//    var csfSampleFile = string.Format(@"C:\Rimed\{0:X16}.AudioSamples.csv", timestamp);
		//    var runCtr = 0;
		//    var sb = new StringBuilder();
		//    using (var dataWriter = File.AppendText(csfSampleFile))
		//    {
		//        dataWriter.WriteLine("Cntr, Num , RcvTime      , RcvDif, delay, Bytes, Samples, Rate, msgTSDiff, msgTSAccDiff, [0: TS, TSD, TSAccDiff] [1: TS, TSD, TSAccDiff] [2: TS, TSD, TSAccDiff]");
		//        var prevNum = -1;
		//        var prevRcvTime = DateTime.UtcNow;
		//        var prevFwd = -1;
		//        var prevRev = 0;
		//        uint prvTimestamp = 0;
		//        uint prvMsgTimestamp = 0;

		//        var rcvAccm = 0.0;
		//        long tsAcc = -1;
		//        var msgTsacc = 0.0;
		//        foreach (var dsptopc in list)
		//        {
		//            var sample = dsptopc.AudioSamples;

		//            sb.Clear();
		//            runCtr++;
		//            sb.Append(runCtr.ToString("D4")).Append(", ");                   //Cntr
		//            sb.Append(dsptopc.Numerator.ToString("D4")).Append(", ");          // Num
		//            sb.Append(sample.RecieveTimestamp.ToString("HH:mm:ss.ffff")).Append(", ");    //RcvTime
		//            var diff = sample.RecieveTimestamp.Subtract(prevRcvTime).TotalMilliseconds;
		//            sb.Append(diff.ToString("F2")).Append(", ");    //RcvDif
		//            prevRcvTime = sample.RecieveTimestamp;

		//            if (diff >= 24 && diff < 1000)
		//                rcvAccm += (diff -24);
		//            sb.Append(rcvAccm.ToString("F2")).Append(", ");    //delay

		//            sb.Append(sample.IsValid).Append(", ");             //Valid
		//            sb.Append(sample.ByteCount.ToString("D5")).Append(", ");           //Bytes    
		//            sb.Append(sample.SamplesCount.ToString("D4")).Append(",   ");        //Samples
		//            sb.Append(sample.SamplesPerSecond.ToString("D5")).Append(", ");    //Rate

		//            for (var i = 0; i < DspBlockConst.NUM_OF_COLUMNS; i++)
		//            {
		//                var col = sample.GetColumn(i);

		//                if (i == 0)
		//                {
		//                    diff = col.TimeStamp - prvMsgTimestamp;
		//                    if (diff >= 24 && diff < 1000)
		//                    {
		//                        msgTsacc += (diff - 24);
		//                        sb.Append(col.TimeStamp - prvMsgTimestamp).Append(", ");    //msgTSDiff
		//                        sb.Append(msgTsacc.ToString("F2")).Append(", ");                       //msgTSAccDiff
		//                    }

                            

		//                    prvMsgTimestamp = col.TimeStamp;
		//                }
		//                sb.Append("[");
		//                //sb.Append(col.IsValid).Append(" , ");           //Valid
		//                //sb.Append(col.SamplesByteCount).Append(",");  //Byte
		//                //sb.Append(col.SamplesCount).Append(",\t");      //Cnt
		//                sb.Append(col.TimeStamp).Append(",  ");         //TS
		//                var diff2 = col.TimeStamp - prvTimestamp;
		//                sb.Append(diff2).Append(", ");         //TSDiff
		//                prvTimestamp = col.TimeStamp;

		//                if (tsAcc < 0)
		//                    tsAcc = 0;
		//                else
		//                    tsAcc += diff2;


		//                sb.Append(tsAcc).Append("] ");         //TSAccDiff
                        
		//                //sb.Append(col.FwdSamples.Length).Append(",\t"); //Fwd
		//                //sb.Append(col.RevSamples.Length).Append("] "); //Rev



		//                //var colSamplesCount = col.FwdSamples.Length;
		//                //if (colSamplesCount > col.RevSamples.Length)
		//                //    colSamplesCount = col.RevSamples.Length;

		//                //for (var j = 0; j < colSamplesCount; j++)
		//                //{
		//                //    var dbgMsg = string.Format("{0}, {1},{2},{3}, 0x{4:X4}, 0x{5:X4}, {6}, {7}, {8}, {9}, {10}, {11}{12}{13}{14}"
		//                //                                , runCtr++
		//                //                                , i * colSamplesCount + j
		//                //                                , dsptopc.Numerator
		//                //                                , col.TimeStamp
		//                //                                , col.FwdSamples[j], col.RevSamples[j]
		//                //                                , (short)col.FwdSamples[j], (short)col.RevSamples[j]
		//                //                                , col.RevSamples[j] - col.FwdSamples[j]
		//                //                                , col.FwdSamples[j] - prevFwd
		//                //                                , col.RevSamples[j] - prevRev
		//                //                                , ((col.RevSamples[j] - col.FwdSamples[j] == 1) && (col.FwdSamples[j] - prevFwd == 2) && (col.RevSamples[j] - prevRev == 2)) ? "     " : "<==== "
		//                //                                , (col.RevSamples[j] - col.FwdSamples[j] == 1) ? " " : "1"
		//                //                                , (col.FwdSamples[j] - prevFwd == 2 || col.FwdSamples[j] - prevFwd == -65534) ? " " : "2"
		//                //                                , (col.RevSamples[j] - prevRev == 2 || col.RevSamples[j] - prevRev == -65534) ? " " : "3");
		//                //    dataWriter.WriteLine(dbgMsg);

		//                //    prevFwd = col.FwdSamples[j];
		//                //    prevRev = col.RevSamples[j];
		//                //}

		//                //dataWriter.WriteLine();
		//            }

		//            if (prevNum > -1 && prevNum + 1 != dsptopc.Numerator)
		//                sb.Append(", NUMERATOR MISMATCH");

		//            prevNum = dsptopc.Numerator;

		//            dataWriter.WriteLine(sb.ToString());
		//        }
		//    }
		//#endregion
            s_sampleRateHz = 0;
            s_datalength = 0;
        }
#endif
		#endregion
        #endregion


		#region Instance members
		private readonly object			m_waveoutLocker = new object();
        private readonly WaveFormat		m_waveFormat;
        private WaveOut					m_waveOut;

        private AudioDSPtoPCStreamPlayer(WaveFormat waveFormat, Volume? volume = null, bool isOpen = true)
        {
			var vol = (volume.HasValue ? volume.Value : new Volume(1F));
			Logger.LogDebug("{0}.Ctor(WaveFmt=[{1}], Volume=[{2}], Open={3}.)", CLASS_NAME, waveFormat, vol, isOpen);

            m_waveOut			= new WaveOut(WaveOut.WAVE_OUT_MAPPER_DEVICE_ID);
            m_waveFormat		= waveFormat;
			m_waveOut.Volume	= vol;

            if (isOpen)
                open();
        }

        private void open()
        {
            lock (m_waveoutLocker)
            {
                if (!m_waveOut.IsOpened)
                    m_waveOut.Open(m_waveFormat);
            }
        }

		private void reset()
		{
			lock (m_waveoutLocker)
			{
				if (m_waveOut.IsOpened)
					m_waveOut.Stop();
			}
		}

        private void write(byte[] buffer)
        {
			lock (m_waveoutLocker)
			{
				if (!m_waveOut.IsOpened)
					open();

				m_waveOut.Write(buffer);
			}
        }

        private Volume Volume
        {
            get { return m_waveOut.Volume; }
            set { m_waveOut.Volume = value; }
        }

        private WaveFormat WavFormat { get { return m_waveOut.WavFormat; } }

        private bool IsOpened { get { return m_waveOut.IsOpened; } }

		protected override void DisposeUnmanagedResources()
        {
            lock (m_waveoutLocker)
            {
                m_waveOut.TryDispose();
                m_waveOut = null;
            }

            base.DisposeUnmanagedResources();
        }
        #endregion
    }
}
