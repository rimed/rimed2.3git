﻿namespace Rimed.TCD.GUI
{
    partial class SaveStudy
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.albStudyType = new Syncfusion.Windows.Forms.Tools.AutoLabel();
            this.albStudyName = new Syncfusion.Windows.Forms.Tools.AutoLabel();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.buttonHelp = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // albStudyType
            // 
            this.albStudyType.DX = 0;
            this.albStudyType.DY = 0;
            this.albStudyType.Location = new System.Drawing.Point(9, 23);
            this.albStudyType.Name = "albStudyType";
            this.albStudyType.Size = new System.Drawing.Size(236, 23);
            this.albStudyType.TabIndex = 1;
            this.albStudyType.Text = "Study Type:";
            this.albStudyType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // albStudyName
            // 
            this.albStudyName.DX = 0;
            this.albStudyName.DY = 0;
            this.albStudyName.Location = new System.Drawing.Point(9, 62);
            this.albStudyName.Name = "albStudyName";
            this.albStudyName.Size = new System.Drawing.Size(144, 23);
            this.albStudyName.TabIndex = 2;
            this.albStudyName.Text = "Study Name:";
            this.albStudyName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxName
            // 
            this.textBoxName.CausesValidation = false;
            this.textBoxName.Location = new System.Drawing.Point(146, 64);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(168, 20);
            this.textBoxName.TabIndex = 3;
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(12, 104);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 4;
            this.buttonOK.Text = "OK";
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(123, 104);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 5;
            this.buttonCancel.Text = "Cancel";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // buttonHelp
            // 
            this.buttonHelp.Location = new System.Drawing.Point(239, 104);
            this.buttonHelp.Name = "buttonHelp";
            this.buttonHelp.Size = new System.Drawing.Size(75, 23);
            this.buttonHelp.TabIndex = 5;
            this.buttonHelp.Text = "Help";
            // 
            // SaveStudy
            // 
            this.AcceptButton = this.buttonOK;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.CancelButton = this.buttonCancel;
            this.ClientSize = new System.Drawing.Size(326, 144);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.textBoxName);
            this.Controls.Add(this.albStudyName);
            this.Controls.Add(this.albStudyType);
            this.Controls.Add(this.buttonHelp);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "SaveStudy";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Save Study";
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private Syncfusion.Windows.Forms.Tools.AutoLabel albStudyType;
        private Syncfusion.Windows.Forms.Tools.AutoLabel albStudyName;
        private System.Windows.Forms.TextBox textBoxName;

        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Button buttonHelp;
        private System.Windows.Forms.Button buttonCancel;
        private System.ComponentModel.IContainer components;
    }
}