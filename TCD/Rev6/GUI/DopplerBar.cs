using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Rimed.Framework.Common;
using Rimed.TCD.DAL;
using Rimed.TCD.GUI.Images;
using Rimed.TCD.Utils;


namespace Rimed.TCD.GUI
{
    /// <summary>Manages the Doppler variable UI and functionality (e.i : Whats happan when the user pressing on the POWER button</summary>
    public partial class DopplerBar : UserControl
    {
        #region Private Fields

		private bool m_fakeRowDataModeSign = false;
	    private int m_volumeLevel;
        private double m_thump = 0.0;
        private int m_heartRate = 0;
        private string m_bvName = string.Empty;
        #endregion Private Fields

        #region Public Properties

        public bool LindegaardRatioVisible
        {
            // LindegaardRatio is relevant only for IntracranialUnilateral study
            get { return RimedDal.IntracranialUnilateral_STUDY_GUID.Equals(MainForm.Instance.CurrentStudyId) /*&& LindegaardRatio.Visible*/; }
            set
            {
                if (RimedDal.IntracranialUnilateral_STUDY_GUID.Equals(MainForm.Instance.CurrentStudyId))
                {
                    LindegaardRatio.Visible = LR_ValuesPanel.Visible =  value;
                }
            }
        }

        public PictureBox PicManualCalculations
        {
            get
            {
                return picManualCalculations;
            }
        }

        public DopplerValLabelVertical DopplerVarLabelDepth
        {
            get
            {
                return dopplerVarLabelDepth;
            }
        }
        public DopplerVarLabel DopplerVarLabelGain
        {
            get
            {
                return dopplerVarLabelGain;
            }
        }
        public DopplerVarLabel DopplerVarLabelRange
        {
            get
            {
                return dopplerVarLabelRange;
            }
        }
        public DopplerVarLabel DopplerVarLabelWidth
        {
            get
            {
                return dopplerVarLabelWidth;
            }
        }
        public DopplerVarLabel DopplerVarLabelThump
        {
            get
            {
                return dopplerVarLabelThump;
            }
        }
        public DopplerVarLabel DopplerVarLabelPower
        {
            get
            {
                return dopplerVarLabelPower;
            }
        }

		public void MuteVolume()
		{
			if (InvokeRequired)
			{
				BeginInvoke((Action)MuteVolume);
				return;
			}

			if (pictureBoxVolume.Image == null)
				setVolumeImage(m_volumeLevel);

			var src = pictureBoxVolume.Image;
			if (src == null)
				return;

			var bmp = new Bitmap(src.Width, src.Height);
			using (var grp = Graphics.FromImage(bmp))
			{
				grp.DrawImage(src, 0, 0);
				using (var redPen = new Pen(Color.Red, 2))
				{
					grp.DrawLine(redPen, 2, 2, bmp.Width-2, bmp.Height-2);
					grp.DrawLine(redPen, bmp.Width-2, 2, 2, bmp.Height-2);
				}
			}

			pictureBoxVolume.Image = bmp;
		}

		public void RefreshMuteVolumeImage()
		{
			if (MainForm.Instance != null && MainForm.Instance.IsMute)
				MuteVolume();
		}

        [Browsable(true)]
        [DefaultValue(0)]
        public int VolumeLevel
        {
            get
            {
                return m_volumeLevel;
            }
            set
            {
	            setVolumeImage(value);
			}
        }

		private void setVolumeImage(int volume)
		{
			m_volumeLevel = volume;
			if (m_volumeLevel < GlobalSettings.VOLUME_MIN)
				m_volumeLevel = GlobalSettings.VOLUME_MIN;

			if (m_volumeLevel > GlobalSettings.VOLUME_MAX)
				m_volumeLevel = GlobalSettings.VOLUME_MAX;

			// select picture
			if (!m_fakeRowDataModeSign)
			{
				var imagIdx = m_volumeLevel;
				if (MainForm.Instance.IsUnfreezMute)
					imagIdx += GlobalSettings.VOLUME_UNFREEZE_IMAGE_INDEX_SHIFT;

				if (InvokeRequired)
					BeginInvoke((Action)delegate() { pictureBoxVolume.Image = imageListVolume.Images[imagIdx]; });
				else
					pictureBoxVolume.Image = imageListVolume.Images[imagIdx];
			}
		}

	    [Browsable(true), DefaultValue(GlobalTypes.ESizeMode.Large)]
	    public GlobalTypes.ESizeMode SizeMode { get; set; }

	    public double DepthVar
        {
            get
            {
                return dopplerVarLabelDepth.VarValueDouble;
            }
            set
            {
                dopplerVarLabelDepth.VarValueDouble = value;
            }
        }

        public int GainVar
        {
            get
            {
                return dopplerVarLabelGain.VarValue;
            }
            set
            {
                dopplerVarLabelGain.VarValue = value;
            }
        }

        public int RangeVar
        {
            get
            {
                return dopplerVarLabelRange.VarValue;
            }
            set
            {
                dopplerVarLabelRange.VarValue = value;
            }
        }

        public int PowerVar
        {
            get
            {
                return dopplerVarLabelPower.VarValue;
            }
            set
            {
                dopplerVarLabelPower.VarValue = value;
            }
        }

        public double WidthVar
        {
            get
            {
                return dopplerVarLabelWidth.VarValueDouble;
            }
            set
            {
                dopplerVarLabelWidth.VarValueDouble = value;
            }
        }

        /// <summary>
        /// We save the thump doppler variable as double because we display it as int.
        /// </summary>
        public double ThumpVar
        {
            get
            {
                return m_thump;
            }
            set
            {
                m_thump = value;
                dopplerVarLabelThump.VarValue = (int)m_thump;
            }
        }

        public int HeartRate
        {
            get
            {
                return m_heartRate;
            }
            set
            {
                m_heartRate = value;
                var text = "HR - " + ((m_heartRate != 0) ? m_heartRate.ToString() : string.Empty);
                
                if (InvokeRequired)
                    BeginInvoke((Action) delegate() { autoLabelHeartRate.Text = text; });
                else
                    autoLabelHeartRate.Text = text;
            }
        }

        public string BVName
        {
            get
            {
                return m_bvName;
            }
            set
            {
                m_bvName = value;
                if (InvokeRequired)
                    BeginInvoke((Action)delegate(){labelBV.Text = value;});
                else
                    labelBV.Text = value;
            }
        }

        public Panel HeaderPanel = null;

        #endregion Public Properties

        private void fireUpdateHeaderEvent()
        {
            if (UpdateHeaderEvent != null)
                UpdateHeaderEvent(this, HeaderPanel);
        }

        public event UpdateHeaderHandler UpdateHeaderEvent;
        public delegate void UpdateHeaderHandler(object sender, Panel headerPanel);

        /// <summary>Constructor</summary>
        public DopplerBar()
        {
	        SizeMode = GlobalTypes.ESizeMode.Large;
            InitializeComponent();
            
            reloadControlLabels();

			RefreshMuteVolumeImage();
        }



		public void UnitsChanged()
		{
			var gv = Parent as GateView;
			if (gv == null || gv.Probe == null)
				return;

			RangeVar = gv.Probe.GetRange();
			ThumpVar = gv.Probe.GetThump();
		}

		private void reloadControlLabels()
        {
            var strRes = MainForm.StringManager;
            if (strRes != null)
            {
                this.dopplerVarLabelDepth.VarText = strRes.GetString("DepthDopplerBar");
                this.dopplerVarLabelGain.VarText = strRes.GetString("Gain");
                this.dopplerVarLabelRange.VarText = strRes.GetString("Scale");
                this.dopplerVarLabelWidth.VarText = strRes.GetString("Sample");
                this.dopplerVarLabelThump.VarText = strRes.GetString("Filter");
                this.dopplerVarLabelPower.VarText = strRes.GetString("Power");
            }
        }

        private void updateCwLabel(DopplerVarLabel label, ref int xLocation, ref int yLocation)
        {
            label.SizeMode = this.SizeMode;
            if ((Parent as GateView).Probe.IsCW)
            {
                if (SizeMode == GlobalTypes.ESizeMode.Small)
                {
                    label.Location = new Point(xLocation, yLocation);
                    xLocation += dopplerVarLabelDepth.Width;
                    label.Enabled = false;
                }
                else
                {
                    label.Location = new Point(Width, yLocation);
                }
            }
            else
            {
                label.Location = new Point(xLocation, yLocation);
                xLocation += label.Width;
                label.Enabled = true;
            }
        }

        public void RecalcLayout()
        {
            if (Parent == null)
                return;

            if (InvokeRequired)
            {
                BeginInvoke((Action)RecalcLayout);
                return;
            }

            pictureBoxHeartRate.Visible = (SizeMode != GlobalTypes.ESizeMode.Small);
            autoLabelHeartRate.Visible = (SizeMode != GlobalTypes.ESizeMode.Small);
            try
            {
                switch (SizeMode)
                {
                    case GlobalTypes.ESizeMode.Large:
							pictureBoxVolume.SizeMode = PictureBoxSizeMode.CenterImage;
							pictureBoxVolume.Width = 41;
							pictureBoxVolume.Height = 58;

							probeIndication1.Width = 105;
							probeIndication1.LabelFont = GlobalSettings.FONT_14_ARIAL;
                            
                            if (AppSettings.Active.ApplicatioWidth > Constants.DIGI_LITE_SCREEN_WIDTH)
                            {   //Added by Alex Feature #572 v.2.2.1.05 28/10/2014
                                picManualCalculations.Width = LindegaardRatio.Width = LR_ValuesPanel.Width = 72;
                                picManualCalculations.Height = LindegaardRatio.Height = 27;
                                labelBV.Width = 100;
                                labelBV.Font = GlobalSettings.FONT_18_ARIAL;
                                LR_Lval.Font = LR_Rval.Font = GlobalSettings.FONT_18_ARIAL;
                            }
                            else
                            {
                                picManualCalculations.Width = LindegaardRatio.Width = LR_ValuesPanel.Width = 53;
                                picManualCalculations.Height = LindegaardRatio.Height = 27;
                                labelBV.Width = 86;
                                labelBV.Font = GlobalSettings.FONT_14_ARIAL;
                                LR_Lval.Font = LR_Rval.Font = GlobalSettings.FONT_12_ARIAL;
                            }
                        break;
                    case GlobalTypes.ESizeMode.Medium:
							pictureBoxVolume.SizeMode = PictureBoxSizeMode.StretchImage;
							pictureBoxVolume.Width = 35;
							pictureBoxVolume.Height = 28;
							labelBV.Width = 50;
							labelBV.Font = GlobalSettings.FONT_12_ARIAL_BOLD;
                            LR_Lval.Font = LR_Rval.Font = GlobalSettings.FONT_12_ARIAL;
							probeIndication1.Width = 70;
							probeIndication1.LabelFont = GlobalSettings.FONT_10_ARIAL_BOLD;
                            //Added by Alex Feature #572 v.2.2.1.05 28/10/2014
                            picManualCalculations.Width = LindegaardRatio.Width = LR_ValuesPanel.Width = 48;
                            picManualCalculations.Height = LindegaardRatio.Width = 18;
                        break;
                    case GlobalTypes.ESizeMode.Small:
							pictureBoxVolume.SizeMode = PictureBoxSizeMode.StretchImage;
							pictureBoxVolume.Width = 30;
							panel1.Width = 3;
							labelBV.Width = 50;
							labelBV.Font = GlobalSettings.FONT_12_ARIAL_BOLD;
							probeIndication1.Width = 55;
							probeIndication1.LabelFont = GlobalSettings.FONT_11_ARIAL_BOLD;
                            //Added by Alex Feature #572 v.2.2.1.05 28/10/2014
                            picManualCalculations.Width = LindegaardRatio.Width = 48;
                            picManualCalculations.Height = LindegaardRatio.Width = 18;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }

            probeIndication1.Height = labelBV.Height;
            probeIndication1.Location = new Point(Width - probeIndication1.Width - 1, Height - probeIndication1.Height - 2);


            labelBV.Height = (int)(labelBV.Font.Size) + 2;
            labelBV.Location = new Point(Width - probeIndication1.Width - labelBV.Width - 2, Height - labelBV.Height - 2);
            LR_ValuesPanel.Height = labelBV.Height;

	        var gv = Parent as GateView;
			if (gv != null && gv.Probe != null)
            {
				probeIndication1.ProbeColor = gv.Probe.Color;
				probeIndication1.ProbeName = gv.Probe.Name;
            }

            // resize the labels and rearrange them in the bar
            int xLocation = panel1.Location.X + panel1.Width;
            int yLocation = dopplerVarLabelDepth.Location.Y;

            updateCwLabel(dopplerVarLabelDepth, ref xLocation, ref yLocation);

            dopplerVarLabelGain.SizeMode = SizeMode;
            dopplerVarLabelGain.Location = new Point(xLocation, yLocation);
            xLocation += dopplerVarLabelGain.Width;

            dopplerVarLabelRange.SizeMode = SizeMode;
            dopplerVarLabelRange.Location = new Point(xLocation, yLocation);
            xLocation += dopplerVarLabelRange.Width;

            updateCwLabel(dopplerVarLabelPower, ref xLocation, ref yLocation);

            updateCwLabel(dopplerVarLabelWidth, ref xLocation, ref yLocation);

            dopplerVarLabelThump.SizeMode = SizeMode;
            dopplerVarLabelThump.Location = new Point(xLocation, yLocation);

            int y = (probeIndication1.Location.Y / 3);
            pictureBoxHeartRate.Location = new Point(pictureBoxHeartRate.Location.X, y);
            autoLabelHeartRate.Location = new Point(autoLabelHeartRate.Location.X, y);


            LindegaardRatio.Visible = true;
            LindegaardRatio.Visible = LR_ValuesPanel.Visible = LindegaardRatioVisible;

            picManualCalculations.Visible = true;
            if (SizeMode == GlobalTypes.ESizeMode.Large) //Changed by Alex Feature #572 (item 6) v.2.2.1.03 26/10/2014
            //&& !(LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.MonitoringLoad || LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Monitoring))
            {
                picManualCalculations.Location = new Point(labelBV.Left + 15, labelBV.Top - picManualCalculations.Height - 5);
                int LR_space = AppSettings.Active.ApplicatioWidth > Constants.DIGI_LITE_SCREEN_WIDTH ? 15 : 8;
                LindegaardRatio.Location = new Point(picManualCalculations.Left - picManualCalculations.Width - LR_space, picManualCalculations.Top);
                LR_ValuesPanel.Location = new Point(picManualCalculations.Left - picManualCalculations.Width - LR_space, labelBV.Top);
            }
            else
                if (SizeMode == GlobalTypes.ESizeMode.Medium) //Changed by Alex Feature #591 v.2.2.1.04 27/10/2014, v.2.2.1.06 30/10/2014
                {
                    picManualCalculations.Location = new Point(labelBV.Left, labelBV.Top - picManualCalculations.Height - 3);
                    LindegaardRatio.Location = new Point(picManualCalculations.Left - picManualCalculations.Width -3, picManualCalculations.Top);
                     LR_ValuesPanel.Location = new Point(picManualCalculations.Left - picManualCalculations.Width - 3, labelBV.Top);
                }
                else
                {
                    picManualCalculations.Visible = false;
                    LindegaardRatio.Visible = LR_ValuesPanel.Visible = false;
                }

            setLabelsVisibility();

			RefreshMuteVolumeImage();

            PerformLayout();
        }

        private void DopplerBar_Load(object sender, System.EventArgs e)
        {
            MinimumSize = new Size(5, 5);

            BackColor = GlobalSettings.P2;
            labelBV.ForeColor = GlobalSettings.P7;
		}

        private void setLabelsVisibility()
        {
            int xLoc = LindegaardRatio.Visible ? LindegaardRatio.Location.X : labelBV.Location.X;
            // control vars visibility -- all we can squeeze up to the labelBV
            // are visible
            this.setDopplerVarVisibility(dopplerVarLabelThump, xLoc);
            this.setDopplerVarVisibility(dopplerVarLabelWidth, xLoc);
            this.setDopplerVarVisibility(dopplerVarLabelPower, xLoc);
            this.setDopplerVarVisibility(dopplerVarLabelRange, xLoc);
            this.setDopplerVarVisibility(dopplerVarLabelGain, xLoc);
            this.setDopplerVarVisibility(dopplerVarLabelDepth, xLoc);
        }

        private void DopplerBar_SizeChanged(object sender, System.EventArgs e)
        {
            setLabelsVisibility();

            // If this a small chart update the header event
            if (SizeMode == GlobalTypes.ESizeMode.Small)
                fireUpdateHeaderEvent();

            RecalcLayout();
        }

        private void setDopplerVarVisibility(DopplerVarLabel var, int rightLimit)
        {
            var.Visible = ((var.Location.X + var.Width) < rightLimit);
        }

        public void DeactivateAll()
        {
            dopplerVarLabelThump.Active = false;
            dopplerVarLabelWidth.Active = false;
            dopplerVarLabelPower.Active = false;
            dopplerVarLabelRange.Active = false;
            dopplerVarLabelGain.Active = false;
            dopplerVarLabelDepth.Active = false;

        }

        public void DopplerVarChanged(GlobalTypes.EDopplerVar dopplerVar)
        {
            DeactivateAll();
            switch (dopplerVar)
            {
                case GlobalTypes.EDopplerVar.Depth:
						dopplerVarLabelDepth.Active = true;
                    break;
                case GlobalTypes.EDopplerVar.Gain:
						dopplerVarLabelGain.Active = true;
                    break;
                case GlobalTypes.EDopplerVar.Range:
						dopplerVarLabelRange.Active = true;
                    break;
                case GlobalTypes.EDopplerVar.Power:
						dopplerVarLabelPower.Active = true;
                    break;
                case GlobalTypes.EDopplerVar.Width:
						dopplerVarLabelWidth.Active = true;
                    break;
                case GlobalTypes.EDopplerVar.Thump:
						dopplerVarLabelThump.Active = true;
                    break;
                case GlobalTypes.EDopplerVar.Toggle:
                default:
                    // do nothing;
                    break;
            }
        }

        public void CopyFrom(DopplerBar src)
        {
            dopplerVarLabelDepth.VarValueDouble = src.dopplerVarLabelDepth.VarValueDouble;
            dopplerVarLabelGain.VarValue	= src.dopplerVarLabelGain.VarValue;
            dopplerVarLabelRange.VarValue	= src.dopplerVarLabelRange.VarValue;
            dopplerVarLabelPower.VarValue	= src.dopplerVarLabelPower.VarValue;
            dopplerVarLabelWidth.VarValueDouble = src.dopplerVarLabelWidth.VarValueDouble;
            dopplerVarLabelThump.VarValue	= src.dopplerVarLabelThump.VarValue;
            dopplerVarLabelDepth.Active		= src.dopplerVarLabelDepth.Active;
            dopplerVarLabelGain.Active		= src.dopplerVarLabelGain.Active;
            dopplerVarLabelRange.Active		= src.dopplerVarLabelRange.Active;
            dopplerVarLabelPower.Active		= src.dopplerVarLabelPower.Active;
            dopplerVarLabelWidth.Active		= src.dopplerVarLabelWidth.Active;
            dopplerVarLabelThump.Active		= src.dopplerVarLabelThump.Active;

            BVName							= src.BVName;
        }

        private delegate void ShowVolumeDelegate(bool show, GlobalTypes.ESizeMode size, bool fakeRowDataModeSign);
        public void ShowVolumeCtrl(bool show, GlobalTypes.ESizeMode size, bool fakeRowDataModeSign)
        {
            if (InvokeRequired)
            {
                BeginInvoke((ShowVolumeDelegate)ShowVolumeCtrl, show, size, fakeRowDataModeSign);
                return;
            }

            m_fakeRowDataModeSign = fakeRowDataModeSign;

            if (size == GlobalTypes.ESizeMode.Small)
            {
                pictureBoxVolume.Visible = false;
                panel1.Location = new Point(0, 0);
            }
            else
            {
                panel1.Location = new Point(pictureBoxVolume.Right, 0);
                pictureBoxVolume.Visible = true;
                if (show)
                {
                    if (m_fakeRowDataModeSign)
                    {
                        pictureBoxVolume.Image = ImageManager.Instance.GetImage(ImageKey.DeletedRowData);
                        pictureBoxVolume.SizeMode = PictureBoxSizeMode.CenterImage;
                    }
                    else
                    {
                        for (int i = 0; i < 7; i++)
                        {
                            if ((float) m_volumeLevel/GlobalSettings.VOLUME_MAX < ((i*60.0F + 30.0F)/360.0F))
                            {
                                pictureBoxVolume.Image = imageListVolume.Images[i];
                                break;
                            }
                        }
                    }
                }
                else
                {
                    pictureBoxVolume.Image = null;
                }
            }
        }

        private void pictureBoxVolume_MouseDown(object sender, MouseEventArgs e)
        {
             LoggerUserActions.MouseClick("pictureBoxVolume", Name);
            if (m_fakeRowDataModeSign)
                return;

            if (e.Y < Height / 2)
                MainForm.Instance.VolumeUp();
            else
                MainForm.Instance.VolumeDown();
        }

        //Added by Alex Feature #587 v.2.2.1.03 27/10/2014
        public void SetPicManualCalculationsImage(bool value)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DopplerBar));
            if (value)
            {
                this.picManualCalculations.Image = ((System.Drawing.Image)(resources.GetObject("picManualCalculations.ErrorImage")));
            }
            else
            {
                this.picManualCalculations.Image = ((System.Drawing.Image)(resources.GetObject("picManualCalculations.Image")));
            }
        }

        private void picManualCalculations_Click(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick("picManualCalculations", Name);
            if (m_fakeRowDataModeSign)
                return;


            //Changed by Alex Feature #572 v.2.2.1.00 08/10/2014  
            if (LayoutManager.Instance.SelectedGv == null)
            {
                LoggedDialog.ShowNotification(MainForm.Instance, MainForm.StringManager.GetString("Please insert cursors"), MainForm.StringManager.GetString("Manual Calculations"));
                return;
            }
            
            if (!LayoutManager.Instance.CursorsMode )
            {
                LayoutManager.Instance.PointMode = !LayoutManager.Instance.PointMode;
                LayoutManager.Instance.CursorsMode = LayoutManager.Instance.PointMode;
            }
            else
                if (LayoutManager.Instance.PointMode)
                {
                    LayoutManager.Instance.PointMode = false;
                    LayoutManager.Instance.CursorsMode = false;
                }

            //Changed by Alex Feature #587 v.2.2.1.03 27/10/2014
            //Removed by Alex 30/12/2014 fixed bug #587 v 2.2.2.8
            //SetPicManualCalculationsImage(LayoutManager.Instance.PointMode);

            LayoutManager.Instance.CurrentPoint = 0;
            LayoutManager.Instance.ManualCalculations();
        }

        private void picManualCalculations_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (m_fakeRowDataModeSign)
                return;

            if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Offline && LayoutManager.Instance.CursorsMode)
                picManualCalculations.BorderStyle = BorderStyle.None;
        }

        private void picManualCalculations_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if (m_fakeRowDataModeSign)
                return;

            if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Offline && LayoutManager.Instance.CursorsMode && !LayoutManager.Instance.PointMode)//Changed by Alex Feature #572 v.2.2.1.00 14/10/2014
                picManualCalculations.BorderStyle = BorderStyle.Fixed3D;
        }

        private void picManualCalculations_MouseLeave(object sender, System.EventArgs e)
        {
            if (m_fakeRowDataModeSign)
                return;

            if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Offline && LayoutManager.Instance.CursorsMode)
                picManualCalculations.BorderStyle = BorderStyle.None;
        }

        private void LindegaardRatio_Click(object sender, EventArgs e)
        {
            using (var dlg = new LindegaardRatioDlg())
            {
                dlg.TopMost = true;

                var res = dlg.ShowDialog(MainForm.Instance);

                Focus();
            }
        }

        private void LR_ValuesPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        public void SetLindegaardRatio(bool visible, double leftRatio, double rightRatio)
        {
            LindegaardRatioVisible = visible;
            UpdateLindegaardRatio(leftRatio, rightRatio);
        }

        public void UpdateLindegaardRatio(double leftRatio, double rightRatio)
        {
            if (LR_ValuesPanel.Visible)
            {
                if (leftRatio != 0)
                {
                    LR_Lval.Text = string.Format("{0:N1}", leftRatio);
                    LR_Lval.Visible = true;

                }
                else
                    LR_Lval.Visible = false;

                if (rightRatio != 0)
                {
                    LR_Rval.Text = string.Format("{0:N1}", rightRatio);
                    LR_Rval.Visible = true;
                }
                else
                    LR_Rval.Visible = false;
            }
        }
    }
}
