using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using Rimed.Framework.Common;
using Rimed.TCD.DAL;
using Rimed.TCD.Utils;

namespace Rimed.TCD.GUI
{
    public partial class PatientListDlg : Form
    {
        #region Private Fields

        private List<string> m_patientsNames = new List<string>();
        private List<string> m_ids = new List<string>();
        private List<Guid> m_patientsIndexes = new List<Guid>();
        private Guid m_selPatientIndex = Guid.Empty;

        #endregion Private Fields

        public PatientListDlg()
        {
//            System.Diagnostics.Trace.WriteLine(string.Format("Window handle: {0} created.", Handle));   // Ofer, Force creation of Window handle

            InitializeComponent();

            var strRes = MainForm.StringManager;
            buttonOK.Text = strRes.GetString("OK");
            buttonCancel.Text = strRes.GetString("Cancel");
            lblName.Text = strRes.GetString("Name") + ":";
            lblID.Text = strRes.GetString("ID Number") + ":";
            buttonDetails.Text = strRes.GetString("Details");
            buttonCreateNewPatient.Text = strRes.GetString("Create New Patient");
            buttonHelp.Text = strRes.GetString("Help");
            Text = strRes.GetString("Select Patient Dialog");

            BackColor = GlobalSettings.BackgroundDlg;
        }

        public Guid SelPatientIndex
        {
            get
            {
                return this.m_selPatientIndex;
            }
        }

        private void PatientListDlg_Load(object sender, EventArgs e)
        {
            this.autoCompleteComboBoxName.DataSource = null;
            this.autoCompleteComboBoxID.DataSource = null;

            DataView dv = new System.Data.DataView(RimedDal.Instance.PatientsDS.tb_Patient, "Deleted = false", null, DataViewRowState.CurrentRows);
            this.m_patientsNames.Clear();
            this.m_ids.Clear();
            this.m_patientsIndexes.Clear();
            List<Patient> patients = new List<Patient>();
            foreach (DataRowView row in dv)
            {
                Patient p = new Patient((Guid)row["Patient_Index"], row["Last_Name"] + " " + row["First_Name"], (String)row["ID"]);
                patients.Add(p);
            }
            patients.Sort();
            foreach (Patient p in patients)
            {
                this.m_patientsNames.Add(p.Name);
                this.m_ids.Add(p.Id);
                this.m_patientsIndexes.Add(p.Guid);
            }

            this.autoCompleteComboBoxName.DataSource = this.m_patientsNames;
            this.autoCompleteComboBoxID.DataSource = this.m_ids;
        }

        private void buttonOK_Click(object sender, System.EventArgs e)
        {
            var fields = LoggerUserActions.GetFilesList();
            fields.Add("autoCompleteComboBoxName", autoCompleteComboBoxName.Text);
            fields.Add("autoCompleteComboBoxID", autoCompleteComboBoxID.Text);
            LoggerUserActions.MouseClick("buttonOK", this.Name, fields);

            int index = autoCompleteComboBoxID.SelectedIndex;
            if (index != -1)
                m_selPatientIndex = (Guid)m_patientsIndexes[index];
        }

        private void autoCompleteComboBoxName_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            try
            {
                int index = autoCompleteComboBoxName.SelectedIndex;
                if (index < autoCompleteComboBoxID.Items.Count)
                    autoCompleteComboBoxID.SelectedIndex = index;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
				LoggedDialog.ShowError(this, ex.Message + "\nSelectedIndexChanged: " + autoCompleteComboBoxName.SelectedIndex);
            }
        }

        private void autoCompleteComboBoxID_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            int index = this.autoCompleteComboBoxID.SelectedIndex;
            this.autoCompleteComboBoxName.SelectedIndex = index;
        }

        private void buttonCreateNewPatient_Click(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick("buttonCreateNewPatient", Name);
            using (var dlg = new PatientDetails())
            {
				dlg.TopMost = true;
	            var res = dlg.ShowDialog(this);
	            Focus();
				if (res == DialogResult.OK)
                {
                    m_selPatientIndex = dlg.PatientIndex;
                    DialogResult = DialogResult.OK;
                    Close();
                }
            }
        }

        private void buttonDetails_Click(object sender, System.EventArgs e)
        {
            var fields = LoggerUserActions.GetFilesList();
            fields.Add("autoCompleteComboBoxName", autoCompleteComboBoxName.Text);
            fields.Add("autoCompleteComboBoxID", autoCompleteComboBoxID.Text);
            LoggerUserActions.MouseClick("buttonDetails", this.Name, fields);

            var filter = "Select * FROM tb_Patient WHERE (Patient_Index = '" + m_patientsIndexes[autoCompleteComboBoxID.SelectedIndex].ToString() + "')";
            using (var dlg = new PatientDetails(RimedDal.Instance.GetPatients(filter)))
            {
				dlg.DisplayPurpose = PatientDetails.EDisplayPurpose.ViewChange;
				dlg.TopMost = true;	//Ofer, 2014.01.05
				dlg.ShowDialog(this);
				Focus();
			}
        }

        private void autoCompleteComboBoxID_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && !Char.IsControl(e.KeyChar) && !Char.IsLetter(e.KeyChar))
                e.Handled = true;
        }
    }
}
