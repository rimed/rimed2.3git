namespace Rimed.TCD.GUI
{
    /// <summary>Summary description for CyclicArray.</summary>
    public class CyclicArray
    {
        private int[] m_intArray;
        private int m_location;

        private readonly object m_lock = new object();

        public CyclicArray(int size)
        {
            ReInit(size);
        }

        public void ReInit(int size)
        {
            lock (m_lock)
            {
                if (m_intArray != null && m_intArray.Length == size)
                    reset();
                else
                    m_intArray = new int[size];

                m_location = 0;
            }
        }

        private int Sum
        {
            get
            {
                var total = 0;

                lock (m_lock)
                {
                    for (int i = 0; i < m_intArray.Length; ++i)
                    {
                        total += m_intArray[i];
                    }
                }

                return total;
            }
        }

        public int Average
        {
            get
            {
                lock (m_lock)
                {
                    return Sum/m_intArray.Length;
                }
            }
        }

        public void UpdateValue(int i)
        {
            lock (m_lock)
            {
                m_intArray[GetLocation()] = i;
            }
        }


        private int GetLocation()
        {
            m_location++;
            if (m_location >= m_intArray.Length)
                m_location = 0;

            return m_location;
        }

        private void reset()
        {
            for (var i = 0; i < m_intArray.Length; ++i)
            {
                m_intArray[i] = 0;
            }
        }
    }
}
