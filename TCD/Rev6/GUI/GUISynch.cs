﻿using System;
using Rimed.TCD.DSPData;
using Rimed.TCD.DspBlock;

namespace Rimed.TCD.GUI
{
	public static class GuiSynch
	{
		// Get the right column in the block since there are 3 columns in each block and we only want 1 of them.
		static public int ColumnInBlock(double t)
		{
			t = Math.Round(t * 1000.0) / 1000.0;
			t = t - ((int)(Math.Round((t * 1000.0) / RawDataFile.DSP_MESSAGE_DURATION_SEC) / 1000.0) * RawDataFile.DSP_MESSAGE_DURATION_SEC);
			var res = (int)Math.Round((t / RawDataFile.DSP_MESSAGE_DURATION_SEC) * DspBlockConst.NUM_OF_COLUMNS);
			if (res > 2)
				res = 2;

			return res;
		}

		static public int CalcIndexFromTime(double t, int eventBufferStart)
		{
			var index = (int)(Math.Round((t / RawDataFile.DSP_MESSAGE_DURATION_SEC) * 1000.0) / 1000.0);
			index = (index + eventBufferStart) % RawDataFile.FILE_MAX_DSP_MESSAGES;

			return index;
		}

		///// <summary>The time from the start of online mode</summary>
		//static public TimeSpan OnlineTime(BmpRawData rawData)
		//{
		//    var lastBufferNum = rawData.LastAbsoluteIndex;

		//    if (lastBufferNum < rawData.EventStartPointAbsoluteIndex || rawData.LastDrawnIndex < 0 || rawData.EventStartPointAbsoluteIndex < 0)
		//        return TimeSpan.Zero;//.FromSeconds(0);

		//    if (rawData.EventStartPointAbsoluteIndex != 0)
		//        return TimeSpan.FromSeconds((lastBufferNum - rawData.EventReplayPointAbsoluteIndex) * RawDataFile.DSP_MESSAGE_DURATION_SEC);

		//    return TimeSpan.FromSeconds(lastBufferNum * RawDataFile.DSP_MESSAGE_DURATION_SEC);
		//}
	}
}
