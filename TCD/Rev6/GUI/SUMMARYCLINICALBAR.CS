using System;
using System.Text;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using Rimed.TCD.DAL;

namespace Rimed.TCD.GUI
{
    public partial class SummaryClinicalBar : UserControl
    {
        #region Private Fields
		private static readonly SolidBrush s_bgBrush = new SolidBrush(GlobalSettings.BackgoundAlmostBlack);

        private readonly List<SummaryClinicalBarLabel> m_clinicalParams = new List<SummaryClinicalBarLabel>();
        private readonly List<SummaryClinicalBarLabel> m_sortClinicalParams = new List<SummaryClinicalBarLabel>();
        private String m_clinicalParamOrder;
        private DataView m_studyClinicalParam;
        private readonly List<Point> m_locations = new List<Point>();

        private GlobalTypes.ESizeMode m_sizeMode = GlobalTypes.ESizeMode.Small;
        private Point L1 = new Point(1, 10);
        private Point L1T = new Point(29, 1);
        private Point L1B = new Point(29, 21);
        private Point L2 = new Point(1, 44);
        private Point L2T = new Point(29, 36);
        private Point L2B = new Point(29, 53);
        private Point L3 = new Point(1, 80);
        private Point L3T = new Point(29, 69);
        private Point L3B = new Point(29, 88);
        private Point L4 = new Point(65, 10);
        private Point L4T = new Point(92, 1);
        private Point L4B = new Point(92, 21);
        private Point L5 = new Point(64, 44);
        private Point L5T = new Point(92, 36);
        private Point L5B = new Point(92, 53);
        private Point L6 = new Point(64, 80);
        private Point L6T = new Point(92, 69);
        private Point L6B = new Point(92, 88);

        #endregion Private Fields

        /// <summary>Constructor</summary>
        public SummaryClinicalBar()
        {
            // This call is required by the Windows.Forms Form Designer.
            InitializeComponent();

            imageList1.ImageSize = new Size(89, 89);
            imageList1.Images.Add(Image.FromFile(Path.Combine(Utils.Constants.DATA_PATH, "cl_param_report.bmp")));
			BackgroundImage = imageList1.Images[0];

            // Insert the labels into the array.
            m_clinicalParams.Add(new SummaryClinicalBarLabel(PeakLabel, PeakTopVal, PeakBottomVal, "A"));
            m_clinicalParams.Add(new SummaryClinicalBarLabel(MeanLabel, MeanTopVal, MeanBottomVal, "B"));
            m_clinicalParams.Add(new SummaryClinicalBarLabel(DVLabel, DVTopVal, DVBottomVal, "C"));
            m_clinicalParams.Add(new SummaryClinicalBarLabel(PILabel, PITopVal, PIBottomVal, "D"));
            m_clinicalParams.Add(new SummaryClinicalBarLabel(SDLabel, SDTopVal, SDBottomVal, "E"));
            m_clinicalParams.Add(new SummaryClinicalBarLabel(SWLabel, SWTopVal, SWBottomVal, "F"));
            m_clinicalParams.Add(new SummaryClinicalBarLabel(RIlabel, RITopVal, RIBottomVal, "G"));
            m_clinicalParams.Add(new SummaryClinicalBarLabel(Modelabel, ModeTopVal, ModeBottomVal, "H"));
            m_clinicalParams.Add(new SummaryClinicalBarLabel(Avrglabel, AvrgTopVal, AvrgBottomVal, "I"));
            m_clinicalParams.Add(new SummaryClinicalBarLabel(Hitslabel, HitsTopVal, HitsBottomVal, "J"));
            m_clinicalParams.Add(new SummaryClinicalBarLabel(HitsRatelabel, HITrTopVal, HITrBottomVal, "K"));

            m_locations.Add(L1);
            m_locations.Add(L1T);
            m_locations.Add(L1B);
            m_locations.Add(L2);
            m_locations.Add(L2T);
            m_locations.Add(L2B);
            m_locations.Add(L3);
            m_locations.Add(L3T);
            m_locations.Add(L3B);
            m_locations.Add(L4);
            m_locations.Add(L4T);
            m_locations.Add(L4B);
            m_locations.Add(L5);
            m_locations.Add(L5T);
            m_locations.Add(L5B);
            m_locations.Add(L6);
            m_locations.Add(L6T);
            m_locations.Add(L6B);


			// RUNTIME ONLY
	        if (DesignMode || MainForm.Instance == null)
		        return;

			fillSortClinicalParams();
	        recalcLayout();
        }

        [Browsable(true)]
        [DefaultValue(GlobalTypes.ESizeMode.Small)]
        public GlobalTypes.ESizeMode SizeMode
        {
            get
            {
                return m_sizeMode;
            }
            set
            {
                gradientPanelSmall.SuspendLayout();
                gradientPanelLarge.SuspendLayout();
                SuspendLayout();
                gradientPanelLarge.Controls.Clear();
                gradientPanelSmall.Controls.Clear();
                m_sizeMode = value;
                if (m_sizeMode == GlobalTypes.ESizeMode.Large)
                {
                    for (int i = 0; i < m_sortClinicalParams.Count && ((i * 3) + 2) < m_locations.Count; i++)
                    {
                        gradientPanelLarge.Controls.Add(m_sortClinicalParams[i].Name);
                        gradientPanelLarge.Controls.Add(m_sortClinicalParams[i].Top);
                        gradientPanelLarge.Controls.Add(m_sortClinicalParams[i].Bottom);
                        m_sortClinicalParams[i].NameLocation = new Point(m_locations[i * 3].X, m_locations[i * 3].Y);
                        m_sortClinicalParams[i].TopLocation = new Point(m_locations[(i * 3) + 1].X, m_locations[(i * 3) + 1].Y);
                        m_sortClinicalParams[i].BottomLocation = new Point(m_locations[(i * 3) + 2].X, m_locations[(i * 3) + 2].Y);
                    }

                    gradientPanelSmall.Visible = false;
                    gradientPanelLarge.Visible = true;
                    Width = gradientPanelLarge.Width;
                }
                else // Small
                {
                    for (int i = 0; i < m_sortClinicalParams.Count && ((i * 3) + 2) < m_locations.Count; i++)
                    {
                        gradientPanelSmall.Controls.Add(this.m_sortClinicalParams[i].Name);
                        gradientPanelSmall.Controls.Add(this.m_sortClinicalParams[i].Top);
                        gradientPanelSmall.Controls.Add(this.m_sortClinicalParams[i].Bottom);
                        m_sortClinicalParams[i].NameLocation = new Point(m_locations[i * 3].X, m_locations[i * 3].Y);
                        m_sortClinicalParams[i].TopLocation = new Point(m_locations[(i * 3) + 1].X, m_locations[(i * 3) + 1].Y);
                        m_sortClinicalParams[i].BottomLocation = new Point(m_locations[(i * 3) + 2].X, m_locations[(i * 3) + 2].Y);
                    }
                    gradientPanelSmall.Visible = true;
                    gradientPanelLarge.Visible = false;
                    Width = gradientPanelSmall.Width;
                }
                gradientPanelSmall.ResumeLayout();
                gradientPanelLarge.ResumeLayout();
                ResumeLayout();
            }
        }

        private void recalcLayout()
        {
            if (InvokeRequired)
            {
                BeginInvoke((Action)recalcLayout);
                return;
            }

            // Hide all the clinical param ctrl.
            for (var i = 0; i < m_clinicalParams.Count; i++)
            {
                m_clinicalParams[i].Visible = false;
            }

            // Display only the clinical param ctrl that being set by the user in the setup.
            for (var i = 0; i < m_sortClinicalParams.Count; i++)
            {
                m_sortClinicalParams[i].Visible = true;
            }
        }

        private string SetLabelValueVisibility(dsGateExamination.tb_GateExaminationRow gateRow)
        {
            return (gateRow.DrawPeakFwdEnvelope ? ToStringValue(gateRow.PeakTopVal) : "");
        }

        // Changed by Alex 12/04/2015 fixed bug #584 v. 2.2.2.29
        public void CopyFrom(dsGateExamination.tb_GateExaminationRow gateRow)
        {
            PeakTopVal.Text = (gateRow.DrawPeakFwdEnvelope ? ToStringValue(gateRow.PeakTopVal) : "");
            PeakBottomVal.Text = (gateRow.DrawPeakBckEnvelope ? ToStringValue(gateRow.PeakBottomVal) : "");

            MeanTopVal.Text = (gateRow.DrawPeakFwdEnvelope ? ToStringValue(gateRow.MeanTopVal) : "");
            MeanBottomVal.Text = (gateRow.DrawPeakBckEnvelope ? ToStringValue(gateRow.MeanBottomVal) : ""); ;

            DVTopVal.Text = (gateRow.DrawPeakFwdEnvelope ? ToStringValue(gateRow.DVTopVal) : "");
            DVBottomVal.Text = (gateRow.DrawPeakBckEnvelope ? ToStringValue(gateRow.DVBottomVal) : "");

            PITopVal.Text = (gateRow.DrawPeakFwdEnvelope ? ToStringValue(gateRow.PITopVal) : "");
            PIBottomVal.Text = (gateRow.DrawPeakBckEnvelope ? ToStringValue(gateRow.PIBottomVal) : "");

            SWTopVal.Text = (gateRow.DrawPeakFwdEnvelope ? ToStringValue(gateRow.SWTopVal) : "");
            SWBottomVal.Text = (gateRow.DrawPeakBckEnvelope ? ToStringValue(gateRow.SWBottomVal) : "");

            SDTopVal.Text = (gateRow.DrawPeakFwdEnvelope ? ToStringValue(gateRow.SDTopVal) : "");
            SDBottomVal.Text = (gateRow.DrawPeakBckEnvelope ? ToStringValue(gateRow.SDBottomVal) : "");

            RITopVal.Text = (gateRow.DrawPeakFwdEnvelope ? ToStringValue(gateRow.RITopVal) : "");
            RIBottomVal.Text = (gateRow.DrawPeakBckEnvelope ? ToStringValue(gateRow.RIBottomVal) : "");

            ModeTopVal.Text = (gateRow.DrawPeakFwdEnvelope ? ToStringValue(gateRow.ModeTopVal) : "");
            ModeBottomVal.Text = (gateRow.DrawPeakBckEnvelope ? ToStringValue(gateRow.ModeBottomVal) : "");

            AvrgTopVal.Text = (gateRow.DrawPeakFwdEnvelope ? ToStringValue(gateRow.AvrgTopVal) : "");
            AvrgBottomVal.Text = (gateRow.DrawPeakBckEnvelope ? ToStringValue(gateRow.AvrgBottomVal) : "");

            HitsTopVal.Text = (gateRow.DrawPeakFwdEnvelope ? ToStringValue(gateRow.HitsTopVal) : "");
            HitsBottomVal.Text = (gateRow.DrawPeakBckEnvelope ? ToStringValue(gateRow.HitsBottomVal) : "");

            HITrTopVal.Text = (gateRow.DrawPeakFwdEnvelope ? ToStringValue(gateRow.HITrTopVal) : "");
            HITrBottomVal.Text = (gateRow.DrawPeakBckEnvelope ? ToStringValue(gateRow.HITrBottomVal) : "");
        }

        private delegate void CopyFromDelegate(ClinicalBar src);	
        public void CopyFrom(ClinicalBar src)
        {
            if (PeakTopVal.InvokeRequired)						
            {
                PeakTopVal.BeginInvoke((CopyFromDelegate)CopyFrom, src);
                return;
            }

            PeakTopVal.Text = ToStringValue(src.clinicalParamLabelPeak.UpperVar);
            PeakBottomVal.Text = ToStringValue(src.clinicalParamLabelPeak.LowerVar);

            MeanTopVal.Text = ToStringValue(src.ClinicalParamLabelMeanUpper);
            MeanBottomVal.Text = ToStringValue(src.ClinicalParamLabelMeanLower);

            DVTopVal.Text = ToStringValue(src.ClinicalParamLabelDvUpper);
            DVBottomVal.Text = ToStringValue(src.ClinicalParamLabelDvLower);

            PITopVal.Text = ToStringValue(src.ClinicalParamLabelPiUpper);
            PIBottomVal.Text = ToStringValue(src.ClinicalParamLabelPiLower);

            SWTopVal.Text = ToStringValue(src.ClinicalParamLabelSwUpper);
            if (!src.clinicalParamLabelSW.DisplayOnlyUpperValue) //
                SWBottomVal.Text = ToStringValue(src.ClinicalParamLabelSwLower);

            SDTopVal.Text = ToStringValue(src.ClinicalParamLabelSdUpper);
            SDBottomVal.Text = ToStringValue(src.ClinicalParamLabelSdLower);

            RITopVal.Text = ToStringValue(src.ClinicalParamLabelRiUpper);
            RIBottomVal.Text = ToStringValue(src.ClinicalParamLabelRiLower);

            ModeTopVal.Text = ToStringValue(src.ClinicalParamLabelModeUpper);
            ModeBottomVal.Text = ToStringValue(src.ClinicalParamLabelModeLower);

            AvrgTopVal.Text = ToStringValue(src.ClinicalParamLabelAverageUpper);
            AvrgBottomVal.Text = ToStringValue(src.ClinicalParamLabelAverageLower);

            HitsTopVal.Text = ToStringValue(src.ClinicalParamLabelHitsUpper);
            HitsBottomVal.Text = ToStringValue(src.ClinicalParamLabelHitsLower);

            HITrTopVal.Text = ToStringValue(src.ClinicalParamLabelHitsRateUpper);
            HITrBottomVal.Text = ToStringValue(src.ClinicalParamLabelHitsRateLower);
        }

        private string ToStringValue(double paramValue)
        {
            if (paramValue == 0)
                return string.Empty;

            return Math.Round(paramValue, 2).ToString();
        }

        public void Serialize(Stream ms)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(ms, PeakTopVal.Text);
            formatter.Serialize(ms, PeakBottomVal.Text);
            formatter.Serialize(ms, MeanTopVal.Text);
            formatter.Serialize(ms, MeanBottomVal.Text);
            formatter.Serialize(ms, DVTopVal.Text);
            formatter.Serialize(ms, DVBottomVal.Text);
            formatter.Serialize(ms, PITopVal.Text);
            formatter.Serialize(ms, PIBottomVal.Text);
            formatter.Serialize(ms, SWTopVal.Text);
            formatter.Serialize(ms, SWBottomVal.Text);
            formatter.Serialize(ms, SDTopVal.Text);
            formatter.Serialize(ms, SDBottomVal.Text);

            formatter.Serialize(ms, RITopVal.Text);
            formatter.Serialize(ms, RIBottomVal.Text);

            formatter.Serialize(ms, ModeTopVal.Text);
            formatter.Serialize(ms, ModeBottomVal.Text);

            formatter.Serialize(ms, AvrgTopVal.Text);
            formatter.Serialize(ms, AvrgBottomVal.Text);

            formatter.Serialize(ms, HitsTopVal.Text);
            formatter.Serialize(ms, HitsBottomVal.Text);

            formatter.Serialize(ms, HITrTopVal.Text);
            formatter.Serialize(ms, HITrBottomVal.Text);
        }

        public void Deserialize(Stream ms)
        {
            var formatter = new BinaryFormatter();
            PeakTopVal.Text = (String)formatter.Deserialize(ms);
            PeakBottomVal.Text = (String)formatter.Deserialize(ms);
            MeanTopVal.Text = (String)formatter.Deserialize(ms);
            MeanBottomVal.Text = (String)formatter.Deserialize(ms);
            DVTopVal.Text = (String)formatter.Deserialize(ms);
            DVBottomVal.Text = (String)formatter.Deserialize(ms);
            PITopVal.Text = (String)formatter.Deserialize(ms);
            PIBottomVal.Text = (String)formatter.Deserialize(ms);
            SWTopVal.Text = (String)formatter.Deserialize(ms);
            SWBottomVal.Text = (String)formatter.Deserialize(ms);
            SDTopVal.Text = (String)formatter.Deserialize(ms);
            SDBottomVal.Text = (String)formatter.Deserialize(ms);

            RITopVal.Text = (String)formatter.Deserialize(ms);
            RIBottomVal.Text = (String)formatter.Deserialize(ms);

            ModeTopVal.Text = (String)formatter.Deserialize(ms);
            ModeBottomVal.Text = (String)formatter.Deserialize(ms);

            AvrgTopVal.Text = (String)formatter.Deserialize(ms);
            AvrgBottomVal.Text = (String)formatter.Deserialize(ms);

            HitsTopVal.Text = (String)formatter.Deserialize(ms);
            HitsBottomVal.Text = (String)formatter.Deserialize(ms);

            HITrTopVal.Text = (String)formatter.Deserialize(ms);
            HITrBottomVal.Text = (String)formatter.Deserialize(ms);
        }

		public bool ShowClinicalValues 
		{
			set
			{
				foreach (var lbl in m_clinicalParams)
				{
					lbl.ShowValues = value;
				}
			}
		}

        /// <summary>This function get the clinical param order from the database.</summary>
        private string getClinicalParamOrder()
        {
            // retrieve all the clinical param.
            dsClinicalParamSetup1 = RimedDal.Instance.GetAllClinicalParam();

            // retrive the study ID.
            var selectedStudyId = MainForm.Instance.CurrentStudyId;

            // retrive the clinical param for the selected study.
            m_studyClinicalParam = RimedDal.Instance.GetClinicalParamByStudyIndex(selectedStudyId);

            var str = new StringBuilder(string.Empty);
            foreach (DataRowView rowStudyClinicalParam in m_studyClinicalParam)
            {
                foreach (dsClinicalParamSetup.tb_ClinicalParamRow rowClinicalParam in dsClinicalParamSetup1.tb_ClinicalParam.Rows)
                {
                    var paramName = (string)rowClinicalParam["Name"];
                    if ((Guid)rowClinicalParam["Index"] == (Guid)rowStudyClinicalParam["ClinicalParam_Index"])
                    {
                        foreach (SummaryClinicalBarLabel paramLabel in m_clinicalParams)
                        {
                            if (paramLabel.Name.Text.Equals(paramName))
                            {
                                str.Append(paramLabel.Id);
                                break;
                            }
                        }
                    }
                }
            }

            return str.ToString();
        }

        /// <summary>This function fills the clinical param order array.</summary>
        private void fillSortClinicalParams()
        {
            string clinicalParamOrder = getClinicalParamOrder();
            if (m_clinicalParamOrder != clinicalParamOrder)
            {
                m_clinicalParamOrder = clinicalParamOrder;
                m_sortClinicalParams.Clear();

                for (int i = 0; i < m_clinicalParamOrder.Length; i++)
                {
                    m_sortClinicalParams.Add(m_clinicalParams[(int)m_clinicalParamOrder[i] - (int)('A')]);
                }
            }
        }

        public Image Convert2Image()
        {
           var image = new Bitmap(GlobalSettings.REPORT_CLINICAL_BAR_SIZE, GlobalSettings.REPORT_CLINICAL_BAR_SIZE);

            using (var g = Graphics.FromImage(image))
            {
                var image1 = imageList1.Images[0];
                g.DrawImage(image1, 0, 0, image.Width, image.Height);

                // display only the clinical param ctrl that being set by the user in the setup.
                var i = 0;
                var j = 0;
                foreach (var label in m_sortClinicalParams)
                {
                    //RIMD-453: Clinical parameters in Summary table of report contain unexpected symbols
                    //Return if Clinical Parameters count > 6 
                    if (j > 5)
                        break;

                    var Y = i * 58 + 17;
                    var X = j > 2 ? 88 : 2;
                    var XValue = X +39;
                    var YTopValue = i * 58 + 2;
                    var YBottomValue = i * 58 + 36;
 
                    var shiftTopXValue = 0;
                    var shiftBottomXValue = 0;
                    // Nadiya: Workaround: the value like -1.01 or 1898 are over the right border Now all values are aligned by right border
                    correctXShift(label.Top.Text, ref shiftTopXValue);
                    correctXShift(label.Bottom.Text, ref shiftBottomXValue);

					lock (s_bgBrush)
					{
                        g.DrawString(label.Top.Text, GlobalSettings.FONT_18_ARIAL_BOLD, Brushes.Black, XValue + shiftTopXValue, YTopValue);
                        g.DrawString(label.Name.Text, GlobalSettings.FONT_18_ARIAL_BOLD, Brushes.Black, X, Y);
                        g.DrawString(label.Bottom.Text, GlobalSettings.FONT_18_ARIAL_BOLD, Brushes.Black, XValue + shiftBottomXValue, YBottomValue);
 
					}

                    i++;
                    if (i == 3)
                        i = 0;

                    j++;
                }
            }

            return image;
        }

        private static void correctXShift(string text, ref int shiftXValue)
        {
            String strNew = text.Replace("-", string.Empty);
            strNew = strNew.Replace(".", string.Empty);

            shiftXValue += (4 - strNew.Length) * 10;
 
            if (text.Contains("-"))
                shiftXValue -= 5;
            if (text.Contains("."))
                shiftXValue -= 3;
         }
    }
}
