using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Rimed.TCD.DAL;
using Rimed.TCD.Utils;
using System.ComponentModel;

namespace Rimed.TCD.GUI
{
    public partial class BVRenameDlg : System.Windows.Forms.Form
    {
	    private readonly string m_originalName;

        public BVRenameDlg(string name, string fullName, List<string> BVnames)
        {
            if (LicenseManager.UsageMode != LicenseUsageMode.Designtime)
            {
                m_originalName = fullName;
                if (string.IsNullOrWhiteSpace(m_originalName))
                    m_originalName = name;

                // Required for Windows Form Designer support
                InitializeComponent();

                var strRes = MainForm.StringManager;

                Text = strRes.GetString("Rename BV") + string.Format(" {0} as", m_originalName);
                //lblBVName.Text = strRes.GetString("Blood Vessel Name") + ":";
                buttonOK.Text = strRes.GetString("OK");
                buttonCncel.Text = strRes.GetString("Cancel");

                BackColor = GlobalSettings.BackgroundDlg;

                listBox1.Items.Clear();

                for (int i = 0; i < BVnames.Count; ++i)
                {
                    listBox1.Items.Add(BVnames[i]);
                    if (BVnames[i] == fullName)
                    {
                        listBox1.SelectedIndex = i;
                        buttonOK.Enabled = true;
                    }
                }

            }
        }

 
         private void BVRenameDlg_Load(object sender, System.EventArgs e)
        {
            listBox1.Focus();
        }
         public int SelectedIndex
         {
             get;
             set;
         }

         private void NextBV()
         {
              listBox1.SelectedIndex = listBox1.SelectedIndex < listBox1.Items.Count-1 ? ++listBox1.SelectedIndex :0;
         }

         private void PrevBV()
         {
             listBox1.SelectedIndex = listBox1.SelectedIndex > 0 ? --listBox1.SelectedIndex : listBox1.Items.Count-1;
         }

         private void buttonOK_Click(object sender, System.EventArgs e)
        {
            OK();
        }

         private void OK()
         {
             var fields = LoggerUserActions.GetFilesList();
             fields.Add("listBox1", listBox1.SelectedItem.ToString());
             LoggerUserActions.MouseClick("buttonOK", Name, fields);

             SelectedIndex = listBox1.SelectedIndex;
             if (SelectedIndex < 0)
                 DialogResult = DialogResult.Abort;
             else
                DialogResult = DialogResult.OK;
             Close();
         }

        // handle keyboard shortcut
         private void BVRenameDlg_KeyDown(object sender, KeyEventArgs e)
		{
            e.Handled = true;
            e.SuppressKeyPress = true;
            switch (e.KeyCode)
            {
                case Keys.D:
                    if (e.Shift && e.Control)
                    {
                        PrevBV();
                    }
                    break;
                case Keys.X:
                    if (e.Shift && e.Control)
                    {
                        NextBV();
                    }
                    break;
                case Keys.A:
                    if (e.Shift && e.Control)
                    {
                        OK();
                    }
                    break;
                default:
                    if (!e.Shift || !e.Control)
                    {
                        e.Handled = false;
                        e.SuppressKeyPress = false;
                    }
                    break;
            }
         }

 
    }
}
