using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

using Rimed.TCD.DAL;
using Rimed.TCD.GUI.Setup;
using Rimed.TCD.Utils;

namespace Rimed.TCD.GUI
{
    /// <summary>
    /// Summary description for ProbeListDlg.
    /// </summary>
    public partial class ProbeListDlg : Form
    {
        private readonly List<ProbeComboBoxItem> m_probeItems = new List<ProbeComboBoxItem>();
        private DataView m_dvBVProbe;
        private Guid m_bvIndex;

        public ProbeListDlg()
        {
            InitializeComponent();

            dsProbe1.Merge(RimedDal.Instance.ProbesDS);

            var strRes = MainForm.StringManager;
            buttonOK.Text = strRes.GetString("OK");
            buttonCancel.Text = strRes.GetString("Cancel");
            buttonHelp.Text = strRes.GetString("Help");
            Text = strRes.GetString("Probe List");
            listBox1.DisplayMember = "Name";
        }

        public Guid BVIndex
        {
            set
            {
                m_bvIndex = value;
            }
        }

        private void ProbeListDlg_Load(object sender, EventArgs e)
        {
            m_dvBVProbe = RimedDal.Instance.GetProbesByBVIndex(m_bvIndex);
            foreach (dsProbe.tb_ProbeRow row in dsProbe1.tb_Probe.Rows)
            {
				if (!row.AuthorizationMgr)	//Hide non-authorized probes
					continue;

                // fill the list box with the all the probes that exist in the system.
                var probeIndex = (Guid)row["ProbeIndex"];
                var item = new ProbeComboBoxItem(probeIndex, (String)row["ProbeName"]);
                m_probeItems.Add(item);
                var itemIndex = listBox1.Items.Add(item);
				
                // mark the probes that already refers to the selected BV.
                foreach (DataRowView r in m_dvBVProbe)
                {
                    if (probeIndex == (Guid)r["Probe_Index"] && (bool)r["Valid"])
                        listBox1.SetSelected(itemIndex, true);
                }
            }
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("buttonOK", Name);

            // delete all the rows that refers to the selected BV.
            RimedDal.Instance.DeleteBVProbesByBVIndex(m_bvIndex);

            // update the DB refers to the results of the dialog
            var indexCollection = listBox1.SelectedIndices;
            var indexInBV = 0;
            foreach (int i in indexCollection)
            {
                RimedDal.Instance.AddProbe2BV(m_bvIndex, m_probeItems[i].ProbeIndex, indexInBV);
                indexInBV++;
            }
        }
    }
}
