using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Windows.Forms;
using Rimed.Framework.Common;
using Rimed.Framework.GUI;
using Rimed.TCD.DAL;
using Rimed.TCD.Utils;
using Rimed.Tools.XZip;

namespace Rimed.TCD.GUI
{
	/// <summary>UI for restoring data from CD/HD/Network</summary>
	public partial class Restore : Form
	{
		#region Private Fields

		private List<Guid> m_selectedPatients = new List<Guid>();
		private List<Guid> m_selectedExaminations = new List<Guid>();
		private List<Patient> m_patients = new List<Patient>();
		private List<string> m_patientsNames = new List<string>();
		private List<string> m_patientsIDs = new List<string>();
		private List<Guid> m_backupSubExamIndexes = new List<Guid>();
		private double m_filesSize = 0.0;
		private List<DataRow> m_selectedExaminationRows = new List<DataRow>();

		private String m_restoreLocation = null;
		private bool m_viewOnly = false;

		#endregion Private Fields

		/// <summary>Constructor</summary>
        public Restore()
		{
            InitializeComponent();
			
            var strRes = MainForm.StringManager;
            lblPatientList.Text = strRes.GetString("Patient List");
            lblExaminationList.Text = strRes.GetString("Examination List");
			label2.Text = strRes.GetString("Name") + ":";
			bDetails.Text = strRes.GetString("Details");
			columnHeader1.Text = strRes.GetString("Date");
			columnHeader2.Text = strRes.GetString("Time");
			columnHeader3.Text = strRes.GetString("Type");
			columnHeader4.Text = strRes.GetString("Size") + ",K";
			columnHeader5.Text = strRes.GetString("Comments");
            gbRestoreInformation.Text = strRes.GetString("Restore Information");
            lbLocation.Text = strRes.GetString("Location") + ":";
            lbEstimatedTime.Text = strRes.GetString("Estimated Time") + ":";
			labelSize.Text = strRes.GetString("0K");
            lbSize.Text = strRes.GetString("Size of selected files") + ":";
			bRestore.Text = strRes.GetString("Restore");
			bHelp.Text = strRes.GetString("Help");
            bClose.Text = strRes.GetString("Close");
            gbSearchPatient.Text = strRes.GetString("Search Patient");
            label9.Text = strRes.GetString("ID") + "#:";
            gbMode.Text = strRes.GetString("Mode For Restore");
			rbUserDefined.Text = strRes.GetString("User Defined");
			rbIncrement.Text = strRes.GetString("Increment");
			rbAll.Text = strRes.GetString("All");
			folderBrowser1.Description = strRes.GetString("Restore Location");
			Text = strRes.GetString("Restore Patient Data");

			RimedDal.Instance.LoadAllSubExaminations();
			listViewExamination.Items.Clear();
			rbUserDefined_Click(this, null);

			BackColor = GlobalSettings.BackgroundDlg;
		}

		#region Event Handlers

		private void patientListBox_SelectedIndexChanged(object sender, System.EventArgs e)
		{
		    listViewExamination.Items.Clear();
			m_selectedExaminationRows.Clear();
			var selectCommand = "Patient_Index = '" + ((Patient)(patientListBox.SelectedItem)).Guid + "'";
            m_selectedExaminationRows.AddRange(RimedDal.Instance.BurnExaminationsDS.tb_Examination.Select(selectCommand));
			foreach (dsExamination.tb_ExaminationRow row in m_selectedExaminationRows) 
            {
				if (rbAll.Checked && (!m_selectedExaminations.Contains(row.Examination_Index))) 
					m_selectedExaminations.Add(row.Examination_Index);

				var item = new ListViewItem();
				item.SubItems.Add(row.Date.ToShortDateString());

				// examination time.
				var time = ((row.TimeHours < 10) ? "0" : "") + row.TimeHours + ":" + ((row.TimeMinutes < 10) ? "0" : "") + row.TimeMinutes;
				item.SubItems.Add(time);

				// examination type.
				var ds = RimedDal.Instance.GetStudyByGuid((Guid)row.Study_Index);
				item.SubItems.Add((String)ds.tb_Study.Rows[0]["Name"]);

				//if this is a monitoring study
				var size = getExaminationSize(row.Examination_Index, ((bool)ds.tb_Study.Rows[0].ItemArray[5]));

				item.SubItems.Add(size.ToString());
				item.SubItems.Add(row.Notes);
				listViewExamination.Items.Add(item);
				if (m_selectedExaminations.Contains(row.Examination_Index)) 
				{
					m_viewOnly = true;
					listViewExamination.Items[listViewExamination.Items.Count - 1].Checked = true;
					m_viewOnly = false;
				}
			}
			
			if (rbAll.Checked && m_selectedExaminations.Count >= 1)
				bRestore.Enabled = true;
		}

		private void listViewExamination_ItemCheck(object sender, System.Windows.Forms.ItemCheckEventArgs e)
		{
			if (m_viewOnly)
				return;

            var row = m_selectedExaminationRows[e.Index] as dsExamination.tb_ExaminationRow;
		    if (row == null)
		        return;

            var checkedItems    = listViewExamination.CheckedItems.Count;
			var examSize        = listViewExamination.Items[e.Index].SubItems[4].Text;
			if (e.NewValue == CheckState.Checked) 
            {
				m_selectedExaminations.Add(row.Examination_Index);
				checkedItems++;
				m_filesSize += Convert.ToDouble(double.Parse(examSize));
			}
			else 
            {
				m_selectedExaminations.Remove(row.Examination_Index);
				checkedItems--;
				m_filesSize -= Convert.ToDouble(double.Parse(examSize));
			}

			if (m_selectedExaminations.Count == 1)
				bRestore.Enabled = true;

			if (m_selectedExaminations.Count == 0)
				bRestore.Enabled = false;

			labelSize.Text = m_filesSize.ToString();

			if (checkedItems > 0) 
			{
				patientListBox.SetItemChecked(patientListBox.SelectedIndex, true);
				if (!m_selectedPatients.Contains(((Patient)(patientListBox.SelectedItem)).Guid))
					m_selectedPatients.Add(((Patient)(patientListBox.SelectedItem)).Guid);
			}
			else 
			{
				patientListBox.SetItemChecked(patientListBox.SelectedIndex, false);
				m_selectedPatients.Remove(((Patient)(patientListBox.SelectedItem)).Guid);
			}
		}

		private void bDetails_Click(object sender, System.EventArgs e)
		{
			LoggerUserActions.MouseClick("bDetails", Name);
			
			var fields = LoggerUserActions.GetFilesList();
            fields.Add("patientListBox", ((Patient)(patientListBox.SelectedItem)).Name);
			LoggerUserActions.MouseClick("bDetails", Name, fields);

			var filter = "Select * FROM tb_Patient WHERE (Patient_Index='" + ((Patient)(patientListBox.SelectedItem)).Guid + "' AND Deleted=false)";
			using (var dlg = new PatientDetails(RimedDal.Instance.GetPatients(filter)))
			{
				dlg.TopMost = true;	
				dlg.ShowDialog(this);
				Focus();
			}
		}

		private void rbAll_Click(object sender, System.EventArgs e)
		{
            LoggerUserActions.MouseClick("rbAll", Name);
            
            patientListBox.Items.Clear();
			listViewExamination.Items.Clear();
			m_selectedExaminations.Clear();
			m_patientsNames.Clear();
			m_patientsIDs.Clear();
			m_filesSize = 0.0;
			m_selectedPatients.Clear();
			m_backupSubExamIndexes.Clear();
			foreach (var p in m_patients) 
			{
				patientListBox.Items.Add(p, true);
				m_patientsNames.Add(p.Name);
				m_patientsIDs.Add(p.Id);
				if (!m_selectedPatients.Contains(p.Guid))
					m_selectedPatients.Add(p.Guid);
			}

			fillComboBoxCtrl(autoCompleteComboBoxName, m_patientsNames);
			fillComboBoxCtrl(autoCompleteComboBoxID, m_patientsIDs);
			try 
			{
				for (var i = m_patients.Count - 1; i >= 0; i--) 
				{
					autoCompleteComboBoxName.SelectedIndex = i;
					autoCompleteComboBoxID.SelectedIndex = i;
				}
			}
			catch (Exception ex) 
			{
				Logger.LogError(ex);
			}
			m_filesSize = calcFilesSize(m_backupSubExamIndexes);
			labelSize.Text = m_filesSize.ToString();
		}

		private void rbUserDefined_Click(object sender, System.EventArgs e)
		{
            LoggerUserActions.MouseClick("rbUserDefined", Name);

            listViewExamination.Items.Clear();
			m_selectedExaminations.Clear();
			m_filesSize = 0.0;
			m_backupSubExamIndexes.Clear();
			bRestore.Enabled = false;

			if (m_restoreLocation == null)
				return;

			displayBurnPackage();
		}

		private void rbIncrement_Click(object sender, System.EventArgs e)
		{
            LoggerUserActions.MouseClick("rbIncrement", Name);

            patientListBox.Items.Clear();
			listViewExamination.Items.Clear();
			m_selectedExaminations.Clear();
			m_patientsNames.Clear();
			m_patientsIDs.Clear();
			m_filesSize = 0.0;
			m_backupSubExamIndexes.Clear();

			foreach (Patient p in m_patients) 
			{
				var selectCommand = "Patient_Index = '" + p.Guid + "' AND (BackedUp='false')";
				fillExaminationsList(p, selectCommand);
			}
			fillComboBoxCtrl(autoCompleteComboBoxName, m_patientsNames);
			fillComboBoxCtrl(autoCompleteComboBoxID, m_patientsIDs);
			try 
			{
				autoCompleteComboBoxName.SelectedIndex = 0;
				autoCompleteComboBoxID.SelectedIndex = 0;
			}
			catch (Exception ex) 
			{
				Logger.LogError(ex);
			}

			m_filesSize = calcFilesSize(m_backupSubExamIndexes);
			labelSize.Text = m_filesSize.ToString();
			labelSize.Text = "0k";
		}

		private void bBrowseFolder_Click(object sender, System.EventArgs e)
		{
			LoggerUserActions.MouseClick("bBrowseFolder", Name);

			var res = folderBrowser1.ShowDialog(this);
			Focus();
			if (res != DialogResult.OK)
				return;

			try 
			{
				location.Text = folderBrowser1.SelectedPath;
				m_restoreLocation = location.Text;
				RimedDal.Instance.ReadBurnDS(m_restoreLocation);
				m_patients = new List<Patient>(RimedDal.Instance.GetBurnPatientsList());
				m_patients.Sort();

				displayBurnPackage();
				gbMode.Enabled = true;
				gbSearchPatient.Enabled = true;
			}
			catch (Exception ex) 
			{
				LoggedDialog.ShowError(this, MainForm.StringManager.GetString("The selected location does not contain valid backup information."), exception: ex);
			}
		}

		private void autoCompleteComboBoxName_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			var index = autoCompleteComboBoxName.SelectedIndex;
			autoCompleteComboBoxID.SelectedIndex = index;
			patientListBox.SelectedIndex = index;
		}

		private void autoCompleteComboBoxID_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			var index = autoCompleteComboBoxID.SelectedIndex;
			autoCompleteComboBoxName.SelectedIndex = index;
			patientListBox.SelectedIndex = index;
		}

		private void bRestore_Click(object sender, System.EventArgs e)
		{
			LoggerUserActions.MouseClick("bRestore", Name);
			using (new CursorSafeChange(this))
			{
				doRestore();
			}
		}

		private void patientListBox_ItemCheck(object sender, System.Windows.Forms.ItemCheckEventArgs e)
		{
			if (m_viewOnly)
				return;

			if (e.NewValue == CheckState.Checked)
				m_selectedPatients.Add(((Patient)((ListBox)sender).Items[e.Index]).Guid);
			else
				m_selectedPatients.Remove(((Patient)((ListBox)sender).Items[e.Index]).Guid);
		}

		private void patientListBox_MouseUp(object sender, MouseEventArgs e)
		{
			if (patientListBox.SelectedIndex == -1)
				return;
				
			//RIMD-316: Change the logic of check-boxes on Backup screen
		    var isChecked = patientListBox.GetItemChecked(patientListBox.SelectedIndex);
            for (var i = 0; i < listViewExamination.Items.Count; i++)
                listViewExamination.Items[i].Checked = isChecked;
		}

		#endregion Event Handlers

		#region Private Methods

		private void doRestore()
		{
			var selectedPatients = string.Empty;
			for (int i = 0; i < patientListBox.CheckedItems.Count; i++)
			{
				selectedPatients += ((Patient)(patientListBox.CheckedItems[i])).Name;
				if (i != patientListBox.CheckedItems.Count - 1)
					selectedPatients += "; ";
			}

			var fields = LoggerUserActions.GetFilesList();
			fields.Add("rbUserDefined", rbUserDefined.Checked ? "true" : "false");
			fields.Add("location", location.Text);
			fields.Add("patientListBox", selectedPatients);
			LoggerUserActions.MouseClick("bRestore", Name, fields);

			try
			{
				Enabled = false;
				var isRestored = restorePackage();
				Enabled = true;

				// Give notification to the user.
				if (isRestored)
					LoggedDialog.ShowInformation(this, MainForm.StringManager.GetString("Restore operation completed successfully."));
				else
					LoggedDialog.ShowWarning(this, MainForm.StringManager.GetString("Restore operation failed."));

				//RIMD-319: 12/01/2010
				listViewExamination.Items.Clear();
				patientListBox.Items.Clear();
				m_selectedExaminations.Clear();
				m_selectedPatients.Clear();

				location.Text = "";

				autoCompleteComboBoxName.Items.Clear();
				autoCompleteComboBoxName.Text = "";
				autoCompleteComboBoxID.Items.Clear();
				autoCompleteComboBoxID.Text = "";
				bDetails.Enabled = false;
				bRestore.Enabled = false;
				rbUserDefined.Checked = true;
				gbMode.Enabled = false;
				gbSearchPatient.Enabled = false;
			}
			catch (ApplicationException ex)
			{
				Logger.LogError(ex);
			}

			Enabled = true;
		}

		private void fillExaminationsList(Patient p, String selectCommand)
		{
			m_selectedExaminationRows.Clear();
			m_selectedExaminationRows.AddRange(RimedDal.Instance.ExaminationsDS.tb_Examination.Select(selectCommand));
			foreach (dsExamination.tb_ExaminationRow row in m_selectedExaminationRows) 
			{
				m_selectedExaminations.Add(row.Examination_Index);
				var filter = "ExaminationIndex = '" + row.Examination_Index + "'";
				DataView dv = RimedDal.Instance.GetBurnSubExaminationView(filter);
				foreach (DataRowView r in dv) 
				{
					if (!m_backupSubExamIndexes.Contains((Guid)r["SubExaminationIndex"]))
						m_backupSubExamIndexes.Add((Guid)r["SubExaminationIndex"]);
				}
			}

			if (m_selectedExaminationRows.Count > 0) 
			{
				patientListBox.Items.Add(p, true);
				m_patientsNames.Add(p.Name);
				m_patientsIDs.Add(p.Id);
			}
		}

		private double calcFilesSize(List<Guid> guidArr)
		{
			double size = 0;
			foreach (Guid g in guidArr)
				size += getFileSize(g);

			return size;
		}

		private double CalcFileSize(String file)
		{
			if (File.Exists(file)) 
			{
				var f = new FileInfo(file);
				return (f.Length / 1024.0);  // in k
			}
			return 0;
		}

		private void fillComboBoxCtrl(ComboBox comboBox, List<string> list)
		{
			if (comboBox.Items.Count != 0) 
			{
				comboBox.Items.Clear();
				comboBox.Text = string.Empty;
			}

			foreach (var s in list)
				comboBox.Items.Add(s);

			// Enable/Disable the details button refers to the selection in the comboBoxes.
			bDetails.Enabled = comboBox.Items.Count != 0;
		}

		private double getFileSize(Guid fileGuid)
		{
			var dirName = Path.Combine(m_restoreLocation, Constants.RAW_DATA_FOLDER, fileGuid.ToString());
			if (!Directory.Exists(dirName))
				return 0;

			var filesInDirectory = Directory.GetFileSystemEntries(dirName);
			double totalSize = 0;
			foreach (var fileName in filesInDirectory)
			{
				// check which file exist on the HD.
				var fileSize = CalcFileSize(fileName);
				totalSize += fileSize;
			}

			return totalSize;
		}

		private double getExaminationSize(Guid examIndex, bool isMonitoringExam)
		{
			var size = 0.0;
			var filter = "ExaminationIndex = '" + examIndex.ToString() + "'";
			if (isMonitoringExam)
			{
				var dirName = Path.Combine(m_restoreLocation, Constants.RAW_DATA_FOLDER, examIndex.ToString());
				var smallBackupFilesArray = Files.GetDirectoryFiles(dirName);//Directory.GetFiles(dirName)); 
				for (var i = 0; i < smallBackupFilesArray.Length; i++)
					size += CalcFileSize(smallBackupFilesArray[i]);
			}
			else 
			{
				DataView dv = RimedDal.Instance.GetSubExaminationView(filter);
				foreach (DataRowView row in dv)
					size += getFileSize((Guid)row["SubExaminationIndex"]);
			}

			return size;
		}

		private void restoreRawData(String sourceDir, String target, CopyFilesDlg cf)
		{
			if (!Directory.Exists(sourceDir))
				return;

			Files.CreatePath(target);	//cf.AddFile(sourceDir, target);
			var filesInDir = Directory.GetFileSystemEntries(sourceDir);
			foreach (var source in filesInDir) 
			{
				var targetFileName = Path.GetFileName(source);
				if (targetFileName != null)
				{
					targetFileName = Path.Combine(target, targetFileName);

					if (string.Compare(Constants.ARCHIVE_FILE_ZIP_EXTENTION, Path.GetExtension(targetFileName), StringComparison.InvariantCultureIgnoreCase) == 0)
					{
						cf.AddFile(source, targetFileName);
						m_unpackZipFiles.Add(targetFileName);
					}
					else if (string.Compare(Constants.ARCHIVE_FILE_7Z_EXTENTION, Path.GetExtension(targetFileName), StringComparison.InvariantCultureIgnoreCase) == 0)
						m_dicUnpackR7ZFiles.Add(source, target);
					else
					    cf.AddFile(source, targetFileName);
				}
			}
		}

		private bool restorePackage()
		{
			var target = String.Empty;
			var source = String.Empty;
			var sourceReplayDir = Path.Combine(m_restoreLocation, Constants.REPLAY_IMAGE_FOLDER);
			var cf = new CopyFilesDlg(Handle);
			m_unpackZipFiles.Clear();
			m_dicUnpackR7ZFiles.Clear();


			// Patient table.
			foreach (Guid g in m_selectedPatients) 
			{
                dsPatient.tb_PatientRow burnPatientRow = RimedDal.Instance.BurnPatientsDS.tb_Patient.FindByPatient_Index(g);
				dsPatient.tb_PatientRow patientRow = RimedDal.Instance.PatientsDS.tb_Patient.FindByPatient_Index(g);
				if (patientRow != null)
					patientRow.ItemArray = burnPatientRow.ItemArray;
				else 
				{
					dsPatient.tb_PatientRow newPatientRow = RimedDal.Instance.PatientsDS.tb_Patient.Newtb_PatientRow();
					newPatientRow.ItemArray = burnPatientRow.ItemArray;
					RimedDal.Instance.PatientsDS.tb_Patient.Rows.Add(newPatientRow);
				}
			}

			var dsExam = new dsExamination();
			var dsGateExam = new dsGateExamination();
			var dsSubExam = new dsSubExamination();
			var dsBVExam = new dsBVExamination();
			var dsEventExam = new dsEventExamination();

			foreach (Guid examIndex in m_selectedExaminations) 
			{
				// examination table.
                dsExamination.tb_ExaminationRow burnExamRow = RimedDal.Instance.BurnExaminationsDS.tb_Examination.FindByExamination_Index(examIndex);
				dsExamination.tb_ExaminationRow examRow = dsExam.tb_Examination.Newtb_ExaminationRow();
				examRow.ItemArray = burnExamRow.ItemArray;
				dsExam.tb_Examination.Rows.Add(examRow);

				// subExamination table & RawData files.
				var filter = "ExaminationIndex = '" + examIndex + "'";
                var burnSubExamRows = RimedDal.Instance.BurnSubExaminationsDS.tb_SubExamination.Select(filter);

				var sourceDir = Path.Combine(m_restoreLocation, Constants.RAW_DATA_FOLDER, examIndex.ToString());
				var targetDir = Path.Combine(Constants.RAW_DATA_PATH, examIndex.ToString());
				restoreRawData(sourceDir, targetDir, cf);
				foreach (var burnSubExamRow in burnSubExamRows) 
				{
					dsSubExamination.tb_SubExaminationRow subExamRow = dsSubExam.tb_SubExamination.Newtb_SubExaminationRow();
					subExamRow.ItemArray = burnSubExamRow.ItemArray;
					dsSubExam.tb_SubExamination.Rows.Add(subExamRow);
					// Add raw data files to the copy buffer.
					sourceDir = Path.Combine(m_restoreLocation, Constants.RAW_DATA_FOLDER, subExamRow.SubExaminationIndex.ToString());
					targetDir = Path.Combine(Constants.RAW_DATA_PATH, subExamRow.SubExaminationIndex.ToString());
					restoreRawData(sourceDir, targetDir, cf);

					// fake autoscan images (left and right)
					string[] tmp = new string[]
							{
								subExamRow.SubExaminationIndex + "_0.jpg",
								subExamRow.SubExaminationIndex + "_1.jpg",
							};
							
					foreach (string name in tmp) 
					{
						target = Path.Combine(Constants.REPLAY_IMG_PATH, name);
						source = Path.Combine(sourceReplayDir, name);
						if (File.Exists(source)) 
							cf.AddFile(source, target);
					}
				}

				// bvExamination table.
				filter = "ExaminationIndex = '" + examIndex + "'";
                DataRow[] burnBVExamRows = RimedDal.Instance.BurnBVExaminationsDS.tb_BVExamination.Select(filter);
				foreach (DataRow burnBVExamRow in burnBVExamRows) 
				{
					dsBVExamination.tb_BVExaminationRow newBVExamRow = dsBVExam.tb_BVExamination.Newtb_BVExaminationRow();
					newBVExamRow.ItemArray = burnBVExamRow.ItemArray;
					dsBVExam.tb_BVExamination.Rows.Add(newBVExamRow);
				}

				// ExaminationEvent table.
				filter = "Examination_Index = '" + examIndex.ToString() + "'";
                DataRow[] burnEventExaminationRows = RimedDal.Instance.BurnExaminationEventsDS.tb_ExaminationEvent.Select(filter);
				foreach (DataRow burnEventExamRow in burnEventExaminationRows) 
				{
					dsEventExamination.tb_ExaminationEventRow newEventExamRow = dsEventExam.tb_ExaminationEvent.Newtb_ExaminationEventRow();
					newEventExamRow.ItemArray = burnEventExamRow.ItemArray;
					dsEventExam.tb_ExaminationEvent.Rows.Add(newEventExamRow);

					var eventGates = Directory.GetFiles(Path.Combine(m_restoreLocation, Constants.GATES_IMAGE_FOLDER), newEventExamRow.Event_Index + "*.jpg", SearchOption.TopDirectoryOnly);
					if (eventGates.Length > 0) 
					{
						foreach (string eventGate in eventGates) 
						{
							target = Path.Combine(Constants.GATES_IMAGE_PATH, Path.GetFileName(eventGate));
							source = eventGate;
							cf.AddFile(source, target);
						}
					}
				}

				// gateExamination table & GateImg files. 
				filter = "ExaminationIndex = '" + examIndex + "'";
                DataRow[] burnGateExamRows = RimedDal.Instance.BurnGateExaminationsDS.tb_GateExamination.Select(filter);
				foreach (DataRow burnGateExamRow in burnGateExamRows) 
				{
					dsGateExamination.tb_GateExaminationRow gateExamRow = dsGateExam.tb_GateExamination.Newtb_GateExaminationRow();
					gateExamRow.ItemArray = burnGateExamRow.ItemArray;
					dsGateExam.tb_GateExamination.Rows.Add(gateExamRow);

					// add Gates Img file to copy buffer.
					String suffix = "_M";
					if (gateExamRow.Name.EndsWith("-L"))
						suffix = "_L";
					else if (gateExamRow.Name.EndsWith("-R"))
						suffix = "_R";

					target = Path.Combine(Constants.GATES_IMAGE_PATH, gateExamRow.Gate_Index + suffix + ".jpg");
					source = Path.Combine(m_restoreLocation, Constants.GATES_IMAGE_FOLDER, gateExamRow.Gate_Index + suffix + ".jpg");
					if (File.Exists(source))
						cf.AddFile(source, target);

					target = Path.Combine(Constants.SUMMARY_IMG_PATH, gateExamRow.Gate_Index + suffix + ".jpg");
					source = Path.Combine(m_restoreLocation, Constants.SUMMARY_IMAGE_FOLDER, gateExamRow.Gate_Index + suffix + ".jpg");
					if (File.Exists(source))
						cf.AddFile(source, target);

                    target = Path.Combine(Constants.TRENDS_PATH, gateExamRow.ExaminationIndex + "_" + gateExamRow.ID + ".bmp");
					source = Path.Combine(m_restoreLocation, Constants.TRENDS_IMAGE_FOLDER, gateExamRow.ExaminationIndex + "_" + gateExamRow.ID + ".bmp");
					if (File.Exists(source))
						cf.AddFile(source, target);

					//fake gate images
				    string[] tmp = new string[]
				        {
				            gateExamRow.Gate_Index.ToString() + ".jpg",
				            gateExamRow.Gate_Index.ToString() + "_CenterView.jpg",
				        };

					foreach (var name in tmp) 
					{
						target = Path.Combine(Constants.REPLAY_IMG_PATH, name);
						source = Path.Combine(sourceReplayDir, name);
						if (File.Exists(source)) 
							cf.AddFile(source, target);
					}
				}

				//IP Images Restore
				target = Path.Combine(Constants.IP_IMAGES_PATH, examIndex.ToString());
				source = Path.Combine(m_restoreLocation, Constants.IP_IMAGE_FOLDER, examIndex.ToString());
				if (Directory.Exists(source)) 
				{
					if (!Directory.Exists(target))
						Directory.CreateDirectory(target);

					string[] files = Directory.GetFiles(source);
					foreach (var file in files)
					{
						cf.AddFile(file, Path.Combine(target, file.Substring(file.LastIndexOf('\\') + 1)));
					}
				}
			}
			
			var res = cf.DoOperation(CopyFilesDlg.EFileOperation.COPY);

			unpackZipFiles();
			
			unpackR7ZFiles();

			RimedDal.Instance.UpdateRestoredData(dsExam, dsSubExam, dsBVExam, dsGateExam, dsEventExam);
			
			return res;
		}

		private readonly List<string> m_unpackZipFiles = new List<string>();
		private void unpackZipFiles()
		{
			if (m_unpackZipFiles.Count <= 0)
				return;

			PopupProgress.Show("Unpacking data files, please wait...", "Restore: Unpack data", m_unpackZipFiles.Count);
			var count = 0;
			foreach (var file in m_unpackZipFiles)
			{
				count++;
				PopupProgress.Update(count, file);

				unpackZipFile(file);
			}
			PopupProgress.HidePopup();

			m_unpackZipFiles.Clear();
		}

		private readonly Dictionary<string, string> m_dicUnpackR7ZFiles = new Dictionary<string, string>();
		private void unpackR7ZFiles()
		{
			if (m_dicUnpackR7ZFiles.Count <= 0)
				return;

			PopupProgress.Show("Unpacking data files, please wait...", "Restore: Unpack data", m_dicUnpackR7ZFiles.Count);
			var count = 0;
			foreach (var kv in m_dicUnpackR7ZFiles)
			{
				count++;
				PopupProgress.UpdateFile(count, kv.Key);

				z7Extract(kv.Key, kv.Value, true);
			}
			PopupProgress.HidePopup();

			m_dicUnpackR7ZFiles.Clear();
		}

		private bool z7Extract(string archiveFile, string targetPath, bool isOverride = false, int timeout = 10000)
		{
			if (string.IsNullOrWhiteSpace(archiveFile) || !File.Exists(archiveFile) || string.IsNullOrWhiteSpace(targetPath))
				return false;

			if (string.Compare(Constants.ARCHIVE_FILE_7Z_EXTENTION, Path.GetExtension(archiveFile), StringComparison.InvariantCultureIgnoreCase) != 0)
				return false;

			if (!Files.CreatePath(targetPath))
				return false;

			var targetFile = Path.Combine(targetPath, Path.GetFileNameWithoutExtension(archiveFile));
			if (File.Exists(targetFile) && isOverride)
				Files.Delete(targetFile);

			var result = SevenZip.Unpack(archiveFile, targetPath, timeout);
			if (!result)
				Files.Delete(targetPath);

			return result;
		}

		//private bool z7Extract2(string archiveFile, bool isOverride = false, bool isDeleteSource = true, int timeout = 10000)
		//{
		//    if (string.IsNullOrWhiteSpace(archiveFile) || !File.Exists(archiveFile))
		//        return false;

		//    if (string.Compare(Constants.ARCHIVE_FILE_7Z_EXTENTION, Path.GetExtension(archiveFile), StringComparison.InvariantCultureIgnoreCase) != 0)
		//        return false;

		//    var targetFile = Path.Combine(Path.GetDirectoryName(archiveFile), Path.GetFileNameWithoutExtension(archiveFile));
		//    if (File.Exists(targetFile))
		//    {
		//        if (isOverride)
		//            Files.Delete(targetFile);
		//        else
		//        {
		//            if (isDeleteSource)
		//                Files.Delete(archiveFile);

		//            return true;
		//        }
		//    }

		//    var result = SevenZip.Unpack(archiveFile, Path.GetDirectoryName(archiveFile));
		//    if (!result)
		//        Files.Delete(targetFile);

		//    if (isDeleteSource)
		//        Files.Delete(archiveFile);

		//    return result;
		//}

		private bool unpackZipFile(string fileName, bool isOverride = false, bool isDeleteSource = true)
		{
			if (string.IsNullOrWhiteSpace(fileName) || !File.Exists(fileName))
				return false;

			if (string.Compare(Constants.ARCHIVE_FILE_ZIP_EXTENTION, Path.GetExtension(fileName), StringComparison.InvariantCultureIgnoreCase) != 0)
				return false;

			try
			{
				var unzippedFile = Path.Combine(Path.GetDirectoryName(fileName),Path.GetFileNameWithoutExtension(fileName));
				if (File.Exists(unzippedFile))
				{
					if (isOverride)
						Files.Delete(unzippedFile);
					else
					{
						if (isDeleteSource)
							Files.Delete(fileName);

						return true;
					}
				}

				XZipManager.theXZip.UnzipFile(fileName, unzippedFile);
			}
			catch (Exception ex)
			{
				Logger.LogError(ex, string.Format("Fail to extract '{0}' file.", fileName));
				return false;
			}

			if (isDeleteSource)
				Files.Delete(fileName);

			return true;
		}
			
		private void displayBurnPackage()
		{
			//RIMD-290: Backup 1 patient and when makes restore you see many patients in the list (fix comment)
			patientListBox.Items.Clear();
			m_patientsNames.Clear();
			m_patientsIDs.Clear();

			patientListBox.Enabled = true;
			listViewExamination.Enabled = true;
			foreach (var p in m_patients) 
			{
				patientListBox.Items.Add(p, false);
				m_patientsNames.Add(p.Name);
				m_patientsIDs.Add(p.Id);
			}

			fillComboBoxCtrl(autoCompleteComboBoxName, m_patientsNames);
			fillComboBoxCtrl(autoCompleteComboBoxID, m_patientsIDs);
			try 
			{
				autoCompleteComboBoxName.SelectedIndex = 0;
				autoCompleteComboBoxID.SelectedIndex = 0;
			}
			catch (Exception ex) 
			{
				Logger.LogError(ex);
			}
		}

		#endregion Private Methods
	}
}
