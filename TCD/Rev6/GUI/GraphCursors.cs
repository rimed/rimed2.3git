using System.Drawing;
using System.Drawing.Drawing2D;

namespace Rimed.TCD.GUI
{
    /// <summary>Creates a Cursors object to handle drawing, dragging and measuring on a form.</summary>
    public class GraphCursors
    {
		//RIMD-381: delta value for Cursors moving (the "width" of cursor line)
		public const int CURSOR_THRESHOLD = 20; //original delta was = 2
		
		public enum EDirection { HORIZONTAL, VERTICAL };


	    private const int CURSOR_WIDTH = 4;
        private const int POINT_WIDTH = 2;
		
		private static readonly Color s_cursorsColor = Color.FromArgb(0, 255, 0);

        private static readonly Color s_pointColor = Color.FromArgb(25, 225, 255); //Added by Alex Feature #572 (item 2) v.2.2.1.03 22/10/2014
        //private static readonly Color s_pointColor = Color.FromArgb(35, 154, 255); //Added by Alex Feature #572 (item 2) v.2.2.1.03 22/10/2014

        private void drawCursors(Graphics grp, Point masterPt1, Point masterPt2, Point secondPt1, Point secondPt2, bool isOnMove = true)
        {
			using (var pen = new Pen(CursorsColor, CURSOR_WIDTH))
            {
                pen.DashStyle = DashStyle.Dash;
                grp.DrawLine(pen, secondPt1, secondPt2);

                if (isOnMove)
                    pen.DashStyle = DashStyle.Solid;
                grp.DrawLine(pen, masterPt1, masterPt2);
            }            
        }


        #region Public Properties

        public static Color CursorsColor { get { return s_cursorsColor; } }

        public static Color PointColor { get { return s_pointColor; } } //Added by Alex Feature #572 (item 2) v.2.2.1.03 22/10/2014

        public EDirection CursorsDirection { get; private set; }

        public int MasterCursorPos { get; set; }

        public int SecondCursorPos { get; set; }

        public string MasterCursorString { get; set; }

        public string SecondCursorString { get; set; }

        public bool CursorSelected { get; set; }

        #endregion Public Properties

        public GraphCursors(EDirection dir)
        {
            CursorsDirection = dir;
            CursorSelected = false;
        }

        public void InitCursorsPos(int width)
        {
	        MasterCursorPos = width/3;		// masterPos;
	        SecondCursorPos = 2*width/3;	// masterPos * 2;
        }

		public void DrawCursors(Size size, Graphics grp, int startLabelPosition, bool isOnMove = true)
		{

			Point masterPt1, masterPt2, secondPt1, secondPt2;
			if (CursorsDirection == EDirection.VERTICAL)
			{
				masterPt1 = new Point(MasterCursorPos, 0);
				masterPt2 = new Point(MasterCursorPos, size.Height);

				secondPt1 = new Point(SecondCursorPos, 0);
				secondPt2 = new Point(SecondCursorPos, size.Height);
			}
			else
			{
				masterPt1 = new Point(startLabelPosition, MasterCursorPos);
				masterPt2 = new Point(size.Width, MasterCursorPos);
				
				secondPt1 = new Point(startLabelPosition, SecondCursorPos);
				secondPt2 = new Point(size.Width, SecondCursorPos);
			}
			drawCursors(grp, masterPt1, masterPt2, secondPt1, secondPt2, isOnMove);

			drawString(size, grp, masterPt1, MasterCursorString);
			drawString(size, grp, secondPt1, SecondCursorString);
		}

        public void DrawPoint(Size size, Graphics grp, int startLabelPosition, GraphCursors point1, GraphCursors point2, int currentPoint, bool isOnMove = true)
        {
            using (var pen = new Pen(PointColor, POINT_WIDTH))
            {

                SolidBrush brush = new SolidBrush(PointColor); //Added by Alex Feature #572 v.2.2.1.03 (item 2) 22/10/2014

                if (currentPoint > 0)
                {
                    grp.DrawEllipse(pen, point2.MasterCursorPos - 5, point1.MasterCursorPos - 5, 10, 10);
                    Point masterPt2 = new Point(point2.MasterCursorPos + 10, point1.MasterCursorPos - 10);
                    grp.DrawString("A", GlobalSettings.FONT_16_ARIAL_BOLD, brush, masterPt2);
                }

                if (currentPoint == 2)
                {
                    grp.DrawEllipse(pen, point2.SecondCursorPos - 5, point1.SecondCursorPos - 5, 10, 10);
                    Point masterPt1 = new Point(point2.SecondCursorPos + 10, point1.SecondCursorPos - 10);
                    grp.DrawString("B", GlobalSettings.FONT_16_ARIAL_BOLD, brush, masterPt1);
                }
            }
        }
        
        //public void DrawCursors(Size size, Graphics grp, double ratioX, double ratioY, int startLabelPosition, bool isOnMove = true)
		//{
		//    GraphSize = new Size(size.Width, GraphSize.Height);
		//    drawCursors(grp, ratioX, ratioY, startLabelPosition, isOnMove);
		//}

		///// <summary>Draw cursor's lines on the spectrum/trend windows</summary>
		//private void drawCursors(Graphics grp, double ratioX, double ratioY, int startLabelPosition, bool isOnMove = true)
		//{
		//    Point masterPt1, masterPt2, secondPt1, secondPt2;
		//    if (CursorsDirection == EDirection.VERTICAL)
		//    {
		//        masterPt1 = new Point((int)(MasterCursorPos / ratioX), 0);
		//        masterPt2 = new Point((int)(MasterCursorPos / ratioX), (int)(GraphSize.Height / ratioY));
		//        secondPt1 = new Point((int)(SecondCursorPos / ratioX), 0);
		//        secondPt2 = new Point((int)(SecondCursorPos / ratioX), (int)(GraphSize.Height / ratioY));
		//    }
		//    else
		//    {
		//        masterPt1 = new Point(startLabelPosition, (int)(MasterCursorPos / ratioY));
		//        masterPt2 = new Point((int)(GraphSize.Width / ratioX), (int)(MasterCursorPos / ratioY));
		//        secondPt1 = new Point(startLabelPosition, (int)(SecondCursorPos / ratioY));
		//        secondPt2 = new Point((int)(GraphSize.Width / ratioX), (int)(SecondCursorPos / ratioY));
		//    }

		//    drawCursors(grp, masterPt1, masterPt2, secondPt1, secondPt2, isOnMove);

		//    if (MasterCursorString != null)
		//    {
		//        var stringSize = grp.MeasureString(MasterCursorString, GlobalSettings.FONT_12_ARIAL_BOLD);
		//        grp.FillRectangle(Brushes.White, new RectangleF(masterPt1, stringSize));
		//        grp.DrawString(MasterCursorString, GlobalSettings.FONT_12_ARIAL_BOLD, Brushes.Purple, masterPt1);
		//    }
		//    if (SecondCursorString != null)
		//    {
		//        var stringSize = grp.MeasureString(SecondCursorString, GlobalSettings.FONT_12_ARIAL_BOLD);
		//        grp.FillRectangle(Brushes.White, new RectangleF(secondPt1, stringSize));
		//        grp.DrawString(SecondCursorString, GlobalSettings.FONT_12_ARIAL_BOLD, Brushes.Purple, secondPt1);
		//    }
		//}

		private void drawString(Size size, Graphics grp, Point location, string text)
		{
			if (string.IsNullOrWhiteSpace(text))
				return;

			var stringSize	= grp.MeasureString(text, GlobalSettings.FONT_12_ARIAL_BOLD);
			if (location.X + stringSize.Width >= size.Width)
				location.X = (int)(location.X - stringSize.Width);

			if (location.Y + stringSize.Height >= size.Height)
				location.Y = (int)(location.Y - stringSize.Height);

			var rect = new RectangleF(location, stringSize);
			grp.FillRectangle(Brushes.White, rect);
			using (var pen = new Pen(Brushes.DarkOrange))
			{
				grp.DrawRectangle(pen, rect.X, rect.Y, rect.Width, rect.Height);
			}

			grp.DrawString(text, GlobalSettings.FONT_12_ARIAL_BOLD, Brushes.Purple, location);
		}
		public void FrameCursors(Size size)
		{
			var diff = (CursorsDirection == EDirection.HORIZONTAL) ? size.Height : size.Width;
			if (MasterCursorPos < 0 || MasterCursorPos > diff || SecondCursorPos < 0 || SecondCursorPos > diff)
				InitCursorsPos(diff);

			//if (CursorsDirection == EDirection.HORIZONTAL)
			//{
			//    if (MasterCursorPos < 0 || MasterCursorPos > size.Height || SecondCursorPos < 0 || SecondCursorPos > size.Height)
			//        InitCursorsPos(size.Height);
			//}
			//else
			//{
			//    if (MasterCursorPos < 0 || MasterCursorPos > size.Width || SecondCursorPos < 0 || SecondCursorPos > size.Width)
			//        InitCursorsPos(size.Width);
			//}
		}

		/// <summary>Draw vertical cursor's lines on the spectrum windows in monitoring mode</summary>
		public void DrawCursors(Size size, Graphics grp, Image t1, Image t2, bool isOnMove = true)
		{
			var masterPt1 = new Point(MasterCursorPos, 0);
			var masterPt2 = new Point(MasterCursorPos, size.Height);

			var secondPt1 = new Point(SecondCursorPos, 0);
			var secondPt2 = new Point(SecondCursorPos, size.Height);
			drawCursors(grp, masterPt1, masterPt2, secondPt1, secondPt2, isOnMove);

			if (masterPt1.X > secondPt1.X)
			{
				var tmp = masterPt1;
				masterPt1 = secondPt1;
				secondPt1 = tmp;
			}

			if (MasterCursorString != null)
				grp.DrawImage(t1, masterPt1);

			if (SecondCursorString != null)
				grp.DrawImage(t2, secondPt1);
		}
	
		///// <summary>Draw vertical cursor's lines on the spectrum windows in monitoring mode</summary>
		//public void DrawCursors(Size size, Graphics grp, double ratioX, double ratioY, Image t1, Image t2, bool isOnMove = true)
		//{
		//    GraphSize = new Size(size.Width, GraphSize.Height); //GraphSize = size;
		//    drawCursors(grp, ratioX, ratioY, t1, t2, isOnMove);
		//}

		///// <summary>Draw vertical cursor's lines on the spectrum windows in monitoring mode</summary>
		//private void drawCursors(Graphics grp, double ratioX, double ratioY, Image t1, Image t2, bool isOnMove = true)
		//{
		//    var masterPt1 = new Point((int)(MasterCursorPos / ratioX), 0);
		//    var masterPt2 = new Point((int)(MasterCursorPos / ratioX), (int)(GraphSize.Height / ratioY));
		//    var secondPt1 = new Point((int)(SecondCursorPos / ratioX), 0);
		//    var secondPt2 = new Point((int)(SecondCursorPos / ratioX), (int)(GraphSize.Height / ratioY));

		//    drawCursors(grp, masterPt1, masterPt2, secondPt1, secondPt2, isOnMove);

		//    if (masterPt1.X > secondPt1.X)
		//    {
		//        var tmp = masterPt1;
		//        masterPt1 = secondPt1;
		//        secondPt1 = tmp;
		//    }

		//    if (MasterCursorString != null)
		//        grp.DrawImage(t1, masterPt1);

		//    if (SecondCursorString != null)
		//        grp.DrawImage(t2, secondPt1);
		//}

        public bool IsCloseToCursors(int xInGraph, int yInGraph)
        {
            var pos = (CursorsDirection == EDirection.VERTICAL ? xInGraph : yInGraph);

            if (MasterCursorPos <= pos + CURSOR_THRESHOLD && MasterCursorPos >= pos - CURSOR_THRESHOLD)
                return true;

            if (SecondCursorPos <= pos + CURSOR_THRESHOLD && SecondCursorPos >= pos - CURSOR_THRESHOLD)
               return true;

            return false;
        }
        
        public bool SelectCursor(int xInGraph, int yInGraph, ref int deltaInGraph)
        {
            var pos = CursorsDirection == EDirection.VERTICAL ? xInGraph : yInGraph;

            if (MasterCursorPos <= pos + CURSOR_THRESHOLD && MasterCursorPos >= pos - CURSOR_THRESHOLD)
            {
                CursorSelected = true;
                deltaInGraph = pos - MasterCursorPos;
            }
            else if (SecondCursorPos <= pos + CURSOR_THRESHOLD && SecondCursorPos >= pos - CURSOR_THRESHOLD)
            {
                // make the selected cursor the master
                CursorSelected = true;
                var secondCursorPos = SecondCursorPos;
                SecondCursorPos		= MasterCursorPos;
                MasterCursorPos		= secondCursorPos;
                deltaInGraph		= pos - secondCursorPos;
            }

            return CursorSelected;
        }
    }
}
