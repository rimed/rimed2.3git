//#define AUDIO_SAMPLES_WRITE

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Resources;
using System.Text;
using System.Threading; 
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Runtime.InteropServices;
using System.Linq;

using Rimed.Framework.Common;
using Rimed.Framework.Config;
using Rimed.Framework.GUI;
using Rimed.Framework.Threads;
using Rimed.Framework.WinAPI;
using Rimed.Framework.WinInternal;
using Rimed.TCD.DAL;
using Rimed.TCD.DSPData;
using Rimed.TCD.GUI.DSPMgr;
using Rimed.TCD.GUI.Setup;
using Rimed.TCD.ManagedInfrastructure;
using Rimed.TCD.RemoteControl;
using Rimed.TCD.ReportMgr;
using ScreenCapture_MedSim;
using Syncfusion.Windows.Forms.Tools;
using Syncfusion.Windows.Forms.Tools.XPMenus;
using Constants = Rimed.TCD.Utils.Constants;
using Rimed.TCD.Utils;
using MedSim.XML;
using Rimed.Tools.ScreenCapture;

namespace Rimed.TCD.GUI
{
	public sealed partial class MainForm : Form
	{

		private static readonly WindowsSpecialKeysWatcher s_windowsSpecialKeysWatcher = new WindowsSpecialKeysWatcher();

		private const double INITIAL_WINDOW_TIME = 5; //5 Sec

        public const int DEFAULT_SPEED_FACTOR_INDEX = 1; 

		#region Private Fields

		private const int MAX_PROCESS_USER_OBJECTS = 10000;
		private const int PROCESS_USER_OBJECTS_THRESHOLD = 7500;
		private const string DEFAULT_LAST_STUDY_ID = RimedDal.IntracranialUnilateral_STUDY_ID; //Intracranial unilateral study
		private const string FMT_MENU_UNDERLINE_NAME = "&{0}";
		private const string VIRTUAL_KEYBOARD_EXECUTABLE = @"C:\WINDOWS\system32\osk.exe";

		public bool IsInitialized { get; private set; }
		private const string FMT_DATE_TIME = "yyyy.MM.dd HH:mm:ss";

		private enum EGuiMgrSmEvent
		{
			GUI_TOGGLE_FREEZE_EVENT,
			GUI_BUTTON_PLAY_EVENT,
			GUI_BUTTON_PAUSE_EVENT,
			GUI_BUTTON_SUMMARY_CLICK_EVENT,
			GUI_KEY_SPACE_EVENT,
			GUI_KEY_NEXT_BV_EVENT,
			GUI_BAR_ITEM_PLAY_EVENT,
			GUI_LOAD_PATIENT_EVENT,
			GUI_NEW_STUDY_MONITORING_EVENT,
			GUI_NEW_STUDY_DIAGNOSTIC_EVENT,
			GUI_FREEZE_EVENT,
			GUI_UNFREEZE_EVENT,
			GUI_FINISH_STUDY_SAVE_STORAGE_EVENT,
			GUI_FINISH_STUDY_REMOVE_STORAGE_EVENT,
			GUI_NEW_REPLAY_MONITORING_EVENT,
			GUI_NEW_REPLAY_DIAGNOSTIC_EVENT,
			GUI_NEW_REPLAY_SUMMARY_IMG_EVENT,
			GUI_REPLAY_PAUSE_EVENT,
			GUI_REPLAY_END_OF_DATA_EVENT,
			GUI_SCROLL_EVENT
		}

		private class CGuiSmEvent : CSmEvent<EGuiMgrSmEvent>
		{
			public CGuiSmEvent(EGuiMgrSmEvent smEventNum) : base(smEventNum)
			{
				EventNum = smEventNum;
			}

			public CGuiSmEvent(EGuiMgrSmEvent smEventNum, Object[] paramsArr) : base(smEventNum, paramsArr)
			{
				EventNum = smEventNum;
			}

			private EGuiMgrSmEvent EventNum { get; set; }

			public override string ToString()
			{
				return string.Format("CGuiSmEvent: {0}", EventNum);
			}
		}


		private enum EGuiMgrSmState
		{
			GUI_HW_INIT_STATE,
			GUI_OFFLINE_STATE,
			GUI_MONITORING_ONLINE_STATE,
			GUI_DIAGNOSTIC_ONLINE_STATE,
			GUI_MONITORING_ONLINE_FREEZE_STATE,
			GUI_DIAGNOSTIC_ONLINE_FREEZE_STATE,
			GUI_END_OF_MONITORING_EXAM_STATE,
			GUI_REPLAY_STATE, // TODO Possibly this should be hierarchical SM accordingly do objects definition.
			GUI_REPLAY_PAUSE_STATE,
			GUI_REPLAY_END_OF_PLAYBACK_STATE,
			GUI_REPLAY_DIAGNOSTIC_WITHOUT_RAW_DATA,
			GUI_FPGA_LOAD_STATE
		}

		private EGuiMgrSmState m_smState;

		/// <summary>Virtual Keyboard Process</summary>
		private Process m_virtuapKeyboardProcess;

		private Thread m_pwrDnThread;

		private bool m_serialNumberValid = true;

		private bool m_displayWarningReportFirstTime = true;

		private int m_systemVolume;
		private Guid m_selectedPatientIndex = Guid.Empty;
		private GlobalTypes.EStudyType m_oldStudyType = GlobalTypes.EStudyType.Unilateral;
		private string m_studyString = string.Empty;

		//sergey 
		private int m_probIndexForUpdateSensitivity = 0;
		private static bool s_sOpenPanel = true;

		// The following strings are used for the text in the caption
		private string m_patientName = string.Empty;
        private string m_patientID = string.Empty;
        private string m_currentStudyName = string.Empty;
		private string m_timeStr = string.Empty;

		private DateTime m_monitoringReplayTimeCounter = DateTime.Now;
		private bool m_recordPaused = false;
		public bool DspRecordMode = false;
		private readonly List<Delegate> m_nextFunctionList;
		private bool m_fakeDataRowMode = false;
		private bool m_shutdownWindows = false;
		private bool m_exitToWindows = false;

		private UInt64 m_studyId;
		private const int GUI_SM_SYNC_CALL_TIMEOUT = 15000; //150000;

		#endregion Private Fields

		#region Public Properties

		private bool MainMenuItemPlayEnabled
		{
			get { return barItemPlay.Enabled; }
			set { barItemPlay.Enabled = value; }
		}

		private bool MainMenuItemStopEnabled
		{
			get { return barItemStop.Enabled; }
			set { barItemStop.Enabled = value; }
		}

        private bool MainMenuItemAccemssionNumberEnabled
        {
            get { return barItemAccessionNumber.Enabled; }
            set { barItemAccessionNumber.Enabled = value; }
        }
        
        private bool MainMenuItemCursorsEnabled
		{
			get { return barItemCursors.Enabled; }
			set { barItemCursors.Enabled = value; }
		}

		private bool MainMenuItemAutoscanEnabled
		{
            get { return barItemAutoscan.Enabled; }
            set { barItemAutoscan.Enabled = value; }
		}

		private bool IsClosing { get; set; }
         
		public bool IsMute { get; private set; }

		public PanelsArr PanelAutoscan { get; private set; }
		public PanelsArr PanelsCharts { get; private set; }
		public PanelsArr PanelsTrends { get; private set; }
		public PanelsArr Panels { get; private set; }

		public static ResourceManagerWrapper StringManager
		{
			get
			{
				lock (s_stringManagerLock)
				{
					if (s_stringManager == null)
					{
						var recource = new ResourceManager(typeof (MainForm).Namespace + LayoutManager.Instance.LanguageResourceFileName, typeof (MainForm).Assembly);
						s_stringManager = ResourceManagerWrapper.GetWrapper(recource);
					}
				}

				return s_stringManager;
			}
		}

		public static ToolBarPanel ToolBar
		{
			get { return Instance.toolBarPanel1; }
		}

		public ComboBox ComboBoxTimeRight
		{
			get { return comboBoxTimeRight; }
		}

        public ComboBox ComboBoxTimeLeft
        {
            get { return comboBoxTimeLeft; }
        }
        
        public double SpectrumTime
		{
			get { return Convert.ToDouble(toolBarPanel1.ComboBoxTime.Text); }
		}

		public static MainForm Instance { get; private set; }

		public bool IsLayoutRepNoBg { get; set; }


        // Added by Alex bug #588 v.2.2.2.1    05/10/2014
        public bool IsFirstScreen
        {
            get { return isFirstScreen; }
            set { isFirstScreen = value; }
        }

        private bool isFirstScreen = true;

        
        /// <summary>The report won't be displayed if the function was called from the Summary report scenario</summary>
		public bool DisplayPatientReport { get; set; }

		public int SystemVolume
		{
			get { return m_systemVolume; }
			set
			{
				m_freezeVolumeIncrease = 0;
				setVolume(value);
 			}
		}

		private void setVolume(int value)
		{
			m_systemVolume = value;
			if (m_systemVolume > GlobalSettings.VOLUME_MAX)
				m_systemVolume = GlobalSettings.VOLUME_MAX;
			else if (m_systemVolume < GlobalSettings.VOLUME_MIN)
				m_systemVolume = GlobalSettings.VOLUME_MIN;

			DspManager.Instance.WriteVolume2Dsp((ushort) m_systemVolume);
			fireVolumeChanged(m_systemVolume, false);
            if (m_systemVolume > 0)
                IsMute = false;

			AudioDSPtoPCStreamPlayer.SetVolume(m_systemVolume);
		}

		public GlobalTypes.EWorkingMode WorkingMode { get; set; }

		public sbyte MicroStepCounter { private get; set; }

		private bool AtTheEndOfMonitoringExam { get; set; }

        public void FinishStudy(bool saveToStorage)
		{
			if (m_smState == EGuiMgrSmState.GUI_HW_INIT_STATE)
			{
				Logger.LogWarning("DSP not initialized. Save study aborted.");
				return;
			}

			var smEvent = new CGuiSmEvent(saveToStorage ? EGuiMgrSmEvent.GUI_FINISH_STUDY_SAVE_STORAGE_EVENT : EGuiMgrSmEvent.GUI_FINISH_STUDY_REMOVE_STORAGE_EVENT);

			guiMgrSmGo(smEvent);
		}

		private void mainFormNewReplayDelegateFunc(Boolean isMonitoring, string dirName)
		{
			var smEventParamsArr = new Object[1];
			CGuiSmEvent smEvent;
			smEventParamsArr[0] = dirName;

			if (dirName == null)
				smEvent = new CGuiSmEvent(EGuiMgrSmEvent.GUI_NEW_REPLAY_SUMMARY_IMG_EVENT);
			else if (isMonitoring)
				smEvent = new CGuiSmEvent(EGuiMgrSmEvent.GUI_NEW_REPLAY_MONITORING_EVENT, smEventParamsArr);
			else
				smEvent = new CGuiSmEvent(EGuiMgrSmEvent.GUI_NEW_REPLAY_DIAGNOSTIC_EVENT, smEventParamsArr);

			guiMgrSmGo(smEvent);
		}

		public void NewReplay(bool isMonitoring, string dirName)
		{
			mainFormNewReplayDelegateFunc(isMonitoring, dirName);
		}

		public void NewDiagnosticBvExam()
		{
			guiMgrSmGo(new CGuiSmEvent(EGuiMgrSmEvent.GUI_KEY_NEXT_BV_EVENT));
		}

		public Guid SelectedPatientIndex
		{
			get { return m_selectedPatientIndex; }
			set
			{
                // Added by Alex 3/11/2015 feature #672 v 2.2.3.15
                var name = string.Empty;
				var id = string.Empty;
				m_selectedPatientIndex = value;

				var row = RimedDal.Instance.GetPatientByIndex(m_selectedPatientIndex);
			    if (row != null)
			    {
			        name = string.Format("{0} {1}", row.Last_Name, row.First_Name);
                    // Added by Alex 3/11/2015 feature #672 v 2.2.3.15
                    id = row.ID;
			    }

                // Changed by Alex 3/11/2015 feature #672 v 2.2.3.15
                setPatientName(name, id);
			}
		}

		public Guid SelectedPatientExaminationIndex
		{
			get
			{
				if (RimedDal.Instance == null)
					return Guid.Empty;

				var examRow = RimedDal.Instance.LoadedExamination();
				return (examRow == null) ? Guid.Empty : examRow.Examination_Index;

				//var index = Guid.Empty;
				//try
				//{
				//    if (RimedDal.Instance != null && RimedDal.Instance.ExaminationsDS.tb_Examination.Rows.Count > 0)
				//        index = (Guid)RimedDal.Instance.ExaminationsDS.tb_Examination.Rows[0]["Examination_Index"];
				//}
				//catch (Exception ex)
				//{
				//    Logger.LogError(ex);
				//    index = Guid.Empty;
				//}

				//return index;
			}
		}

		public GlobalTypes.EStudyType StudyType { get; private set; }

		public bool IsBilateralStudy
		{
			get { return StudyType == GlobalTypes.EStudyType.Bilateral; }
		}

		public bool IsUnilateralStudy
		{
			get { return StudyType == GlobalTypes.EStudyType.Unilateral; }
		}

		public bool IsNotUnilateralStudy
		{
			get { return !IsUnilateralStudy; }
		}

		private bool ChangeStudy { get; set; }

		public Guid CurrentStudyId { get; set; }

		public bool ScrollingEnabled { get; set; }

		/// <summary>Set the Monitoring Replay Time Counter</summary>
		public DateTime MonitoringReplayTimeCounter
		{
			get { return m_monitoringReplayTimeCounter; }
			set
			{
				m_monitoringReplayTimeCounter = value;

				var labelText = value.ToString("HH:mm:ss");
				toolBarPanel1.TimeLabelText = labelText;
				toolBarPanel1.Refresh();
			}
		}

		private DateTime MonitoringReplayStartTime
		{
			get { return m_monitoringReplayStartTime; }
			set
			{
				m_monitoringReplayStartTime = value;
				MonitoringReplayTimeCounter = value;
			}
		}

		/// <summary>Set Study name in caption</summary>
		public string CurrentStudyName
		{
			get { return m_currentStudyName; }
			private set
			{
				m_currentStudyName = value;
				setCaptionText();
			}
		}

		/// <summary>Set Study name in caption</summary>
		private bool BarItemSaveStudyEnabled
		{
			get { return barItemSaveStudy.Enabled; }
			set
			{
				if (InvokeRequired)
					BeginInvoke((Action) delegate() { barItemSaveStudy.Enabled = value; });
				else
					barItemSaveStudy.Enabled = value;
			}
		}

		private bool BarItemDeleteStudyEnabled
		{
			get { return barItemDeleteStudy.Enabled; }
			set
			{
				if (InvokeRequired)
					BeginInvoke((Action) delegate() { barItemDeleteStudy.Enabled = value; });
				else
					barItemDeleteStudy.Enabled = value;
			}
		}

		/// <summary>Monitoring properties</summary>
		public bool IsRecordMode { get; set; }

		public bool IsMonitoringSaved { get; set; }

        // Added by Alex 04/04/2016 fixed bug #NN v. 2.2.3.28
        public bool IsMonitoringStoped { get; set; }
        
        public void ExitWindow(bool confirm = true)
		{
			Logger.LogTrace("ExitWindow()", Name);

			if (confirm && LoggedDialog.Show(this, "Exit Rimed TCD and shutdown the System?", "COMPUTER SHOTDOWN", MessageBoxButtons.YesNo, MessageBoxIcon.Stop, MessageBoxDefaultButton.Button2) != DialogResult.Yes)
				return;

			ExitDigiLite(isShutdownWindows: true);
		}

		private bool DebugDspActive { get; set; }

		#endregion Public Properties

		#region Public Get properties to container controls

		public Panel MainChartsPanel
		{
			get { return panelMainCharts; }
		}

		public Panel RightChartsPanel
		{
			get { return panelRightCharts; }
		}

		public Panel RightPanel
		{
			get { return panelRight; }
		}

		public Splitter LeftSplitter
		{
			get { return splitterLeft; }
		}

		public Splitter RightSplitter
		{
			get { return splitterRight; }
		}

		public Panel LeftChartsPanel
		{
			get { return panelLeftCharts; }
		}

		public Panel LeftPanel
		{
			get { return panelLeft; }
		}

		public Panel MiddlePanel
		{
			get { return panelMiddle; }
		}

		public Panel TrendPanel
		{
			get { return panelTrend; }
		}

		public Splitter BottomSplitter
		{
			get { return splitterBottom; }
		}

		public Splitter BottomSplitter2
		{
			get { return splitterBottom2; }
		}

		public Panel PanelHeaderRight
		{
			get { return panelHeaderRight; }
		}

		public Panel PanelHeaderLeft
		{
			get { return panelHeaderLeft; }
		}

		public HitsHistogram HitsHistogram
		{
			get { return hitsHistogram1; }
		}

		#endregion Public compoennt's Get properties

		#region Events and Delegates

		/// <summary>Event fired when volume is changed</summary>
		public event VolumeChangedHandler VolumeChangedEvent;

		public delegate void VolumeChangedHandler(int volume, bool isMute);

		public delegate bool NextBVDelegate();

		public event NextBVDelegate NextBVEvent;
		public event Action CurrentBVEvent;
		public event Action PrevBVEvent;

		#endregion Events and Delegates

		private System.Windows.Forms.Timer m_oneSecondTimer = new System.Windows.Forms.Timer();
		private DateTime m_lastTimerUtcTime = DateTime.UtcNow;

		private bool initVerifySerialNumber()
		{
            if (!AppSettings.Active.IsReviewStation) // added by bug #578 v.2.2.1.00 28/09/2014 
            {
                m_serialNumberValid = AuthorizationMgrDlg.CheckSerialNumber();
                if (m_serialNumberValid)
                    return true;

                //barItemAuthorizationMgr_Click(this, null);

                //m_serialNumberValid = AuthorizationMgrDlg.CheckSerialNumber();
                //if (m_serialNumberValid)
                //    return true;

                if (LoggedDialog.ShowQuestion(this, "Authorization serial number Expired or invalid.\n\nDo you want to continue in Review Station mode?") != DialogResult.Yes)
                {
                    LoggedDialog.ShowNotification(this, "Click OK to terminate the application.");

                    return false;
                }

                AppSettings.Active.AppWorkMode = AppSettings.EAppWorkMode.DEMO;
                AppSettings.Active.IsDspOffMode = true;
            }

            return true;
		}

		private dsConfiguation.tb_BasicConfigurationRow initLoadLastStudy()
		{
			try
			{
				var basicCfg = RimedDal.Instance.ConfigDS.tb_BasicConfiguration[0];

				string studyStr;
				var tempStudy = RimedDal.Instance.GetStudy(basicCfg.LastStudy);

				//Bug fix: LastStudy not exists
				if (tempStudy == null)
				{
					var studyId = AppConfig.GetConfigValue("Default last study id", DEFAULT_LAST_STUDY_ID);
					Logger.LogWarning("Last study Id ({0}) is invalid. Loading default study (Id={1}).", basicCfg.LastStudy, studyId);

					tempStudy = RimedDal.Instance.GetStudy(studyId);
					if (tempStudy == null)
						throw new ApplicationException(string.Format("Default study (Id={0}) load fail.", studyId));

					basicCfg.LastStudy = new Guid(studyId);
				}

				if (tempStudy.DefaultStudy)
				{
					studyStr = tempStudy.Name;
					CurrentStudyName = studyStr;
				}
				else
				{
					studyStr = RimedDal.Instance.GetStudy(tempStudy.BasedOn).Name;
					CurrentStudyName = studyStr + " - " + tempStudy.Name;
				}

				setWorkingStudyType(studyStr);

				return basicCfg;
			}
			catch (Exception ex)
			{
				NewSplashScreen.SetText("Initializing error.");
				var msg = "Initialization error. " + ex.Message + "\n\nClick OK to terminate the application.";
				LoggedDialog.ShowFatalError(this, msg, exception: ex);

				return null;
			}
		}

		private bool startDSPManager(dsConfiguation.tb_BasicConfigurationRow basicCfg)
		{
			NewSplashScreen.SetText("Initializing DSP");
			DspPerformanceCounters.Init();

			DspManager.Start();

			if (!initDSPCommunication())
			{
				if (!AppSettings.Active.IsDspOffMode && LoggedDialog.ShowError(this, "DSP Initialization fail.\n\nDo you want to continue in DEMO/Read-Only mode?", buttons: MessageBoxButtons.YesNo) != DialogResult.Yes)
				{
					LoggedDialog.ShowError(this, "DSP Initialization fail.\n\nClick OK to terminate the application.");

					return false;
				}

				AppSettings.Active.IsDspOffMode = true;

				DspManager.Restart();
			}

			if (!AppSettings.Active.IsDspOffMode && !DspManager.Instance.IsCommInitiated)
				Logger.LogWarning("ATTENTION: DSP Not initiated.");

			DspManager.DspEndPlayback += Instance.onDspEndPlayback;
			DspManager.DspEndDrawHistoryEvent += Instance.onDspEndDrawHistoryEvent;

			DspManager.Instance.WriteSpeakerOrHeadphone2Dsp(basicCfg.SpeakerActive);

            //Added by Alex v.2.2.2.05 18/12/2014
            DspManager.Instance.WriteAveragingCP2Dsp(basicCfg.AveragingCalc);

            //Added by Alex Feature #755 v.2.2.3.19 04/01/2016
            DspManager.Instance.WriteAutoMuteEnable2Dsp(basicCfg.AutoMuteEnable);

            SystemVolume = (basicCfg.SpeakerActive ? basicCfg.SpeakerVolume : basicCfg.HeadphoneVolume);

			return true;
		}

		private bool initRemoteControl()
		{
			try
			{
				SerialPortWrapper.Instance.Init(serialOperation);
				NewSplashScreen.SetText(string.Format("Remote control (port: {0}) {1}", SerialPortWrapper.Instance.Port, SerialPortWrapper.Instance.IsOpen ? "Enabled" : "Disabled"));


				if (!AppSettings.Active.IsReviewStation && SerialPortWrapper.Instance.IsEnabled && !SerialPortWrapper.Instance.IsOpen)
                    Logger.SystemInfoLog("+NOTE: remote control is disabled.");

                // Deleted by Alex CR #866 CR #866  v.2.2.3.37 25/05/2016
                //LoggedDialog.ShowNotification(this, "+NOTE: remote control is disabled.", "Remote control");
            }
			catch (Exception ex)
			{
				var res = LoggedDialog.ShowQuestion(this, string.Format("RemoteControl Initialization fail.\n{0}\n\nDo you want to continue without the RemoteControl?.", ex.Message));
				if (res != DialogResult.Yes)
					return false;
			}

			return true;
		}

		private bool initLayout()
		{
			LayoutManager.Instance.Init(this);

			LayoutManager.Instance.Layout_Load();

			//initialization of MainForm labels
			reloadControlLabels();

			panelHeaderRight.BackColor = GlobalSettings.P2;
			panelHeaderRight.ForeColor = GlobalSettings.P7;
			panelHeaderLeft.BackColor = GlobalSettings.P2;
			panelHeaderLeft.ForeColor = GlobalSettings.P7;

			panelHITS2.BackColor = GlobalSettings.P3;
			panelHITS2.ForeColor = GlobalSettings.P7;

			toolBarPanel1.ComboBoxTime.SelectedIndexChanged += comboBoxTime_SelectedIndexChanged;
			toolBarPanel1.ComboBoxTime.SelectionChangeCommitted += comboBoxTime_SelectionChangeCommitted;

			toolBarPanel1.Init();
			return true;
		}

		private bool initReportEngine()
		{
			try
			{
				BaseReport.Initialize();

				return true;
			}
			catch (Exception ex)
			{
				NewSplashScreen.SetText("Report engine initializing fail.");
				var msg = "Report engine initialization error. " + ex.Message + "\n\nClick OK to terminate the application.";
				LoggedDialog.ShowFatalError(this, msg, exception: ex);

				return false;
			}
		}

        // Deleted by Alex 27/04/2016 bug #854 v 2.2.3.31
        //private bool init()
        //{
        //    NewSplashScreen.SetText("Verifying serial number.....");
        //    if (! initVerifySerialNumber())
        //        return false;

        //    NewSplashScreen.SetText("Initiating Remote control....");
        //    if (!initRemoteControl())
        //        return false;

        //    NewSplashScreen.SetText("Initiating Dummy Data....");
        //    bvListViewCtrl1.RegisterEvents();
        //    SpectrumGraph.Init();
        //    AutoScanGraph.Init();

        //    NewSplashScreen.SetText("Loading last study.....");
        //    var basicCfg = initLoadLastStudy();
        //    if (basicCfg == null)
        //        return false;

        //    NewSplashScreen.SetText("Starting DSP Manager.....");
        //    if (!startDSPManager(basicCfg))
        //        return false;

        //    NewSplashScreen.SetText("Initializing Layout manager.....");
        //    if (!initLayout())
        //        return false;

        //    updateStudiesMenus();

        //    NewSplashScreen.SetText("Initializing report engine....");
        //    if (!initReportEngine())
        //        return false;

        //    NewSplashScreen.SetText("Loading undefined patient.....");
        //    loadUndefinedPatient();

        //    NewSplashScreen.SetText(string.Format("Loading '{0}' study [{1}]....", CurrentStudyName, basicCfg.LastStudy));
        //    if (!loadSelectedStudy(basicCfg.LastStudy, false))
        //    {
        //        LoggedDialog.ShowError(this, string.Format("{0} Study load fail.\n\nClick OK to terminate the application.", CurrentStudyName));

        //        return false;
        //    }

        //    NewSplashScreen.SetText("Enabling licensed features....");
        //    enableFeatures();

        //    NewSplashScreen.SetText("Initialize DB......");
        //    retrieveDB();

        //    NewSplashScreen.SetText("Initializing temporary work space....");
        //    ExaminationRawData.Init(AppSettings.Active.PlaybackRawDataCacheForward, AppSettings.Active.PlaybackRawDataCacheBackward);


        //    DicomExporter.EchoRetryCount = AppSettings.Active.PACSEchoRetryCount;
        //    DicomExporter.ModelName = string.Format("{0} v{1}", AppSettings.Active.ApplicationProductName, AppGlobals.FileVersion);


        //    ReportViewer.IsRetainSentDicom = AppSettings.Active.PACSRetainSentDicom;
        //    ReportViewer.IsRetainSentImage = AppSettings.Active.PACSRetainSentImage;

        //    // Start the time display timer
        //    NewSplashScreen.SetText("Initializing main timer....");
        //    m_timeStr = DateTime.Now.ToString(FMT_DATE_TIME);
        //    m_oneSecondTimer.Tick += onOneSecondTimer_Tick;
        //    m_oneSecondTimer.Interval = 1000;
        //    m_oneSecondTimer.Start();
        //    m_lastTimerUtcTime = DateTime.UtcNow;

        //    EventListViewCtrl.Instance.UseRelativeTime = AppSettings.Active.IsUseEventsRelativeTime;

        //    Logger.SystemInfoLog("Application initialized.");

        //    IsInitialized = true;

        //    return true;
        //}

        // Changed by Alex 27/04/2016 bug #854  v 2.2.3.31
        private bool init()
        {
            NewSplashScreen.SetText("8%");
            if (!initVerifySerialNumber())
                return false;

            NewSplashScreen.SetText("15%");
            if (!initRemoteControl())
                return false;

            NewSplashScreen.SetText("23%");
            bvListViewCtrl1.RegisterEvents();
            SpectrumGraph.Init();
            AutoScanGraph.Init();

            NewSplashScreen.SetText("31%");
            var basicCfg = initLoadLastStudy();
            if (basicCfg == null)
                return false;

            NewSplashScreen.SetText("38%");
            if (!startDSPManager(basicCfg))
                return false;

            NewSplashScreen.SetText("46%");
            if (!initLayout())
                return false;

            updateStudiesMenus();

            NewSplashScreen.SetText("54%");
            if (!initReportEngine())
                return false;

            NewSplashScreen.SetText("62%");
            loadUndefinedPatient();

            NewSplashScreen.SetText("69%");
            if (!loadSelectedStudy(basicCfg.LastStudy, false))
            {
                LoggedDialog.ShowError(this, string.Format("{0} Study load fail.\n\nClick OK to terminate the application.", CurrentStudyName));

                return false;
            }

            NewSplashScreen.SetText("77%");
            enableFeatures();

            NewSplashScreen.SetText("85%");
            retrieveDB();

            NewSplashScreen.SetText("92%");
            ExaminationRawData.Init(AppSettings.Active.PlaybackRawDataCacheForward, AppSettings.Active.PlaybackRawDataCacheBackward);


            DicomExporter.EchoRetryCount = AppSettings.Active.PACSEchoRetryCount;
            DicomExporter.ModelName = string.Format("{0} v{1}", AppSettings.Active.ApplicationProductName, AppGlobals.FileVersion);


            ReportViewer.IsRetainSentDicom = AppSettings.Active.PACSRetainSentDicom;
            ReportViewer.IsRetainSentImage = AppSettings.Active.PACSRetainSentImage;

            // Start the time display timer
            NewSplashScreen.SetText("100%");
            m_timeStr = DateTime.Now.ToString(FMT_DATE_TIME);
            m_oneSecondTimer.Tick += onOneSecondTimer_Tick;
            m_oneSecondTimer.Interval = 1000;
            m_oneSecondTimer.Start();
            m_lastTimerUtcTime = DateTime.UtcNow;

            EventListViewCtrl.Instance.UseRelativeTime = AppSettings.Active.IsUseEventsRelativeTime;

            Logger.SystemInfoLog("Application initialized.");

            IsInitialized = true;

            return true;
        }

		private void initBaseTime()
		{
			var initTime = INITIAL_WINDOW_TIME*Width/Constants.DIGI_LITE_SCREEN_WIDTH;

			if (initTime < INITIAL_WINDOW_TIME)
				initTime = INITIAL_WINDOW_TIME;

			var minDiff = double.MaxValue;

			var minIndex = -1;
			for (int i = 0; i < toolBarPanel1.ComboBoxTime.Items.Count; i++)
			{
				var obj = toolBarPanel1.ComboBoxTime.Items[i];
				if (obj == null)
					continue;

				double outVal;
				if (double.TryParse(obj.ToString(), out outVal))
				{
					if (outVal < initTime)
						continue;

					var diff = Math.Abs(initTime - outVal);
					if (diff <= minDiff)
					{
						minDiff = diff;
						minIndex = i;
					}
				}
			}

			if (minIndex < 0)
				minIndex = toolBarPanel1.ComboBoxTime.Items.Count - 1;



            toolBarPanel1.ComboBoxTime.SelectedIndex = minIndex;

            if (IsUnilateralStudy && minIndex > 0)// Channged by Alex CR #571 v.2.2.0.11 21/09/2014
                toolBarPanel1.ComboBoxTime.SelectedIndex = minIndex;
            else
                toolBarPanel1.ComboBoxTime.SelectedIndex = minIndex - 1;


			comboBoxTime_SelectedIndexChanged(toolBarPanel1.ComboBoxTime, null);
		}

		private static void setBaseTime(ComboBox cbox, double duration)
		{
			if (cbox == null)
				return;

			var minDiff = double.MaxValue;
			var minIdx = -1;

			for (var index = 0; index < cbox.Items.Count; index++)
			{
				var obj = cbox.Items[index];
				if (obj == null)
					continue;

				double outVal;
				if (double.TryParse(obj.ToString(), out outVal))
				{
					var diff = Math.Abs(duration - outVal);
					if (diff <= minDiff)
					{
						minDiff = diff;
						minIdx = index;
					}
				}
			}

			if (minIdx >= 0 && minIdx < cbox.Items.Count)
				cbox.SelectedIndex = minIdx;
			else
				cbox.SelectedIndex = cbox.Items.Count - 1;
		}

		private bool initDSPCommunication()
		{
			if (!DspManager.IsRunning)
				return false;

			if (m_smState != EGuiMgrSmState.GUI_HW_INIT_STATE)
				return true;

			
            //const string FMT_SPLASH_MSG = "Initializing DSP communication (timeout {0:D2} sec)...{1}";
            // Changed by Alex 27/04/2016 bug #854 v 2.2.3.31
            const string FMT_SPLASH_MSG = "Connecting ({0:D2} sec)";
            var dots = string.Empty;

			var timeoutSec = AppConfig.GetConfigValue("DSPManager:Initialization.Timeout", 60);
			var initTimeout = DateTime.UtcNow.AddSeconds(timeoutSec);
			var msg = string.Format(FMT_SPLASH_MSG, timeoutSec, dots);
			NewSplashScreen.SetText(msg);

			while (!DspManager.Instance.HwConnected)
			{
				if (initTimeout <= DateTime.UtcNow)
				{
					msg = string.Format("DSP communication handshake timeout ({0} sec.). Initialization fail.", timeoutSec);
					var ex = new ApplicationException(msg);
                    //Changed by Alex 27/04/2016 bug #854 v 2.2.3.31
                    //NewSplashScreen.SetText(msg, false);
                    NewSplashScreen.SetText("DSP communication timeout.", false);
					Logger.LogFatalError(ex);

					return false;
				}

				dots += ".";
				var sec = (int) initTimeout.Subtract(DateTime.UtcNow).TotalSeconds;
				msg = string.Format(FMT_SPLASH_MSG, sec, dots);
				NewSplashScreen.SetText(msg, false);

				Thread.Sleep(500);
			}

			m_smState = EGuiMgrSmState.GUI_OFFLINE_STATE;
			Logger.LogInfo("DSP Initiated. DSP version: v{0}, FPGA version: v{1}.", DspManager.Instance.DSPVersion, DspManager.Instance.FPGAVersion);

			return true;
		}

		private void retrieveDB()
		{
			Logger.LogTrace("retrieveDB(): START", Name);

			try
			{
				RimedDal.Instance.UpdateSecondDSFromDB();
				// TODO: Set layout member variables refers to the database.
				LayoutManager.Instance.UpdateMemberVar();
				LayoutReport.Instance.TemplateLayoutRep = (LayoutReport.ELayoutRep) RimedDal.Instance.GetLayoutRepTemplate();
				IsLayoutRepNoBg = RimedDal.Instance.IsLayoutRepBg();
				RimedDal.Instance.DeleteSetupStudyEvent += new Action(deleteStudyEvent);
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);
			}

			Logger.LogTrace("retrieveDB(): END", Name);
		}

		private void fireVolumeChanged(int volume, bool isMute)
		{
			if (VolumeChangedEvent != null)
			{
				using (new CursorSafeChange(this, true))
					VolumeChangedEvent(volume, isMute);
			}
		}

		private void barItemPatientSearch_Click(object sender, EventArgs e)
		{
			LoggerUserActions.MouseClick("barItemPatientSearch", Name);
			OpenPatientSearchDialog();
		}

		public void OpenPatientSearchDialog(Form owner = null)
		{
			if (IsClosing)
				return;

			if (owner == null)
				owner = this;

			PatientSearch.OpenDialog(owner);
			owner.Focus();
		}


		private void barItemStudies_Click(object sender, EventArgs e)
		{
			LoggerUserActions.MouseClick("barItemStudies", Name);
			var bi = sender as BarItem;
			if (bi == null)
				return;

			var id = (Guid) bi.Tag;

			loadSelectedStudy(id);
		}

		private bool loadSelectedStudy(Guid studyIndex, bool isShowMshBox = true)
		{
            //Return by Alex Bug #625 v.2.2.2.09 08/01/2015
            //LayoutManager.Instance.SetVisibilityClinikParams(false);

            // Added by Alex 04/04/2016 fixed bug #NN v. 2.2.3.28
            IsMonitoringStoped = false;

			var res = bvListViewCtrl1.LoadStudy(studyIndex, SelectedPatientIndex, isShowMshBox);
			if (res)
				validateLoadedStudy(studyIndex);

			var gv = LayoutManager.Instance.SelectedGv;
			if (gv != null && gv.Probe != null)
				AudioDSPtoPCStreamPlayer.PRF = gv.Probe.Range;

			//initBaseTime();

			return res;
		}

		#region Workaround for setting new intra-operative studies default depth

		//TODO, Ofer: Replace workaround
		private const int INDRAOPERATIVE_DEPTH_DEFAULT = 6; //6mm

		private void validateLoadedStudy(Guid studyIndex)
		{
			if (IsCurrentExamInteroperative())
			{
				bvListViewCtrl1.UpdateBVsDepth(INDRAOPERATIVE_DEPTH_DEFAULT);
				LayoutManager.Instance.DepthChanged(this, INDRAOPERATIVE_DEPTH_DEFAULT);
			}
		}

		public bool IsCurrentExamInteroperative()
		{
            return RimedDal.Intraoperative_STUDY_GUID.Equals(CurrentStudyId);
		}

		#endregion


		/// <summary>Reload the resource according to the desired language. No need to do anything if the language is the default one - English</summary>
		private void reloadControlLabels()
		{
			if (string.Compare(LayoutManager.Instance.LanguageResourceFileName, "English", StringComparison.InvariantCultureIgnoreCase) == 0)
				return;

			dockingManager1.SetDockLabel(panelHits, StringManager.GetString("Hits"));
			dockingManager1.SetDockLabel(panelEvents, StringManager.GetString("Events"));
			dockingManager1.SetDockLabel(panelBV, StringManager.GetString("Blood Vessel List"));
			dockingManager1.SetDockLabel(panelFFT, StringManager.GetString("FFT"));
			button3.Text = StringManager.GetString("Toggle");
			label13.Text = StringManager.GetString("HITS");
			parentBarItem1.Text = string.Format(FMT_MENU_UNDERLINE_NAME, StringManager.GetString("Debug"));
			parentBarItem1.Text = StringManager.GetString("Add Spectrum");
			barItemNewSpectrum.Text = StringManager.GetString("Add Spectrum");
			barItemNewTrend.Text = string.Format(FMT_MENU_UNDERLINE_NAME, StringManager.GetString("Trend"));
			barItemSaveStudy.Text = string.Format(FMT_MENU_UNDERLINE_NAME, StringManager.GetString("Save Study Layout"));
			barItemDeleteStudy.Text = string.Format(FMT_MENU_UNDERLINE_NAME, StringManager.GetString("Delete Study Layout"));
			barItemUnFreeze.Text = StringManager.GetString("UnFreeze");
			barItem1.Text = StringManager.GetString("ReadFFT");
			BarItem_UpZeroLine.Text = StringManager.GetString("Up Zero Line");
			barItem_DownZeroLine.Text = StringManager.GetString("Down Zero Line");
			SaveDebugData.Text = StringManager.GetString("Save Debug Data");
			barItemSaveDebugFFT.Text = StringManager.GetString("Save Debug FFT");
			parentBarItemPatient.Text = string.Format(FMT_MENU_UNDERLINE_NAME, StringManager.GetString("Patient"));
			barItemPatientNew.Text = string.Format(FMT_MENU_UNDERLINE_NAME, StringManager.GetString("New..."));
			barItemPatientLoad.Text = string.Format(FMT_MENU_UNDERLINE_NAME, StringManager.GetString("Load..."));
            barItemPatientWorkList.Text = string.Format(FMT_MENU_UNDERLINE_NAME, StringManager.GetString("Work List..."));
            barItemPatientSearch.Text = string.Format(FMT_MENU_UNDERLINE_NAME, StringManager.GetString("Search..."));
			barItemPatientDelete.Text = string.Format(FMT_MENU_UNDERLINE_NAME, StringManager.GetString("Delete..."));
			barItemPatientExport.Text = StringManager.GetString("Export");
			barItemExportSelectedSpectrum.Text = StringManager.GetString("Selected Spectrum");
			barItemExportFullScreen.Text = StringManager.GetString("Full Screen");
			barItemBackup.Text = StringManager.GetString("Backup...");
			barItemRestore.Text = string.Format(FMT_MENU_UNDERLINE_NAME, StringManager.GetString("Restore..."));
			barItemPatientPrintPreview.Text = StringManager.GetString("Print Preview...");
			barItemPrint.Text = string.Format(FMT_MENU_UNDERLINE_NAME, StringManager.GetString("Print..."));
			barItemExit.Text = StringManager.GetString("Main Menu");
			barItemExitWindows.Text = StringManager.GetString("Exit windows");
			barItemAdminExitWindows.Text = StringManager.GetString("Exit to Windows Desktop");
			parentBarItemStudies.Text = string.Format(FMT_MENU_UNDERLINE_NAME, StringManager.GetString("Studies"));
			parentBarItemSetup.Text = StringManager.GetString("Setup");
			barItemPatientReportWizard.Text = StringManager.GetString("Report Generator Wizard");
			barItemSetupGeneral.Text = string.Format(FMT_MENU_UNDERLINE_NAME, StringManager.GetString("General..."));
			barItemSetupStudies.Text = string.Format(FMT_MENU_UNDERLINE_NAME, StringManager.GetString("Studies..."));
			parentBarItemFunctions.Text = string.Format(FMT_MENU_UNDERLINE_NAME, StringManager.GetString("Functions"));

            barItemAutoscan.Text = StringManager.GetString("M-Mode");
			
            barItemCursors.Text = StringManager.GetString("Cursors");
			barItemNotes.Text = StringManager.GetString("Notes...");
			barItemPatientReport.Text = StringManager.GetString("Patient Report...");
			barItemHitsDetection.Text = StringManager.GetString("Hits Detection");
			barItemAddEvent.Text = StringManager.GetString("Add Event");
			barItemDeleteEvent.Text = StringManager.GetString("Delete Event");
			barItemNextBV.Text = StringManager.GetString("Next BV");
			barItemPrevBV.Text = StringManager.GetString("Previous BV");
			barItemSummaryScreen.Text = StringManager.GetString("Summary Screen");
            barItemAccessionNumber.Text = string.Format(FMT_MENU_UNDERLINE_NAME, StringManager.GetString("Accession Number"));
			barItemRecord.Text = StringManager.GetString("Record");
			barItemStop.Text = StringManager.GetString("Stop");
			barItemPlay.Text = StringManager.GetString("Play");
			barItemExpandClinicalParameters.Text = StringManager.GetString("Expand Clinical Parameters");
			parentBarItemUtilities.Text = string.Format(FMT_MENU_UNDERLINE_NAME, StringManager.GetString("Utilities"));
			barItemAuthorizationMgr.Text = StringManager.GetString("Authorization Manager...");
			barItemDebug.Text = StringManager.GetString("DSP Debug tools");
			barItemZipRawDataFiles.Text = StringManager.GetString("Zip Raw-Data Files");
			parentBarItemHelp.Text = string.Format(FMT_MENU_UNDERLINE_NAME, StringManager.GetString("Help"));
			barItemHelpContent.Text = string.Format(FMT_MENU_UNDERLINE_NAME, StringManager.GetString("Content..."));
			barItemHelpIndex.Text = string.Format(FMT_MENU_UNDERLINE_NAME, StringManager.GetString("Index..."));
			barItemHelpSearch.Text = string.Format(FMT_MENU_UNDERLINE_NAME, StringManager.GetString("Search..."));
			barItemHelpWhatIsThis.Text = StringManager.GetString("What is this?");
			barItemHelpAbout.Text = string.Format(FMT_MENU_UNDERLINE_NAME, StringManager.GetString("About..."));
			parentBarItem2.Text = StringManager.GetString("KBShortCuts");
			barItemDepthDown.Text = StringManager.GetString("Depth Down");
			barItemDepthUp.Text = StringManager.GetString("Depth Up");
			barItemGainUp.Text = StringManager.GetString("Gain Up");
			barItemGainDown.Text = StringManager.GetString("Gain Down");
			barItemFreqDown.Text = StringManager.GetString("Range Down");
			barItemFreqUp.Text = StringManager.GetString("Range Up");
			barItemPowerDown.Text = StringManager.GetString("Power Down");
			barItemPowerUp.Text = StringManager.GetString("Power Up");
			barItemWidthDown.Text = StringManager.GetString("Width Down");
			barItemWidthUp.Text = StringManager.GetString("Width Up");
			barItemThumpDown.Text = StringManager.GetString("Thump Down");
			barItemThumpUp.Text = StringManager.GetString("Thump Up");
			barItemPanelHits.Text = StringManager.GetString("Panel Hits");
			barItem2.Text = StringManager.GetString("UnFreeze");
			barItemSave.Text = StringManager.GetString("Save...");
			Name = StringManager.GetString("MainForm");
			Text = "DigiLite™ - " + StringManager.GetString("Undefined [Intracranial Unilateral]");

			barItemExportTrends.Text = StringManager.GetString("Export trends to Excel");
			barItemExportLog.Text = StringManager.GetString("Export Log File");
			barItemCancelUtilitiesMenu.Text = StringManager.GetString("Cancel utilities menu");
		}

		private void setStudyType()
		{
			if (CurrentStudyName.Contains("Bilateral"))
				StudyType = GlobalTypes.EStudyType.Bilateral;
			else if (CurrentStudyName.StartsWith("Multifrequency"))
				StudyType = GlobalTypes.EStudyType.Multifrequency;
			else
				StudyType = GlobalTypes.EStudyType.Unilateral;
		}

		private void setWorkingStudyType(string study)
		{
			m_studyString = study;
			setStudyType();

			ChangeStudy = (m_oldStudyType != StudyType);
			m_oldStudyType = StudyType;
		}

		public DateTime MonitoringStartTime { get; private set; }

		internal void LoadStudy(dsStudy.tb_StudyRow dvStudy, Guid patientIndex, bool examIsTemporary)
		{
            toolBarPanel1.ButtonWMV.Visible = false;

			AtTheEndOfMonitoringExam = false;
			CurrentStudyId = dvStudy.Study_Index;
			SelectedPatientIndex = patientIndex;
			MonitoringStartTime = DateTime.MinValue;

			string studyStr;
			// Functions Optimization - Add the next lines from SelectStudyId Property.
			if (dvStudy.DefaultStudy)
			{
				studyStr = dvStudy.Name;
				CurrentStudyName = dvStudy.Name;
			}
			else
			{
				studyStr = RimedDal.Instance.GetStudy(dvStudy.BasedOn).Name;
				CurrentStudyName = studyStr + " - " + dvStudy.Name;
			}

			Logger.LogInfo("'{0}' Study loaded.", CurrentStudyName);

			setWorkingStudyType(studyStr);

			LayoutManager.Instance.IsPaused = false;
			bool isMonitoring = dvStudy.Monitoring;

			setDocking(isMonitoring);
			if (!isMonitoring || CurrentStudyName.StartsWith(Constants.StudyType.STUDY_MONITORING_INTRACRANIAL))
			{
				BarItemSaveStudyEnabled = true;
				BarItemDeleteStudyEnabled = true;
			}
			else
			{
				BarItemSaveStudyEnabled = false;
				BarItemDeleteStudyEnabled = false;
			}

			// OFER: Enable diagnostic post save edit
			if (examIsTemporary || LayoutManager.IsPostSaveEditEnabled)
				guiMgrSmGo(new CGuiSmEvent(isMonitoring ? EGuiMgrSmEvent.GUI_NEW_STUDY_MONITORING_EVENT : EGuiMgrSmEvent.GUI_NEW_STUDY_DIAGNOSTIC_EVENT));

			// OFER: Enable diagnostic post save edit
			m_displayWarningReportFirstTime = examIsTemporary;
			m_recordPaused = false;
			DspRecordMode = false;

			var x = toolBarPanel1.Handle; //Ofer, v1.18.2.12 - Workaround: Invoke or BeginInvoke cannot be called on a control until the window handle has been created. (ErrorCode: 0x80131509)
			toolBarPanel1.BeginInvoke((Action) delegate()
				{
					// OFER: Enable diagnostic post save edit
					toolBarPanel1.ComboBoxSensitivity.Visible = examIsTemporary;
					toolBarPanel1.ComboBoxSensitivity.Enabled = examIsTemporary;
					if (examIsTemporary)
					{
						toolBarPanel1.ComboBoxSensitivity.SelectedIndex = 1;
						if (IsBilateralStudy)
						{
							toolBarPanel1.ComboBoxSensitivitySecondary.Visible = true;
							toolBarPanel1.ComboBoxSensitivitySecondary.Enabled = true;
							toolBarPanel1.ComboBoxSensitivitySecondary.SelectedIndex = 1;
						}
						else
							toolBarPanel1.ComboBoxSensitivitySecondary.Visible = false;
					}
					else
						toolBarPanel1.ComboBoxSensitivitySecondary.Visible = false;

					toolBarPanel1.ButtonPlayCtrl.Enabled = !examIsTemporary;
					SetHitsControls(!examIsTemporary);
					if (isMonitoring)
					{
						var examRow = RimedDal.Instance.LoadedExamination();
						MonitoringReplayTimeCounter = (examRow == null) ? DateTime.Now : examRow.Date; //= (DateTime)RimedDal.Instance.ExaminationsDS.tb_Examination.Rows[0].ItemArray[5];
						IsMonitoringSaved = false;
						MonitoringStartTime = MonitoringReplayTimeCounter;

						toolBarPanel1.ButtonPause.IsPressed = false;
						toolBarPanel1.TimeLabel.Visible = true;
					}
					else
					{
						toolBarPanel1.TimeLabel.Visible = false;
					}

					//Don't show the sensitivity combo boxes for rev3 or if the Prob is not 2M
					if (LayoutManager.Instance.CurrentGateView != null && LayoutManager.Instance.CurrentGateView.Probe != null && LayoutManager.Instance.CurrentGateView.Probe.Name != "2Mhz PW")
					{
						toolBarPanel1.ComboBoxSensitivity.Visible = false;
						toolBarPanel1.ComboBoxSensitivitySecondary.Visible = false;
					}

					// Hide the Manual Calculation button (MC) in case of monitoring study
                    //Changed by Alex Feature #572 (item 6) v.2.2.1.03 26/10/2014
                    //LayoutManager.Instance.PicManualCalculationsVisible = !isMonitoring;


                    //Added by Alex Bug #597 v.2.2.2.02 11/11/2014
                    LayoutManager.Instance.PicManualCalculationsVisible = (!CurrentStudyName.StartsWith(Constants.StudyType.STUDY_EVOKED_FLOW_BILATERAL) && !CurrentStudyName.StartsWith(Constants.StudyType.STUDY_EVOKED_FLOW_UNILATERAL));
                    //Changed by Alex Feature #572 v.2.2.1.05 28/10/2014
                    LayoutManager.Instance.PicManualCalculationsEnabled = true; // examIsTemporary;
                    
                });

			if (examIsTemporary)
			{
				var memStream = new MemoryStream(dvStudy.SerializedData);
				LayoutManager.Instance.Deserialize(memStream);
			}

			initBaseTime();
		}

		private delegate void SetDockingDelegate(bool isMonitoring);

		private void setDocking(bool isMonitoring)
		{
			if (InvokeRequired)
			{
				BeginInvoke((SetDockingDelegate) setDocking, isMonitoring);
				return;
			}

			if (isMonitoring)
			{
				dockingManager1.SetDockVisibility(panelEvents, true);
				dockingManager1.DockControl(panelEvents, panelHits, DockingStyle.Right, 160);
				dockingManager1.DockControl(panelHits, panelEvents, DockingStyle.Tabbed, 160);

                dockingManager1.SetAutoHideButtonVisibility(panelEvents, false); // Channged by Alex CR #577 v.2.2.0.12 22/09/2014  must be false
                dockingManager1.SetAutoHideButtonVisibility(panelHits, false);
				dockingManager1.SetDockVisibility(panelBV, false);
				dockingManager1.SetDockVisibility(panelFFT, false);

				dockingManager1.ActivateControl(panelEvents);
			}
			else
			{
				dockingManager1.SetDockVisibility(panelBV, true);
				dockingManager1.DockControl(panelHits, panelBV, DockingStyle.Right, 160);
				dockingManager1.DockControl(panelBV, panelHits, DockingStyle.Tabbed, 160);

                dockingManager1.SetAutoHideButtonVisibility(panelBV, true);// Channged by Alex CR #569 v.2.2.0.11 21/09/2014 must be true
                dockingManager1.SetAutoHideButtonVisibility(panelHits, true);
				dockingManager1.SetDockVisibility(panelEvents, false);
				dockingManager1.SetDockVisibility(panelFFT, false);

				dockingManager1.ActivateControl(panelBV);
			}
		}

		private void updateStudiesMenus()
		{
			// Dynamically build Studies menu
			updateStudiesMenu(mainFrameBarManager1, parentBarItemStudies);
			updateStudiesMenu(SummaryScreen.FrameBarManager, SummaryScreen.ParentBarStudies);
		}

		private void updateStudiesMenu(MainFrameBarManager mainFrameBarManager, ParentBarItem parentBarItem)
		{
			if (parentBarItem == null || mainFrameBarManager == null)
				return;

			parentBarItem.Items.Clear();


			if (AppSettings.Active.IsReviewStation)
			{
				parentBarItem.Enabled = false;
				barItemSetupStudies.Enabled = false;		//Alex, v2.2.0.1 Bug# 525

				return;
			}

			//Get all diagnostics studies from DB.
			//Assaf : For now I use the "SpecialInfo" field for sorting the rows and making the monitoring unilateral and monitoring bilateral to be the
			// last one in the list. I didn't want to change the DB structure and by now there was no use for this field.
			var s = RimedDal.Instance.GetStudies("SELECT tb_Study.*,DefaultStudy AS Expr1, Monitoring AS Expr2 from tb_Study WHERE ((DefaultStudy=true) ) order by SpecialInfo");
			if (s.tb_Study.Rows.Count == 0)
				return;

			foreach (dsStudy.tb_StudyRow row in s.tb_Study.Rows)
			{
				if (row.AuthorizationMgr)
				{
					var bi = new ParentBarItem(row.Name);
					bi.Tag = row.Study_Index;

					//Added by Natalie: rename VMR to VMR Unilateral - 10.31.2008
					if (string.Compare(Constants.StudyType.STUDY_VMR, bi.Text, StringComparison.InvariantCultureIgnoreCase) == 0)
						bi.Text = Constants.StudyType.STUDY_VMR_UNIILATERAL;

					mainFrameBarManager.Items.Add(bi);

					parentBarItem.Items.Add(bi);
				}
			}

			foreach (ParentBarItem ib in parentBarItem.Items)
			{
				// Add a default entry
				var Default = new BarItem("Default");
				Default.Tag = ib.Tag;
				Default.Click += barItemStudies_Click;
				mainFrameBarManager.Items.Add(Default);
				ib.Items.Add(Default);

				// Add custom entries for the study
				s = RimedDal.Instance.GetStudies("SELECT tb_Study.*,BasedOn AS Expr1 from tb_Study WHERE (BasedOn= {guid {" + ib.Tag + "}}) Order by SpecialInfo");

				var firstTime = true;
				foreach (dsStudy.tb_StudyRow row in s.tb_Study.Rows)
				{
					if (!row.AuthorizationMgr)
						continue;

					var custom = new BarItem(row.Name);
					custom.Tag = row.Study_Index;
					custom.Click += barItemStudies_Click;

					mainFrameBarManager.Items.Add(custom);
					ib.Items.Add(custom);

					// Add separator after default if custom exists
					if (firstTime)
					{
						ib.SeparatorIndices.Add(1);
						firstTime = false;
					}
				}
			}
		}

		private void barItemSaveStudy_Click(object sender, EventArgs e)
		{
			LoggerUserActions.MouseClick("barItemSaveStudy", Name);

			string studyLayoutName;
			using (var dlg = new SaveStudy(m_studyString))
			{
				dlg.TopMost = true;
				var res = dlg.ShowDialog(this);
				Focus();
				if (res != DialogResult.OK)
					return;

				studyLayoutName = dlg.StudyLayoutName;
			}

			try
			{
				byte[] myData;
				using (Stream ms = new MemoryStream())
				{
					LayoutManager.Instance.Serialize(ms);
					myData = new byte[ms.Length];
					ms.Seek(0, SeekOrigin.Begin);
					ms.Read(myData, 0, Convert.ToInt32(ms.Length));
					ms.Close();
				}

				var defaultStudy = RimedDal.Instance.GetDefaultStudyGuid(m_studyString);
				var dsS = RimedDal.Instance.GetStudyByGuid(defaultStudy);

				//if the name is not default then add a new study, otherwise  update the default study parameters
				if (string.Compare("default", studyLayoutName, StringComparison.InvariantCultureIgnoreCase) != 0)
				{
					//add a new study to the studies table
					var newStudyGuid = Guid.NewGuid();
					dsS.tb_Study.Addtb_StudyRow(newStudyGuid, studyLayoutName, false, defaultStudy, dsS.tb_Study[0].NumOfProbes, dsS.tb_Study[0].Monitoring, myData, true, "Unilateral");

					//add BV's to the new study according to the default study it is based on
					var defaultStudyBV = RimedDal.Instance.GetBVByStudyIndex(defaultStudy);
					foreach (DataRowView row in defaultStudyBV)
					{
						RimedDal.Instance.StudyBVsDS.tb_Study_BV.Addtb_Study_BVRow(newStudyGuid, (Guid) row["Blood_Vessel_Index"], (int) row["IndexInStudy"], true);
					}

					//add clinical parameters order appearance
					var clinicalParamOrder = RimedDal.Instance.GetClinicalParamByStudyIndex(defaultStudy);
					foreach (DataRowView row in clinicalParamOrder)
					{
						RimedDal.Instance.StudyClinicalParamsDS.tb_Study_ClinicalParam.Addtb_Study_ClinicalParamRow((Guid) row["ClinicalParam_Index"], newStudyGuid, (int) row["IndexInStudy"], true);
					}

					if (dsS.tb_Study[0].Monitoring)
					{
						var channelOrder = RimedDal.Instance.GetChannelsByStudyIndex(defaultStudy);
						foreach (DataRowView row in channelOrder)
						{
							RimedDal.Instance.AddChannel2Study(newStudyGuid, (Guid) row["Channel_Index"], (int) row["IndexInStudy"]);
						}

						var eventOrder = RimedDal.Instance.GetEventsBySetupStudyIndex(defaultStudy);
						foreach (DataRowView row in eventOrder)
						{
							RimedDal.Instance.AddPredefienedEvent2Study(newStudyGuid, Guid.NewGuid(), Convert.ToInt16(row["IndexInStudy"].ToString()), (String) row["Name"]);
						}
					}
					CurrentStudyId = newStudyGuid;
					CurrentStudyName = dsS.tb_Study[0].Name + " - " + studyLayoutName;
				}
				else
				{
					dsS.tb_Study[0].SerializedData = myData;
					CurrentStudyId = dsS.tb_Study[0].Study_Index;
					CurrentStudyName = dsS.tb_Study[0].Name;
				}
				RimedDal.Instance.StudysDS.Merge(dsS);
				RimedDal.Instance.UpdateDBWithDS();

				// Add the layout to the menu
				parentBarItemStudies.Items.Clear();
				updateStudiesMenus();
				setCaptionText();
			}
			catch (Exception ex)
			{
				var sb = new StringBuilder();
				sb.Append(StringManager.GetString("Study layout saving Failed.")).Append(Constants.NEW_LINE).Append(Constants.NEW_LINE);
				sb.Append(StringManager.GetString("More Details:")).Append(Constants.NEW_LINE);
				sb.Append(ex.Message);

				Logger.LogError(ex);
				LoggedDialog.ShowError(this, sb.ToString());
			}
		}

		public void SetSpectrumSidePanelsWidth()
		{
			var chartCount = (LayoutManager.Instance.GateViews == null) ? 0 : LayoutManager.Instance.GateViews.Count;
			var width = dockingClientPanel.Width/3;
			if (chartCount > 4)
				width = dockingClientPanel.Width/4;

			dockingClientPanel.SuspendLayout();

			LeftPanel.Width = width;
			RightPanel.Width = width;

			dockingClientPanel.ResumeLayout();

			AudioDSPtoPCStreamPlayer.Reset();
		}

		/// <summary>Whenever the header size is changed we need to set the visibility of the text labels</summary>
		public void UpdateSmallChartsHeader(object sender, Panel headerPanel)
		{
			if ((headerPanel == null) || (!headerPanel.Visible))
				return;

			// update the left and the right panels
			var db = sender as DopplerBar;
			if (db == null)
				return;

			int headerIndex = 0;
			foreach (Control c in db.Controls)
			{
				var dvl = c as DopplerVarLabel;
				if (dvl == null)
					continue;

				if (dvl.Visible)
				{
					Label l;

					// Skip the combo
					while ((l = headerPanel.Controls[headerIndex] as Label) == null)
						headerIndex++;

					l.Visible = true;
					l.Text = dvl.VarText;
					l.Location = dvl.Location;
					l.Left += 3;
					l.Size = dvl.Size;
					l.Font = GlobalSettings.FONT_10_ARIAL_BOLD;
					l.ForeColor = GlobalSettings.P6;
					l.TextAlign = ContentAlignment.MiddleLeft;
					headerIndex++;
				}
			}

			for (int i = headerIndex; i < headerPanel.Controls.Count; i++)
				headerPanel.Controls[i].Visible = false;
		}

		private void barItemExit_Click(object sender, EventArgs e)
		{
			LoggerUserActions.MouseClick("barItemExit", Name);
			if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Online)
				return;

			//TODO: delete additional information after hardware problem fixing
			ExitDigiLite(true);
		}

		private void barItemAdminExitWindows_Click(object sender, EventArgs e)
		{
			LoggerUserActions.MouseClick("barItemAdminExitWindows", Name);
			if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Online)
				return;

			if (LoggedDialog.Show(this, StringManager.GetString("Are you sure you want to quit?"), buttons: MessageBoxButtons.OKCancel) != DialogResult.OK)
				return;

			ProcessComm.MainMenuApplicationExit();

			ExitDigiLite(isExitToWindows: true);
		}

		private void barItemSetupGeneral_Click(object sender, EventArgs e)
		{
			LoggerUserActions.MouseClick("barItemSetupGeneral", Name);
			OpenSetupDialog();
		}

		public void OpenSetupDialog(Form owner = null)
		{
			if (IsClosing)
				return;

			if (owner == null)
				owner = this;

			using (var dlg = new SetupForm(true))
			{
				dlg.TopMost = true;
				dlg.ShowDialog(owner);
				owner.Focus();
			}

			// Saving setup info is taken care of in the Setup.buttonOK_Click
			loadSelectedStudy(CurrentStudyId);
		}

		private void barItemUnFreeze_Click(object sender, EventArgs e)
		{
			LoggerUserActions.MouseClick("barItemUnFreeze", Name);
			toggleFreezeMode();
			guiMgrSmGo(new CGuiSmEvent(EGuiMgrSmEvent.GUI_UNFREEZE_EVENT));
			Focus();
            // Added by Alex bug #588 v.2.2.2.1 05/10/2014
            isFirstScreen = true;
		}

		private void barItem1_Click(object sender, EventArgs e)
		{
			LoggerUserActions.MouseClick("barItem1", Name);
			SpectrumGraph.Init();
		}

		/// <summary>Commits the function that register for the "Next function" button</summary>
		private void applyNextFunction()
		{
			Logger.LogTrace("ApplyNextFunction()", Name);

			if (InvokeRequired)
			{
				Invoke(new Action(applyNextFunction));
				return;
			}

			var dv = RimedDal.Instance.DvFunctionOrder();
			foreach (DataRowView row in dv)
			{
				int index = Convert.ToInt32(row["NextFunctionIndex"].ToString(), 10);

				//Check type of return value ...
				var func = m_nextFunctionList[index] as Action;

				//Unfreeze call
				if (func != null)
				{
					Logger.LogTrace(Logger.ETraceLevel.L2, Name, "ApplyNextFunction().{0}", func.Method.Name);
					func();
				}
				else
				{
					var nextbv = m_nextFunctionList[index] as NextBVDelegate;

					//NextBV call
					if (nextbv != null)
					{
						Logger.LogTrace(Logger.ETraceLevel.L2, Name, "ApplyNextFunction().{0}", nextbv.Method.Name);

						//if current vessel is last, don't call unfreeze
						if (!nextbv())
							break;
					}
				}
			}
		}

		private void barItem_DownZeroLine_Click(object sender, EventArgs e)
		{
			LoggerUserActions.MouseClick("barItem_DownZeroLine", Name);
			zeroLineDown();
		}

		private void BarItem_UpZeroLine_Click(object sender, EventArgs e)
		{
			LoggerUserActions.MouseClick("BarItem_UpZeroLine", Name);
			zeroLineUp();
		}

		private ComboBox getTimeComboboxForPanel(Panel panel)
		{
			if (panel == panelMainCharts || panel == panel0Charts || panel == panel1Charts)
				return toolBarPanel1.ComboBoxTime;

			if (panel == panelRightCharts)
				return comboBoxTimeRight;

			if (panel == panelLeftCharts)
				return comboBoxTimeLeft;

			return null;
		}

		public int GetSpeedFactorForPanel(Panel panel)
		{
			var cBox = getTimeComboboxForPanel(panel);
			if (cBox != null)
				return GetSpeedFactor(cBox.SelectedIndex);

			return -1;
		}

		// TODO: $$$ add return function of the probe combobox.
		private void comboBoxTime_SelectionChangeCommitted(object sender, EventArgs e)
		{
			var cbTime = sender as ComboBox;
			if (cbTime == null)
				return;

			LoggerUserActions.ComboBoxSelectedValueChanged(cbTime.Name, Name, cbTime.Text);
		}

		private double m_gateTimeInterval;
		private readonly object m_timeIntervalPropertyLock = new object();

		private double GateTimeInterval
		{
			set
			{
				lock (m_timeIntervalPropertyLock)
				{
					m_gateTimeInterval = value;
				}
			}
			get
			{
				lock (m_timeIntervalPropertyLock)
				{
					return m_gateTimeInterval;
				}
			}
		}

		private void comboBoxTime_SelectedIndexChanged(object sender, EventArgs e)
		{
			var cbTime = sender as ComboBox;
			if (cbTime == null)
				return;

			GateTimeInterval = Convert.ToDouble(cbTime.Text);

			// TODO : BILATERAL - need to change the time-display of the panelMainChartsLeft in case that releted
			// panel is panelMainCharts.
			var index = cbTime.SelectedIndex;
            var speedFactor = GetSpeedFactor(index);
			var relatedPanel = new Panel[1];
			relatedPanel[0] = null; // The panel that is related (affected) by the combobox
			if (cbTime == toolBarPanel1.ComboBoxTime)
			{
				if (IsUnilateralStudy)
				{
					relatedPanel[0] = panelMainCharts;
					foreach (var obj in PanelAutoscan[0].Controls)
					{
						var chart = obj as AutoScanChart;
						if (chart != null)
							chart.SpeedFactor = speedFactor;
					}
				}
				else
				{
					relatedPanel = new Panel[2];
					for (var i = 0; i < GlobalSettings.VIEWS_IN_BILATERAL; ++i)
					{
						relatedPanel[i] = (i == 0) ? panel0Charts : panel1Charts;
						foreach (var obj in PanelAutoscan[i].Controls)
						{
							var chart = obj as AutoScanChart;
							if (chart != null)
								chart.SpeedFactor = speedFactor;
						}
					}
				}
			}
			else if (cbTime == comboBoxTimeRight)
				relatedPanel[0] = panelRightCharts;
			else if (cbTime == comboBoxTimeLeft)
				relatedPanel[0] = panelLeftCharts;

			foreach (var p in relatedPanel)
			{
				foreach (GateView gv in p.Controls)
				{
					if (gv != null)
						gv.SpeedFactor = speedFactor;
				}
			}

			if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Monitoring)
				AudioDSPtoPCStreamPlayer.Reset();
		}

		public void VolumeUp()
		{
			if (InvokeRequired)
			{
				Invoke(new Action(VolumeUp));
				return;
			}

			Logger.LogTrace("VolumeUp()", Name);
			SystemVolume++;
		}

		public void VolumeDown()
		{
			if (InvokeRequired)
			{
				Invoke(new Action(VolumeDown));
				return;
			}

			Logger.LogTrace("VolumeDown()", Name);
			SystemVolume--;
		}

		public void DepthUp()
		{
			Logger.LogTrace("DepthUp()", Name);
			dopplerChanged(GlobalTypes.EDopplerVar.Depth, true);
		}

		private void barItemDepthUp_Click(object sender, EventArgs e)
		{
			LoggerUserActions.MouseClick("barItemDepthUp", Name);
			DepthUp();
		}

		public void DepthDown()
		{
			Logger.LogTrace("DepthDown()", Name);
			dopplerChanged(GlobalTypes.EDopplerVar.Depth, false);
		}

		private void barItemDepthDown_Click(object sender, EventArgs e)
		{
			LoggerUserActions.MouseClick("barItemDepthDown", Name);
			DepthDown();
		}

		public void GainDown()
		{
			Logger.LogTrace("GainDown()", Name);

			dopplerChanged(GlobalTypes.EDopplerVar.Gain, false);
		}

		public void GainUp()
		{
			Logger.LogTrace("GainUp()", Name);

			dopplerChanged(GlobalTypes.EDopplerVar.Gain, true);
		}

		public void ResetCurrentGraphs()
		{
			if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Offline) //BugFix #224: Intracranial unilateral - Load from database - Spectrum vanishes while in replay
				return;

			var speedFactor = GetSpeedFactorForPanel(MainChartsPanel);
			if (speedFactor <= 0)
				return;

			var controls = MainChartsPanel.Controls;
			foreach (Control c in controls)
			{
				var gv = c as GateView;
				if (gv != null)
					gv.Spectrum.SpeedFactor = speedFactor;
			}

			for (var i = 0; i < PanelAutoscan.Count; ++i)
			{
				var panel = PanelAutoscan[i];
				if (panel == null)
					continue;

				foreach (AutoScanChart chart in panel.Controls)
				{
					if (chart != null)
						chart.SpeedFactor = speedFactor;
				}
			}
		}

		private void resetGraphs()
		{
			ResetCurrentGraphs();

			var speedFactor = GetSpeedFactorForPanel(RightChartsPanel);
			if (speedFactor <= 0)
				return;

			var controls = RightChartsPanel.Controls;
			foreach (Control c in controls)
			{
				var gv = c as GateView;
				if (gv != null)
					gv.Spectrum.SpeedFactor = speedFactor;
			}

			speedFactor = GetSpeedFactorForPanel(LeftChartsPanel);
			if (speedFactor <= 0)
				return;

			controls = LeftChartsPanel.Controls;
			foreach (Control c in controls)
			{
				var gv = c as GateView;
				if (gv != null)
					gv.Spectrum.SpeedFactor = speedFactor;
			}
		}


		private void barItemGainDown_Click(object sender, EventArgs e)
		{
			LoggerUserActions.MouseClick("barItemGainDown", Name);
			GainDown();
		}

		private void barItemGainUp_Click(object sender, EventArgs e)
		{
			LoggerUserActions.MouseClick("barItemGainUp", Name);
			GainUp();
		}

		public void SensitivityChanged(int probIndexForUpdateSensitivity)
		{
			m_probIndexForUpdateSensitivity = probIndexForUpdateSensitivity;
			dopplerChanged(GlobalTypes.EDopplerVar.Sensitivity, false);
		}

		private delegate void DopplerChangedDelegate(GlobalTypes.EDopplerVar dopplerVar, bool up);

		private void dopplerChanged(GlobalTypes.EDopplerVar dopplerVar, bool up)
		{
			if (LayoutManager.Instance.ReplayMode || LayoutManager.Instance.IsBusy)
				return;

			if (InvokeRequired)
			{
				Invoke((DopplerChangedDelegate) dopplerChanged, dopplerVar, up);
				return;
			}

			var gv = LayoutManager.Instance.SelectedGv;
			if (gv == null)
			{
				Logger.LogError(string.Format("{0}.DopplerChanged(dopplerVar={1}, up={2}): LayoutManager.Instance.SelectedGv is NULL.", Name, dopplerVar, up));
				return;
			}

			var curGv = LayoutManager.Instance.CurrentGateView;
			var depthVar = curGv != null ? curGv.DopplerBar.DepthVar : gv.DopplerBar.DepthVar;

			var p = gv.Probe;
			Probe p2 = null;
			var iVal = 0;
			double dVal = 0;

			//Check between old and new values was added for RIMD-350
			int oldIVal;

			switch (dopplerVar)
			{
				case GlobalTypes.EDopplerVar.Depth:
					if (p.IsCW)
						return;

					dVal = up ? p.DepthUp(depthVar) : p.DepthDown(depthVar);
					LayoutManager.Instance.DepthChanged(this, dVal);
					break;

				case GlobalTypes.EDopplerVar.Gain:
					oldIVal = p.Gain;
					iVal = up ? p.GainUp() : p.GainDown();
					p.WriteGain2Dsp(iVal);

					if (!AppSettings.Active.IsDspOffMode && LayoutManager.SystemMode != GlobalTypes.ESystemModes.Offline && (oldIVal != iVal))
						LayoutManager.Instance.UpdateDopplerFlag();
					break;

				case GlobalTypes.EDopplerVar.Sensitivity:
					var sensitivity = (m_probIndexForUpdateSensitivity == 0) ? toolBarPanel1.ComboBoxSensitivity.SelectedIndex : toolBarPanel1.ComboBoxSensitivitySecondary.SelectedIndex;
					ProbesCollection.Instance.SetProbeSensitivity(m_probIndexForUpdateSensitivity, sensitivity);
					break;

				case GlobalTypes.EDopplerVar.Range:
                        oldIVal = p.Range;
                        if (IsUnilateralStudy)
                        {
                            iVal = up ? p.RangeUp() : p.RangeDown();
                            if (iVal <= 12) // added by Alex bug #583 v.2.2.1.02 21/10/2014 
                            {
                                // Changed by Alex bug #680 v.2.2.2.14 12/02/2015 
                                //if (p.DepthMax <= p.ProbeDepth) 
                                    //LayoutManager.Instance.DepthChanged(this, p.DepthDown(p.DepthMax));
                                //else if (p.DepthMin >= p.ProbeDepth)
                                    //LayoutManager.Instance.DepthChanged(this, p.DepthUp(p.DepthMin));
                                
                                if (p.DepthMax <= p.ProbeDepth / Probe.DSPDepthConvertor[p.DSPCode])
                                    LayoutManager.Instance.DepthChanged(this, p.DepthDown(p.DepthMax));
                                else if (p.DepthMin >= p.ProbeDepth / Probe.DSPDepthConvertor[p.DSPCode])
                                    LayoutManager.Instance.DepthChanged(this, p.DepthUp(p.DepthMin));

                                p.WriteRange2Dsp(iVal);

                                if (!AppSettings.Active.IsDspOffMode && LayoutManager.SystemMode != GlobalTypes.ESystemModes.Offline && (oldIVal != iVal))
                                    LayoutManager.Instance.UpdateDopplerFlag();
                                p.GetNumOfGates(iVal, true);
                            }
                            else
                            {
                                iVal = oldIVal;
                                return;
                            }
                        }
                        else
                        {
                            var gv1 = LayoutManager.Instance.GetGate(0);
                            var gv2 = LayoutManager.Instance.GetGate(1);

                            p = gv1.Probe;
                            p2 = gv2.Probe;

                            if (up)
                            {
                                iVal = p.RangeUp();
                                iVal = p2.RangeUp();
                            }
                            else
                            {
                                iVal = p.RangeDown();
                                iVal = p2.RangeDown();
                            }

                            if (iVal <= 12) // added by Alex bug #583 v.2.2.1.02 22/10/2014 
                            {
                                p.WriteRange2Dsp(iVal);
                                p2.WriteRange2Dsp(iVal);

                                if (!AppSettings.Active.IsDspOffMode && LayoutManager.SystemMode != GlobalTypes.ESystemModes.Offline && (oldIVal != iVal))
                                    LayoutManager.Instance.UpdateDopplerFlag();

                                p.GetNumOfGates(iVal, true);
                                p2.GetNumOfGates(iVal, true);
                            }
                            else
                            {
                                iVal = 12;
                                return;
                            }
                        }

                        if (oldIVal != iVal)
                            AudioDSPtoPCStreamPlayer.PRF = iVal;
					break;
				case GlobalTypes.EDopplerVar.Power:
					turboActionAbort();

					if (p.IsCW)
						return;

					oldIVal = p.Power;
					//RIMD-434: System hangs after trying to set power >100 or <0 and changing sample 2 times on 8 MHz PW probe
					if (up)
					{
						if (oldIVal != Probe.MAX_NORMAL_POWER)
							iVal = p.PowerUp();
						else
							return;
					}
					else
					{
						iVal = p.PowerDown();
						if (iVal == oldIVal)
							return;
					}

					p.WritePower2Dsp(iVal);
					if (!AppSettings.Active.IsDspOffMode && LayoutManager.SystemMode != GlobalTypes.ESystemModes.Offline && (oldIVal != iVal))
						LayoutManager.Instance.UpdateDopplerFlag();

					//TODO, OFER: Re comment below:
					/*very important : For now I don't update the flag that informs about change in the Doppler variables
                         * but it will cause to the Doppler bar to be not-up-to-date in replay mode, so there is a work to do here
                         * uncomment this line will cause inability to change the power at all.
                         */
					break;

				case GlobalTypes.EDopplerVar.ExtraPower:
					if (p.IsCW)
						return;

					if (IsUnilateralStudy)
					{
						if (up)
						{
							iVal = p.TurboPowerUp();
							startTurboAction();
						}
						else
						{
							turboActionAbort();
							iVal = p.TurboPowerDown();
						}

						p.WritePower2Dsp(iVal);
						if (!AppSettings.Active.IsDspOffMode && LayoutManager.SystemMode != GlobalTypes.ESystemModes.Offline)
							LayoutManager.Instance.UpdateDopplerFlag();
					}
					else
					{
						var gv1 = LayoutManager.Instance.GetGate(0);
						var gv2 = LayoutManager.Instance.GetGate(1);

						p = gv1.Probe;
						p2 = gv2.Probe;

						if (up)
						{
							p.TurboPowerUp();
							iVal = p2.TurboPowerUp();
							startTurboAction();
						}
						else
						{
							turboActionAbort();
							iVal = p.TurboPowerDown();
							iVal = p2.TurboPowerDown();
						}
						p.WritePower2Dsp(iVal);
						p2.WritePower2Dsp(iVal);
						if (!AppSettings.Active.IsDspOffMode && LayoutManager.SystemMode != GlobalTypes.ESystemModes.Offline)
							LayoutManager.Instance.UpdateDopplerFlag();
					}
					break;

				case GlobalTypes.EDopplerVar.Width: // = Sample
					if (p.IsCW)
						return;

					double oldDVal = p.Width;
					dVal = up ? p.WidthUp() : p.WidthDown();
					p.WriteWidth2Dsp(p.ProbeIndex, dVal);
					if (!AppSettings.Active.IsDspOffMode && LayoutManager.SystemMode != GlobalTypes.ESystemModes.Offline && (Math.Abs(oldDVal - dVal) > 0.01))
						LayoutManager.Instance.UpdateDopplerFlag();
					break;

				case GlobalTypes.EDopplerVar.Thump: // = Filter
					oldIVal = p.Thump;
					iVal = up ? p.ThumpUp() : p.ThumpDown();
					p.WriteThump2Dsp(iVal);
					if (!AppSettings.Active.IsDspOffMode && LayoutManager.SystemMode != GlobalTypes.ESystemModes.Offline && (oldIVal != iVal))
						LayoutManager.Instance.UpdateDopplerFlag();
					break;
			}

			gv.DopplerBar.DopplerVarChanged(dopplerVar); // Highlight the doppler Var control.

			// check for Big GateView
			if (curGv != null)
				curGv.DopplerBar.DopplerVarChanged(dopplerVar);

			if (AppSettings.Active.IsDspOffMode || LayoutManager.SystemMode == GlobalTypes.ESystemModes.Offline)
			{
				LayoutManager.Instance.UpdateAllDopplerBar(p);
				if (p2 != null)
					LayoutManager.Instance.UpdateAllDopplerBar(p2);
			}

			Logger.LogInfo("Doppler #1 parameter changed: {0}", p);
			if (p2 != null)
				Logger.LogInfo("Doppler #2 parameter changed: {0}", p2);
		}

		private void validateDepth(Probe p)
		{

			if (p == null)
				return;

			if (p.ProbeDepth > p.DepthMax)
			{
				p.ProbeDepth = p.DepthMax;
			}
			else if (p.ProbeDepth < p.DepthMin)
			{
				p.ProbeDepth = p.DepthMin;
			}

		}

		public void RangeDown()
        {
            Logger.LogTrace("RangeDown()", Name);
			
            dopplerChanged(GlobalTypes.EDopplerVar.Range, false);
        }

        private void barItemRangeDown_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemRangeDown", Name);
            RangeDown();
        }

        public void RangeUp()
        {
            Logger.LogTrace("RangeUp()", Name);
    	    dopplerChanged(GlobalTypes.EDopplerVar.Range, true);
		}

        private void barItemRangeUp_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemRangeUp", Name);
            RangeUp();
        }

        private void powerTurboDown()
        {
            Logger.LogTrace("PowerTurboDown()", Name);

            dopplerChanged(GlobalTypes.EDopplerVar.ExtraPower, false);
        }

        public void PowerDown()
        {
            Logger.LogTrace("PowerDown()", Name);

            turboActionAbort();
            dopplerChanged(GlobalTypes.EDopplerVar.Power, false);
        }

        private void barItemPowerDown_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemPowerDown", Name);
            PowerDown();
        }

        private void powerTurboUp()
        {
            Logger.LogTrace("PowerTurboUp()", Name);

            dopplerChanged(GlobalTypes.EDopplerVar.ExtraPower, true);
        }

        public void PowerUp()
        {
            Logger.LogTrace("PowerUp()", Name);
			
            turboActionAbort();
            dopplerChanged(GlobalTypes.EDopplerVar.Power, true);
        }

        private void barItemPowerUp_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemPowerUp", Name);
            PowerUp();
        }

        public void WidthDown()
        {
            Logger.LogTrace("WidthDown()", Name);
			
            dopplerChanged(GlobalTypes.EDopplerVar.Width, false);
        }

        private void barItemWidthDown_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemWidthDown", Name);
            WidthDown();
        }

        public void WidthUp()
        {
            Logger.LogTrace("WidthUp()", Name);
			
            dopplerChanged(GlobalTypes.EDopplerVar.Width, true);
        }

        private void barItemWidthUp_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemWidthUp", Name);
            WidthUp();
        }

        public void ThumpDown()
        {
            Logger.LogTrace("ThumpDown()", Name);
			
            dopplerChanged(GlobalTypes.EDopplerVar.Thump, false);
        }

        private void barItemThumpDown_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemThumpDown", Name);
            ThumpDown();
        }

        public void ThumpUp()
        {
            Logger.LogTrace("ThumpUp()", Name);
			
            dopplerChanged(GlobalTypes.EDopplerVar.Thump, true);
        }

        private void barItemThumpUp_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemThumpUp", Name);
            ThumpUp();
        }

        private void toggleFwdPeakEnvelope()
        {
            Logger.LogTrace("toggleFwdPeakEnvelope()", Name);

            LayoutManager.Instance.ToggleFwdPeakEnvelope();
        
            //Work around to get the spectrum be repainted in offline mode(I guess there are other, and more strait forward ways to do it
            if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Offline)
            {
                if (LayoutManager.SystemLayoutMode != GlobalTypes.ESystemLayoutMode.Monitoring && LayoutManager.SystemLayoutMode != GlobalTypes.ESystemLayoutMode.MonitoringLoad)
                    callScrolling(EScrollAction.REFRESH_WITH_NO_SCROLLING);
            }
        }

        private void toggleBckPeakEnvelope()
        {
            Logger.LogTrace("toggleBckPeakEnvelope()", Name);

            LayoutManager.Instance.ToggleBckPeakEnvelope();
            
            //Work around to get the spectrum be repainted in offline mode(I guess there are other, and more strait forward ways to do it
            if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Offline)
            {
                if (LayoutManager.SystemLayoutMode != GlobalTypes.ESystemLayoutMode.Monitoring && LayoutManager.SystemLayoutMode != GlobalTypes.ESystemLayoutMode.MonitoringLoad)
                    callScrolling(EScrollAction.REFRESH_WITH_NO_SCROLLING);
            }
        }

        private void toggleFwdModeEnvelope()
        {
            Logger.LogTrace("toggleFwdModeEnvelope()", Name);

			if (LayoutManager.Instance.SelectedGv == null)
			{
				Logger.LogWarning("No selected GateView. {0}.toggleFwdModeEnvelope() aborted.", Name);
				return;
			}
			
			var newMode = !LayoutManager.Instance.SelectedGv.Spectrum.Graph.DrawModeFwdEnvelope;
            LayoutManager.Instance.SelectedGv.Spectrum.Graph.DrawModeFwdEnvelope = newMode;

            if (LayoutManager.Instance.CurrentGateView != null)
                LayoutManager.Instance.CurrentGateView.Spectrum.Graph.DrawModeFwdEnvelope = newMode;

            if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Offline)
            {
                if (LayoutManager.SystemLayoutMode != GlobalTypes.ESystemLayoutMode.Monitoring && LayoutManager.SystemLayoutMode != GlobalTypes.ESystemLayoutMode.MonitoringLoad)
                    callScrolling(EScrollAction.REFRESH_WITH_NO_SCROLLING);
            }
        }

        private void toggleBckModeEnvelope()
        {
            Logger.LogTrace("toggleBckModeEnvelope()", Name);

			if (LayoutManager.Instance.SelectedGv == null)
			{
				Logger.LogWarning("No selected GateView. {0}.toggleBckModeEnvelope() aborted.", Name);
				return;
			}

			bool newMode = !LayoutManager.Instance.SelectedGv.Spectrum.Graph.DrawModeBckEnvelope;
            LayoutManager.Instance.SelectedGv.Spectrum.Graph.DrawModeBckEnvelope = newMode;

            if (LayoutManager.Instance.CurrentGateView != null)
                LayoutManager.Instance.CurrentGateView.Spectrum.Graph.DrawModeBckEnvelope = newMode;

            if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Offline)
            {
                if (LayoutManager.SystemLayoutMode != GlobalTypes.ESystemLayoutMode.Monitoring && LayoutManager.SystemLayoutMode != GlobalTypes.ESystemLayoutMode.MonitoringLoad)
                    callScrolling(EScrollAction.REFRESH_WITH_NO_SCROLLING);
            }
        }

        private void toggleSpectrumDisplay()
        {
            Logger.LogTrace("toggleSpectrumDisplay()", Name);
			if (LayoutManager.Instance.SelectedGv == null)
			{
				Logger.LogWarning("No selected GateView. {0}.toggleSpectrumDisplay() aborted.", Name);
				return;
			}

			LayoutManager.Instance.SelectedGv.Spectrum.Graph.DrawSpectrum = !LayoutManager.Instance.SelectedGv.Spectrum.Graph.DrawSpectrum;
            if (LayoutManager.Instance.CurrentGateView != null)
                LayoutManager.Instance.CurrentGateView.Spectrum.Graph.DrawSpectrum = !LayoutManager.Instance.CurrentGateView.Spectrum.Graph.DrawSpectrum;
           
        }

		private int m_freezeVolume			= 0;
		private int m_freezeVolumeIncrease	= 0;

		public bool IsUnfreezMute { get { return m_freezeVolumeIncrease > 0; } }
		private void toggleFreezeMode()
        {

            Logger.LogTrace("toggleFreezeMode()", Name);

            //Return by Alex Bug #625 v.2.2.2.09 08/01/2015
            //LayoutManager.Instance.SetVisibilityClinikParams(true);

            StopTurboAction();
            LayoutManager.Instance.FireChangeModeEvent(); //Call this from dspManager - this is temporary solution, this module needs redesign!!

            if (!LayoutManager.ReplayStarted)
                StopWMVRecord();
            else
                toolBarPanel1.ButtonWMV.Visible = true;



            hitsHistogram1.MarkHitsStatus();

			// Reset audio player
			AudioDSPtoPCStreamPlayer.Reset();

            //Changed by Alex Feature #572 (item 3) v.2.2.1.00 22/10/2014 and Bug #597 v.2.2.2.02 11/11/2014
            LayoutManager.Instance.PicManualCalculationsVisible = (LayoutManager.SystemMode != GlobalTypes.ESystemModes.Online && 
                                                                   !CurrentStudyName.StartsWith(Constants.StudyType.STUDY_EVOKED_FLOW_BILATERAL) && 
                                                                   !CurrentStudyName.StartsWith(Constants.StudyType.STUDY_EVOKED_FLOW_UNILATERAL)
                                                                   );

            LayoutManager.Instance.LindegaardRatioVisible = LayoutManager.SystemMode != GlobalTypes.ESystemModes.Online;

            //Added by Alex Bug #594 v.2.2.1.05 29/10/2014
            MainForm.ToolBar.ButtonCursors.Enabled = true;


            //Added by Alex Feature #587 v.2.2.1.04 27/10/2014
            LayoutManager.Instance.PicManualCalculationsOffOn = false;
            LayoutManager.Instance.PointMode = false;
            LayoutManager.Instance.CursorsMode = false;

            // Added by Alex 19/02/2015 New 128 bits v 2.2.2.18
            LayoutManager.Instance.UpdateDopplerFlag();

			// Set OS timer resolution (1ms for online mode, OS default (15.6ms) for offline mode.
	        if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Online)
	        {
				if (!IsMute && AppSettings.Active.AudioUnfreezMuteEnable)
				{
					m_freezeVolume = SystemVolume;
					m_freezeVolumeIncrease = (m_freezeVolume - AppSettings.Active.AudioUnfreezMuteVolume) / AppSettings.Active.AudioUnfreezMuteRestoreSeconds;
					if (m_freezeVolumeIncrease < 1)
						m_freezeVolumeIncrease = 1;
					setVolume(AppSettings.Active.AudioUnfreezMuteVolume);
				}
				OsTimerResolution.Set();
                IsFirstScreen = true;
	        }
	        else
	        {
		        OsTimerResolution.ResetLast();
			}
        }

        private void zeroLineUp()
        {
            Logger.LogTrace("zeroLineUp()", Name);
			if (LayoutManager.Instance.SelectedGv == null)
			{
				Logger.LogWarning("No selected GateView. {0}.zeroLineUp() aborted.", Name);
				return;
			}

			LayoutManager.Instance.SelectedGv.Spectrum.Graph.ZeroLinePercent += SpectrumGraph.ZERO_LINE_STEP;
            if (LayoutManager.Instance.CurrentGateView != null)
                LayoutManager.Instance.CurrentGateView.Spectrum.Graph.ZeroLinePercent += SpectrumGraph.ZERO_LINE_STEP;
        }

        private void zeroLineDown()
        {
            Logger.LogTrace("zeroLineDown()", Name);
			if (LayoutManager.Instance.SelectedGv == null)
			{
				Logger.LogWarning("No selected GateView. {0}.zeroLineDown() aborted.", Name);
				return;
			}

			LayoutManager.Instance.SelectedGv.Spectrum.Graph.ZeroLinePercent -= SpectrumGraph.ZERO_LINE_STEP;
            if (LayoutManager.Instance.CurrentGateView != null)
                LayoutManager.Instance.CurrentGateView.Spectrum.Graph.ZeroLinePercent -= SpectrumGraph.ZERO_LINE_STEP;
        }

        // toggle on/off mute
        // if muteOnly=true only mute
        private void mute(bool muteOnly=false)
        {
            if (muteOnly && IsMute)
                return;
            Logger.LogTrace("Mute()", Name);

			IsMute	= !IsMute;
			var vol = (IsMute) ? 0 : SystemVolume;

			m_freezeVolumeIncrease = 0;

			fireVolumeChanged(SystemVolume, IsMute);

			DspManager.Instance.WriteVolume2Dsp((ushort)vol);
			AudioDSPtoPCStreamPlayer.SetVolume(vol);
        }

        private void doSpaceBarWork()
        {
            Logger.LogTrace("DoSpaceBarWork()", Name);

			AudioDSPtoPCStreamPlayer.Reset();

			// Disable method for new studies on DSP Off mode
	        if (AppSettings.Active.IsDspOffMode && !bvListViewCtrl1.ExaminationWasSaved) 
		        return;

            if (AtTheEndOfMonitoringExam)
            {
                toolBarPanel1.ButtonFreeze.IsPressed = false;
				LoggedDialog.Show(this, StringManager.GetString("To start a new examination please choose one from the main menu"), StringManager.GetString("Information"));

                return;
            }

            if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.MonitoringLoad)
            {
	            doPauseAction();

				return;
            }

            if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Monitoring && LayoutManager.SystemMode == GlobalTypes.ESystemModes.Online)
            {
				toggleFreezeMode();
				guiMgrSmGo(new CGuiSmEvent(EGuiMgrSmEvent.GUI_FREEZE_EVENT));
			}
            else if (!m_fakeDataRowMode)
            {
                var win32UseObjects = User32.GetGuiResources(Processes.Current.Handle, User32.EGetGuiResourceType.GR_USEROBJECTS);
                if (win32UseObjects > PROCESS_USER_OBJECTS_THRESHOLD && LayoutManager.SystemMode != GlobalTypes.ESystemModes.Online)
                {
                    LoggedDialog.ShowWarning(this, string.Format("Attention system resources is low ({0:D2}%).\nNew tests can NOT be added to the study.", (100 * win32UseObjects) / MAX_PROCESS_USER_OBJECTS));
                }
                else
                {
                    toggleFreezeMode();
                    if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Monitoring && LayoutManager.SystemMode == GlobalTypes.ESystemModes.Online)
                        toolBarPanel1.ButtonStartRecording.IsPressed = startRecord();

                    guiMgrSmGo(new CGuiSmEvent(EGuiMgrSmEvent.GUI_TOGGLE_FREEZE_EVENT));
                }
            }

            toolBarPanel1.ButtonFreeze.IsPressed = (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Online);
        }

        private delegate void SetFakeDataRowModeDelegate(bool fakeDataRowMode);

        public void SetFakeDataRowMode(bool fakeDataRowMode)
        {
	        if (InvokeRequired)
            {
                BeginInvoke((SetFakeDataRowModeDelegate)SetFakeDataRowMode, fakeDataRowMode);
                return;
            }

            m_fakeDataRowMode = fakeDataRowMode;
	  
            MainMenuItemAutoscanEnabled = !fakeDataRowMode;
            MainMenuItemPlayEnabled = !fakeDataRowMode;
            MainMenuItemStopEnabled = !fakeDataRowMode;
            MainMenuItemAccemssionNumberEnabled = !fakeDataRowMode;
            MainMenuItemCursorsEnabled = !fakeDataRowMode;

			toolBarPanel1.ButtonPlayCtrl.Enabled = !fakeDataRowMode;
			toolBarPanel1.ButtonPause.Enabled = !fakeDataRowMode;
			toolBarPanel1.ButtonStopRecordingCtrl.Enabled = !fakeDataRowMode;
			toolBarPanel1.ButtonCursors.Enabled = !fakeDataRowMode;
			toolBarPanel1.ComboBoxTime.Visible = !fakeDataRowMode;
            ComboBoxTimeRight.Visible = !fakeDataRowMode;
        }


        private delegate void ExitDigiLiteDelegate(bool isConfirm, bool isExitToWindows, bool isShutdownWindows);
		public void ExitDigiLite(bool isConfirm = false, bool isExitToWindows = false, bool isShutdownWindows = false)
        {
			if (InvokeRequired)
			{
				BeginInvoke((ExitDigiLiteDelegate)ExitDigiLite, isConfirm, isExitToWindows, isShutdownWindows);
				return;
			}

			Logger.LogDebug("ExitDigiLite(isConfirm={0}, isExitToWindows={1}, isShutdownWindows={2})", isConfirm, isExitToWindows, isShutdownWindows);

			if (isConfirm && !s_isThreadUnhandledException)
            {
				if (LoggedDialog.Show(this, "Are You sure you want to exit " + AppSettings.Active.ApplicationProductName + "?", "Exit " + AppSettings.Active.ApplicationProductName, MessageBoxButtons.YesNo, MessageBoxIcon.Warning) != DialogResult.Yes)
                    return;
            }

            try
            {
                
	            TopMost				= IsInitialized;
				m_exitToWindows		= isExitToWindows;
				m_shutdownWindows	= isShutdownWindows;

				if (IsInitialized)
					Close();
				else
					Application.Exit();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
			//finally
			//{
			//    if (IsInitialized)
			//        Application.Exit();
			//    else
			//        Processes.Current.Kill();
			//}
        }

        private void timerPanels_Tick(object sender, EventArgs e)
        {
            var move = panelHITS2.Width / 10;
            // move to the left
            if (s_sOpenPanel)
            {
                if (panelHITS2.Right == panelRight.Width)
                {
                    s_sOpenPanel = false;
                    timerPanels.Enabled = false;
                    panelHeaderRight.Dock = DockStyle.None;
                    panelHITS2.Dock = DockStyle.Fill;
                }
                else
                {
                    panelHITS2.Left -= move;
                    if (panelHITS2.Left < (panelRight.Width - panelHITS2.Width))
                        panelHITS2.Left = (panelRight.Width - panelHITS2.Width);
                }
            }
            else // move to the right
            {
                if (panelHITS2.Left == panelRight.Width)
                {
                    s_sOpenPanel = true;
                    timerPanels.Enabled = false;
                    panelHITS2.Visible = false;
                }
                else
                {
                    panelHITS2.Left += move;
                    if (panelHITS2.Left > panelRight.Width)
                        panelHITS2.Left = panelRight.Width;
                }
            }
        }

        private void barItemPanelHits_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemPanelHits", Name);
            timerPanels.Enabled = true;
            panelHITS2.Visible = true;
            if (!s_sOpenPanel)
            {
                panelHITS2.Dock = DockStyle.None;
                panelHeaderRight.Dock = DockStyle.Top;
            }
            else
                panelHITS2.Size = panelRight.Size;
        }

        public void LoadStudy()
        {
            SummaryScreen.HideDialog();

            using (var dlg = new StudiesSelectDlg())
            {
				dlg.TopMost = true;	
				dlg.InitData(parentBarItemStudies.Items);
	            var res = dlg.ShowDialog(this);
	            Focus();
				if (res == DialogResult.OK)
                    loadSelectedStudy(dlg.SelectedNodeTag);  
            }
        }

        /// <summary>Function is called when the user open the summary screen. Application go through all the gates from the gates collection and add the gates that not being added yet.</summary>
        private void buttonSummary_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("buttonSummary", Name);
            if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.MonitoringLoad || LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Monitoring)
                return;

            //ReplayMode is changed to false, because buttonPlay_Click is started every time when we return from Summary Screen (if Replay -> Summary Screen was) in other case Was added by Natalie (05/22/2009) for RIMD-100 fix
            LayoutManager.Instance.ReplayMode = false;

            guiMgrSmGo(new CGuiSmEvent(EGuiMgrSmEvent.GUI_BUTTON_SUMMARY_CLICK_EVENT));
            string selectedItem = bvListViewCtrl1.SelectBVSubExamIndex; // added by Alex 19/08/2014 fixed bug #507 ver. 2.1.7.6
            SummaryScreen.JustExit = false; // added by Alex 04/09/2014 fixed bug #548 ver. 2.2.0.7
            SummaryScreen.OpenDialog();     //Ofer, v2.0.4.16: BugFix. Unhandled exception.
            if (SummaryScreen.JustExit) // added by Alex 04/09/2014 fixed bug #548 ver. 2.2.0.7
                bvListViewCtrl1.SelectBVbyIndex(selectedItem);// added by Alex 19/08/2014 fixed bug #507 ver. 2.1.7.6
	        Focus();

			//if (SummaryScreen.IsReturnForReplay && LayoutManager.Instance.ReplayMode && !bvListViewCtrl1.IsCurrentBVHasOnlySummaryImages())
			//    buttonPlay_Click(toolBarPanel1.ButtonPlay, e);
        }

        private void buttonAccessionNumber_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("buttonAccessionNumber", Name);
            EditAccessionNumber();
        }
		
        private void buttonScrollBackWard_Click(object sender, EventArgs e)
        {
            // TODO Call dspManager func that does all that
            LoggerUserActions.MouseClick("buttonScrollBackWard", Name);
			if (ScrollingEnabled)
                callScrolling(EScrollAction.STEP_BACK);

            //RIMD-552: Trend window is not updated (event jumping works incorrect)
            LayoutManager.Instance.CursorsMode = false;
        }
		
        private void buttonScrollForward_Click(object sender, EventArgs e)
        {
			//System.Diagnostics.Debug.WriteLine("[RIMED] OFER: =================================================================================================================");
			
			LoggerUserActions.MouseClick("buttonScrollForward", Name);
            toolBarPanel1.ButtonScrollBackWardCtrl.Enabled = true;

            //RIMD-552: Trend window is not updated (event jumping works incorrect)
            LayoutManager.Instance.CursorsMode = false;
			
            if (ScrollingEnabled)
                callScrolling(EScrollAction.STEP_FORWARD);
        }
		
        // studies button on tool bar
        private void buttonStudies_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("buttonStudies", Name);
            LoadStudy();
        }

        private enum EScrollAction { STEP_FORWARD, STEP_BACK, REFRESH_WITH_NO_SCROLLING };

        private void callScrolling(EScrollAction scrollAction)
        {
            var numberOfSteps = 0;
            var isScrollBack = false;
            var index = ComboBoxTimeRight.SelectedIndex;
            if (IsUnilateralStudy)  //Merge, Ofer
            {
                switch (index)
                {
                    case 0:
                        numberOfSteps = 146;
                        break;
                    case 1:
                        numberOfSteps = 271;
                        break;
                    case 2:
                        numberOfSteps = 417;
                        break;
                    case 3:
                        numberOfSteps = 542;
                        break;
                }
            }
            //bilateral
            else
            {
                switch (index)
                {
                    case 0:
                        numberOfSteps = 63;
                        break;
                    case 1:
                        numberOfSteps = 125;
                        break;
                    case 2:
                        numberOfSteps = 188;
                        break;
                    case 3:
                        numberOfSteps = 229;
                        break;
                    case 4:
                        numberOfSteps = 292;
                        break;
                    case 5:
                        numberOfSteps = 583;
                        break;
                }
            }

            //NumberOfSteps *= 3; // Scroll by the whole seconds - ARCHER
            
			// Asked for smaller resolution jumps. Before this change it was half-screen "jumps"
            if (scrollAction == EScrollAction.STEP_BACK)
            {
                MonitoringReplayTimeCounter = MonitoringReplayTimeCounter.AddSeconds(-1);
                if (MonitoringReplayTimeCounter < EventListViewCtrl.Instance.ExaminationDate)
                    MonitoringReplayTimeCounter = EventListViewCtrl.Instance.ExaminationDate;
                isScrollBack = true;
            }
            else if (scrollAction == EScrollAction.STEP_FORWARD)
                MonitoringReplayTimeCounter = MonitoringReplayTimeCounter.AddSeconds(1);
			
            if (scrollAction == EScrollAction.REFRESH_WITH_NO_SCROLLING)
                numberOfSteps = 0;
            else
                numberOfSteps = (int)numberOfSteps / 35;
				
			var paramsArr = new Object[1];
			paramsArr[0] = new CDspMgrScrollParams(isScrollBack, numberOfSteps);
			guiMgrSmGo(new CGuiSmEvent(EGuiMgrSmEvent.GUI_SCROLL_EVENT, paramsArr));
        }

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("buttonPrint", Name);
            printScreenRep(false);
        }
		
        private void printScreenRep(bool preview)
        {
			var examRow = RimedDal.Instance.LoadedExamination();
	        if (examRow == null)
		        return;

            toolBarPanel1.Visible = false;

            toolBarPanel1.Hide();
            toolBarPanel1.Invalidate();
            using (var ms = new MemoryStream())
            {
                if (screenCapture(ms))
                {
                    var notes = bvListViewCtrl1.SubExamNotes;

                    var dvPatient   = new DataView(RimedDal.Instance.PatientsDS.tb_Patient, "Patient_Index = '" + m_selectedPatientIndex + "'", null, DataViewRowState.CurrentRows);
                    var dvHospital  = new DataView(RimedDal.Instance.ConfigDS.tb_HospitalDetails);
                    var dvExam      = new DataView(RimedDal.Instance.ExaminationsDS.tb_Examination);
					var report		= new PrintScreenReport(examRow.Examination_Index, ms, notes, dvHospital, dvPatient, dvExam);	//= new PrintScreenReport((Guid)RimedDal.Instance.ExaminationsDS.tb_Examination.Rows[0]["Examination_Index"], ms, notes, dvHospital, dvPatient, dvExam);

					report.Display(preview);
                    toolBarPanel1.Visible = true;
                }
            }
        }

        private void buttonAddTrend_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("buttonAddTrend", Name);
            LayoutManager.Instance.OnNewTrend(true);
        }

        public string DoNotes(GlobalTypes.ENotesType notesType, string notes)
        {
            using (var dlg = new NotesFrm(notesType, notes))
            {
				dlg.TopMost = true;
				var res = dlg.ShowDialog(this);
				Focus();
				if (res == DialogResult.OK)
                {
                    notes = dlg.Notes;
                    //Changed by Alex bug #713 v.2.2.3.19 03/01/2016 
                    //CloseKeyboard();
                    //return dlg.Notes;
                }
            }
            CloseKeyboard();

            return notes;
        }

        private void buttonNotes_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("buttonNotes", Name);
            var notes = bvListViewCtrl1.SubExamNotes;
            bvListViewCtrl1.SubExamNotes = DoNotes(GlobalTypes.ENotesType.SubExamNotes, notes);
        }

	    // Added by Alex 05/04/2016 fixed bug #NN v. 2.2.3.28
	    public bool CanSendToDicom()
	    {
	        bool isMonitoring = LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Monitoring ||
	                            LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.MonitoringLoad;
            bool isDiagnostic = LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Diagnostic ||
                                LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.DiagnosticLoad;
            bool isLoaded = LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.DiagnosticLoad ||
                          LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.MonitoringLoad;

            return isLoaded || (IsMonitoringStoped && isMonitoring) || (bvListViewCtrl1.ExaminationWasSaved && isDiagnostic);
	    }

	    private void buttonDicom_Click(object sender, EventArgs e)
	    {
	        LoggerUserActions.MouseClick("buttonDicom", Name);
	        // Changed by Alex 04/04/2016 fixed bug #NN v. 2.2.3.28
            if (!CanSendToDicom())
	        {
                LoggedDialog.Show(this, "Please save the examination before sending it to a DICOM server.", "DICOM", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            
            toolBarPanel1.DisableDicomForPeriod();

		    Image img = HelperMethods.CaptureControlSnapshot(this);
			if (img == null)
				return;

			using (img)
				PacsManager.SendImageToPacsServer(img);
		}

	    private void startRecProcess()
	    {
            myProcess = new Process();

            myProcess.StartInfo.FileName = "Rimed.RecWMV.exe";
            myProcess.StartInfo.UseShellExecute = false;
            myProcess.StartInfo.RedirectStandardInput = true;

            myProcess.Start();
            myStreamWriter = myProcess.StandardInput;
        
	    }

	    private bool isRecWMN = false;
        Process myProcess = null;
        StreamWriter myStreamWriter = null;
        private bool useScabLib = true;
        
        private void buttonWMV_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("buttonWMV", Name);
            
            // Changed by Alex 25/11/2015 feature #672 v 2.2.3.12
            string fileName = "OutputFile.wmv";
            if (myProcess == null)
            {
                startRecProcess();
                Thread.Sleep(100);
            }

            bool wasChanged = false; 

            if (isRecWMN)
            {
                // Changed by Alex 20/12/2015 feature #672 v 2.2.3.16
                if (LayoutManager.ReplayStarted)
                {
                    stop();
                    return;
                }

                isRecWMN = !isRecWMN;
                wasChanged = true;

                if (useScabLib)
                {
                    myStreamWriter.WriteLine("STOP" + ";" + m_patientName + ";" + m_patientID);
                    
                }
                else
                { // not use
                    myStreamWriter.WriteLine("STOP");
                    if (MessageBox.Show("Do you want to save this video?", "Video", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        SaveFileDialog sfd = new SaveFileDialog();


                        string pref = (m_patientName.IndexOf('*') > -1 ? m_patientID : m_patientName);
                        pref = pref.Replace('/', '_').Replace(':', '_').Replace('*', '_').Replace('?', '_').Replace('"', '_').Replace('<', '_').Replace('>', '_').Replace('|', '_').Trim();

                        sfd.FileName = pref + "_" + DateTime.Now.ToString("ddMMyy") + ".wmv";
                        sfd.Filter = "Windows Media Video  files (*.wmv)|*.wmv";
                        if (sfd.ShowDialog() == DialogResult.OK)
                            myStreamWriter.WriteLine("SAVE;" + sfd.FileName);
                    }
                    else
                    {
                        // Added by Alex 31/03/2016 bug #839 v 2.2.3.26
                        myStreamWriter.WriteLine("HIDE");
                    }
                }
            }
            else
            {
                myStreamWriter.WriteLine("START");
            }
            if (!wasChanged)
                isRecWMN = !isRecWMN;
        }

	    private void StopWMVRecord()
	    {
	        BeginInvoke((Action) delegate()
	        {
	            if (isRecWMN)
	            {
	                buttonWMV_Click(null, null);
	                toolBarPanel1.ButtonWMV.IsPressed = false;
	            }
	            toolBarPanel1.ButtonWMV.Visible = false;
	        });
	    }

	    // Added by Alex 30/11/2015 feature #672 v 2.2.3.14
        private void StopMyProcess()
	    {
	        if (myProcess != null)
	        {
	            myStreamWriter.WriteLine("CLOSE");
	            // Wait for the sort process to write the sorted text lines.
	            myProcess.WaitForExit();
	            myProcess.Close();

	            Thread.Sleep(1000);
	            myStreamWriter.Close();
                myProcess = null;
            }
	    }


	    #region Virtual Keyboard

        public void DisplayKeyboard()
        {
            // Changed by Alex 26/08/2014 fixed bug #513 v 2.2.0.3
            Process.Start(@"C:\Program Files\Common Files\Microsoft Shared\ink\TabTip.exe");
        }

        public void CloseKeyboard()
        {
            // Changed by Alex 26/08/2014 fixed bug #513 v 2.2.0.3
            var iHandle = User32.FindWindow("IPTIP_Main_Window", "");
            if (iHandle != IntPtr.Zero)
            {
                User32.SendMessage((uint)iHandle, User32.WinMessage.WM_SYSCOMMAND, User32.WinMessage.SC_CLOSE, 0);
            }
        }
        #endregion


        public void ToggleHits()
        {
            Logger.LogTrace("ToggleHits()", Name);
            SetHitsControls(!barItemHitsDetection.Checked);
		}

        private delegate void SetHitsControlsDelegate(bool isChecked);
        public void SetHitsControls(bool isChecked)
        {
            if (InvokeRequired)
            {
                BeginInvoke((SetHitsControlsDelegate)SetHitsControls, isChecked);
                return;
            }

            barItemHitsDetection.Checked = isChecked;
            toolBarPanel1.ButtonHitsDetection.IsPressed = isChecked;
            HitsHistogram.HitsOnOff(isChecked);
        }

        private void buttonAHitsDetection_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("buttonAHitsDetection", Name);
            ToggleHits();
        }

        /// <summary>Add spectrum button on toolbar.</summary>
        private void buttonAddSpectrum_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("buttonAddSpectrum", Name);
            LayoutManager.Instance.OpenNewSpectrumWindow();
            //Changed by Alex Feature #586 v.2.2.1.04 27/10/2014 and Bug #597 v.2.2.2.02 11/11/2014
            LayoutManager.Instance.PicManualCalculationsVisible = (LayoutManager.SystemMode != GlobalTypes.ESystemModes.Online &&
                                                                   !CurrentStudyName.StartsWith(Constants.StudyType.STUDY_EVOKED_FLOW_BILATERAL) &&
                                                                   !CurrentStudyName.StartsWith(Constants.StudyType.STUDY_EVOKED_FLOW_UNILATERAL)
                                                                   );
            LayoutManager.Instance.LindegaardRatioVisible = LayoutManager.SystemMode != GlobalTypes.ESystemModes.Online;

	        resetGraphs();
	        //ResetMainSpectrumTimeFrame();
        }

        private void buttonLoad_Click(object sender, EventArgs e)
        {
            if (IsClosing)   
                return;

            LoggerUserActions.MouseClick("buttonLoad", Name);

	        OpenLoadDialog();
        }

		public void OpenLoadDialog()
		{
			if (IsClosing)    
				return;

			SummaryScreen.HideDialog();

			//resetRelativeTimeFlages();
			WorkingMode = GlobalTypes.EWorkingMode.LoadMode;

			var res = PatientSearch.OpenDialog(this);
			Focus();

			if (res == DialogResult.OK)
				loadSelectedStudy(CurrentStudyId);
		}

        // New Patient button on tool-bar.
        private void buttonNewPatient_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("buttonNewPatient", Name);
	        OpenNewPatientDialog();
        }

        public void OpenNewPatientDialog()
		{
			if (IsClosing)    
				return;

			SummaryScreen.HideDialog();

			using (var dlg = new PatientDetails())
			{
				dlg.TopMost = true;
				var res = dlg.ShowDialog(this);
				Focus();
				if (res == DialogResult.OK)
				{
					SelectedPatientIndex = dlg.DSPatient.tb_Patient[0].Patient_Index;
					loadSelectedStudy(CurrentStudyId); 
				}
			}
		}

        private void buttonWorkList_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("buttonWorkList", Name);
            OpenWorkListDialog();
        }

        // Added by Alex Feature #836 v.2.2.3.24 14/03/2016
        string currentAccessionNumber = "";
	    public string CurrentAccessionNumber
	    {
	        get
	        {
                if (currentAccessionNumber == "")
	            {
                    int lastAccessionNumber = RimedDal.Instance.GetLatestAccessionNumber(m_selectedPatientIndex, m_patientID) + 1;
                    currentAccessionNumber = m_patientID + "E" + lastAccessionNumber;
	            }
	            return currentAccessionNumber;
	        }
            set { currentAccessionNumber = value; }
	    }

        // Added by Alex Feature #836 v.2.2.3.24 14/03/2016
        public void EditAccessionNumber()
	    {
            var examRow = RimedDal.Instance.LoadedExamination();
            string accessionNumber = RimedDal.Instance.GetAccessionNumber(examRow.Examination_Index);

            var editAccessionNumber = new EditAccessionNumber { Current = accessionNumber };
            if (editAccessionNumber.ShowDialog() == DialogResult.OK)
            {
                CurrentAccessionNumber = editAccessionNumber.Current;
                examRow.AccessionNumber = CurrentAccessionNumber;
                RimedDal.Instance.UpdateExamination(examRow, "AccessionNumber", CurrentAccessionNumber);
            }
	    }

        // Added by Alex CR #867  v.2.2.3.37 25/05/2016
	    private string dicom_modality = "";
        private string dicom_StationNameEdit = "";

        public void OpenWorkListDialog()
        {
            if (IsClosing)
                return;
            PatientDicomWorkList patientDicomWorkList = new PatientDicomWorkList();

            // added by Alex 23/08/2015 featere "Use different PACS server for Worklist" v 2.2.3.5
            IniXML iniXML = new IniXML("WorkList.xml");
            PacsManager.WorkListIP = iniXML.Read("PACS", "IP", "www.dicomserver.co.uk");
            PacsManager.WorkListPort = iniXML.Read("PACS", "Port", "104");
            PacsManager.WorkListLAE = iniXML.Read("PACS", "LocalAE", "localAE");
            PacsManager.WorkListRAE = iniXML.Read("PACS", "RemoteAE", "remoteAE");

            patientDicomWorkList.pdl = PacsManager.GetDICOMWorkList("", "");

            patientDicomWorkList.ShowDialog();

            if (patientDicomWorkList.current != null)
            {
                // Deleted by Alex Bug #845 v.2.2.3.28 05/04/2016
                // CurrentAccessionNumber = patientDicomWorkList.current.AccessionNumber;
                
                var filter = "Select * FROM tb_Patient WHERE (ID = '" + patientDicomWorkList.current.ID + "')";
                dsPatient pat = RimedDal.Instance.GetPatients(filter);
                if (pat.tb_Patient.Rows.Count == 1) //start procedure with exist patient
                {
                    SelectedPatientIndex = pat.tb_Patient[0].Patient_Index;
                    loadSelectedStudy(CurrentStudyId);
                    // Added by Alex Feature #845 v.2.2.3.28 05/04/2016
                    CurrentAccessionNumber = patientDicomWorkList.current.AccessionNumber;
                }
                else // open new patient form
                {
                    SummaryScreen.HideDialog();
                    {
                        using (var dlg = new PatientDetails())
                        {
                            dlg.TopMost = true;
                            dlg.SetDataFromWorkList(patientDicomWorkList.current);
                            var res = dlg.ShowDialog();
                            Focus();
                            if (res == DialogResult.OK)
                            {
                                SelectedPatientIndex = dlg.DSPatient.tb_Patient[0].Patient_Index;
                                loadSelectedStudy(CurrentStudyId);
                                // Added by Alex Feature #845 v.2.2.3.28 05/04/2016
                                CurrentAccessionNumber = patientDicomWorkList.current.AccessionNumber;
                            }
                        }
                    }
                }
            }
        }

        // auto scan button on tool-bar
        private void buttonAutoscan_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("buttonAutoscan", Name);
            // Simulate selection of autoscan in right click menu
            if (LayoutManager.Instance.SelectedGv == null)
                return;

            LayoutManager.Instance.ToggleAutoscanWindow(true);
        }

        private delegate void SetAutoscanCheckedDelegate(bool isChecked);
        public void SetAutoscanChecked(bool isChecked)
        {
            if (InvokeRequired)
            {
                BeginInvoke((SetAutoscanCheckedDelegate)SetAutoscanChecked, isChecked);
                return;
            }

            barItemAutoscan.Checked = isChecked;
        }

        /// <summary>Set the text in the caption according to the current patient name, study, and time</summary>
        private StringBuilder buildCaption(string mainTitle, string name = null)
        {
            var sb = new StringBuilder();

			// Application name
			sb.Append("Rimed "+ mainTitle);

			if (AppSettings.Active.IsDspOffMode && AppSettings.Active.IsDemoMode)
	            sb.Append(" [DEMO]");

			sb.Append(": ");

            // patient name
            if (! string.IsNullOrWhiteSpace(m_patientName))
            {
                sb.Append(m_patientName);
            }
            else
            {
                // this code was change to Exhibition only
                sb.Append("Unknown");
                m_selectedPatientIndex = Guid.Empty;
            }

            // study name
            sb.Append(" - [");
	        if (name != null)
				sb.Append(name);
			else if (CurrentStudyName != string.Empty)
                sb.Append(CurrentStudyName);
            else 
                sb.Append("--");
            sb.Append("]");

            return sb;
        }

        private void setCaptionText()
        {
            var text = buildCaption(AppSettings.Active.ApplicationProductName) + "           " + m_timeStr + (isRecWMN ? "       Exporting to *.wmv" : "");
            if (InvokeRequired)
            {
                BeginInvoke((Action)delegate
                                        {
                                            Text = text;
                                            labelCaption.Text = text;
                                            labelCaption.Left = Width - labelCaption.Width -3;
                                        }
                           );
            }
            else
            {
                Text = text;
                labelCaption.Text = text;
                labelCaption.Left = Width - labelCaption.Width -3;
            }
        }

        /// <summary>Set patient name in caption</summary>
        // Changed by Alex 3/11/2015 feature #672 v 2.2.3.15
        private void setPatientName(string patientName, string id)
        {
			if (InvokeRequired)
			{
				BeginInvoke((Action)(() => setPatientName(patientName, id)));
				return;
			}

            m_patientID = id;
            m_patientName = patientName;
            setCaptionText();
        }

        private readonly object m_lockTimer = new object();

        private void onOneSecondTimer_Tick(object sender, EventArgs e)
        {
            var secs = DateTime.UtcNow.Subtract(m_lastTimerUtcTime).TotalSeconds;
            m_lastTimerUtcTime = DateTime.UtcNow;
            if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Monitoring)
            {
                //TODO, Ofer: Add relative timer in monitoring
				toolBarPanel1.TimeLabelText = DateTime.Now.ToString("HH:mm:ss");
            }
            else if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.MonitoringLoad && !LayoutManager.Instance.IsPaused && LayoutManager.SystemMode == GlobalTypes.ESystemModes.Online)
            {
                MonitoringReplayTimeCounter = MonitoringReplayTimeCounter.AddSeconds(secs);
                lock(m_lockTimer)
                    LayoutManager.Instance.SetEventValues(MonitoringReplayTimeCounter);
            }

			//Auto Mute/UnMute
			if (IsUnfreezMute && LayoutManager.SystemMode == GlobalTypes.ESystemModes.Online)
			{
				if (IsMute)
					setVolume(m_freezeVolume);	

				if (SystemVolume <= m_freezeVolume)
				{
					var vol = SystemVolume + m_freezeVolumeIncrease;
					if (vol >= m_freezeVolume)
					{
						vol						= m_freezeVolume;
						m_freezeVolumeIncrease	= 0;
					}

					setVolume(vol);
				}
				else
					m_freezeVolumeIncrease = 0;
			}

			// Update timer text
	        lock (m_lockTimer)
            {
                m_timeStr = DateTime.Now.ToString(FMT_DATE_TIME);
                setCaptionText();
            }
        }

        public BarItems GetStudyMenuItems()
        {
            return parentBarItemStudies.Items;
        }

        private void barItemSetupStudies_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemSetupStudies", Name);
	        OpenSetupStudiesDialog();
        }

		public void OpenSetupStudiesDialog(Form owner = null)
		{
			if (IsClosing)    
				return;

			if (owner == null)
				owner = this;

			using (var dlg = new SetupForm(false))
			{
				dlg.TopMost = true;
				dlg.ShowDialog(owner);
				owner.Focus();
			}

			//Added by Natalie - 11/06/2008 - refresh study
			loadSelectedStudy(CurrentStudyId); 
		}

        private void onClosing()
        {
            Logger.LogTrace("onClosing(): START", Name);

            HardDriveSpaceKeeper.DeleteUnusedExamData();       

            try
            {
                bvListViewCtrl1.Save(false);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }

            RimedDal.Instance.ConfigDS.tb_BasicConfiguration[0].LastStudy = CurrentStudyId;
            EventListViewCtrl.Instance.SaveEventHistory();
            RimedDal.Instance.UpdateDBWithDS();

            SummaryScreen.CloseDialog();

            Logger.LogTrace("onClosing(): END", Name);
        }

        private bool screenCapture(Stream ms)
        {
            using (var clipBitmap = new Bitmap(Bounds.Width, Bounds.Height))
            {
                using (var grp = Graphics.FromImage(clipBitmap))
                {
                    grp.CopyFromScreen(0, 0, 0, 0, clipBitmap.Size);
                    clipBitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                }
            }

            return true;
        }

        private void parentBarItemFunctions_BeforePopup(object sender, CancelEventArgs e)
        {
            // Hide all the child item in the function menu.
            foreach (BarItem item in parentBarItemFunctions.Items)
            {
                item.Visible = false;
            }

            // get the ID of the items to display.
            string[] itemsToDisplay;
            if (LayoutManager.Instance.ReplayMode)
                itemsToDisplay = FunctionMenuMgr.theFunctionMgr.GetItems(GlobalTypes.ETooolBarType.TB6_ReplayFreeze);
            else if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Online && LayoutManager.Instance.IsStudyDiagnostic())
                itemsToDisplay = FunctionMenuMgr.theFunctionMgr.GetItems(GlobalTypes.ETooolBarType.TB1_DiagnosticUnfreeze);
            else if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Offline && LayoutManager.Instance.IsStudyDiagnostic())
                itemsToDisplay = FunctionMenuMgr.theFunctionMgr.GetItems(GlobalTypes.ETooolBarType.TB2_DiagnosticFreeze);
            else if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Online && (!LayoutManager.Instance.IsStudyDiagnostic()))
                itemsToDisplay = FunctionMenuMgr.theFunctionMgr.GetItems(GlobalTypes.ETooolBarType.TB4_MonitoringUnfreeze);
            else if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Offline && (!LayoutManager.Instance.IsStudyDiagnostic()))
                itemsToDisplay = FunctionMenuMgr.theFunctionMgr.GetItems(GlobalTypes.ETooolBarType.TB5_MonitoringFreeze);
            else
                itemsToDisplay = FunctionMenuMgr.theFunctionMgr.GetItems(GlobalTypes.ETooolBarType.TB3_Summary);

            // display the items on the function menu.
            foreach (var str in itemsToDisplay)
            {
                var barItem = mainFrameBarManager1.Items.FindItem(str);
                if (barItem != null)
                    barItem.Visible = true;
            }
			
            enableFeatures();
        }

		private void barItemHelpAbout_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemHelpAbout", Name);
	        OpenHelpAboutDialog();
        }

		public void OpenHelpAboutDialog(Form owner = null)
		{
			if (IsClosing)    
				return;

			if (owner == null)
				owner = this;

			using (var dlg = new AboutDlg())
			{
				dlg.TopMost = true;
				dlg.ShowDialog(owner);
				owner.Focus();
			}
		}

		private void serialFunction1()
        {
            Logger.LogTrace("SerialFunction1()", Name);
            applyNextFunction();
        }

        private void serialFunction2()
        {
            Logger.LogTrace("SerialFunction2()", Name);
        }

        private void serialFunction4()
        {
            Logger.LogTrace("SerialFunction4() / mute()", Name);
            mute();
        }

        private void serialEsc()
        {
            Logger.LogTrace("SerialEsc()", Name);
            if (LayoutManager.Instance.CursorsMode)
                LayoutManager.Instance.CursorsMode = false;
        }

        private void barItemCursors_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemCursors", Name);
            if (!LayoutManager.Instance.ExaminationDirtyFlag && !LayoutManager.Instance.ReplayMode)
                return;

            LayoutManager.Instance.CursorsMode = !LayoutManager.Instance.CursorsMode;
        }

        private void freeze()
        {
        }
		 
        private void unFreeze()
        {
			  if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Offline) 
				  doSpaceBarWork();
        }
		 
        private bool nextBv()
        {
            return nextBvNotification();
        }

        private void print()
        {
            //Assaf (07/12/06)
            printScreenRep(false);
        }

        private void barItemBackup_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemBackup", Name);
	        OpenBackupDialog();
        }

		public void OpenBackupDialog(Form owner = null)
		{
			if (IsClosing)    
				return;

			if (owner == null)
				owner = this;

			using (var dlg = new Backup())
			{
				dlg.TopMost = true;
				dlg.ShowDialog(owner);
				owner.Focus();
			}
		}

        private delegate void SerialOperationDelegate(byte opCode);

        private void serialOperation(byte opCode)
        {
            if (IsClosing)    
                return;

            if (InvokeRequired)
            {
				//DO NOT USE BeginInvoke - Operation must be synch.
                Invoke(new SerialOperationDelegate(serialOperation), opCode); //Ofer, v1.18.2.9: Fix ESystem.InvalidOperationException Message: Object is currently in use elsewhere. 
                return;
            }

            var remoteKey = (SerialPortWrapper.EKeys)opCode;
            LoggerUserActions.MouseClick(string.Format("SerialOperation: '{0}'", remoteKey), Name);

            switch (remoteKey)
            {
                case SerialPortWrapper.EKeys.DEPTH_UP:
                    DepthUp();
                    break;
                case SerialPortWrapper.EKeys.DEPTH_DOWN:
                    DepthDown();
                    break;
                case SerialPortWrapper.EKeys.GAIN_DOWN:
                    GainDown();
                    break;
                case SerialPortWrapper.EKeys.GAIN_UP:
                    GainUp();
                    break;
                case SerialPortWrapper.EKeys.RANGE_DOWN:
                    RangeDown();
                    break;
                case SerialPortWrapper.EKeys.RANGE_UP:
                    RangeUp();
                    break;
                case SerialPortWrapper.EKeys.POWER_DOWN:
                    PowerDown();
                    break;
                case SerialPortWrapper.EKeys.POWER_UP:
                    PowerUp();
                    break;
                case SerialPortWrapper.EKeys.WIDTH_DOWN:
                    WidthDown();
                    break;
                case SerialPortWrapper.EKeys.WIDTH_UP:
                    WidthUp();
                    break;
                case SerialPortWrapper.EKeys.ZEROLINE_DOWN:
                    zeroLineDown();
                    break;
                case SerialPortWrapper.EKeys.ZEROLINE_UP:
                    zeroLineUp();
                    break;
                case SerialPortWrapper.EKeys.FREEZE:
                    doSpaceBarWork();
                    break;

                case SerialPortWrapper.EKeys.HITS:
                        if (GlobalSettings.AMhits)
                            ToggleHits();
                    break;
                case SerialPortWrapper.EKeys.VOL_DOWN:
                    VolumeDown();
                    break;
                case SerialPortWrapper.EKeys.VOL_UP:
                    VolumeUp();
                    break;
                case SerialPortWrapper.EKeys.FUNCTION_1:
                    serialFunction1();
                    break;
                case SerialPortWrapper.EKeys.FUNCTION_2:
                    serialFunction2();
                    break;
                case SerialPortWrapper.EKeys.FUNCTION_3:
                    var curGv   = LayoutManager.Instance.CurrentGateView;
                    
                    int currentPower;
                    if (curGv != null)
	                    currentPower = curGv.Probe.Power;
                    else
                    {
						var gv = LayoutManager.Instance.SelectedGv;
						currentPower = gv.Probe.Power;
	                    if (gv == null)
		                    currentPower = Probe.MAX_NORMAL_POWER;
                    }

                    if (currentPower <= Probe.MAX_NORMAL_POWER)
                        powerTurboUp();
                    else
                        powerTurboDown();
                    break;
                case SerialPortWrapper.EKeys.FUNCTION_4:
                    serialFunction4();
                    break;
                case SerialPortWrapper.EKeys.ESC:
                    serialEsc();
                    break;

                case SerialPortWrapper.EKeys.CHANNEL:	// selected gate.
                    LayoutManager.Instance.ChangeSelectedGV();
                    break;
                case SerialPortWrapper.EKeys.PROBE:
                    if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Offline)
                        changeCurrentProbe();
                    break;
                case SerialPortWrapper.EKeys.RETURN:
                    enterKeyDown();
                    break;
                case SerialPortWrapper.EKeys.RVER:	// flow direction.
                    LayoutManager.Instance.ChangeFlowDirection();
                    break;

                default:
                    break;
            }
        }

        private void buttonFreezeToggle_MouseDown(object sender, MouseEventArgs e)
        {
            LoggerUserActions.MouseClick("buttonFreezeToggle", Name);
			doSpaceBarWork();
        }

        private bool nextBvNotification()
        {
            if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Online)
                doSpaceBarWork();

            if (NextBVEvent != null)
            {
				using (new CursorSafeChange(this, true))
					return NextBVEvent();
            }

            return false;
        }

        public void CurrentBvNotification()
        {
            if (CurrentBVEvent == null)
                return;
                
            if (LayoutManager.SystemMode != GlobalTypes.ESystemModes.Online)
            {
				using (new CursorSafeChange(this, true))
					CurrentBVEvent();
            }
        }

        private void prevBvNotification()
        {
            if (PrevBVEvent == null)
                return;

            if (LayoutManager.SystemMode != GlobalTypes.ESystemModes.Online)
            {
				using (new CursorSafeChange(this, true))
					PrevBVEvent();
            }
        }

        private void barItemNextBV_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemNextBV", Name);
            nextBvNotification();
        }

        private void barItemPrevBV_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemPrevBV", Name);
            prevBvNotification();
        }

        private void clearTrends()
        {
            Logger.LogTrace("ClearTrends()", Name);
            foreach (TrendChart trend in LayoutManager.Instance.Trends)
            {
                trend.ClearTrendChart();
            }
        }

        private void buttonPlay_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("buttonPlay", Name);

            //RIMD-552: Trend window is not updated (event jumping works incorrect)
            LayoutManager.Instance.CursorsMode = false;

            toggleFreezeMode();

			guiMgrSmGo(new CGuiSmEvent(EGuiMgrSmEvent.GUI_TOGGLE_FREEZE_EVENT));	/*Marina Ignore the button if in pause replay*/
            if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.MonitoringLoad)
            {
                HitsHistogram.Reset();
				toolBarPanel1.ButtonPlayCtrl.Enabled = false;
            }
        }

        private void buttonPause_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("buttonPause", Name);
			doPauseAction();
        }

        private bool doPauseAction()
        {
			var bRet = false;
			
			//Marina we are here from DoSpaceBarWork only for if (LayoutManager.SystemLayoutMode == GlobalTypes.TSystemLayoutMode.MonitoringLoad)
            //Assaf : By now there is no implementation for pausing (and scrolling) in diagnostic 
            if (LayoutManager.SystemLayoutMode != GlobalTypes.ESystemLayoutMode.Monitoring && LayoutManager.SystemLayoutMode != GlobalTypes.ESystemLayoutMode.Diagnostic && LayoutManager.SystemLayoutMode != GlobalTypes.ESystemLayoutMode.DiagnosticLoad)
                LayoutManager.Instance.IsPaused = (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Online);

            if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.MonitoringLoad)
            {
                if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Online)
                {
                    toolBarPanel1.ButtonPause.IsPressed = true;
                    bRet = true;
                }
                else
                {
                    toolBarPanel1.ButtonPause.IsPressed = false;
					if (toolBarPanel1.ButtonPlayCtrl.Enabled)
                        return false;

                    bRet = true;
                }
            }
            else if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Monitoring && LayoutManager.SystemMode == GlobalTypes.ESystemModes.Online && IsMonitoringSaved)
            {
                if (IsRecordMode)
                {
                    IsRecordMode = false;
                    m_recordPaused = true;
					DspRecordMode=false;
                    toolBarPanel1.ButtonPause.IsPressed = true;
                }
                else
                {
                    IsRecordMode = true;
                    m_recordPaused = false;
					DspRecordMode=false;
                    toolBarPanel1.ButtonPause.IsPressed = false;
                }
            }
			else if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Monitoring && LayoutManager.SystemMode == GlobalTypes.ESystemModes.Offline) 
            {
			}
			else 
            {
				bRet = true;
			}

			if (bRet)
			{
				toggleFreezeMode();
				guiMgrSmGo(new CGuiSmEvent(EGuiMgrSmEvent.GUI_TOGGLE_FREEZE_EVENT));
			}

            return bRet;
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("buttonStopRecording", Name);
            toolBarPanel1.ButtonStartRecording.IsPressed = !stop();
        }

        private void updateExaminationDateTime()
        {
			var examRow = RimedDal.Instance.LoadedExamination();
			if (examRow == null)
				return;

			var dt		= DateTime.Now;
            var rowArr	= new object[13];
			rowArr[0]	= examRow.ItemArray[0];
			rowArr[1]	= examRow.ItemArray[1];
			rowArr[2]	= examRow.ItemArray[2];
			rowArr[3]	= examRow.ItemArray[3];
			rowArr[4]	= examRow.ItemArray[4];
			rowArr[5]	= dt;
			rowArr[6]	= examRow.ItemArray[6];
			rowArr[7]	= dt.Hour;
			rowArr[8]	= dt.Minute;
			rowArr[9]	= examRow.ItemArray[9];
			rowArr[10]	= examRow.ItemArray[10];
			rowArr[11]	= examRow.ItemArray[11];
            //rowArr[12]  = CurrentAccessionNumber;
 
			examRow.ItemArray = rowArr;
			RimedDal.Instance.UpdateExamination();
			EventListViewCtrl.Instance.ExaminationDate = dt;

			//var t = DateTime.Now;
			//var myArray = new object[12];
			//myArray[0] = RimedDal.Instance.ExaminationsDS.tb_Examination.Rows[0].ItemArray[0];
			//myArray[1] = RimedDal.Instance.ExaminationsDS.tb_Examination.Rows[0].ItemArray[1];
			//myArray[2] = RimedDal.Instance.ExaminationsDS.tb_Examination.Rows[0].ItemArray[2];
			//myArray[3] = RimedDal.Instance.ExaminationsDS.tb_Examination.Rows[0].ItemArray[3];
			//myArray[4] = RimedDal.Instance.ExaminationsDS.tb_Examination.Rows[0].ItemArray[4];
			//myArray[5] = (object)t;
			//myArray[6] = RimedDal.Instance.ExaminationsDS.tb_Examination.Rows[0].ItemArray[6];
			//myArray[7] = t.Hour;
			//myArray[8] = t.Minute;
			//myArray[9] = RimedDal.Instance.ExaminationsDS.tb_Examination.Rows[0].ItemArray[9];
			//myArray[10] = RimedDal.Instance.ExaminationsDS.tb_Examination.Rows[0].ItemArray[10];
			//myArray[11] = RimedDal.Instance.ExaminationsDS.tb_Examination.Rows[0].ItemArray[11];
			//RimedDal.Instance.ExaminationsDS.tb_Examination.Rows[0].ItemArray = myArray;

            //RimedDal.Instance.UpdateExamination();
            //EventListViewCtrl.Instance.ExaminationDate = t;
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("ButtonStartRecording", Name);
            startRecord();
        }

        private bool startRecord()
        {
            if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Offline || AtTheEndOfMonitoringExam)
                return false;

            if (IsRecordMode || m_recordPaused)
                return false;

			var examRow = RimedDal.Instance.LoadedExamination();
			if (examRow == null)
				return false;

            bvListViewCtrl1.SaveMonitoringExam();
            IsRecordMode = true;
            IsMonitoringSaved = true;
            DspRecordMode = true;
            if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Monitoring)
            {
                if (!Directory.Exists(Constants.RAW_DATA_PATH + RimedDal.Instance.ExaminationsDS.tb_Examination.Rows[RimedDal.Instance.ExaminationsDS.tb_Examination.Rows.Count - 1].ItemArray[0]))
                {
                    updateExaminationDateTime();
					var dirName = Constants.RAW_DATA_PATH + examRow.ItemArray[0];	//TODO: replace with examRow.Examination_Index
                    Directory.CreateDirectory(dirName);
                    DspManager.Instance.DspMgrRecordStart(dirName);
                }
            }

            if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Diagnostic)
            {
                var guid = Guid.NewGuid();
                var dirName = Constants.RAW_DATA_PATH + guid;
                while (Directory.Exists(dirName))
                {
                    guid = Guid.NewGuid();
                    dirName = Constants.RAW_DATA_PATH + guid;
                }

                DspManager.Instance.DspMgrRecordStart(dirName);
            }
            return true;
        }


        private bool stop()
        {
            if (LayoutManager.SystemLayoutMode != GlobalTypes.ESystemLayoutMode.Monitoring)
            {
                toggleFreezeMode();                                                
                guiMgrSmGo(new CGuiSmEvent(EGuiMgrSmEvent.GUI_TOGGLE_FREEZE_EVENT));

                return true;
            }

            Logger.LogTrace(Logger.ETraceLevel.L5, Name, "stop()");


            //
            // NONITORING MODE
            //
            if (!IsRecordMode)
                return true;

            bool removeExamination = false;

            if (IsRecordMode || m_recordPaused)
            {
                DialogResult res;
                // Added by Alex 04/04/2016 fixed bug #NN v. 2.2.3.28
                IsMonitoringStoped = false;

                using (var dlg = new AskRecordDlg())
                {
					dlg.TopMost = true;	//Ofer, 2014.01.05
					res = dlg.ShowDialog(this);
					Focus();
				}

                //Continue recording
                if (res == DialogResult.Ignore)
                    return false;
                    
                //Delete examination files
                if (res == DialogResult.Cancel)
                    removeExamination = true;
                else // Added by Alex 04/04/2016 fixed bug #NN v. 2.2.3.28
                    IsMonitoringStoped = true;
            }

            //Save examination
            IsRecordMode = false;

            //ATTENTION: Add call to invocation list - method executed after execution of this method is completed
            toolBarPanel1.BeginInvoke((Action)delegate()
                {
                    toolBarPanel1.ButtonStartRecording.IsPressed = false;
                    toolBarPanel1.ButtonPause.IsPressed = false;
                    toolBarPanel1.ButtonFreeze.IsPressed = false;
                });

            m_recordPaused = false;
            AtTheEndOfMonitoringExam = true;

			if ((LayoutManager.SystemMode == GlobalTypes.ESystemModes.Online))
				toggleFreezeMode(); 
			
			if (removeExamination)
			{
				EventListViewCtrl.Instance.SaveEventHistory();			//Bug #446, Workaround - Save events to DB to enable images removal at the end of the method.
				EventListViewCtrl.Instance.ClearEventsHistoryList();	

                guiMgrSmGo(new CGuiSmEvent(EGuiMgrSmEvent.GUI_FINISH_STUDY_REMOVE_STORAGE_EVENT));
			}
            else
            {
				guiMgrSmGo(new CGuiSmEvent(EGuiMgrSmEvent.GUI_FINISH_STUDY_SAVE_STORAGE_EVENT));

                RimedDal.Instance.UpdateCurExam2DB();	
                EventListViewCtrl.Instance.SaveEventHistory();

                //Save the Examination trends data zzzzz
                foreach (var trend in LayoutManager.Instance.Trends)
                {
                    trend.SaveTrendPicture();
                }
				
				var isSaved = false;
                var patientName = m_patientName.Replace('/', '_').Replace(':', '_').Replace('*', '_').Replace('?', '_').Replace('"', '_').Replace('<', '_').Replace('>', '_').Replace('|', '_').Trim();

				var row = RimedDal.Instance.LoadedExamination();	//= RimedDal.Instance.ExaminationsDS.tb_Examination.Rows[0] as dsExamination.tb_ExaminationRow;
				if (row != null)
	            {
					var filename = string.Format("{0}{1}-{2} ({3}).CSV",Constants.TRENDS_PATH, patientName, DateTime.Now.ToString("yyyy-MM-dd"), row.Examination_Index);

                	if (IsUnilateralStudy)
	                	isSaved = ExportManager.SaveToFile(LayoutManager.Instance.Trends[0].ExportDataPackage, filename);
	                else if (IsBilateralStudy)
		                isSaved = ExportManager.SaveToFile(LayoutManager.Instance.Trends[0].ExportDataPackage, LayoutManager.Instance.Trends[1].ExportDataPackage, filename);


                    // Added by Alex 03/04/2016 fixed bug #841 v. 2.2.3.27
                    if (string.IsNullOrEmpty(row.AccessionNumber))
                    {
                        row.AccessionNumber = CurrentAccessionNumber;
                        RimedDal.Instance.UpdateExamination(row, "AccessionNumber", CurrentAccessionNumber);
                        CurrentAccessionNumber = "";
                    }
                }


                if (!isSaved)
					LoggedDialog.ShowNotification(this, "Fail to write examination trends data CSV file.");
			}

            EnableControls();
            if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Offline)
				toolBarPanel1.ShowToolbar(GlobalTypes.ETooolBarType.TB5_MonitoringFreeze);

            IsMonitoringSaved = false;
            MonitoringReplayTimeCounter = (DateTime)RimedDal.Instance.ExaminationsDS.tb_Examination.Rows[0].ItemArray[5];  



            if (removeExamination)
            {
                RimedDal.Instance.DeleteCurrentExamination();

                //RIMD-549: Notes are not saved for Monitoring examination
                bvListViewCtrl1.ExaminationWasSaved = false;
            }

            // added by Alex 08/04/2015 fixed bug #619 v. 2.2.2.29
            // changed by Alex 12/04/2015 fixed bug #747 v. 2.2.2.30
            //bvListViewCtrl1.DontLoadExamination = true;

            return true;
        }

        private void barItemPlay_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemPlay", Name);
            if (LayoutManager.SystemLayoutMode != GlobalTypes.ESystemLayoutMode.MonitoringLoad)
            {
                toggleFreezeMode();
				guiMgrSmGo(new CGuiSmEvent(EGuiMgrSmEvent.GUI_BAR_ITEM_PLAY_EVENT));
            }
        }

        private void buttonNextFunction_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("ButtonNextFunction", Name);
            applyNextFunction(); 
        }

        private void barItemStop_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemStop", Name);
            toolBarPanel1.ButtonStartRecording.IsPressed = !stop();
        }

        private void barItemRecord_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemRecord", Name);
            toolBarPanel1.ButtonStartRecording.IsPressed = startRecord();
        }

        public void EnableControls()
        {
            if (InvokeRequired)
            {
                BeginInvoke((Action)EnableControls);
                return;
            }

            if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Online)
            {
                parentBarItemPatient.Enabled = false;
                parentBarItemStudies.Enabled = false;
                parentBarItemSetup.Enabled = false;
                parentBarItemUtilities.Enabled = false;
                parentBarItemHelp.Enabled = false;
                bvListViewCtrl1.Enabled = false;
	            
                barItemSummaryScreen.Enabled = false;
                barItemAccessionNumber.Enabled = false;
                
			}
            else if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Offline)
            {
                parentBarItemPatient.Enabled = true;
                parentBarItemStudies.Enabled = true;
                parentBarItemSetup.Enabled = true;
                parentBarItemUtilities.Enabled = true;
                parentBarItemHelp.Enabled = true;

				//if (LayoutManager.SystemLayoutMode != GlobalTypes.ESystemLayoutMode.Monitoring)
				bvListViewCtrl1.Enabled = true;		//Bug #391

                barItemSummaryScreen.Enabled = true;
                barItemAccessionNumber.Enabled = true;
            }
        }

		public void AddDebugHitEvent(int probSide, double elapseMSec)
        {
			if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Monitoring && IsRecordMode)
				DspManager.Instance.DspMgrAddHistoryEvent(SingleHistoryEvent.HIT_PREFIX + " [Debug]", GlobalTypes.EEventType.HitEvent, false, probSide, elapseMSec);
		}

        private void loadUndefinedPatient()
        {
            var row = RimedDal.Instance.GetPatientByIndex(Guid.Empty);

            if (row == null)
            {
                var r           = RimedDal.Instance.PatientsDS.tb_Patient.Newtb_PatientRow();
                r.Patient_Index = Guid.Empty;
                r.First_Name    = "Undefined";
                r.Last_Name     = string.Empty;
                r.Birth_date    = DateTime.Today;
                r.ID            = GlobalSettings.UNDEFINED_ID;

                RimedDal.Instance.PatientsDS.tb_Patient.Addtb_PatientRow(r);
            }

            m_selectedPatientIndex = Guid.Empty;
            m_patientName           = "Undefined";
        }

        private void enableFeatures()
        {
            // Authorization manager.
            barItemAutoscan.Visible = GlobalSettings.AMautoscan;
			barItemHitsDetection.Visible	= GlobalSettings.AMhits && !AppSettings.Active.IsReviewStation; ;
            if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.MonitoringLoad && CurrentStudyName.StartsWith(Constants.StudyType.STUDY_MONITORING_INTRACRANIAL))
            {
                barItemAddEvent.Enabled = true;
                barItemDeleteEvent.Enabled = (EventListViewCtrl.Instance.SelectedEvent != null);
            }
            else
            {
                barItemAddEvent.Enabled = false;
                barItemDeleteEvent.Enabled = false;
            }
        }

        private void barItemAuthorizationMgr_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemAuthorizationMgr", Name);

			var prev = s_windowsSpecialKeysWatcher.EnableKeys(true); 
			using (var dlg = new AuthorizationMgrDlg())
            {
				dlg.TopMost = true;	
				dlg.ShowDialog(this);
				Focus();
				if (dlg.OkButtonPressed)
                {
                    LoggedDialog.ShowInformation(this, "Authorization completed.\n\n" + StringManager.GetString("Please, reopen the application"), "Authorization");
                    ExitDigiLite(); 
                }
                else
                {
					LoggedDialog.ShowWarning(this, "Authorization FAIL.\n\nSystem continue to work in DEMO/Read-Only mode.", "Authorization");
                }
            }

			s_windowsSpecialKeysWatcher.EnableKeys(prev); 
		}

        private GlobalTypes.EUserPremission m_userPremission = GlobalTypes.EUserPremission.User;

        private void userManager()
        {
            Logger.LogTrace("UserManager()", Name);
            if (m_userPremission == GlobalTypes.EUserPremission.Operator)
            {
                m_userPremission = GlobalTypes.EUserPremission.User;
                updateUserPremission();

                // give notification when regular user was activate.
                LoggedDialog.Show(this, StringManager.GetString("Regular user was activate."));
            }
            else
            {
                using (var dlg = new UserMgrDlg())
                {
					dlg.TopMost = true;	
	                var res = dlg.ShowDialog(this);
					Focus();
					if (res == DialogResult.OK)
                    {
                        m_userPremission = GlobalTypes.EUserPremission.Operator;
                        updateUserPremission();
                    }
                }
            }
        }

        private void updateUserPremission()
        {
            parentBarItemUtilities.Visible = (m_userPremission == GlobalTypes.EUserPremission.Operator);
        }

        private void barItemPatientReportWizard_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemPatientReportWizard", Name);
	        OpenPatientReportWizard();
        }

		public void OpenPatientReportWizard(Form owner = null)
		{
			if (IsClosing)    
				return;

			if (owner == null)
				owner = this;

			using (var dlg = new ReportWizard(owner))	//Fix bug #411: Report Generator Wizard - Application crush
			{
				dlg.TopMost = true;
				dlg.ShowDialog(owner);
				owner.Focus();
			}
		}

		public void ResizePanelsSize()
        {
            if (InvokeRequired)
            {
                BeginInvoke((Action)ResizePanelsSize);
                return;
            }

            var size = panel0.Parent.Width;
            panel0.Width    = panel1.Width = size / 2;
            panel1.Location = new Point(size + 1, panel1.Height);
        }

		private void panelMiddle_Resize(object sender, EventArgs e)
		{
			SetSpectrumSidePanelsWidth();
		}

        private void dockingManager1_DockStateChanged(object sender, Syncfusion.Windows.Forms.Tools.DockStateChangeEventArgs arg)
        {
            dockingManager1.DisallowFloating = true;

            if (dockingManager1.ActiveControl == panelHits)
                dockingManager1.ActivateControl(panelBV);

            if ((LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.MonitoringLoad) || (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Monitoring))
            {
                if (LayoutManager.Instance.TrendToShowCount > 0)
                    LayoutManager.Instance.Trends[0].Width = 830;
            }

            // this is work around in order to change the autoscan chart's size in case of docking the docking windows...
            if (LayoutManager.Instance.Autoscans[Probe.PROB_SIDE_LEFT] != null)
                LayoutManager.Instance.Autoscans[Probe.PROB_SIDE_LEFT].CalcSize();

            var chart = LayoutManager.Instance.Autoscans[Probe.PROB_SIDE_RIGHT];
            if (chart != null && chart.Display && chart.Visible)
                chart.CalcSize();
        }

        private void barItemExitWindows_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemExitWindows", Name);
            ExitWindow();
        }

        private void barItemPatientDelete_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemPatientDelete", Name);
	        OpenDeletePatientDeleteDlg();
        }

		public void OpenDeletePatientDeleteDlg(Form owner = null)
		{
			if (IsClosing)
				return;

			if (owner == null)
				owner = this;
			using (var dlg = new PatientDeleteDlg())
			{
				dlg.TopMost = true;
				dlg.ShowDialog(owner);
				owner.Focus();
			}
		}
        private void barItemZipRawDataFiles_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemZipRawDataFiles", Name);
            using (var dlg = new PackRawdataFilesDlg())
            {
				dlg.TopMost = true;	
				dlg.ShowDialog(this);
				Focus();
			}
        }

        private void barItemRestore_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemRestore", Name);
	        OpenRestoreDialog();
        }

		public void OpenRestoreDialog(Form owner = null)
		{
			if (IsClosing)    
				return;

			if (owner == null)
				owner = this;
			using (var dlg = new Restore())
			{
				dlg.TopMost = true;
				dlg.ShowDialog(owner);
				owner.Focus();
			}
		}

        public void DisplayPatientRep(Guid patientIndex, Guid examIndex, bool saveExam)
        {
            Logger.LogTrace(Logger.ETraceLevel.L5, Name, "DisplayPatientRep(patientIndex={0}, examIndex={1}, saveExam={2})", patientIndex, examIndex, saveExam);

            Logger.SystemInfoLog("Start Final Patient Report");

            if (! DisplayPatientReport)
                return;

            try
            {
                var dsExam      = new dsExamination();
                var dsGateExam  = new dsGateExamination();
				if (!saveExam && LayoutManager.SystemLayoutMode != GlobalTypes.ESystemLayoutMode.DiagnosticLoad) //if (!saveExam || (AppSettings.Active.DiagnosticPostSaveEdit && LayoutManager.IsDiagnosticMode))	
				{
                    if (RimedDal.Instance.GateExaminationsDS.tb_GateExamination.Rows.Count == 0)
                    {
                        LoggedDialog.ShowError(this, StringManager.GetString("There is no examination to display"));
                        return;
                    }

                    //v2.00.04.020 BugFix# 093
                    if (m_displayWarningReportFirstTime && DialogResult.Yes != LoggedDialog.ShowNotification(this, "Examination was NOT saved.\n\nGenerate report for the unsaved examination?", "ATTENTION", MessageBoxButtons.YesNo))
                        return;

                    m_displayWarningReportFirstTime = false;
                }

                var selCmd = "SELECT tb_Examination.* FROM tb_Examination WHERE Patient_Index='" + patientIndex + "'";
                RimedDal.Instance.GetExamination(dsExam, selCmd);

                //Added by Natalie for RIMD-286: Incorrect order of BVs in Report after Save
                selCmd = "Select tb_GateExamination.* FROM tb_GateExamination WHERE (ExaminationIndex = {guid {" + examIndex + "}}) AND InSummaryScreen = TRUE ORDER BY OnSummaryScreenIndex ASC";// ORDER by StartTime DESC";
                RimedDal.Instance.GetGateExam(dsGateExam, selCmd);
                
                // Hospital data view.
                var dvHospital  = new DataView(RimedDal.Instance.ConfigDS.tb_HospitalDetails);

                // patient data view.
                var dvPatient   = new DataView(RimedDal.Instance.PatientsDS.tb_Patient, "Patient_Index = '" + patientIndex + "'", null, DataViewRowState.CurrentRows);	//Merge, Ofer

                var report      = new PatientReport(examIndex, dvHospital, dvPatient, dsExam, dsGateExam);

				report.Display(true);
            }
            catch (Exception ex)
            {
                LoggedDialog.ShowError(this, StringManager.GetString("Can not display the selected report"),exception: ex);
            }
        }

        private void barItemPatientPrintPreview_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemPatientPrintPreview", Name);
            printScreenRep(true);
        }

        private void barItemPrint_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemPrint", Name);
            printScreenRep(false);
        }

        private void barItemExportSelectedSpectrum_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemExportSelectedSpectrum", Name);
			DumpFullScreenImage();
        }

        private void barItemExportFullScreen_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemExportFullScreen", Name);
            DumpFullScreenImage();
        }

        // Changed by Alex bug #794 v.2.2.3.21 31/01/2016
        public void DumpFullScreenImage(bool isSummaryScreen = false)
		{
			try
			{
				using (var sf = new SaveFileDialog())
				{
					sf.DefaultExt = "jpg";
					sf.Filter = "jpg files (*.jpg)|*.jpg|All files (*.*)|*.*";
					sf.FilterIndex = 1;
					if ((LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.MonitoringLoad) || (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Monitoring))
						sf.FileName = m_patientName + DateTime.Now.ToString("_HH_mm_ss") + ".jpg";
					else
						sf.FileName = m_patientName + ".jpg";

					sf.RestoreDirectory = true;

					var res = sf.ShowDialog(this);
					Focus();
					if (res == DialogResult.OK)
					{
                        // Changed by Alex bug #794 v.2.2.3.21 31/01/2016
                        Image img;
                        if (isSummaryScreen)
                            img = HelperMethods.CaptureControlSnapshot(ActiveForm);
                        else
						    img = HelperMethods.CaptureControlSnapshot(this);
						if (img == null)
						{
							LoggedDialog.ShowNotification(this, "Full screen image dump fail.");
							return;
						}

						using (img)
							Files.ImageSave(img, sf.FileName);
					}
				}
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);
			}
		}

		public void DumpSelectedGateViewImage()
		{
			var gv = LayoutManager.Instance.SelectedGv;
			if (gv == null)
			{
				Logger.LogWarning("No selected GateView. {0}.selSpectrumCaptureThreadStart() aborted.", Name);
				return;
			}

			try
			{
				using (var sf = new SaveFileDialog())
				{
					sf.DefaultExt = "jpg";
					sf.Filter = "jpg files (*.jpg)|*.jpg|All files (*.*)|*.*";
					sf.FilterIndex = 1;
					if ((LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.MonitoringLoad) || (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Monitoring))
						sf.FileName = m_patientName + "_" + gv.DopplerBar.BVName + DateTime.Now.ToString("_HH_mm_ss") + ".jpg";
					else
						sf.FileName = m_patientName + "_" + gv.DopplerBar.BVName + ".jpg";
					sf.RestoreDirectory = true;

					var res = sf.ShowDialog(this);
					Focus();
					if (res == DialogResult.OK)
					{
						Image img = HelperMethods.CaptureControlSnapshot(gv);
						if (img == null)
						{
							LoggedDialog.ShowNotification(this, "Selected gateView image dump fail.");
							return;
						}

						using (img)
							Files.ImageSave(img, sf.FileName);
					}
				}
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);
			}
		}

        private void deleteStudyEvent()
        {
            Logger.LogTrace("deleteStudyEvent()", Name);
            if (InvokeRequired)
			{
				Invoke(new Action(deleteStudyEvent));	//Merge, Nadiia (Override: BeginInvoke)
				return;
			}

            updateStudiesMenus();	
        }

        private void changeCurrentProbe()
        {
            Logger.LogTrace("changeCurrentProbe()", Name);
            toolBarPanel1.ComboBoxProbes.ChangeCurrentProbe();
        }

        private void enterKeyDown()
        {
            Logger.LogTrace("enterKeyDown()", Name);
            if (InvokeRequired)
            {
                Invoke(new Action(enterKeyDown));	//Merge, Nadiia (Override: BeginInvoke)
                return;
            }

            var e = new KeyEventArgs(Keys.Enter);
            mainForm_KeyDown(this, e);
        }

        public int GetBVThreshold()
        {
            if (LayoutManager.Instance != null)
            {
                var gv = (LayoutManager.Instance.CurrentGateView == null) ? LayoutManager.Instance.SelectedGv : LayoutManager.Instance.CurrentGateView;

                return (gv != null && gv.Probe != null) ? gv.Probe.HitsThreshold : 0;
            }

            return 0;
        }


        private readonly object m_pwrDnThreadLock = new object();
        private DateTime m_monitoringReplayStartTime;


        private void startTurboAction()
        {
            lock (m_pwrDnThreadLock)
            {
                turboActionAbort();

                m_pwrDnThread               = new Thread(pwrDnThreadStart);
                m_pwrDnThread.IsBackground  = true;
                m_pwrDnThread.Priority      = ThreadPriority.Normal;
                m_pwrDnThread.SetName("PowerDn");
                m_pwrDnThread.Start();
            }
        }

        public void StopTurboAction()
        {
            Logger.LogTrace("StopTurboAction()", Name);
            lock (m_pwrDnThreadLock)
            {
                if (m_pwrDnThread == null)
                    return;

                turboActionAbort();
            }

            powerTurboDown();

            //P.U. fix (after Freeze we have to write MaxNormalPower to current Probe and reset DopplerFlag to zero)
            if (!AppSettings.Active.IsDspOffMode)
            {
                if (LayoutManager.Instance.SelectedGv != null)
                    LayoutManager.Instance.SelectedGv.Probe.Power = Probe.MAX_NORMAL_POWER;
                else
                    LayoutManager.Instance.CurrentGateView.Probe.Power = Probe.MAX_NORMAL_POWER;

                LayoutManager.Instance.SetDopplerFlagToZero();
            }
        }

        private void turboActionAbort()
        {
            Logger.LogTrace("turboActionAbort()", Name);
            if (m_pwrDnThread != null)
            {
                try
                {
                    m_pwrDnThread.Abort();
                    m_pwrDnThread.Join(1000);
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex);
                }
            }

            m_pwrDnThread = null;
        }

        private void pwrDnThreadStart()
        {
            Logger.LogInfo("{0}.pwrDnThreadStart(): START", Name);

            try
            {
				Thread.Sleep(90000); //It was 45000 in original
                LoggerUserActions.MouseClick("Auto.PowerTurboDown", Name);
                powerTurboDown();
            }
            catch (ThreadAbortException)
            {
                Logger.LogInfo("{0}.pwrDnThreadStart(): ABORT", Name);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }

            Logger.LogInfo("{0}.pwrDnThreadStart(): END", Name);
        }



        private void barItemDebug_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemDebug", Name);
            
            DebugDspActive        = !DebugDspActive;
            barItemDebug.Checked    = DebugDspActive;
        }

        private void barItemHelpContent_Click_1(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemHelpContent", Name);
	        openUserManual();
        }

		private void openUserManual()
		{
			try
			{
				Process.Start(Constants.USER_MANUAL_FILE);
			}
			catch (Exception ex)
			{
				LoggedDialog.ShowWarning(this, string.Format("Fail to open user manual: '{0}'.\n\n{1}.", Constants.USER_MANUAL_FILE, ex.Message), exception: ex);
			}
		}

        private void barItemDeleteStudy_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemDeleteStudy", Name);

			using (var dlg = new StudyDeleteDlg())
            {
				dlg.TopMost = true;	//Ofer, 2014.01.05
				dlg.InitData(parentBarItemStudies.Items);
				dlg.ShowDialog(this);
				Focus();
			}
        }

        private void barItemCancelUtilitiesMenu_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemCancelUtilitiesMenu", Name);
            userManager();
        }

        private void barItemExportTrends_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemExportTrends", Name);

			var examRow = RimedDal.Instance.LoadedExamination();
			if (examRow == null)
				return;

			var examinationName = examRow.ItemArray[0].ToString();
            if (string.IsNullOrWhiteSpace(examinationName))
			{
				Logger.LogWarning("Export trends to CSV file Fail. Source examination name is empty.");
				return;
			}

            using (var sf = new SaveFileDialog())
            {
                sf.DefaultExt   = "csv";
                sf.Filter       = "csv files (*.csv)|*.csv|All files (*.*)|*.*";
                sf.AddExtension = true;
                sf.FilterIndex = 1;
                sf.RestoreDirectory = true;
                sf.InitialDirectory = Constants.TRENDS_PATH;

                //Get the list of csv-files of Trends directory
                var dirInfo         = new DirectoryInfo(sf.InitialDirectory);
                var files           = dirInfo.GetFiles("*.csv");
                
                //Search for file contains loaded examination's name
                var fullFileName = string.Empty;
                for (int i = 0; i < files.Length; i++)
                {
                    if (files[i].Name.IndexOf(examinationName) != -1)
                    {
                        sf.FileName     = files[i].Name;
                        fullFileName    = files[i].FullName;
                        break;
                    }
                }

                if (string.IsNullOrWhiteSpace(fullFileName))
                {
					Logger.LogWarning("Export trends to CSV file Fail. Examination '{0}' trends file was not found.", examinationName);
                    LoggedDialog.ShowError(this, StringManager.GetString("CSV-file for this examination is absent"));
                    return;
                }

	            var res = sf.ShowDialog(this);
	            Focus();
				if (res == DialogResult.OK)
                {
                    string data;
                    using (var sr = new StreamReader(fullFileName))
                    {
                        data = sr.ReadToEnd();
                        sr.Close();
                    }

                    using (var sw = new StreamWriter(sf.FileName))
                    {
                        sw.Write(data);
                        sw.Close();
                    }
                }
            }
        }

        private void barItemPatientExport_Popup(object sender, EventArgs e)
        {
            barItemExportTrends.Enabled = (AtTheEndOfMonitoringExam || (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.MonitoringLoad));
        }

        private void barItemExportLog_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemExportLog", Name);

            try
            {
	            using (var explorer = new Process())
	            {
		            explorer.StartInfo.FileName = "explorer";

		            explorer.StartInfo.Arguments = @"/e, C:\Rimed\SRC\TCD2003\GUI\Logs";
		            explorer.StartInfo.CreateNoWindow = true;
		            explorer.Start();
	            }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
        }

        private void barItemPatientReport_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemPatientReport", Name);
            
            if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Diagnostic || LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.DiagnosticLoad) 
                SummaryScreen.ActivatePatientReport();
            else if (CurrentStudyName.StartsWith(Constants.StudyType.STUDY_MONITORING_INTRACRANIAL))
                activateMonitoringPatientReport();
        }

        /// <summary>Updating the Sensitivity combo boxes visibility</summary>
        public void UpdateToolbar()
        {
            if (InvokeRequired)
            {
                BeginInvoke((Action)UpdateToolbar);
                return;
            }

            var replayMode          = LayoutManager.Instance.ReplayMode;
            var systemMode          = LayoutManager.SystemMode;
            var systemLayoutMode    = LayoutManager.SystemLayoutMode;

            if (replayMode)
            {
                toolBarPanel1.ComboBoxSensitivity.Visible = false;
                toolBarPanel1.ComboBoxSensitivitySecondary.Visible = false;
            }
            else
            {
                //Don't show the sensitivity combo boxes for rev3 or if the Prob is not 2M
				if (LayoutManager.Instance.SelectedGv != null && LayoutManager.Instance.SelectedGv.Probe.Name != "2Mhz PW")
                {
                    toolBarPanel1.ComboBoxSensitivity.Visible = false;
                    toolBarPanel1.ComboBoxSensitivitySecondary.Visible = false;
                }
                else
                {
                    toolBarPanel1.ComboBoxSensitivity.Visible = true;
	                toolBarPanel1.ComboBoxSensitivitySecondary.Visible = (StudyType == GlobalTypes.EStudyType.Bilateral);
                }
            }

            if (!IsRecordMode)
            {
                if ((replayMode) && (systemLayoutMode != GlobalTypes.ESystemLayoutMode.MonitoringLoad) && systemMode == GlobalTypes.ESystemModes.Offline)
                {
                    toolBarPanel1.ShowToolbar(GlobalTypes.ETooolBarType.TB6_ReplayFreeze);
                    toolBarPanel1.ComboBoxProbes.Enabled = false;
                }
                else if ((replayMode) && (systemLayoutMode != GlobalTypes.ESystemLayoutMode.MonitoringLoad) && systemMode == GlobalTypes.ESystemModes.Online)
                {
                    toolBarPanel1.ShowToolbar(GlobalTypes.ETooolBarType.TB9_ReplayUnfreeze);
                }
                else if ((replayMode) && (systemLayoutMode == GlobalTypes.ESystemLayoutMode.MonitoringLoad))
                {
					toolBarPanel1.ShowToolbar(GlobalTypes.ETooolBarType.TB7_MonitoringReplay);
                    if (systemMode == GlobalTypes.ESystemModes.Online)
                        toolBarPanel1.DisableButtonsInMonitoringOnlineMode();
                    else
                        toolBarPanel1.EnableButtonsInMonitoringOnlineMode();
                }
                else if (LayoutManager.Instance.IsStudyDiagnostic())
                {
                    if (systemMode == GlobalTypes.ESystemModes.Online)
                        toolBarPanel1.ShowToolbar(GlobalTypes.ETooolBarType.TB1_DiagnosticUnfreeze);
                    else if (systemMode == GlobalTypes.ESystemModes.Offline)
                        toolBarPanel1.ShowToolbar(GlobalTypes.ETooolBarType.TB2_DiagnosticFreeze);
                }
                else
                {
                    if (systemMode == GlobalTypes.ESystemModes.Online)
                        toolBarPanel1.ShowToolbar(GlobalTypes.ETooolBarType.TB4_MonitoringUnfreeze);
                    else if (systemMode == GlobalTypes.ESystemModes.Offline)
                        toolBarPanel1.ShowToolbar(GlobalTypes.ETooolBarType.TB5_MonitoringFreeze);
                }
            }
            else
            {
				//RIMD-484: Hide Pause button in Online mode for Monitoring recording
                if ((systemMode == GlobalTypes.ESystemModes.Offline) && (CurrentStudyName == Constants.StudyType.STUDY_EVOKED_FLOW_UNILATERAL || CurrentStudyName == Constants.StudyType.STUDY_EVOKED_FLOW_BILATERAL || CurrentStudyName.StartsWith(Constants.StudyType.STUDY_MONITORING_INTRACRANIAL)))                    
                    toolBarPanel1.ShowToolbar(GlobalTypes.ETooolBarType.TB5_MonitoringFreeze);
                else if ((systemMode == GlobalTypes.ESystemModes.Online) && (CurrentStudyName == Constants.StudyType.STUDY_EVOKED_FLOW_UNILATERAL || CurrentStudyName == Constants.StudyType.STUDY_EVOKED_FLOW_BILATERAL || CurrentStudyName.StartsWith(Constants.StudyType.STUDY_MONITORING_INTRACRANIAL)))
                        toolBarPanel1.ShowToolbar(GlobalTypes.ETooolBarType.TB4_MonitoringUnfreeze);
            }
        }

        private void activateMonitoringPatientReport()
        {
			var examRow = RimedDal.Instance.LoadedExamination();
			if (examRow != null && (DspRecordMode || LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.MonitoringLoad))
				DisplayMonitoringRep(SelectedPatientIndex, examRow.Examination_Index);
			else
				LoggedDialog.ShowError(this, StringManager.GetString("There is no examination to display"));

			//var examIndex = (Guid)RimedDal.Instance.ExaminationsDS.tb_Examination.Rows[0]["Examination_Index"];
			//if (DspRecordMode || LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.MonitoringLoad)
			//    DisplayMonitoringRep(SelectedPatientIndex, examIndex);
			//else
			//    LoggedDialog.ShowError(this, StringManager.GetString("There is no examination to display"));
        }

        public void DisplayMonitoringRep(Guid patientIndex, Guid examIndex)
        {
            try
            {
                //Added by Natalie for RIMD-286: Incorrect order of BVs in Report after Save
                var dsGateExam  = new dsGateExamination();
                var selCmd       = "Select tb_GateExamination.* FROM tb_GateExamination WHERE (ExaminationIndex = {guid {" + examIndex + "}}) AND InSummaryScreen = TRUE ORDER BY OnSummaryScreenIndex ASC";// ORDER by StartTime DESC";
                RimedDal.Instance.GetGateExam(dsGateExam, selCmd);
                if (dsGateExam.tb_GateExamination.Rows.Count == 0)  //Ofer, v1.18.2.11
                {
                    LoggedDialog.ShowError(this, StringManager.GetString("There is no examination to display"));
                    return;
                }

                var dsExam = new dsExamination();
                selCmd = "SELECT tb_Examination.* FROM tb_Examination WHERE Patient_Index='" + patientIndex + "'";
                RimedDal.Instance.GetExamination(dsExam, selCmd);

                var dvPatient   = new DataView(RimedDal.Instance.PatientsDS.tb_Patient, "Patient_Index = '" + patientIndex + "'", null, DataViewRowState.CurrentRows);
                var dvHospital  = new DataView(RimedDal.Instance.ConfigDS.tb_HospitalDetails);
                var dsEventExam = RimedDal.Instance.GetEventHistoryByExaminationID(examIndex);

                var report      = new MonitoringReport(examIndex, dvHospital, dvPatient, dsExam, dsGateExam, dsEventExam);
				report.Display(true);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, "Report creation failed");
                LoggedDialog.ShowError(this, StringManager.GetString("Can not display the selected report"));
            }
        }

        private void buttonPatientRep_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("buttonPatientRep", Name);
            activateMonitoringPatientReport();
        }

        private void barItemAddEvent_Click(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemAddEvent", Name);
            AddCustomEvent();
        }

        public void AddCustomEvent()
        {
            DialogResult dlgRes;
            using (var dlg = new AddEventDlg())
            {
				dlg.TopMost = true;	//Ofer, 2014.01.05
				dlgRes = dlg.ShowDialog(this);
				Focus();
			}

            if (DialogResult.OK == dlgRes)
            {
				eventListViewCtrl1.EventListSaved = false;
                foreach (var tc in LayoutManager.Instance.Trends)
                {
                    tc.Graph.RepaintChart();
                }
            }
        }

        private void barItemDeleteEvent_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemDeleteEvent", Name);
            DeleteEvent();
        }

        public void DeleteEvent()
        {
            EventListViewCtrl.Instance.DeleteHistoryEvent();
            foreach (TrendChart tc in LayoutManager.Instance.Trends)
            {
                tc.Graph.RepaintChart();
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("buttonSave", Name);
            BVListViewCtrl.Instance.Save(true);	//Merge, Nadiia (Override: saveCurrentExamination();)
        }

        #region State Machine

        private void guiMgrSmOfflineState(CGuiSmEvent smEvent)
        {
            //var enterState = m_smState;
            switch(smEvent.SmEventNum) 
            {
	            case EGuiMgrSmEvent.GUI_NEW_STUDY_MONITORING_EVENT:
		            DspManager.Instance.DspMgrNewStudy(new CDspMgrStudyParams(null, m_studyId, EStudyLayoutType.Monitoring)); // TODO check where we do  know the data dir
		            m_smState = EGuiMgrSmState.GUI_MONITORING_ONLINE_FREEZE_STATE;

		            LayoutManager.Instance.FireNewStudyEvent(m_studyId);
		            break;
	            case EGuiMgrSmEvent.GUI_KEY_NEXT_BV_EVENT:
	            case EGuiMgrSmEvent.GUI_NEW_STUDY_DIAGNOSTIC_EVENT:
		            m_smState = EGuiMgrSmState.GUI_DIAGNOSTIC_ONLINE_FREEZE_STATE;
		            LayoutManager.Instance.FireNewStudyEvent(m_studyId);
		            break;
	            case EGuiMgrSmEvent.GUI_NEW_REPLAY_DIAGNOSTIC_EVENT:
		            LayoutManager.Instance.FireNewStudyEvent(m_studyId);
		            DspManager.Instance.DspMgrReplayNew(new CDspMgrStudyParams(smEvent.SmEventParamsArr[0].ToString(), m_studyId, EStudyLayoutType.Diagnostic));
		            m_smState = EGuiMgrSmState.GUI_REPLAY_END_OF_PLAYBACK_STATE;
		            break;
	            case EGuiMgrSmEvent.GUI_NEW_REPLAY_MONITORING_EVENT:
		            LayoutManager.Instance.FireNewStudyEvent(m_studyId);
		            DspManager.Instance.DspMgrReplayNew(new CDspMgrStudyParams(smEvent.SmEventParamsArr[0].ToString(), m_studyId, EStudyLayoutType.Monitoring));
		            m_smState = EGuiMgrSmState.GUI_REPLAY_END_OF_PLAYBACK_STATE;
		            break;
	            case EGuiMgrSmEvent.GUI_NEW_REPLAY_SUMMARY_IMG_EVENT:
		            m_smState = EGuiMgrSmState.GUI_REPLAY_DIAGNOSTIC_WITHOUT_RAW_DATA;
		            break;

	            case EGuiMgrSmEvent.GUI_FINISH_STUDY_SAVE_STORAGE_EVENT: // Marina because of old Bug wrong buttons are always enabled
	            case EGuiMgrSmEvent.GUI_FINISH_STUDY_REMOVE_STORAGE_EVENT:
	            case EGuiMgrSmEvent.GUI_LOAD_PATIENT_EVENT:
	            case EGuiMgrSmEvent.GUI_BUTTON_SUMMARY_CLICK_EVENT:
	            case EGuiMgrSmEvent.GUI_BUTTON_PLAY_EVENT:
	            case EGuiMgrSmEvent.GUI_BUTTON_PAUSE_EVENT:
	            case EGuiMgrSmEvent.GUI_SCROLL_EVENT:
		            break;
		            //DirName = Path.Combine(Constants.RAW_DATA_PATH, RimedDal.Instance.ExaminationsDS.tb_Examination.Rows[0].ItemArray[0].ToString());
	            default:
                    Logger.LogWarning("guiMgrSmOfflineState: {0} event is unhandled.", smEvent.SmEventNum);
	            break;
            }

            //Logger.LogTrace(Name, Logger.TraceLevel.L3, "STATE-MACHINE: EnterState={0}, Event={1}, ExitState={2}, Method=guiMgrSmOfflineState", enterState, smEvent, m_smState);
        }

        private void guiMgrSmOnlineMonitoringState(CGuiSmEvent smEvent)
        {
            //var enterState = m_smState;
			switch (smEvent.SmEventNum) 
            {
				case EGuiMgrSmEvent.GUI_KEY_SPACE_EVENT:

				case EGuiMgrSmEvent.GUI_TOGGLE_FREEZE_EVENT:
				case EGuiMgrSmEvent.GUI_BAR_ITEM_PLAY_EVENT:
				case EGuiMgrSmEvent.GUI_FREEZE_EVENT:
					m_smState = EGuiMgrSmState.GUI_MONITORING_ONLINE_FREEZE_STATE;
					DspManager.Instance.DspMgrFreeze();
					break;
				case EGuiMgrSmEvent.GUI_FINISH_STUDY_REMOVE_STORAGE_EVENT:
				case EGuiMgrSmEvent.GUI_FINISH_STUDY_SAVE_STORAGE_EVENT:
					guiMgrSmOnlineMonitoringFreezeState(smEvent);
					break;
				case EGuiMgrSmEvent.GUI_SCROLL_EVENT:
					break;

				case EGuiMgrSmEvent.GUI_KEY_NEXT_BV_EVENT: // Must be disabled during monitoring
				default:
                    Logger.LogWarning("guiMgrSmOnlineMonitoringState: {0} event is unhandled.", smEvent.SmEventNum);
					break;
			}

            //Logger.LogTrace(Name, Logger.TraceLevel.L3, "STATE-MACHINE: EnterState={0}, Event={1}, ExitState={2}, Method=guiMgrSmOnlineMonitoringState", enterState, smEvent, m_smState);
        }

        private void guiMgrSmOnlineDiagnosticState(CGuiSmEvent smEvent)
        {
            //var enterState = m_smState;
			switch (smEvent.SmEventNum) 
            {
				case EGuiMgrSmEvent.GUI_KEY_SPACE_EVENT:
				case EGuiMgrSmEvent.GUI_KEY_NEXT_BV_EVENT:
				case EGuiMgrSmEvent.GUI_TOGGLE_FREEZE_EVENT:
				case EGuiMgrSmEvent.GUI_BAR_ITEM_PLAY_EVENT:
				case EGuiMgrSmEvent.GUI_FREEZE_EVENT:
    					m_smState = EGuiMgrSmState.GUI_DIAGNOSTIC_ONLINE_FREEZE_STATE;
	    				m_studyId++;
		    			LayoutManager.Instance.StudyId = m_studyId;
			    		DspManager.Instance.DspMgrFinishStudySaveStorageWait();
                    break;
				case EGuiMgrSmEvent.GUI_FINISH_STUDY_REMOVE_STORAGE_EVENT:
				case EGuiMgrSmEvent.GUI_FINISH_STUDY_SAVE_STORAGE_EVENT:
				case EGuiMgrSmEvent.GUI_LOAD_PATIENT_EVENT:
					guiMgrSmOnlineDiagnosticFreezeState(smEvent);
					break;
				case EGuiMgrSmEvent.GUI_NEW_REPLAY_DIAGNOSTIC_EVENT:
					m_studyId++;
					LayoutManager.Instance.StudyId = m_studyId;
					DspManager.Instance.DspMgrFinishStudySaveStorageWait();
                    LayoutManager.Instance.FireNewStudyEvent(m_studyId);
    				DspManager.Instance.DspMgrReplayNew(new CDspMgrStudyParams(smEvent.SmEventParamsArr[0].ToString(), m_studyId, EStudyLayoutType.Diagnostic));
					m_smState = EGuiMgrSmState.GUI_REPLAY_END_OF_PLAYBACK_STATE;
					break;
				case EGuiMgrSmEvent.GUI_NEW_REPLAY_SUMMARY_IMG_EVENT:
					m_studyId++;
					LayoutManager.Instance.StudyId = m_studyId;
					DspManager.Instance.DspMgrFinishStudySaveStorageWait();
					LayoutManager.Instance.FireNewStudyEvent(m_studyId);
					m_smState = EGuiMgrSmState.GUI_REPLAY_DIAGNOSTIC_WITHOUT_RAW_DATA;
                    break;

				case EGuiMgrSmEvent.GUI_SCROLL_EVENT:
					break;

				default:
                    Logger.LogWarning("guiMgrSmOnlineDiagnosticState: {0} event is unhandled.", smEvent.SmEventNum);
					break;
			}

            //Logger.LogTrace(Name, Logger.TraceLevel.L3, "STATE-MACHINE: EnterState={0}, Event={1}, ExitState={2}, Method=guiMgrSmOnlineDiagnosticState", enterState, smEvent, m_smState);
        }
		 
        private void guiMgrSmOnlineMonitoringFreezeState(CGuiSmEvent smEvent)
        {
            //var enterState = m_smState;
			switch (smEvent.SmEventNum) 
            {
				case EGuiMgrSmEvent.GUI_KEY_SPACE_EVENT:
				case EGuiMgrSmEvent.GUI_BUTTON_PLAY_EVENT:
				case EGuiMgrSmEvent.GUI_TOGGLE_FREEZE_EVENT:
				case EGuiMgrSmEvent.GUI_BAR_ITEM_PLAY_EVENT:
				case EGuiMgrSmEvent.GUI_UNFREEZE_EVENT:
						m_smState = EGuiMgrSmState.GUI_MONITORING_ONLINE_STATE;
						DspManager.Instance.DspMgrUnFreeze();
					break;
				case EGuiMgrSmEvent.GUI_FINISH_STUDY_REMOVE_STORAGE_EVENT:
    					m_smState = EGuiMgrSmState.GUI_OFFLINE_STATE;
	    				m_studyId++;
		    			LayoutManager.Instance.StudyId = m_studyId;
			    		DspManager.Instance.DspMgrFinishStudyRemoveStorage();
					break;
				case EGuiMgrSmEvent.GUI_FINISH_STUDY_SAVE_STORAGE_EVENT:
    					m_smState = EGuiMgrSmState.GUI_OFFLINE_STATE;					 
	    				m_studyId++;
		    			LayoutManager.Instance.StudyId = m_studyId;
			    		DspManager.Instance.DspMgrFinishStudySaveStorageWait();
                    break;
				case EGuiMgrSmEvent.GUI_SCROLL_EVENT:
					break;

				default:
                    Logger.LogWarning("guiMgrSmOnlineMonitoringFreezeState: {0} event is unhandled.", smEvent.SmEventNum);
					break;
			}

            //Logger.LogTrace(Name, Logger.TraceLevel.L3, "STATE-MACHINE: EnterState={0}, Event={1}, ExitState={2}, Method=guiMgrSmOnlineMonitoringFreezeState", enterState, smEvent, m_smState);
        }

        private void guiMgrSmOnlineDiagnosticFreezeState(CGuiSmEvent smEvent)
        {
            //var enterState = m_smState;
			switch (smEvent.SmEventNum) 
            {
				case EGuiMgrSmEvent.GUI_KEY_SPACE_EVENT:
				case EGuiMgrSmEvent.GUI_BUTTON_PLAY_EVENT:
				case EGuiMgrSmEvent.GUI_TOGGLE_FREEZE_EVENT:
				case EGuiMgrSmEvent.GUI_BAR_ITEM_PLAY_EVENT:
				case EGuiMgrSmEvent.GUI_UNFREEZE_EVENT:
	    				LayoutManager.Instance.FireNewStudyEvent(m_studyId);
    					m_smState = EGuiMgrSmState.GUI_DIAGNOSTIC_ONLINE_STATE;
		    			DspManager.Instance.DspMgrNewStudy(new CDspMgrStudyParams(null, m_studyId, EStudyLayoutType.Diagnostic));
			    		DspManager.Instance.DspMgrUnFreeze();
					break;
				case EGuiMgrSmEvent.GUI_LOAD_PATIENT_EVENT:
				case EGuiMgrSmEvent.GUI_FINISH_STUDY_REMOVE_STORAGE_EVENT:
    					m_smState = EGuiMgrSmState.GUI_OFFLINE_STATE;					
					break;
				case EGuiMgrSmEvent.GUI_FINISH_STUDY_SAVE_STORAGE_EVENT:
	    				m_smState = EGuiMgrSmState.GUI_OFFLINE_STATE;
 		    			m_studyId++;
			    		LayoutManager.Instance.StudyId = m_studyId;					 
					break;
				case EGuiMgrSmEvent.GUI_NEW_REPLAY_DIAGNOSTIC_EVENT: // This event occurs when do prev BV in diagnostic state or  click on summary screen
				    	DspManager.Instance.DspMgrReplayNew(new CDspMgrStudyParams(smEvent.SmEventParamsArr[0].ToString(), m_studyId, EStudyLayoutType.Diagnostic));
					    m_smState = EGuiMgrSmState.GUI_REPLAY_END_OF_PLAYBACK_STATE;
					break;
				case EGuiMgrSmEvent.GUI_NEW_REPLAY_SUMMARY_IMG_EVENT:
    					m_smState = EGuiMgrSmState.GUI_REPLAY_DIAGNOSTIC_WITHOUT_RAW_DATA;
					break;

				case EGuiMgrSmEvent.GUI_FREEZE_EVENT:
				case EGuiMgrSmEvent.GUI_BUTTON_SUMMARY_CLICK_EVENT: // Valid event, just do nothing
				case EGuiMgrSmEvent.GUI_KEY_NEXT_BV_EVENT:
					break;
				case EGuiMgrSmEvent.GUI_SCROLL_EVENT:
	    				DspManager.Instance.DspMgrScroll(smEvent.SmEventParamsArr);	//Merge: Marina
					break;

				default:
                    Logger.LogWarning("guiMgrSmOnlineDiagnosticFreezeState: {0} event is unhandled.", smEvent.SmEventNum);
					break;
			}

            //Logger.LogTrace(Name, Logger.TraceLevel.L3, "STATE-MACHINE: EnterState={0}, Event={1}, ExitState={2}, Method=guiMgrSmOnlineDiagnosticFreezeState", enterState, smEvent, m_smState);
        }

        private void guiMgrSmDiagnosticReplayState(CGuiSmEvent smEvent)
        {
            //var enterState = m_smState;
			switch (smEvent.SmEventNum) 
            {
				case EGuiMgrSmEvent.GUI_KEY_NEXT_BV_EVENT: // Only for Diagnostic replay may be should be separated state!
					m_smState = EGuiMgrSmState.GUI_DIAGNOSTIC_ONLINE_FREEZE_STATE;
					m_studyId++;
					LayoutManager.Instance.StudyId = m_studyId;
					DspManager.Instance.DspMgrReplayStop();
					break;
				case EGuiMgrSmEvent.GUI_BUTTON_SUMMARY_CLICK_EVENT:
					m_smState = EGuiMgrSmState.GUI_REPLAY_PAUSE_STATE;
					DspManager.Instance.DspMgrReplayPause();
					break;
				case EGuiMgrSmEvent.GUI_FINISH_STUDY_REMOVE_STORAGE_EVENT:
				case EGuiMgrSmEvent.GUI_FINISH_STUDY_SAVE_STORAGE_EVENT:
					m_smState = EGuiMgrSmState.GUI_REPLAY_PAUSE_STATE;
					DspManager.Instance.DspMgrReplayPause();
					break;
				case EGuiMgrSmEvent.GUI_NEW_REPLAY_DIAGNOSTIC_EVENT:
					m_studyId++;
					LayoutManager.Instance.StudyId = m_studyId;
					DspManager.Instance.DspMgrReplayStop();
					LayoutManager.Instance.FireNewStudyEvent(m_studyId);
					DspManager.Instance.DspMgrReplayNew(new CDspMgrStudyParams(smEvent.SmEventParamsArr[0].ToString(), m_studyId, EStudyLayoutType.Diagnostic));
					m_smState = EGuiMgrSmState.GUI_REPLAY_END_OF_PLAYBACK_STATE;
					break;
				case EGuiMgrSmEvent.GUI_NEW_REPLAY_SUMMARY_IMG_EVENT:
					m_studyId++;
					LayoutManager.Instance.StudyId = m_studyId;
					DspManager.Instance.DspMgrReplayStop();
					LayoutManager.Instance.FireNewStudyEvent(m_studyId);
					m_smState = EGuiMgrSmState.GUI_REPLAY_DIAGNOSTIC_WITHOUT_RAW_DATA;
					break;

				case EGuiMgrSmEvent.GUI_BUTTON_PLAY_EVENT: // Marina because of old bug in buttons they are not disabled properly
					break;
				case EGuiMgrSmEvent.GUI_SCROLL_EVENT:

					break;
				default:
                    Logger.LogWarning("guiMgrSmDiagnosticReplayState: {0} event is unhandled.", smEvent.SmEventNum);
					break;
			}

            //Logger.LogTrace(Name, Logger.TraceLevel.L3, "STATE-MACHINE: EnterState={0}, Event={1}, ExitState={2}, Method=guiMgrSmDiagnosticReplayState", enterState, smEvent, m_smState);
        }

		private void guiMgrSmReplayState(CGuiSmEvent smEvent)
		{
            //var enterState = m_smState;
			switch (smEvent.SmEventNum) 
            {
				case EGuiMgrSmEvent.GUI_KEY_SPACE_EVENT:
				case EGuiMgrSmEvent.GUI_TOGGLE_FREEZE_EVENT:
				case EGuiMgrSmEvent.GUI_BUTTON_PAUSE_EVENT:
				case EGuiMgrSmEvent.GUI_REPLAY_PAUSE_EVENT:
			    		m_smState = EGuiMgrSmState.GUI_REPLAY_PAUSE_STATE;
				    	DspManager.Instance.DspMgrReplayPause();
					break;
				case EGuiMgrSmEvent.GUI_REPLAY_END_OF_DATA_EVENT:
					    m_smState = EGuiMgrSmState.GUI_REPLAY_END_OF_PLAYBACK_STATE;
					break;
				case EGuiMgrSmEvent.GUI_FINISH_STUDY_REMOVE_STORAGE_EVENT:
				case EGuiMgrSmEvent.GUI_FINISH_STUDY_SAVE_STORAGE_EVENT:
					    if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Diagnostic) 
                        {
						    m_smState = EGuiMgrSmState.GUI_REPLAY_PAUSE_STATE;
						    DspManager.Instance.DspMgrReplayPause();
					    }
					    else 
                        {
						    m_smState = EGuiMgrSmState.GUI_OFFLINE_STATE;
						    m_studyId++;
						    LayoutManager.Instance.StudyId = m_studyId;
						    DspManager.Instance.DspMgrReplayStop();
					    }
					break;
				case EGuiMgrSmEvent.GUI_SCROLL_EVENT:
					break;

				default:
						if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Diagnostic) 
							guiMgrSmDiagnosticReplayState(smEvent);
	                    else
                        	Logger.LogWarning("guiMgrSmReplayState: {0} event is unhandled.", smEvent.SmEventNum);
						
    				break;
			}

            //Logger.LogTrace(Name, Logger.TraceLevel.L3, "STATE-MACHINE: EnterState={0}, Event={1}, ExitState={2}, Method=guiMgrSmReplayState", enterState, smEvent, m_smState);
        }

		private void guiMgrSmReplayEndOfPlaybackState(CGuiSmEvent smEvent)
		{
            //var enterState = m_smState;
			switch (smEvent.SmEventNum) 
            {
				case EGuiMgrSmEvent.GUI_KEY_SPACE_EVENT:
				case EGuiMgrSmEvent.GUI_BUTTON_PLAY_EVENT:
				case EGuiMgrSmEvent.GUI_TOGGLE_FREEZE_EVENT:
				case EGuiMgrSmEvent.GUI_UNFREEZE_EVENT:
    					m_smState = EGuiMgrSmState.GUI_REPLAY_STATE;
	    				LayoutManager.Instance.FireNewStudyEvent(m_studyId);
		    			DspManager.Instance.DspMgrReplayResume();
					break;
				case EGuiMgrSmEvent.GUI_SCROLL_EVENT:
				case EGuiMgrSmEvent.GUI_REPLAY_PAUSE_EVENT:
					break;

				default:
						guiMgrSmReplayPauseState(smEvent);
					break;
			}

            //Logger.LogTrace(Name, Logger.TraceLevel.L3, "STATE-MACHINE: EnterState={0}, Event={1}, ExitState={2}, Method=guiMgrSmReplayEndOfPlaybackState", enterState, smEvent, m_smState);
        }

		private void guiMgrSmReplayPauseState(CGuiSmEvent smEvent)
		{
            //var enterState = m_smState;
			switch (smEvent.SmEventNum) 
            {
				case EGuiMgrSmEvent.GUI_KEY_SPACE_EVENT:
				case EGuiMgrSmEvent.GUI_BUTTON_PLAY_EVENT:
				case EGuiMgrSmEvent.GUI_TOGGLE_FREEZE_EVENT:
				case EGuiMgrSmEvent.GUI_UNFREEZE_EVENT:
						m_smState = EGuiMgrSmState.GUI_REPLAY_STATE;
						DspManager.Instance.DspMgrReplayResume();
					break;
				case EGuiMgrSmEvent.GUI_FINISH_STUDY_SAVE_STORAGE_EVENT: // Both events occure when we do replay during the diagnostic study
				case EGuiMgrSmEvent.GUI_FINISH_STUDY_REMOVE_STORAGE_EVENT:// and press stop button??
					    if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Diagnostic) 
                        {
						    m_smState = EGuiMgrSmState.GUI_REPLAY_PAUSE_STATE;
					    }
					    else 
                        {
						    m_smState = EGuiMgrSmState.GUI_OFFLINE_STATE;
						    m_studyId++;
						    LayoutManager.Instance.StudyId = m_studyId;
						    DspManager.Instance.DspMgrReplayStop();
					    }
					break;
				case EGuiMgrSmEvent.GUI_BUTTON_SUMMARY_CLICK_EVENT:
					break;
				case EGuiMgrSmEvent.GUI_BUTTON_PAUSE_EVENT:
					// The button Should be disabled...
					break;
				case EGuiMgrSmEvent.GUI_KEY_NEXT_BV_EVENT:
					m_smState = EGuiMgrSmState.GUI_DIAGNOSTIC_ONLINE_FREEZE_STATE;
					m_studyId++;
					LayoutManager.Instance.StudyId = m_studyId;
					DspManager.Instance.DspMgrReplayStop();
					break;
				case EGuiMgrSmEvent.GUI_NEW_REPLAY_DIAGNOSTIC_EVENT:
					m_studyId++;
					LayoutManager.Instance.StudyId = m_studyId;
					DspManager.Instance.DspMgrReplayStop();
					LayoutManager.Instance.FireNewStudyEvent(m_studyId);
					DspManager.Instance.DspMgrReplayNew(new CDspMgrStudyParams(smEvent.SmEventParamsArr[0].ToString(), m_studyId,  EStudyLayoutType.Diagnostic) );
					m_smState = EGuiMgrSmState.GUI_REPLAY_END_OF_PLAYBACK_STATE;
					break;
				case EGuiMgrSmEvent.GUI_NEW_REPLAY_MONITORING_EVENT:
					m_studyId++;
					LayoutManager.Instance.StudyId = m_studyId;
					DspManager.Instance.DspMgrReplayStop();
					LayoutManager.Instance.FireNewStudyEvent(m_studyId);
					DspManager.Instance.DspMgrReplayNew(new CDspMgrStudyParams(smEvent.SmEventParamsArr[0].ToString(), m_studyId,  EStudyLayoutType.Monitoring) );
					m_smState = EGuiMgrSmState.GUI_REPLAY_END_OF_PLAYBACK_STATE;
					break;
				case EGuiMgrSmEvent.GUI_NEW_REPLAY_SUMMARY_IMG_EVENT:
					m_studyId++;
					LayoutManager.Instance.StudyId = m_studyId;
					DspManager.Instance.DspMgrReplayStop();
					LayoutManager.Instance.FireNewStudyEvent(m_studyId);
					m_smState = EGuiMgrSmState.GUI_REPLAY_DIAGNOSTIC_WITHOUT_RAW_DATA;
					break;
				case EGuiMgrSmEvent.GUI_NEW_STUDY_DIAGNOSTIC_EVENT:
    					// We played the blood vessel wihtout leaving the study, user pressed new study
	    				m_studyId++;
		    			LayoutManager.Instance.StudyId = m_studyId;
			    		DspManager.Instance.DspMgrReplayStop();
				    	m_smState = EGuiMgrSmState.GUI_DIAGNOSTIC_ONLINE_FREEZE_STATE;
					    LayoutManager.Instance.FireNewStudyEvent(m_studyId);
					break;
				case EGuiMgrSmEvent.GUI_NEW_STUDY_MONITORING_EVENT:
					m_studyId++;
					LayoutManager.Instance.StudyId = m_studyId;
					DspManager.Instance.DspMgrReplayStop();
					m_smState = EGuiMgrSmState.GUI_MONITORING_ONLINE_FREEZE_STATE;
					DspManager.Instance.DspMgrNewStudy(new CDspMgrStudyParams(null, m_studyId, EStudyLayoutType.Monitoring));
					LayoutManager.Instance.FireNewStudyEvent(m_studyId);
					break;
				case EGuiMgrSmEvent.GUI_REPLAY_END_OF_DATA_EVENT:
					m_smState = EGuiMgrSmState.GUI_REPLAY_END_OF_PLAYBACK_STATE;
					break;
				case EGuiMgrSmEvent.GUI_SCROLL_EVENT:
					DspManager.Instance.DspMgrScroll(smEvent.SmEventParamsArr); 
					break;
				default:
                        Logger.LogWarning("guiMgrSmReplayPauseState: {0} event is unhandled.", smEvent.SmEventNum);
					break;
			}

            //Logger.LogTrace(Name, Logger.TraceLevel.L3, "STATE-MACHINE: EnterState={0}, Event={1}, ExitState={2}, Method=guiMgrSmReplayPauseState", enterState, smEvent, m_smState);
         }

		private void guiMgrSmReplayDiagnosticWithoutRawData(CGuiSmEvent smEvent)
		{
            //var enterState = m_smState;
			switch (smEvent.SmEventNum) 
            {
				case EGuiMgrSmEvent.GUI_KEY_SPACE_EVENT:
				case EGuiMgrSmEvent.GUI_BUTTON_PLAY_EVENT:
				case EGuiMgrSmEvent.GUI_TOGGLE_FREEZE_EVENT:
				case EGuiMgrSmEvent.GUI_UNFREEZE_EVENT:
					break;
				case EGuiMgrSmEvent.GUI_FINISH_STUDY_SAVE_STORAGE_EVENT: // Both events occure when we do replay during the diagnostic study
				case EGuiMgrSmEvent.GUI_FINISH_STUDY_REMOVE_STORAGE_EVENT:// and press stop button??
					if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Diagnostic) 
						m_smState = EGuiMgrSmState.GUI_REPLAY_PAUSE_STATE;
					else 
						m_smState = EGuiMgrSmState.GUI_OFFLINE_STATE;
					break;
				case EGuiMgrSmEvent.GUI_BUTTON_SUMMARY_CLICK_EVENT:
					break;
				case EGuiMgrSmEvent.GUI_BUTTON_PAUSE_EVENT:
					// The button Should be disabled...
					break;
				case EGuiMgrSmEvent.GUI_KEY_NEXT_BV_EVENT:
					m_smState = EGuiMgrSmState.GUI_DIAGNOSTIC_ONLINE_FREEZE_STATE;
					break;
				case EGuiMgrSmEvent.GUI_NEW_REPLAY_DIAGNOSTIC_EVENT:
					DspManager.Instance.DspMgrReplayNew(new CDspMgrStudyParams(smEvent.SmEventParamsArr[0].ToString(), m_studyId, EStudyLayoutType.Diagnostic));
					m_smState = EGuiMgrSmState.GUI_REPLAY_END_OF_PLAYBACK_STATE;
					break;
				case EGuiMgrSmEvent.GUI_NEW_REPLAY_SUMMARY_IMG_EVENT:
					break;
				case EGuiMgrSmEvent.GUI_NEW_STUDY_DIAGNOSTIC_EVENT:
					m_smState = EGuiMgrSmState.GUI_DIAGNOSTIC_ONLINE_FREEZE_STATE;
					break;
				case EGuiMgrSmEvent.GUI_NEW_STUDY_MONITORING_EVENT:
					m_smState = EGuiMgrSmState.GUI_MONITORING_ONLINE_FREEZE_STATE;
					DspManager.Instance.DspMgrNewStudy(new CDspMgrStudyParams(null, m_studyId, EStudyLayoutType.Monitoring));
					break;
				case EGuiMgrSmEvent.GUI_SCROLL_EVENT:
					break;
				default:
                    Logger.LogWarning("guiMgrSmReplayDiagnosticWithoutRawData: {0} event is unhandled.", smEvent.SmEventNum);
					break;
			}

            //Logger.LogTrace(Name, Logger.TraceLevel.L3, "STATE-MACHINE: EnterState={0}, Event={1}, ExitState={2}, Method=guiMgrSmReplayDiagnosticWithoutRawData", enterState, smEvent, m_smState);
        }
		
		private void guiMgrSmGo(CGuiSmEvent smEvent)
		{
            //var enterState = m_smState;
            if (InvokeRequired) 
            {
				var result = BeginInvoke(new GuiMgrSmGoDelegate(guiMgrSmGoDelegateFunc), smEvent);
				if (! result.AsyncWaitHandle.WaitOne(GUI_SM_SYNC_CALL_TIMEOUT, false))
                    Logger.LogWarning("guiMgrSmGo.Invoke timeout ({0}). State={1}, Event={2}", GUI_SM_SYNC_CALL_TIMEOUT, m_smState, smEvent);
			}
			else
				guiMgrSmGoDelegateFunc(smEvent);

            //Logger.LogTrace(Logger.ETraceLevel.L9, Name, "guiMgrSmGo(...): EnterState={0}, Event={1}, ExitState={2}", enterState, smEvent.SmEventNum, m_smState);
        }

        private delegate void GuiMgrSmGoDelegate(CGuiSmEvent smEvent);

		private void guiMgrSmGoDelegateFunc(CGuiSmEvent smEvent)
		{
			if (InvokeRequired)
				return;

            switch (m_smState) 
            {
				case EGuiMgrSmState.GUI_HW_INIT_STATE:
					//if (! m_isInitialized)
					//    initDSP();
					break;
				case EGuiMgrSmState.GUI_OFFLINE_STATE:
    					guiMgrSmOfflineState(smEvent);
					break;
				case EGuiMgrSmState.GUI_MONITORING_ONLINE_STATE:
	    				guiMgrSmOnlineMonitoringState(smEvent);
					break;
				case EGuiMgrSmState.GUI_DIAGNOSTIC_ONLINE_STATE:
		    			guiMgrSmOnlineDiagnosticState(smEvent);
					break;
				case EGuiMgrSmState.GUI_DIAGNOSTIC_ONLINE_FREEZE_STATE:
			    		guiMgrSmOnlineDiagnosticFreezeState(smEvent);
					break;
				case EGuiMgrSmState.GUI_MONITORING_ONLINE_FREEZE_STATE:
				    	guiMgrSmOnlineMonitoringFreezeState(smEvent);
					break;
				case EGuiMgrSmState.GUI_REPLAY_STATE:
					    guiMgrSmReplayState(smEvent);
					break;
				case EGuiMgrSmState.GUI_REPLAY_PAUSE_STATE:
    					guiMgrSmReplayPauseState(smEvent);
					break;
				case EGuiMgrSmState.GUI_REPLAY_END_OF_PLAYBACK_STATE:
	    				guiMgrSmReplayEndOfPlaybackState(smEvent);
					break;
				case EGuiMgrSmState.GUI_REPLAY_DIAGNOSTIC_WITHOUT_RAW_DATA:
		    			guiMgrSmReplayDiagnosticWithoutRawData(smEvent);
					break;
				case EGuiMgrSmState.GUI_FPGA_LOAD_STATE:
					break;
				default:
                    Logger.LogWarning("guiMgrSmGoDelegateFunc: {0} event is unhandled.", smEvent.SmEventNum);
	            break;
			}
        }

        #endregion State Machine

	    private void onDspEndPlayback()
	    {
            LayoutManager.Instance.OnDspEndPlayback();
		    clearTrends(); //Must do restart!! 							//Merge, Marina
		    guiMgrSmGo(new CGuiSmEvent(EGuiMgrSmEvent.GUI_REPLAY_END_OF_DATA_EVENT));

            //Changed by Alex Bug #712 v.2.2.3.19 03/01/2016
            if (InvokeRequired)
                Invoke((Action) delegate()
                {
                    LayoutManager.Instance.PicManualCalculationsVisible = (LayoutManager.SystemMode != GlobalTypes.ESystemModes.Online &&
                                                       !CurrentStudyName.StartsWith(Constants.StudyType.STUDY_EVOKED_FLOW_BILATERAL) &&
                                                       !CurrentStudyName.StartsWith(Constants.StudyType.STUDY_EVOKED_FLOW_UNILATERAL)
                                                       );
                    LayoutManager.Instance.LindegaardRatioVisible = LayoutManager.SystemMode != GlobalTypes.ESystemModes.Online;
                });

            StopWMVRecord();
	    }
		 
	    private void onDspEndDrawHistoryEvent()
	    {
            Logger.LogTrace("onDspEndDrawHistoryEvent()", Name);
            if (InvokeRequired)
            {
                BeginInvoke(new Action(onDspEndDrawHistoryEvent));
                return;
            }

            if (EventListViewCtrl.Instance.SelectedEvent != null)
                MonitoringReplayTimeCounter = EventListViewCtrl.Instance.SelectedEvent.EventTime; 

			doPauseAction();
	    }

		private bool checkHDFreeSpace(char driveLatter, string driveName, int freeGBStop, int freeGBWarn = 25, int freePercentWarn = 10, string delMsg = null)
		{
			double hdSysFreeSpace = 0;
			try
			{
				double hdSize;
				using (var disk = new System.Management.ManagementObject("win32_logicaldisk.deviceid=\"" + driveLatter + ":\""))
				{
					disk.Get();
					hdSize			= Convert.ToDouble(disk["Size"]);
					hdSysFreeSpace	= Convert.ToDouble(disk["FreeSpace"]);
				}

				var hdSysFreePercentage = hdSysFreeSpace * 100.0 / hdSize;
				hdSysFreeSpace			= hdSysFreeSpace / 1024 / 1024 / 1024;

				if (hdSysFreeSpace < freeGBStop || hdSysFreeSpace < freeGBWarn || hdSysFreePercentage < freePercentWarn)
				{
					var msg = string.Format("\t-      A T T E N T I O N      -\n\n{0} HD {1}:\\ available space is low ({2:F2}GB).\n\n{3}.", driveName, driveLatter, hdSysFreeSpace, delMsg);
					if (string.IsNullOrWhiteSpace(delMsg))
						Logger.LogWarning(msg);
					else
						LoggedDialog.ShowWarning(this, msg, AppSettings.Active.ApplicationProductName + ": Low disk space warning");
				}
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);
			}

			return hdSysFreeSpace > freeGBStop;
		}

		private MainForm()
		{
			Trace.WriteLine(string.Format("Window handle: {0} created.", Handle));   // Ofer, Force creation of Window handle
			InitializeComponent();

			Instance = this;

			//NewSplashScreen.On();

			DebugDspActive = false;
			IsMonitoringSaved = false;
			IsRecordMode = false;
			StudyType = GlobalTypes.EStudyType.Unilateral;
			WorkingMode = GlobalTypes.EWorkingMode.ExameMode;
			DisplayPatientReport = true;
			IsLayoutRepNoBg = true;
			ScrollingEnabled = true;
			CurrentStudyId = Guid.Empty;
			ChangeStudy = false;
			AtTheEndOfMonitoringExam = false;
			MicroStepCounter = 0;

			m_smState = EGuiMgrSmState.GUI_HW_INIT_STATE;
			m_studyId = 0;

			NewSplashScreen.SetText("Initiating hard disk space monitor");
			HardDriveSpaceKeeper.Instance.PrepareForWork();

			NewSplashScreen.SetText("Initiating main screen panels");
			PanelAutoscan = new PanelsArr(panelAutoscan0, panelAutoscan1);
			PanelsCharts = new PanelsArr(panel0Charts, panel1Charts);
			PanelsTrends = new PanelsArr(panel0Trend, panel1Trend);
			Panels = new PanelsArr(panel0, panel1);

			foreach (var i in s_speedFactors)
			{
				var itemValue = i * INITIAL_WINDOW_TIME / 2.0;

				toolBarPanel1.ComboBoxTime.Items.Add(itemValue);
				comboBoxTimeRight.Items.Add(itemValue);
				comboBoxTimeLeft.Items.Add(itemValue);
			}

			toolBarPanel1.ComboBoxTime.SelectedIndex = DEFAULT_SPEED_FACTOR_INDEX;
			comboBoxTimeRight.SelectedIndex = DEFAULT_SPEED_FACTOR_INDEX;
			comboBoxTimeLeft.SelectedIndex = DEFAULT_SPEED_FACTOR_INDEX;

			// Handlers for the ToolbarPanel component cannot be added through the designer, so we add them here
			toolBarPanel1.ButtonStudies.Click += new EventHandler(buttonStudies_Click);
			toolBarPanel1.ButtonSummary.Click += new EventHandler(buttonSummary_Click);
			toolBarPanel1.ButtonScrollBackward.Click += new EventHandler(buttonScrollBackWard_Click);
			toolBarPanel1.ButtonScrollForward.Click += new EventHandler(buttonScrollForward_Click);
			toolBarPanel1.ButtonAutoscan.Button.Click += new EventHandler(buttonAutoscan_Click);
			toolBarPanel1.ButtonNewPatient.Click += new EventHandler(buttonNewPatient_Click);
			toolBarPanel1.ButtonLoad.Click += new EventHandler(buttonLoad_Click);
			toolBarPanel1.ButtonAddSpectrum.Click += new EventHandler(buttonAddSpectrum_Click);
			toolBarPanel1.ButtonHitsDetection.Button.Click += new EventHandler(buttonAHitsDetection_Click);
			toolBarPanel1.ButtonNotes.Click += new EventHandler(buttonNotes_Click);
			toolBarPanel1.ButtonDicom.Button.Click += new EventHandler(buttonDicom_Click);
            toolBarPanel1.ButtonWMV.Button.Click += new EventHandler(buttonWMV_Click);

			toolBarPanel1.ButtonAddTrend.Click += new EventHandler(buttonAddTrend_Click);
			toolBarPanel1.ButtonPrint.Click += new EventHandler(buttonPrint_Click);
			toolBarPanel1.ButtonCursors.Button.Click += new EventHandler(barItemCursors_Click);
			toolBarPanel1.ButtonFreeze.Button.MouseDown += new MouseEventHandler(buttonFreezeToggle_MouseDown);
			toolBarPanel1.ButtonPlay.Click += new EventHandler(buttonPlay_Click);
			toolBarPanel1.ButtonPause.Button.Click += new EventHandler(buttonPause_Click);
			toolBarPanel1.ButtonStartRecording.Button.Click += new EventHandler(buttonStart_Click);
			toolBarPanel1.ButtonStopRecording.Click += new EventHandler(buttonStop_Click);
			toolBarPanel1.ButtonNextFunction.Click += new EventHandler(buttonNextFunction_Click);
			toolBarPanel1.ButtonPatientRep.Click += new EventHandler(buttonPatientRep_Click);
			toolBarPanel1.ButtonSave.Click += new EventHandler(buttonSave_Click);

			if (GlobalSettings.HIDE_DEBUGFEATURE)
			{
				parentBarItem1.Visible = false;
				panelLeftCharts.BackColor = GlobalSettings.FormPanelBk;
				panelMainCharts.BackColor = GlobalSettings.FormPanelBk;
				panel0Charts.BackColor = GlobalSettings.FormPanelBk;
				panel1Charts.BackColor = GlobalSettings.FormPanelBk;
				panel0Trend.BackColor = GlobalSettings.FormPanelBk;
				panel1Trend.BackColor = GlobalSettings.FormPanelBk;
				panelTrend.BackColor = GlobalSettings.FormPanelBk;
				panelRightCharts.BackColor = GlobalSettings.FormPanelBk;
				panelAutoscan0.BackColor = GlobalSettings.FormPanelBk;
				panelAutoscan1.BackColor = GlobalSettings.FormPanelBk;
			}
			else
				parentBarItem1.Visible = true;

			m_nextFunctionList = new List<Delegate>();
			m_nextFunctionList.Add(new Action(freeze));
			m_nextFunctionList.Add(new Action(print));
			m_nextFunctionList.Add(new NextBVDelegate(nextBv));
			m_nextFunctionList.Add(new Action(unFreeze));


			if (!AppSettings.Active.ApplicationTitle)
			{
				FormBorderStyle = FormBorderStyle.None;
				labelCaption.Top = 3;
				labelCaption.Left = Width - labelCaption.Width - 3;
				labelCaption.Visible = true;
				labelCaption.BringToFront();
			}

			// set app screen size.
			Left = 0;
			Top = 0;
			Width = AppSettings.Active.ApplicatioWidth;		
			Height = AppSettings.Active.ApplicationHeight;	
			MaximumSize = Size;
			MinimumSize = Size;

		}

		#region MainForm event handlers (MainForm_XXXX())
		private void MainForm_Load(object sender, EventArgs e)
		{
			Application.Idle += onApplicationIdle;

			NewSplashScreen.Off();
			Focus();
            // Changed by Alex 20/12/2015 feature #672 v 2.2.3.16
            startRecProcess();
        }

        // mainForm_KeyDown: handle keyboard shortcut + USB Remote which act as keyboard input
        // more Shortcut are defined for the Toolbar items in MainForm.Designer.cs
		private void mainForm_KeyDown(object sender, KeyEventArgs e)
		{
			e.Handled = true;
			if (e.KeyCode != Keys.ControlKey && e.KeyCode != Keys.ShiftKey && e.KeyCode != Keys.Menu)
				LoggerUserActions.MouseClick(string.Format("MainForm_KeyDown: '{0}'", e.KeyData), Name);
			switch (e.KeyCode)
			{
				case Keys.D1:
					zeroLineDown();
					break;
				case Keys.D2:
					zeroLineUp();
					break;
                // CtrlShiftB is shortcut for prev BV, CtrlB for Next BV
                case Keys.B:
                    break;
                case Keys.C:
                    if (e.Shift && e.Control)
                        ;// saved for user defined F2
                    else if(e.Control)
                        toggleSpectrumDisplay();
                    break;
				case Keys.D:
                    // Debug
                    if (AppSettings.Active.AppOperationMode == General.EAppOperationMode.DEBUG)
                    {
                        if (hitsHistogram1.EnableHits)
                        {
                            if (e.Control && e.Shift)
                                HitsHistogram.DebugAddHits(Probe.PROB_SIDE_LEFT);
                            else if (e.Alt && e.Shift)
                                HitsHistogram.DebugAddHits(Probe.PROB_SIDE_RIGHT);
                        }
                        else if (e.Shift && e.Control)
                            GainUp();
                    }
                    else
                    {
                        if (e.Shift && e.Control)
                            GainUp();
                    }
                    break;
                case Keys.E:
                    //dont use Control+Shift+N is windows explorer shortcut for creating new folder
                    break;
                case Keys.F:
                    if (e.Shift && e.Control)
                       RangeUp(); //Scale 
                    break;
                case Keys.G:
                    if (e.Shift && e.Control)
                        RangeDown();    
                    break;
                // Ctrl+Shift+h is shortcut for HITS
                case Keys.H:
                    break;
				case Keys.I:
					if (e.Alt && e.Shift)
						changeCurrentProbe();
                    else if (e.Shift && e.Control)
                        zeroLineDown(); 
					break;
                case Keys.J:
                    if (e.Shift && e.Control)
                    {
                        if (LayoutManager.SystemLayoutMode != GlobalTypes.ESystemLayoutMode.Monitoring)
                            return;
                        eventListViewCtrl1.AddHistoryEvent();
                    }
                    break;
               case Keys.K:
                    if (e.Shift && e.Control)
                    {
                        if (LayoutManager.SystemLayoutMode != GlobalTypes.ESystemLayoutMode.Monitoring)
                            return;
                        eventListViewCtrl1.NextEvent();
                    }
                    break;
				case Keys.M:
                    if (e.Shift && e.Control)
                        toggleFwdPeakEnvelope();
                    else
					    mute();
					break;
				//case Keys.N: dont use Control+Shift+N is windows explorer shortcut for creating new folder
                case Keys.W:
                    if (e.Shift && e.Control)
                        toggleBckPeakEnvelope();
					break;
				case Keys.O:
					if (e.Alt && e.Shift && !e.Control)
						LayoutManager.Instance.ChangeSelectedGV();
                    else if (e.Shift && e.Control)
                    {
                        doSpaceBarWork();
                        Focus();
                    }
					break;
                case Keys.P:
                    if (e.Shift && e.Control)
                        DepthUp();
                    break;
				case Keys.Q:
					if (e.Alt && e.Shift)
						enterKeyDown();
                    else if (e.Shift && e.Control)
                        DepthDown();
					break;
				case Keys.U:
					if (e.Alt && e.Shift)
						userManager();
                    else if (e.Shift && e.Control)
                        zeroLineUp();
					break;
                case Keys.V:
                    if (e.Shift && e.Control)
                        ;// saved for user defined F1
                    break;
                case Keys.R:
                    if (e.Shift && e.Control)
                    {
                        applyNextFunction();
                        Focus();
                    }
                    break;
                case Keys.S:
                    if (e.Shift && e.Control)
                        VolumeUp();
                    break;
                case Keys.T:
                    if (e.Shift && e.Control)
                    {
                        if (SystemVolume <= GlobalSettings.VOLUME_MIN)
                            mute(true);
                        else
                            VolumeDown();
                        // long key press identification
                        //if (!m_keyDownSince.ContainsKey(e.KeyCode))
                        //{
                        //    m_keyDownSince.Add(e.KeyCode, DateTime.UtcNow);
                        //    VolumeDown();
                        //}
                        //else
                        //{
                        //    double diff = (DateTime.UtcNow - m_keyDownSince[e.KeyCode]).TotalSeconds;
                        //    if (diff < 0.8)
                        //        VolumeDown();
                        //    else
                        //        mute2();
                        //}
                    }
                    break;
				case Keys.X:
                    if (e.Control && !e.Alt && !e.Shift)
						toggleBckModeEnvelope();
					else if (!e.Alt && !e.Shift)
						toggleFwdModeEnvelope();
                    else if (e.Shift && e.Control)
                        GainDown();
					break;
				case Keys.Z:
					if (e.Control)
						toggleBckPeakEnvelope();
					else if (!e.Alt && !e.Shift)
						toggleFwdPeakEnvelope();
					break;

				case Keys.F8:
					if (e.Control)
					{
						var curGv = LayoutManager.Instance.CurrentGateView;
						int currentPower;
						if (curGv != null)
							currentPower = curGv.Probe.Power;
						else
						{
							var gv = LayoutManager.Instance.SelectedGv;
							currentPower = (gv != null) ? gv.Probe.Power : Probe.MAX_NORMAL_POWER;
						}

						if (currentPower <= Probe.MAX_NORMAL_POWER)
							powerTurboUp();
						else
							powerTurboDown();
					}
					break;

				case Keys.Escape:
					if (e.Shift)
					{
						if (LayoutManager.SystemMode != GlobalTypes.ESystemModes.Online)
							ExitDigiLite(true);
					}
					else
					{
						if (LayoutManager.Instance.CursorsMode)
							LayoutManager.Instance.CursorsMode = false;

						LayoutManager.Instance.ResetGvBMPSize();
					}
					break;

				case Keys.Left:
					if (!LayoutManager.Instance.CursorsMode)
					{
						if (toolBarPanel1.ComboBoxTime.SelectedIndex < toolBarPanel1.ComboBoxTime.Items.Count - 1)
							toolBarPanel1.ComboBoxTime.SelectedIndex = toolBarPanel1.ComboBoxTime.SelectedIndex + 1;
					}
					break;

				case Keys.Right:
					if (LayoutManager.Instance.CursorsMode)
					{
					}
					break;

				case Keys.Down:
					if (LayoutManager.Instance.CursorsMode)
					{
					}
					break;
				case Keys.Up:
					if (LayoutManager.Instance.CursorsMode)
					{
					}
					break;

				case Keys.Space:
					doSpaceBarWork();
					Focus();			//BugFix #217: All Studies -Pressing alt+ctrl+space makes space button unresponsive
					break;

				case Keys.OemMinus:
					VolumeDown();
					break;
				case Keys.Oemplus:
					if (e.Shift)
						VolumeUp();
					break;
                case Keys.A:
                    if (e.Shift && e.Control)
                    {
                        if (LayoutManager.SystemLayoutMode != GlobalTypes.ESystemLayoutMode.Diagnostic)
                            return;
                        bvListViewCtrl1.rename();// open rename BV dialog
                    }
                    break;
                case Keys.L:
                    if (e.Shift && e.Control)
                    {
                        LayoutManager.Instance.ChangeFlowDirection(); //flow direction
                    }
                    break;
 


			}
		}
        // long key press identification
        //private void mainForm_KeyUp(object sender, KeyEventArgs e)
        //{
        //    e.Handled = true;
        //    switch (e.KeyCode)
        //    {
        //        case Keys.T:
        //            m_keyDownSince.Remove(e.KeyCode);
        //            break;
        //    }
        //}

		private void MainForm_Closing(object sender, CancelEventArgs e)
		{
            // Added by Alex 30/11/2015 feature #672 v 2.2.3.14
            StopMyProcess();

			Enabled = false;

			TopMost = true;
			Focus();

			if (m_oneSecondTimer != null)
				m_oneSecondTimer.Stop();

			if (!s_isThreadUnhandledException)
				onClosing();

			DspManager.DspEndPlayback -= onDspEndPlayback;
			DspManager.DspEndDrawHistoryEvent -= onDspEndDrawHistoryEvent;
			DspManager.Stop();

			eventListViewCtrl1.StopEventCapture();

			if (SerialPortWrapper.IsInitiated)
				SerialPortWrapper.Instance.Close();

			CloseKeyboard();

			//Send notification message to Rimed.MainMenu application
			if (m_exitToWindows)
				ProcessComm.MainMenuApplicationExit();
			else
				ProcessComm.MainMenuApplicationRestore();	//User32.SendNotifyMessage(new IntPtr(-1), ProcessComm.WinMsgMainMenuApplicationRestore, 0, 0);

			IsClosing = true;
			if (m_shutdownWindows)
				ProcessComm.ShutdownWindows();

#if AUDIO_SAMPLES_WRITE
			AudioDSPtoPCStreamPlayer.LogDebugTests();
#endif
		}

		private void onApplicationIdle(object sender, EventArgs e)
		{
			Application.Idle -= onApplicationIdle;

			//Check available disk space
			var isContinue = checkHDFreeSpace(Environment.SystemDirectory[0], "System", AppSettings.Active.DiskSysFreeGBStop, AppSettings.Active.DiskSysFreeGBWarning, AppSettings.Active.DiskSysFreePercentWarinig, "Please free disk space.");
			isContinue &= checkHDFreeSpace(Constants.DATA_PATH[0], "Application data", AppSettings.Active.DiskDataFreeGBStop, AppSettings.Active.DiskDataFreeGBWarning, AppSettings.Active.DiskDataFreePercentWarinig, "Please free disk space using backup move operation.");

			// If not enough disk space exit application
			if (!isContinue)
			{
				OpenBackupDialog(this);
				isContinue = checkHDFreeSpace(Environment.SystemDirectory[0], "System", AppSettings.Active.DiskSysFreeGBStop, AppSettings.Active.DiskSysFreeGBWarning, AppSettings.Active.DiskSysFreePercentWarinig);
				isContinue &= checkHDFreeSpace(Constants.DATA_PATH[0], "Application", AppSettings.Active.DiskDataFreeGBStop, AppSettings.Active.DiskDataFreeGBWarning, AppSettings.Active.DiskDataFreePercentWarinig);
				if (!isContinue)
				{
					LoggedDialog.Show(this, "Low disk space.\n\nClick OK to exit application.", AppSettings.Active.ApplicationProductName);

					ExitDigiLite();

					return;
				}
			}

			// Readonly mode notification
			if (AppSettings.Active.IsDspOffMode)
			{
				if (!AppSettings.Active.IsReviewStation)
				{
					AppSettings.Active.DiagnosticPostSaveEdit = false;
					LoggedDialog.ShowNotification(this, "NOTE: Application running in DEMO/Read-Only mode.");
				}

				OpenLoadDialog();
			}
		}
		#endregion

        #region identify key press time
        [DllImport("user32.dll")]
        static extern short GetKeyState(int key);

        static bool IsKeyPressed(Keys key)
        {
            short state = GetKeyState((int)key);
            return ((state & 128) != 0);
        }

        Dictionary<Keys, DateTime> m_keyDownSince = new Dictionary<Keys, DateTime>();

        private void UpdateKeyStates()
        {
            foreach (var entry in m_keyDownSince.ToArray())
            {
                if (!IsKeyPressed(entry.Key))
                    m_keyDownSince.Remove(entry.Key);
            }
        }
        #endregion

        protected override void WndProc(ref Message m)
        {
			if (m.Msg == User32.WinMessage.WM_NCHITTEST)
                return;

            // This code was added to make Main Form fixed in Aero Themes of Windows 7
			if ((m.Msg == User32.WinMessage.WM_NCLBUTTONDOWN || m.Msg == User32.WinMessage.WM_NCLBUTTONDBLCLK) && m.WParam == (IntPtr)User32.WinMessage.WMParam.HTCAPTION)
                return;

            try
            {
                base.WndProc(ref m);
            }
            catch (Exception ex)
            {
                if (IsInitialized)
                {
                    Logger.LogError(ex, string.Format("{0}.WndProc({1})", Name, m));
                }
                else
                {
                    Logger.LogFatalError(ex, string.Format("Application initialization fail. {0}.WndProc({1})", Name, m));
                    LoggedDialog.ShowFatalError(this, "Application initialization Fail.\n\n" + ex.Message);

                    ExitDigiLite();
                }
            }
        }

		#region Application entry point
		[STAThread]
		static void Main()
		{
			// Set application process affinity and application base priority.
			var currProcess = Process.GetCurrentProcess();
			currProcess.ProcessorAffinity	= (IntPtr)1;
			currProcess.PriorityClass		= ProcessPriorityClass.AboveNormal;

			// Set application main thread (GUI thread) name and priority.
			var guiThread = Thread.CurrentThread;
			guiThread.SetName("Main");
			guiThread.Priority	= ThreadPriority.AboveNormal;

			// Set application unhandled exceptions handlers
			Application.ThreadException += threadUnhandledExceptionHandler;
			Application.SetUnhandledExceptionMode(UnhandledExceptionMode.Automatic);
			AppDomain.CurrentDomain.UnhandledException += domainUnhandledExceptionHandler;

            //Added by Alex v.2.2.2.05 18/12/2014
            // Initiate DSRimedDB before RemadDAL if you need update DB
            DSRimedDB.Init();

			// Initiate RemadDAL instance
			RimedDal.Init();

			writeLogHeader();

			LoggedDialog.FatalErrorCaption = StringManager.GetString("Fatal Error");
			LoggedDialog.ErrorCaption = StringManager.GetString("Error");
			LoggedDialog.WarningCaption = StringManager.GetString("Warning");
			LoggedDialog.InformationCaption = StringManager.GetString("Information");
			LoggedDialog.NotificationCaption = StringManager.GetString("Notification");
			LoggedDialog.QuestionCaption = StringManager.GetString("Confirmation");
			LoggedDialog.ConfirmationCaption = StringManager.GetString("Confirmation");

			bool isOneInstanceOnly;
			using (var mx = new Mutex(true, "Rimed.TCD.Rev6", out isOneInstanceOnly))
			{
				if (!isOneInstanceOnly)
				{
					LoggedDialog.Show(StringManager.GetString("Application is already running."));
					return;
				}
			}

			var isDebugMode = (AppSettings.Active.AppOperationMode == General.EAppOperationMode.DEBUG);
			if (!isDebugMode)
				User32.WindowsTaskBarHide();

			s_windowsSpecialKeysWatcher.Start(isDebugMode);

			Logger.SystemInfoLog("Loading configuration.....");
			try
			{
				var traceThreshold = AppConfig.GetConfigValue("Logger:TraceThreshold");
				Logger.SetTraceFilter(traceThreshold);

				if (AppSettings.Active.LoggerConfigParamsDump)
					AppConfig.LogConfigurationParameters();

				verifyApplicationFolders();

				var mainForm = new MainForm();
                // Added by Alex 01/05/2016 bug #852 v 2.2.3.31
                //Deleted by Alex v 2.2.3.36 18/05/2016 
                //mainForm.TopMost = true;

                NewSplashScreen.ShowSplashScreen();
                if (mainForm.init())
                {
                    NewSplashScreen.Off();
                    Application.Run(mainForm);
                }
            }
			catch (Exception ex)
			{
				Logger.LogFatalError(ex, "Application startup failed");
				LoggedDialog.ShowFatalError(StringManager.GetString("Exception") + Logger.ExceptionToString(ex));
			}

			s_windowsSpecialKeysWatcher.End();

			if (isDebugMode)
				User32.WindowsTaskBarShow();

			Logger.LogWarning("----- Rimed {0} is DOWN -----", AppSettings.Active.ApplicationProductName);

			shutdownLogger();
		}
		#endregion


		#region Static members
		/// <summary>Shutdown logger and rename log file (add time)</summary>
		private static void shutdownLogger()
		{
			var file = Logger.GetFileAppenderLocation("RollingFileAppender");
			Logger.Shutdown();

			if (!File.Exists(file))
				return;

			var fi = Files.GetFileInfo(file);
			if (fi == null)
				return;

			if (fi.CreationTime.Date == DateTime.Now.Date && !AppSettings.Active.LoggerPerApplicationRun)
				return;
			
			var filePath = Path.GetDirectoryName(file);
			var fileName = Path.GetFileName(file);

			string dest;
			var cntr = 1;
			do
			{
				dest = string.Format("{0}\\{1} {2:D2} {3}", filePath, fi.CreationTime.Date.ToString("yyyy.MM.dd"), cntr++, fileName);
			} while (File.Exists(dest));

			// Rename App log
			Files.Move(file, dest);

			// Rename App.Comm log
			file = file.Replace("Rimed.TCD.Rev6.LOG", "Rimed.TCD.Rev6.Comm.LOG");
			dest = dest.Replace("Rimed.TCD.Rev6.LOG", "Rimed.TCD.Rev6.Comm.LOG");
			Files.Move(file, dest);

		}

        private static void verifyApplicationFolders()
        {
            Logger.SystemInfoLog("Verifying application folders:");

            verifyeFolder(Constants.TEMP_IMG_PATH);
            Files.DeleteFolderFiles(Constants.TEMP_IMG_PATH);

            verifyeFolder(Constants.REPLAY_IMG_PATH);

            verifyeFolder(Constants.IP_IMAGES_PATH);

            verifyeFolder(Constants.GATES_IMAGE_PATH);

            verifyeFolder(Constants.RAW_DATA_PATH);

            verifyeFolder(Constants.SUMMARY_IMG_PATH);

            verifyeFolder(Constants.TRENDS_PATH);

            verifyeFolder(Constants.DICOM_FILES_PATH);
        }

        private static void verifyeFolder(string path)
        {
            Logger.LogInfo("- Folder: '{0}': ", path);

            try
            {
                // Create existing directory dose NOT throw an exception.
                Directory.CreateDirectory(path);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
        }

        private static void domainUnhandledExceptionHandler(object sender, UnhandledExceptionEventArgs e)
        {
            var msg = string.Format("Domain UnhandledException. IsTerminating={0}", e.IsTerminating);
            var ex  = e.ExceptionObject as Exception;
            Logger.LogFatalError(ex, msg);
        }

        private static bool s_isThreadUnhandledException;
        private static ResourceManagerWrapper s_stringManager;
        private static readonly object s_stringManagerLock = new object();

	    private static void threadUnhandledExceptionHandler(object sender, ThreadExceptionEventArgs e)
        {
            s_isThreadUnhandledException = true;
            try
            {
                Exception ex = null;
				var msg = "Non-specific error.";
				if (e != null && e.Exception != null)
                {
                    ex = e.Exception;
                    msg = ex.Message;
                }

                var nativeEx = ex as Win32Exception;
                if (nativeEx != null)
                    msg += string.Format("\nNativeErrorCode=0x{0:X8}.", nativeEx.NativeErrorCode);

                Logger.LogFatalError(ex, "UNHANDLED EXCEPTION");
                LoggedDialog.ShowFatalError(msg); 

                Instance.ExitDigiLite();
            }
            catch (Exception ex)
            {
                try
                {
                    Logger.LogFatalError(ex);

                    LoggedDialog.ShowFatalError(ex.Message);
                }
                finally
                {
					Processes.Current.CloseMainWindow();
					Application.Exit();
                }
            }
        }

        private static void writeLogHeader()
        {
			var key		= RimedDal.Instance.AuthorizationMgrDS.tb_AuthorizationMgr[0].ProductKey;
			var title = " Rimed ltd. " + AppSettings.Active.ApplicationProductName + " Rev6\t\tv" + AppGlobals.FileVersion + " [" + AppGlobals.AppVersion + "]\t\tDB v" + DSRimedDB.DBVersion + " (" + DSRimedDB.DBVersionDate + ")";

            Logger.SystemInfoLog("========================================================================================================");
			Logger.LogWarning(title);
            Logger.SystemInfoLog("--------------------------------------------------------------------------------------------------------");
            Logger.SystemInfoLog("- Product key     " + key.ToUpper());
            Logger.SystemInfoLog("- PID:            " + Process.GetCurrentProcess().Id);
            Logger.SystemInfoLog("- MachineName:    " + Environment.MachineName);
            Logger.SystemInfoLog("- OS:             " + Environment.OSVersion);
            Logger.SystemInfoLog("- Clr Version:    " + Environment.Version);
            Logger.SystemInfoLog("- WorkingSet:     " + Environment.WorkingSet);
            Logger.SystemInfoLog("- Processors:     " + Environment.ProcessorCount);
            Logger.SystemInfoLog("- Current folder: " + Environment.CurrentDirectory);
            Logger.SystemInfoLog("- User name:      " + Environment.UserName);
            Logger.SystemInfoLog("- Start time      " + DateTime.Now.ToString("yyyy.MM.dd HH:mm:ss (zzz)"));
            Logger.SystemInfoLog("--------------------------------------------------------------------------------------------------------");
        }
		#endregion


		private static readonly int[] s_speedFactors = { 1, 2, 4, 8, 10, 12 };

		public static int GetSpeedFactor(int index = -1)
		{
			if (index == -1)
				index = ToolBar.ComboBoxTime.SelectedIndex;

			if (index < 0 || index >= s_speedFactors.Length)
				index = DEFAULT_SPEED_FACTOR_INDEX;

			return s_speedFactors[index];
		}

		public static void GraphTimeBaseChanged(Control ctrl, int width)	//BugFix #0425
		{
			if (ctrl == null)
				return;

			// We need to find the panel that hosts the gate view (spectrum is in a summary screen, we might not have such a panel.)
			ComboBox cbox = null;
			while (ctrl != null)
			{
				var panel = ctrl as Panel;
				if (panel != null)
				{
					cbox = Instance.getTimeComboboxForPanel(panel);
					if (cbox != null)
						break;
				}

				ctrl = ctrl.Parent;
			}

			if (cbox == null)
				return;

			graphTimeBaseChanged(cbox, width);
		}

		public static void ResetMainSpectrumTimeFrame()
		{
			if (Instance == null)
				return;

			Instance.toolBarPanel1.ComboBoxTime.SelectedIndex = DEFAULT_SPEED_FACTOR_INDEX;
		}

		/// <summary>Called whenever the time base of the spectrum is changed due to ChangeSize event.</summary>
		/// <param name="cbox"></param>
		/// <param name="width"></param>
		private static void graphTimeBaseChanged(ComboBox cbox, int width)	//BugFix #0425
		{
			if (cbox == null)
				return;

			if (cbox.Tag != null)
			{
				int prevWidth;
				if (int.TryParse(cbox.Tag.ToString(), out prevWidth))
				{
					if (prevWidth == width)
						return;
				}
			}

			// Add missing speed items 
			for (var i = cbox.Items.Count; i < s_speedFactors.Length; i++)
				cbox.Items.Add(GetSpeedFactor(i));
			
			if (cbox.SelectedIndex < 0)
				cbox.SelectedIndex = DEFAULT_SPEED_FACTOR_INDEX;

			double selectedItemValue;
			double.TryParse(cbox.SelectedItem.ToString(), out selectedItemValue);

			for (var i = s_speedFactors.Length - 1; i >= 0; i--)
			{
				//Because the new time resolution of 2.5 at unilateral full view We need sometimes to show unnatural numbers (Again with kind of rounding)
				var graphTime = width * GlobalSettings.FFT_COLUMN_IN_SEC * GetSpeedFactor(i) / 2;		//BugFix #0425

				if (graphTime < 0.75)				
					graphTime = 0.5;
				else if (graphTime < 1.25)			
					graphTime = 1;
				else if (graphTime < 1.75)			
					graphTime = 1.5;
				else if (graphTime < 2.25)			
					graphTime = 2;
				else if (graphTime < 2.75)			
					graphTime = 2.5;
				else								
					graphTime = Math.Round(graphTime);

				cbox.Items[i] = graphTime;
			}

			if (Instance.IsInitialized)
				setBaseTime(cbox, selectedItemValue);
			else
				cbox.SelectedIndex = DEFAULT_SPEED_FACTOR_INDEX;

			if (cbox.SelectedIndex == -1 && cbox.Items.Count > 0)
				cbox.SelectedIndex = cbox.Items.Count - 1;

			// Save selected value
			cbox.Tag = width.ToString();
		}
    }
}
