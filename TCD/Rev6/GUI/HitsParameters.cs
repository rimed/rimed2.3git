using System;

namespace Rimed.TCD.GUI
{
    public class HitsParameters
    {
        /// <summary>Constructor</summary>
        public HitsParameters(int step, int rate, int threshold)
        {
            Step = step;
            Rate = rate;
            Threshold = threshold;
        }

        public int Step { get; private set; }

        public int Rate { get; private set; }

        public int Threshold { get; set; }
    }
}