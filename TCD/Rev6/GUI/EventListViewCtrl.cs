using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using Rimed.Framework.Common;
using Rimed.Framework.GUI;
using Rimed.Framework.Threads;
using Rimed.TCD.DAL;
using Rimed.TCD.DSPData;
using Rimed.TCD.GUI.DSPMgr;
using Rimed.TCD.Utils;

namespace Rimed.TCD.GUI
{
    /// <summary>Manages event functionality and UI. The user can insert events during monitoring examination using this control He can also "Jump" to events using the bottom list at LODING mode</summary>
    public partial class EventListViewCtrl : UserControl
    {
        private const int		EVENT_PLAY_MIN_TIME_SEC = 5;
        private const string	CLASS_NAME = "EventListViewCtrl";
        #region Private Fields

        private readonly List<SingleHistoryEvent> m_addedEventsHistory = new List<SingleHistoryEvent>();
        private readonly List<SingleHistoryEvent> m_deletedEventsHistory = new List<SingleHistoryEvent>();
        private bool m_eventListSaved = true;

        private Double m_baselineVelocityPrimary = 0;
        private Double m_baselineVelocitySecondary = 0;

        private volatile Stack<SingleHistoryEvent> m_captureEvents = new Stack<SingleHistoryEvent>();
        private Thread m_captureEventsThread = null;
        private readonly object m_lockCaptureEvents = new object();

        #endregion Private Fields

        #region Public Properties

        [Browsable(false)]
        public double VMRPrimary { get; private set; }

        [Browsable(false)]
        public double VMRSecondary { get; private set; }

        [Browsable(false)]
        public SingleHistoryEvent LastStopStimulationEvent
        {
            get
            {
                for (int i = HistoryEvents.Count - 1; i > -1; i--)
                {
                    if (HistoryEvents[i].EventName.Equals("Stop Stimulation"))
                        return HistoryEvents[i];
                }

                return null;
            }
        }

        public static EventListViewCtrl Instance { get; private set; }

        [Browsable(false)]
        public List<SingleEvent> PredefinedEvents { get; private set; }

        [Browsable(false)]
        public List<SingleHistoryEvent> HistoryEvents { get; private set; }

        [Browsable(false)]
        public bool EventListSaved
        {
            get
            {
                return m_eventListSaved;
            }
            set
            {
                m_eventListSaved = value;
                if (MainForm.Instance != null)  
                    MainForm.ToolBar.ButtonSaveCtrl.Enabled = !value;

                if (value)
                {
                    m_addedEventsHistory.Clear();
                    m_deletedEventsHistory.Clear();
                }
            }
        }

        /// <summary>Returns selected event, or null if there is no selected events or event list is empty.</summary>
        /// <returns>Selected event</returns>
        [Browsable(false)]
        internal SingleHistoryEvent SelectedEvent { get; private set; }

        [Browsable(false)]
        internal DateTime ExaminationDate { get; set; }

        #endregion Public Properties

        #region Public Methods

		private delegate void AddHistoryDelegate(string eventName, GlobalTypes.EEventType eventType, int probSide, bool postEvent, int indexInFile, int fileIndex, double msecOffset);
		internal void AddHistoryEvent(string eventName, GlobalTypes.EEventType eventType, int probSide, bool postEvent, int indexInFile, int fileIndex, double msecOffset = -1.0)
        {


            if (!MainForm.Instance.IsRecordMode && LayoutManager.SystemLayoutMode != GlobalTypes.ESystemLayoutMode.MonitoringLoad)
                return;

            if (InvokeRequired)
            {
				BeginInvoke((AddHistoryDelegate)AddHistoryEvent, eventName, eventType, probSide, postEvent, indexInFile, fileIndex, msecOffset);
                return;
            }

            if (!MainForm.Instance.IsRecordMode && LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Monitoring)
            {
                LoggedDialog.Show(MainForm.Instance,MainForm.StringManager.GetString("Adding events is enabled just in record mode please press the record button to use this feature"), MainForm.StringManager.GetString("Adding Events"), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

			var examRow = RimedDal.Instance.LoadedExamination();
			if (examRow == null)
				return;

			var examGuid			= examRow.Examination_Index;	
			var examStartTime		= examRow.Date;					

            DateTime eventTime;
            var newLastLeftPositionPainted = LayoutManager.Instance.Trends[0].Graph.LastLeftPositionPainted;
            if (LayoutManager.SystemLayoutMode != GlobalTypes.ESystemLayoutMode.MonitoringLoad)
            {
                eventTime = DateTime.Now;

                if (eventName.CompareTo("BaseLine Velocity") == 0)
                    setBaselineVelocity();

                if (eventName.CompareTo("Test Velocity") == 0)
                    setVMRValue();

                if (eventName.CompareTo("Start Stimulation") == 0)
                    LayoutManager.Instance.SetStartPeakValue();

                if (eventName.CompareTo("Stop Stimulation") == 0)
                    LayoutManager.Instance.SetStopPeakValue();
            }
            else
            {
	            if (MainForm.Instance.MonitoringStartTime == DateTime.MinValue || msecOffset < 0)
		            eventTime	= MainForm.Instance.MonitoringReplayTimeCounter;
	            else
		            eventTime	= MainForm.Instance.MonitoringStartTime.AddMilliseconds(msecOffset);

				fileIndex	= ExaminationRawData.GetFileIndex((int) Math.Round(msecOffset));
				indexInFile = ExaminationRawData.GetIndexInFile((int)Math.Round(msecOffset));
            }

            //var currentGv   = LayoutManager.Instance.SelectedGv;
            var hitType     = GlobalTypes.EFHitType.None;
            if (eventType == GlobalTypes.EEventType.HitEvent)
            {
				if (probSide == Probe.PROB_SIDE_RIGHT)//eventType.HasFlag(GlobalTypes.EFHitType.RightProbe)
					hitType |= GlobalTypes.EFHitType.RightProbe;

                if (postEvent)
                    hitType |= GlobalTypes.EFHitType.Manual;
            }

			SingleHistoryEvent historyEvent;

            if (MainForm.Instance.CurrentStudyName == Constants.StudyType.STUDY_EVOKED_FLOW_UNILATERAL)
				historyEvent = new SingleHistoryEvent(Guid.NewGuid(), examGuid, fileIndex, indexInFile, eventTime, false, eventType, eventName, VMRPrimary, VMRSecondary, newLastLeftPositionPainted, LayoutManager.Instance.Trends[0].EvokedFlowPeak, 0, true, hitType, examStartTime);
            else if (MainForm.Instance.CurrentStudyName == Constants.StudyType.STUDY_EVOKED_FLOW_BILATERAL)
				historyEvent = new SingleHistoryEvent(Guid.NewGuid(), examGuid, fileIndex, indexInFile, eventTime, false, eventType, eventName, VMRPrimary, VMRSecondary, newLastLeftPositionPainted, LayoutManager.Instance.Trends[0].EvokedFlowPeak, LayoutManager.Instance.Trends[1].EvokedFlowPeak, true, hitType, examStartTime);
            else
				historyEvent = new SingleHistoryEvent(Guid.NewGuid(), examGuid, fileIndex, indexInFile, eventTime, false, eventType, eventName, VMRPrimary, VMRSecondary, newLastLeftPositionPainted, 0, 0, false, hitType, examStartTime);

            insertHistoryEvent(historyEvent);

            // Changed by Alex 21.12.2015 v. 2.2.3.17 feature # 670
            if (!MainForm.Instance.CurrentStudyName.StartsWith(Constants.StudyType.STUDY_MONITORING_INTRACRANIAL) && 
                !MainForm.Instance.CurrentStudyName.StartsWith(Constants.StudyType.STUDY_VMR))
                return;

            // Copy clinical bar values to Summary View
            foreach (var gv in LayoutManager.Instance.GateViews.Where(gv => gv.IsMainSpectrum))
            {
                foreach (dsGateExamination.tb_GateExaminationRow gateRow in RimedDal.Instance.GateExaminationsDS.tb_GateExamination.Rows)
                {
                    if (gateRow.Gate_Index == gv.GateIndex)
                    {
                        SummaryScreen.UpdateSummaryScreenWithNewValues(gateRow, gv.Spectrum.ClinicalBar1);
                        break;
                    }
                }
            }

            var isQueueCapture = (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Monitoring && LayoutManager.SystemMode == GlobalTypes.ESystemModes.Online);
            if (isQueueCapture)
            {
                lock (m_lockCaptureEvents)
                {
                    m_captureEvents.Push(historyEvent);
                }
            }
            else
                captureEvent(historyEvent);
        }


	    private ListViewItem gethistoryEventListItem(SingleHistoryEvent historyEvent)
	    {
		    var item = new ListViewItem();
		    if (UseRelativeTime && historyEvent.ElapseTime > TimeSpan.Zero)
			    item.Text = historyEvent.ElapseTime.ToString(@"hh\:mm\:ss");
		    else
			    item.Text = historyEvent.EventTime.ToLongTimeString();


		    var name = historyEvent.EventName;
			if (historyEvent.IsHitEvent && LayoutManager.IsBilateralStudy)
		    {
			    if (historyEvent.HitType.HasFlag(GlobalTypes.EFHitType.RightProbe))
					name += " [Right]";
				else
					name += " [Left]";
			}
	    
			item.Font = GlobalSettings.FONT_12_ARIAL_BOLD;
			item.SubItems.Add(name);
            item.Name = historyEvent.EventIndex.ToString();


            return item;
        }

        public bool UseRelativeTime
        {
            get { return m_useRelativeTime; }
            set
            {
                if (value == m_useRelativeTime)
                    return;

                m_useRelativeTime = value;
                refreshEventItems();
            }
        }


        public void StopEventCapture()
        {
            m_captureEventThreadContinue = false;
        }

        private void refreshEventItems()
        {
            if (InvokeRequired)
            {
                BeginInvoke((Action) refreshEventItems);
                return;
            }

            foreach (var hisEvt in HistoryEvents)
            {
                var item = listView2.Items[hisEvt.EventIndex.ToString()];
                if (UseRelativeTime && hisEvt.ElapseTime > TimeSpan.Zero)
                    item.Text = hisEvt.ElapseTime.ToString(@"hh\:mm\:ss");
                else
                    item.Text = hisEvt.EventTime.ToLongTimeString();
            }
        }


        private void insertHistoryEvent(SingleHistoryEvent historyEvent)
        {
            var item = gethistoryEventListItem(historyEvent);
            
            var eventPosition = 0;

            while (eventPosition < HistoryEvents.Count && HistoryEvents[eventPosition].EventTime < historyEvent.EventTime)
                eventPosition++;

            listView2.Items.Insert(eventPosition, item);
            listView2.SelectedIndices.Clear();
            SelectedEvent = null;
            HistoryEvents.Insert(eventPosition, historyEvent);
            m_addedEventsHistory.Add(historyEvent);

			var isReplay = (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.DiagnosticLoad || LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.MonitoringLoad); //OFER.Enable load study edit: var isReplay = (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.DiagnosticLoad || LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.MonitoringLoad);
			var isManualHit = (historyEvent.EventType == GlobalTypes.EEventType.HitEvent && historyEvent.HitType.HasFlag(GlobalTypes.EFHitType.Manual)); //BugFix #0431

			if (!isReplay || isManualHit) //if (isManualHit) //OFER.Enable load study edit: if (!isReplay || isManualHit)
					EventListSaved = false;
        }

	    private void loadEventHistoryList(Guid examinationId)
        {
            DateTime? examStartTime = null;
            var exame = RimedDal.Instance.GetExaminationByIndex(examinationId);
            if (exame != null)
                examStartTime = (DateTime)exame["Date"];

            var ds = RimedDal.Instance.GetEventHistoryByExaminationID(examinationId);
            foreach (dsEventExamination.tb_ExaminationEventRow row in ds.tb_ExaminationEvent.Rows)
            {
                SingleHistoryEvent hisEv;
                //Changed by Alex Bug #860 v.2.2.3.34 10/05/2016
                if (row["EventName"].ToString().EndsWith("Stimulation") && 
                    row["ExtraInfo4"] != Convert.DBNull && row["ExtraInfo5"] != Convert.DBNull)
                {
                    hisEv = new SingleHistoryEvent((Guid)row["Event_Index"], (Guid)row["Examination_Index"], (Int16)row["FileIndex"]
                            , (Int32)row["IndexInFile"], (DateTime)row["EventTime"], (bool)row["Deleted"], (GlobalTypes.EEventType)(Int16)row["EventType"], (string)row["EventName"], (System.Double)row["ExtraInfo1"], (System.Double)row["ExtraInfo2"], (System.Double)row["ExtraInfo3"]
                            , Convert.ToInt16(row["ExtraInfo4"]), Convert.ToInt16(row["ExtraInfo5"]), true, (GlobalTypes.EFHitType)Convert.ToInt16(row["HitType"]), examStartTime);
                }
                else
                {
                    hisEv = new SingleHistoryEvent((Guid)row["Event_Index"], (Guid)row["Examination_Index"], (Int16)row["FileIndex"]
                        , (Int32)row["IndexInFile"], (DateTime)row["EventTime"], (bool)row["Deleted"], (GlobalTypes.EEventType)(Int16)row["EventType"], (string)row["EventName"], (System.Double)row["ExtraInfo1"], (System.Double)row["ExtraInfo2"], (System.Double)row["ExtraInfo3"]
                        , 0, 0, false, (GlobalTypes.EFHitType)Convert.ToInt16(row["HitType"]), examStartTime);
                }
				hisEv.HitEnergy		= (double)row["HitEnergy"];
				hisEv.HitVelocity	= (double)row["HitVelocity"];
                addHistoryEvent2List(hisEv);
            }
            updateHistoryEventList();
        }

        internal void LoadPredefinedEvents()
        {
            loadPredefinedEvents(MainForm.Instance.CurrentStudyId);
        }

        private void loadPredefinedEvents(Guid currentStudyId)
        {
            var dv = RimedDal.Instance.GetEventsBySetupStudyIndex(currentStudyId);
            clearPredefinedEventsList();
            foreach (DataRowView row in dv)
            {
                var s = new SingleEvent((Guid)row["Event_Index"], (string)row["Name"]);
                addPredefinedEvent(s);
            }
            
            updatePredefinedEventList();
            lock (m_lockCaptureEvents)
            {
                m_captureEvents.Clear();
            }
        }

        private void captureEvent(SingleHistoryEvent historyEvent)
        {
            var gates = LayoutManager.Instance.GateViews.ToArray();
            string depthStr="";
            foreach (var gv in gates)
            {
                if (gv.IsMainSpectrum)
                {
                    gv.GenerateTmpImage();
                    // get depth value from GUI thread synchronic
                    if (InvokeRequired)
                    {
                        Invoke((Action)(() => { depthStr = gv.DopplerBar.DepthVar.ToString(); }));
                    }
                    else
                        depthStr = gv.DopplerBar.DepthVar.ToString();
                    SummaryScreen.SaveImage2HD(gv, historyEvent.EventIndex, depthStr);
                }
            }
        }

        internal void SaveEventHistory()
        {
            if (LayoutManager.SystemLayoutMode != GlobalTypes.ESystemLayoutMode.MonitoringLoad && LayoutManager.SystemLayoutMode != GlobalTypes.ESystemLayoutMode.Monitoring)
                return;

            if (EventListSaved || RimedDal.Instance.ExaminationsDS.tb_Examination.Rows.Count == 0)
                return;

			var examRow = RimedDal.Instance.LoadedExamination();
			if (examRow == null)
				return;

	        RimedDal.Instance.GetEventHistoryByExaminationID(examRow.Examination_Index);	//RimedDal.Instance.GetEventHistoryByExaminationID((Guid)RimedDal.Instance.ExaminationsDS.tb_Examination.Rows[0]["Examination_Index"]);	

            for (int i = 0; i < HistoryEvents.Count; i++)
            {
                RimedDal.Instance.AddHistoryEvent2Study(HistoryEvents[i].EventIndex, HistoryEvents[i].ExaminationIndex, HistoryEvents[i].GateIndex,
                    HistoryEvents[i].FileIndex, HistoryEvents[i].IndexInFile, HistoryEvents[i].EventTime,
                    HistoryEvents[i].Deleted, (int)HistoryEvents[i].EventType, HistoryEvents[i].EventName, (int)HistoryEvents[i].HitType, HistoryEvents[i].HitVelocity,
                    HistoryEvents[i].HitEnergy, HistoryEvents[i].AlarmValue, HistoryEvents[i].VmrPrimary, HistoryEvents[i].VmrSecondary, HistoryEvents[i].TrendSiganlIndexAtEventTime,
                    HistoryEvents[i].FlowChangePrimary, HistoryEvents[i].FlowChangeSecondary, HistoryEvents[i].EvokedFlowEvent);
            }
            
            for (int i = 0; i < m_deletedEventsHistory.Count; i++)
            {
                RimedDal.Instance.DeleteHistoryEvent(m_deletedEventsHistory[i].EventIndex);
            }

            RimedDal.Instance.SaveEventHistory();
            EventListSaved = true;

            if (!MainForm.Instance.CurrentStudyName.StartsWith(Constants.StudyType.STUDY_MONITORING_INTRACRANIAL))
                return;

            if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.MonitoringLoad)
                RimedDal.Instance.UpdateCurExamModified();	

            waitForEventsSave();
        }

        private bool captureEventsThreadStart()
        {
            m_captureEventsThread = new Thread(captureEventThreadStart);
            m_captureEventsThread.Priority = ThreadPriority.Normal;
            m_captureEventsThread.IsBackground = true;
            m_captureEventsThread.SetName("SaveCaptureEvents");
            m_captureEventsThread.Start();

            return true;
        }

        private bool m_captureEventThreadContinue = true;
        
        /// <summary>= 500 mSec</summary>
        private const int CAPTURE_EVENTS_CAPTURE_SLEEP_MSEC = 500;

        /// <summary>= 1000 mSec</summary>
        private const int CAPTURE_EVENTS_THREAD_SLEEP_MSEC = 1 * 1000;

        /// <summary>= 30 Sec</summary>
        private const int CAPTURE_EVENTS_SAVE_WAIT_TIMEOUT = 30 * 1000;

        private readonly ManualResetEvent m_captureEventSaved = new ManualResetEvent(true);
        private bool m_useRelativeTime;

	    private bool waitForEventsSave(int timeout = CAPTURE_EVENTS_SAVE_WAIT_TIMEOUT)
        {
            Logger.LogTrace(Logger.ETraceLevel.L3, Name, "WaitForEventsSave({0}).", timeout);
            var ret = m_captureEventSaved.WaitOne(timeout);
            Logger.LogTrace(Logger.ETraceLevel.L3, Name, "WaitForEventsSave({0}): {1}.", timeout, ret ? "Signaled" : "TIMEOUT");

            return ret;
        }

        private void captureEventThreadStart()
        {
            Logger.LogInfo("{0}.captureEventThreadStart: START", Name);

            m_captureEventThreadContinue = true;
            while (m_captureEventThreadContinue)
            {
                try
                {
                    if (m_captureEvents.Count > 0)
                    {
                        // Block waiting threads
                        m_captureEventSaved.Reset();

                        // Save events
                        while (m_captureEvents.Count > 0)
                        {
                            Thread.Sleep(CAPTURE_EVENTS_CAPTURE_SLEEP_MSEC);

                            SingleHistoryEvent hisEvt;
                            lock (m_lockCaptureEvents)
                            {
                                hisEvt = m_captureEvents.Pop();
                            }

                            // Save screenshot
                            captureEvent(hisEvt);
                        }

                        // Release waiting threads
                        m_captureEventSaved.Set();
                    }

                    Thread.Sleep(CAPTURE_EVENTS_THREAD_SLEEP_MSEC);
                }
                catch (ThreadAbortException)
                {
                    Logger.LogInfo("{0}.captureEventThreadStart(): ABORT", Name);
                    StopEventCapture();
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex);
                }
            }

            m_captureEventSaved.Dispose();
            Logger.LogInfo("{0}.captureEventThreadStart(): END", Name);
        }

        internal void DeleteCurEventFiles()
        {
            foreach (var addedEvent in m_addedEventsHistory)
            {
                RimedDal.DeleteHistoryEventFiles(addedEvent.EventIndex);
            }
            EventListSaved = true;
        }

        internal void RewriteVMR(DateTime? currentReplayTime)
        {
            SingleHistoryEvent historyEvent = null;
            if (currentReplayTime.HasValue)
                historyEvent = HistoryEvents.LastOrDefault(item => item.EventType != GlobalTypes.EEventType.HitEvent && item.EventName == "Test Velocity" && item.EventTime <= currentReplayTime);

            if (historyEvent == null)
            {
                VMRPrimary = 0.0;
                VMRSecondary = 0.0;
            }
            else
            {
                VMRPrimary = historyEvent.VmrPrimary;
                VMRSecondary = historyEvent.VmrSecondary;
            }
        }

        internal void ClearEventsHistoryList()
        {
			//Logger.LogInfo("EVENTS: Clear history event list.");
			HistoryEvents.Clear();
            SelectedEvent = null;

            if (InvokeRequired)
                listView2.Invoke((Action)delegate(){listView2.Items.Clear();});    
            else
                listView2.Items.Clear();

            //UseRelativeTime = false;
            EventListSaved  = true;
        }

        internal void GetHitCountByTime(DateTime? dateTime, out int leftProbe, out int rightProbe)
        {
            leftProbe = 0;
            rightProbe = 0;
            if (!dateTime.HasValue) 
                return;

	        var selectedIndex = 0;
			foreach (var historyEvent in HistoryEvents)
            {
				if (historyEvent.EventTime > dateTime)
                    break;

	            selectedIndex++;
                if (historyEvent.EventType == GlobalTypes.EEventType.HitEvent)
                {
                    if (historyEvent.HitType.HasFlag(GlobalTypes.EFHitType.RightProbe))
                        rightProbe++;
                    else
                        leftProbe++;
                }
            }

			if (selectedIndex >= 0 && selectedIndex < listView2.Items.Count)
			{
				listView2.Items[selectedIndex].Selected = true;
				listView2.Items[selectedIndex].EnsureVisible();
			}
        }

        internal void DeleteHistoryEvent()
        {
            //v2.0.4.15, BugFix: #043 - System.ArgumentOutOfRangeException: Index was out of range
            DeleteHistoryEvent(SelectedEvent);
        }

        internal void DeleteHistoryEvent(SingleHistoryEvent deleteEvent)
        {
            if (deleteEvent == null) 
                return;
            
            var index = HistoryEvents.IndexOf(deleteEvent);

            //v2.0.4.15, BugFix: #043 - System.ArgumentOutOfRangeException: Index was out of range
            if (index < 0)
            {
                Logger.LogWarning("DeleteHistoryEvent({0}) fail. Event NOT found.", deleteEvent);
                return;
            }

            if (index < HistoryEvents.Count)
            {
				//Logger.LogInfo("EVENTS: Remove history event from list at {0}.", index);
				HistoryEvents.RemoveAt(index);
            }

            if (index < listView2.Items.Count)
                listView2.Items.RemoveAt(index);
            
            m_deletedEventsHistory.Add(deleteEvent);
            deleteEvent.Deleted = true;                 //Ofer, v2.0.4.16: BugFix #094

            EventListSaved = false;
            SelectedEvent = null;
        }

        internal void LoadExamination(dsExamination.tb_ExaminationRow examRow)
        {
            ClearEventsHistoryList();
            if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Monitoring || LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.MonitoringLoad)
            {
                ExaminationDate = examRow.Date;
                LoadPredefinedEvents();                                 
                loadEventHistoryList(examRow.Examination_Index);       
            }
        }

        internal void AddHistoryEvent()
        {
            if (LayoutManager.Instance == null)
                return;

            if (!MainForm.Instance.IsRecordMode || LayoutManager.SystemMode == GlobalTypes.ESystemModes.Offline) //Fix bug #  - Prevent adding event while study is in offline mode
                return;

            if (listView1.SelectedItems.Count <= 0)
                return;

            var side = Probe.PROB_SIDE_LEFT;
            var elapseMSec = -1.0;
            var currentGv = LayoutManager.Instance.SelectedGv;
            if (currentGv != null)
            {
                if (currentGv.GateSide == GlobalTypes.EGateSide.Right)
                    side = Probe.PROB_SIDE_RIGHT;

                elapseMSec = currentGv.Spectrum.Graph.MasterCursorTime * 1000.0;
            }

            DspManager.Instance.DspMgrAddHistoryEvent(listView1.SelectedItems[0].Text, GlobalTypes.EEventType.PreDefinedEvent, false, side, elapseMSec);

        }

        //move selected event in events list to next item
        internal void NextEvent()
        {
            if (LayoutManager.Instance == null)
                return;

            if (listView1.Items.Count <= 0)
                return;
            int selectedindex = listView1.SelectedItems.Count > 0 ? listView1.SelectedIndices[0] : -1 ;
            if (selectedindex >= 0 && selectedindex < listView1.Items.Count - 1)
                listView1.Items[selectedindex + 1].Selected = true;
            else
                listView1.Items[0].Selected = true;
        }

        #endregion Public Methods

        /// <summary>Constructor</summary>
        public EventListViewCtrl()
        {
            SelectedEvent = null;
            HistoryEvents = new List<SingleHistoryEvent>();
            PredefinedEvents = new List<SingleEvent>();
            VMRSecondary = 0;
            VMRPrimary = 0;
            
            InitializeComponent();

			if (!DesignMode)
			{
				var strRes = MainForm.StringManager;
	            EventName.Text = strRes.GetString("Event");
		        EventTime.Text = strRes.GetString("Time");
			    EventType.Text = strRes.GetString("Event");
			}

            // Init background worker thread
            captureEventsThreadStart();

            Instance = this;
        }

        #region Private Methods

        private void clearPredefinedEventsList()
        {
            PredefinedEvents.Clear();

            if (InvokeRequired)
                listView1.BeginInvoke((Action)delegate(){ listView1.Items.Clear(); });
            else
                listView1.Items.Clear();

//            UseRelativeTime = false;
        }

        /// <summary>This function is called when the user load new study.</summary>
        private void addPredefinedEvent(SingleEvent se)
        {
            PredefinedEvents.Add(se);
        }

        /// <summary>This function fill listview1 control with the BVs items.</summary>
        private void updatePredefinedEventList()
        {
            if (InvokeRequired)
            {
                BeginInvoke((Action)updatePredefinedEventList);
                return;
            }

            listView1.Items.Clear();
            foreach (var se in PredefinedEvents)
            {
                var item = new ListViewItem();
                item.Text = se.Name;
                item.Font = GlobalSettings.FONT_16_ARIAL_BOLD;
                item.SubItems.Add(se.Index.ToString());
                listView1.Items.Add(item);
            }
        }

        private void addHistoryEvent2List(SingleHistoryEvent se)
        {
			//Logger.LogInfo("EVENTS: Add history event to list - {0}.", se);
			HistoryEvents.Add(se);
            if (se.EventName.Equals("Test Velocity", StringComparison.InvariantCultureIgnoreCase))
            {
                VMRPrimary = se.VmrPrimary;
                VMRSecondary = se.VmrSecondary;
            }
        }

        private void updateHistoryEventList()
        {
            if (InvokeRequired)
            {
                BeginInvoke((Action)updateHistoryEventList);
                return;
            }

            listView2.Items.Clear();
            foreach (var se in HistoryEvents)
            {
                var item = gethistoryEventListItem(se);

                listView2.Items.Add(item);
            }
        }

        private void setBaselineVelocity()
        {
            //Peak -> Mean on customer's request
            if (LayoutManager.Instance.GateViews[0].Spectrum.ClinicalBar1.ClinicalParamLabelMeanUpper > 0)
                m_baselineVelocityPrimary = LayoutManager.Instance.GateViews[0].Spectrum.ClinicalBar1.ClinicalParamLabelMeanUpper;
            else
                m_baselineVelocityPrimary = LayoutManager.Instance.GateViews[0].Spectrum.ClinicalBar1.ClinicalParamLabelMeanLower;
            
            if (MainForm.Instance.IsNotUnilateralStudy)
            {
                if (LayoutManager.Instance.GateViews[0].Spectrum.ClinicalBar1.ClinicalParamLabelMeanUpper > 0)
                    m_baselineVelocitySecondary = LayoutManager.Instance.GateViews[1].Spectrum.ClinicalBar1.ClinicalParamLabelMeanUpper;
                else
                    m_baselineVelocitySecondary = LayoutManager.Instance.GateViews[1].Spectrum.ClinicalBar1.ClinicalParamLabelMeanLower;
            }
        }

        private void setVMRValue()
        {
			//RIMD-439: Change VMR formula: VMR = 100 * (Mean-Mean.Base) /Mean.Base
            if (LayoutManager.Instance.GateViews[0].Spectrum.ClinicalBar1.ClinicalParamLabelMeanUpper > 0)
                VMRPrimary = (int)Math.Round((LayoutManager.Instance.GateViews[0].Spectrum.ClinicalBar1.ClinicalParamLabelMeanUpper - m_baselineVelocityPrimary) / m_baselineVelocityPrimary * 100);
            else if (m_baselineVelocityPrimary != 0)
                VMRPrimary = (int)Math.Round((Math.Abs(LayoutManager.Instance.GateViews[0].Spectrum.ClinicalBar1.ClinicalParamLabelMeanLower) - Math.Abs(m_baselineVelocityPrimary)) / System.Math.Abs(m_baselineVelocityPrimary) * 100);
            else
                VMRPrimary = 0;

            LayoutManager.Instance.GateViews[0].Spectrum.ClinicalBar1.ClinicalParamLabelSwLower = VMRPrimary;
            LayoutManager.Instance.GateViews[0].Spectrum.ClinicalBar1.ClinicalParamLabelSwUpper = VMRPrimary;

            if (MainForm.Instance.IsNotUnilateralStudy)
            {
                if (LayoutManager.Instance.GateViews[0].Spectrum.ClinicalBar1.ClinicalParamLabelMeanUpper > 0)
                    VMRSecondary = (int)Math.Round((LayoutManager.Instance.GateViews[1].Spectrum.ClinicalBar1.ClinicalParamLabelMeanUpper - m_baselineVelocitySecondary) / m_baselineVelocitySecondary * 100);
                else if (m_baselineVelocitySecondary != 0)
                    VMRSecondary = (int)Math.Round((Math.Abs(LayoutManager.Instance.GateViews[1].Spectrum.ClinicalBar1.ClinicalParamLabelMeanLower) - Math.Abs(m_baselineVelocitySecondary)) / System.Math.Abs(m_baselineVelocitySecondary) * 100);
                else
                    VMRSecondary = 0;
                LayoutManager.Instance.GateViews[1].Spectrum.ClinicalBar1.ClinicalParamLabelSwLower = VMRSecondary;
                LayoutManager.Instance.GateViews[1].Spectrum.ClinicalBar1.ClinicalParamLabelSwUpper = VMRSecondary;
            }
        }

		private int calcIndexInFileForStartDrawing(int indexInFile, GlobalTypes.EStudyType studyType, int startPlayEventTime)
        {
			//TODO rewrite - recalculate index with shift!!!, change parameters
			if (startPlayEventTime == 0) 
			{
                if (studyType == GlobalTypes.EStudyType.Bilateral) 
				{
				    if (indexInFile < 45) 
					    return 0;
					   
				    return indexInFile - 45;
		        }

                if (indexInFile < 116) 
						  return 0;

                return indexInFile - 116;
		    }

			var secondsShift = startPlayEventTime % RawDataFile.FILE_MAX_DURATION_SEC;	// = startPlayEventTime % DspManager.MONITORING_FILE_TIME_INTERVAL_SEC;
			var indexShift = (int)Math.Round(secondsShift * 1000.0 / RawDataFile.DSP_MESSAGE_DURATION_MSEC);	//= secondsShift * 1000 / DspManager.DSP_INT_CYCLE_MILISEC;	
			
			if (indexShift > indexInFile)
			    return 0;

            return indexInFile - indexShift;
        }

        private int NumberOfColumnToDraw
        {
            get
            {
                //TODO, Ofer: Adopt value to screen time width (current 229 ~ 5Sec)
				//return (MainForm.Instance.IsBilateralStudy) ? 90 : 232;

	            var playFrame = MainForm.Instance.SpectrumTime;// -AppSettings.Active.EventPlayOffsetSeconds;
				if (playFrame < AppSettings.Active.EventPlayOffsetSeconds)
					playFrame = AppSettings.Active.EventPlayOffsetSeconds;

				var res = (int)Math.Round(playFrame * 1000.0 / RawDataFile.DSP_MESSAGE_DURATION_MSEC);	
				

	            return res;
            }
        }

        private void moveToCurrentHistoryEvent()
        {
            var fileIndex                   = SelectedEvent.FileIndex;
            var indexInFile                 = (int)SelectedEvent.IndexInFile;
            var eventTime                   = SelectedEvent.EventTime;
			var indexInFileToStratDrawing   = calcIndexInFileForStartDrawing(indexInFile, MainForm.Instance.StudyType, 0);

            //LayoutManager.Instance.Jump2FileInMonitoringLoadExamination(eventTime); // Changed by Alex 24.08.2014 v. 2.2.0.2 bug # 519
            LayoutManager.Instance.IsOfflineData = true;	
            
            MainForm.ToolBar.ButtonPlayCtrl.Enabled = false;

            LayoutManager.Instance.FireJumpToEvent();
			DspManager.Instance.DspMgrReplayJumpToEvent(new CDspMgrReplayJumpToEventData(fileIndex, indexInFileToStratDrawing, NumberOfColumnToDraw));
            foreach (AutoScanChart autoScanC in LayoutManager.Instance.Autoscans)
            {
                autoScanC.Graph.UpdateDepthLine();
            }
			
            foreach (TrendChart tc in LayoutManager.Instance.Trends)
            {
	            tc.Graph.AbsolutIndex				= SelectedEvent.AbsolutIndex;
                tc.Graph.LastLeftPositionPainted	= SelectedEvent.TrendSiganlIndexAtEventTime;
                tc.Graph.CopyFromFullResolutionToCurrentResolution();
            }
			
            if (SelectedEvent.VmrPrimary > 0)
            {
                LayoutManager.Instance.GateViews[0].Spectrum.ClinicalBar1.ClinicalParamLabelSwLower = SelectedEvent.VmrPrimary;
                LayoutManager.Instance.GateViews[0].Spectrum.ClinicalBar1.ClinicalParamLabelSwUpper = SelectedEvent.VmrPrimary;
            }

            if (HistoryEvents[listView2.SelectedItems[0].Index].VmrSecondary > 0)
            {
                LayoutManager.Instance.GateViews[1].Spectrum.ClinicalBar1.ClinicalParamLabelSwLower = SelectedEvent.VmrSecondary;
                LayoutManager.Instance.GateViews[1].Spectrum.ClinicalBar1.ClinicalParamLabelSwUpper = SelectedEvent.VmrSecondary;
            }
			
            LayoutManager.Instance.IsOfflineData = false;
			
            //Natalie: check for Pause button's pushed position was added In the other case the application stops responding when the pause button is pushed and we push the play button also
            if (!LayoutManager.Instance.IsPaused)
                MainForm.ToolBar.ButtonPlayCtrl.Enabled = true;
        }
        
        private void moveToCurrentEventWithPlayInterval()
        {
            foreach (var gv in LayoutManager.Instance.GateViews)
            {
                gv.Spectrum.Graph.ResetLastHitEvent();
            }
            
            // #830
			var startTime = SelectedEvent.EventTime.AddSeconds(-1 * AppSettings.Active.EventPlayOffsetSeconds);
            //var startTime = SelectedEvent.EventTime.AddSeconds(-1 * RimedDal.GeneralSettings.StartPlayEvent);
			if (startTime < ExaminationDate)
				startTime = ExaminationDate;

			MainForm.ToolBar.ButtonPlayCtrl.Enabled = false;
			LayoutManager.Instance.Jump2FileInMonitoringLoadExamination(startTime);
			LayoutManager.Instance.IsOfflineData = true;
			LayoutManager.Instance.FireJumpToEvent();


			var startOffset = (int)startTime.Subtract(ExaminationDate).TotalMilliseconds;
	        var fileIndex = ExaminationRawData.GetFileIndex(startOffset);
	        var indexInFile = ExaminationRawData.GetIndexInFile(startOffset);

	        //RawDataFile.FILE_DURATION_MSEC;
			DspManager.Instance.DspMgrReplayJumpToEvent(new CDspMgrReplayJumpToEventData(fileIndex, indexInFile, NumberOfColumnToDraw));

			foreach (AutoScanChart autoScanC in LayoutManager.Instance.Autoscans)
            {
                autoScanC.Graph.UpdateDepthLine();
            }

            //calculate new LastLeftPositionPainted 
			var newLastLeftPositionPainted = SelectedEvent.TrendSiganlIndexAtEventTime;
            foreach (var tc in LayoutManager.Instance.Trends)
            {
				tc.Graph.AbsolutIndex				= SelectedEvent.AbsolutIndex;
				tc.Graph.LastLeftPositionPainted	= newLastLeftPositionPainted;
                tc.Graph.CopyFromFullResolutionToCurrentResolution();
            }
            
          	LayoutManager.Instance.IsOfflineData = false;		

            //Natalie: check for Pause button's pushed position was added in the other case the application stops responding when the pause button is pushed and we push the play button also
            if (!LayoutManager.Instance.IsPaused)
                MainForm.ToolBar.ButtonPlayCtrl.Enabled = true;
        }

        #endregion Private Methods

        #region Event Handlers

        private void eventListViewCtrl_SizeChanged(object sender, EventArgs e)
        {
            if (Width < 50)
                Width = 50;

            if (MainForm.Instance != null)                
                MainForm.Instance.ResizePanelsSize();

            var hight = listView1.Parent.Height;
            listView1.Height = hight / 2;
            listView2.Height = hight / 2;

            var widthCtrl = listView1.Width;
            listView1.Columns[0].Width = widthCtrl;
            listView2.Columns[0].Width = widthCtrl / 3;
            listView2.Columns[1].Width = 2* widthCtrl / 3;
        }

        private void listView2_DoubleClick(object sender, EventArgs e)
        {
            if (listView2.SelectedIndices.Count == 0 || HistoryEvents.Count == 0 || listView2.SelectedIndices[0] >= HistoryEvents.Count)
                return;

            SelectedEvent = HistoryEvents[listView2.SelectedIndices[0]];

			if (SelectedEvent != null)
				LoggerUserActions.MouseDoubleClick(string.Format("Goto {0} event: {1}. Event time={2}", SelectedEvent.EventType, SelectedEvent.EventName, SelectedEvent.ElapseTime), Name);

            //RIMD-552: Trend window is not updated (event jumping works incorrect)
            LayoutManager.Instance.CursorsMode = false;

		    if (LayoutManager.SystemMode != GlobalTypes.ESystemModes.Online)
		    {
				using (new CursorSafeChange(this))
				{
					if (MainForm.Instance.CurrentStudyName.StartsWith(Constants.StudyType.STUDY_MONITORING_INTRACRANIAL, StringComparison.InvariantCultureIgnoreCase))
						moveToCurrentEventWithPlayInterval();
					else
						moveToCurrentHistoryEvent();
				}
	        }
        }

        private void listView1_MouseUp(object sender, MouseEventArgs e)
        {
            AddHistoryEvent();
        }

        #endregion Event Handlers
    }
}
