﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using Rimed.TCD.DAL;
using Rimed.TCD.Utils;
using MedSim.XML;

namespace Rimed.TCD.GUI
{
    public partial class PatientDicomWorkList : Form
    {
        public List<PatientDICOMInfo> pdl = null;
        public PatientDICOMInfo current;
        public IniXML iniXML;
        private const string FILENAME = "WorkList.xml";


        public PatientDicomWorkList()
        {
            InitializeComponent();


            // added by Alex 30/08/2015 featere "Use different PACS server for Worklist" v 2.2.3.5
            if (!File.Exists(FILENAME))
                File.Create(FILENAME).Dispose();

            // Added by Alex CR #867  v.2.2.3.37 25/05/2016
            iniXML = new IniXML("WorkList.xml");
            modality.Text = iniXML.Read("WorkList", "modality", "");
            StationNameEdit.Text = iniXML.Read("WorkList", "StationNameEdit", "");
        }


        private void PatientDicomWorkList_Load(object sender, EventArgs e)
        {
            try
            {
                DataTable rq = new DataTable("RQ");
                DataRow rqRow;
                rq.Columns.Add(new DataColumn("ID", typeof (string)));
                rq.Columns.Add(new DataColumn("Last Name", typeof (string)));
                rq.Columns.Add(new DataColumn("First Name", typeof (string)));
                rq.Columns.Add(new DataColumn("Middle Name", typeof (string)));
                rq.Columns.Add(new DataColumn("Title", typeof (string)));
                rq.Columns.Add(new DataColumn("Accession Number", typeof(string)));
                rq.Columns.Add(new DataColumn("Sex", typeof(string)));
                rq.Columns.Add(new DataColumn("DofB", typeof (string)));
                rq.Columns.Add(new DataColumn("Address", typeof (string)));
                rq.Columns.Add(new DataColumn("NN", typeof(string)));

                if (pdl == null)
                    return;

                // Iterate over the query results
                int counter = 0;
                foreach (var item in pdl)
                {
                    rqRow = rq.NewRow();
                    rqRow["ID"] = item.ID;
                    rqRow["Last Name"] = item.LastName;
                    rqRow["First Name"] = item.FirstName;
                    rqRow["Middle Name"] = item.MiddleName;
                    rqRow["Title"] = item.Title;
                    rqRow["Accession Number"] = item.AccessionNumber;
                    rqRow["Sex"] = item.Sex;
                    rqRow["DofB"] = item.DofB;
                    rqRow["Address"] = item.Address;
                    rqRow["NN"] = counter;
                    rq.Rows.Add(rqRow);
                    counter++;
                }

                var _bindingSource = new BindingSource();
                _bindingSource.DataSource = rq;
                dgvQueryResults.DataSource = _bindingSource;

                dgvQueryResults.AutoResizeColumns();

                // added by Alex 23/08/2015 fixed bug #799 v 2.2.3.4
                SetExistPatients();

                dgvQueryResults.CurrentCell = dgvQueryResults.Rows[0].Cells[0];
                dgvQueryResults.Columns[9].Visible = false;
            }
            finally
            {
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (dgvQueryResults.CurrentRow != null)
            {
                int index = Convert.ToInt16(dgvQueryResults.CurrentRow.Cells[9].Value);
                current = pdl[index];
            }
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            current = null;
            Close();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkDateTo.Checked)
            {
                checkDateExact.Checked = false;
                dateEndDate.Enabled = true;
            }
            else
                dateEndDate.Enabled = false;

        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkDateFrom.Checked)
            {
                checkDateExact.Checked = false;
                dateStartDate.Enabled = true;
            }
            else
                dateStartDate.Enabled = false;

        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkDateExact.Checked)
            {
                dateExact.Enabled = true;
                checkDateTo.Checked = false;
                checkDateFrom.Checked = false;
            }
            else
            {
                dateExact.Enabled = false;
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            string dateRange = null;
            if (checkDateExact.Checked)
            {
                dateRange = dateExact.Value.ToString("yyyyMMdd");
            }
            else
            {
                if (checkDateTo.Checked && checkDateFrom.Checked)
                {
                    dateRange = dateStartDate.Value.ToString("yyyyMMdd") + "-" + dateEndDate.Value.ToString("yyyyMMdd");
                }
                else if (checkDateFrom.Checked && !checkDateTo.Checked)
                {
                    dateRange = dateStartDate.Value.ToString("yyyyMMdd") + "-";
                }
                else if (!checkDateTo.Checked && checkDateFrom.Checked)
                {
                    dateRange = "-" + dateEndDate.Value.ToString("yyyyMMdd");
                }
            }

            // added by Alex 03/08/2015 featere "Use different PACS server for Worklist" v 2.2.3.5
            PacsManager.WorkListIP = iniXML.Read("PACS", "IP", "10.0.0.17");
            PacsManager.WorkListPort = iniXML.Read("PACS", "Port", "104");
            PacsManager.WorkListLAE = iniXML.Read("PACS", "LocalAE", "localAE");
            PacsManager.WorkListRAE = iniXML.Read("PACS", "RemoteAE", "remoteAE");

            this.pdl = PacsManager.GetDICOMWorkList(modality.Text, StationNameEdit.Text, dateRange);
            PatientDicomWorkList_Load(sender, e);
        }

        // added by Alex 03/01/2016 featere #823 v 2.2.3.19
        private bool isChangeText = true;


        // added by Alex 31/08/2015 featere "Find in WorkList" v 2.2.3.5
        private void dgvQueryResults_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvQueryResults.CurrentCell != null && isChangeText)
            {
                string str = dgvQueryResults.Columns[dgvQueryResults.CurrentCell.ColumnIndex].HeaderText;
                lblSearch.Text = str;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            // added by Alex 03/01/2016 featere #823 v 2.2.3.19
            isChangeText = false;
            if (dgvQueryResults.CurrentCell == null)
                dgvQueryResults.CurrentCell = dgvQueryResults.Rows[0].Cells[0];

            int index = dgvQueryResults.CurrentCell.ColumnIndex;
            BindingSource bs = new BindingSource();
            bs.DataSource = dgvQueryResults.DataSource;
            string str = dgvQueryResults.Columns[index].HeaderText;
            bs.Filter = "[" + str + "] LIKE '%" + textBox1.Text + "%'";
            dgvQueryResults.DataSource = bs;

            dgvQueryResults.CurrentCell = dgvQueryResults.Rows[0].Cells[index];
            SetExistPatients();
            // added by Alex 03/01/2016 featere #823 v 2.2.3.19
            isChangeText = true;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
                button2_Click(sender, e);
        }


        private void dgvQueryResults_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            SetExistPatients();
        }

        private void SetExistPatients()
        {
            for (int i = 0; i < dgvQueryResults.Rows.Count; i++)
            {
                var filter = "Select * FROM tb_Patient WHERE (ID = '" + dgvQueryResults.Rows[i].Cells[0].Value + "')";
                dsPatient pat = RimedDal.Instance.GetPatients(filter);
                if (pat.tb_Patient.Rows.Count == 1)
                    dgvQueryResults.Rows[i].HeaderCell.Value = "*";

            }
        }

        // Added by Alex CR #867  v.2.2.3.37 25/05/2016
        private void PatientDicomWorkList_FormClosed(object sender, FormClosedEventArgs e)
        {
            iniXML.Write("WorkList", "modality", modality.Text);
            iniXML.Write("WorkList", "StationNameEdit", StationNameEdit.Text);
        }
    }
}
