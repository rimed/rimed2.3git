namespace Rimed.TCD.GUI
{
    partial class PatientSearch
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //				if(components != null)
                //				{
                //					components.Dispose();
                //				}
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>

        private void InitializeComponent()
        {
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.gradientPanel4 = new Syncfusion.Windows.Forms.Tools.GradientPanel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.buttonShowGate = new System.Windows.Forms.Button();
            this.listViewGate = new System.Windows.Forms.ListView();
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOK = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonBVView = new System.Windows.Forms.Button();
            this.listViewBV = new System.Windows.Forms.ListView();
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonExaminationView = new System.Windows.Forms.Button();
            this.listViewExamination = new System.Windows.Forms.ListView();
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonShowReport = new System.Windows.Forms.Button();
            this.buttonDeleteExam = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.autoCompleteComboBoxName = new Rimed.TCD.GUI.AutoCompleteComboBox();
            this.buttonPatientDetails = new System.Windows.Forms.Button();
            this.lbIDNumber = new Syncfusion.Windows.Forms.Tools.AutoLabel();
            this.lbName = new Syncfusion.Windows.Forms.Tools.AutoLabel();
            this.autoCompleteComboBoxID = new Rimed.TCD.GUI.AutoCompleteComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.autoLabel6 = new Syncfusion.Windows.Forms.Tools.AutoLabel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.autoLabel7 = new Syncfusion.Windows.Forms.Tools.AutoLabel();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.buttonHelp = new System.Windows.Forms.Button();
            this.xpTaskBar1 = new Syncfusion.Windows.Forms.Tools.XPTaskBar();
            this.xpTaskBarBoxSearch = new Syncfusion.Windows.Forms.Tools.XPTaskBarBox();
            this.gradientPanel1 = new Syncfusion.Windows.Forms.Tools.GradientPanel();
            this.lbID = new Syncfusion.Windows.Forms.Tools.AutoLabel();
            this.textBoxID = new System.Windows.Forms.TextBox();
            this.lbFirstName = new Syncfusion.Windows.Forms.Tools.AutoLabel();
            this.textBoxFirstName = new System.Windows.Forms.TextBox();
            this.lbLastName = new Syncfusion.Windows.Forms.Tools.AutoLabel();
            this.textBoxFamilyName = new System.Windows.Forms.TextBox();
            this.xpTaskBarBoxDetails = new Syncfusion.Windows.Forms.Tools.XPTaskBarBox();
            this.gradientPanel7 = new Syncfusion.Windows.Forms.Tools.GradientPanel();
            this.textBoxReason = new System.Windows.Forms.TextBox();
            this.textBoxRefferedBy = new System.Windows.Forms.TextBox();
            this.checkBoxRefferedBy = new System.Windows.Forms.CheckBox();
            this.checkBoxReason = new System.Windows.Forms.CheckBox();
            this.checkBoxAge = new System.Windows.Forms.CheckBox();
            this.checkBoxSex = new System.Windows.Forms.CheckBox();
            this.doubleTextBoxTo = new Syncfusion.Windows.Forms.Tools.DoubleTextBox();
            this.doubleTextBoxFrom = new Syncfusion.Windows.Forms.Tools.DoubleTextBox();
            this.lbAgeTo = new Syncfusion.Windows.Forms.Tools.AutoLabel();
            this.comboBoxSex = new Syncfusion.Windows.Forms.Tools.ComboBoxAdv();
            this.xpTaskBarBoxExamination = new Syncfusion.Windows.Forms.Tools.XPTaskBarBox();
            this.gradientPanel6 = new Syncfusion.Windows.Forms.Tools.GradientPanel();
            this.comboBoxType = new System.Windows.Forms.ComboBox();
            this.dateTimePickerExtTo = new Syncfusion.Windows.Forms.Tools.DateTimePickerAdv();
            this.lbTo = new System.Windows.Forms.Label();
            this.lbFrom = new System.Windows.Forms.Label();
            this.textBoxDrName = new System.Windows.Forms.TextBox();
            this.checkBoxDrName = new System.Windows.Forms.CheckBox();
            this.textBoxLocation = new System.Windows.Forms.TextBox();
            this.checkBoxLocation = new System.Windows.Forms.CheckBox();
            this.textBoxType = new System.Windows.Forms.TextBox();
            this.checkBoxType = new System.Windows.Forms.CheckBox();
            this.textBoxOther = new System.Windows.Forms.TextBox();
            this.checkBoxDate = new System.Windows.Forms.CheckBox();
            this.checkBoxOther = new System.Windows.Forms.CheckBox();
            this.dateTimePickerExtFrom = new Syncfusion.Windows.Forms.Tools.DateTimePickerAdv();
            this.xpTaskBarBoxHistory = new Syncfusion.Windows.Forms.Tools.XPTaskBarBox();
            this.gradientPanel2 = new Syncfusion.Windows.Forms.Tools.GradientPanel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.rbArrithymia_No = new System.Windows.Forms.RadioButton();
            this.rbArrithymia_Yes = new System.Windows.Forms.RadioButton();
            this.rbArrithymia_DC = new System.Windows.Forms.RadioButton();
            this.panel21 = new System.Windows.Forms.Panel();
            this.radioButton52 = new System.Windows.Forms.RadioButton();
            this.radioButton53 = new System.Windows.Forms.RadioButton();
            this.radioButton54 = new System.Windows.Forms.RadioButton();
            this.lbArrhythmia = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.rbCBP_No = new System.Windows.Forms.RadioButton();
            this.rbCBP_Yes = new System.Windows.Forms.RadioButton();
            this.rbCBP_DC = new System.Windows.Forms.RadioButton();
            this.panel19 = new System.Windows.Forms.Panel();
            this.radioButton46 = new System.Windows.Forms.RadioButton();
            this.radioButton47 = new System.Windows.Forms.RadioButton();
            this.radioButton48 = new System.Windows.Forms.RadioButton();
            this.lbCoronaryByPass = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.rbPHV_No = new System.Windows.Forms.RadioButton();
            this.rbPHV_Yes = new System.Windows.Forms.RadioButton();
            this.rbPHV_DC = new System.Windows.Forms.RadioButton();
            this.panel17 = new System.Windows.Forms.Panel();
            this.radioButton40 = new System.Windows.Forms.RadioButton();
            this.radioButton41 = new System.Windows.Forms.RadioButton();
            this.radioButton42 = new System.Windows.Forms.RadioButton();
            this.lbHeartValves = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.rbAF_No = new System.Windows.Forms.RadioButton();
            this.rbAF_Yes = new System.Windows.Forms.RadioButton();
            this.rbAF_DC = new System.Windows.Forms.RadioButton();
            this.panel15 = new System.Windows.Forms.Panel();
            this.radioButton34 = new System.Windows.Forms.RadioButton();
            this.radioButton35 = new System.Windows.Forms.RadioButton();
            this.radioButton36 = new System.Windows.Forms.RadioButton();
            this.lbAtrialFibrillation = new System.Windows.Forms.Label();
            this.panel12 = new System.Windows.Forms.Panel();
            this.rbCVA_No = new System.Windows.Forms.RadioButton();
            this.rbCVA_Yes = new System.Windows.Forms.RadioButton();
            this.rbCVA_DC = new System.Windows.Forms.RadioButton();
            this.panel13 = new System.Windows.Forms.Panel();
            this.radioButton28 = new System.Windows.Forms.RadioButton();
            this.radioButton29 = new System.Windows.Forms.RadioButton();
            this.radioButton30 = new System.Windows.Forms.RadioButton();
            this.lbCVA = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.rbTIA_No = new System.Windows.Forms.RadioButton();
            this.rbTIA_Yes = new System.Windows.Forms.RadioButton();
            this.rbTIA_DC = new System.Windows.Forms.RadioButton();
            this.panel11 = new System.Windows.Forms.Panel();
            this.radioButton22 = new System.Windows.Forms.RadioButton();
            this.radioButton23 = new System.Windows.Forms.RadioButton();
            this.radioButton24 = new System.Windows.Forms.RadioButton();
            this.panel8 = new System.Windows.Forms.Panel();
            this.rbACS_No = new System.Windows.Forms.RadioButton();
            this.rbACS_Yes = new System.Windows.Forms.RadioButton();
            this.rbACS_DC = new System.Windows.Forms.RadioButton();
            this.panel9 = new System.Windows.Forms.Panel();
            this.radioButton16 = new System.Windows.Forms.RadioButton();
            this.radioButton17 = new System.Windows.Forms.RadioButton();
            this.radioButton18 = new System.Windows.Forms.RadioButton();
            this.lbTIA = new System.Windows.Forms.Label();
            this.lbACS = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.rbSCS_No = new System.Windows.Forms.RadioButton();
            this.rbSCS_Yes = new System.Windows.Forms.RadioButton();
            this.rbSCS_DC = new System.Windows.Forms.RadioButton();
            this.panel7 = new System.Windows.Forms.Panel();
            this.radioButton10 = new System.Windows.Forms.RadioButton();
            this.radioButton11 = new System.Windows.Forms.RadioButton();
            this.radioButton12 = new System.Windows.Forms.RadioButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.rbHypertension_No = new System.Windows.Forms.RadioButton();
            this.rbHypertension_Yes = new System.Windows.Forms.RadioButton();
            this.rbHypertension_DC = new System.Windows.Forms.RadioButton();
            this.panel5 = new System.Windows.Forms.Panel();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.lbYes = new System.Windows.Forms.Label();
            this.lbNo = new System.Windows.Forms.Label();
            this.lbDontCare = new System.Windows.Forms.Label();
            this.lbSCS = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.rbSmoking_No = new System.Windows.Forms.RadioButton();
            this.rbSmoking_Yes = new System.Windows.Forms.RadioButton();
            this.rbSmoking_DC = new System.Windows.Forms.RadioButton();
            this.lbHypertension = new System.Windows.Forms.Label();
            this.lbSmoking = new System.Windows.Forms.Label();
            this.xpTaskBarBoxBloodVessel = new Syncfusion.Windows.Forms.Tools.XPTaskBarBox();
            this.gradientPanelBV = new Syncfusion.Windows.Forms.Tools.GradientPanel();
            this.xpTaskBarBox3 = new Syncfusion.Windows.Forms.Tools.XPTaskBarBox();
            this.gradientPanel3 = new Syncfusion.Windows.Forms.Tools.GradientPanel();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.autoCompleteDataColumnInfo1 = new Syncfusion.Windows.Forms.Tools.AutoCompleteDataColumnInfo("Column0", 100, true);
            this.gradientPanel4.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xpTaskBar1)).BeginInit();
            this.xpTaskBar1.SuspendLayout();
            this.xpTaskBarBoxSearch.SuspendLayout();
            this.gradientPanel1.SuspendLayout();
            this.xpTaskBarBoxDetails.SuspendLayout();
            this.gradientPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.doubleTextBoxTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.doubleTextBoxFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxSex)).BeginInit();
            this.xpTaskBarBoxExamination.SuspendLayout();
            this.gradientPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateTimePickerExtTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateTimePickerExtTo.Calendar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateTimePickerExtFrom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateTimePickerExtFrom.Calendar)).BeginInit();
            this.xpTaskBarBoxHistory.SuspendLayout();
            this.gradientPanel2.SuspendLayout();
            this.panel20.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel3.SuspendLayout();
            this.xpTaskBarBoxBloodVessel.SuspendLayout();
            this.xpTaskBarBox3.SuspendLayout();
            this.gradientPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(0, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(4, 592);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // gradientPanel4
            // 
            this.gradientPanel4.BorderColor = System.Drawing.Color.Black;
            this.gradientPanel4.Controls.Add(this.groupBox4);
            this.gradientPanel4.Controls.Add(this.buttonCancel);
            this.gradientPanel4.Controls.Add(this.buttonOK);
            this.gradientPanel4.Controls.Add(this.groupBox2);
            this.gradientPanel4.Controls.Add(this.groupBox1);
            this.gradientPanel4.Controls.Add(this.panel1);
            this.gradientPanel4.Controls.Add(this.panel2);
            this.gradientPanel4.Controls.Add(this.buttonHelp);
            this.gradientPanel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.gradientPanel4.Location = new System.Drawing.Point(274, 0);
            this.gradientPanel4.Name = "gradientPanel4";
            this.gradientPanel4.Size = new System.Drawing.Size(712, 592);
            this.gradientPanel4.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.buttonShowGate);
            this.groupBox4.Controls.Add(this.listViewGate);
            this.groupBox4.Location = new System.Drawing.Point(560, 64);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(144, 471);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            // 
            // buttonShowGate
            // 
            this.buttonShowGate.Location = new System.Drawing.Point(8, 434);
            this.buttonShowGate.Name = "buttonShowGate";
            this.buttonShowGate.Size = new System.Drawing.Size(128, 27);
            this.buttonShowGate.TabIndex = 1;
            this.buttonShowGate.Click += new System.EventHandler(this.buttonShowGate_Click);
            // 
            // listViewGate
            // 
            this.listViewGate.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader10,
            this.columnHeader11});
            this.listViewGate.FullRowSelect = true;
            this.listViewGate.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listViewGate.HideSelection = false;
            this.listViewGate.Location = new System.Drawing.Point(8, 28);
            this.listViewGate.MultiSelect = false;
            this.listViewGate.Name = "listViewGate";
            this.listViewGate.Size = new System.Drawing.Size(128, 396);
            this.listViewGate.TabIndex = 0;
            this.listViewGate.UseCompatibleStateImageBehavior = false;
            this.listViewGate.View = System.Windows.Forms.View.Details;
            this.listViewGate.DoubleClick += new System.EventHandler(this.listViewGate_DoubleClick);
            // 
            // columnHeader10
            // 
            this.columnHeader10.Width = 72;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Width = 50;
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(328, 552);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 5;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOK
            // 
            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOK.Location = new System.Drawing.Point(216, 552);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 4;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.buttonBVView);
            this.groupBox2.Controls.Add(this.listViewBV);
            this.groupBox2.Location = new System.Drawing.Point(400, 65);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(152, 471);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            // 
            // buttonBVView
            // 
            this.buttonBVView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonBVView.Location = new System.Drawing.Point(8, 430);
            this.buttonBVView.Name = "buttonBVView";
            this.buttonBVView.Size = new System.Drawing.Size(136, 36);
            this.buttonBVView.TabIndex = 1;
            this.buttonBVView.Click += new System.EventHandler(this.buttonBVView_Click);
            // 
            // listViewBV
            // 
            this.listViewBV.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader6,
            this.columnHeader8});
            this.listViewBV.FullRowSelect = true;
            this.listViewBV.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listViewBV.HideSelection = false;
            this.listViewBV.Location = new System.Drawing.Point(8, 28);
            this.listViewBV.MultiSelect = false;
            this.listViewBV.Name = "listViewBV";
            this.listViewBV.Size = new System.Drawing.Size(128, 396);
            this.listViewBV.TabIndex = 0;
            this.listViewBV.UseCompatibleStateImageBehavior = false;
            this.listViewBV.View = System.Windows.Forms.View.Details;
            this.listViewBV.SelectedIndexChanged += new System.EventHandler(this.listViewBV_SelectedIndexChanged);
            this.listViewBV.DoubleClick += new System.EventHandler(this.listViewBV_DoubleClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.buttonExaminationView);
            this.groupBox1.Controls.Add(this.listViewExamination);
            this.groupBox1.Controls.Add(this.buttonShowReport);
            this.groupBox1.Controls.Add(this.buttonDeleteExam);
            this.groupBox1.Location = new System.Drawing.Point(9, 65);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(392, 471);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // buttonExaminationView
            // 
            this.buttonExaminationView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExaminationView.Location = new System.Drawing.Point(12, 433);
            this.buttonExaminationView.Name = "buttonExaminationView";
            this.buttonExaminationView.Size = new System.Drawing.Size(120, 27);
            this.buttonExaminationView.TabIndex = 1;
            this.buttonExaminationView.Click += new System.EventHandler(this.buttonExaminationView_Click);
            // 
            // listViewExamination
            // 
            this.listViewExamination.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listViewExamination.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader7,
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.listViewExamination.FullRowSelect = true;
            this.listViewExamination.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listViewExamination.HideSelection = false;
            this.listViewExamination.Location = new System.Drawing.Point(9, 28);
            this.listViewExamination.MultiSelect = false;
            this.listViewExamination.Name = "listViewExamination";
            this.listViewExamination.Size = new System.Drawing.Size(375, 396);
            this.listViewExamination.TabIndex = 0;
            this.listViewExamination.UseCompatibleStateImageBehavior = false;
            this.listViewExamination.View = System.Windows.Forms.View.Details;
            this.listViewExamination.SelectedIndexChanged += new System.EventHandler(this.listViewExamination_SelectedIndexChanged);
            this.listViewExamination.DoubleClick += new System.EventHandler(this.listViewExamination_DoubleClick);
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "";
            this.columnHeader7.Width = 20;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Width = 110;
            // 
            // buttonShowReport
            // 
            this.buttonShowReport.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonShowReport.Location = new System.Drawing.Point(139, 433);
            this.buttonShowReport.Name = "buttonShowReport";
            this.buttonShowReport.Size = new System.Drawing.Size(120, 27);
            this.buttonShowReport.TabIndex = 1;
            this.buttonShowReport.Click += new System.EventHandler(this.buttonShowReport_Click);
            // 
            // buttonDeleteExam
            // 
            this.buttonDeleteExam.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonDeleteExam.Location = new System.Drawing.Point(264, 433);
            this.buttonDeleteExam.Name = "buttonDeleteExam";
            this.buttonDeleteExam.Size = new System.Drawing.Size(120, 27);
            this.buttonDeleteExam.TabIndex = 1;
            this.buttonDeleteExam.Click += new System.EventHandler(this.buttonDeleteExam_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.autoCompleteComboBoxName);
            this.panel1.Controls.Add(this.buttonPatientDetails);
            this.panel1.Controls.Add(this.lbIDNumber);
            this.panel1.Controls.Add(this.lbName);
            this.panel1.Controls.Add(this.autoCompleteComboBoxID);
            this.panel1.Location = new System.Drawing.Point(9, 9);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(761, 47);
            this.panel1.TabIndex = 0;
            // 
            // autoCompleteComboBoxName
            // 
            this.autoCompleteComboBoxName.LimitToList = false;
            this.autoCompleteComboBoxName.Location = new System.Drawing.Point(64, 8);
            this.autoCompleteComboBoxName.Name = "autoCompleteComboBoxName";
            this.autoCompleteComboBoxName.Size = new System.Drawing.Size(149, 21);
            this.autoCompleteComboBoxName.TabIndex = 6;
            this.autoCompleteComboBoxName.SelectedIndexChanged += new System.EventHandler(this.autoCompleteComboBoxName_SelectedIndexChanged);
            // 
            // buttonPatientDetails
            // 
            this.buttonPatientDetails.Enabled = false;
            this.buttonPatientDetails.Location = new System.Drawing.Point(456, 8);
            this.buttonPatientDetails.Name = "buttonPatientDetails";
            this.buttonPatientDetails.Size = new System.Drawing.Size(95, 23);
            this.buttonPatientDetails.TabIndex = 2;
            this.buttonPatientDetails.Text = "Details";
            this.buttonPatientDetails.Click += new System.EventHandler(this.buttonPatientDetails_Click);
            // 
            // lbIDNumber
            // 
            this.lbIDNumber.DX = 0;
            this.lbIDNumber.DY = 0;
            this.lbIDNumber.Location = new System.Drawing.Point(211, 7);
            this.lbIDNumber.Name = "lbIDNumber";
            this.lbIDNumber.Size = new System.Drawing.Size(86, 24);
            this.lbIDNumber.TabIndex = 3;
            this.lbIDNumber.Text = "ID Number";
            this.lbIDNumber.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbName
            // 
            this.lbName.DX = 0;
            this.lbName.DY = 0;
            this.lbName.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbName.Location = new System.Drawing.Point(3, 7);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(53, 24);
            this.lbName.TabIndex = 1;
            this.lbName.Text = "Name";
            this.lbName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // autoCompleteComboBoxID
            // 
            this.autoCompleteComboBoxID.LimitToList = true;
            this.autoCompleteComboBoxID.Location = new System.Drawing.Point(296, 8);
            this.autoCompleteComboBoxID.Name = "autoCompleteComboBoxID";
            this.autoCompleteComboBoxID.Size = new System.Drawing.Size(149, 21);
            this.autoCompleteComboBoxID.TabIndex = 6;
            this.autoCompleteComboBoxID.SelectedIndexChanged += new System.EventHandler(this.autoCompleteComboBoxID_SelectedIndexChanged);
            this.autoCompleteComboBoxID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.autoCompleteComboBoxID_KeyPress);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.autoLabel6);
            this.panel2.Controls.Add(this.textBox1);
            this.panel2.Controls.Add(this.autoLabel7);
            this.panel2.Controls.Add(this.textBox2);
            this.panel2.Location = new System.Drawing.Point(9, 9);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(761, 47);
            this.panel2.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(449, 9);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(88, 27);
            this.button1.TabIndex = 4;
            // 
            // autoLabel6
            // 
            this.autoLabel6.DX = -74;
            this.autoLabel6.DY = -3;
            this.autoLabel6.LabeledControl = this.textBox1;
            this.autoLabel6.Location = new System.Drawing.Point(216, 6);
            this.autoLabel6.Name = "autoLabel6";
            this.autoLabel6.Size = new System.Drawing.Size(70, 26);
            this.autoLabel6.TabIndex = 3;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(290, 9);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(140, 20);
            this.textBox1.TabIndex = 2;
            // 
            // autoLabel7
            // 
            this.autoLabel7.DX = -51;
            this.autoLabel7.DY = -3;
            this.autoLabel7.LabeledControl = this.textBox2;
            this.autoLabel7.Location = new System.Drawing.Point(5, 6);
            this.autoLabel7.Name = "autoLabel7";
            this.autoLabel7.Size = new System.Drawing.Size(47, 26);
            this.autoLabel7.TabIndex = 1;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(56, 9);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(149, 20);
            this.textBox2.TabIndex = 0;
            // 
            // buttonHelp
            // 
            this.buttonHelp.Location = new System.Drawing.Point(440, 552);
            this.buttonHelp.Name = "buttonHelp";
            this.buttonHelp.Size = new System.Drawing.Size(75, 23);
            this.buttonHelp.TabIndex = 5;
            // 
            // xpTaskBar1
            // 
            this.xpTaskBar1.AutoPersistStates = true;
            this.xpTaskBar1.AutoScroll = true;
            this.xpTaskBar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(170)))), ((int)(((byte)(230)))));
            this.xpTaskBar1.Controls.Add(this.xpTaskBarBoxSearch);
            this.xpTaskBar1.Controls.Add(this.xpTaskBarBoxDetails);
            this.xpTaskBar1.Controls.Add(this.xpTaskBarBoxExamination);
            this.xpTaskBar1.Controls.Add(this.xpTaskBarBoxHistory);
            this.xpTaskBar1.Controls.Add(this.xpTaskBarBoxBloodVessel);
            this.xpTaskBar1.Controls.Add(this.xpTaskBarBox3);
            this.xpTaskBar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.xpTaskBar1.Location = new System.Drawing.Point(4, 0);
            this.xpTaskBar1.Name = "xpTaskBar1";
            this.xpTaskBar1.Padding = new System.Windows.Forms.Padding(10, 11, 10, 0);
            this.xpTaskBar1.Size = new System.Drawing.Size(262, 592);
            this.xpTaskBar1.TabIndex = 1;
            this.xpTaskBar1.ThemesEnabled = true;
            // 
            // xpTaskBarBoxSearch
            // 
            this.xpTaskBarBoxSearch.Controls.Add(this.gradientPanel1);
            this.xpTaskBarBoxSearch.HeaderImageIndex = -1;
            this.xpTaskBarBoxSearch.ItemBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(170)))), ((int)(((byte)(230)))));
            this.xpTaskBarBoxSearch.Location = new System.Drawing.Point(10, 22);
            this.xpTaskBarBoxSearch.Name = "xpTaskBarBoxSearch";
            this.xpTaskBarBoxSearch.PreferredChildPanelHeight = 95;
            this.xpTaskBarBoxSearch.Size = new System.Drawing.Size(225, 128);
            this.xpTaskBarBoxSearch.TabIndex = 0;
            // 
            // gradientPanel1
            // 
            this.gradientPanel1.BackgroundColor = new Syncfusion.Drawing.BrushInfo(Syncfusion.Drawing.GradientStyle.Vertical, new Syncfusion.Drawing.BrushInfoColorArrayList(new System.Drawing.Color[] {
                System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240))))),
                System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))))}));
            this.gradientPanel1.Border3DStyle = System.Windows.Forms.Border3DStyle.Flat;
            this.gradientPanel1.BorderColor = System.Drawing.Color.Black;
            this.gradientPanel1.Controls.Add(this.lbID);
            this.gradientPanel1.Controls.Add(this.textBoxID);
            this.gradientPanel1.Controls.Add(this.lbFirstName);
            this.gradientPanel1.Controls.Add(this.textBoxFirstName);
            this.gradientPanel1.Controls.Add(this.lbLastName);
            this.gradientPanel1.Controls.Add(this.textBoxFamilyName);
            this.gradientPanel1.Location = new System.Drawing.Point(2, 31);
            this.gradientPanel1.Name = "gradientPanel1";
            this.gradientPanel1.Size = new System.Drawing.Size(221, 95);
            this.gradientPanel1.TabIndex = 0;
            // 
            // lbID
            // 
            this.lbID.BackColor = System.Drawing.Color.Transparent;
            this.lbID.DX = -89;
            this.lbID.DY = 1;
            this.lbID.LabeledControl = this.textBoxID;
            this.lbID.Location = new System.Drawing.Point(4, 63);
            this.lbID.Name = "lbID";
            this.lbID.Size = new System.Drawing.Size(85, 18);
            this.lbID.TabIndex = 5;
            this.lbID.Text = "ID Number";
            // 
            // textBoxID
            // 
            this.textBoxID.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxID.Location = new System.Drawing.Point(93, 62);
            this.textBoxID.Name = "textBoxID";
            this.textBoxID.Size = new System.Drawing.Size(510, 20);
            this.textBoxID.TabIndex = 2;
            // 
            // lbFirstName
            // 
            this.lbFirstName.BackColor = System.Drawing.Color.Transparent;
            this.lbFirstName.DX = -89;
            this.lbFirstName.DY = -1;
            this.lbFirstName.LabeledControl = this.textBoxFirstName;
            this.lbFirstName.Location = new System.Drawing.Point(4, 35);
            this.lbFirstName.Name = "lbFirstName";
            this.lbFirstName.Size = new System.Drawing.Size(85, 22);
            this.lbFirstName.TabIndex = 3;
            this.lbFirstName.Text = "First Name";
            // 
            // textBoxFirstName
            // 
            this.textBoxFirstName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFirstName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxFirstName.Location = new System.Drawing.Point(93, 36);
            this.textBoxFirstName.Name = "textBoxFirstName";
            this.textBoxFirstName.Size = new System.Drawing.Size(510, 20);
            this.textBoxFirstName.TabIndex = 1;
            // 
            // lbLastName
            // 
            this.lbLastName.BackColor = System.Drawing.Color.Transparent;
            this.lbLastName.DX = -89;
            this.lbLastName.DY = 0;
            this.lbLastName.LabeledControl = this.textBoxFamilyName;
            this.lbLastName.Location = new System.Drawing.Point(4, 9);
            this.lbLastName.Name = "lbLastName";
            this.lbLastName.Size = new System.Drawing.Size(85, 21);
            this.lbLastName.TabIndex = 1;
            this.lbLastName.Text = "Last Name";
            // 
            // textBoxFamilyName
            // 
            this.textBoxFamilyName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxFamilyName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxFamilyName.Location = new System.Drawing.Point(93, 9);
            this.textBoxFamilyName.Name = "textBoxFamilyName";
            this.textBoxFamilyName.Size = new System.Drawing.Size(510, 20);
            this.textBoxFamilyName.TabIndex = 0;
            // 
            // xpTaskBarBoxDetails
            // 
            this.xpTaskBarBoxDetails.Controls.Add(this.gradientPanel7);
            this.xpTaskBarBoxDetails.HeaderImageIndex = -1;
            this.xpTaskBarBoxDetails.ItemBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(170)))), ((int)(((byte)(230)))));
            this.xpTaskBarBoxDetails.Location = new System.Drawing.Point(10, 161);
            this.xpTaskBarBoxDetails.Name = "xpTaskBarBoxDetails";
            this.xpTaskBarBoxDetails.PreferredChildPanelHeight = 150;
            this.xpTaskBarBoxDetails.Size = new System.Drawing.Size(225, 183);
            this.xpTaskBarBoxDetails.TabIndex = 1;
            // 
            // gradientPanel7
            // 
            this.gradientPanel7.AutoScroll = true;
            this.gradientPanel7.BackgroundColor = new Syncfusion.Drawing.BrushInfo(Syncfusion.Drawing.GradientStyle.Vertical, new Syncfusion.Drawing.BrushInfoColorArrayList(new System.Drawing.Color[] {
                System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240))))),
                System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))))}));
            this.gradientPanel7.Border3DStyle = System.Windows.Forms.Border3DStyle.Flat;
            this.gradientPanel7.BorderColor = System.Drawing.Color.Black;
            this.gradientPanel7.Controls.Add(this.textBoxReason);
            this.gradientPanel7.Controls.Add(this.textBoxRefferedBy);
            this.gradientPanel7.Controls.Add(this.checkBoxRefferedBy);
            this.gradientPanel7.Controls.Add(this.checkBoxReason);
            this.gradientPanel7.Controls.Add(this.checkBoxAge);
            this.gradientPanel7.Controls.Add(this.checkBoxSex);
            this.gradientPanel7.Controls.Add(this.doubleTextBoxTo);
            this.gradientPanel7.Controls.Add(this.doubleTextBoxFrom);
            this.gradientPanel7.Controls.Add(this.lbAgeTo);
            this.gradientPanel7.Controls.Add(this.comboBoxSex);
            this.gradientPanel7.Location = new System.Drawing.Point(2, 31);
            this.gradientPanel7.Name = "gradientPanel7";
            this.gradientPanel7.Size = new System.Drawing.Size(221, 150);
            this.gradientPanel7.TabIndex = 1;
            // 
            // textBoxReason
            // 
            this.textBoxReason.Enabled = false;
            this.textBoxReason.Location = new System.Drawing.Point(104, 104);
            this.textBoxReason.Name = "textBoxReason";
            this.textBoxReason.Size = new System.Drawing.Size(111, 20);
            this.textBoxReason.TabIndex = 8;
            // 
            // textBoxRefferedBy
            // 
            this.textBoxRefferedBy.Enabled = false;
            this.textBoxRefferedBy.Location = new System.Drawing.Point(103, 72);
            this.textBoxRefferedBy.Name = "textBoxRefferedBy";
            this.textBoxRefferedBy.Size = new System.Drawing.Size(112, 20);
            this.textBoxRefferedBy.TabIndex = 6;
            // 
            // checkBoxRefferedBy
            // 
            this.checkBoxRefferedBy.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxRefferedBy.Location = new System.Drawing.Point(8, 72);
            this.checkBoxRefferedBy.Name = "checkBoxRefferedBy";
            this.checkBoxRefferedBy.Size = new System.Drawing.Size(104, 23);
            this.checkBoxRefferedBy.TabIndex = 5;
            this.checkBoxRefferedBy.Text = "Referred By";
            this.checkBoxRefferedBy.UseVisualStyleBackColor = false;
            this.checkBoxRefferedBy.CheckStateChanged += new System.EventHandler(this.checkBoxRefferedBy_CheckStateChanged_1);
            // 
            // checkBoxReason
            // 
            this.checkBoxReason.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxReason.Location = new System.Drawing.Point(8, 104);
            this.checkBoxReason.Name = "checkBoxReason";
            this.checkBoxReason.Size = new System.Drawing.Size(90, 23);
            this.checkBoxReason.TabIndex = 7;
            this.checkBoxReason.Text = "Reason";
            this.checkBoxReason.UseVisualStyleBackColor = false;
            this.checkBoxReason.CheckStateChanged += new System.EventHandler(this.checkBoxReason_CheckStateChanged_1);
            // 
            // checkBoxAge
            // 
            this.checkBoxAge.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxAge.Location = new System.Drawing.Point(8, 39);
            this.checkBoxAge.Name = "checkBoxAge";
            this.checkBoxAge.Size = new System.Drawing.Size(73, 23);
            this.checkBoxAge.TabIndex = 2;
            this.checkBoxAge.Text = "Age";
            this.checkBoxAge.UseVisualStyleBackColor = false;
            this.checkBoxAge.CheckStateChanged += new System.EventHandler(this.checkBoxAge_CheckStateChanged);
            // 
            // checkBoxSex
            // 
            this.checkBoxSex.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxSex.Location = new System.Drawing.Point(8, 8);
            this.checkBoxSex.Name = "checkBoxSex";
            this.checkBoxSex.Size = new System.Drawing.Size(80, 23);
            this.checkBoxSex.TabIndex = 0;
            this.checkBoxSex.Text = "Sex";
            this.checkBoxSex.UseVisualStyleBackColor = false;
            this.checkBoxSex.CheckStateChanged += new System.EventHandler(this.checkBoxSex_CheckStateChanged);
            // 
            // doubleTextBoxTo
            // 
            this.doubleTextBoxTo.Culture = new System.Globalization.CultureInfo("en-US");
            this.doubleTextBoxTo.DoubleValue = 0D;
            this.doubleTextBoxTo.Enabled = false;
            this.doubleTextBoxTo.ForeColor = System.Drawing.Color.Black;
            this.doubleTextBoxTo.Location = new System.Drawing.Point(177, 40);
            this.doubleTextBoxTo.Name = "doubleTextBoxTo";
            this.doubleTextBoxTo.NullString = "";
            this.doubleTextBoxTo.NumberDecimalDigits = 0;
            this.doubleTextBoxTo.NumberNegativePattern = 0;
            this.doubleTextBoxTo.PositiveColor = System.Drawing.Color.Black;
            this.doubleTextBoxTo.Size = new System.Drawing.Size(38, 20);
            this.doubleTextBoxTo.SpecialCultureValue = Syncfusion.Windows.Forms.Tools.SpecialCultureValues.None;
            this.doubleTextBoxTo.TabIndex = 4;
            // 
            // doubleTextBoxFrom
            // 
            this.doubleTextBoxFrom.Culture = new System.Globalization.CultureInfo("en-US");
            this.doubleTextBoxFrom.DoubleValue = 0D;
            this.doubleTextBoxFrom.Enabled = false;
            this.doubleTextBoxFrom.ForeColor = System.Drawing.Color.Black;
            this.doubleTextBoxFrom.Location = new System.Drawing.Point(103, 40);
            this.doubleTextBoxFrom.Name = "doubleTextBoxFrom";
            this.doubleTextBoxFrom.NullString = "";
            this.doubleTextBoxFrom.NumberDecimalDigits = 0;
            this.doubleTextBoxFrom.NumberNegativePattern = 0;
            this.doubleTextBoxFrom.PositiveColor = System.Drawing.Color.Black;
            this.doubleTextBoxFrom.Size = new System.Drawing.Size(37, 20);
            this.doubleTextBoxFrom.SpecialCultureValue = Syncfusion.Windows.Forms.Tools.SpecialCultureValues.None;
            this.doubleTextBoxFrom.TabIndex = 3;
            // 
            // lbAgeTo
            // 
            this.lbAgeTo.BackColor = System.Drawing.Color.Transparent;
            this.lbAgeTo.DX = 0;
            this.lbAgeTo.DY = 0;
            this.lbAgeTo.Location = new System.Drawing.Point(142, 42);
            this.lbAgeTo.Name = "lbAgeTo";
            this.lbAgeTo.Size = new System.Drawing.Size(29, 15);
            this.lbAgeTo.TabIndex = 6;
            this.lbAgeTo.Text = "To";
            this.lbAgeTo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comboBoxSex
            // 
            this.comboBoxSex.Border3DStyle = System.Windows.Forms.Border3DStyle.Flat;
            this.comboBoxSex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSex.Enabled = false;
            this.comboBoxSex.FlatStyle = Syncfusion.Windows.Forms.Tools.ComboFlatStyle.Flat;
            this.comboBoxSex.Location = new System.Drawing.Point(104, 8);
            this.comboBoxSex.Name = "comboBoxSex";
            this.comboBoxSex.SelectedIndex = -1;
            this.comboBoxSex.Size = new System.Drawing.Size(112, 21);
            this.comboBoxSex.TabIndex = 1;
            // 
            // xpTaskBarBoxExamination
            // 
            this.xpTaskBarBoxExamination.Controls.Add(this.gradientPanel6);
            this.xpTaskBarBoxExamination.HeaderFont = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xpTaskBarBoxExamination.HeaderImageIndex = -1;
            this.xpTaskBarBoxExamination.ItemBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(170)))), ((int)(((byte)(230)))));
            this.xpTaskBarBoxExamination.Location = new System.Drawing.Point(10, 355);
            this.xpTaskBarBoxExamination.Name = "xpTaskBarBoxExamination";
            this.xpTaskBarBoxExamination.PreferredChildPanelHeight = 200;
            this.xpTaskBarBoxExamination.Size = new System.Drawing.Size(225, 233);
            this.xpTaskBarBoxExamination.TabIndex = 2;
            // 
            // gradientPanel6
            // 
            this.gradientPanel6.AutoScroll = true;
            this.gradientPanel6.BackgroundColor = new Syncfusion.Drawing.BrushInfo(Syncfusion.Drawing.GradientStyle.Vertical, new Syncfusion.Drawing.BrushInfoColorArrayList(new System.Drawing.Color[] {
                System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240))))),
                System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))))}));
            this.gradientPanel6.Border3DStyle = System.Windows.Forms.Border3DStyle.Flat;
            this.gradientPanel6.BorderColor = System.Drawing.Color.Black;
            this.gradientPanel6.Controls.Add(this.comboBoxType);
            this.gradientPanel6.Controls.Add(this.dateTimePickerExtTo);
            this.gradientPanel6.Controls.Add(this.lbTo);
            this.gradientPanel6.Controls.Add(this.lbFrom);
            this.gradientPanel6.Controls.Add(this.textBoxDrName);
            this.gradientPanel6.Controls.Add(this.checkBoxDrName);
            this.gradientPanel6.Controls.Add(this.textBoxLocation);
            this.gradientPanel6.Controls.Add(this.checkBoxLocation);
            this.gradientPanel6.Controls.Add(this.textBoxType);
            this.gradientPanel6.Controls.Add(this.checkBoxType);
            this.gradientPanel6.Controls.Add(this.textBoxOther);
            this.gradientPanel6.Controls.Add(this.checkBoxDate);
            this.gradientPanel6.Controls.Add(this.checkBoxOther);
            this.gradientPanel6.Controls.Add(this.dateTimePickerExtFrom);
            this.gradientPanel6.Location = new System.Drawing.Point(2, 31);
            this.gradientPanel6.Name = "gradientPanel6";
            this.gradientPanel6.Size = new System.Drawing.Size(221, 200);
            this.gradientPanel6.TabIndex = 1;
            // 
            // comboBoxType
            // 
            this.comboBoxType.Enabled = false;
            this.comboBoxType.Location = new System.Drawing.Point(120, 70);
            this.comboBoxType.Name = "comboBoxType";
            this.comboBoxType.Size = new System.Drawing.Size(88, 21);
            this.comboBoxType.Sorted = true;
            this.comboBoxType.TabIndex = 4;
            // 
            // dateTimePickerExtTo
            // 
            this.dateTimePickerExtTo.BackgroundColor = new Syncfusion.Drawing.BrushInfo(Syncfusion.Drawing.GradientStyle.Vertical, new Syncfusion.Drawing.BrushInfoColorArrayList(new System.Drawing.Color[] {
                System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240))))),
                System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))))}));
            // 
            // 
            // 
            this.dateTimePickerExtTo.Calendar.AllowMultipleSelection = false;
            this.dateTimePickerExtTo.Calendar.BottomHeight = 19;
            this.dateTimePickerExtTo.Calendar.Culture = new System.Globalization.CultureInfo("");
            this.dateTimePickerExtTo.Calendar.DaysFont = new System.Drawing.Font("Verdana", 8F);
            this.dateTimePickerExtTo.Calendar.DaysHeaderInterior = new Syncfusion.Drawing.BrushInfo(Syncfusion.Drawing.GradientStyle.Vertical, new Syncfusion.Drawing.BrushInfoColorArrayList(new System.Drawing.Color[] {
                System.Drawing.Color.AntiqueWhite,
                System.Drawing.Color.PeachPuff}));
            this.dateTimePickerExtTo.Calendar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateTimePickerExtTo.Calendar.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerExtTo.Calendar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.dateTimePickerExtTo.Calendar.HeaderHeight = 30;
            this.dateTimePickerExtTo.Calendar.HeadForeColor = System.Drawing.SystemColors.ControlText;
            this.dateTimePickerExtTo.Calendar.Location = new System.Drawing.Point(0, 0);
            this.dateTimePickerExtTo.Calendar.MaxValue = new System.DateTime(9999, 12, 31, 23, 59, 59, 999);
            this.dateTimePickerExtTo.Calendar.MinValue = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dateTimePickerExtTo.Calendar.Name = "monthCalendar";
            // 
            // 
            // 
            this.dateTimePickerExtTo.Calendar.NoneButton.Location = new System.Drawing.Point(186, 0);
            this.dateTimePickerExtTo.Calendar.NoneButton.Size = new System.Drawing.Size(66, 19);
            this.dateTimePickerExtTo.Calendar.ScrollButtonSize = new System.Drawing.Size(16, 18);
            this.dateTimePickerExtTo.Calendar.Size = new System.Drawing.Size(252, 174);
            this.dateTimePickerExtTo.Calendar.SizeToFit = true;
            this.dateTimePickerExtTo.Calendar.TabIndex = 0;
            // 
            // 
            // 
            this.dateTimePickerExtTo.Calendar.TodayButton.Location = new System.Drawing.Point(0, 0);
            this.dateTimePickerExtTo.Calendar.TodayButton.Size = new System.Drawing.Size(186, 19);
            this.dateTimePickerExtTo.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerExtTo.CalendarTitleForeColor = System.Drawing.SystemColors.ControlText;
            this.dateTimePickerExtTo.Enabled = false;
            this.dateTimePickerExtTo.ForeColor = System.Drawing.SystemColors.ControlText;
            this.dateTimePickerExtTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerExtTo.Location = new System.Drawing.Point(120, 32);
            this.dateTimePickerExtTo.MaxValue = new System.DateTime(9999, 12, 31, 23, 59, 59, 999);
            this.dateTimePickerExtTo.Name = "dateTimePickerExtTo";
            this.dateTimePickerExtTo.ShowCheckBox = false;
            this.dateTimePickerExtTo.Size = new System.Drawing.Size(91, 19);
            this.dateTimePickerExtTo.TabIndex = 2;
            this.dateTimePickerExtTo.ThemesEnabled = true;
            // 
            // lbTo
            // 
            this.lbTo.BackColor = System.Drawing.Color.Transparent;
            this.lbTo.Location = new System.Drawing.Point(77, 32);
            this.lbTo.Name = "lbTo";
            this.lbTo.Size = new System.Drawing.Size(37, 15);
            this.lbTo.TabIndex = 45;
            this.lbTo.Text = "To";
            // 
            // lbFrom
            // 
            this.lbFrom.BackColor = System.Drawing.Color.Transparent;
            this.lbFrom.Location = new System.Drawing.Point(77, 12);
            this.lbFrom.Name = "lbFrom";
            this.lbFrom.Size = new System.Drawing.Size(37, 15);
            this.lbFrom.TabIndex = 44;
            this.lbFrom.Text = "From";
            // 
            // textBoxDrName
            // 
            this.textBoxDrName.Enabled = false;
            this.textBoxDrName.Location = new System.Drawing.Point(120, 50);
            this.textBoxDrName.Name = "textBoxDrName";
            this.textBoxDrName.Size = new System.Drawing.Size(91, 20);
            this.textBoxDrName.TabIndex = 8;
            this.textBoxDrName.Visible = false;
            // 
            // checkBoxDrName
            // 
            this.checkBoxDrName.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxDrName.Location = new System.Drawing.Point(6, 48);
            this.checkBoxDrName.Name = "checkBoxDrName";
            this.checkBoxDrName.Size = new System.Drawing.Size(66, 22);
            this.checkBoxDrName.TabIndex = 7;
            this.checkBoxDrName.Text = "Doctor";
            this.checkBoxDrName.UseVisualStyleBackColor = false;
            this.checkBoxDrName.Visible = false;
            this.checkBoxDrName.CheckStateChanged += new System.EventHandler(this.checkBoxDrName_CheckStateChanged);
            // 
            // textBoxLocation
            // 
            this.textBoxLocation.Enabled = false;
            this.textBoxLocation.Location = new System.Drawing.Point(120, 102);
            this.textBoxLocation.Name = "textBoxLocation";
            this.textBoxLocation.Size = new System.Drawing.Size(91, 20);
            this.textBoxLocation.TabIndex = 6;
            // 
            // checkBoxLocation
            // 
            this.checkBoxLocation.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxLocation.Location = new System.Drawing.Point(5, 99);
            this.checkBoxLocation.Name = "checkBoxLocation";
            this.checkBoxLocation.Size = new System.Drawing.Size(107, 23);
            this.checkBoxLocation.TabIndex = 5;
            this.checkBoxLocation.Text = "Location";
            this.checkBoxLocation.UseVisualStyleBackColor = false;
            this.checkBoxLocation.CheckStateChanged += new System.EventHandler(this.checkBoxLocation_CheckStateChanged);
            // 
            // textBoxType
            // 
            this.textBoxType.Enabled = false;
            this.textBoxType.Location = new System.Drawing.Point(120, 71);
            this.textBoxType.Name = "textBoxType";
            this.textBoxType.Size = new System.Drawing.Size(91, 20);
            this.textBoxType.TabIndex = 39;
            this.textBoxType.Visible = false;
            // 
            // checkBoxType
            // 
            this.checkBoxType.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxType.Location = new System.Drawing.Point(5, 70);
            this.checkBoxType.Name = "checkBoxType";
            this.checkBoxType.Size = new System.Drawing.Size(51, 23);
            this.checkBoxType.TabIndex = 3;
            this.checkBoxType.Text = "Type";
            this.checkBoxType.UseVisualStyleBackColor = false;
            this.checkBoxType.CheckStateChanged += new System.EventHandler(this.checkBoxType_CheckStateChanged);
            // 
            // textBoxOther
            // 
            this.textBoxOther.Enabled = false;
            this.textBoxOther.Location = new System.Drawing.Point(120, 128);
            this.textBoxOther.Name = "textBoxOther";
            this.textBoxOther.Size = new System.Drawing.Size(91, 20);
            this.textBoxOther.TabIndex = 10;
            // 
            // checkBoxDate
            // 
            this.checkBoxDate.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxDate.Location = new System.Drawing.Point(5, 8);
            this.checkBoxDate.Name = "checkBoxDate";
            this.checkBoxDate.Size = new System.Drawing.Size(66, 23);
            this.checkBoxDate.TabIndex = 0;
            this.checkBoxDate.Text = "Date";
            this.checkBoxDate.UseVisualStyleBackColor = false;
            this.checkBoxDate.CheckStateChanged += new System.EventHandler(this.checkBoxDate_CheckStateChanged);
            // 
            // checkBoxOther
            // 
            this.checkBoxOther.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxOther.Location = new System.Drawing.Point(5, 127);
            this.checkBoxOther.Name = "checkBoxOther";
            this.checkBoxOther.Size = new System.Drawing.Size(93, 23);
            this.checkBoxOther.TabIndex = 9;
            this.checkBoxOther.Text = "Other";
            this.checkBoxOther.UseVisualStyleBackColor = false;
            this.checkBoxOther.CheckStateChanged += new System.EventHandler(this.checkBoxOther_CheckStateChanged);
            // 
            // dateTimePickerExtFrom
            // 
            this.dateTimePickerExtFrom.BackgroundColor = new Syncfusion.Drawing.BrushInfo(Syncfusion.Drawing.GradientStyle.Vertical, new Syncfusion.Drawing.BrushInfoColorArrayList(new System.Drawing.Color[] {
                System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240))))),
                System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))))}));
            // 
            // 
            // 
            this.dateTimePickerExtFrom.Calendar.AllowMultipleSelection = false;
            this.dateTimePickerExtFrom.Calendar.BottomHeight = 19;
            this.dateTimePickerExtFrom.Calendar.Culture = new System.Globalization.CultureInfo("");
            this.dateTimePickerExtFrom.Calendar.DaysFont = new System.Drawing.Font("Verdana", 8F);
            this.dateTimePickerExtFrom.Calendar.DaysHeaderInterior = new Syncfusion.Drawing.BrushInfo(Syncfusion.Drawing.GradientStyle.Vertical, new Syncfusion.Drawing.BrushInfoColorArrayList(new System.Drawing.Color[] {
                System.Drawing.Color.AntiqueWhite,
                System.Drawing.Color.PeachPuff}));
            this.dateTimePickerExtFrom.Calendar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dateTimePickerExtFrom.Calendar.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerExtFrom.Calendar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.dateTimePickerExtFrom.Calendar.HeaderHeight = 30;
            this.dateTimePickerExtFrom.Calendar.HeadForeColor = System.Drawing.SystemColors.ControlText;
            this.dateTimePickerExtFrom.Calendar.Location = new System.Drawing.Point(0, 0);
            this.dateTimePickerExtFrom.Calendar.MaxValue = new System.DateTime(9999, 12, 31, 23, 59, 59, 999);
            this.dateTimePickerExtFrom.Calendar.MinValue = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dateTimePickerExtFrom.Calendar.Name = "monthCalendar";
            // 
            // 
            // 
            this.dateTimePickerExtFrom.Calendar.NoneButton.Location = new System.Drawing.Point(186, 0);
            this.dateTimePickerExtFrom.Calendar.NoneButton.Size = new System.Drawing.Size(66, 19);
            this.dateTimePickerExtFrom.Calendar.ScrollButtonSize = new System.Drawing.Size(16, 18);
            this.dateTimePickerExtFrom.Calendar.Size = new System.Drawing.Size(252, 174);
            this.dateTimePickerExtFrom.Calendar.SizeToFit = true;
            this.dateTimePickerExtFrom.Calendar.TabIndex = 0;
            // 
            // 
            // 
            this.dateTimePickerExtFrom.Calendar.TodayButton.Location = new System.Drawing.Point(0, 0);
            this.dateTimePickerExtFrom.Calendar.TodayButton.Size = new System.Drawing.Size(186, 19);
            this.dateTimePickerExtFrom.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerExtFrom.CalendarTitleForeColor = System.Drawing.SystemColors.ControlText;
            this.dateTimePickerExtFrom.Enabled = false;
            this.dateTimePickerExtFrom.ForeColor = System.Drawing.SystemColors.ControlText;
            this.dateTimePickerExtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerExtFrom.Location = new System.Drawing.Point(120, 8);
            this.dateTimePickerExtFrom.MaxValue = new System.DateTime(9999, 12, 31, 23, 59, 59, 999);
            this.dateTimePickerExtFrom.Name = "dateTimePickerExtFrom";
            this.dateTimePickerExtFrom.ShowCheckBox = false;
            this.dateTimePickerExtFrom.Size = new System.Drawing.Size(91, 19);
            this.dateTimePickerExtFrom.TabIndex = 1;
            this.dateTimePickerExtFrom.ThemesEnabled = true;
            // 
            // xpTaskBarBoxHistory
            // 
            this.xpTaskBarBoxHistory.Controls.Add(this.gradientPanel2);
            this.xpTaskBarBoxHistory.HeaderImageIndex = -1;
            this.xpTaskBarBoxHistory.ItemBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(170)))), ((int)(((byte)(230)))));
            this.xpTaskBarBoxHistory.Location = new System.Drawing.Point(10, 599);
            this.xpTaskBarBoxHistory.Name = "xpTaskBarBoxHistory";
            this.xpTaskBarBoxHistory.PreferredChildPanelHeight = 360;
            this.xpTaskBarBoxHistory.Size = new System.Drawing.Size(225, 393);
            this.xpTaskBarBoxHistory.TabIndex = 3;
            // 
            // gradientPanel2
            // 
            this.gradientPanel2.AutoScroll = true;
            this.gradientPanel2.BackgroundColor = new Syncfusion.Drawing.BrushInfo(Syncfusion.Drawing.GradientStyle.Vertical, new Syncfusion.Drawing.BrushInfoColorArrayList(new System.Drawing.Color[] {
                System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240))))),
                System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))))}));
            this.gradientPanel2.Border3DStyle = System.Windows.Forms.Border3DStyle.Flat;
            this.gradientPanel2.BorderColor = System.Drawing.Color.Black;
            this.gradientPanel2.Controls.Add(this.panel20);
            this.gradientPanel2.Controls.Add(this.lbArrhythmia);
            this.gradientPanel2.Controls.Add(this.panel18);
            this.gradientPanel2.Controls.Add(this.lbCoronaryByPass);
            this.gradientPanel2.Controls.Add(this.panel16);
            this.gradientPanel2.Controls.Add(this.lbHeartValves);
            this.gradientPanel2.Controls.Add(this.panel14);
            this.gradientPanel2.Controls.Add(this.lbAtrialFibrillation);
            this.gradientPanel2.Controls.Add(this.panel12);
            this.gradientPanel2.Controls.Add(this.lbCVA);
            this.gradientPanel2.Controls.Add(this.panel10);
            this.gradientPanel2.Controls.Add(this.panel8);
            this.gradientPanel2.Controls.Add(this.lbTIA);
            this.gradientPanel2.Controls.Add(this.lbACS);
            this.gradientPanel2.Controls.Add(this.panel6);
            this.gradientPanel2.Controls.Add(this.panel4);
            this.gradientPanel2.Controls.Add(this.lbYes);
            this.gradientPanel2.Controls.Add(this.lbNo);
            this.gradientPanel2.Controls.Add(this.lbDontCare);
            this.gradientPanel2.Controls.Add(this.lbSCS);
            this.gradientPanel2.Controls.Add(this.panel3);
            this.gradientPanel2.Controls.Add(this.lbHypertension);
            this.gradientPanel2.Controls.Add(this.lbSmoking);
            this.gradientPanel2.Location = new System.Drawing.Point(2, 31);
            this.gradientPanel2.Name = "gradientPanel2";
            this.gradientPanel2.Size = new System.Drawing.Size(221, 360);
            this.gradientPanel2.TabIndex = 1;
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.Transparent;
            this.panel20.Controls.Add(this.rbArrithymia_No);
            this.panel20.Controls.Add(this.rbArrithymia_Yes);
            this.panel20.Controls.Add(this.rbArrithymia_DC);
            this.panel20.Controls.Add(this.panel21);
            this.panel20.Location = new System.Drawing.Point(113, 329);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(103, 24);
            this.panel20.TabIndex = 84;
            // 
            // rbArrithymia_No
            // 
            this.rbArrithymia_No.BackColor = System.Drawing.Color.Transparent;
            this.rbArrithymia_No.Location = new System.Drawing.Point(43, 4);
            this.rbArrithymia_No.Name = "rbArrithymia_No";
            this.rbArrithymia_No.Size = new System.Drawing.Size(12, 16);
            this.rbArrithymia_No.TabIndex = 1;
            this.rbArrithymia_No.UseVisualStyleBackColor = false;
            // 
            // rbArrithymia_Yes
            // 
            this.rbArrithymia_Yes.BackColor = System.Drawing.Color.Transparent;
            this.rbArrithymia_Yes.Location = new System.Drawing.Point(8, 4);
            this.rbArrithymia_Yes.Name = "rbArrithymia_Yes";
            this.rbArrithymia_Yes.Size = new System.Drawing.Size(12, 16);
            this.rbArrithymia_Yes.TabIndex = 0;
            this.rbArrithymia_Yes.UseVisualStyleBackColor = false;
            // 
            // rbArrithymia_DC
            // 
            this.rbArrithymia_DC.BackColor = System.Drawing.Color.Transparent;
            this.rbArrithymia_DC.Checked = true;
            this.rbArrithymia_DC.Location = new System.Drawing.Point(76, 4);
            this.rbArrithymia_DC.Name = "rbArrithymia_DC";
            this.rbArrithymia_DC.Size = new System.Drawing.Size(12, 16);
            this.rbArrithymia_DC.TabIndex = 2;
            this.rbArrithymia_DC.TabStop = true;
            this.rbArrithymia_DC.UseVisualStyleBackColor = false;
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.Color.Transparent;
            this.panel21.Controls.Add(this.radioButton52);
            this.panel21.Controls.Add(this.radioButton53);
            this.panel21.Controls.Add(this.radioButton54);
            this.panel21.Location = new System.Drawing.Point(0, 56);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(96, 24);
            this.panel21.TabIndex = 70;
            // 
            // radioButton52
            // 
            this.radioButton52.BackColor = System.Drawing.Color.Transparent;
            this.radioButton52.Location = new System.Drawing.Point(44, 4);
            this.radioButton52.Name = "radioButton52";
            this.radioButton52.Size = new System.Drawing.Size(12, 16);
            this.radioButton52.TabIndex = 1;
            this.radioButton52.UseVisualStyleBackColor = false;
            // 
            // radioButton53
            // 
            this.radioButton53.BackColor = System.Drawing.Color.Transparent;
            this.radioButton53.Location = new System.Drawing.Point(8, 4);
            this.radioButton53.Name = "radioButton53";
            this.radioButton53.Size = new System.Drawing.Size(12, 16);
            this.radioButton53.TabIndex = 0;
            this.radioButton53.UseVisualStyleBackColor = false;
            // 
            // radioButton54
            // 
            this.radioButton54.BackColor = System.Drawing.Color.Transparent;
            this.radioButton54.Location = new System.Drawing.Point(80, 4);
            this.radioButton54.Name = "radioButton54";
            this.radioButton54.Size = new System.Drawing.Size(12, 16);
            this.radioButton54.TabIndex = 2;
            this.radioButton54.UseVisualStyleBackColor = false;
            // 
            // lbArrhythmia
            // 
            this.lbArrhythmia.BackColor = System.Drawing.Color.Transparent;
            this.lbArrhythmia.Location = new System.Drawing.Point(0, 333);
            this.lbArrhythmia.Name = "lbArrhythmia";
            this.lbArrhythmia.Size = new System.Drawing.Size(98, 19);
            this.lbArrhythmia.TabIndex = 83;
            this.lbArrhythmia.Text = "Arrhythmia";
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.Transparent;
            this.panel18.Controls.Add(this.rbCBP_No);
            this.panel18.Controls.Add(this.rbCBP_Yes);
            this.panel18.Controls.Add(this.rbCBP_DC);
            this.panel18.Controls.Add(this.panel19);
            this.panel18.Location = new System.Drawing.Point(113, 292);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(103, 24);
            this.panel18.TabIndex = 82;
            // 
            // rbCBP_No
            // 
            this.rbCBP_No.BackColor = System.Drawing.Color.Transparent;
            this.rbCBP_No.Location = new System.Drawing.Point(43, 4);
            this.rbCBP_No.Name = "rbCBP_No";
            this.rbCBP_No.Size = new System.Drawing.Size(12, 16);
            this.rbCBP_No.TabIndex = 1;
            this.rbCBP_No.UseVisualStyleBackColor = false;
            // 
            // rbCBP_Yes
            // 
            this.rbCBP_Yes.BackColor = System.Drawing.Color.Transparent;
            this.rbCBP_Yes.Location = new System.Drawing.Point(8, 4);
            this.rbCBP_Yes.Name = "rbCBP_Yes";
            this.rbCBP_Yes.Size = new System.Drawing.Size(12, 16);
            this.rbCBP_Yes.TabIndex = 0;
            this.rbCBP_Yes.UseVisualStyleBackColor = false;
            // 
            // rbCBP_DC
            // 
            this.rbCBP_DC.BackColor = System.Drawing.Color.Transparent;
            this.rbCBP_DC.Checked = true;
            this.rbCBP_DC.Location = new System.Drawing.Point(76, 4);
            this.rbCBP_DC.Name = "rbCBP_DC";
            this.rbCBP_DC.Size = new System.Drawing.Size(12, 16);
            this.rbCBP_DC.TabIndex = 2;
            this.rbCBP_DC.TabStop = true;
            this.rbCBP_DC.UseVisualStyleBackColor = false;
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.Color.Transparent;
            this.panel19.Controls.Add(this.radioButton46);
            this.panel19.Controls.Add(this.radioButton47);
            this.panel19.Controls.Add(this.radioButton48);
            this.panel19.Location = new System.Drawing.Point(0, 56);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(96, 24);
            this.panel19.TabIndex = 70;
            // 
            // radioButton46
            // 
            this.radioButton46.BackColor = System.Drawing.Color.Transparent;
            this.radioButton46.Location = new System.Drawing.Point(44, 4);
            this.radioButton46.Name = "radioButton46";
            this.radioButton46.Size = new System.Drawing.Size(12, 16);
            this.radioButton46.TabIndex = 1;
            this.radioButton46.UseVisualStyleBackColor = false;
            // 
            // radioButton47
            // 
            this.radioButton47.BackColor = System.Drawing.Color.Transparent;
            this.radioButton47.Location = new System.Drawing.Point(8, 4);
            this.radioButton47.Name = "radioButton47";
            this.radioButton47.Size = new System.Drawing.Size(12, 16);
            this.radioButton47.TabIndex = 0;
            this.radioButton47.UseVisualStyleBackColor = false;
            // 
            // radioButton48
            // 
            this.radioButton48.BackColor = System.Drawing.Color.Transparent;
            this.radioButton48.Location = new System.Drawing.Point(80, 4);
            this.radioButton48.Name = "radioButton48";
            this.radioButton48.Size = new System.Drawing.Size(12, 16);
            this.radioButton48.TabIndex = 2;
            this.radioButton48.UseVisualStyleBackColor = false;
            // 
            // lbCoronaryByPass
            // 
            this.lbCoronaryByPass.BackColor = System.Drawing.Color.Transparent;
            this.lbCoronaryByPass.Location = new System.Drawing.Point(0, 289);
            this.lbCoronaryByPass.Name = "lbCoronaryByPass";
            this.lbCoronaryByPass.Size = new System.Drawing.Size(88, 31);
            this.lbCoronaryByPass.TabIndex = 81;
            this.lbCoronaryByPass.Text = "Coronary By Pass";
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.Transparent;
            this.panel16.Controls.Add(this.rbPHV_No);
            this.panel16.Controls.Add(this.rbPHV_Yes);
            this.panel16.Controls.Add(this.rbPHV_DC);
            this.panel16.Controls.Add(this.panel17);
            this.panel16.Location = new System.Drawing.Point(113, 253);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(103, 24);
            this.panel16.TabIndex = 80;
            // 
            // rbPHV_No
            // 
            this.rbPHV_No.BackColor = System.Drawing.Color.Transparent;
            this.rbPHV_No.Location = new System.Drawing.Point(43, 4);
            this.rbPHV_No.Name = "rbPHV_No";
            this.rbPHV_No.Size = new System.Drawing.Size(12, 16);
            this.rbPHV_No.TabIndex = 1;
            this.rbPHV_No.UseVisualStyleBackColor = false;
            // 
            // rbPHV_Yes
            // 
            this.rbPHV_Yes.BackColor = System.Drawing.Color.Transparent;
            this.rbPHV_Yes.Location = new System.Drawing.Point(8, 4);
            this.rbPHV_Yes.Name = "rbPHV_Yes";
            this.rbPHV_Yes.Size = new System.Drawing.Size(12, 16);
            this.rbPHV_Yes.TabIndex = 0;
            this.rbPHV_Yes.UseVisualStyleBackColor = false;
            // 
            // rbPHV_DC
            // 
            this.rbPHV_DC.BackColor = System.Drawing.Color.Transparent;
            this.rbPHV_DC.Checked = true;
            this.rbPHV_DC.Location = new System.Drawing.Point(76, 4);
            this.rbPHV_DC.Name = "rbPHV_DC";
            this.rbPHV_DC.Size = new System.Drawing.Size(12, 16);
            this.rbPHV_DC.TabIndex = 2;
            this.rbPHV_DC.TabStop = true;
            this.rbPHV_DC.UseVisualStyleBackColor = false;
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.Color.Transparent;
            this.panel17.Controls.Add(this.radioButton40);
            this.panel17.Controls.Add(this.radioButton41);
            this.panel17.Controls.Add(this.radioButton42);
            this.panel17.Location = new System.Drawing.Point(0, 56);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(96, 24);
            this.panel17.TabIndex = 70;
            // 
            // radioButton40
            // 
            this.radioButton40.BackColor = System.Drawing.Color.Transparent;
            this.radioButton40.Location = new System.Drawing.Point(44, 4);
            this.radioButton40.Name = "radioButton40";
            this.radioButton40.Size = new System.Drawing.Size(12, 16);
            this.radioButton40.TabIndex = 1;
            this.radioButton40.UseVisualStyleBackColor = false;
            // 
            // radioButton41
            // 
            this.radioButton41.BackColor = System.Drawing.Color.Transparent;
            this.radioButton41.Location = new System.Drawing.Point(8, 4);
            this.radioButton41.Name = "radioButton41";
            this.radioButton41.Size = new System.Drawing.Size(12, 16);
            this.radioButton41.TabIndex = 0;
            this.radioButton41.UseVisualStyleBackColor = false;
            // 
            // radioButton42
            // 
            this.radioButton42.BackColor = System.Drawing.Color.Transparent;
            this.radioButton42.Location = new System.Drawing.Point(80, 4);
            this.radioButton42.Name = "radioButton42";
            this.radioButton42.Size = new System.Drawing.Size(12, 16);
            this.radioButton42.TabIndex = 2;
            this.radioButton42.UseVisualStyleBackColor = false;
            // 
            // lbHeartValves
            // 
            this.lbHeartValves.BackColor = System.Drawing.Color.Transparent;
            this.lbHeartValves.Location = new System.Drawing.Point(0, 249);
            this.lbHeartValves.Name = "lbHeartValves";
            this.lbHeartValves.Size = new System.Drawing.Size(112, 32);
            this.lbHeartValves.TabIndex = 79;
            this.lbHeartValves.Text = "Prosthetic Heart Valves";
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.Transparent;
            this.panel14.Controls.Add(this.rbAF_No);
            this.panel14.Controls.Add(this.rbAF_Yes);
            this.panel14.Controls.Add(this.rbAF_DC);
            this.panel14.Controls.Add(this.panel15);
            this.panel14.Location = new System.Drawing.Point(113, 220);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(103, 24);
            this.panel14.TabIndex = 78;
            // 
            // rbAF_No
            // 
            this.rbAF_No.BackColor = System.Drawing.Color.Transparent;
            this.rbAF_No.Location = new System.Drawing.Point(43, 4);
            this.rbAF_No.Name = "rbAF_No";
            this.rbAF_No.Size = new System.Drawing.Size(12, 16);
            this.rbAF_No.TabIndex = 1;
            this.rbAF_No.UseVisualStyleBackColor = false;
            // 
            // rbAF_Yes
            // 
            this.rbAF_Yes.BackColor = System.Drawing.Color.Transparent;
            this.rbAF_Yes.Location = new System.Drawing.Point(8, 4);
            this.rbAF_Yes.Name = "rbAF_Yes";
            this.rbAF_Yes.Size = new System.Drawing.Size(12, 16);
            this.rbAF_Yes.TabIndex = 0;
            this.rbAF_Yes.UseVisualStyleBackColor = false;
            // 
            // rbAF_DC
            // 
            this.rbAF_DC.BackColor = System.Drawing.Color.Transparent;
            this.rbAF_DC.Checked = true;
            this.rbAF_DC.Location = new System.Drawing.Point(76, 4);
            this.rbAF_DC.Name = "rbAF_DC";
            this.rbAF_DC.Size = new System.Drawing.Size(12, 16);
            this.rbAF_DC.TabIndex = 2;
            this.rbAF_DC.TabStop = true;
            this.rbAF_DC.UseVisualStyleBackColor = false;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.Transparent;
            this.panel15.Controls.Add(this.radioButton34);
            this.panel15.Controls.Add(this.radioButton35);
            this.panel15.Controls.Add(this.radioButton36);
            this.panel15.Location = new System.Drawing.Point(0, 56);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(96, 24);
            this.panel15.TabIndex = 70;
            // 
            // radioButton34
            // 
            this.radioButton34.BackColor = System.Drawing.Color.Transparent;
            this.radioButton34.Location = new System.Drawing.Point(44, 4);
            this.radioButton34.Name = "radioButton34";
            this.radioButton34.Size = new System.Drawing.Size(12, 16);
            this.radioButton34.TabIndex = 1;
            this.radioButton34.UseVisualStyleBackColor = false;
            // 
            // radioButton35
            // 
            this.radioButton35.BackColor = System.Drawing.Color.Transparent;
            this.radioButton35.Location = new System.Drawing.Point(8, 4);
            this.radioButton35.Name = "radioButton35";
            this.radioButton35.Size = new System.Drawing.Size(12, 16);
            this.radioButton35.TabIndex = 0;
            this.radioButton35.UseVisualStyleBackColor = false;
            // 
            // radioButton36
            // 
            this.radioButton36.BackColor = System.Drawing.Color.Transparent;
            this.radioButton36.Location = new System.Drawing.Point(80, 4);
            this.radioButton36.Name = "radioButton36";
            this.radioButton36.Size = new System.Drawing.Size(12, 16);
            this.radioButton36.TabIndex = 2;
            this.radioButton36.UseVisualStyleBackColor = false;
            // 
            // lbAtrialFibrillation
            // 
            this.lbAtrialFibrillation.BackColor = System.Drawing.Color.Transparent;
            this.lbAtrialFibrillation.Location = new System.Drawing.Point(0, 220);
            this.lbAtrialFibrillation.Name = "lbAtrialFibrillation";
            this.lbAtrialFibrillation.Size = new System.Drawing.Size(114, 29);
            this.lbAtrialFibrillation.TabIndex = 77;
            this.lbAtrialFibrillation.Text = "Atrial Fibrillation";
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.Transparent;
            this.panel12.Controls.Add(this.rbCVA_No);
            this.panel12.Controls.Add(this.rbCVA_Yes);
            this.panel12.Controls.Add(this.rbCVA_DC);
            this.panel12.Controls.Add(this.panel13);
            this.panel12.Location = new System.Drawing.Point(113, 196);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(103, 24);
            this.panel12.TabIndex = 76;
            // 
            // rbCVA_No
            // 
            this.rbCVA_No.BackColor = System.Drawing.Color.Transparent;
            this.rbCVA_No.Location = new System.Drawing.Point(43, 4);
            this.rbCVA_No.Name = "rbCVA_No";
            this.rbCVA_No.Size = new System.Drawing.Size(12, 16);
            this.rbCVA_No.TabIndex = 1;
            this.rbCVA_No.UseVisualStyleBackColor = false;
            // 
            // rbCVA_Yes
            // 
            this.rbCVA_Yes.BackColor = System.Drawing.Color.Transparent;
            this.rbCVA_Yes.Location = new System.Drawing.Point(8, 4);
            this.rbCVA_Yes.Name = "rbCVA_Yes";
            this.rbCVA_Yes.Size = new System.Drawing.Size(12, 16);
            this.rbCVA_Yes.TabIndex = 0;
            this.rbCVA_Yes.UseVisualStyleBackColor = false;
            // 
            // rbCVA_DC
            // 
            this.rbCVA_DC.BackColor = System.Drawing.Color.Transparent;
            this.rbCVA_DC.Checked = true;
            this.rbCVA_DC.Location = new System.Drawing.Point(76, 4);
            this.rbCVA_DC.Name = "rbCVA_DC";
            this.rbCVA_DC.Size = new System.Drawing.Size(12, 16);
            this.rbCVA_DC.TabIndex = 2;
            this.rbCVA_DC.TabStop = true;
            this.rbCVA_DC.UseVisualStyleBackColor = false;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.Transparent;
            this.panel13.Controls.Add(this.radioButton28);
            this.panel13.Controls.Add(this.radioButton29);
            this.panel13.Controls.Add(this.radioButton30);
            this.panel13.Location = new System.Drawing.Point(0, 56);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(96, 24);
            this.panel13.TabIndex = 70;
            // 
            // radioButton28
            // 
            this.radioButton28.BackColor = System.Drawing.Color.Transparent;
            this.radioButton28.Location = new System.Drawing.Point(44, 4);
            this.radioButton28.Name = "radioButton28";
            this.radioButton28.Size = new System.Drawing.Size(12, 16);
            this.radioButton28.TabIndex = 59;
            this.radioButton28.UseVisualStyleBackColor = false;
            // 
            // radioButton29
            // 
            this.radioButton29.BackColor = System.Drawing.Color.Transparent;
            this.radioButton29.Location = new System.Drawing.Point(8, 4);
            this.radioButton29.Name = "radioButton29";
            this.radioButton29.Size = new System.Drawing.Size(12, 16);
            this.radioButton29.TabIndex = 58;
            this.radioButton29.UseVisualStyleBackColor = false;
            // 
            // radioButton30
            // 
            this.radioButton30.BackColor = System.Drawing.Color.Transparent;
            this.radioButton30.Location = new System.Drawing.Point(80, 4);
            this.radioButton30.Name = "radioButton30";
            this.radioButton30.Size = new System.Drawing.Size(12, 16);
            this.radioButton30.TabIndex = 60;
            this.radioButton30.UseVisualStyleBackColor = false;
            // 
            // lbCVA
            // 
            this.lbCVA.BackColor = System.Drawing.Color.Transparent;
            this.lbCVA.Location = new System.Drawing.Point(0, 201);
            this.lbCVA.Name = "lbCVA";
            this.lbCVA.Size = new System.Drawing.Size(56, 16);
            this.lbCVA.TabIndex = 75;
            this.lbCVA.Text = "CVA";
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Transparent;
            this.panel10.Controls.Add(this.rbTIA_No);
            this.panel10.Controls.Add(this.rbTIA_Yes);
            this.panel10.Controls.Add(this.rbTIA_DC);
            this.panel10.Controls.Add(this.panel11);
            this.panel10.Location = new System.Drawing.Point(113, 172);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(103, 24);
            this.panel10.TabIndex = 74;
            // 
            // rbTIA_No
            // 
            this.rbTIA_No.BackColor = System.Drawing.Color.Transparent;
            this.rbTIA_No.Location = new System.Drawing.Point(43, 4);
            this.rbTIA_No.Name = "rbTIA_No";
            this.rbTIA_No.Size = new System.Drawing.Size(12, 16);
            this.rbTIA_No.TabIndex = 1;
            this.rbTIA_No.UseVisualStyleBackColor = false;
            // 
            // rbTIA_Yes
            // 
            this.rbTIA_Yes.BackColor = System.Drawing.Color.Transparent;
            this.rbTIA_Yes.Location = new System.Drawing.Point(8, 4);
            this.rbTIA_Yes.Name = "rbTIA_Yes";
            this.rbTIA_Yes.Size = new System.Drawing.Size(12, 16);
            this.rbTIA_Yes.TabIndex = 0;
            this.rbTIA_Yes.UseVisualStyleBackColor = false;
            // 
            // rbTIA_DC
            // 
            this.rbTIA_DC.BackColor = System.Drawing.Color.Transparent;
            this.rbTIA_DC.Checked = true;
            this.rbTIA_DC.Location = new System.Drawing.Point(76, 4);
            this.rbTIA_DC.Name = "rbTIA_DC";
            this.rbTIA_DC.Size = new System.Drawing.Size(12, 16);
            this.rbTIA_DC.TabIndex = 2;
            this.rbTIA_DC.TabStop = true;
            this.rbTIA_DC.UseVisualStyleBackColor = false;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.Transparent;
            this.panel11.Controls.Add(this.radioButton22);
            this.panel11.Controls.Add(this.radioButton23);
            this.panel11.Controls.Add(this.radioButton24);
            this.panel11.Location = new System.Drawing.Point(0, 56);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(96, 24);
            this.panel11.TabIndex = 70;
            // 
            // radioButton22
            // 
            this.radioButton22.BackColor = System.Drawing.Color.Transparent;
            this.radioButton22.Location = new System.Drawing.Point(44, 4);
            this.radioButton22.Name = "radioButton22";
            this.radioButton22.Size = new System.Drawing.Size(12, 16);
            this.radioButton22.TabIndex = 59;
            this.radioButton22.UseVisualStyleBackColor = false;
            // 
            // radioButton23
            // 
            this.radioButton23.BackColor = System.Drawing.Color.Transparent;
            this.radioButton23.Location = new System.Drawing.Point(8, 4);
            this.radioButton23.Name = "radioButton23";
            this.radioButton23.Size = new System.Drawing.Size(12, 16);
            this.radioButton23.TabIndex = 58;
            this.radioButton23.UseVisualStyleBackColor = false;
            // 
            // radioButton24
            // 
            this.radioButton24.BackColor = System.Drawing.Color.Transparent;
            this.radioButton24.Location = new System.Drawing.Point(80, 4);
            this.radioButton24.Name = "radioButton24";
            this.radioButton24.Size = new System.Drawing.Size(12, 16);
            this.radioButton24.TabIndex = 60;
            this.radioButton24.UseVisualStyleBackColor = false;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.Transparent;
            this.panel8.Controls.Add(this.rbACS_No);
            this.panel8.Controls.Add(this.rbACS_Yes);
            this.panel8.Controls.Add(this.rbACS_DC);
            this.panel8.Controls.Add(this.panel9);
            this.panel8.Location = new System.Drawing.Point(113, 141);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(103, 24);
            this.panel8.TabIndex = 73;
            // 
            // rbACS_No
            // 
            this.rbACS_No.BackColor = System.Drawing.Color.Transparent;
            this.rbACS_No.Location = new System.Drawing.Point(43, 4);
            this.rbACS_No.Name = "rbACS_No";
            this.rbACS_No.Size = new System.Drawing.Size(12, 16);
            this.rbACS_No.TabIndex = 1;
            this.rbACS_No.UseVisualStyleBackColor = false;
            // 
            // rbACS_Yes
            // 
            this.rbACS_Yes.BackColor = System.Drawing.Color.Transparent;
            this.rbACS_Yes.Location = new System.Drawing.Point(8, 4);
            this.rbACS_Yes.Name = "rbACS_Yes";
            this.rbACS_Yes.Size = new System.Drawing.Size(12, 16);
            this.rbACS_Yes.TabIndex = 0;
            this.rbACS_Yes.UseVisualStyleBackColor = false;
            // 
            // rbACS_DC
            // 
            this.rbACS_DC.BackColor = System.Drawing.Color.Transparent;
            this.rbACS_DC.Checked = true;
            this.rbACS_DC.Location = new System.Drawing.Point(76, 4);
            this.rbACS_DC.Name = "rbACS_DC";
            this.rbACS_DC.Size = new System.Drawing.Size(12, 16);
            this.rbACS_DC.TabIndex = 2;
            this.rbACS_DC.TabStop = true;
            this.rbACS_DC.UseVisualStyleBackColor = false;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Transparent;
            this.panel9.Controls.Add(this.radioButton16);
            this.panel9.Controls.Add(this.radioButton17);
            this.panel9.Controls.Add(this.radioButton18);
            this.panel9.Location = new System.Drawing.Point(0, 56);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(96, 24);
            this.panel9.TabIndex = 70;
            // 
            // radioButton16
            // 
            this.radioButton16.BackColor = System.Drawing.Color.Transparent;
            this.radioButton16.Location = new System.Drawing.Point(44, 4);
            this.radioButton16.Name = "radioButton16";
            this.radioButton16.Size = new System.Drawing.Size(12, 16);
            this.radioButton16.TabIndex = 59;
            this.radioButton16.UseVisualStyleBackColor = false;
            // 
            // radioButton17
            // 
            this.radioButton17.BackColor = System.Drawing.Color.Transparent;
            this.radioButton17.Location = new System.Drawing.Point(8, 4);
            this.radioButton17.Name = "radioButton17";
            this.radioButton17.Size = new System.Drawing.Size(12, 16);
            this.radioButton17.TabIndex = 58;
            this.radioButton17.UseVisualStyleBackColor = false;
            // 
            // radioButton18
            // 
            this.radioButton18.BackColor = System.Drawing.Color.Transparent;
            this.radioButton18.Location = new System.Drawing.Point(80, 4);
            this.radioButton18.Name = "radioButton18";
            this.radioButton18.Size = new System.Drawing.Size(12, 16);
            this.radioButton18.TabIndex = 60;
            this.radioButton18.UseVisualStyleBackColor = false;
            // 
            // lbTIA
            // 
            this.lbTIA.BackColor = System.Drawing.Color.Transparent;
            this.lbTIA.Location = new System.Drawing.Point(0, 177);
            this.lbTIA.Name = "lbTIA";
            this.lbTIA.Size = new System.Drawing.Size(40, 15);
            this.lbTIA.TabIndex = 72;
            this.lbTIA.Text = "TIA";
            // 
            // lbACS
            // 
            this.lbACS.BackColor = System.Drawing.Color.Transparent;
            this.lbACS.Location = new System.Drawing.Point(-14, 133);
            this.lbACS.Name = "lbACS";
            this.lbACS.Size = new System.Drawing.Size(112, 32);
            this.lbACS.TabIndex = 71;
            this.lbACS.Text = "Asymptomatic Carotid Stenosis";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Transparent;
            this.panel6.Controls.Add(this.rbSCS_No);
            this.panel6.Controls.Add(this.rbSCS_Yes);
            this.panel6.Controls.Add(this.rbSCS_DC);
            this.panel6.Controls.Add(this.panel7);
            this.panel6.Location = new System.Drawing.Point(113, 97);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(103, 24);
            this.panel6.TabIndex = 70;
            // 
            // rbSCS_No
            // 
            this.rbSCS_No.BackColor = System.Drawing.Color.Transparent;
            this.rbSCS_No.Location = new System.Drawing.Point(43, 4);
            this.rbSCS_No.Name = "rbSCS_No";
            this.rbSCS_No.Size = new System.Drawing.Size(12, 16);
            this.rbSCS_No.TabIndex = 1;
            this.rbSCS_No.UseVisualStyleBackColor = false;
            // 
            // rbSCS_Yes
            // 
            this.rbSCS_Yes.BackColor = System.Drawing.Color.Transparent;
            this.rbSCS_Yes.Location = new System.Drawing.Point(8, 4);
            this.rbSCS_Yes.Name = "rbSCS_Yes";
            this.rbSCS_Yes.Size = new System.Drawing.Size(12, 16);
            this.rbSCS_Yes.TabIndex = 0;
            this.rbSCS_Yes.UseVisualStyleBackColor = false;
            // 
            // rbSCS_DC
            // 
            this.rbSCS_DC.BackColor = System.Drawing.Color.Transparent;
            this.rbSCS_DC.Checked = true;
            this.rbSCS_DC.Location = new System.Drawing.Point(76, 4);
            this.rbSCS_DC.Name = "rbSCS_DC";
            this.rbSCS_DC.Size = new System.Drawing.Size(12, 16);
            this.rbSCS_DC.TabIndex = 2;
            this.rbSCS_DC.TabStop = true;
            this.rbSCS_DC.UseVisualStyleBackColor = false;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Transparent;
            this.panel7.Controls.Add(this.radioButton10);
            this.panel7.Controls.Add(this.radioButton11);
            this.panel7.Controls.Add(this.radioButton12);
            this.panel7.Location = new System.Drawing.Point(0, 56);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(96, 24);
            this.panel7.TabIndex = 70;
            // 
            // radioButton10
            // 
            this.radioButton10.BackColor = System.Drawing.Color.Transparent;
            this.radioButton10.Location = new System.Drawing.Point(44, 4);
            this.radioButton10.Name = "radioButton10";
            this.radioButton10.Size = new System.Drawing.Size(12, 16);
            this.radioButton10.TabIndex = 59;
            this.radioButton10.UseVisualStyleBackColor = false;
            // 
            // radioButton11
            // 
            this.radioButton11.BackColor = System.Drawing.Color.Transparent;
            this.radioButton11.Location = new System.Drawing.Point(8, 4);
            this.radioButton11.Name = "radioButton11";
            this.radioButton11.Size = new System.Drawing.Size(12, 16);
            this.radioButton11.TabIndex = 58;
            this.radioButton11.UseVisualStyleBackColor = false;
            // 
            // radioButton12
            // 
            this.radioButton12.BackColor = System.Drawing.Color.Transparent;
            this.radioButton12.Location = new System.Drawing.Point(80, 4);
            this.radioButton12.Name = "radioButton12";
            this.radioButton12.Size = new System.Drawing.Size(12, 16);
            this.radioButton12.TabIndex = 60;
            this.radioButton12.UseVisualStyleBackColor = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.Controls.Add(this.rbHypertension_No);
            this.panel4.Controls.Add(this.rbHypertension_Yes);
            this.panel4.Controls.Add(this.rbHypertension_DC);
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Location = new System.Drawing.Point(113, 67);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(103, 24);
            this.panel4.TabIndex = 69;
            // 
            // rbHypertension_No
            // 
            this.rbHypertension_No.BackColor = System.Drawing.Color.Transparent;
            this.rbHypertension_No.Location = new System.Drawing.Point(43, 4);
            this.rbHypertension_No.Name = "rbHypertension_No";
            this.rbHypertension_No.Size = new System.Drawing.Size(12, 16);
            this.rbHypertension_No.TabIndex = 1;
            this.rbHypertension_No.UseVisualStyleBackColor = false;
            // 
            // rbHypertension_Yes
            // 
            this.rbHypertension_Yes.BackColor = System.Drawing.Color.Transparent;
            this.rbHypertension_Yes.Location = new System.Drawing.Point(8, 4);
            this.rbHypertension_Yes.Name = "rbHypertension_Yes";
            this.rbHypertension_Yes.Size = new System.Drawing.Size(12, 16);
            this.rbHypertension_Yes.TabIndex = 0;
            this.rbHypertension_Yes.UseVisualStyleBackColor = false;
            // 
            // rbHypertension_DC
            // 
            this.rbHypertension_DC.BackColor = System.Drawing.Color.Transparent;
            this.rbHypertension_DC.Checked = true;
            this.rbHypertension_DC.Location = new System.Drawing.Point(76, 4);
            this.rbHypertension_DC.Name = "rbHypertension_DC";
            this.rbHypertension_DC.Size = new System.Drawing.Size(12, 16);
            this.rbHypertension_DC.TabIndex = 2;
            this.rbHypertension_DC.TabStop = true;
            this.rbHypertension_DC.UseVisualStyleBackColor = false;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Transparent;
            this.panel5.Controls.Add(this.radioButton4);
            this.panel5.Controls.Add(this.radioButton5);
            this.panel5.Controls.Add(this.radioButton6);
            this.panel5.Location = new System.Drawing.Point(60, 56);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(96, 24);
            this.panel5.TabIndex = 70;
            // 
            // radioButton4
            // 
            this.radioButton4.BackColor = System.Drawing.Color.Transparent;
            this.radioButton4.Location = new System.Drawing.Point(44, 4);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(12, 16);
            this.radioButton4.TabIndex = 59;
            this.radioButton4.UseVisualStyleBackColor = false;
            // 
            // radioButton5
            // 
            this.radioButton5.BackColor = System.Drawing.Color.Transparent;
            this.radioButton5.Location = new System.Drawing.Point(8, 4);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(12, 16);
            this.radioButton5.TabIndex = 58;
            this.radioButton5.UseVisualStyleBackColor = false;
            // 
            // radioButton6
            // 
            this.radioButton6.BackColor = System.Drawing.Color.Transparent;
            this.radioButton6.Location = new System.Drawing.Point(80, 4);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(12, 16);
            this.radioButton6.TabIndex = 60;
            this.radioButton6.UseVisualStyleBackColor = false;
            // 
            // lbYes
            // 
            this.lbYes.BackColor = System.Drawing.Color.Transparent;
            this.lbYes.Location = new System.Drawing.Point(116, 8);
            this.lbYes.Name = "lbYes";
            this.lbYes.Size = new System.Drawing.Size(31, 16);
            this.lbYes.TabIndex = 0;
            this.lbYes.Text = "Yes";
            // 
            // lbNo
            // 
            this.lbNo.BackColor = System.Drawing.Color.Transparent;
            this.lbNo.Location = new System.Drawing.Point(152, 8);
            this.lbNo.Name = "lbNo";
            this.lbNo.Size = new System.Drawing.Size(21, 16);
            this.lbNo.TabIndex = 1;
            this.lbNo.Text = "No";
            // 
            // lbDontCare
            // 
            this.lbDontCare.BackColor = System.Drawing.Color.Transparent;
            this.lbDontCare.Location = new System.Drawing.Point(173, 0);
            this.lbDontCare.Name = "lbDontCare";
            this.lbDontCare.Size = new System.Drawing.Size(42, 29);
            this.lbDontCare.TabIndex = 2;
            this.lbDontCare.Text = "Dont Care";
            this.lbDontCare.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbSCS
            // 
            this.lbSCS.BackColor = System.Drawing.Color.Transparent;
            this.lbSCS.Location = new System.Drawing.Point(0, 97);
            this.lbSCS.Name = "lbSCS";
            this.lbSCS.Size = new System.Drawing.Size(112, 32);
            this.lbSCS.TabIndex = 65;
            this.lbSCS.Text = "Symptomatic Carotid Stenosis";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.Controls.Add(this.rbSmoking_No);
            this.panel3.Controls.Add(this.rbSmoking_Yes);
            this.panel3.Controls.Add(this.rbSmoking_DC);
            this.panel3.Location = new System.Drawing.Point(113, 41);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(103, 24);
            this.panel3.TabIndex = 63;
            // 
            // rbSmoking_No
            // 
            this.rbSmoking_No.BackColor = System.Drawing.Color.Transparent;
            this.rbSmoking_No.Location = new System.Drawing.Point(43, 4);
            this.rbSmoking_No.Name = "rbSmoking_No";
            this.rbSmoking_No.Size = new System.Drawing.Size(12, 16);
            this.rbSmoking_No.TabIndex = 1;
            this.rbSmoking_No.UseVisualStyleBackColor = false;
            // 
            // rbSmoking_Yes
            // 
            this.rbSmoking_Yes.BackColor = System.Drawing.Color.Transparent;
            this.rbSmoking_Yes.Location = new System.Drawing.Point(8, 4);
            this.rbSmoking_Yes.Name = "rbSmoking_Yes";
            this.rbSmoking_Yes.Size = new System.Drawing.Size(12, 16);
            this.rbSmoking_Yes.TabIndex = 0;
            this.rbSmoking_Yes.UseVisualStyleBackColor = false;
            // 
            // rbSmoking_DC
            // 
            this.rbSmoking_DC.BackColor = System.Drawing.Color.Transparent;
            this.rbSmoking_DC.Checked = true;
            this.rbSmoking_DC.Location = new System.Drawing.Point(76, 4);
            this.rbSmoking_DC.Name = "rbSmoking_DC";
            this.rbSmoking_DC.Size = new System.Drawing.Size(12, 16);
            this.rbSmoking_DC.TabIndex = 2;
            this.rbSmoking_DC.TabStop = true;
            this.rbSmoking_DC.UseVisualStyleBackColor = false;
            // 
            // lbHypertension
            // 
            this.lbHypertension.BackColor = System.Drawing.Color.Transparent;
            this.lbHypertension.Location = new System.Drawing.Point(0, 73);
            this.lbHypertension.Name = "lbHypertension";
            this.lbHypertension.Size = new System.Drawing.Size(98, 16);
            this.lbHypertension.TabIndex = 59;
            this.lbHypertension.Text = "Hypertension";
            // 
            // lbSmoking
            // 
            this.lbSmoking.BackColor = System.Drawing.Color.Transparent;
            this.lbSmoking.Location = new System.Drawing.Point(0, 49);
            this.lbSmoking.Name = "lbSmoking";
            this.lbSmoking.Size = new System.Drawing.Size(67, 16);
            this.lbSmoking.TabIndex = 58;
            this.lbSmoking.Text = "Smoking";
            // 
            // xpTaskBarBoxBloodVessel
            // 
            this.xpTaskBarBoxBloodVessel.Controls.Add(this.gradientPanelBV);
            this.xpTaskBarBoxBloodVessel.HeaderImageIndex = -1;
            this.xpTaskBarBoxBloodVessel.ItemBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(170)))), ((int)(((byte)(230)))));
            this.xpTaskBarBoxBloodVessel.Location = new System.Drawing.Point(10, 1003);
            this.xpTaskBarBoxBloodVessel.Name = "xpTaskBarBoxBloodVessel";
            this.xpTaskBarBoxBloodVessel.PreferredChildPanelHeight = 200;
            this.xpTaskBarBoxBloodVessel.Size = new System.Drawing.Size(225, 233);
            this.xpTaskBarBoxBloodVessel.TabIndex = 4;
            // 
            // gradientPanelBV
            // 
            this.gradientPanelBV.AutoScroll = true;
            this.gradientPanelBV.BackgroundColor = new Syncfusion.Drawing.BrushInfo(Syncfusion.Drawing.GradientStyle.Vertical, new Syncfusion.Drawing.BrushInfoColorArrayList(new System.Drawing.Color[] {
                System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240))))),
                System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))))}));
            this.gradientPanelBV.Border3DStyle = System.Windows.Forms.Border3DStyle.Flat;
            this.gradientPanelBV.BorderColor = System.Drawing.Color.Black;
            this.gradientPanelBV.Location = new System.Drawing.Point(2, 31);
            this.gradientPanelBV.Name = "gradientPanelBV";
            this.gradientPanelBV.Size = new System.Drawing.Size(221, 200);
            this.gradientPanelBV.TabIndex = 1;
            // 
            // xpTaskBarBox3
            // 
            this.xpTaskBarBox3.Controls.Add(this.gradientPanel3);
            this.xpTaskBarBox3.HeaderImageIndex = -1;
            this.xpTaskBarBox3.ItemBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(170)))), ((int)(((byte)(230)))));
            this.xpTaskBarBox3.Location = new System.Drawing.Point(10, 1247);
            this.xpTaskBarBox3.Name = "xpTaskBarBox3";
            this.xpTaskBarBox3.PreferredChildPanelHeight = 35;
            this.xpTaskBarBox3.ShowCollapseButton = false;
            this.xpTaskBarBox3.Size = new System.Drawing.Size(225, 49);
            this.xpTaskBarBox3.TabIndex = 5;
            // 
            // gradientPanel3
            // 
            this.gradientPanel3.BackgroundColor = new Syncfusion.Drawing.BrushInfo(Syncfusion.Drawing.GradientStyle.Vertical, new Syncfusion.Drawing.BrushInfoColorArrayList(new System.Drawing.Color[] {
                System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(240)))), ((int)(((byte)(240))))),
                System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(200)))), ((int)(((byte)(200)))))}));
            this.gradientPanel3.Border3DStyle = System.Windows.Forms.Border3DStyle.Flat;
            this.gradientPanel3.BorderColor = System.Drawing.Color.Black;
            this.gradientPanel3.Controls.Add(this.buttonSearch);
            this.gradientPanel3.Location = new System.Drawing.Point(2, 12);
            this.gradientPanel3.Name = "gradientPanel3";
            this.gradientPanel3.Size = new System.Drawing.Size(221, 35);
            this.gradientPanel3.TabIndex = 2;
            // 
            // buttonSearch
            // 
            this.buttonSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSearch.BackColor = System.Drawing.Color.Transparent;
            this.buttonSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSearch.Location = new System.Drawing.Point(24, 0);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(268, 24);
            this.buttonSearch.TabIndex = 0;
            this.buttonSearch.Text = "Search";
            this.buttonSearch.UseVisualStyleBackColor = false;
            this.buttonSearch.Click += new System.EventHandler(this.buttonSearch_Click);
            // 
            // autoCompleteDataColumnInfo1
            // 
            this.autoCompleteDataColumnInfo1.ColumnHeaderText = "Column0";
            this.autoCompleteDataColumnInfo1.ImageColumn = false;
            this.autoCompleteDataColumnInfo1.MatchingColumn = false;
            this.autoCompleteDataColumnInfo1.Visible = true;
            // 
            // PatientSearch
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(986, 592);
            this.Controls.Add(this.xpTaskBar1);
            this.Controls.Add(this.gradientPanel4);
            this.Controls.Add(this.splitter1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.HelpButton = true;
            this.MaximizeBox = false;
            this.Name = "PatientSearch";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.PatientSearch_Load);
            this.gradientPanel4.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xpTaskBar1)).EndInit();
            this.xpTaskBar1.ResumeLayout(false);
            this.xpTaskBarBoxSearch.ResumeLayout(false);
            this.gradientPanel1.ResumeLayout(false);
            this.gradientPanel1.PerformLayout();
            this.xpTaskBarBoxDetails.ResumeLayout(false);
            this.gradientPanel7.ResumeLayout(false);
            this.gradientPanel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.doubleTextBoxTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.doubleTextBoxFrom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxSex)).EndInit();
            this.xpTaskBarBoxExamination.ResumeLayout(false);
            this.gradientPanel6.ResumeLayout(false);
            this.gradientPanel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateTimePickerExtTo.Calendar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateTimePickerExtTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateTimePickerExtFrom.Calendar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateTimePickerExtFrom)).EndInit();
            this.xpTaskBarBoxHistory.ResumeLayout(false);
            this.gradientPanel2.ResumeLayout(false);
            this.panel20.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.xpTaskBarBoxBloodVessel.ResumeLayout(false);
            this.xpTaskBarBox3.ResumeLayout(false);
            this.gradientPanel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Splitter splitter1;
        private Syncfusion.Windows.Forms.Tools.GradientPanel gradientPanel4;
        private System.Windows.Forms.Panel panel1;
        private Syncfusion.Windows.Forms.Tools.AutoLabel lbName;
        private Syncfusion.Windows.Forms.Tools.AutoLabel lbIDNumber;
        private System.Windows.Forms.Button buttonPatientDetails;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button1;
        private Syncfusion.Windows.Forms.Tools.AutoLabel autoLabel6;
        private System.Windows.Forms.TextBox textBox1;
        private Syncfusion.Windows.Forms.Tools.AutoLabel autoLabel7;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button buttonExaminationView;
        private System.Windows.Forms.GroupBox groupBox2;
        private Syncfusion.Windows.Forms.Tools.XPTaskBar xpTaskBar1;
        private Syncfusion.Windows.Forms.Tools.GradientPanel gradientPanel1;
        private Syncfusion.Windows.Forms.Tools.AutoLabel lbID;
        private System.Windows.Forms.TextBox textBoxID;
        private Syncfusion.Windows.Forms.Tools.AutoLabel lbFirstName;
        private System.Windows.Forms.TextBox textBoxFirstName;
        private Syncfusion.Windows.Forms.Tools.AutoLabel lbLastName;
        private System.Windows.Forms.TextBox textBoxFamilyName;
        private Syncfusion.Windows.Forms.Tools.GradientPanel gradientPanel7;
        private Syncfusion.Windows.Forms.Tools.AutoLabel lbAgeTo;
        private Syncfusion.Windows.Forms.Tools.GradientPanel gradientPanel6;
        private Syncfusion.Windows.Forms.Tools.GradientPanel gradientPanel2;
        private Syncfusion.Windows.Forms.Tools.XPTaskBarBox xpTaskBarBox3;
        private Syncfusion.Windows.Forms.Tools.GradientPanel gradientPanel3;
        private System.Windows.Forms.Button buttonSearch;
        private Syncfusion.Windows.Forms.Tools.XPTaskBarBox xpTaskBarBoxDetails;
        private System.Windows.Forms.CheckBox checkBoxSex;
        private System.Windows.Forms.CheckBox checkBoxAge;
        private System.Windows.Forms.CheckBox checkBoxDate;
        private System.Windows.Forms.CheckBox checkBoxType;
        private System.Windows.Forms.CheckBox checkBoxLocation;
        private System.Windows.Forms.CheckBox checkBoxDrName;
        private Syncfusion.Windows.Forms.Tools.ComboBoxAdv comboBoxSex;
        private Syncfusion.Windows.Forms.Tools.DoubleTextBox doubleTextBoxTo;
        private Syncfusion.Windows.Forms.Tools.DoubleTextBox doubleTextBoxFrom;
        private Syncfusion.Windows.Forms.Tools.XPTaskBarBox xpTaskBarBoxExamination;
        private System.Windows.Forms.TextBox textBoxDrName;
        private System.Windows.Forms.TextBox textBoxLocation;
        private System.Windows.Forms.TextBox textBoxType;
        private System.Windows.Forms.Label lbFrom;
        private System.Windows.Forms.Label lbTo;
        private Syncfusion.Windows.Forms.Tools.DateTimePickerAdv dateTimePickerExtFrom;
        private Syncfusion.Windows.Forms.Tools.DateTimePickerAdv dateTimePickerExtTo;
        private System.Windows.Forms.Label lbSmoking;
        private System.Windows.Forms.Label lbHypertension;
        private Syncfusion.Windows.Forms.Tools.XPTaskBarBox xpTaskBarBoxSearch;
        private Syncfusion.Windows.Forms.Tools.XPTaskBarBox xpTaskBarBoxHistory;
        private Syncfusion.Windows.Forms.Tools.XPTaskBarBox xpTaskBarBoxBloodVessel;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lbSCS;
        private System.Windows.Forms.Label lbDontCare;
        private System.Windows.Forms.Label lbNo;
        private System.Windows.Forms.Label lbYes;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.RadioButton radioButton10;
        private System.Windows.Forms.RadioButton radioButton11;
        private System.Windows.Forms.RadioButton radioButton12;
        private System.Windows.Forms.Label lbACS;
        private System.Windows.Forms.Label lbTIA;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.RadioButton radioButton16;
        private System.Windows.Forms.RadioButton radioButton17;
        private System.Windows.Forms.RadioButton radioButton18;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.RadioButton radioButton22;
        private System.Windows.Forms.RadioButton radioButton23;
        private System.Windows.Forms.RadioButton radioButton24;
        private System.Windows.Forms.Label lbCVA;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.RadioButton radioButton28;
        private System.Windows.Forms.RadioButton radioButton29;
        private System.Windows.Forms.RadioButton radioButton30;
        private System.Windows.Forms.Label lbAtrialFibrillation;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.RadioButton radioButton34;
        private System.Windows.Forms.RadioButton radioButton35;
        private System.Windows.Forms.RadioButton radioButton36;
        private System.Windows.Forms.Label lbHeartValves;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.RadioButton radioButton40;
        private System.Windows.Forms.RadioButton radioButton41;
        private System.Windows.Forms.RadioButton radioButton42;
        private System.Windows.Forms.Label lbCoronaryByPass;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.RadioButton radioButton46;
        private System.Windows.Forms.RadioButton radioButton47;
        private System.Windows.Forms.RadioButton radioButton48;
        private System.Windows.Forms.Label lbArrhythmia;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.RadioButton radioButton52;
        private System.Windows.Forms.RadioButton radioButton53;
        private System.Windows.Forms.RadioButton radioButton54;
        private System.Windows.Forms.RadioButton rbSmoking_No;
        private System.Windows.Forms.RadioButton rbSmoking_Yes;
        private System.Windows.Forms.RadioButton rbSmoking_DC;
        private System.Windows.Forms.RadioButton rbHypertension_No;
        private System.Windows.Forms.RadioButton rbHypertension_Yes;
        private System.Windows.Forms.RadioButton rbHypertension_DC;
        private System.Windows.Forms.RadioButton rbSCS_No;
        private System.Windows.Forms.RadioButton rbSCS_Yes;
        private System.Windows.Forms.RadioButton rbSCS_DC;
        private System.Windows.Forms.RadioButton rbACS_No;
        private System.Windows.Forms.RadioButton rbACS_Yes;
        private System.Windows.Forms.RadioButton rbTIA_No;
        private System.Windows.Forms.RadioButton rbTIA_Yes;
        private System.Windows.Forms.RadioButton rbTIA_DC;
        private System.Windows.Forms.RadioButton rbCVA_No;
        private System.Windows.Forms.RadioButton rbCVA_Yes;
        private System.Windows.Forms.RadioButton rbCVA_DC;
        private System.Windows.Forms.RadioButton rbAF_No;
        private System.Windows.Forms.RadioButton rbAF_Yes;
        private System.Windows.Forms.RadioButton rbAF_DC;
        private System.Windows.Forms.RadioButton rbPHV_No;
        private System.Windows.Forms.RadioButton rbPHV_Yes;
        private System.Windows.Forms.RadioButton rbPHV_DC;
        private System.Windows.Forms.RadioButton rbCBP_No;
        private System.Windows.Forms.RadioButton rbCBP_Yes;
        private System.Windows.Forms.RadioButton rbCBP_DC;
        private System.Windows.Forms.RadioButton rbArrithymia_No;
        private System.Windows.Forms.RadioButton rbArrithymia_Yes;
        private System.Windows.Forms.RadioButton rbArrithymia_DC;
        private System.Windows.Forms.TextBox textBoxOther;
        private System.Windows.Forms.CheckBox checkBoxOther;
        private Syncfusion.Windows.Forms.Tools.GradientPanel gradientPanelBV;
        private System.Windows.Forms.RadioButton rbACS_DC;
        private System.Windows.Forms.TextBox textBoxReason;
        private System.Windows.Forms.TextBox textBoxRefferedBy;
        private System.Windows.Forms.CheckBox checkBoxRefferedBy;
        private System.Windows.Forms.CheckBox checkBoxReason;
        private System.Windows.Forms.ComboBox comboBoxType;
        private System.Windows.Forms.ListView listViewExamination;
        private System.Windows.Forms.ListView listViewBV;
        //		private System.ComponentModel.IContainer components;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private Syncfusion.Windows.Forms.Tools.AutoCompleteDataColumnInfo autoCompleteDataColumnInfo1;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonBVView;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ListView listViewGate;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.Button buttonShowGate;
        private AutoCompleteComboBox autoCompleteComboBoxName;
        private AutoCompleteComboBox autoCompleteComboBoxID;
        private System.Windows.Forms.Button buttonHelp;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.Button buttonDeleteExam;
        private System.Windows.Forms.Button buttonShowReport;
    }
}