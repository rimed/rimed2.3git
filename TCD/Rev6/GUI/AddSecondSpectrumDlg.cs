﻿using System.Windows.Forms;

namespace Rimed.TCD.GUI
{
    public partial class AddSecondSpectrumDlg : Form
    {
        public AddSecondSpectrumDlg()
        {
            InitializeComponent();

            var strRes = MainForm.StringManager;// MainForm.StringManager;
            lblCaption.Text = strRes.GetString("You need to close the M-Mode window or the Trend window in order to add Spectrum window");
            btnMModeClose.Text = strRes.GetString("Close M-Mode");
            btnTrendClose.Text = strRes.GetString("Close Trend");
            btnCancel.Text = strRes.GetString("Cancel");
        }
    }
}
