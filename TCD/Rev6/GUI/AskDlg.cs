using System.Collections.Generic;
using System.Windows.Forms;
using Rimed.Framework.Common;
using Rimed.TCD.Utils;

namespace Rimed.TCD.GUI
{
	public partial class AskDlg : Form
	{
		public AskDlg()
		{
			InitializeComponent();

            var strRes  = MainForm.StringManager;// MainForm.StringManager;
            label1.Text = strRes.GetString("Cannot delete one side spectrum. Delete both sides\' spectrums?");
            Text        = strRes.GetString("About") + " DigiLite™";
            
            BackColor   = GlobalSettings.BackgroundDlg;
		}

		private void buttonNo_Click(object sender, System.EventArgs e)
		{
            LoggerUserActions.MouseClick("buttonNo", Name);
            DialogResult = DialogResult.No;
            Close();
        }

		private void buttonYes_Click(object sender, System.EventArgs e)
		{
            var fields = LoggerUserActions.GetFilesList();
            fields.Add("label1", label1.Text);
			LoggerUserActions.MouseClick("buttonYes", Name, fields);

            DialogResult = DialogResult.Yes;
            Close();
        }
	}
}
