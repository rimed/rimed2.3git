using System;
using System.Windows.Forms;
using Rimed.TCD.GUI.DSPMgr;
using Rimed.TCD.Utils;

namespace Rimed.TCD.GUI
{
    public partial class AddEventDlg : Form
    {
		private readonly int	m_probSide		= Probe.PROB_SIDE_LEFT;
	    private readonly double	m_elapseTime	= -1.0;
        public AddEventDlg()
        {
            InitializeComponent();

			var currentGv = LayoutManager.Instance.SelectedGv;
			if (currentGv != null)
			{
				if (currentGv.GateSide == GlobalTypes.EGateSide.Right)
					m_probSide = Probe.PROB_SIDE_RIGHT;

				m_elapseTime = currentGv.Spectrum.Graph.MasterCursorTime * 1000.0;
			}

			//textBoxSide.Text = ((m_probSide == Probe.PROB_SIDE_RIGHT) ? GlobalTypes.EGateSide.Right : GlobalTypes.EGateSide.Left).ToString();

			if (m_elapseTime > 0)
			{
				var ts = TimeSpan.FromMilliseconds(m_elapseTime);
				textBoxElapseTime.Text = ts.ToString(@"hh\:mm\:ss\.fff");
			}
		}

        private void AddEventDlg_Load(object sender, EventArgs e)
        {
            var strRes = MainForm.StringManager;// MainForm.StringManager;
            Text = strRes.GetString("Add Event");
            btnOK.Text = strRes.GetString("OK");
            btnCancel.Text = strRes.GetString("Cancel");
            lblName.Text = strRes.GetString("Event name");

            var events = EventListViewCtrl.Instance.PredefinedEvents;

            foreach (var item in events)
                cbEventList.Items.Add(item.Name);
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("btnOK", Name);

	        if (string.IsNullOrEmpty(cbEventList.Text))
		        return;

			DspManager.Instance.DspMgrAddHistoryEvent(cbEventList.Text, GlobalTypes.EEventType.PreDefinedEvent, true, m_probSide, m_elapseTime);
	        Close();
        }
    }
}
