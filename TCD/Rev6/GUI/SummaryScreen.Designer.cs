﻿using System.Drawing;
using System.Windows.Forms;

namespace Rimed.TCD.GUI
{
    partial class SummaryScreen
    {
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				foreach (var sv in m_dummySVs)
				{
					sv.Dispose();
				}

				m_dummySVs.Clear();
			}
			base.Dispose(disposing);
		}

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SummaryScreen));
            this.RightPanel = new System.Windows.Forms.Panel();
            this.LeftPanel = new System.Windows.Forms.Panel();
            this.MidPanel = new System.Windows.Forms.Panel();
            this.TopPanel = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.contextMenuPicture = new System.Windows.Forms.ContextMenu();
            this.menuItemSave = new System.Windows.Forms.MenuItem();
            this.menuItemReport = new System.Windows.Forms.MenuItem();
            this.menuItemPatientReport = new System.Windows.Forms.MenuItem();
            this.menuItemExamination = new System.Windows.Forms.MenuItem();
            this.menuItemSumInsertNotes = new System.Windows.Forms.MenuItem();
            this.menuItem7 = new System.Windows.Forms.MenuItem();
            this.contextMenuSpectrum = new System.Windows.Forms.ContextMenu();
            this.menuItemReplay = new System.Windows.Forms.MenuItem();
            this.menuItemDelete = new System.Windows.Forms.MenuItem();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.labelCaption = new System.Windows.Forms.Label();
            this.toolBarPanel1 = new Rimed.TCD.GUI.ToolBarPanel();
            this.mainFrameBarManager1 = new Syncfusion.Windows.Forms.Tools.XPMenus.MainFrameBarManager(this.components, this);
            this.barMainMenu = new Syncfusion.Windows.Forms.Tools.XPMenus.Bar(this.mainFrameBarManager1, "MainMenu");
            this.parentBarItemPatient = new Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem();
            this.barItemPatientNew = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemPatientLoad = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemPatientWorkList = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemPatientSearch = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemPatientDelete = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.parentBarItemExport = new Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem();
            this.barItemFullSceen = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemExportLog = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemBackup = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemRestore = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemPatientPrintPreview = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemPrint = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemExit = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemExitWindows = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.parentBarItemStudies = new Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem();
            this.parentBarItemSetup = new Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem();
            this.barItemPatientReportWizard = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.imageListMenu = new System.Windows.Forms.ImageList(this.components);
            this.barItemSetupGeneral = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemSetupStudies = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemSaveStudy = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.parentBarItemFunctions = new Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem();
            this.barItemNotes = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemPatientReport = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemSave = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemExpandClinicalParameters = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemReturn = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemAccessionNumber = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.parentBarItemHelp = new Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem();
            this.barItemHelpContext = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemHelpIndex = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemHelpSearch = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemHelpWhatIsThis = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemHelpAbout = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.parentBarItem2 = new Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem();
            this.barItemDepthDown = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemDepthUp = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemGainUp = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemGainDown = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItem4 = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemAuthorizationMgr = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.parentBarItemUtilities = new Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem();
            this.barItemAddPrinter = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.parentBarItem1 = new Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem();
            this.barItemUnFreeze = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItem1 = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.BarItem_UpZeroLine = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItem_DownZeroLine = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.ChangeBV = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemPrintScreen = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemFlashPrining = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemFFT = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemNextFunction = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.SaveDebugData = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.LoadFromFile = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemNextBV = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemPrevBV = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemDuplicateBV = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemFreqDown = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemFreqUp = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemPowerDown = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemPowerUp = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemWidthDown = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemWidthUp = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemThumpDown = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemThumpUp = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItemPanelHits = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.barItem2 = new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem();
            this.cachedcrTableRep1 = new Rimed.TCD.ReportMgr.CachedcrTableRep();
            this.cachedcrTableRep2 = new Rimed.TCD.ReportMgr.CachedcrTableRep();
            this.LRleftLabel = new System.Windows.Forms.Label();
            this.LRrightLabel = new System.Windows.Forms.Label();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainFrameBarManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // RightPanel
            // 
            this.RightPanel.AutoScroll = true;
            this.RightPanel.BackColor = System.Drawing.Color.LightSteelBlue;
            this.RightPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightPanel.Location = new System.Drawing.Point(874, 58);
            this.RightPanel.Name = "RightPanel";
            this.RightPanel.Size = new System.Drawing.Size(144, 678);
            this.RightPanel.TabIndex = 0;
            // 
            // LeftPanel
            // 
            this.LeftPanel.AutoScroll = true;
            this.LeftPanel.BackColor = System.Drawing.Color.LightSteelBlue;
            this.LeftPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftPanel.Location = new System.Drawing.Point(0, 58);
            this.LeftPanel.Name = "LeftPanel";
            this.LeftPanel.Size = new System.Drawing.Size(152, 678);
            this.LeftPanel.TabIndex = 1;
            // 
            // MidPanel
            // 
            this.MidPanel.AutoScroll = true;
            this.MidPanel.BackColor = System.Drawing.Color.LightSteelBlue;
            this.MidPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MidPanel.Location = new System.Drawing.Point(152, 358);
            this.MidPanel.Name = "MidPanel";
            this.MidPanel.Size = new System.Drawing.Size(722, 378);
            this.MidPanel.TabIndex = 2;
            // 
            // TopPanel
            // 
            this.TopPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(227)))), ((int)(((byte)(234)))), ((int)(((byte)(249)))));
            this.TopPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TopPanel.Controls.Add(this.pictureBox1);
            this.TopPanel.Controls.Add(this.LRleftLabel);
            this.TopPanel.Controls.Add(this.LRrightLabel);
            this.TopPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopPanel.Location = new System.Drawing.Point(152, 58);
            this.TopPanel.Name = "TopPanel";
            //this.TopPanel.Size = new System.Drawing.Size(722, 300);
            this.TopPanel.TabIndex = 4;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(152, 58);
            this.pictureBox1.Name = "pictureBox1";
            //this.pictureBox1.Size = new System.Drawing.Size(722, 300);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // LRleftLabel
            // 
            this.LRleftLabel.AutoSize = true;
            this.LRleftLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.LRleftLabel.Location = new System.Drawing.Point(0, 0);
            this.LRleftLabel.Name = "LRleftLabel";
            this.LRleftLabel.Size = new System.Drawing.Size(95, 18);
            this.LRleftLabel.TabIndex = 0;
            this.LRleftLabel.Text = "Lindegaard Ratio Left";
            this.LRleftLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.LRleftLabel.Visible = false;
            // 
            // LRrightLabel
            // 
            this.LRrightLabel.AutoSize = true;
            this.LRrightLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.LRrightLabel.Location = new System.Drawing.Point(0, 90);
            this.LRrightLabel.Name = "LRrightLabel";
            this.LRrightLabel.Size = new System.Drawing.Size(95, 18);
            this.LRrightLabel.TabIndex = 0;
            this.LRrightLabel.Text = "Lindegaard Ratio Right";
            this.LRrightLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.LRrightLabel.Visible = false;
            // 
            // contextMenuPicture
            // 
            this.contextMenuPicture.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemSave,
            this.menuItemReport,
            this.menuItemSumInsertNotes,
            this.menuItem7});
            this.contextMenuPicture.Popup += new System.EventHandler(this.contextMenuPicture_Popup);
            // 
            // menuItemSave
            // 
            this.menuItemSave.Index = 0;
            this.menuItemSave.Text = "";
            this.menuItemSave.Click += new System.EventHandler(this.menuItemSave_Click);
            // 
            // menuItemReport
            // 
            this.menuItemReport.Index = 1;
            this.menuItemReport.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemPatientReport,
            this.menuItemExamination});
            this.menuItemReport.Text = "";
            // 
            // menuItemPatientReport
            // 
            this.menuItemPatientReport.Index = 0;
            this.menuItemPatientReport.Text = "";
            this.menuItemPatientReport.Click += new System.EventHandler(this.menuItemPatientReport_Click);
            // 
            // menuItemExamination
            // 
            this.menuItemExamination.Index = 1;
            this.menuItemExamination.Text = "";
            this.menuItemExamination.Click += new System.EventHandler(this.menuItemExamination_Click);
            // 
            // menuItemSumInsertNotes
            // 
            this.menuItemSumInsertNotes.Index = 2;
            this.menuItemSumInsertNotes.Text = "";
            this.menuItemSumInsertNotes.Click += new System.EventHandler(this.menuItemSumInsertNotes_Click);
            // 
            // menuItem7
            // 
            this.menuItem7.Enabled = false;
            this.menuItem7.Index = 3;
            this.menuItem7.Text = "";
            // 
            // contextMenuSpectrum
            // 
            this.contextMenuSpectrum.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemReplay,
            this.menuItemDelete});
            this.contextMenuSpectrum.Popup += new System.EventHandler(this.contextMenuSpectrum_Popup);
            // 
            // menuItemReplay
            // 
            this.menuItemReplay.Index = 0;
            this.menuItemReplay.Text = "";
            this.menuItemReplay.Click += new System.EventHandler(this.menuItemReplay_Click);
            // 
            // menuItemDelete
            // 
            this.menuItemDelete.Index = 1;
            this.menuItemDelete.Text = "";
            this.menuItemDelete.Click += new System.EventHandler(this.menuItemDelete_Click);
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(256, 256);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // labelCaption
            // 
            this.labelCaption.AutoSize = true;
            this.labelCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.labelCaption.Location = new System.Drawing.Point(8, 8);
            this.labelCaption.Name = "labelCaption";
            this.labelCaption.Size = new System.Drawing.Size(95, 18);
            this.labelCaption.TabIndex = 0;
            this.labelCaption.Text = "Rimed TCD";
            this.labelCaption.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.labelCaption.Visible = false;
            // 
            // toolBarPanel1
            // 
            this.toolBarPanel1.BackColor = System.Drawing.SystemColors.Control;
            this.toolBarPanel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("toolBarPanel1.BackgroundImage")));
            this.toolBarPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.toolBarPanel1.Location = new System.Drawing.Point(0, 24);
            this.toolBarPanel1.Name = "toolBarPanel1";
            this.toolBarPanel1.Size = new System.Drawing.Size(1018, 34);
            this.toolBarPanel1.TabIndex = 6;
            // 
            // mainFrameBarManager1
            // 
            this.mainFrameBarManager1.BarPositionInfo = ((System.IO.MemoryStream)(resources.GetObject("mainFrameBarManager1.BarPositionInfo")));
            this.mainFrameBarManager1.Bars.Add(this.barMainMenu);
            this.mainFrameBarManager1.Categories.Add("Patient");
            this.mainFrameBarManager1.Categories.Add("Studies");
            this.mainFrameBarManager1.Categories.Add("Setup");
            this.mainFrameBarManager1.Categories.Add("Function");
            this.mainFrameBarManager1.Categories.Add("Utilities");
            this.mainFrameBarManager1.Categories.Add("Help");
            this.mainFrameBarManager1.Categories.Add("Debug");
            this.mainFrameBarManager1.Categories.Add("KBShortCuts");
            this.mainFrameBarManager1.Categories.Add("MainMenuPopups");
            this.mainFrameBarManager1.CurrentBaseFormType = "System.Windows.Forms.Form";
            this.mainFrameBarManager1.Form = this;
            this.mainFrameBarManager1.ImageList = null;
            this.mainFrameBarManager1.Items.AddRange(new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem[] {
            this.barItemPatientNew,
            this.barItemPatientLoad,
            this.barItemPatientWorkList,
            this.barItemBackup,
            this.parentBarItemPatient,
            this.barItemRestore,
            this.barItemPatientSearch,
            this.barItem4,
            this.barItemPatientDelete,
            this.barItemAuthorizationMgr,
            this.parentBarItemStudies,
            this.parentBarItemSetup,
            this.barItemPatientPrintPreview,
            this.barItemPrint,
            this.parentBarItemFunctions,
            this.parentBarItemUtilities,
            this.parentBarItemHelp,
            this.parentBarItem1,
            this.parentBarItem2,
            this.barItemPatientReportWizard,
            this.barItemSetupGeneral,
            this.barItemSetupStudies,
            this.barItemHelpContext,
            this.barItemHelpIndex,
            this.barItemHelpSearch,
            this.barItemHelpAbout,
            this.barItemSaveStudy,
            this.barItemUnFreeze,
            this.barItem1,
            this.BarItem_UpZeroLine,
            this.barItem_DownZeroLine,
            this.barItemGainUp,
            this.barItemGainDown,
            this.barItemDepthUp,
            this.barItemDepthDown,
            this.barItemFreqDown,
            this.barItemFreqUp,
            this.barItemPowerDown,
            this.barItemPowerUp,
            this.barItemWidthDown,
            this.barItemWidthUp,
            this.barItemThumpDown,
            this.barItemThumpUp,
            this.barItemPanelHits,
            this.barItem2,
            this.barItemHelpWhatIsThis,
            this.ChangeBV,
            this.barItemPrintScreen,
            this.barItemFlashPrining,
            this.barItemNotes,
            this.barItemPatientReport,
            this.barItemReturn,
            this.barItemAccessionNumber,
            this.barItemSave,
            this.barItemExpandClinicalParameters,
            this.barItemFFT,
            this.barItemNextFunction,
            this.SaveDebugData,
            this.LoadFromFile,
            this.barItemNextBV,
            this.barItemPrevBV,
            this.barItemDuplicateBV,
            this.barItemAddPrinter,
            this.parentBarItemExport,
            this.barItemFullSceen,
            this.barItemExportLog,
            this.barItemExit,
            this.barItemExitWindows});
            this.mainFrameBarManager1.LargeImageList = null;
            this.mainFrameBarManager1.ResetCustomization = false;
            // 
            // barMainMenu
            // 
            this.barMainMenu.BarName = "MainMenu";
            this.barMainMenu.BarStyle = ((Syncfusion.Windows.Forms.Tools.XPMenus.BarStyle)((((Syncfusion.Windows.Forms.Tools.XPMenus.BarStyle.IsMainMenu | Syncfusion.Windows.Forms.Tools.XPMenus.BarStyle.RotateWhenVertical) 
            | Syncfusion.Windows.Forms.Tools.XPMenus.BarStyle.Visible) 
            | Syncfusion.Windows.Forms.Tools.XPMenus.BarStyle.UseWholeRow)));
            this.barMainMenu.Items.AddRange(new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem[] {
            this.parentBarItemPatient,
            this.parentBarItemStudies,
            this.parentBarItemSetup,
            this.parentBarItemFunctions,
            this.parentBarItemHelp,
            this.parentBarItem2});
            this.barMainMenu.Manager = this.mainFrameBarManager1;
            // 
            // parentBarItemPatient
            // 
            this.parentBarItemPatient.CategoryIndex = 8;
            this.parentBarItemPatient.ID = "Patient";
            this.parentBarItemPatient.Items.AddRange(new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem[] {
            this.barItemPatientNew,
            this.barItemPatientLoad,
            this.barItemPatientWorkList,
            this.barItemPatientSearch,
            this.barItemPatientDelete,
            this.parentBarItemExport,
            this.barItemBackup,
            this.barItemRestore,
            this.barItemPatientPrintPreview,
            this.barItemPrint,
            this.barItemExit,
            this.barItemExitWindows});
            this.parentBarItemPatient.SeparatorIndices.AddRange(new int[] {
            3,
            4,
            5,
            7,
            9});
            // 
            // barItemPatientNew
            // 
            this.barItemPatientNew.CategoryIndex = 0;
            this.barItemPatientNew.ID = "New";
            this.barItemPatientNew.Click += new System.EventHandler(this.buttonNewPatient_Click);
            // 
            // barItemPatientLoad
            // 
            this.barItemPatientLoad.CategoryIndex = 0;
            this.barItemPatientLoad.ID = "Load";
            this.barItemPatientLoad.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // barItemPatientWorkList
            // 
            this.barItemPatientWorkList.CategoryIndex = 0;
            this.barItemPatientWorkList.ID = "Work List";
            this.barItemPatientWorkList.Click += new System.EventHandler(this.buttonWorkList_Click);
            // 
            // barItemPatientSearch
            // 
            this.barItemPatientSearch.CategoryIndex = 0;
            this.barItemPatientSearch.ID = "Search";
            this.barItemPatientSearch.Click += new System.EventHandler(this.barItemPatientSearch_Click);
            // 
            // barItemPatientDelete
            // 
            this.barItemPatientDelete.CategoryIndex = 0;
            this.barItemPatientDelete.ID = "Delete";
            this.barItemPatientDelete.Click += new System.EventHandler(this.barItemPatientDelete_Click);
            // 
            // parentBarItemExport
            // 
            this.parentBarItemExport.CategoryIndex = 0;
            this.parentBarItemExport.ID = "Export";
            this.parentBarItemExport.Items.AddRange(new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem[] {
            this.barItemFullSceen,
            this.barItemExportLog});
            // 
            // barItemFullSceen
            // 
            this.barItemFullSceen.CategoryIndex = 0;
            this.barItemFullSceen.ID = "Full Screen";
            this.barItemFullSceen.Click += new System.EventHandler(this.barItemFullSceen_Click);
            // 
            // barItemExportLog
            // 
            this.barItemExportLog.CategoryIndex = 0;
            this.barItemExportLog.ID = "Export Logs";
            this.barItemExportLog.Click += new System.EventHandler(this.barItemExportLog_Click);
            // 
            // barItemBackup
            // 
            this.barItemBackup.CategoryIndex = 4;
            this.barItemBackup.ID = "Backup";
            this.barItemBackup.Click += new System.EventHandler(this.barItemBackup_Click);
            // 
            // barItemRestore
            // 
            this.barItemRestore.CategoryIndex = 4;
            this.barItemRestore.ID = "Restore";
            this.barItemRestore.Click += new System.EventHandler(this.barItemRestore_Click);
            // 
            // barItemPatientPrintPreview
            // 
            this.barItemPatientPrintPreview.CategoryIndex = 0;
            this.barItemPatientPrintPreview.ID = "Print Preview";
            this.barItemPatientPrintPreview.Click += new System.EventHandler(this.barItemPatientPrintPreview_Click);
            // 
            // barItemPrint
            // 
            this.barItemPrint.CategoryIndex = 0;
            this.barItemPrint.ID = "Print";
            this.barItemPrint.Click += new System.EventHandler(this.barItemPrint_Click);
            // 
            // barItemExit
            // 
            this.barItemExit.CategoryIndex = 0;
            this.barItemExit.ID = "Exit";
            this.barItemExit.Click += new System.EventHandler(this.barItemExit_Click);
            // 
            // barItemExitWindows
            // 
            this.barItemExitWindows.CategoryIndex = 0;
            this.barItemExitWindows.ID = "Exit windows";
            this.barItemExitWindows.Click += new System.EventHandler(this.barItemExitWindows_Click);
            // 
            // parentBarItemStudies
            // 
            this.parentBarItemStudies.CategoryIndex = 8;
            this.parentBarItemStudies.ID = "Studies";
            // 
            // parentBarItemSetup
            // 
            this.parentBarItemSetup.CategoryIndex = 8;
            this.parentBarItemSetup.ID = "Setup";
            this.parentBarItemSetup.Items.AddRange(new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem[] {
            this.barItemPatientReportWizard,
            this.barItemSetupGeneral,
            this.barItemSetupStudies,
            this.barItemSaveStudy});
            this.parentBarItemSetup.SeparatorIndices.AddRange(new int[] {
            2});
            // 
            // barItemPatientReportWizard
            // 
            this.barItemPatientReportWizard.CategoryIndex = 2;
            this.barItemPatientReportWizard.ID = "Report Generator Wizard";
            this.barItemPatientReportWizard.ImageIndex = 0;
            this.barItemPatientReportWizard.ImageList = this.imageListMenu;
            this.barItemPatientReportWizard.Click += new System.EventHandler(this.barItemPatientReportWizard_Click);
            // 
            // imageListMenu
            // 
            this.imageListMenu.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageListMenu.ImageSize = new System.Drawing.Size(16, 16);
            this.imageListMenu.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // barItemSetupGeneral
            // 
            this.barItemSetupGeneral.CategoryIndex = 2;
            this.barItemSetupGeneral.ID = "General";
            this.barItemSetupGeneral.ImageIndex = 0;
            this.barItemSetupGeneral.ImageList = this.imageListMenu;
            this.barItemSetupGeneral.Click += new System.EventHandler(this.barItemSetupGeneral_Click);
            // 
            // barItemSetupStudies
            // 
            this.barItemSetupStudies.CategoryIndex = 2;
            this.barItemSetupStudies.ID = "Studies_1";
            this.barItemSetupStudies.Click += new System.EventHandler(this.barItemSetupStudies_Click);
            // 
            // barItemSaveStudy
            // 
            this.barItemSaveStudy.CategoryIndex = 6;
            this.barItemSaveStudy.Enabled = false;
            this.barItemSaveStudy.ID = "&save Study";
            this.barItemSaveStudy.ImageIndex = 1;
            this.barItemSaveStudy.ImageList = this.imageListMenu;
            this.barItemSaveStudy.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            // 
            // parentBarItemFunctions
            // 
            this.parentBarItemFunctions.CategoryIndex = 8;
            this.parentBarItemFunctions.ID = "Function";
            this.parentBarItemFunctions.Items.AddRange(new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem[] {
            this.barItemNotes,
            this.barItemPatientReport,
            this.barItemSave,
            this.barItemExpandClinicalParameters,
            this.barItemReturn,
            this.barItemAccessionNumber});
            this.parentBarItemFunctions.SeparatorIndices.AddRange(new int[] {
            4});
            // 
            // barItemNotes
            // 
            this.barItemNotes.CategoryIndex = 3;
            this.barItemNotes.ID = "Notes";
            this.barItemNotes.Click += new System.EventHandler(this.buttonNotes_Click);
            // 
            // barItemPatientReport
            // 
            this.barItemPatientReport.CategoryIndex = 3;
            this.barItemPatientReport.ID = "Patient Report";
            this.barItemPatientReport.Click += new System.EventHandler(this.barItemPatientReport_Click);
            // 
            // barItemSave
            // 
            this.barItemSave.CategoryIndex = 3;
            this.barItemSave.ID = "Save";
            this.barItemSave.Click += new System.EventHandler(this.barItemSave_Click);
            // 
            // barItemExpandClinicalParameters
            // 
            this.barItemExpandClinicalParameters.CategoryIndex = 3;
            this.barItemExpandClinicalParameters.ID = "Expand Clinical Parameters";
            this.barItemExpandClinicalParameters.Click += new System.EventHandler(this.buttonClinicalParameters_Click);
            // 
            // barItemReturn
            // 
            this.barItemReturn.CategoryIndex = 3;
            this.barItemReturn.ID = "Return";
            this.barItemReturn.Click += new System.EventHandler(this.buttonReturn_Click);
            // 
            // barItemAccessionNumber
            // 
            // Added by Alex 04/04/2016 fixed CR #843 v. 2.2.3.28
            this.barItemAccessionNumber.CategoryIndex = 3;
            this.barItemAccessionNumber.ID = "Accession Number";
            this.barItemAccessionNumber.Click += new System.EventHandler(this.buttonAccessionNumber_Click);
            // 
            // parentBarItemHelp
            // 
            this.parentBarItemHelp.CategoryIndex = 8;
            this.parentBarItemHelp.ID = "Help";
            this.parentBarItemHelp.Items.AddRange(new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem[] {
            this.barItemHelpContext,
            this.barItemHelpIndex,
            this.barItemHelpSearch,
            this.barItemHelpWhatIsThis,
            this.barItemHelpAbout});
            this.parentBarItemHelp.SeparatorIndices.AddRange(new int[] {
            3,
            4});
            // 
            // barItemHelpContext
            // 
            this.barItemHelpContext.CategoryIndex = 5;
            this.barItemHelpContext.ID = "Context";
            // 
            // barItemHelpIndex
            // 
            this.barItemHelpIndex.CategoryIndex = 5;
            this.barItemHelpIndex.ID = "Index";
            // 
            // barItemHelpSearch
            // 
            this.barItemHelpSearch.CategoryIndex = 5;
            this.barItemHelpSearch.ID = "Search_1";
            // 
            // barItemHelpWhatIsThis
            // 
            this.barItemHelpWhatIsThis.CategoryIndex = 5;
            this.barItemHelpWhatIsThis.ID = "What is this?";
            // 
            // barItemHelpAbout
            // 
            this.barItemHelpAbout.CategoryIndex = 5;
            this.barItemHelpAbout.ID = "About";
            this.barItemHelpAbout.Click += new System.EventHandler(this.barItemHelpAbout_Click);
            // 
            // parentBarItem2
            // 
            this.parentBarItem2.CategoryIndex = 8;
            this.parentBarItem2.ID = "KBShortCuts";
            this.parentBarItem2.Items.AddRange(new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem[] {
            this.barItemDepthDown,
            this.barItemDepthUp,
            this.barItemGainUp,
            this.barItemGainDown});
            this.parentBarItem2.Visible = false;
            // 
            // barItemDepthDown
            // 
            this.barItemDepthDown.CategoryIndex = 7;
            this.barItemDepthDown.ID = "Depth Down";
            this.barItemDepthDown.Shortcut = System.Windows.Forms.Shortcut.F1;
            // 
            // barItemDepthUp
            // 
            this.barItemDepthUp.CategoryIndex = 7;
            this.barItemDepthUp.ID = "Depth Up";
            this.barItemDepthUp.Shortcut = System.Windows.Forms.Shortcut.F2;
            // 
            // barItemGainUp
            // 
            this.barItemGainUp.CategoryIndex = 7;
            this.barItemGainUp.ID = "Gain Up";
            this.barItemGainUp.Shortcut = System.Windows.Forms.Shortcut.F4;
            // 
            // barItemGainDown
            // 
            this.barItemGainDown.CategoryIndex = 7;
            this.barItemGainDown.ID = "Gain Down";
            this.barItemGainDown.Shortcut = System.Windows.Forms.Shortcut.F3;
            // 
            // barItem4
            // 
            this.barItem4.CategoryIndex = 4;
            this.barItem4.ID = "Remote Station";
            // 
            // barItemAuthorizationMgr
            // 
            this.barItemAuthorizationMgr.CategoryIndex = 4;
            this.barItemAuthorizationMgr.ID = "Authorization Manager...";
            // 
            // parentBarItemUtilities
            // 
            this.parentBarItemUtilities.CategoryIndex = 8;
            this.parentBarItemUtilities.ID = "Utilities";
            this.parentBarItemUtilities.Items.AddRange(new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem[] {
            this.barItem4,
            this.barItemAddPrinter,
            this.barItemAuthorizationMgr});
            this.parentBarItemUtilities.SeparatorIndices.AddRange(new int[] {
            2});
            this.parentBarItemUtilities.Visible = false;
            // 
            // barItemAddPrinter
            // 
            this.barItemAddPrinter.CategoryIndex = 4;
            this.barItemAddPrinter.ID = "Add Printer...";
            // 
            // parentBarItem1
            // 
            this.parentBarItem1.CategoryIndex = 8;
            this.parentBarItem1.ID = "&Debug";
            this.parentBarItem1.Items.AddRange(new Syncfusion.Windows.Forms.Tools.XPMenus.BarItem[] {
            this.barItemSaveStudy,
            this.barItemUnFreeze,
            this.barItem1,
            this.BarItem_UpZeroLine,
            this.barItem_DownZeroLine,
            this.ChangeBV,
            this.barItemPrintScreen,
            this.barItemFlashPrining,
            this.barItemFFT,
            this.barItemNextFunction,
            this.SaveDebugData,
            this.LoadFromFile,
            this.barItemNextBV,
            this.barItemPrevBV,
            this.barItemDuplicateBV});
            // 
            // barItemUnFreeze
            // 
            this.barItemUnFreeze.CategoryIndex = 6;
            this.barItemUnFreeze.ID = "UnFreeze";
            // 
            // barItem1
            // 
            this.barItem1.CategoryIndex = 6;
            this.barItem1.ID = "ReadFFT";
            this.barItem1.Text = "ReadFFT";
            // 
            // BarItem_UpZeroLine
            // 
            this.BarItem_UpZeroLine.CategoryIndex = 6;
            this.BarItem_UpZeroLine.ID = "UpZeroLine";
            this.BarItem_UpZeroLine.Shortcut = System.Windows.Forms.Shortcut.CtrlU;
            // 
            // barItem_DownZeroLine
            // 
            this.barItem_DownZeroLine.CategoryIndex = 6;
            this.barItem_DownZeroLine.ID = "DownZeroLine";
            this.barItem_DownZeroLine.Shortcut = System.Windows.Forms.Shortcut.CtrlD;
            // 
            // ChangeBV
            // 
            this.ChangeBV.CategoryIndex = 6;
            this.ChangeBV.ID = "ChangeBV";
            this.ChangeBV.Text = "ChangeBV";
            // 
            // barItemPrintScreen
            // 
            this.barItemPrintScreen.CategoryIndex = 6;
            this.barItemPrintScreen.ID = "Print Screen";
            this.barItemPrintScreen.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftP;
            // 
            // barItemFlashPrining
            // 
            this.barItemFlashPrining.CategoryIndex = 6;
            this.barItemFlashPrining.ID = "Flash Printing";
            this.barItemFlashPrining.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftM;
            // 
            // barItemFFT
            // 
            this.barItemFFT.CategoryIndex = 6;
            this.barItemFFT.ID = "Create FFTGraph";
            this.barItemFFT.Text = "Create FFTGraph";
            // 
            // barItemNextFunction
            // 
            this.barItemNextFunction.CategoryIndex = 6;
            this.barItemNextFunction.ID = "NextFunction";
            this.barItemNextFunction.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftN;
            // 
            // SaveDebugData
            // 
            this.SaveDebugData.CategoryIndex = 8;
            this.SaveDebugData.ID = "SaveDebugData";
            this.SaveDebugData.Text = "SaveDebugData";
            // 
            // LoadFromFile
            // 
            this.LoadFromFile.CategoryIndex = 8;
            this.LoadFromFile.ID = "LoadFromFile";
            this.LoadFromFile.Text = "LoadFromFile";
            // 
            // barItemNextBV
            // 
            this.barItemNextBV.CategoryIndex = 8;
            this.barItemNextBV.ID = "NextBV";
            this.barItemNextBV.Text = "NextBV";
            // 
            // barItemPrevBV
            // 
            this.barItemPrevBV.CategoryIndex = 8;
            this.barItemPrevBV.ID = "PrevBV";
            this.barItemPrevBV.Text = "PrevBV";
            // 
            // barItemDuplicateBV
            // 
            this.barItemDuplicateBV.CategoryIndex = 8;
            this.barItemDuplicateBV.ID = "DuplicateBV";
            this.barItemDuplicateBV.Text = "DuplicateBV";
            // 
            // barItemFreqDown
            // 
            this.barItemFreqDown.CategoryIndex = 7;
            this.barItemFreqDown.ID = "FreqDown";
            this.barItemFreqDown.Shortcut = System.Windows.Forms.Shortcut.F5;
            this.barItemFreqDown.Text = "RangeDown";
            // 
            // barItemFreqUp
            // 
            this.barItemFreqUp.CategoryIndex = 7;
            this.barItemFreqUp.ID = "FreqUp";
            this.barItemFreqUp.Shortcut = System.Windows.Forms.Shortcut.F6;
            this.barItemFreqUp.Text = "RangeUp";
            // 
            // barItemPowerDown
            // 
            this.barItemPowerDown.CategoryIndex = 7;
            this.barItemPowerDown.ID = "PowerDown";
            this.barItemPowerDown.Shortcut = System.Windows.Forms.Shortcut.F7;
            this.barItemPowerDown.Text = "PowerDown";
            // 
            // barItemPowerUp
            // 
            this.barItemPowerUp.CategoryIndex = 7;
            this.barItemPowerUp.ID = "PowerUp";
            this.barItemPowerUp.Shortcut = System.Windows.Forms.Shortcut.F8;
            this.barItemPowerUp.Text = "PowerUp";
            // 
            // barItemWidthDown
            // 
            this.barItemWidthDown.CategoryIndex = 7;
            this.barItemWidthDown.ID = "Width Down";
            this.barItemWidthDown.Shortcut = System.Windows.Forms.Shortcut.F9;
            this.barItemWidthDown.Text = "WidthDown";
            // 
            // barItemWidthUp
            // 
            this.barItemWidthUp.CategoryIndex = 7;
            this.barItemWidthUp.ID = "WidhtUp";
            this.barItemWidthUp.Shortcut = System.Windows.Forms.Shortcut.F10;
            this.barItemWidthUp.Text = "WidhtUp";
            // 
            // barItemThumpDown
            // 
            this.barItemThumpDown.CategoryIndex = 7;
            this.barItemThumpDown.ID = "ThumpDown";
            this.barItemThumpDown.Shortcut = System.Windows.Forms.Shortcut.F11;
            this.barItemThumpDown.Text = "ThumpDown";
            // 
            // barItemThumpUp
            // 
            this.barItemThumpUp.CategoryIndex = 7;
            this.barItemThumpUp.ID = "ThumpUp";
            this.barItemThumpUp.Shortcut = System.Windows.Forms.Shortcut.F12;
            this.barItemThumpUp.Text = "ThumpUp";
            // 
            // barItemPanelHits
            // 
            this.barItemPanelHits.CategoryIndex = 7;
            this.barItemPanelHits.ID = "PanelHits";
            this.barItemPanelHits.Shortcut = System.Windows.Forms.Shortcut.CtrlShiftH;
            this.barItemPanelHits.Text = "PanelHits";
            this.barItemPanelHits.Visible = false;
            // 
            // barItem2
            // 
            this.barItem2.CategoryIndex = 3;
            this.barItem2.Checked = true;
            this.barItem2.ID = "Freeze";
            this.barItem2.Text = "UnFreeze";
            // 
            // SummaryScreen
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1018, 736);
            this.ContextMenu = this.contextMenuPicture;
            this.ControlBox = false;
            this.Controls.Add(this.MidPanel);
            this.Controls.Add(this.TopPanel);
            this.Controls.Add(this.LeftPanel);
            this.Controls.Add(this.RightPanel);
            this.Controls.Add(this.toolBarPanel1);
            this.Controls.Add(this.labelCaption);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SummaryScreen";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.SummaryScreen_Load);
            this.VisibleChanged += new System.EventHandler(this.SummaryScreen_VisibleChanged);
            this.TopPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainFrameBarManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.IContainer components;
		private System.Windows.Forms.Panel TopPanel;
		private System.Windows.Forms.Panel RightPanel;
        private System.Windows.Forms.Panel LeftPanel;
        private System.Windows.Forms.Panel MidPanel;
        private System.Windows.Forms.ContextMenu contextMenuPicture;
        private System.Windows.Forms.MenuItem menuItem7;
        private System.Windows.Forms.ContextMenu contextMenuSpectrum;
        private System.Windows.Forms.MenuItem menuItemSave;
        private System.Windows.Forms.MenuItem menuItemReport;
        private System.Windows.Forms.MenuItem menuItemPatientReport;
        private System.Windows.Forms.MenuItem menuItemExamination;
        private System.Windows.Forms.MenuItem menuItemReplay;
        private System.Windows.Forms.MenuItem menuItemDelete;
        private System.Windows.Forms.MenuItem menuItemSumInsertNotes;
        private System.Windows.Forms.ImageList imageList1;
        private ToolBarPanel toolBarPanel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label LRleftLabel;
        private System.Windows.Forms.Label LRrightLabel;

        /// /////////////////////////////////////////////////////////////////////
        private Syncfusion.Windows.Forms.Tools.XPMenus.MainFrameBarManager mainFrameBarManager1;
        private Syncfusion.Windows.Forms.Tools.XPMenus.Bar barMainMenu;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemPatientNew;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemPatientLoad;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemPatientWorkList;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemPatientSearch;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemPatientDelete;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemPatientPrintPreview;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemPrint;
        //private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemMinimize;
        private Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem parentBarItemPatient;
        private Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem parentBarItemStudies;
        private Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem parentBarItemSetup;
        private Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem parentBarItemFunctions;
        private Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem parentBarItemUtilities;
        private Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem parentBarItemHelp;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemPatientReportWizard;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemSetupGeneral;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemSetupStudies;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemHelpContext;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemHelpIndex;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemHelpSearch;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemHelpAbout;
        private Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem parentBarItem1;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemBackup;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemRestore;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemSaveStudy;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemUnFreeze;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItem1;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem BarItem_UpZeroLine;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItem_DownZeroLine;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemGainUp;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemGainDown;
        private Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem parentBarItem2;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemDepthUp;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemDepthDown;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemFreqDown;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemFreqUp;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemPowerDown;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemPowerUp;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemWidthDown;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemWidthUp;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemThumpDown;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemThumpUp;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemPanelHits;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItem2;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItem4;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemHelpWhatIsThis;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem ChangeBV;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemPrintScreen;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemFlashPrining;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemNotes;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemPatientReport;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemReturn;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemAccessionNumber;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemSave;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemExpandClinicalParameters;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemFFT;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemNextFunction;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem SaveDebugData;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem LoadFromFile;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemNextBV;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemPrevBV;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemDuplicateBV;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemAuthorizationMgr;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemAddPrinter;
        private Syncfusion.Windows.Forms.Tools.XPMenus.ParentBarItem parentBarItemExport;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemFullSceen;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemExportLog;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemExitWindows;
        private Syncfusion.Windows.Forms.Tools.XPMenus.BarItem barItemExit;
        private System.Windows.Forms.ImageList imageListMenu;
		private ReportMgr.CachedcrTableRep cachedcrTableRep1;
		private ReportMgr.CachedcrTableRep cachedcrTableRep2;
        private System.Windows.Forms.Label labelCaption;
    }
}