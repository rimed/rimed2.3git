using System;

namespace Rimed.TCD.GUI
{
	/// <summary>
	/// Summary description for ExportElement.
	/// </summary>
	public class ExportElement
	{
		public DateTime CurrentTime;

		public readonly double[] Parameters;

        //OFER: Usage not found private const int DEFAULT_COUNT = 5;
		
		/// <summary>Count is number of displayed clinical parameters</summary>
		public ExportElement(int count)
		{
			Parameters = new double[count];
		}

        //OFER: Usage not found 
        //public ExportElement()
        //{
        //    parameters = new double[defaultCount];
        //}
	}
}
