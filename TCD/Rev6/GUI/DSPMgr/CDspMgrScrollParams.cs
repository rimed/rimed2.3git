namespace Rimed.TCD.GUI.DSPMgr
{
    public class CDspMgrScrollParams
    {
        public CDspMgrScrollParams(bool isScrollBack, int paramNumberOfStepsToMove)
        {
            IsScrollBack        = isScrollBack;
            NumberOfStepsToMove = paramNumberOfStepsToMove;
        }

        public readonly bool IsScrollBack;
        public readonly int NumberOfStepsToMove;
    }
}