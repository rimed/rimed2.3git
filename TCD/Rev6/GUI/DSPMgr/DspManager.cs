using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Rimed.Framework.Threads;
using Rimed.TCD.DspBlock;

using Rimed.Framework.Common;
using Rimed.TCD.CommunicationLayer;
using Rimed.TCD.ManagedInfrastructure;
using Rimed.TCD.DAL;
using Rimed.TCD.DSPData;
using Rimed.TCD.Utils;

//NOTE: Don't turn off timer, do send pc2dsp in all modes
namespace Rimed.TCD.GUI.DSPMgr
{
    public class DspManager : Disposable
    {
		private static DspManager s_dspManager = new DspManager();
		private static readonly object s_mgrLocker = new object();


		public static DspManager Instance {get { return s_dspManager; }}

		public static bool Restart(int restartTimeoutSec = 30)
		{
			Logger.LogInfo("DSP Manager restart (timeout={0} sec).", restartTimeoutSec);
			lock (s_mgrLocker)
			{
				if (s_dspManager != null)
				{
					s_dspManager.stop();
					s_dspManager.Dispose();
				}

				s_dspManager = new DspManager();
				return s_dspManager.start(restartTimeoutSec);
			}
		}


		public static bool IsRunning { get { return Instance != null && Instance.m_isRunning; } }

		public static bool Start(int startWaitSec = -1)
		{
			if (IsRunning)
				return false;

			return Instance.start(startWaitSec);
		}

		public static void Stop()
		{
			if (Instance == null)
				return;
			
			DspManager tmp;
			lock (s_mgrLocker)
			{
				tmp = s_dspManager;
				s_dspManager = null;
			}

			if (tmp != null)
				tmp.stop();

			tmp.TryDispose();
		}

		#region Constants

	    private const int DSP_TX_ALIVE_INTERVAL_MS = 250;
        private const string CLASS_NAME = "DspMgr";
        private const int FFT_SIZE256 = 256;
        private const int FFT_SIZE128 = 128;
        private const int FFT_SIZE64 = 64;

		public const double DSP_MESSAGE_RATE_IN_SEC						= 1000.0 / RawDataFile.DSP_MESSAGE_DURATION_MSEC;	// = 1000.0/(DspBlockConst.COLUMN_DURATION*DspBlockConst.NUM_OF_COLUMNS);

		private const int THREAD_START_STOP_DELAY_MSEC					= (int)(RawDataFile.DSP_MESSAGE_DURATION_MSEC + 1);
		private const int TIMER_TIMEOUT_MSEC							= (int)(RawDataFile.DSP_MESSAGE_DURATION_MSEC + 1);

	    /// <summary>= 1000 milliseconds.</summary>
		private const int TIMER_DISPOSE_WAIT_TIMEOUT_MSEC				= 1000;
		private const int PCTODSP_ROUTINE_INTERVAL_MSEC					= 100;
		private const int DSP_ROUTINE_CONNECTION_CHECK_INTERVAL_MSEC	= 1000;
		private const int WAIT_IO_COMPLETE_TIMEOUT_MSEC					= 20 * 1000;
		#endregion  Constants

	
		#region Constructors
		private DspManager()
		{
			DspState = EDspState.DSP_UNKNOWN;

			m_studyParams = new CDspMgrStudyParams(null, 0, EStudyLayoutType.None);
			m_pc2DspMutex = new Mutex();
			m_smPendingEvents = new PendingEventsQueue();

			m_dspDataPublisherTimer = new System.Threading.Timer(timerMethod, this, Timeout.Infinite, Timeout.Infinite);

			m_rawDataFileMgr = null;

			m_drawLastBufferIndex = -1;
			m_studyAbsoluteLastBufferIndex = -1;
			m_eventBufferStartPoint = 0;
			m_replayJumpToEventData = null;

			m_rawDataForDiagnosticStudyScroll = null;

			m_pc2Dsp	= new RPc2Dsp();
			m_cardInfo	= new RCardInfo(RPc2Dsp.MemOffset.CardInfoMemOffset);

			initCommLayer();

			m_dspRxPullThread = new Thread(dspRxThreadDelegate);
			m_dspRxPullThread.IsBackground = true;
			m_dspRxPullThread.SetName("DspMgr.RxThread");
		}
		#endregion Constructors


		#region Public Events
		public static event Action DspEndPlayback;
		public static event Action DspEndDrawHistoryEvent;
		#endregion

	
		#region Public Properties
		public string		FPGAVersion { get; private set; }
        public string		DSPVersion { get; private set; }
        public string		HwVersion { get; private set; }
	    public bool			HwConnected { get; private set; }
		public EDspState	DspState { get; private set; }

	    public bool			IsCommInitiated { get { return DspState != EDspState.DSP_UNKNOWN && DspState != EDspState.DSP_HW_INIT_STATE; } }

	    public bool			IsReplayMode
	    {
		    get { return DspState == EDspState.DSP_REPLAY_DRAW_EVENT_STATE || DspState == EDspState.DSP_REPLAY_PAUSE_STATE || DspState == EDspState.DSP_REPLAY_STATE; }
	    }
        #endregion Properties

		#region Public Methods

		private bool start(int startWaitSec = -1)
		{
		    Logger.LogInfo("Starting DSP Manager...");

			//Logger.DebugStringOut("Start({0}): m_commLayerBoundary.CommLayerBoundaryStart()", startWaitSec);
			if (! m_commLayerBoundary.CommLayerBoundaryStart())
			{
				Logger.LogError("DSP Manager fail to start. Check DSP communication log for more details.");
				return false;
			}

			m_dspRxPullThread.Start();
			m_isRunning = true;

            timerOn();
                    
            if (startWaitSec > 0)
                return startWait(startWaitSec);

			Thread.Sleep(THREAD_START_STOP_DELAY_MSEC);
			Logger.LogInfo("DSP Manager started.");

            return true;
		}

        private bool stop()
        {
			if (!m_isRunning)
                return true;

			Logger.LogWarning("Stopping DSP Manager...");
			m_isRunning = false;
            timerOff();
			Thread.Sleep(THREAD_START_STOP_DELAY_MSEC);

			if (m_commLayerBoundary != null)
            {
                try
			    {
					//Logger.DebugStringOut("Stop(): m_commLayerBoundary.CommLayerBoundaryStop()");
					DspState			= EDspState.DSP_UNKNOWN;
					m_commLayerBoundary.CommLayerBoundaryStop();

					m_commLayerBoundary.Dispose();
				    m_commLayerBoundary = null;
			    }
			    catch (Exception ex)
			    {
                    Logger.LogError(ex);
			    }
            }
			DspState = EDspState.DSP_STOPED;

            Logger.LogInfo("DSP Manager stooped.");
			Thread.Sleep(THREAD_START_STOP_DELAY_MSEC);

            return true;
        }

		///// <summary>Write flow direction</summary>
		//public void WriteFlowDirection2Dsp(int probeIndex, int flowDirection)
		//{
		//    // the DSP can not get 'double number there for we multiple the value.

		//    var probInfo = ProbesCollection.Instance.GetProbInfo(probeIndex);
		//    if (probInfo != null)
		//        probInfo.DirectionSwitch = (ushort)flowDirection;
		//}

		//public void WriteProbe2Dsp(int probeIndex, int dspCode)
		//{
		//    var probInfo = ProbesCollection.Instance.GetProbInfo(probeIndex);
		//    if (probInfo != null)
		//        probInfo.ProbeType = (ushort)dspCode;
		//}

		//public void WriteHitsEnable2Dsp(int probeIndex, int indexInProbe, bool active)
		//{
		//    var hitsEnable = ushort.MinValue;
		//    if (active)
		//        hitsEnable++;

		//    var probInfo = ProbesCollection.Instance.GetProbInfo(probeIndex);
		//    if (probInfo != null)
		//    {
		//        var gi = probInfo.GetMGate(indexInProbe);
		//        gi.HitEnable = hitsEnable;
		//    }
		//}

		//public void WriteSensitivity2DSP(int probeIndex, int sensitivity)
		//{
		//    var probInfo = ProbesCollection.Instance.GetProbInfo(probeIndex);
		//    if (probInfo != null)
		//        probInfo.SetReserve(0, (Byte)sensitivity);
		//}

		public void WriteOffline2Dsp()
		{
			m_cardInfo.OperationMode = (ushort)GlobalTypes.ESystemModes.Offline;
		}

		/// <summary>Write Volume</summary>
		public void WriteVolume2Dsp(ushort vol)
		{
			m_cardInfo.SoundVolume = vol;
		}

		public void WriteGate2Hear2Dsp(ushort gate2Hear)
		{
			m_cardInfo.Gate2Hear		= gate2Hear;
			AudioDSPtoPCStreamPlayer.Gate2Hear = gate2Hear;
		}
		
		public void WriteSpeakerOrHeadphone2Dsp(bool speakerActive)
		{
			m_cardInfo.SpeakerOrHeadphone = (ushort)((speakerActive) ? 0 : 1);
		}

        public void WriteHitThreshHold2Dsp(int threshHold)
        {
            m_cardInfo.HitThreshold = (ushort)(threshHold);
        }

		public void WriteMModeThreshold2DSP(int threshold1, int threshold2)
		{
			m_cardInfo.MModeThreshold1 = (Byte)threshold1;
			m_cardInfo.MModeThreshold2 = (Byte)threshold2;
		}

        //Changed by Alex Bug #613 v.2.2.2.10 15/01/2015
        public void WriteAveragingCP2Dsp(byte AveragingCP)
        {
            m_cardInfo.AverageCP = AveragingCP;
        }

        //Added by Alex Feature #755 v.2.2.3.19 04/01/2016
        public void WriteAutoMuteEnable2Dsp(byte AutoMuteEnable)
        {
            m_cardInfo.AutoMuteEnable = AutoMuteEnable;
        }

		public void DspMgrNewStudy(CDspMgrStudyParams studyParams)
		{
            var studyParamsArr = new Object[1];
			studyParamsArr[0] = studyParams;
			if (studyParams.StudyType == EStudyLayoutType.Diagnostic)
				m_smPendingEvents.Add(EDspSmEvent.DSP_NEW_STUDY_DIAGNOSTIC_EVENT, studyParamsArr);
			else if (studyParams.StudyType == EStudyLayoutType.Monitoring)
                m_smPendingEvents.Add(EDspSmEvent.DSP_NEW_STUDY_MONITORING_EVENT, studyParamsArr);
		}

		public void DspMgrReplayNew(CDspMgrStudyParams studyParams)
		{
            var studyParamsArr = new Object[1];
			studyParamsArr[0] = studyParams;

			if (studyParams.StudyType == EStudyLayoutType.Diagnostic)
                m_smPendingEvents.Add(EDspSmEvent.DSP_NEW_REPLAY_DIAGNOSTIC_EVENT, studyParamsArr);
			else if (studyParams.StudyType == EStudyLayoutType.Monitoring)
                m_smPendingEvents.Add(EDspSmEvent.DSP_NEW_REPLAY_MONITORING_EVENT, studyParamsArr);
		}

		public void DspMgrReplayStop()
		{
            m_smPendingEvents.Add(EDspSmEvent.DSP_STOP_REPLAY_EVENT);
		}

		public void DspMgrReplayPause()
		{
            m_smPendingEvents.Add(EDspSmEvent.DSP_PAUSE_REPLAY_EVENT);
		}

		public void DspMgrReplayResume()
		{
            m_smPendingEvents.Add(EDspSmEvent.DSP_RESUME_REPLAY_EVENT);
		}

		public void DspMgrReplayJumpToEvent(CDspMgrReplayJumpToEventData replayJumpToEventData)
		{
			var paramsArr = new object[1];
			paramsArr[0] = replayJumpToEventData;
            m_smPendingEvents.Add(EDspSmEvent.DSP_REPLAY_JUMP_TO_EVENT, paramsArr);
		}

		public void DspMgrRecordStart(string rawDataDirName)
		{
            m_smPendingEvents.Add(EDspSmEvent.DSP_START_RECORD_EVENT, rawDataDirName);
		}

		public void DspMgrFinishStudySaveStorageWait()
		{
			var tmprawDataFileMgr = m_rawDataFileMgr;

            // Reset wait handles
			if (tmprawDataFileMgr == null)
		        return;

			tmprawDataFileMgr.ResetWaitHandels();

            // Post new state request
            m_smPendingEvents.Add(EDspSmEvent.DSP_FINISH_STUDY_SAVE_STORAGE_EVENT);

			tmprawDataFileMgr.WaitIoCompleate(WAIT_IO_COMPLETE_TIMEOUT_MSEC);
			//tmprawDataFileMgr.TryDispose();
			m_rawDataFileMgr = null;
		}

		public void DspMgrFinishStudyRemoveStorage()
		{
            m_smPendingEvents.Add(EDspSmEvent.DSP_FINISH_STUDY_REMOVE_STORAGE_EVENT);
			//TODO Add waithandle for event process and after that wait for IO completion!
		}

		public void DspMgrFreeze()
		{
            m_smPendingEvents.Add(EDspSmEvent.DSP_FREEZE_EVENT);
		}

		public void DspMgrUnFreeze()
		{
            m_smPendingEvents.Add(EDspSmEvent.DSP_UNFREEZE_EVENT);
		}

		public void DspMgrScroll(Object[] paramsArr)
		{
            m_smPendingEvents.Add(EDspSmEvent.DSP_SCROLL_EVENT, paramsArr);
		}

		public void DspMgrAddHistoryEvent(string eventName, GlobalTypes.EEventType eventType, bool postEvent, int probSode = Probe.PROB_SIDE_LEFT, double timeOffsetMSec = -1.0)
		{
            var paramsArr = new Object[5];
			paramsArr[0] = eventName;			// Name
			paramsArr[1] = eventType;			// Type
			paramsArr[2] = postEvent;			
			paramsArr[3] = timeOffsetMSec;		// Event elapse time
			paramsArr[4] = probSode;			// Probe side
			m_smPendingEvents.Add(EDspSmEvent.DSP_ADD_HISTORY_EVENT, paramsArr);
		}

		public void DspMgrLoadBasicConfig()
		{
			switch (RimedDal.Instance.ConfigDS.tb_BasicConfiguration[0].FFT_SIZE)
			{
				case FFT_SIZE256:
					m_cardInfo.FFTSize = DspBlockConst.FFT_SIZE256;
					break;
				case FFT_SIZE128:
					m_cardInfo.FFTSize = DspBlockConst.FFT_SIZE128;
					break;
				case FFT_SIZE64:
					m_cardInfo.FFTSize = DspBlockConst.FFT_SIZE64;
					break;
				default:
					m_cardInfo.FFTSize = DspBlockConst.FFT_SIZE256;
					break;
			}
			m_cardInfo.MModeThreshold1 = (Byte)RimedDal.Instance.ConfigDS.tb_BasicConfiguration[0].M_ModeThreshould1;
			m_cardInfo.MModeThreshold2 = (Byte)RimedDal.Instance.ConfigDS.tb_BasicConfiguration[0].M_ModeThreshould2;
		}

		#endregion Public Methods


		#region Override base methods
		protected override void DisposeManagedResources()
		{
			//TODO, OFER: Add flag to exit timer method-> dspTransferTask-> while
			stop();

			if (m_dspDataPublisherTimer != null)
			{
				var tmpTimer			= m_dspDataPublisherTimer;
				m_dspDataPublisherTimer = null;

				var disposeWait			= new AutoResetEvent(false);
				tmpTimer.Dispose(disposeWait);
				disposeWait.WaitOne(TIMER_DISPOSE_WAIT_TIMEOUT_MSEC);
			}

			if (m_pc2DspMutex != null)
			{
				m_pc2DspMutex.Dispose();
				m_pc2DspMutex = null;
			}

			base.DisposeManagedResources();
		}

		protected override void DisposeUnmanagedResources()
		{
			if (m_commLayerBoundary != null)
				m_commLayerBoundary.Dispose();

			m_commLayerBoundary = null;

			base.DisposeUnmanagedResources();
		}
		#endregion


		#region Private HelperClasses
		private class PendingEventsQueue
		{
			//private const string CLASS_NAME = "PendingEventsQueue";
			private readonly Queue<CSmEvent<EDspSmEvent>> m_dspSmEventsQueue = new Queue<CSmEvent<EDspSmEvent>>();

			public void Add(EDspSmEvent smEventNum, string strParam)
			{
				var paramsArr = new object[1]; ;
				paramsArr[0] = strParam;
				Add(smEventNum, paramsArr);
			}

			public void Add(EDspSmEvent smEventNum)
			{
				var smEvent = new CSmEvent<EDspSmEvent>(smEventNum, null);
				add(smEvent);
			}

			public void Add(EDspSmEvent smEventNum, object[] eventParamsArr)
			{
				var smEvent = new CSmEvent<EDspSmEvent>(smEventNum, eventParamsArr);
				add(smEvent);
			}

			private void add(CSmEvent<EDspSmEvent> dspEvent)
			{
				lock (m_dspSmEventsQueue)
				{
					m_dspSmEventsQueue.Enqueue(dspEvent);
				}
			}

			public CSmEvent<EDspSmEvent> Dequeue()
			{
				lock (m_dspSmEventsQueue)
				{
					if (!IsEmpty)
					{
						var dspEvent = m_dspSmEventsQueue.Dequeue();
						return dspEvent;
					}
				}

				return null;
			}

			public bool IsEmpty { get { return m_dspSmEventsQueue.Count == 0; } }
		}
		#endregion

		#region Private Fields
		private enum EDspSmEvent
        {
            DSP_NEW_STUDY_MONITORING_EVENT,
            DSP_NEW_STUDY_DIAGNOSTIC_EVENT,
            DSP_START_RECORD_EVENT,
            DSP_FREEZE_EVENT,
            DSP_UNFREEZE_EVENT,
            DSP_HW_CONNECT_EVENT,
            DSP_HW_DISCONNECT_EVENT,
            DSP_FINISH_STUDY_SAVE_STORAGE_EVENT,
            DSP_FINISH_STUDY_REMOVE_STORAGE_EVENT,
            DSP_NEW_REPLAY_MONITORING_EVENT,
            DSP_NEW_REPLAY_DIAGNOSTIC_EVENT,
            DSP_STOP_REPLAY_EVENT,
            DSP_PAUSE_REPLAY_EVENT,
            DSP_RESUME_REPLAY_EVENT,
            DSP_REPLAY_JUMP_TO_EVENT,
            DSP_FPGA_LOAD_START_EVENT,
            DSP_FPGA_LOAD_FINISH_EVENT,
            //DSP_PIC_MANUAL_CALC_EVENT,
            DSP_SCROLL_EVENT,
            DSP_ADD_HISTORY_EVENT
        }

        public enum EDspState
        {
            DSP_HW_INIT_STATE,
            DSP_OFFLINE_STATE,
            DSP_ONLINE_STATE,
            DSP_ONLINE_FREEZE_STATE,
            DSP_REPLAY_STATE,
            DSP_REPLAY_PAUSE_STATE,
            DSP_REPLAY_DRAW_EVENT_STATE,
            DSP_FPGA_LOAD_STATE,
            DSP_UNKNOWN,
			DSP_RUNNING,
			DSP_STOPED
        }

        private readonly RCardInfo m_cardInfo;
		private readonly RPc2Dsp m_pc2Dsp;

        private readonly PendingEventsQueue m_smPendingEvents;

		private bool m_isRunning;
		private CCommLayerBoundary m_commLayerBoundary;			//TODO [Ofer]: Add lock and check for access / use m_commLayerBoundary
        private DspToPcContainer m_dspToPcContainer;
		private System.Threading.Timer m_dspDataPublisherTimer;
		private Mutex m_pc2DspMutex;
        private CRawDataFileMgr m_rawDataFileMgr;
		private CDspMgrStudyParams m_studyParams;
        private int m_drawLastBufferIndex;
		private int m_studyAbsoluteLastBufferIndex;
		private int m_eventBufferStartPoint;
		private CDspMgrReplayJumpToEventData m_replayJumpToEventData;
	    private bool m_isDSPConnectedOnce = false;

		private DateTime m_nextTransferPc2Dsp = DateTime.UtcNow;
		private DateTime m_checkConnection = DateTime.UtcNow;

        private BmpRawData m_rawDataForDiagnosticStudyScroll;

		private readonly Thread m_dspRxPullThread;
		private readonly Queue<RDspToPc> m_dspToPcQeueue = new Queue<RDspToPc>();
		private bool m_disconnectedLogged = false;
		#endregion Private Fields


        #region Private Methods

	    private DateTime m_lastDspFramTx = DateTime.UtcNow;

		private void dspMgrTransferPc2DspWait(int mutexTimeout = TIMER_TIMEOUT_MSEC)
		{
			var isNewData = 0;

			if (!m_isRunning)
				return;

			if (!m_pc2DspMutex.WaitOne(mutexTimeout))
            {
                Logger.LogInfo("DSP Transfer PC-2-DSP wait Timeout ({0}mSec)", mutexTimeout);
                return;
            }
			
            try
            {
				foreach (var probeInfo in ProbesCollection.Instance.ProbsInfo)
		        {
		            if (m_pc2Dsp.MergeRawData(probeInfo.ListNewFieldVals))
		            {
		                isNewData++;
                        probeInfo.ListNewFieldVals.Clear(); 
		            }
		        }

				if (!m_isRunning)
                    return;

				if (DateTime.UtcNow.Subtract(m_lastDspFramTx).TotalMilliseconds > DSP_TX_ALIVE_INTERVAL_MS)
					m_cardInfo.AddDemiAliveMessage();

				var bRet = m_pc2Dsp.MergeRawData(m_cardInfo.ListNewFieldVals);
		        if (bRet)
		        {
		            isNewData++;
                    m_cardInfo.ListNewFieldVals.Clear();    
		        }

				if (isNewData > 0 && m_isRunning)
                {
                    m_pc2Dsp.Numerator++;

					if (m_pc2Dsp.RawData != null && m_pc2Dsp.RawData.Length > 0)
					{
						var buffer = m_pc2Dsp.RawData;
						if (AppSettings.Active.DSPCopyTxMessageBuffer)				//TODO [Ofer]: check if required
						{
							buffer = new byte[m_pc2Dsp.RawData.Length];
							Buffer.BlockCopy(m_pc2Dsp.RawData, 0, buffer, 0, buffer.Length);
						}

						m_commLayerBoundary.ComLayerBoundaryFrameTx(buffer, eDspMsgType.PC2DSP_MSG_TYPE);
						m_lastDspFramTx = DateTime.UtcNow;
					}
                }
            }
		    catch (Exception ex)
		    {
                Logger.LogError(ex);
		    }
		    finally
		    {
                m_pc2DspMutex.ReleaseMutex();
            }
		}

		private void timerOn(int startDelay = 0, int period = TIMER_TIMEOUT_MSEC)
        {
			if (m_dspDataPublisherTimer != null && m_isRunning)
                m_dspDataPublisherTimer.Change(startDelay, period);
        }

        private void timerOff()
        {
            if (m_dspDataPublisherTimer != null)
                m_dspDataPublisherTimer.Change(Timeout.Infinite, Timeout.Infinite);
        }

		private BmpRawData createBmpRawData()
		{
			var rawData								= new BmpRawData();
			rawData.LastDrawnIndex					= m_drawLastBufferIndex;
			rawData.EventStartPointAbsoluteIndex	= m_eventBufferStartPoint;
				
            if (rawData.EventStartPointAbsoluteIndex > 0 && m_replayJumpToEventData != null)
				rawData.EventReplayPointAbsoluteIndex = (m_replayJumpToEventData.FileIndex * RawDataFile.FILE_MAX_DSP_MESSAGES) + m_replayJumpToEventData.EventBufferStartPoint + m_replayCounter++; //rawData.EventReplayPointAbsoluteIndex = m_replayJumpToEventData.EventStartPointAbsoluteIndex + (m_replayJumpToEventData.FileIndex * ((1000 * MONITORING_FILE_TIME_INTERVAL_SEC) / DSP_INT_CYCLE_MILISEC));
            else
	            m_replayCounter = 0;

			//rawData.DSPIntCycleSec		= RawDataFile.DSP_MESSAGE_DURATION_SEC;
			rawData.DspToPcContainer	= m_dspToPcContainer;
			rawData.LastAbsoluteIndex	= m_studyAbsoluteLastBufferIndex;

			//System.Diagnostics.Debug.WriteLine(string.Format("m_replayCounter={0}, m_drawLastBufferIndex={1}, rawData.EventReplayPointAbsoluteIndex={2}", m_replayCounter, m_drawLastBufferIndex, rawData.EventReplayPointAbsoluteIndex));
            return rawData;
		}

		private void initCommLayer()
        {
			Logger.LogWarning("Initiating communication layer...");

			DSPVersion	= "Offline";
			FPGAVersion	= string.Empty;
            HwConnected	= false;
            DspState	= EDspState.DSP_HW_INIT_STATE;

			if (AppSettings.Active.IsDspOffMode)
                m_commLayerBoundary = new CCommLayerBoundary(InfraBoundaryWrapper.Wrapper.InfraBoundary, InfraBoundaryWrapper.Wrapper.DebugBoundar, null);
            else
                m_commLayerBoundary = new CCommLayerBoundary(InfraBoundaryWrapper.Wrapper.InfraBoundary, InfraBoundaryWrapper.Wrapper.DebugBoundar);
        }

		private bool getHWInfo()
		{
			HwVersion = @"Offline";

			UInt32 fpgaSubVersion = 0;
			UInt32 fpgaMainVersion = 0;
			UInt32 dspVersionNum = 0;
			UInt32 dspVersionFormat = 0;
			//Logger.DebugStringOut("getHWInfo(): m_commLayerBoundary.GetHwInfo()");
			if (!m_commLayerBoundary.GetHwInfo(ref fpgaSubVersion, ref fpgaMainVersion, ref dspVersionNum, ref dspVersionFormat))
				return false;

			var version = new StringBuilder();
			if ((int)dspVersionNum == -1 && dspVersionFormat == 10)
			{
				version.Append("\n  DSP:Offline\n  HW:Offline ");
			}
			else
			{
				if (dspVersionFormat == 1)
				{
					var dsp1 = (int)(dspVersionNum / 1000);
					dspVersionNum = dspVersionNum % 1000;
					var dsp2 = (int)(dspVersionNum / 100);
					var dsp3 = (int)(dspVersionNum % 100);

					DSPVersion = string.Format("{0}.{1}.{2}", dsp1, dsp2, dsp3);
					version.Append("\n  DSP: " + DSPVersion);
				}
				else if (dspVersionFormat == 2)
				{
					var dsp1 = (int)(dspVersionNum / 1000000);
					dspVersionNum = dspVersionNum % 1000000;
					var dsp2 = (int)(dspVersionNum / 10000);
					dspVersionNum = dspVersionNum % 10000;
					var dsp3 = (int)(dspVersionNum / 100);
					var dsp4 = (int)(dspVersionNum % 100);

					DSPVersion = string.Format("{0}.{1}.{2}.{3}", dsp1, dsp2, dsp3, dsp4);
					version.Append("\n  DSP: " + DSPVersion);
				}

				FPGAVersion = string.Format("{0}.{1}", fpgaMainVersion, fpgaSubVersion);
				version.Append("\n  HW: " + FPGAVersion);
			}

			HwVersion = version.ToString();

			setSmState(EDspState.DSP_OFFLINE_STATE);
			dspMgrTransferPc2DspWait();

			return true;
		}

        private bool startWait(int timeoutSec = 30)
        {
            var initTimeout = DateTime.UtcNow.AddSeconds(timeoutSec);
            
            while (!HwConnected)
            {
				testDspConnection();

				if (initTimeout <= DateTime.UtcNow)
                {
					Logger.LogError(string.Format("DSP Manager start timeout ({0} sec.). DSP manager start fail.", timeoutSec));
                    return false;
                }

                Thread.Sleep(500);
            }

            DspState = EDspState.DSP_OFFLINE_STATE;
            Logger.LogInfo("DSP Manager started. DSP version: v{0}, FPGA version: v{1}.", DSPVersion, FPGAVersion);

            return true;
        }


        //TODO, Ofer: [Performance] use object pool for DSP2PC buffer. Allocate buffer from object pool instead of in ComLayerBoundaryFrameRx(...) method
        private void processDspPendingMessage(bool isSaveHit) 
        {
			if (!m_isRunning || m_dspToPcContainer == null)
				return;

			if (m_dspToPcQeueue.Count == 0)
				return;

			RDspToPc dspToPc;
	        lock (m_dspToPcQeueue)
		       dspToPc = m_dspToPcQeueue.Dequeue();

	        if (dspToPc	 == null || !dspToPc.IsValid)
				return;

			var bRet = m_dspToPcContainer.Add(dspToPc);
			if (!bRet)
				return;

			var currBufferIndex = m_studyAbsoluteLastBufferIndex + (m_dspToPcContainer.Count - 1 - m_drawLastBufferIndex);
			MainForm.Instance.HitsHistogram.ProcessDspData(dspToPc, currBufferIndex, isSaveHit); 

			if (m_studyParams.StudyType == EStudyLayoutType.Monitoring)
            {
				if (m_dspToPcContainer.Count < 1)
	                return;

				m_dspToPcContainer.WriteRawDataBlockToStream(m_dspToPcContainer.Count - 1);	//TODO [Ofer]: check if method can accept dspToPc instead of container index.
            }

			if (m_dspToPcContainer.Count < m_dspToPcContainer.Capacity)	
				return;

			processDspPendingMessageFireDspNewData();

			var tmprawDataFileMgr = m_rawDataFileMgr;
			if (tmprawDataFileMgr == null)
			{
				Logger.LogWarning("{0}.processDspPendingMessage(): m_rawDataFileMgr is NULL.", CLASS_NAME);
				return;
			}

			var capacity = m_dspToPcContainer.Capacity;
			if (m_studyParams.StudyType == EStudyLayoutType.Diagnostic)
				tmprawDataFileMgr.RawDataWrite(m_dspToPcContainer); // save ref. save data to disk (a-sync)

			m_dspToPcContainer			= tmprawDataFileMgr.RawDataAlloc(capacity); // get new initialized object 
            m_drawLastBufferIndex		= 0;
            m_eventBufferStartPoint		= 0;
            m_studyAbsoluteLastBufferIndex++;
		}

		private void processDspPendingMessageFireDspNewData()
		{
			if (!m_isRunning || m_dspToPcContainer == null)
				return;

			if (m_dspToPcContainer.Count <= m_eventBufferStartPoint + 1)
				return;

			m_studyAbsoluteLastBufferIndex += (m_dspToPcContainer.Count - 1 - m_drawLastBufferIndex);
			m_drawLastBufferIndex = m_dspToPcContainer.Count - 1;

			var bmpRawData = createBmpRawData();
			LayoutManager.Instance.FireDspNewData(this, bmpRawData, m_studyParams.StudyId, false);
		}

		private void processDspPendingMessages() 
		{
			if (!m_isRunning || m_dspToPcContainer == null)
				return;

			var isSaveHits = !(LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.MonitoringLoad && MainForm.Instance.CurrentStudyName.StartsWith(Constants.StudyType.STUDY_MONITORING_INTRACRANIAL));

			while (m_dspToPcQeueue.Count > 0)
			{
				processDspPendingMessage(isSaveHits);
			}

			processDspPendingMessageFireDspNewData();
		}


        #region State Machine
        private long m_replayMilisecStart;
		private long m_replayMilisecLast;
	    private int m_replayCounter = 0;
		private void dspMgrReplayTransferSmAction()
		{
			var isLast = false;
			if ((m_dspToPcContainer == null) || (m_drawLastBufferIndex>=m_dspToPcContainer.Count)) 
            {
				if (m_replayJumpToEventData != null && m_replayJumpToEventData.EventDspToPcContainer != null) 
                {
					m_dspToPcContainer = m_replayJumpToEventData.EventDspToPcContainer;
					m_replayJumpToEventData.EventDspToPcContainer = null;
				}
				else 
				{
					var tmprawDataFileMgr = m_rawDataFileMgr;
					if (tmprawDataFileMgr != null)
						m_dspToPcContainer = tmprawDataFileMgr.RawDataReadNext(ref isLast);
				}

				m_drawLastBufferIndex = 0;
				if (m_replayMilisecLast == -1)
					m_replayMilisecStart = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
				else
					m_replayMilisecStart = m_replayMilisecLast;
			}

			
			if (m_dspToPcContainer != null) 
			{
				if (m_replayMilisecLast == -1)
					m_replayMilisecStart = (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond) - (long)Math.Round(m_drawLastBufferIndex * RawDataFile.DSP_MESSAGE_DURATION_MSEC);	

                m_replayMilisecLast = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
				var numOfBuffs = (int)Math.Round((m_replayMilisecLast - m_replayMilisecStart) / RawDataFile.DSP_MESSAGE_DURATION_MSEC);			// = (m_replayMilisecLast - m_replayMilisecStart) / DSP_INT_CYCLE_MILISEC;
				if (numOfBuffs > m_drawLastBufferIndex + 2) 
                {
					numOfBuffs = m_drawLastBufferIndex + 2;
					m_replayMilisecStart = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond - (m_replayMilisecLast - m_replayMilisecStart);	// = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond - numOfBuffs * DSP_INT_CYCLE_MILISEC;
				}

				if (m_dspToPcContainer != null && m_dspToPcContainer.RawDataFileNum == 0 && m_drawLastBufferIndex==0) 
                {
					foreach (TrendChart tc in LayoutManager.Instance.Trends) 
                    {
						tc.Graph.ReplayReset();
					}
				}

				var isOfflineData	= (m_replayJumpToEventData != null && m_replayJumpToEventData.DrawColCount < m_replayJumpToEventData.NumOfBuffersToDraw);
				var action			= DspEndDrawHistoryEvent;
				while (m_isRunning && m_drawLastBufferIndex < numOfBuffs && m_drawLastBufferIndex < m_dspToPcContainer.Count) //TODO: enable play of next container
                {
					var dspToPc = m_dspToPcContainer[m_drawLastBufferIndex];
					AudioDSPtoPCStreamPlayer.PostPlay(dspToPc);						//Ofer, Enable sound on PC

					var rawData = createBmpRawData();
                    LayoutManager.Instance.FireDspNewData(this, rawData, m_studyParams.StudyId, isOfflineData);

                    m_drawLastBufferIndex++;
					m_studyAbsoluteLastBufferIndex++;

					if (AppSettings.Active.PlaybackSendAudioDataToDSP)
					{
						// Send I and Q data to thr DSP for audio play back.
						var buffer = dspToPc.GetReplayTimeDomainGateCopy(m_cardInfo.Gate2Hear);   //Get played gate timeDomain I,Q array

						m_commLayerBoundary.ComLayerBoundaryFrameTx(buffer, eDspMsgType.REPLAY_DSP2PC_TYPE);
					}

					if (m_replayJumpToEventData != null) 
                    {
						m_replayJumpToEventData.DrawColCount++;
						if (m_replayJumpToEventData.DrawColCount >= m_replayJumpToEventData.NumOfBuffersToDraw) 
                        {
							setSmState(EDspState.DSP_REPLAY_PAUSE_STATE);
							m_eventBufferStartPoint = 0;	
							m_replayJumpToEventData = null;

							if (action != null)
								action();
								
                            break;
						}
					}
				}
			}
			else if(isLast) 
            {
                setSmState(EDspState.DSP_REPLAY_PAUSE_STATE);

	            var action = DspEndPlayback;
				if (action != null)
					action();

				var tmprawDataFileMgr = m_rawDataFileMgr;
				if (tmprawDataFileMgr != null)
					tmprawDataFileMgr.RawDataReadRestart();

				m_dspToPcContainer = null;
				m_drawLastBufferIndex = 0;
				m_studyAbsoluteLastBufferIndex = 0;
				m_eventBufferStartPoint = 0;
				m_replayMilisecLast = -1;
			}
		}

		private void setSmState(EDspState value)
		{
			Logger.LogDebug("DspManager.setSmState({0})", value);
			DspState = value;
		}

		private void dspMgrOfflineToOnlineFreezeStateSmAction(object[] smEventParams)
		{
            setSmState(EDspState.DSP_ONLINE_FREEZE_STATE);
			m_drawLastBufferIndex = 0;
			m_eventBufferStartPoint = 0;
			m_studyAbsoluteLastBufferIndex = 0;

			var tmprawDataFileMgr = m_rawDataFileMgr;
			if (tmprawDataFileMgr != null)
			{
				m_dspToPcContainer = tmprawDataFileMgr.RawDataAlloc(RawDataFile.FILE_MAX_DSP_MESSAGES); 
				
				if (smEventParams != null)
				{
					var dirName = smEventParams[0].ToString();
					tmprawDataFileMgr.RawDataRecordStart(dirName);
				}
			}

			DspMgrLoadBasicConfig();
		}

		private eRmdErrorCode dspMgrHwInitStateSmGo(CSmEvent<EDspSmEvent> smEvent)
		{
			switch (smEvent.SmEventNum)
			{
				case EDspSmEvent.DSP_HW_CONNECT_EVENT:
			            getHWInfo();
					break;
				case EDspSmEvent.DSP_HW_DISCONNECT_EVENT:
					break;

				default:
					Logger.LogWarning("{0}.dspMgrHwInitStateSmGo(SmEvent={1}) - WRONG event for {2} state.", CLASS_NAME, smEvent.SmEventNum, DspState);
					return eRmdErrorCode.eRMD_SM_WRONG_EVENT4STATE;
			}

			return eRmdErrorCode.eRMD_SUCCESS;
		}

		private void dspMgrReplayStartSmAction(object[] paramsArr)
		{
            m_studyParams = (CDspMgrStudyParams)paramsArr[0];
			m_drawLastBufferIndex = 0;
			m_eventBufferStartPoint = 0;
			m_studyAbsoluteLastBufferIndex = 0;
			var tmprawDataFileMgr = m_rawDataFileMgr;
			if (tmprawDataFileMgr != null)
				tmprawDataFileMgr.RawDataReadStart(m_studyParams.DirName, 0);
			setSmState(EDspState.DSP_REPLAY_PAUSE_STATE);
			m_dspToPcContainer = null;
			m_replayMilisecLast = -1;
		}

        private void dspDisconnect()
		{
			if (!m_isRunning)
                return;

			if (!m_isDSPConnectedOnce)
			{
				Logger.LogWarning("NOTE: DSP never connected. DspDisconnect message received.");
				return;
			}

	        stop();

			var res = LoggedDialog.Show(MainForm.Instance, "DSP Disconnected, reconnect fail.\n"+ AppSettings.Active.ApplicationProductName +" application / device require a restart.\n\nClick <Yes> to exit the application.\nClick  <No>: for a SYSTEM restart.", "DSP ERROR", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
			if (res == DialogResult.Yes)
				MainForm.Instance.ExitDigiLite();
			else
			{
				res = LoggedDialog.Show(MainForm.Instance, "Please confirm windows SYSTEM restart.\n\nClick <Yes>: for a SYSTEM restart.\nClick <No> to exit the application.", "DSP ERROR", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
				if (res == DialogResult.No)
					MainForm.Instance.ExitDigiLite();
				else
					MainForm.Instance.ExitWindow();
			}
        }

 
        private eRmdErrorCode dspMgrOfflineStateSmGo(CSmEvent<EDspSmEvent> smEvent)
		{
			var errCode = eRmdErrorCode.eRMD_SUCCESS;   //default return value;

			var tmprawDataFileMgr = m_rawDataFileMgr;
			switch (smEvent.SmEventNum)
			{
				case EDspSmEvent.DSP_HW_DISCONNECT_EVENT:
					dspDisconnect();
					break;

				case EDspSmEvent.DSP_NEW_STUDY_MONITORING_EVENT:
						m_rawDataForDiagnosticStudyScroll = null;
						m_studyParams = (CDspMgrStudyParams)smEvent.SmEventParamsArr[0];
						//tmprawDataFileMgr.TryDispose();
						m_rawDataFileMgr = new CMonitoringFileMgr();
						dspMgrOfflineToOnlineFreezeStateSmAction(smEvent.SmEventParamsArr);
					break;
				case EDspSmEvent.DSP_NEW_STUDY_DIAGNOSTIC_EVENT:
						m_rawDataForDiagnosticStudyScroll = null;
						//tmprawDataFileMgr.TryDispose();
						m_rawDataFileMgr = new CDiagnosticFileMgr();
						dspMgrOfflineToOnlineFreezeStateSmAction(smEvent.SmEventParamsArr);
						m_studyParams = (CDspMgrStudyParams)smEvent.SmEventParamsArr[0];
					break;
				case EDspSmEvent.DSP_HW_CONNECT_EVENT:
					break;
				case EDspSmEvent.DSP_NEW_REPLAY_MONITORING_EVENT:
						m_rawDataForDiagnosticStudyScroll = null;
						//tmprawDataFileMgr.TryDispose();
						m_rawDataFileMgr = new CMonitoringFileMgr();
						dspMgrReplayStartSmAction(smEvent.SmEventParamsArr);
					break;
				case EDspSmEvent.DSP_NEW_REPLAY_DIAGNOSTIC_EVENT:
						m_rawDataForDiagnosticStudyScroll = null;
						//tmprawDataFileMgr.TryDispose();
						m_rawDataFileMgr = new CDiagnosticFileMgr();
						dspMgrReplayStartSmAction(smEvent.SmEventParamsArr);
					break;

				case EDspSmEvent.DSP_FPGA_LOAD_START_EVENT:
				case EDspSmEvent.DSP_FPGA_LOAD_FINISH_EVENT:
					break;
				case EDspSmEvent.DSP_REPLAY_JUMP_TO_EVENT:
					break;
				case EDspSmEvent.DSP_SCROLL_EVENT:
					if (m_rawDataForDiagnosticStudyScroll != null)
					{
						try 
                        {
                            var scrollParams = smEvent.SmEventParamsArr[0] as CDspMgrScrollParams;
                            if (scrollParams != null)
								m_rawDataForDiagnosticStudyScroll.LastDrawnIndex = LayoutManager.Instance.Scroll(scrollParams.IsScrollBack, scrollParams.NumberOfStepsToMove, m_rawDataForDiagnosticStudyScroll.DspToPcContainer, m_rawDataForDiagnosticStudyScroll.EventStartPointAbsoluteIndex, m_rawDataForDiagnosticStudyScroll.LastDrawnIndex);
                        }
						catch (Exception ex) 
                        {
							Logger.LogError(ex);
                        }
					}
				    break;
				case EDspSmEvent.DSP_ADD_HISTORY_EVENT:
					LoggedDialog.ShowNotification(MainForm.Instance, MainForm.StringManager.GetString("Adding events is enabled just in record mode please press the record button to use this feature"), MainForm.StringManager.GetString("Adding Events"));
					break;
				default:
						Logger.LogWarning("{0}.dspMgrOfflineStateSmGo(SmEvent={1}) - WRONG event for {2} state.", CLASS_NAME, smEvent.SmEventNum, DspState);
						errCode = eRmdErrorCode.eRMD_SM_WRONG_EVENT4STATE;
					break;
			}

			return errCode;
		}

		private eRmdErrorCode dspMgrOnlineStateSmGo(CSmEvent<EDspSmEvent> smEvent)
		{
			var errCode = eRmdErrorCode.eRMD_SUCCESS;   //default return value;
			switch (smEvent.SmEventNum) 
            {
				case EDspSmEvent.DSP_HW_DISCONNECT_EVENT:
						dspDisconnect();
					break;

				case EDspSmEvent.DSP_HW_CONNECT_EVENT:      //created internally - received every 24msec
						processDspPendingMessages();
                    break;
				
				case EDspSmEvent.DSP_FREEZE_EVENT:
						m_cardInfo.OperationMode = (ushort)GlobalTypes.ESystemModes.Offline;
						DspMgrLoadBasicConfig();
						dspMgrTransferPc2DspWait(); // Send immediately to DSP -  fix important for single core arch
						setSmState(EDspState.DSP_ONLINE_FREEZE_STATE);
						m_drawLastBufferIndex = m_dspToPcContainer.Count - 1;
						LayoutManager.Instance.FireChangeModeGraphEvent(createBmpRawData());// New developer, redesign LayoutManager !!!
					break;

				case EDspSmEvent.DSP_ADD_HISTORY_EVENT:
						dspMgrSmAddHistoryEvent(smEvent.SmEventParamsArr);
					break;

				case EDspSmEvent.DSP_FINISH_STUDY_SAVE_STORAGE_EVENT:
				case EDspSmEvent.DSP_FINISH_STUDY_REMOVE_STORAGE_EVENT:
				case EDspSmEvent.DSP_START_RECORD_EVENT:
						errCode = dspMgrOnlineFreezeStateSmGo(smEvent);
					break;

				default:
						Logger.LogWarning("{0}.dspMgrOnlineStateSmGo(SmEvent={1}) - WRONG Event for {2} state.", CLASS_NAME, smEvent.SmEventNum, DspState);
						errCode = eRmdErrorCode.eRMD_SM_WRONG_EVENT4STATE;
					break;
			}

			return errCode;
		}

		private void dspMgrSmScrollEvent(object[] paramsArr)
		{
			try 
            {
                var scrollParams = paramsArr[0] as CDspMgrScrollParams;
                if (scrollParams != null)
					m_drawLastBufferIndex = LayoutManager.Instance.Scroll(scrollParams.IsScrollBack, scrollParams.NumberOfStepsToMove, m_dspToPcContainer, m_eventBufferStartPoint, m_drawLastBufferIndex);
            }
			catch (Exception ex) 
            {
				Logger.LogError(ex);
            }
		}

		private void dspMgrSmAddHistoryEvent(object[] paramsArr)
		{
            if (m_dspToPcContainer == null)
                return;

			if (m_dspToPcContainer.RawDataFileNum < 0 || m_dspToPcContainer.RawDataFileName == null)
				return;
                
            var eventName	= paramsArr[0].ToString();
			var eventType	= (GlobalTypes.EEventType)paramsArr[1];
			var postEvent	= Convert.ToBoolean(paramsArr[2]);
			var msecOffset	= Convert.ToDouble(paramsArr[3]);
			var side		= Convert.ToInt32(paramsArr[4]);

			EventListViewCtrl.Instance.AddHistoryEvent(eventName, eventType, side, postEvent, m_drawLastBufferIndex, m_dspToPcContainer.RawDataFileNum, msecOffset);
		}

		private eRmdErrorCode dspMgrOnlineFreezeStateSmGo(CSmEvent<EDspSmEvent> smEvent)
		{
			var errCode = eRmdErrorCode.eRMD_SUCCESS;   //default return value;
			var tmprawDataFileMgr = m_rawDataFileMgr;
			switch (smEvent.SmEventNum)
			{
				case EDspSmEvent.DSP_HW_DISCONNECT_EVENT:
					dspDisconnect();
					break;

				case EDspSmEvent.DSP_HW_CONNECT_EVENT:
					break;

				case EDspSmEvent.DSP_UNFREEZE_EVENT:
					m_cardInfo.OperationMode = (ushort)GlobalTypes.ESystemModes.Online;
					DspMgrLoadBasicConfig();
					setSmState(EDspState.DSP_ONLINE_STATE);

					LayoutManager.Instance.FireChangeModeGraphEvent(createBmpRawData());// New developer, redesign LayoutManager !!!

					break;
				case EDspSmEvent.DSP_START_RECORD_EVENT:
					if (smEvent.SmEventParamsArr != null && tmprawDataFileMgr != null) 
                    {
                        var dirName = smEvent.SmEventParamsArr[0].ToString();
						tmprawDataFileMgr.RawDataRecordStart(dirName);
						if (m_studyParams.StudyType == EStudyLayoutType.Monitoring) 
                        {
							m_dspToPcContainer = tmprawDataFileMgr.RawDataAlloc(RawDataFile.FILE_MAX_DSP_MESSAGES);
							m_drawLastBufferIndex = 0;
							m_eventBufferStartPoint = 0;
						}
					}
					break;
					
				case EDspSmEvent.DSP_FINISH_STUDY_REMOVE_STORAGE_EVENT:
						m_cardInfo.OperationMode = (ushort)GlobalTypes.ESystemModes.Offline;
						DspMgrLoadBasicConfig();
						dspMgrTransferPc2DspWait(); // Send immediately to DSP -  fix important for single core arch

						try
						{
							if (tmprawDataFileMgr != null)
								tmprawDataFileMgr.RawDataRecordCancel();
						}
						finally
						{
							//tmprawDataFileMgr.TryDispose();
							m_rawDataFileMgr = null;
							m_dspToPcContainer = null;
							setSmState(EDspState.DSP_OFFLINE_STATE);
						}
					break;

				case EDspSmEvent.DSP_FINISH_STUDY_SAVE_STORAGE_EVENT:
                        m_cardInfo.OperationMode = (ushort)GlobalTypes.ESystemModes.Offline;
				        DspMgrLoadBasicConfig();
				        dspMgrTransferPc2DspWait(); // Send immediately to DSP -  fix important for single core arch

						try
						{
							if (tmprawDataFileMgr != null)
							{
								if (m_dspToPcContainer != null && m_dspToPcContainer.Count > 0)
									tmprawDataFileMgr.RawDataWrite(m_dspToPcContainer);

								tmprawDataFileMgr.RawDataRecordFinish();
							}
						}
						finally
						{
							m_rawDataForDiagnosticStudyScroll = createBmpRawData();
							m_dspToPcContainer = null;

							setSmState(EDspState.DSP_OFFLINE_STATE);
						}
				    break;
				case EDspSmEvent.DSP_SCROLL_EVENT:
						dspMgrSmScrollEvent(smEvent.SmEventParamsArr);
					break;

				case EDspSmEvent.DSP_ADD_HISTORY_EVENT:
						dspMgrSmAddHistoryEvent(smEvent.SmEventParamsArr);
					break;
				default:
						Logger.LogWarning("{0}.dspMgrOnlineFreezeStateSmGo(SmEvent={1}) - WRONG Event for {2} state.", CLASS_NAME, smEvent.SmEventNum, DspState);
						errCode = eRmdErrorCode.eRMD_SM_WRONG_EVENT4STATE;
					break;

			}

			return errCode;
		}

		private eRmdErrorCode dspMgrReplayStateSmGo(CSmEvent<EDspSmEvent> smEvent)
		{
			var errCode = eRmdErrorCode.eRMD_SUCCESS;   //default return value;
			switch (smEvent.SmEventNum) 
            {
				case EDspSmEvent.DSP_HW_DISCONNECT_EVENT:
					dspDisconnect();
					break;

				case EDspSmEvent.DSP_HW_CONNECT_EVENT:
					//TODO check block list size and load from file if needed
					dspMgrReplayTransferSmAction();
					break;
					
				case EDspSmEvent.DSP_STOP_REPLAY_EVENT:
					m_cardInfo.OperationMode = (ushort)GlobalTypes.ESystemModes.Offline;
					DspMgrLoadBasicConfig();
					dspMgrTransferPc2DspWait();             // Send immediately to DSP -  fix important for single core arch
					errCode = dspMgrReplayPauseStateSmGo(smEvent);
					break;

				case EDspSmEvent.DSP_PAUSE_REPLAY_EVENT:
					m_cardInfo.OperationMode = (ushort)GlobalTypes.ESystemModes.Offline;
					DspMgrLoadBasicConfig();
					dspMgrTransferPc2DspWait(); // Send immediately to DSP -  fix important for single core arch
					setSmState(EDspState.DSP_REPLAY_PAUSE_STATE);

					LayoutManager.Instance.FireChangeModeGraphEvent(createBmpRawData());    // New developer, redesign LayoutManager !!!

					break;

				case EDspSmEvent.DSP_ADD_HISTORY_EVENT:
					dspMgrSmAddHistoryEvent(smEvent.SmEventParamsArr);
					break;

				default:
						Logger.LogWarning("{0}.dspMgrReplayStateSmGo(SmEvent={1}) - WRONG event for {2} state.", CLASS_NAME, smEvent.SmEventNum, DspState);
						errCode =	eRmdErrorCode.eRMD_SM_WRONG_EVENT4STATE;
		            break;
            }

			return errCode;
		}

		private void dspMgrSmReplayDrawEventAction()
		{
			var readFailed = false;
			BmpRawData bmpRawData;
			if (m_replayJumpToEventData.DrawColCount > 0) 
            {
				m_drawLastBufferIndex = m_replayJumpToEventData.NumOfBuffersToDraw - m_replayJumpToEventData.DrawColCount;
				if (m_drawLastBufferIndex >= m_dspToPcContainer.Count)
					m_drawLastBufferIndex = m_dspToPcContainer.Count - 1;

				m_studyAbsoluteLastBufferIndex += m_drawLastBufferIndex;
				bmpRawData = createBmpRawData();
                LayoutManager.Instance.FireDspNewData(this, bmpRawData, m_studyParams.StudyId, true);
				m_replayJumpToEventData.DrawColCount += m_drawLastBufferIndex;
			}
			else 
            {
				m_replayCounter = 0;
				m_eventBufferStartPoint = m_replayJumpToEventData.EventBufferStartPoint;
				m_drawLastBufferIndex = m_eventBufferStartPoint + m_replayJumpToEventData.NumOfBuffersToDraw;

				if (m_drawLastBufferIndex >= m_dspToPcContainer.Count)
					m_drawLastBufferIndex = m_dspToPcContainer.Count - 1;

				m_studyAbsoluteLastBufferIndex = m_drawLastBufferIndex + m_dspToPcContainer.RawDataFileNum * RawDataFile.FILE_MAX_DSP_MESSAGES;
				m_replayJumpToEventData.EventDspToPcContainer = m_dspToPcContainer;
				bmpRawData = createBmpRawData();

                LayoutManager.Instance.FireDspNewData(this, bmpRawData, m_studyParams.StudyId, true);
                m_replayJumpToEventData.DrawColCount = m_drawLastBufferIndex - m_eventBufferStartPoint;
			}

			var tmprawDataFileMgr = m_rawDataFileMgr;
			if (m_replayJumpToEventData.DrawColCount < m_replayJumpToEventData.NumOfBuffersToDraw && tmprawDataFileMgr != null)
				m_dspToPcContainer = tmprawDataFileMgr.RawDataReadNext(ref readFailed);

			if (m_replayJumpToEventData.DrawColCount >= m_replayJumpToEventData.NumOfBuffersToDraw || readFailed) 
            {
				m_drawLastBufferIndex = m_replayJumpToEventData.EventBufferStartPoint;

				m_studyAbsoluteLastBufferIndex = m_drawLastBufferIndex + (m_replayJumpToEventData.FileIndex * RawDataFile.FILE_MAX_DSP_MESSAGES);
				if (m_dspToPcContainer != null && m_dspToPcContainer.RawDataFileNum != m_replayJumpToEventData.FileIndex) 
                {
					var tmp = m_dspToPcContainer;
					m_dspToPcContainer = m_replayJumpToEventData.EventDspToPcContainer;
					m_replayJumpToEventData.EventDspToPcContainer = tmp;
				}
				else
					m_replayJumpToEventData.EventDspToPcContainer = null;

				setSmState(EDspState.DSP_REPLAY_PAUSE_STATE);
				m_replayJumpToEventData.DrawColCount = 0;
			}
		}
			
		private void dspMgrReplayJumpToEventSmAction(object[] paramsArr)
		{
			var tmprawDataFileMgr = m_rawDataFileMgr;
			if (tmprawDataFileMgr == null)
				return;

			var replayJumpToEventData = paramsArr[0] as CDspMgrReplayJumpToEventData;
			if (replayJumpToEventData == null)
				return;

			Logger.LogDebug("Replay: Jump to event {0}.", replayJumpToEventData);
			m_dspToPcContainer			= tmprawDataFileMgr.RawDataReadJumpToFile(replayJumpToEventData.FileIndex);
			m_replayJumpToEventData		= replayJumpToEventData;

			if (m_dspToPcContainer == null)
			{
				Logger.LogWarning(@"Replay: Jump to event fail. Raw data file {0}\{1}{2} load fail.", tmprawDataFileMgr.DirName, replayJumpToEventData.FileIndex, RawDataFile.DATA_FILES_EXTENTION);
				setSmState(EDspState.DSP_REPLAY_DRAW_EVENT_STATE);
			}
			else 
				dspMgrSmReplayDrawEventAction();
		}

		private eRmdErrorCode dspMgrReplayDrawEventStateSmGo(CSmEvent<EDspSmEvent> smEvent)
		{
			var errCode = eRmdErrorCode.eRMD_SUCCESS;   //default return value;
			var tmprawDataFileMgr = m_rawDataFileMgr;
			var isLast = false;
			switch (smEvent.SmEventNum) 
            {
				case EDspSmEvent.DSP_HW_DISCONNECT_EVENT:
					dspDisconnect();
					break;

				case EDspSmEvent.DSP_HW_CONNECT_EVENT:
					if (tmprawDataFileMgr != null)
					{
						m_dspToPcContainer = tmprawDataFileMgr.RawDataReadNext(ref isLast);
						if (m_dspToPcContainer == null)
						{
							if (isLast)
								setSmState(EDspState.DSP_REPLAY_PAUSE_STATE);
						}
						else
						{
							dspMgrSmReplayDrawEventAction();
						}
					}
					break;

				case EDspSmEvent.DSP_REPLAY_JUMP_TO_EVENT:
				case EDspSmEvent.DSP_STOP_REPLAY_EVENT:
						m_replayJumpToEventData = null;
						errCode = dspMgrReplayPauseStateSmGo(smEvent);
					break;

				default:
						Logger.LogWarning("{0}.dspMgrReplayDrawEventStateSmGo(SmEvent={1}) - WRONG event for {2} state.", CLASS_NAME, smEvent.SmEventNum, DspState);
						errCode = eRmdErrorCode.eRMD_SM_WRONG_EVENT4STATE;
		            break;
            }

			return errCode;
		}

		private eRmdErrorCode dspMgrReplayPauseStateSmGo(CSmEvent<EDspSmEvent> smEvent)
		{
			var errCode = eRmdErrorCode.eRMD_SUCCESS;   //default return value;
			var tmprawDataFileMgr = m_rawDataFileMgr;
			switch (smEvent.SmEventNum)
			{
				case EDspSmEvent.DSP_STOP_REPLAY_EVENT:
                    m_cardInfo.OperationMode = (ushort)GlobalTypes.ESystemModes.Offline;
					DspMgrLoadBasicConfig();
					dspMgrTransferPc2DspWait(); // Send immediately to DSP -  fix important for single core arch
					if (tmprawDataFileMgr != null)
						tmprawDataFileMgr.RawDataReadStop();

					//tmprawDataFileMgr.TryDispose();
					m_rawDataFileMgr = null;

					m_dspToPcContainer = null;
					m_eventBufferStartPoint = 0;
					m_drawLastBufferIndex = 0;
					m_studyAbsoluteLastBufferIndex = 0;

					setSmState(EDspState.DSP_OFFLINE_STATE);
					break;

				case EDspSmEvent.DSP_RESUME_REPLAY_EVENT:
					m_replayMilisecLast = -1;
					m_cardInfo.OperationMode = (ushort)GlobalTypes.ESystemModes.Replay;
					DspMgrLoadBasicConfig();
					setSmState(EDspState.DSP_REPLAY_STATE);

					if(m_replayJumpToEventData != null) 
						LayoutManager.Instance.FireJumpToEvent();
					else
						LayoutManager.Instance.FireChangeModeGraphEvent(createBmpRawData());// New developer, redesign LayoutManager !!!

					break;

				case EDspSmEvent.DSP_HW_DISCONNECT_EVENT:
					dspDisconnect();
					break;

				case EDspSmEvent.DSP_HW_CONNECT_EVENT:
					break;

                case EDspSmEvent.DSP_SCROLL_EVENT:
					dspMgrSmScrollEvent(smEvent.SmEventParamsArr);
					break;

				case EDspSmEvent.DSP_ADD_HISTORY_EVENT:
					dspMgrSmAddHistoryEvent(smEvent.SmEventParamsArr);
					break;

				case EDspSmEvent.DSP_REPLAY_JUMP_TO_EVENT:
					dspMgrReplayJumpToEventSmAction(smEvent.SmEventParamsArr);
					break;

				case EDspSmEvent.DSP_PAUSE_REPLAY_EVENT:
					break;

				default:
						Logger.LogWarning("{0}.dspMgrReplayPauseStateSmGo(SmEvent={1}) - WRONG event for {2} state.", CLASS_NAME, smEvent.SmEventNum, DspState);
						errCode = eRmdErrorCode.eRMD_SM_WRONG_EVENT4STATE;
					break;
			}
			return errCode;
		}

		private eRmdErrorCode dspMgrSmGo(CSmEvent<EDspSmEvent> smEvent)
		{
			var enterDspState = DspState;
            if (smEvent == null)
            {
				Logger.LogWarning("dspMgrSmGo(smEvent) input parameter can not be null.");
                return eRmdErrorCode.eRMD_INVALID_PARAM;
            }
			var errCode = eRmdErrorCode.eRMD_SUCCESS;   //default return value;
            try
            {
                switch (DspState)
                {
					case EDspState.DSP_HW_INIT_STATE:
						errCode = dspMgrHwInitStateSmGo(smEvent);
						break;
					case EDspState.DSP_OFFLINE_STATE:
						errCode = dspMgrOfflineStateSmGo(smEvent); //start new study
						break;
					case EDspState.DSP_ONLINE_STATE:
						errCode = dspMgrOnlineStateSmGo(smEvent);
						break;
					case EDspState.DSP_ONLINE_FREEZE_STATE:
						errCode = dspMgrOnlineFreezeStateSmGo(smEvent);
						break;
					case EDspState.DSP_FPGA_LOAD_STATE:
						break;
					case EDspState.DSP_REPLAY_STATE:
						errCode = dspMgrReplayStateSmGo(smEvent);
						break;
					case EDspState.DSP_REPLAY_PAUSE_STATE:
						errCode = dspMgrReplayPauseStateSmGo(smEvent);
						break;
					case EDspState.DSP_REPLAY_DRAW_EVENT_STATE:
						errCode = dspMgrReplayDrawEventStateSmGo(smEvent);
						break;
					default:
			                errCode = eRmdErrorCode.eRMD_FAILED;
							Logger.LogWarning("{0}.dspMgrSmGo({1}): Unrecognized State.", CLASS_NAME, DspState);
						break;
				}
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                errCode = eRmdErrorCode.eRMD_FAILED;
            }

            if (errCode != eRmdErrorCode.eRMD_SUCCESS)
            {
				Logger.LogWarning("DspMgr.dspMgrSmGo() State={0}: ERROR={1}", DspState, errCode);
	            recoverDSPState();

				if (smEvent.SmEventNum == EDspSmEvent.DSP_FINISH_STUDY_SAVE_STORAGE_EVENT)
				{
					//m_rawDataFileMgr.TryDispose();
					m_rawDataFileMgr = null;
					LoggedDialog.ShowError(MainForm.Instance, "Study data save fail.\n");
				}
			}

			if (enterDspState != DspState)
				Logger.LogDebug("DspManager SM ({0}): {1} --{2}--> {3}", errCode, enterDspState, smEvent.SmEventNum, DspState);

            return errCode;
		}

		private void recoverDSPState()
		{
			Logger.LogWarning("Application state ({0}) vs. DSP state ({1}) mismatch. Attempt to recover.", LayoutManager.SystemMode, DspState);

			if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Offline && DspState != EDspState.DSP_OFFLINE_STATE)
			{
				recoverDSPState(EDspState.DSP_OFFLINE_STATE);
			}
			else if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Online && (DspState != EDspState.DSP_ONLINE_STATE || DspState != EDspState.DSP_ONLINE_FREEZE_STATE))
			{
				recoverDSPState(EDspState.DSP_ONLINE_FREEZE_STATE);
				recoverDSPState(EDspState.DSP_ONLINE_STATE);
			}
			else
			{
				Logger.LogError(string.Format("Application state ({0}) vs DSP state ({1}) mismatch. Unable to resolve state", LayoutManager.SystemMode, DspState));
				return;
			}

			Logger.LogInfo("Application state vs. DSP state mismatch recovered ({0} vs. {1})", LayoutManager.SystemMode, DspState);
		}
		#endregion state Machine

		private void recoverDSPState(EDspState dspState)
		{
			m_cardInfo.OperationMode	= (ushort)(dspState == EDspState.DSP_ONLINE_STATE ? GlobalTypes.ESystemModes.Online : GlobalTypes.ESystemModes.Offline);

			DspMgrLoadBasicConfig();
			dspMgrTransferPc2DspWait(); // Send immediately to DSP 
			setSmState(dspState);
		}

		#region Main Task
		private void dspTransferTask()
		{
			CSmEvent<EDspSmEvent> smEvent;

            // Process GUI (none-DSP) events
			while (!m_smPendingEvents.IsEmpty && m_isRunning) 
            {
				smEvent = m_smPendingEvents.Dequeue();
				dspMgrSmGo(smEvent);
			}

			if (!m_isRunning)
                return;

			testDspConnection();    //HwConnected = m_commLayerBoundary.HwConnected();

			if (HwConnected)
			{
				if (DateTime.UtcNow >= m_nextTransferPc2Dsp)
				{
					m_nextTransferPc2Dsp = DateTime.UtcNow.AddMilliseconds(PCTODSP_ROUTINE_INTERVAL_MSEC);
					dspMgrTransferPc2DspWait();
				}

			    smEvent = new CSmEvent<EDspSmEvent>(EDspSmEvent.DSP_HW_CONNECT_EVENT);
			}
			else
			{
			    smEvent = new CSmEvent<EDspSmEvent>(EDspSmEvent.DSP_HW_DISCONNECT_EVENT);
			}

            dspMgrSmGo(smEvent);
		}


		private long m_timerCounter = 0;
        private void timerMethod(object state)
		{
            var pDspMgr = state as DspManager;
			if (pDspMgr == null)
			    return;

            var timeStamp = DateTime.UtcNow;
            timerOff();

	        m_timerCounter++;
	        if (Thread.CurrentThread.Name == null)
		        Thread.CurrentThread.Name = string.Format("Timer.{0:X6}", m_timerCounter);

            try
            {
				pDspMgr.dspTransferTask();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
				Thread.Sleep(500);
            }
            finally
            {
				if (m_isRunning)
                {

					var wait = (int)Math.Round(RawDataFile.DSP_MESSAGE_DURATION_MSEC - DateTime.UtcNow.Subtract(timeStamp).TotalMilliseconds); //var wait = (DSP_INT_CYCLE_MILISEC - (int) DateTime.UtcNow.Subtract(timeStamp).TotalMilliseconds);
                    if (wait < 0)
                        wait = 0;

                    timerOn(wait);
                }
            }
		}
		#endregion Main Task

		private void dspRxThreadDelegate()
		{
			Logger.LogInfo("{0}.dspRxThreadDelegate(): waitForDspToConnect.", CLASS_NAME);

			// Wait for DSPManager to run and DSP Card to connect
			try
			{
				waitForDspToConnect();
			}
			catch (ThreadAbortException)
			{
				Logger.LogInfo("{0}.dspRxThreadDelegate(): ThreadAbortException.", CLASS_NAME);
				return;
			}

			Logger.LogInfo("{0}.dspRxThreadDelegate(): START.", CLASS_NAME);
			RDspToPc preValidMsg = null;
			var iterationSleep = QThreadBase.WAIT_TIMEOUT_NONE;
			m_isDSPConnectedOnce = HwConnected;
			while (m_isRunning && HwConnected)
			{
				if (!m_isRunning || LayoutManager.SystemMode == GlobalTypes.ESystemModes.Offline)
				{
					Thread.Sleep(100);
					iterationSleep = QThreadBase.WAIT_TIMEOUT_NONE;

					continue;
				}

				try
				{
					if (iterationSleep >= 0)
						Thread.Sleep(iterationSleep);	

					var dspRxBufferSize = 0;
					var dspRxBuffer		= new byte[DspBlockConst.SIZE_OF_R_DSP2PC];
					var errCode			= m_commLayerBoundary.ComLayerBoundaryFrameRx(dspRxBuffer, eDspMsgType.DSP2PC_MSG_TYPE, ref dspRxBufferSize);
					if (errCode != eRmdErrorCode.eRMD_SUCCESS)
					{
						iterationSleep = 1;
						if (errCode != eRmdErrorCode.eRMD_LIST_EMPTY && errCode != eRmdErrorCode.eRMD_DATA_NOT_READY)
						{
							iterationSleep = (int)(DspBlockConst.COLUMN_DURATION);
							Logger.LogWarning("DspMgr.dspRxThreadDelegate(): Error ({0}) reading DSP message: {1}.", (int)errCode, rmdErrorCodeDescription(errCode));
						}

						continue;
					}

					iterationSleep = QThreadBase.WAIT_TIMEOUT_NONE;

					var dspToPc = new RDspToPc(dspRxBuffer, AppSettings.Active.DSPInvalidMessageCheckNumerator);
					DspPerformanceCounters.DspToPcMessagesIncrement(dspToPc.IsValid);
					if (!dspToPc.IsValid)
					{
						if (AppSettings.Active.DSPInvalidMessageDrop)
						{
							Logger.LogWarning("DspMgr.dspRxThreadDelegate(): Invalid DSP message - message dropped.");
							continue;
						}

						if (AppSettings.Active.DSPInvalidMessageUsePrevious && preValidMsg != null)
						{
							dspToPc = preValidMsg;
							Logger.LogWarning("DspMgr.dspRxThreadDelegate(): Invalid DSP message - using previous message data.");
						}
					}

					lock (m_dspToPcQeueue)
						m_dspToPcQeueue.Enqueue(dspToPc);

					AudioDSPtoPCStreamPlayer.PostPlay(dspToPc);	//Ofer, Enable sound on PC

					preValidMsg = dspToPc;
				}
				catch (ThreadAbortException)
				{
					Logger.LogInfo("{0}.dspRxThreadDelegate(): ThreadAbortException.", CLASS_NAME);
					break;
				}
				catch (Exception ex)
				{
					Logger.LogError(ex);
					Thread.Sleep(500);
					iterationSleep = QThreadBase.WAIT_TIMEOUT_NONE;
				}
			}

			Logger.LogInfo("{0}.dspRxThreadDelegate(): END.", CLASS_NAME);
		}

		private string rmdErrorCodeDescription(eRmdErrorCode value)
		{
			switch (value)
			{
				case eRmdErrorCode.eRMD_ABNORMAL_KERNEL_EXECUTION:
					return "eRMD_ABNORMAL_KERNEL_EXECUTION";
				case eRmdErrorCode.eRMD_SM_WRONG_EVENT4STATE:
					return "eRMD_SM_WRONG_EVENT4STATE";
				case eRmdErrorCode.eRMD_SM_UNKNOWN_EVENT:
					return "eRMD_SM_UNKNOWN_EVENT";
				case eRmdErrorCode.eRMD_INPUT_BUFF_TOO_LARGE:
					return "eRMD_INPUT_BUFF_TOO_LARGE";
				case eRmdErrorCode.eRMD_DISCARD_UNKNOWN_MESSAGE:
					return "eRMD_DISCARD_UNKNOWN_MESSAGE";
				case eRmdErrorCode.eRMD_MORE_DATA:
					return "eRMD_MORE_DATA";
				case eRmdErrorCode.eRMD_INVALID_PARAM:
					return "eRMD_INVALID_PARAM";
				case eRmdErrorCode.eRMD_DEVICE_UNKNOWN_TYPE:
					return "eRMD_DEVICE_UNKNOWN_TYPE";
				case eRmdErrorCode.eRMD_LIST_EMPTY:
					return "eRMD_LIST_EMPTY";
				case eRmdErrorCode.eRMD_OBJ_NOT_INITIALIZED:
					return "eRMD_OBJ_NOT_INITIALIZED";
				case eRmdErrorCode.eRMD_WAIT_TIMEOUT_ERR:
					return "eRMD_WAIT_TIMEOUT_ERR";
				case eRmdErrorCode.eRMD_FILE_NOT_FOUND:
					return "eRMD_FILE_NOT_FOUND";
				case eRmdErrorCode.eRMD_FILE_ERROR:
					return "eRMD_FILE_ERROR";
				case eRmdErrorCode.eRMD_DEVICE_NOT_READY:
					return "eRMD_DEVICE_NOT_READY";
				case eRmdErrorCode.eRMD_WRONG_STATE:
					return "eRMD_WRONG_STATE";
				case eRmdErrorCode.eRMD_NOT_ENOUGH_SPACE_ALLOCATION:
					return "eRMD_NOT_ENOUGH_SPACE_ALLOCATION";
				case eRmdErrorCode.eRMD_DOUBLE_FAULT:
					return "eRMD_DOUBLE_FAULT";
				case eRmdErrorCode.eRMD_ERR_FREE_NATIVE_RESOURCE:
					return "eRMD_ERR_FREE_NATIVE_RESOURCE";
				case eRmdErrorCode.eRMD_THREAD_TERMINATION_TIMEOUT:
					return "eRMD_THREAD_TERMINATION_TIMEOUT";
				case eRmdErrorCode.eRMD_FAILED:
					return "eRMD_FAILED";
				case eRmdErrorCode.eRMD_SUCCESS:
					return "eRMD_SUCCESS";
				case eRmdErrorCode.eRMD_DATA_NOT_READY:
					return "eRMD_DATA_NOT_READY";
				case eRmdErrorCode.eRMD_MESSAGE_NOT_SUPPORTED:
					return "eRMD_MESSAGE_NOT_SUPPORTED";

				default:
					return string.Format("UNKNOWN ({0})", (int) value);
			}
		}
		private bool waitForDspToConnect(TimeSpan? timeout = null, int spinWait = 250)
        {
            var expired = DateTime.MaxValue;
            if (timeout.HasValue)
                expired = DateTime.UtcNow.Add(timeout.Value);

            // Wait for DSP to connect
			while ((!m_isRunning || !HwConnected) && expired > DateTime.UtcNow)
            {
                Thread.Sleep(spinWait);
            }

			return HwConnected && m_isRunning;
        }

        private void testDspConnection(int tryCount = 2, int tryInterval = 250)
        {
	        if (DateTime.UtcNow < m_checkConnection)
		        return;

	        m_checkConnection = DateTime.UtcNow.AddMilliseconds(DSP_ROUTINE_CONNECTION_CHECK_INTERVAL_MSEC);

			while (tryCount > 0 && m_isRunning)
            {
                try
                {
	                tryCount--;
					var isConnected = m_commLayerBoundary.HwConnected();
                    //Logger.DebugStringOut("testDspConnection(): m_commLayerBoundary.HwConnected(): isConnected={0}", isConnected);
                    if (isConnected)
                    {
                        if (!HwConnected)
                            getHWInfo();

                        HwConnected			 = true;
						m_disconnectedLogged = false;

						return;
                    }

					if (m_isDSPConnectedOnce && !m_disconnectedLogged)
					{
						m_disconnectedLogged = true;
						Logger.LogWarning("DSP DISCONNECTED: tryCount={0}, IsRunning={1}, HwConnected flag value={2}, Is once connected={3}", tryCount, m_isRunning, HwConnected, m_isDSPConnectedOnce);
					}
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex);
                }

                Thread.Sleep(tryInterval);
            }

            HwConnected = false;
        }
		#endregion Private Methods
	}
}