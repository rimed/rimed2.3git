using Rimed.TCD.DSPData;

namespace Rimed.TCD.GUI.DSPMgr
{
    public class CDspMgrReplayJumpToEventData
    {
        public CDspMgrReplayJumpToEventData(int paramFileIndex, int paramEventBufferStartPoint, int paramNumOfBuffersToDraw)
        {
            FileIndex = paramFileIndex;
            EventBufferStartPoint = paramEventBufferStartPoint;
            NumOfBuffersToDraw = paramNumOfBuffersToDraw;
            DrawColCount = 0;
            EventDspToPcContainer = null;
        }

        public readonly int FileIndex;
        public readonly int EventBufferStartPoint;
        public readonly int NumOfBuffersToDraw;
        public int DrawColCount;
        public DspToPcContainer EventDspToPcContainer;

        public override string ToString()
        {
            return string.Format("FileIndex={0}, EventStartPointAbsoluteIndex={1}, NumOfBuffersToDraw={2}, DrawColCount={3}, EventDspToPcContainer=({4})", FileIndex, EventBufferStartPoint, NumOfBuffersToDraw, DrawColCount, EventDspToPcContainer);
        }
    }
}