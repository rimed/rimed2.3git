using System;

namespace Rimed.TCD.GUI.DSPMgr
{
    public class CDspMgrStudyParams
    {
        public CDspMgrStudyParams(string paramDirName, UInt64 paramStudyId, EStudyLayoutType paramStudyType)
        {
            StudyId = paramStudyId;
            DirName = paramDirName;
            StudyType = paramStudyType;
        }

        public readonly String DirName;
        public readonly UInt64 StudyId;
        public readonly EStudyLayoutType StudyType;

        public override string ToString()
        {
            return string.Format("DirName='{0}', StudyId={1}, StudyType={2}", DirName, StudyId, StudyType);
        }
    }
}