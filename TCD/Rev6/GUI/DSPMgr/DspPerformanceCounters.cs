﻿using System;
using System.Diagnostics;
using Rimed.Framework.Common;

namespace Rimed.TCD.GUI.DSPMgr
{
	public class DspPerformanceCounters : Rimed.Framework.Common.Disposable
	{
		private const string CATEGORY_NAME                          = "Rimed.TCD.Rev6: DSP";
		private const string COUNTER_DSPTOPC_MESSAGES				= "DSPtoPC messages";
		private const string COUNTER_DSPTOPC_VALID_MESSAGES			= "DSPtoPC Valid messages";
		private const string COUNTER_DSPTOPC_MESSAGE_RATE			= "DSPtoPC messages / Second";
		private static DspPerformanceCounters s_instance;
		
		private static void createInstance()
		{
			try
			{
				s_instance = new DspPerformanceCounters();
			}
			catch (Exception ex)
			{
				s_instance = null;
				Logger.LogError(ex);
			}
		}

		private static void deleteCounters()
		{
			if (!PerformanceCounterCategory.Exists(CATEGORY_NAME))
				return;

			try
			{
				PerformanceCounterCategory.Delete(CATEGORY_NAME);
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);
			}
		}


		/// <summary>Create instance / Delete DSP performance counters (configured on AppConfig).</summary>
		public static void Init()
		{
			if (AppSettings.Active.DSPPerformanceCountersEnabled)
				createInstance();
            //else
            //    deleteCounters();
		}

		public static void DspToPcMessagesIncrement(bool isValid = true, int value = 1)
		{
			if (s_instance == null)
				return;

			s_instance.m_pcDspToPcMessages.IncrementBy(value);
			s_instance.m_pcDspToPcMessagesRate.IncrementBy(value);
			if (isValid)
				s_instance.m_pcDspToPcValidMessages.IncrementBy(value);
		}


		private PerformanceCounter m_pcDspToPcMessages;
		private PerformanceCounter m_pcDspToPcValidMessages;
		private PerformanceCounter m_pcDspToPcMessagesRate;

		private DspPerformanceCounters()
		{
			init();
		}


		protected override void DisposeUnmanagedResources()
		{
			m_pcDspToPcMessages.TryDispose();
			m_pcDspToPcMessages = null;

			m_pcDspToPcValidMessages.TryDispose();
			m_pcDspToPcValidMessages = null;
		}

		private void init()
		{
			createCategory();

			m_pcDspToPcMessages						= new PerformanceCounter(CATEGORY_NAME, COUNTER_DSPTOPC_MESSAGES, false);
			m_pcDspToPcMessages.RawValue			= 0;

			m_pcDspToPcValidMessages				= new PerformanceCounter(CATEGORY_NAME, COUNTER_DSPTOPC_VALID_MESSAGES, false);
			m_pcDspToPcValidMessages.RawValue		= 0;


			m_pcDspToPcMessagesRate					= new PerformanceCounter(CATEGORY_NAME, COUNTER_DSPTOPC_MESSAGE_RATE, false);
			m_pcDspToPcMessagesRate.RawValue		= 01;
		}

		private void createCategory()
		{
			if (PerformanceCounterCategory.Exists(CATEGORY_NAME))
				return;

			var ccds = new CounterCreationDataCollection();

			var pc = new CounterCreationData(COUNTER_DSPTOPC_MESSAGES, "Number of messages received from the DSP", PerformanceCounterType.NumberOfItems64);
			ccds.Add(pc);

			pc = new CounterCreationData(COUNTER_DSPTOPC_VALID_MESSAGES, "Number of valid messages received from the DSP", PerformanceCounterType.NumberOfItems64);
			ccds.Add(pc);

			pc = new CounterCreationData(COUNTER_DSPTOPC_MESSAGE_RATE, "", PerformanceCounterType.RateOfCountsPerSecond64);
			ccds.Add(pc);

			PerformanceCounterCategory.Create(CATEGORY_NAME, "", PerformanceCounterCategoryType.SingleInstance, ccds);
		}
	}
}
