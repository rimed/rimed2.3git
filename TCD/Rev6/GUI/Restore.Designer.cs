﻿namespace Rimed.TCD.GUI
{
    partial class Restore
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.patientListBox = new System.Windows.Forms.CheckedListBox();
            this.lblPatientList = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.bDetails = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.listViewExamination = new System.Windows.Forms.ListView();
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblExaminationList = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.gbRestoreInformation = new System.Windows.Forms.GroupBox();
            this.lbLocation = new System.Windows.Forms.Label();
            this.bBrowseFolder = new System.Windows.Forms.Button();
            this.location = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lbEstimatedTime = new System.Windows.Forms.Label();
            this.labelSize = new System.Windows.Forms.Label();
            this.lbSize = new System.Windows.Forms.Label();
            this.bRestore = new System.Windows.Forms.Button();
            this.bHelp = new System.Windows.Forms.Button();
            this.bClose = new System.Windows.Forms.Button();
            this.gbSearchPatient = new System.Windows.Forms.GroupBox();
            this.autoCompleteComboBoxID = new AutoCompleteComboBox();
            this.autoCompleteComboBoxName = new AutoCompleteComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.gbMode = new System.Windows.Forms.GroupBox();
            this.rbUserDefined = new System.Windows.Forms.RadioButton();
            this.rbIncrement = new System.Windows.Forms.RadioButton();
            this.rbAll = new System.Windows.Forms.RadioButton();
            this.folderBrowser1 = new System.Windows.Forms.FolderBrowserDialog();
            this.panel1.SuspendLayout();
            this.gbRestoreInformation.SuspendLayout();
            this.gbSearchPatient.SuspendLayout();
            this.gbMode.SuspendLayout();
            this.SuspendLayout();
            // 
            // patientListBox
            // 
            this.patientListBox.Location = new System.Drawing.Point(16, 192);
            this.patientListBox.Name = "patientListBox";
            this.patientListBox.Size = new System.Drawing.Size(200, 214);
            this.patientListBox.TabIndex = 0;
            this.patientListBox.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.patientListBox_ItemCheck);
            this.patientListBox.SelectedIndexChanged += new System.EventHandler(this.patientListBox_SelectedIndexChanged);
            this.patientListBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.patientListBox_MouseUp);
            // 
            // lblPatientList
            // 
            this.lblPatientList.Location = new System.Drawing.Point(16, 168);
            this.lblPatientList.Name = "lblPatientList";
            this.lblPatientList.Size = new System.Drawing.Size(200, 16);
            this.lblPatientList.TabIndex = 2;
            this.lblPatientList.Text = "Patient List";
            this.lblPatientList.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(32, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 20);
            this.label2.TabIndex = 11;
            this.label2.Text = "Name:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // bDetails
            // 
            this.bDetails.Enabled = false;
            this.bDetails.Location = new System.Drawing.Point(272, 48);
            this.bDetails.Name = "bDetails";
            this.bDetails.Size = new System.Drawing.Size(96, 32);
            this.bDetails.TabIndex = 12;
            this.bDetails.Text = "Details";
            this.bDetails.Click += new System.EventHandler(this.bDetails_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(216, 248);
            this.panel2.TabIndex = 5;
            // 
            // listViewExamination
            // 
            this.listViewExamination.CheckBoxes = true;
            this.listViewExamination.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader6,
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.listViewExamination.FullRowSelect = true;
            this.listViewExamination.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listViewExamination.HideSelection = false;
            this.listViewExamination.Location = new System.Drawing.Point(224, 32);
            this.listViewExamination.Name = "listViewExamination";
            this.listViewExamination.Size = new System.Drawing.Size(432, 214);
            this.listViewExamination.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.listViewExamination.TabIndex = 4;
            this.listViewExamination.UseCompatibleStateImageBehavior = false;
            this.listViewExamination.View = System.Windows.Forms.View.Details;
            this.listViewExamination.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.listViewExamination_ItemCheck);
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "";
            this.columnHeader6.Width = 20;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Width = 75;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Width = 50;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Width = 100;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Width = 151;
            // 
            // lblExaminationList
            // 
            this.lblExaminationList.BackColor = System.Drawing.SystemColors.Control;
            this.lblExaminationList.Location = new System.Drawing.Point(248, 8);
            this.lblExaminationList.Name = "lblExaminationList";
            this.lblExaminationList.Size = new System.Drawing.Size(368, 16);
            this.lblExaminationList.TabIndex = 3;
            this.lblExaminationList.Text = "Examination List";
            this.lblExaminationList.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Location = new System.Drawing.Point(216, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(440, 248);
            this.panel3.TabIndex = 6;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.listViewExamination);
            this.panel1.Controls.Add(this.lblExaminationList);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Location = new System.Drawing.Point(8, 160);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(664, 256);
            this.panel1.TabIndex = 9;
            // 
            // gbRestoreInformation
            // 
            this.gbRestoreInformation.Controls.Add(this.lbLocation);
            this.gbRestoreInformation.Controls.Add(this.bBrowseFolder);
            this.gbRestoreInformation.Controls.Add(this.location);
            this.gbRestoreInformation.Controls.Add(this.label6);
            this.gbRestoreInformation.Controls.Add(this.lbEstimatedTime);
            this.gbRestoreInformation.Controls.Add(this.labelSize);
            this.gbRestoreInformation.Controls.Add(this.lbSize);
            this.gbRestoreInformation.Location = new System.Drawing.Point(8, 424);
            this.gbRestoreInformation.Name = "gbRestoreInformation";
            this.gbRestoreInformation.Size = new System.Drawing.Size(664, 112);
            this.gbRestoreInformation.TabIndex = 13;
            this.gbRestoreInformation.TabStop = false;
            this.gbRestoreInformation.Text = "Restore Information";
            // 
            // lbLocation
            // 
            this.lbLocation.Location = new System.Drawing.Point(16, 48);
            this.lbLocation.Name = "lbLocation";
            this.lbLocation.Size = new System.Drawing.Size(85, 20);
            this.lbLocation.TabIndex = 8;
            this.lbLocation.Text = "Location:";
            this.lbLocation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // bBrowseFolder
            // 
            this.bBrowseFolder.Location = new System.Drawing.Point(273, 46);
            this.bBrowseFolder.Name = "bBrowseFolder";
            this.bBrowseFolder.Size = new System.Drawing.Size(24, 24);
            this.bBrowseFolder.TabIndex = 7;
            this.bBrowseFolder.Text = "...";
            this.bBrowseFolder.Click += new System.EventHandler(this.bBrowseFolder_Click);
            // 
            // location
            // 
            this.location.Location = new System.Drawing.Point(107, 48);
            this.location.Name = "location";
            this.location.Size = new System.Drawing.Size(160, 20);
            this.location.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label6.Location = new System.Drawing.Point(568, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 16);
            this.label6.TabIndex = 3;
            this.label6.Text = "00:00:00";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbEstimatedTime
            // 
            this.lbEstimatedTime.Location = new System.Drawing.Point(399, 72);
            this.lbEstimatedTime.Name = "lbEstimatedTime";
            this.lbEstimatedTime.Size = new System.Drawing.Size(134, 16);
            this.lbEstimatedTime.TabIndex = 2;
            this.lbEstimatedTime.Text = "Estimated Time:";
            this.lbEstimatedTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelSize
            // 
            this.labelSize.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.labelSize.Location = new System.Drawing.Point(568, 32);
            this.labelSize.Name = "labelSize";
            this.labelSize.Size = new System.Drawing.Size(72, 16);
            this.labelSize.TabIndex = 1;
            this.labelSize.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbSize
            // 
            this.lbSize.Location = new System.Drawing.Point(399, 32);
            this.lbSize.Name = "lbSize";
            this.lbSize.Size = new System.Drawing.Size(164, 16);
            this.lbSize.TabIndex = 0;
            this.lbSize.Text = "Size of selected files:";
            this.lbSize.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // bRestore
            // 
            this.bRestore.Enabled = false;
            this.bRestore.Location = new System.Drawing.Point(16, 552);
            this.bRestore.Name = "bRestore";
            this.bRestore.Size = new System.Drawing.Size(104, 23);
            this.bRestore.TabIndex = 14;
            this.bRestore.Text = "Restore";
            this.bRestore.Click += new System.EventHandler(this.bRestore_Click);
            // 
            // bHelp
            // 
            this.bHelp.Location = new System.Drawing.Point(496, 552);
            this.bHelp.Name = "bHelp";
            this.bHelp.Size = new System.Drawing.Size(75, 23);
            this.bHelp.TabIndex = 16;
            this.bHelp.Text = "Help";
            // 
            // bClose
            // 
            this.bClose.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bClose.Location = new System.Drawing.Point(592, 552);
            this.bClose.Name = "bClose";
            this.bClose.Size = new System.Drawing.Size(75, 23);
            this.bClose.TabIndex = 17;
            this.bClose.Text = "Close";
            // 
            // gbSearchPatient
            // 
            this.gbSearchPatient.Controls.Add(this.autoCompleteComboBoxID);
            this.gbSearchPatient.Controls.Add(this.autoCompleteComboBoxName);
            this.gbSearchPatient.Controls.Add(this.bDetails);
            this.gbSearchPatient.Controls.Add(this.label9);
            this.gbSearchPatient.Controls.Add(this.label2);
            this.gbSearchPatient.Location = new System.Drawing.Point(232, 24);
            this.gbSearchPatient.Name = "gbSearchPatient";
            this.gbSearchPatient.Size = new System.Drawing.Size(440, 120);
            this.gbSearchPatient.TabIndex = 18;
            this.gbSearchPatient.TabStop = false;
            this.gbSearchPatient.Text = "Search Patient";
            // 
            // autoCompleteComboBoxID
            // 
            this.autoCompleteComboBoxID.LimitToList = true;
            this.autoCompleteComboBoxID.Location = new System.Drawing.Point(104, 72);
            this.autoCompleteComboBoxID.Name = "autoCompleteComboBoxID";
            this.autoCompleteComboBoxID.Size = new System.Drawing.Size(121, 21);
            this.autoCompleteComboBoxID.TabIndex = 22;
            this.autoCompleteComboBoxID.SelectedIndexChanged += new System.EventHandler(this.autoCompleteComboBoxID_SelectedIndexChanged);
            // 
            // autoCompleteComboBoxName
            // 
            this.autoCompleteComboBoxName.LimitToList = true;
            this.autoCompleteComboBoxName.Location = new System.Drawing.Point(104, 40);
            this.autoCompleteComboBoxName.Name = "autoCompleteComboBoxName";
            this.autoCompleteComboBoxName.Size = new System.Drawing.Size(121, 21);
            this.autoCompleteComboBoxName.TabIndex = 21;
            this.autoCompleteComboBoxName.SelectedIndexChanged += new System.EventHandler(this.autoCompleteComboBoxName_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(32, 73);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(66, 20);
            this.label9.TabIndex = 19;
            this.label9.Text = "ID#:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gbMode
            // 
            this.gbMode.Controls.Add(this.rbUserDefined);
            this.gbMode.Controls.Add(this.rbIncrement);
            this.gbMode.Controls.Add(this.rbAll);
            this.gbMode.Enabled = false;
            this.gbMode.Location = new System.Drawing.Point(8, 24);
            this.gbMode.Name = "gbMode";
            this.gbMode.Size = new System.Drawing.Size(216, 120);
            this.gbMode.TabIndex = 19;
            this.gbMode.TabStop = false;
            this.gbMode.Text = "Mode For Restore";
            // 
            // rbUserDefined
            // 
            this.rbUserDefined.Checked = true;
            this.rbUserDefined.Location = new System.Drawing.Point(16, 80);
            this.rbUserDefined.Name = "rbUserDefined";
            this.rbUserDefined.Size = new System.Drawing.Size(192, 20);
            this.rbUserDefined.TabIndex = 2;
            this.rbUserDefined.TabStop = true;
            this.rbUserDefined.Text = "User Defined";
            this.rbUserDefined.Click += new System.EventHandler(this.rbUserDefined_Click);
            // 
            // rbIncrement
            // 
            this.rbIncrement.Location = new System.Drawing.Point(16, 56);
            this.rbIncrement.Name = "rbIncrement";
            this.rbIncrement.Size = new System.Drawing.Size(153, 24);
            this.rbIncrement.TabIndex = 1;
            this.rbIncrement.Text = "Increment";
            this.rbIncrement.Visible = false;
            this.rbIncrement.Click += new System.EventHandler(this.rbIncrement_Click);
            // 
            // rbAll
            // 
            this.rbAll.Location = new System.Drawing.Point(16, 24);
            this.rbAll.Name = "rbAll";
            this.rbAll.Size = new System.Drawing.Size(96, 48);
            this.rbAll.TabIndex = 0;
            this.rbAll.Text = "All";
            this.rbAll.Click += new System.EventHandler(this.rbAll_Click);
            // 
            // folderBrowser1
            // 
            this.folderBrowser1.RootFolder = System.Environment.SpecialFolder.MyComputer;
            this.folderBrowser1.ShowNewFolderButton = false;
            // 
            // Restore
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(680, 584);
            this.Controls.Add(this.gbMode);
            this.Controls.Add(this.bClose);
            this.Controls.Add(this.bHelp);
            this.Controls.Add(this.bRestore);
            this.Controls.Add(this.gbRestoreInformation);
            this.Controls.Add(this.lblPatientList);
            this.Controls.Add(this.patientListBox);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.gbSearchPatient);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Restore";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Restore Patient Data";
            this.panel1.ResumeLayout(false);
            this.gbRestoreInformation.ResumeLayout(false);
            this.gbRestoreInformation.PerformLayout();
            this.gbSearchPatient.ResumeLayout(false);
            this.gbMode.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        
        #endregion

        private System.Windows.Forms.Label lblPatientList;
        private System.ComponentModel.IContainer components;
        private System.Windows.Forms.CheckedListBox patientListBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ListView listViewExamination;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Label lblExaminationList;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox gbRestoreInformation;
        private System.Windows.Forms.Button bClose;
        private System.Windows.Forms.Label lbSize;
        private System.Windows.Forms.Label lbEstimatedTime;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button bDetails;
        private System.Windows.Forms.RadioButton rbUserDefined;
        private System.Windows.Forms.RadioButton rbIncrement;
        private System.Windows.Forms.RadioButton rbAll;
        private System.Windows.Forms.FolderBrowserDialog folderBrowser1;
        private System.Windows.Forms.Button bBrowseFolder;
        private System.Windows.Forms.TextBox location;
        private System.Windows.Forms.Label lbLocation;
        private System.Windows.Forms.Label labelSize;
        private System.Windows.Forms.Button bHelp;
        private AutoCompleteComboBox autoCompleteComboBoxName;
        private AutoCompleteComboBox autoCompleteComboBoxID;
        private System.Windows.Forms.Button bRestore;
        private System.Windows.Forms.GroupBox gbSearchPatient;
        private System.Windows.Forms.GroupBox gbMode;
    }
}