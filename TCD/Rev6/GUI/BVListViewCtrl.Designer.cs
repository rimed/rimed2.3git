﻿namespace Rimed.TCD.GUI
{
    partial class BVListViewCtrl
    {
        protected override void Dispose(bool disposing)
        {
            removeEvents();
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.listView1 = new System.Windows.Forms.ListView();
            this.BVName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.GateDepth = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenu1 = new System.Windows.Forms.ContextMenu();
            this.menuItemReplayBV = new System.Windows.Forms.MenuItem();
            this.menuItemRename = new System.Windows.Forms.MenuItem();
            this.menuItemReturn = new System.Windows.Forms.MenuItem();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.listView2 = new System.Windows.Forms.ListView();
            this.BVName2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.GateDepth2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.BackColor = System.Drawing.SystemColors.Control;
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.BVName,
            this.GateDepth});
            this.listView1.ContextMenu = this.contextMenu1;
            this.listView1.Dock = System.Windows.Forms.DockStyle.Top;
            this.listView1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(0, 0);
            this.listView1.MultiSelect = false;
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(150, 80);
            this.listView1.StateImageList = this.imageList1;
            this.listView1.TabIndex = 0;
            this.listView1.Tag = "";
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            this.listView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.listView1_MouseDown);
            // 
            // BVName
            // 
            this.BVName.Text = "BV Name";
            this.BVName.Width = 55;
            // 
            // GateDepth
            // 
            this.GateDepth.Text = "Depth";
            this.GateDepth.Width = 59;
            // 
            // contextMenu1
            // 
            this.contextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemReplayBV,
            this.menuItemRename,
            this.menuItemReturn});
            this.contextMenu1.Popup += new System.EventHandler(this.contextMenu1_Popup);
            // 
            // menuItemReplayBV
            // 
            this.menuItemReplayBV.Index = 0;
            this.menuItemReplayBV.Text = "Replay BV";
            this.menuItemReplayBV.Visible = false;
            this.menuItemReplayBV.Click += new System.EventHandler(this.menuItemReplayBV_Click);
            // 
            // menuItemRename
            // 
            this.menuItemRename.Index = 1;
            this.menuItemRename.Text = "Rename BV";
            this.menuItemRename.Click += new System.EventHandler(this.menuRename_Click);
            // 
            // menuItemReturn
            // 
            this.menuItemReturn.Index = 2;
            this.menuItemReturn.Text = "Return BV";
            this.menuItemReturn.Click += new System.EventHandler(this.menuItemReturn_Click);
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(20, 20);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // listView2
            // 
            this.listView2.BackColor = System.Drawing.SystemColors.Control;
            this.listView2.CausesValidation = false;
            this.listView2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.BVName2,
            this.GateDepth2});
            this.listView2.ContextMenu = this.contextMenu1;
            this.listView2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.listView2.Enabled = false;
            this.listView2.FullRowSelect = true;
            this.listView2.GridLines = true;
            this.listView2.HideSelection = false;
            this.listView2.Location = new System.Drawing.Point(0, 575);
            this.listView2.MultiSelect = false;
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(150, 80);
            this.listView2.StateImageList = this.imageList1;
            this.listView2.TabIndex = 1;
            this.listView2.Tag = "";
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.View = System.Windows.Forms.View.Details;
            this.listView2.SelectedIndexChanged += new System.EventHandler(this.listView2_SelectedIndexChanged);
            // 
            // BVName2
            // 
            this.BVName2.Text = "BV Name";
            this.BVName2.Width = 30;
            // 
            // GateDepth2
            // 
            this.GateDepth2.Text = "Depth";
            this.GateDepth2.Width = 30;
            // 
            // imageList2
            // 
            this.imageList2.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imageList2.ImageSize = new System.Drawing.Size(20, 20);
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // BVListViewCtrl
            // 
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.listView2);
            this.Controls.Add(this.listView1);
            this.Name = "BVListViewCtrl";
            this.Size = new System.Drawing.Size(150, 655);
            this.SizeChanged += new System.EventHandler(this.BVListViewCtrl_SizeChanged);
            this.ResumeLayout(false);

        }

        #endregion

        private System.ComponentModel.IContainer components;
        private System.Windows.Forms.ColumnHeader BVName;
        private System.Windows.Forms.ColumnHeader GateDepth;
        private System.Windows.Forms.ColumnHeader BVName2;
        private System.Windows.Forms.ColumnHeader GateDepth2;
        private System.Windows.Forms.MenuItem menuItemReplayBV;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.ContextMenu contextMenu1;
        private System.Windows.Forms.MenuItem menuItemRename;
        private System.Windows.Forms.MenuItem menuItemReturn;
        private System.Windows.Forms.ImageList imageList1;
    }
}