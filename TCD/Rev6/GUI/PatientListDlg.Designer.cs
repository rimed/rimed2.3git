﻿namespace Rimed.TCD.GUI
{
    partial class PatientListDlg
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.lblName = new System.Windows.Forms.Label();
            this.lblID = new System.Windows.Forms.Label();
            this.buttonDetails = new System.Windows.Forms.Button();
            this.buttonCreateNewPatient = new System.Windows.Forms.Button();
            this.buttonHelp = new System.Windows.Forms.Button();
            this.autoCompleteComboBoxName = new AutoCompleteComboBox();
            this.autoCompleteComboBoxID = new AutoCompleteComboBox();
            this.SuspendLayout();
            // 
            // buttonOK
            // 
            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOK.Location = new System.Drawing.Point(44, 80);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(112, 24);
            this.buttonOK.TabIndex = 1;
            this.buttonOK.Text = "OK";
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(188, 80);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(112, 24);
            this.buttonCancel.TabIndex = 1;
            this.buttonCancel.Text = "Cancel";
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(3, 24);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(63, 24);
            this.lblName.TabIndex = 3;
            this.lblName.Text = "Name";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblID
            // 
            this.lblID.Location = new System.Drawing.Point(246, 24);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(74, 24);
            this.lblID.TabIndex = 3;
            this.lblID.Text = "ID Number";
            this.lblID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buttonDetails
            // 
            this.buttonDetails.Location = new System.Drawing.Point(512, 24);
            this.buttonDetails.Name = "buttonDetails";
            this.buttonDetails.Size = new System.Drawing.Size(112, 24);
            this.buttonDetails.TabIndex = 4;
            this.buttonDetails.Text = "Details";
            this.buttonDetails.Click += new System.EventHandler(this.buttonDetails_Click);
            // 
            // buttonCreateNewPatient
            // 
            this.buttonCreateNewPatient.AutoSize = true;
            this.buttonCreateNewPatient.Location = new System.Drawing.Point(338, 80);
            this.buttonCreateNewPatient.Name = "buttonCreateNewPatient";
            this.buttonCreateNewPatient.Size = new System.Drawing.Size(112, 24);
            this.buttonCreateNewPatient.TabIndex = 1;
            this.buttonCreateNewPatient.Text = "Create New Patient";
            this.buttonCreateNewPatient.Click += new System.EventHandler(this.buttonCreateNewPatient_Click);
            // 
            // buttonHelp
            // 
            this.buttonHelp.Location = new System.Drawing.Point(512, 80);
            this.buttonHelp.Name = "buttonHelp";
            this.buttonHelp.Size = new System.Drawing.Size(112, 24);
            this.buttonHelp.TabIndex = 1;
            this.buttonHelp.Text = "Help";
            // 
            // autoCompleteComboBoxName
            // 
            this.autoCompleteComboBoxName.LimitToList = true;
            this.autoCompleteComboBoxName.Location = new System.Drawing.Point(72, 24);
            this.autoCompleteComboBoxName.Name = "autoCompleteComboBoxName";
            this.autoCompleteComboBoxName.Size = new System.Drawing.Size(168, 21);
            this.autoCompleteComboBoxName.TabIndex = 5;
            this.autoCompleteComboBoxName.SelectedIndexChanged += new System.EventHandler(this.autoCompleteComboBoxName_SelectedIndexChanged);
            // 
            // autoCompleteComboBoxID
            // 
            this.autoCompleteComboBoxID.LimitToList = true;
            this.autoCompleteComboBoxID.Location = new System.Drawing.Point(328, 24);
            this.autoCompleteComboBoxID.Name = "autoCompleteComboBoxID";
            this.autoCompleteComboBoxID.Size = new System.Drawing.Size(168, 21);
            this.autoCompleteComboBoxID.TabIndex = 5;
            this.autoCompleteComboBoxID.SelectedIndexChanged += new System.EventHandler(this.autoCompleteComboBoxID_SelectedIndexChanged);
            this.autoCompleteComboBoxID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.autoCompleteComboBoxID_KeyPress);
            // 
            // PatientListDlg
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(640, 126);
            this.Controls.Add(this.autoCompleteComboBoxName);
            this.Controls.Add(this.buttonDetails);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.lblID);
            this.Controls.Add(this.autoCompleteComboBoxID);
            this.Controls.Add(this.buttonCreateNewPatient);
            this.Controls.Add(this.buttonHelp);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PatientListDlg";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Select Patient Dialog";
            this.Load += new System.EventHandler(this.PatientListDlg_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        
        #endregion

        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.Button buttonDetails;
        private AutoCompleteComboBox autoCompleteComboBoxName;
        private AutoCompleteComboBox autoCompleteComboBoxID;
        private System.Windows.Forms.Button buttonCreateNewPatient;
        private System.Windows.Forms.Button buttonHelp;
        private System.ComponentModel.Container components = null;
    }
}