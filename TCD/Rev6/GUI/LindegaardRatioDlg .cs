﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Rimed.TCD.Utils;
using Rimed.TCD.DAL;

namespace Rimed.TCD.GUI
{
    public partial class LindegaardRatioDlg : Form
    {

        double lMca, rMca, lIca, rIca;
        bool lMcaDone, rMcaDone, lIcaDone, rIcaDone;

        public LindegaardRatioDlg()
        {
            InitializeComponent();

            BackColor = GlobalSettings.BackgroundDlg;

            var strRes = MainForm.StringManager;
            this.lblLeft.Text = strRes.GetString("Left Side");
            this.lblRight.Text = strRes.GetString("Right Side");
            this.lblLR.Text = strRes.GetString("Lindegaard Ratio");
            this.btnCalculate.Text = strRes.GetString("Calculate");
            this.btnCancel.Text = strRes.GetString("Close");
            this.Text = strRes.GetString("Lindegaard Ratio");

            InitValues();
        }

        void InitValues()
        {
             RimedDal.Instance.GetLindegaardRatio(out lMca, out rMca, out lIca, out rIca, out lMcaDone, out rMcaDone, out lIcaDone, out rIcaDone);
             // enable manual calculation if one side is of vessels was recorded and then only of hte side/s that was recorded
             btnCalculate.Enabled = lMcaDone && lIcaDone || rMcaDone  && rIcaDone;

            if (lMcaDone)
                textBox_McaL.Text =  lMca.ToString();
            if (lIcaDone)
                textBox_IcaL.Text = lIca.ToString();
            if (rMcaDone)
                textBox_McaR.Text = rMca.ToString();
            if (rIcaDone)
                textBox_IcaR.Text = rIca.ToString();
            if (!lMcaDone || !lIcaDone)
                textBox_McaL.Enabled = textBox_IcaL.Enabled = false;
            else if (lIca != 0)
                lblRatioL.Text = String.Format("{0:N1}", lMca / lIca);
            if (!rMcaDone || !rIcaDone)
                textBox_IcaR.Enabled = textBox_McaR.Enabled = false;
            else if (rIca != 0)
                lblRatioR.Text = String.Format("{0:N1}", rMca / rIca);

        }

        private void Numeric_textbox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) &&
        (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private bool IsValid()
        {
            if (lMcaDone && !Double.TryParse(textBox_McaL.Text, out lMca))
                    return false;
            if (rMcaDone && !Double.TryParse(textBox_McaR.Text, out rMca))
                    return false;
            if (lIcaDone && !Double.TryParse(textBox_IcaL.Text, out lIca))
                return false;
            if (rIcaDone && !Double.TryParse(textBox_IcaR.Text, out rIca))
                return false;

            if (lIca == 0 && lIcaDone || rIca == 0 && rIcaDone)
                return false;

            return true;
        }


        private void btnCalculate_Click(object sender, EventArgs e)
        {
            if (!IsValid())
            {
                MessageBox.Show("Some values are invalid", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (textBox_McaL.Enabled)
                lblRatioL.Text = String.Format("{0:N1}", lMca / lIca);
            if (textBox_McaR.Enabled)
                lblRatioR.Text = String.Format("{0:N1}", rMca / rIca);

            RimedDal.Instance.SetManualLindegaardRatio(lMca, rMca, lIca, rIca);
            LayoutManager.Instance.UpdateLindegaardRatio();
            SummaryScreen.UpdateLindegaardRatio();
        }


 
    }
}
