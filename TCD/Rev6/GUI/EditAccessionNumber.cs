﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Rimed.TCD.DAL;

namespace Rimed.TCD.GUI
{

    // Added by Alex Feature #836 v.2.2.3.24 26/03/2016
    public partial class EditAccessionNumber : Form
    {
        public EditAccessionNumber()
        {
            InitializeComponent();
        }

        public string Current
        {
            get { return textBoxAccessionNumber.Text; }
            set { textBoxAccessionNumber.Text = value; }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
