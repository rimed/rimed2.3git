﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;
using Rimed.Framework.Common;
using Rimed.TCD.DAL;
using Rimed.TCD.Utils;

namespace Rimed.TCD.GUI
{
	public static class PacsManager
	{
		private const int PACS_MAX_PENDING_ITEMS						= 3;
		private const int PACS_MAX_PENDING_TIMEOUT_SEC					= 30;
		private const int PACS_DISCONNECTED_NOTIFICATION_INTERVAL_SEC	= 30;
		private const int PACS_DISCONNECTED_TIMEOUT_SEC					= 60;

		private const string PACS_LOG_PREFIX							= "PACS Server:";

		private static int s_queuedPacsImages;
		private static DateTime s_nextUserMsg = DateTime.UtcNow.AddSeconds(-PACS_DISCONNECTED_NOTIFICATION_INTERVAL_SEC);


		private class SendImageParameters
		{
			public SendImageParameters(Image img, Guid patientIndex, Guid examIndex	, double delayMSec = 0.0)
			{
				Img				= img;
				PatientIndex	= patientIndex;
				ExamIndex		= examIndex;
				RequestId		= DateTime.UtcNow.Ticks.ToString("X16");
				//SendTime		= DateTime.UtcNow.AddMilliseconds(delayMSec);
			}

			public Image	Img { get; private set; }
			public Guid		PatientIndex { get; private set; }
			public Guid		ExamIndex { get; private set; }
			public string	RequestId { get; private set; }
			//public DateTime SendTime{ get; private set; }

			public string   ImageFile;
			public string	PatientId;
			public string	PatientName;
			public string	PatientSex;
			public DateTime PatientBirthDate;
			public string	PatientRefferedBy;
			public string	PatientAccessNo;
			public DateTime ExamDate;
			public string	DicomStudyUid;

		}

		private static SendImageParameters prepareParameters(object state)
		{
			    var param = state as SendImageParameters;
			if (param == null || param.Img == null)
			{
				Logger.LogWarning("sendImageToPacsServerDelegate() ABORTED: param == null or param.Img == null.");
				return null;
			}

			var patient = RimedDal.Instance.GetPatientByIndex(param.PatientIndex);
			if (patient == null)
			{
				Logger.LogWarning("sendImageToPacsServerDelegate() ABORTED: Fail to find patient record ({0}).", param.PatientIndex);
				param.Img.Dispose();
				return null;
			}

			param.PatientName = string.Format("{0} {1}", patient.Last_Name, patient.First_Name);
			param.PatientId = patient.ID;
			param.PatientSex = patient.Sex;
			param.PatientBirthDate = patient.Birth_date;
			param.PatientRefferedBy = patient.RefferedBy;

            //param.PatientAccessNo = patient.AccessionNumber;
            //Changed by Alex Get Acession number just from MainForm v 2.2.3.26 03/04/2016 
            param.PatientAccessNo = MainForm.Instance.CurrentAccessionNumber;


			var examination = RimedDal.Instance.GetExaminationByIndex(param.ExamIndex);
			if (examination == null)
			{
				Logger.LogWarning("sendImageToPacsServerDelegate() ABORTED: Fail to find patient examination record ({0}).", param.ExamIndex);
				param.Img.Dispose();
				return null;
			}

            // Added by Alex Feature #836 v.2.2.3.24 22/03/2016
		    if (examination.AccessionNumber == "")
		    {
		        param.PatientAccessNo = MainForm.Instance.CurrentAccessionNumber;
		        examination.AccessionNumber = param.PatientAccessNo;
		        RimedDal.Instance.UpdateExamination(examination, "AccessionNumber", param.PatientAccessNo);
		    }
		    else
		        param.PatientAccessNo = examination.AccessionNumber;

		    //If DicomStudyUid is empty create new uid and assign it to the examination
			if (string.IsNullOrWhiteSpace(examination.DicomStudyUid)) 
			{
				examination.DicomStudyUid = DicomExporter.GetNewStudyUid();
				RimedDal.Instance.UpdateExamination();
			}

			param.ExamDate		= examination.Date;
			param.DicomStudyUid = examination.DicomStudyUid;

			var path = Path.Combine(Constants.DICOM_FILES_PATH, param.PatientIndex.ToString());
			if (!Directory.Exists(path))
				Directory.CreateDirectory(path);

			param.ImageFile = string.Format("{0}\\{1}.JPG", path, param.RequestId);

			var isSaved = Files.ImageSave(param.Img, param.ImageFile);
            // Bmp image
            //var isSaved = Files.ImageSave(param.Img, param.ImageFile, ImageFormat.Bmp);
			param.Img.Dispose();
			if (!isSaved)
			{
				Logger.LogWarning("sendImageToPacsServerDelegate() ABORTED: Fail to save capture image.");
				return null;
			}

			return param;
		}

		private static void sendImageToPacsServerDelegate(object state)
		{
			SendImageParameters param = null;
			try
			{
				param = prepareParameters(state);
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);
				param = null;			//Force method exit and counter decrement
			}

			if (param == null)
			{
				Interlocked.Decrement(ref s_queuedPacsImages);

				return;
			}

			var aeTitle = string.Empty;
			try
			{
				DicomExporter.SetStudyUid(param.DicomStudyUid, MainForm.Instance.CurrentStudyName);

				aeTitle = RimedDal.Instance.ConfigDS.tb_HospitalDetails[0].AE_Title;
				var localAeTitle = RimedDal.Instance.ConfigDS.tb_HospitalDetails[0].localAE_Title;
				var ipAddress = RimedDal.Instance.ConfigDS.tb_HospitalDetails[0].IP_Address;
				var port = RimedDal.Instance.ConfigDS.tb_HospitalDetails[0].Port_Number;
			    using (var dicom = new DicomExporter(localAeTitle, aeTitle, ipAddress, port))
			    {
			        var logFileName = String.Format(@"{0}\Rimed.TCD.Rev6.ExportToDicom.{1}.LOG", Constants.APP_LOGS_PATH,
			            param.RequestId);
			        if (dicom.ConvertAndSend(param.ImageFile, param.PatientId, param.PatientName, param.PatientSex,
			            param.PatientBirthDate, param.PatientRefferedBy, param.ExamDate, param.PatientAccessNo, logFileName))
			        {
			            Logger.LogInfo(PACS_LOG_PREFIX + " Request #{0} sent successfully.", param.RequestId);
			            //Added by Alex bug #666 v.2.2.2.12 27/01/2015
			            EnableButtons(true);
			        }
			    }
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);

				disableButtonsForPeriod(PACS_DISCONNECTED_TIMEOUT_SEC);					
				
				if (DateTime.UtcNow.Subtract(s_nextUserMsg).TotalSeconds > PACS_DISCONNECTED_NOTIFICATION_INTERVAL_SEC)
				{
					s_nextUserMsg = DateTime.UtcNow.AddSeconds(PACS_DISCONNECTED_NOTIFICATION_INTERVAL_SEC);
					var msg = string.Format("Connection to {0} PACS server is invalid.\nDICOM tool-bar button will be disabled for the next {1} seconds .\n\nPlease check connection settings and verify communication.", aeTitle, PACS_DISCONNECTED_TIMEOUT_SEC);
					LoggedDialog.ShowWarning(msg);

					s_nextUserMsg = DateTime.UtcNow.AddSeconds(PACS_DISCONNECTED_NOTIFICATION_INTERVAL_SEC);
				}
			}
			finally
			{
				// Delete temporary dicom files
				if (!AppSettings.Active.PACSRetainSentDicom)
				{
					var tempDicomPath = Path.Combine(Constants.DICOM_FILES_PATH, param.PatientIndex.ToString());
					var dcmFiles = Directory.GetFiles(tempDicomPath, "*" + DicomExporter.DICOM_FILE_EXT);
					foreach (var dcmFile in dcmFiles)
					{
						Files.Delete(dcmFile);
					}
				}

				// Delete temporary image files
				if (!AppSettings.Active.PACSRetainSentImage)
					Files.Delete(param.ImageFile);

				Interlocked.Decrement(ref s_queuedPacsImages);
			}
		}

        public static string WorkListIP { get; set; }
        public static string WorkListPort { get; set; }
        public static string WorkListLAE { get; set; }
        public static string WorkListRAE { get; set; }

        
	    private static List<PatientDICOMInfo> getWorkListFromPacsServerDelegate(string dateRange, string modality, string stantion)
        {
            var aeTitle = string.Empty;
            try
            {
                //aeTitle = RimedDal.Instance.ConfigDS.tb_HospitalDetails[0].AE_Title;
                //var localAeTitle = RimedDal.Instance.ConfigDS.tb_HospitalDetails[0].localAE_Title;
                //var ipAddress = RimedDal.Instance.ConfigDS.tb_HospitalDetails[0].IP_Address;
                //short port = RimedDal.Instance.ConfigDS.tb_HospitalDetails[0].Port_Number;
                aeTitle = WorkListRAE;
                var localAeTitle = WorkListLAE;
                var ipAddress = WorkListIP;
                short port = Convert.ToInt16(WorkListPort);
                using (var dicom = new DicomWorkList(localAeTitle, aeTitle, ipAddress, port))
                {
                    dicom.DateRange = dateRange;
                    dicom.Modality = modality;
                    dicom.StationName = stantion;
                    List<PatientDICOMInfo> result = dicom.BuildQuery();
                    if (result != null)
                    {
                        Logger.LogInfo(PACS_LOG_PREFIX + " WorkList #{0} recieved successfully.", null);
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                if (DateTime.UtcNow.Subtract(s_nextUserMsg).TotalSeconds > PACS_DISCONNECTED_NOTIFICATION_INTERVAL_SEC)
                {
                    s_nextUserMsg = DateTime.UtcNow.AddSeconds(PACS_DISCONNECTED_NOTIFICATION_INTERVAL_SEC);
                    var msg = string.Format("Connection to {0} PACS server is invalid.\n\nPlease check connection settings and verify communication.", aeTitle, PACS_DISCONNECTED_TIMEOUT_SEC);
                    LoggedDialog.ShowWarning(msg);
                    s_nextUserMsg = DateTime.UtcNow.AddSeconds(PACS_DISCONNECTED_NOTIFICATION_INTERVAL_SEC);
                }
                return null;
            }
        }


		private static void disableButtonsForPeriod(int maxPendingTimeoutSec = PACS_MAX_PENDING_TIMEOUT_SEC)
		{
			try
			{
				if (maxPendingTimeoutSec != Timeout.Infinite)
					maxPendingTimeoutSec *= 1000;

				MainForm.ToolBar.DisableDicomForPeriod(maxPendingTimeoutSec);
				SummaryScreen.ToolBar.DisableDicomForPeriod(maxPendingTimeoutSec);
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);
			}
		}

		public static void SendImageToPacsServer(Image img, int maxPendingTimeoutSec = PACS_MAX_PENDING_TIMEOUT_SEC)
		{
			if (img == null)
				return;

			if (s_queuedPacsImages >= PACS_MAX_PENDING_ITEMS)
			{
				disableButtonsForPeriod(maxPendingTimeoutSec);
				LoggedDialog.ShowWarning(string.Format("You have reached PACS server max. pending images ({0}).\nDICOM button disabled for the next {1} seconds.", s_queuedPacsImages, maxPendingTimeoutSec));

				return;
			}
			Interlocked.Increment(ref s_queuedPacsImages);

			var param = new SendImageParameters(new Bitmap(img), MainForm.Instance.SelectedPatientIndex, MainForm.Instance.SelectedPatientExaminationIndex);
			Logger.LogInfo(PACS_LOG_PREFIX +" Request #{0} added to PACS send queue.", param.RequestId);

            //Deleted by Alex  v 2.2.3.36 18/05/2016 
		    //img.Save("c:\\dicom.jpg");

			ThreadPool.QueueUserWorkItem(sendImageToPacsServerDelegate, param);
		}

        public static List<PatientDICOMInfo> GetDICOMWorkList(string modality, string stantion, string dateRange = null, int maxPendingTimeoutSec = PACS_MAX_PENDING_TIMEOUT_SEC)
        {
            Interlocked.Increment(ref s_queuedPacsImages);
            return getWorkListFromPacsServerDelegate(dateRange, modality, stantion);
        }

		public static void EnableButtons(bool isEnabled)
		{
			try
			{
                //Changed by Alex bug #822 v.2.2.3.19 03/01/2016
                MainForm.ToolBar.ButtonDicom.Enabled = isEnabled && Rimed.TCD.DAL.RimedDal.Instance.AuthorizationMgrDS.tb_AuthorizationMgr[0].Dicom;
                SummaryScreen.ToolBar.ButtonDicom.Enabled = isEnabled && Rimed.TCD.DAL.RimedDal.Instance.AuthorizationMgrDS.tb_AuthorizationMgr[0].Dicom;
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);
			}
		}
	}
}
