namespace Rimed.TCD.GUI
{
    partial class TrendChart
    {
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TrendChart));
			this.contextMenuTrend = new System.Windows.Forms.ContextMenu();
			this.menuItemChangeTimeDisplay = new System.Windows.Forms.MenuItem();
			this.menuItem4Min = new System.Windows.Forms.MenuItem();
			this.menuItem30Min = new System.Windows.Forms.MenuItem();
			this.menuItem60Min = new System.Windows.Forms.MenuItem();
			this.menuItem4 = new System.Windows.Forms.MenuItem();
			this.menuItemClose = new System.Windows.Forms.MenuItem();
			this.menuItem16 = new System.Windows.Forms.MenuItem();
			this.trendMCLabel = new System.Windows.Forms.Label();
			this.CaptionLabel = new System.Windows.Forms.Label();
			this.pictureBoxTriangle = new System.Windows.Forms.PictureBox();
			this.trendGraph1 = new Rimed.TCD.GUI.TrendGraph();
			this.trendChannelsList1 = new Rimed.TCD.GUI.TrendChannelsList();
			this.verticalTextCtrl1 = new Rimed.TCD.GUI.OrientedTextLabel();
			this.spectrumYAxis1 = new Rimed.TCD.GUI.SpectrumYAxis();
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxTriangle)).BeginInit();
			this.SuspendLayout();
			// 
			// contextMenuTrend
			// 
			this.contextMenuTrend.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemChangeTimeDisplay,
            this.menuItem4,
            this.menuItemClose});
			// 
			// menuItemChangeTimeDisplay
			// 
			this.menuItemChangeTimeDisplay.Index = 0;
			this.menuItemChangeTimeDisplay.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem4Min,
            this.menuItem30Min,
            this.menuItem60Min});
			this.menuItemChangeTimeDisplay.Text = "";
			// 
			// menuItem4Min
			// 
			this.menuItem4Min.Checked = true;
			this.menuItem4Min.Index = 0;
			this.menuItem4Min.RadioCheck = true;
			this.menuItem4Min.Text = "";
			this.menuItem4Min.Click += new System.EventHandler(this.menuItemTimeDisplay_Click);
			// 
			// menuItem30Min
			// 
			this.menuItem30Min.Index = 1;
			this.menuItem30Min.Text = "";
			this.menuItem30Min.Click += new System.EventHandler(this.menuItemTimeDisplay_Click);
			// 
			// menuItem60Min
			// 
			this.menuItem60Min.Index = 2;
			this.menuItem60Min.RadioCheck = true;
			this.menuItem60Min.Text = "";
			this.menuItem60Min.Click += new System.EventHandler(this.menuItemTimeDisplay_Click);
			// 
			// menuItem4
			// 
			this.menuItem4.Index = 1;
			this.menuItem4.Text = "-";
			// 
			// menuItemClose
			// 
			this.menuItemClose.Index = 2;
			this.menuItemClose.Text = "";
			this.menuItemClose.Click += new System.EventHandler(this.menuItemClose_Click);
			// 
			// menuItem16
			// 
			this.menuItem16.Index = -1;
			this.menuItem16.Text = "";
			// 
			// trendMCLabel
			// 
			this.trendMCLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
			this.trendMCLabel.Location = new System.Drawing.Point(277, 0);
			this.trendMCLabel.Name = "trendMCLabel";
			this.trendMCLabel.Size = new System.Drawing.Size(160, 24);
			this.trendMCLabel.TabIndex = 19;
			this.trendMCLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// CaptionLabel
			// 
			this.CaptionLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.CaptionLabel.ContextMenu = this.contextMenuTrend;
			this.CaptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
			this.CaptionLabel.Location = new System.Drawing.Point(4, 0);
			this.CaptionLabel.Name = "CaptionLabel";
			this.CaptionLabel.Size = new System.Drawing.Size(777, 24);
			this.CaptionLabel.TabIndex = 5;
			this.CaptionLabel.Text = "label1";
			this.CaptionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// pictureBoxTriangle
			// 
			this.pictureBoxTriangle.BackColor = System.Drawing.Color.Transparent;
			this.pictureBoxTriangle.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBoxTriangle.BackgroundImage")));
			this.pictureBoxTriangle.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.pictureBoxTriangle.Location = new System.Drawing.Point(16, 103);
			this.pictureBoxTriangle.Name = "pictureBoxTriangle";
			this.pictureBoxTriangle.Size = new System.Drawing.Size(13, 16);
			this.pictureBoxTriangle.TabIndex = 7;
			this.pictureBoxTriangle.TabStop = false;
			// 
			// trendGraph1
			// 
			this.trendGraph1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.trendGraph1.BackColor = System.Drawing.SystemColors.InactiveCaption;
			this.trendGraph1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.trendGraph1.ContextMenu = this.contextMenuTrend;
			this.trendGraph1.CursorsMode = false;
			this.trendGraph1.ForeColor = System.Drawing.SystemColors.InactiveCaption;
			this.trendGraph1.LastLeftPositionPainted = 0D;
			this.trendGraph1.Location = new System.Drawing.Point(47, 27);
			this.trendGraph1.Name = "trendGraph1";
			this.trendGraph1.Size = new System.Drawing.Size(628, 343);
			this.trendGraph1.TabIndex = 1;
			// 
			// trendChannelsList1
			// 
			this.trendChannelsList1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.trendChannelsList1.BackColor = System.Drawing.Color.White;
			this.trendChannelsList1.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.trendChannelsList1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
			this.trendChannelsList1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
			this.trendChannelsList1.IntegralHeight = false;
			this.trendChannelsList1.ItemHeight = 50;
			this.trendChannelsList1.Location = new System.Drawing.Point(681, 24);
			this.trendChannelsList1.Name = "trendChannelsList1";
			this.trendChannelsList1.Size = new System.Drawing.Size(100, 340);
			this.trendChannelsList1.TabIndex = 6;
			// 
			// verticalTextCtrl1
			// 
			this.verticalTextCtrl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(190)))), ((int)(((byte)(230)))));
			this.verticalTextCtrl1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.verticalTextCtrl1.ForeColor = System.Drawing.Color.Red;
			this.verticalTextCtrl1.Location = new System.Drawing.Point(12, 144);
			this.verticalTextCtrl1.Name = "verticalTextCtrl1";
			this.verticalTextCtrl1.RotationAngle = 270D;
			this.verticalTextCtrl1.Size = new System.Drawing.Size(16, 56);
			this.verticalTextCtrl1.TabIndex = 4;
			this.verticalTextCtrl1.Text = null;
			this.verticalTextCtrl1.TextDirection = Rimed.TCD.GUI.Direction.Clockwise;
			this.verticalTextCtrl1.TextOrientation = Rimed.TCD.GUI.Orientation.Rotate;
			this.verticalTextCtrl1.Visible = false;
			// 
			// spectrumYAxis1
			// 
			this.spectrumYAxis1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.spectrumYAxis1.BackColor = System.Drawing.SystemColors.Control;
			this.spectrumYAxis1.ContextMenu = this.contextMenuTrend;
			this.spectrumYAxis1.Location = new System.Drawing.Point(4, 27);
			this.spectrumYAxis1.MaxValue = 250D;
			this.spectrumYAxis1.MinValue = 20D;
			this.spectrumYAxis1.Name = "spectrumYAxis1";
			this.spectrumYAxis1.Size = new System.Drawing.Size(37, 354);
			this.spectrumYAxis1.TabIndex = 2;
			// 
			// TrendChart
			// 
			this.ContextMenu = this.contextMenuTrend;
			this.Controls.Add(this.trendMCLabel);
			this.Controls.Add(this.pictureBoxTriangle);
			this.Controls.Add(this.trendGraph1);
			this.Controls.Add(this.trendChannelsList1);
			this.Controls.Add(this.verticalTextCtrl1);
			this.Controls.Add(this.spectrumYAxis1);
			this.Controls.Add(this.CaptionLabel);
			this.Name = "TrendChart";
			this.Size = new System.Drawing.Size(784, 384);
			this.SizeChanged += new System.EventHandler(this.TrendChart_SizeChanged);
			((System.ComponentModel.ISupportInitialize)(this.pictureBoxTriangle)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenu contextMenuTrend;
        private System.Windows.Forms.MenuItem menuItem16;
        private System.Windows.Forms.Label CaptionLabel;
        private System.ComponentModel.Container components = null;
        private TrendGraph trendGraph1;
        private SpectrumYAxis spectrumYAxis1;
        private OrientedTextLabel verticalTextCtrl1;
        private TrendChannelsList trendChannelsList1;
        private System.Windows.Forms.MenuItem menuItem4;
        private System.Windows.Forms.MenuItem menuItemChangeTimeDisplay;
        private System.Windows.Forms.MenuItem menuItemClose;
        private System.Windows.Forms.MenuItem menuItem4Min;
        private System.Windows.Forms.MenuItem menuItem30Min;
        private System.Windows.Forms.MenuItem menuItem60Min;
        private System.Windows.Forms.PictureBox pictureBoxTriangle;
		private System.Windows.Forms.Label trendMCLabel;
    }
}