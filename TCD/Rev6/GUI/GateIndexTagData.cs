using System;

namespace Rimed.TCD.GUI
{
	/// <summary>
	/// Summary description for GateIndexTagData.
	/// </summary>
	public class GateIndexTagData
	{
		public Guid GateIndex = Guid.Empty;
	    private Guid SubExamnIndex = Guid.Empty;
		public Guid BVExamIndex = Guid.Empty;
		public Guid ExamIndex = Guid.Empty;

        public GateIndexTagData(Guid gate,Guid subExam,Guid bvExamIndex,Guid examIndex)
		{
			this.GateIndex = gate;
			this.SubExamnIndex = subExam;
			this.BVExamIndex = bvExamIndex;
			this.ExamIndex = examIndex;
		}
	}
}
