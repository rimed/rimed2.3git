﻿namespace Rimed.TCD.GUI
{
    partial class AuthorizationMgrDlg
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AuthorizationMgrDlg));
            this.lblProductKey = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.labelSperate6 = new System.Windows.Forms.Label();
            this.labelSperate5 = new System.Windows.Forms.Label();
            this.labelSperate4 = new System.Windows.Forms.Label();
            this.labelSperate3 = new System.Windows.Forms.Label();
            this.labelSperate2 = new System.Windows.Forms.Label();
            this.labelSperate1 = new System.Windows.Forms.Label();
            this.lblSystemKey = new System.Windows.Forms.Label();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonOK = new System.Windows.Forms.Button();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.labelHD = new System.Windows.Forms.Label();
            this.textBoxSerialNumber = new System.Windows.Forms.TextBox();
            this.btnPasteSystemKey = new System.Windows.Forms.PictureBox();
            this.btnPasteProductKey = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.btnPasteSystemKey)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPasteProductKey)).BeginInit();
            this.SuspendLayout();
            // 
            // lblProductKey
            // 
            this.lblProductKey.AutoSize = true;
            this.lblProductKey.Location = new System.Drawing.Point(24, 72);
            this.lblProductKey.Name = "lblProductKey";
            this.lblProductKey.Size = new System.Drawing.Size(68, 13);
            this.lblProductKey.TabIndex = 35;
            this.lblProductKey.Text = "Product Key:";
            this.lblProductKey.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(24, 40);
            this.textBox6.MaxLength = 8;
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(88, 20);
            this.textBox6.TabIndex = 32;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(120, 40);
            this.textBox7.MaxLength = 4;
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(48, 20);
            this.textBox7.TabIndex = 33;
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(232, 40);
            this.textBox9.MaxLength = 4;
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(48, 20);
            this.textBox9.TabIndex = 30;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(176, 40);
            this.textBox8.MaxLength = 4;
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(48, 20);
            this.textBox8.TabIndex = 31;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(24, 96);
            this.textBox1.MaxLength = 8;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(88, 20);
            this.textBox1.TabIndex = 21;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(120, 96);
            this.textBox2.MaxLength = 4;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(48, 20);
            this.textBox2.TabIndex = 20;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(232, 96);
            this.textBox4.MaxLength = 4;
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(48, 20);
            this.textBox4.TabIndex = 23;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(176, 96);
            this.textBox3.MaxLength = 4;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(48, 20);
            this.textBox3.TabIndex = 22;
            // 
            // labelSperate6
            // 
            this.labelSperate6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSperate6.Location = new System.Drawing.Point(224, 40);
            this.labelSperate6.Name = "labelSperate6";
            this.labelSperate6.Size = new System.Drawing.Size(8, 16);
            this.labelSperate6.TabIndex = 29;
            this.labelSperate6.Text = "-";
            // 
            // labelSperate5
            // 
            this.labelSperate5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSperate5.Location = new System.Drawing.Point(168, 40);
            this.labelSperate5.Name = "labelSperate5";
            this.labelSperate5.Size = new System.Drawing.Size(8, 16);
            this.labelSperate5.TabIndex = 28;
            this.labelSperate5.Text = "-";
            // 
            // labelSperate4
            // 
            this.labelSperate4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSperate4.Location = new System.Drawing.Point(112, 40);
            this.labelSperate4.Name = "labelSperate4";
            this.labelSperate4.Size = new System.Drawing.Size(8, 16);
            this.labelSperate4.TabIndex = 27;
            this.labelSperate4.Text = "-";
            // 
            // labelSperate3
            // 
            this.labelSperate3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSperate3.Location = new System.Drawing.Point(224, 96);
            this.labelSperate3.Name = "labelSperate3";
            this.labelSperate3.Size = new System.Drawing.Size(8, 16);
            this.labelSperate3.TabIndex = 26;
            this.labelSperate3.Text = "-";
            // 
            // labelSperate2
            // 
            this.labelSperate2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSperate2.Location = new System.Drawing.Point(168, 96);
            this.labelSperate2.Name = "labelSperate2";
            this.labelSperate2.Size = new System.Drawing.Size(8, 16);
            this.labelSperate2.TabIndex = 25;
            this.labelSperate2.Text = "-";
            // 
            // labelSperate1
            // 
            this.labelSperate1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSperate1.Location = new System.Drawing.Point(112, 96);
            this.labelSperate1.Name = "labelSperate1";
            this.labelSperate1.Size = new System.Drawing.Size(8, 16);
            this.labelSperate1.TabIndex = 24;
            this.labelSperate1.Text = "-";
            // 
            // lblSystemKey
            // 
            this.lblSystemKey.AutoSize = true;
            this.lblSystemKey.Location = new System.Drawing.Point(24, 16);
            this.lblSystemKey.Name = "lblSystemKey";
            this.lblSystemKey.Size = new System.Drawing.Size(65, 13);
            this.lblSystemKey.TabIndex = 34;
            this.lblSystemKey.Text = "System Key:";
            this.lblSystemKey.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.buttonCancel.Location = new System.Drawing.Point(304, 184);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 37;
            this.buttonCancel.Text = "Close";
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // buttonOK
            // 
            this.buttonOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.buttonOK.Location = new System.Drawing.Point(24, 184);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 36;
            this.buttonOK.Text = "Apply";
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(288, 40);
            this.textBox10.MaxLength = 12;
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(88, 20);
            this.textBox10.TabIndex = 30;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(280, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(8, 16);
            this.label3.TabIndex = 38;
            this.label3.Text = "-";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(288, 96);
            this.textBox5.MaxLength = 12;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(88, 20);
            this.textBox5.TabIndex = 30;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(280, 96);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(8, 16);
            this.label4.TabIndex = 38;
            this.label4.Text = "-";
            // 
            // labelHD
            // 
            this.labelHD.AutoSize = true;
            this.labelHD.Location = new System.Drawing.Point(24, 128);
            this.labelHD.Name = "labelHD";
            this.labelHD.Size = new System.Drawing.Size(95, 13);
            this.labelHD.TabIndex = 40;
            this.labelHD.Text = "HD Serial Number:";
            // 
            // textBoxSerialNumber
            // 
            this.textBoxSerialNumber.Location = new System.Drawing.Point(24, 144);
            this.textBoxSerialNumber.Name = "textBoxSerialNumber";
            this.textBoxSerialNumber.Size = new System.Drawing.Size(352, 20);
            this.textBoxSerialNumber.TabIndex = 63;
            // 
            // btnPasteSystemKey
            // 
            this.btnPasteSystemKey.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPasteSystemKey.BackgroundImage")));
            this.btnPasteSystemKey.Location = new System.Drawing.Point(390, 35);
            this.btnPasteSystemKey.Name = "btnPasteSystemKey";
            this.btnPasteSystemKey.Size = new System.Drawing.Size(32, 32);
            this.btnPasteSystemKey.TabIndex = 64;
            this.btnPasteSystemKey.TabStop = false;
            this.btnPasteSystemKey.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnPasteSystemKey_MouseDown);
            this.btnPasteSystemKey.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnPasteSystemKey_MouseUp);
            // 
            // btnPasteProductKey
            // 
            this.btnPasteProductKey.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnPasteProductKey.BackgroundImage")));
            this.btnPasteProductKey.Location = new System.Drawing.Point(389, 90);
            this.btnPasteProductKey.Name = "btnPasteProductKey";
            this.btnPasteProductKey.Size = new System.Drawing.Size(32, 32);
            this.btnPasteProductKey.TabIndex = 65;
            this.btnPasteProductKey.TabStop = false;
            this.btnPasteProductKey.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnPasteProductKey_MouseDown);
            this.btnPasteProductKey.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnPasteProductKey_MouseUp);
            // 
            // AuthorizationMgrDlg
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(434, 223);
            this.Controls.Add(this.btnPasteProductKey);
            this.Controls.Add(this.btnPasteSystemKey);
            this.Controls.Add(this.textBoxSerialNumber);
            this.Controls.Add(this.labelHD);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.textBox7);
            this.Controls.Add(this.textBox9);
            this.Controls.Add(this.textBox8);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox10);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.Controls.Add(this.lblProductKey);
            this.Controls.Add(this.labelSperate6);
            this.Controls.Add(this.labelSperate5);
            this.Controls.Add(this.labelSperate4);
            this.Controls.Add(this.labelSperate3);
            this.Controls.Add(this.labelSperate2);
            this.Controls.Add(this.labelSperate1);
            this.Controls.Add(this.lblSystemKey);
            this.Controls.Add(this.label4);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AuthorizationMgrDlg";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Authorization Manager";
            this.Load += new System.EventHandler(this.AuthorizationMgrDlg_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.AuthorizationMgrDlg_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.btnPasteSystemKey)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnPasteProductKey)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        
        #endregion

        private System.Windows.Forms.Label lblProductKey;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label labelSperate6;
        private System.Windows.Forms.Label labelSperate5;
        private System.Windows.Forms.Label labelSperate4;
        private System.Windows.Forms.Label labelSperate3;
        private System.Windows.Forms.Label labelSperate2;
        private System.Windows.Forms.Label labelSperate1;
        private System.Windows.Forms.Label lblSystemKey;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelHD;
        private System.Windows.Forms.TextBox textBoxSerialNumber;
        private System.Windows.Forms.PictureBox btnPasteSystemKey;
        private System.Windows.Forms.PictureBox btnPasteProductKey;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
    }
}