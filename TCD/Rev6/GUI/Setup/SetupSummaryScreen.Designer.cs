namespace Rimed.TCD.GUI.Setup
{
    partial class SetupSummaryScreen
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbCaption = new System.Windows.Forms.Label();
            this.radioButtonAddAllOpenSpectrums = new System.Windows.Forms.RadioButton();
            this.radioButtonAddOnlyselectedSpectrum = new System.Windows.Forms.RadioButton();
            this.radioButtonCustomizeMultipleTestsOnSameBVAndDepth = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButtonReplaceOld = new System.Windows.Forms.RadioButton();
            this.radioButtonAddNew = new System.Windows.Forms.RadioButton();
            this.radioButtonAskForEachTest = new System.Windows.Forms.RadioButton();
            this.dsConfiguation1 = new TCD.DAL.dsConfiguation();
            this.checkSaveOnlyImages = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dsConfiguation1)).BeginInit();
            this.SuspendLayout();
            // 
            // lbCaption
            // 
            this.lbCaption.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lbCaption.Location = new System.Drawing.Point(29, 16);
            this.lbCaption.Name = "lbCaption";
            this.lbCaption.Size = new System.Drawing.Size(400, 23);
            this.lbCaption.TabIndex = 1;
            this.lbCaption.Text = "Summary Screen Setup";
            this.lbCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radioButtonAddAllOpenSpectrums
            // 
            this.radioButtonAddAllOpenSpectrums.Location = new System.Drawing.Point(40, 48);
            this.radioButtonAddAllOpenSpectrums.Name = "radioButtonAddAllOpenSpectrums";
            this.radioButtonAddAllOpenSpectrums.Size = new System.Drawing.Size(389, 32);
            this.radioButtonAddAllOpenSpectrums.TabIndex = 2;
            this.radioButtonAddAllOpenSpectrums.Text = "Add all open spectrums";
            // 
            // radioButtonAddOnlyselectedSpectrum
            // 
            this.radioButtonAddOnlyselectedSpectrum.Location = new System.Drawing.Point(40, 96);
            this.radioButtonAddOnlyselectedSpectrum.Name = "radioButtonAddOnlyselectedSpectrum";
            this.radioButtonAddOnlyselectedSpectrum.Size = new System.Drawing.Size(389, 32);
            this.radioButtonAddOnlyselectedSpectrum.TabIndex = 2;
            this.radioButtonAddOnlyselectedSpectrum.Text = "Add only selected spectrum";
            // 
            // radioButtonCustomizeMultipleTestsOnSameBVAndDepth
            // 
            this.radioButtonCustomizeMultipleTestsOnSameBVAndDepth.Location = new System.Drawing.Point(40, 136);
            this.radioButtonCustomizeMultipleTestsOnSameBVAndDepth.Name = "radioButtonCustomizeMultipleTestsOnSameBVAndDepth";
            this.radioButtonCustomizeMultipleTestsOnSameBVAndDepth.Size = new System.Drawing.Size(389, 32);
            this.radioButtonCustomizeMultipleTestsOnSameBVAndDepth.TabIndex = 2;
            this.radioButtonCustomizeMultipleTestsOnSameBVAndDepth.Text = "Customize multiple tests on same BV and depth";
            this.radioButtonCustomizeMultipleTestsOnSameBVAndDepth.CheckedChanged += new System.EventHandler(this.radioButtonCustomizeMultipleTestsOnSameBVAndDepth_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButtonReplaceOld);
            this.groupBox1.Controls.Add(this.radioButtonAddNew);
            this.groupBox1.Controls.Add(this.radioButtonAskForEachTest);
            this.groupBox1.Location = new System.Drawing.Point(32, 144);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(304, 152);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // radioButtonReplaceOld
            // 
            this.radioButtonReplaceOld.Location = new System.Drawing.Point(32, 24);
            this.radioButtonReplaceOld.Name = "radioButtonReplaceOld";
            this.radioButtonReplaceOld.Size = new System.Drawing.Size(266, 32);
            this.radioButtonReplaceOld.TabIndex = 3;
            this.radioButtonReplaceOld.Text = "Replace old";
            // 
            // radioButtonAddNew
            // 
            this.radioButtonAddNew.Location = new System.Drawing.Point(32, 64);
            this.radioButtonAddNew.Name = "radioButtonAddNew";
            this.radioButtonAddNew.Size = new System.Drawing.Size(266, 32);
            this.radioButtonAddNew.TabIndex = 3;
            this.radioButtonAddNew.Text = "Add new";
            // 
            // radioButtonAskForEachTest
            // 
            this.radioButtonAskForEachTest.Location = new System.Drawing.Point(32, 112);
            this.radioButtonAskForEachTest.Name = "radioButtonAskForEachTest";
            this.radioButtonAskForEachTest.Size = new System.Drawing.Size(266, 32);
            this.radioButtonAskForEachTest.TabIndex = 3;
            this.radioButtonAskForEachTest.Text = "Ask for each test";
            // 
            // dsConfiguation1
            // 
            this.dsConfiguation1.DataSetName = "dsConfiguation";
            this.dsConfiguation1.Locale = new System.Globalization.CultureInfo("en-US");
            this.dsConfiguation1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // checkSaveOnlyImages
            // 
            this.checkSaveOnlyImages.Location = new System.Drawing.Point(40, 302);
            this.checkSaveOnlyImages.Name = "checkSaveOnlyImages";
            this.checkSaveOnlyImages.Size = new System.Drawing.Size(356, 39);
            this.checkSaveOnlyImages.TabIndex = 4;
            this.checkSaveOnlyImages.Text = "Save only summary images (No replay Option)";
            // 
            // SetupSummaryScreen
            // 
            this.Controls.Add(this.checkSaveOnlyImages);
            this.Controls.Add(this.radioButtonAddAllOpenSpectrums);
            this.Controls.Add(this.radioButtonAddOnlyselectedSpectrum);
            this.Controls.Add(this.radioButtonCustomizeMultipleTestsOnSameBVAndDepth);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lbCaption);
            this.Name = "SetupSummaryScreen";
            this.Load += new System.EventHandler(this.SetupSummaryScreen_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dsConfiguation1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        
        private System.Windows.Forms.Label lbCaption;
        private System.Windows.Forms.RadioButton radioButtonAddAllOpenSpectrums;
        private System.Windows.Forms.RadioButton radioButtonAddOnlyselectedSpectrum;
        private System.Windows.Forms.RadioButton radioButtonCustomizeMultipleTestsOnSameBVAndDepth;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButtonAddNew;
        private System.Windows.Forms.RadioButton radioButtonAskForEachTest;
        private System.Windows.Forms.RadioButton radioButtonReplaceOld;
        private TCD.DAL.dsConfiguation dsConfiguation1;
        private System.Windows.Forms.CheckBox checkSaveOnlyImages;
        private System.ComponentModel.IContainer components = null;
    }
}