using System.Windows.Forms;

namespace Rimed.TCD.GUI.Setup
{
    partial class SetUpBloodVessels
    {
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                    components.Dispose();

	            components = null;
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetUpBloodVessels));
            this.chkDisplaySpectrum = new System.Windows.Forms.CheckBox();
            this.dsBVSetup1 = new TCD.DAL.dsBVSetup();
            this.gbDisplayEnvelopes = new System.Windows.Forms.GroupBox();
            this.checkBoxReverseMode = new System.Windows.Forms.CheckBox();
            this.checkBoxForwardMode = new System.Windows.Forms.CheckBox();
            this.checkBoxReversePeak = new System.Windows.Forms.CheckBox();
            this.checkBoxForwardPeak = new System.Windows.Forms.CheckBox();
            this.comboBoxPower = new System.Windows.Forms.ComboBox();
            this.numericUpDownExtDepth = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownExtWidth = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownExtGain = new System.Windows.Forms.NumericUpDown();
            this.lbPower = new System.Windows.Forms.Label();
            this.lbSampleVolume = new System.Windows.Forms.Label();
            this.lbDepth = new System.Windows.Forms.Label();
            this.lbFilter = new System.Windows.Forms.Label();
            this.comboBoxThump = new System.Windows.Forms.ComboBox();
            this.lbGain = new System.Windows.Forms.Label();
            this.albDirection = new System.Windows.Forms.Label();
            this.lbUnits = new System.Windows.Forms.Label();
            this.comboBoxUnits = new System.Windows.Forms.ComboBox();
            this.lbZeroLine = new System.Windows.Forms.Label();
            this.lbScale = new System.Windows.Forms.Label();
            this.comboBoxFreqRange = new System.Windows.Forms.ComboBox();
            this.lbProbe = new System.Windows.Forms.Label();
            this.comboBoxProbe = new System.Windows.Forms.ComboBox();
            this.lbCaption = new System.Windows.Forms.Label();
            this.gradientPanel1 = new System.Windows.Forms.Panel();
            this.gridDataBoundGrid1 = new Syncfusion.Windows.Forms.Grid.GridDataBoundGrid();
            this.xpToolBar1 = new System.Windows.Forms.ToolBar();
            this.barItemAdd = new System.Windows.Forms.ToolBarButton();
            this.barItemDelete = new System.Windows.Forms.ToolBarButton();
            this.imageListBV = new System.Windows.Forms.ImageList(this.components);
            this.gbHitsDetection = new System.Windows.Forms.GroupBox();
            this.autoLabel1 = new System.Windows.Forms.Label();
            this.textBoxHitsThreshold = new Syncfusion.Windows.Forms.Tools.IntegerTextBox();
            this.pictureBoxFromProbe = new System.Windows.Forms.PictureBox();
            this.pictureBoxToProbe = new System.Windows.Forms.PictureBox();
            this.lbAngle = new System.Windows.Forms.Label();
            this.numericUpDownAngle = new System.Windows.Forms.NumericUpDown();
            this.tmpDirection = new System.Windows.Forms.CheckBox();
            this.dsBVSetup2 = new TCD.DAL.dsBVSetup();
            this.numericUpDownZeroLine = new System.Windows.Forms.NumericUpDown();
            this.chkUseEnvelope = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dsBVSetup1)).BeginInit();
            this.gbDisplayEnvelopes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownExtDepth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownExtWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownExtGain)).BeginInit();
            this.gradientPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDataBoundGrid1)).BeginInit();
            this.gbHitsDetection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxHitsThreshold)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFromProbe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxToProbe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAngle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsBVSetup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZeroLine)).BeginInit();
            this.SuspendLayout();
            // 
            // chkDisplaySpectrum
            // 
            this.chkDisplaySpectrum.Checked = true;
            this.chkDisplaySpectrum.CheckState = System.Windows.Forms.CheckState.Checked;
			//this.chkDisplaySpectrum.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.dsBVSetup1, "tb_BloodVesselSetup.DisplaySecptrum", true, DataSourceUpdateMode.OnValidation));
            this.chkDisplaySpectrum.Location = new System.Drawing.Point(112, 195);
            this.chkDisplaySpectrum.Name = "chkDisplaySpectrum";
            this.chkDisplaySpectrum.Size = new System.Drawing.Size(128, 19);
            this.chkDisplaySpectrum.TabIndex = 71;
            this.chkDisplaySpectrum.Text = "Display Spectrum";
            // 
            // dsBVSetup1
            // 
            this.dsBVSetup1.DataSetName = "dsBVSetup";
            this.dsBVSetup1.Locale = new System.Globalization.CultureInfo("he-IL");
            this.dsBVSetup1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gbDisplayEnvelopes
            // 
            this.gbDisplayEnvelopes.Controls.Add(this.checkBoxReverseMode);
            this.gbDisplayEnvelopes.Controls.Add(this.checkBoxForwardMode);
            this.gbDisplayEnvelopes.Controls.Add(this.checkBoxReversePeak);
            this.gbDisplayEnvelopes.Controls.Add(this.checkBoxForwardPeak);
            this.gbDisplayEnvelopes.Location = new System.Drawing.Point(104, 264);
            this.gbDisplayEnvelopes.Name = "gbDisplayEnvelopes";
            this.gbDisplayEnvelopes.Size = new System.Drawing.Size(224, 72);
            this.gbDisplayEnvelopes.TabIndex = 70;
            this.gbDisplayEnvelopes.TabStop = false;
            this.gbDisplayEnvelopes.Text = "Display Envelopes";
            // 
            // checkBoxReverseMode
            // 
            this.checkBoxReverseMode.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			//this.checkBoxReverseMode.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.dsBVSetup1, "tb_BloodVesselSetup.DisplayMeanBckEnvelope", true, DataSourceUpdateMode.OnValidation));
            this.checkBoxReverseMode.Location = new System.Drawing.Point(114, 40);
            this.checkBoxReverseMode.Name = "checkBoxReverseMode";
            this.checkBoxReverseMode.Size = new System.Drawing.Size(100, 30);
            this.checkBoxReverseMode.TabIndex = 3;
            this.checkBoxReverseMode.Text = "Reverse Mode";
            // 
            // checkBoxForwardMode
            // 
            this.checkBoxForwardMode.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			//this.checkBoxForwardMode.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.dsBVSetup1, "tb_BloodVesselSetup.DisplayMeanFwdEnvelope", true, DataSourceUpdateMode.OnValidation));
            this.checkBoxForwardMode.Location = new System.Drawing.Point(10, 40);
            this.checkBoxForwardMode.Name = "checkBoxForwardMode";
            this.checkBoxForwardMode.Size = new System.Drawing.Size(96, 30);
            this.checkBoxForwardMode.TabIndex = 2;
            this.checkBoxForwardMode.Text = "Forward Mode";
            // 
            // checkBoxReversePeak
            // 
            this.checkBoxReversePeak.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			//this.checkBoxReversePeak.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.dsBVSetup1, "tb_BloodVesselSetup.DisplayPeakBckEnvelope", true, DataSourceUpdateMode.OnValidation));
            this.checkBoxReversePeak.Location = new System.Drawing.Point(114, 11);
            this.checkBoxReversePeak.Name = "checkBoxReversePeak";
            this.checkBoxReversePeak.Size = new System.Drawing.Size(96, 33);
            this.checkBoxReversePeak.TabIndex = 1;
            this.checkBoxReversePeak.Text = "Reverse Peak";
            // 
            // checkBoxForwardPeak
            // 
            this.checkBoxForwardPeak.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
			//this.checkBoxForwardPeak.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.dsBVSetup1, "tb_BloodVesselSetup.DisplayPeakFwdEnvelope", true, DataSourceUpdateMode.OnValidation));
            this.checkBoxForwardPeak.Location = new System.Drawing.Point(10, 11);
            this.checkBoxForwardPeak.Name = "checkBoxForwardPeak";
            this.checkBoxForwardPeak.Size = new System.Drawing.Size(96, 33);
            this.checkBoxForwardPeak.TabIndex = 0;
            this.checkBoxForwardPeak.Text = "Forward Peak";
            // 
            // comboBoxPower
            // 
            this.comboBoxPower.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxPower.Location = new System.Drawing.Point(173, 64);
            this.comboBoxPower.MaxDropDownItems = 16;
            this.comboBoxPower.Name = "comboBoxPower";
            this.comboBoxPower.Size = new System.Drawing.Size(101, 21);
            this.comboBoxPower.TabIndex = 69;
            // 
            // numericUpDownExtDepth
            // 
			//this.numericUpDownExtDepth.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.dsBVSetup1, "tb_BloodVesselSetup.Depth", true, DataSourceUpdateMode.OnValidation));
            this.numericUpDownExtDepth.Location = new System.Drawing.Point(360, 32);
            this.numericUpDownExtDepth.Name = "numericUpDownExtDepth";
            this.numericUpDownExtDepth.Size = new System.Drawing.Size(72, 20);
            this.numericUpDownExtDepth.TabIndex = 68;
            this.numericUpDownExtDepth.Leave += new System.EventHandler(this.numericUpDownExtDepth_Leave);
            // 
            // numericUpDownExtWidth
            // 
			//this.numericUpDownExtWidth.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.dsBVSetup1, "tb_BloodVesselSetup.Width", true, DataSourceUpdateMode.OnValidation));
            this.numericUpDownExtWidth.Location = new System.Drawing.Point(360, 96);
            this.numericUpDownExtWidth.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numericUpDownExtWidth.Name = "numericUpDownExtWidth";
            this.numericUpDownExtWidth.Size = new System.Drawing.Size(72, 20);
            this.numericUpDownExtWidth.TabIndex = 67;
            // 
            // numericUpDownExtGain
            // 
			//this.numericUpDownExtGain.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.dsBVSetup1, "tb_BloodVesselSetup.Gain", true, DataSourceUpdateMode.OnValidation));
            this.numericUpDownExtGain.Location = new System.Drawing.Point(173, 96);
            this.numericUpDownExtGain.Name = "numericUpDownExtGain";
            this.numericUpDownExtGain.Size = new System.Drawing.Size(101, 20);
            this.numericUpDownExtGain.TabIndex = 66;
            // 
            // lbPower
            // 
            this.lbPower.Location = new System.Drawing.Point(94, 64);
            this.lbPower.Name = "lbPower";
            this.lbPower.Size = new System.Drawing.Size(88, 24);
            this.lbPower.TabIndex = 65;
            this.lbPower.Text = "Power [%]:";
            this.lbPower.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbSampleVolume
            // 
            this.lbSampleVolume.Location = new System.Drawing.Point(280, 89);
            this.lbSampleVolume.Name = "lbSampleVolume";
            this.lbSampleVolume.Size = new System.Drawing.Size(80, 34);
            this.lbSampleVolume.TabIndex = 64;
            this.lbSampleVolume.Text = "Sample Volume [mm]:";
            this.lbSampleVolume.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbDepth
            // 
            this.lbDepth.Location = new System.Drawing.Point(280, 34);
            this.lbDepth.Name = "lbDepth";
            this.lbDepth.Size = new System.Drawing.Size(80, 16);
            this.lbDepth.TabIndex = 63;
            this.lbDepth.Text = "Depth [mm]:";
            this.lbDepth.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbFilter
            // 
            this.lbFilter.Location = new System.Drawing.Point(94, 133);
            this.lbFilter.Name = "lbFilter";
            this.lbFilter.Size = new System.Drawing.Size(73, 16);
            this.lbFilter.TabIndex = 62;
            this.lbFilter.Text = "Filter [Hz]:";
            this.lbFilter.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBoxThump
            // 
            this.comboBoxThump.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxThump.Location = new System.Drawing.Point(173, 129);
            this.comboBoxThump.Name = "comboBoxThump";
            this.comboBoxThump.Size = new System.Drawing.Size(101, 21);
            this.comboBoxThump.TabIndex = 61;
            // 
            // lbGain
            // 
            this.lbGain.Location = new System.Drawing.Point(94, 96);
            this.lbGain.Name = "lbGain";
            this.lbGain.Size = new System.Drawing.Size(88, 20);
            this.lbGain.TabIndex = 60;
            this.lbGain.Text = "Gain [db]:";
            this.lbGain.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // albDirection
            // 
            this.albDirection.Location = new System.Drawing.Point(295, 195);
            this.albDirection.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.albDirection.Name = "albDirection";
            this.albDirection.Size = new System.Drawing.Size(88, 19);
            this.albDirection.TabIndex = 59;
            this.albDirection.Text = "Direction:";
			this.albDirection.Click += new System.EventHandler(this.albDirection_Click);

            // 
            // lbUnits
            // 
            this.lbUnits.Location = new System.Drawing.Point(280, 130);
            this.lbUnits.Name = "lbUnits";
            this.lbUnits.Size = new System.Drawing.Size(72, 20);
            this.lbUnits.TabIndex = 58;
            this.lbUnits.Text = "Units";
            this.lbUnits.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBoxUnits
            // 
            this.comboBoxUnits.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxUnits.Items.AddRange(new object[] {
            "cm/s",
            "KHz"});
            this.comboBoxUnits.Location = new System.Drawing.Point(360, 128);
            this.comboBoxUnits.Name = "comboBoxUnits";
            this.comboBoxUnits.Size = new System.Drawing.Size(72, 21);
            this.comboBoxUnits.TabIndex = 57;
            // 
            // lbZeroLine
            // 
            this.lbZeroLine.Location = new System.Drawing.Point(280, 152);
            this.lbZeroLine.Name = "lbZeroLine";
            this.lbZeroLine.Size = new System.Drawing.Size(80, 32);
            this.lbZeroLine.TabIndex = 56;
            this.lbZeroLine.Text = "Zero Line [%]:";
            this.lbZeroLine.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbScale
            // 
            this.lbScale.Location = new System.Drawing.Point(280, 64);
            this.lbScale.Name = "lbScale";
            this.lbScale.Size = new System.Drawing.Size(72, 24);
            this.lbScale.TabIndex = 54;
            this.lbScale.Text = "Scale [KHz]:";
            this.lbScale.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBoxFreqRange
            // 
			//this.comboBoxFreqRange.DataBindings.Add(new System.Windows.Forms.Binding("SelectedItem", this.dsBVSetup1, "tb_BloodVesselSetup.FreqRang", true, DataSourceUpdateMode.OnValidation));
            this.comboBoxFreqRange.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxFreqRange.Location = new System.Drawing.Point(360, 64);
            this.comboBoxFreqRange.Name = "comboBoxFreqRange";
            this.comboBoxFreqRange.Size = new System.Drawing.Size(72, 21);
            this.comboBoxFreqRange.TabIndex = 53;
            this.comboBoxFreqRange.Leave += new System.EventHandler(this.comboBoxFreqRange_Leave);
            // 
            // lbProbe
            // 
            this.lbProbe.Location = new System.Drawing.Point(94, 33);
            this.lbProbe.Name = "lbProbe";
            this.lbProbe.Size = new System.Drawing.Size(73, 18);
            this.lbProbe.TabIndex = 52;
            this.lbProbe.Text = "Probe:";
            this.lbProbe.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // comboBoxProbe
            // 
            this.comboBoxProbe.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxProbe.Location = new System.Drawing.Point(173, 31);
            this.comboBoxProbe.Name = "comboBoxProbe";
            this.comboBoxProbe.Size = new System.Drawing.Size(101, 21);
            this.comboBoxProbe.TabIndex = 51;
            this.comboBoxProbe.SelectedIndexChanged += new System.EventHandler(this.comboBoxProbe_SelectedIndexChanged);
            // 
            // lbCaption
            // 
            this.lbCaption.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbCaption.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lbCaption.Location = new System.Drawing.Point(88, 0);
            this.lbCaption.Name = "lbCaption";
            this.lbCaption.Size = new System.Drawing.Size(376, 23);
            this.lbCaption.TabIndex = 50;
            this.lbCaption.Text = "Default Parameters For Selected Blood Vessel";
            this.lbCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gradientPanel1
            // 
            this.gradientPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(109)))), ((int)(((byte)(169)))));
            this.gradientPanel1.Controls.Add(this.gridDataBoundGrid1);
            this.gradientPanel1.Controls.Add(this.xpToolBar1);
            this.gradientPanel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.gradientPanel1.Location = new System.Drawing.Point(0, 0);
            this.gradientPanel1.Name = "gradientPanel1";
            this.gradientPanel1.Size = new System.Drawing.Size(88, 368);
            this.gradientPanel1.TabIndex = 49;
            // 
            // gridDataBoundGrid1
            // 
            this.gridDataBoundGrid1.AllowDragSelectedCols = true;
            this.gridDataBoundGrid1.AllowResizeToFit = false;
            this.gridDataBoundGrid1.AllowSelection = ((Syncfusion.Windows.Forms.Grid.GridSelectionFlags)((Syncfusion.Windows.Forms.Grid.GridSelectionFlags.Cell | Syncfusion.Windows.Forms.Grid.GridSelectionFlags.AlphaBlend)));
            this.gridDataBoundGrid1.DataMember = "tb_BloodVesselSetup";
            this.gridDataBoundGrid1.DataSource = this.dsBVSetup1;
            this.gridDataBoundGrid1.EnableAddNew = false;
            this.gridDataBoundGrid1.EnableEdit = false;
            this.gridDataBoundGrid1.EnableRemove = false;
            this.gridDataBoundGrid1.Location = new System.Drawing.Point(0, 40);
            this.gridDataBoundGrid1.Name = "gridDataBoundGrid1";
            this.gridDataBoundGrid1.ResizeColsBehavior = Syncfusion.Windows.Forms.Grid.GridResizeCellsBehavior.None;
            this.gridDataBoundGrid1.ResizeRowsBehavior = Syncfusion.Windows.Forms.Grid.GridResizeCellsBehavior.None;
            this.gridDataBoundGrid1.ShowCurrentCellBorderBehavior = Syncfusion.Windows.Forms.Grid.GridShowCurrentCellBorder.GrayWhenLostFocus;
            this.gridDataBoundGrid1.Size = new System.Drawing.Size(84, 296);
            this.gridDataBoundGrid1.SortBehavior = Syncfusion.Windows.Forms.Grid.GridSortBehavior.DoubleClick;
            this.gridDataBoundGrid1.TabIndex = 2;
            this.gridDataBoundGrid1.Text = "gridDataBoundGrid1";
			this.gridDataBoundGrid1.Click += new System.EventHandler(this.gridDataBoundGrid1_Click);
			this.gridDataBoundGrid1.CurrentCellActivated += this.gridDataBoundGrid1_CurrentCellActivated;
			// 
            // xpToolBar1
            // 
            this.xpToolBar1.Appearance = System.Windows.Forms.ToolBarAppearance.Flat;
            this.xpToolBar1.AutoSize = false;
            this.xpToolBar1.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {this.barItemAdd,this.barItemDelete});
            this.xpToolBar1.DropDownArrows = true;
            this.xpToolBar1.ForeColor = System.Drawing.Color.White;
            this.xpToolBar1.ImageList = this.imageListBV;
            this.xpToolBar1.Location = new System.Drawing.Point(0, 0);
            this.xpToolBar1.Name = "xpToolBar1";
            this.xpToolBar1.ShowToolTips = true;
            this.xpToolBar1.Size = new System.Drawing.Size(88, 33);
            this.xpToolBar1.TabIndex = 0;
            this.xpToolBar1.ButtonClick += new System.Windows.Forms.ToolBarButtonClickEventHandler(this.xpToolBar1_ButtonClick);
            // 
            // barItemAdd
            // 
            this.barItemAdd.ImageIndex = 2;
            this.barItemAdd.Name = "barItemAdd";
            this.barItemAdd.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            // 
            // barItemDelete
            // 
            this.barItemDelete.ImageIndex = 0;
            this.barItemDelete.Name = "barItemDelete";
            this.barItemDelete.Style = System.Windows.Forms.ToolBarButtonStyle.ToggleButton;
            this.barItemDelete.Visible = false;
			this.barItemDelete.Enabled = false;
			// 
            // imageListBV
            // 
            this.imageListBV.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListBV.ImageStream")));
            this.imageListBV.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListBV.Images.SetKeyName(0, "");
            this.imageListBV.Images.SetKeyName(1, "");
            this.imageListBV.Images.SetKeyName(2, "");
            // 
            // gbHitsDetection
            // 
            this.gbHitsDetection.Controls.Add(this.autoLabel1);
            this.gbHitsDetection.Controls.Add(this.textBoxHitsThreshold);
            this.gbHitsDetection.Location = new System.Drawing.Point(332, 264);
            this.gbHitsDetection.Name = "gbHitsDetection";
            this.gbHitsDetection.Size = new System.Drawing.Size(110, 72);
            this.gbHitsDetection.TabIndex = 72;
            this.gbHitsDetection.TabStop = false;
            this.gbHitsDetection.Text = "Hits Detection";
            // 
            // autoLabel1
            // 
            this.autoLabel1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.autoLabel1.Location = new System.Drawing.Point(10, 20);
            this.autoLabel1.Name = "autoLabel1";
            this.autoLabel1.Size = new System.Drawing.Size(80, 16);
            this.autoLabel1.TabIndex = 1;
            this.autoLabel1.Text = "Threshold [db]:";
            this.autoLabel1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBoxHitsThreshold
            // 
            this.textBoxHitsThreshold.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textBoxHitsThreshold.Culture = new System.Globalization.CultureInfo("he-IL");
            this.textBoxHitsThreshold.ForeColor = System.Drawing.Color.Black;
            this.textBoxHitsThreshold.IntegerValue = ((long)(5));
            this.textBoxHitsThreshold.Location = new System.Drawing.Point(20, 40);
            this.textBoxHitsThreshold.MaxValue = ((long)(15));
            this.textBoxHitsThreshold.MinValue = ((long)(5));
            this.textBoxHitsThreshold.Name = "textBoxHitsThreshold";
            this.textBoxHitsThreshold.NullString = "0";
            this.textBoxHitsThreshold.NumberNegativePattern = 2;
            this.textBoxHitsThreshold.PositiveColor = System.Drawing.Color.Black;
            this.textBoxHitsThreshold.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textBoxHitsThreshold.Size = new System.Drawing.Size(64, 20);
            this.textBoxHitsThreshold.SpecialCultureValue = Syncfusion.Windows.Forms.Tools.SpecialCultureValues.None;
            this.textBoxHitsThreshold.TabIndex = 0;
            this.textBoxHitsThreshold.Leave += new System.EventHandler(this.textBoxHitsThreshold_Leave);
            this.textBoxHitsThreshold.MouseLeave += new System.EventHandler(this.textBoxHitsThreshold_MouseLeave);
            // 
            // pictureBoxFromProbe
            // 
            this.pictureBoxFromProbe.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.pictureBoxFromProbe.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxFromProbe.Image")));
            this.pictureBoxFromProbe.Location = new System.Drawing.Point(386, 192);
            this.pictureBoxFromProbe.Name = "pictureBoxFromProbe";
            this.pictureBoxFromProbe.Size = new System.Drawing.Size(30, 32);
            this.pictureBoxFromProbe.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxFromProbe.TabIndex = 74;
            this.pictureBoxFromProbe.TabStop = false;
            this.pictureBoxFromProbe.Click += new System.EventHandler(this.pictureBoxFromProbe_Click);
            // 
            // pictureBoxToProbe
            // 
            this.pictureBoxToProbe.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxToProbe.Image")));
            this.pictureBoxToProbe.Location = new System.Drawing.Point(422, 192);
            this.pictureBoxToProbe.Name = "pictureBoxToProbe";
            this.pictureBoxToProbe.Size = new System.Drawing.Size(30, 32);
            this.pictureBoxToProbe.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxToProbe.TabIndex = 74;
            this.pictureBoxToProbe.TabStop = false;
            this.pictureBoxToProbe.Click += new System.EventHandler(this.pictureBoxToProbe_Click);
            // 
            // lbAngle
            // 
            this.lbAngle.Location = new System.Drawing.Point(94, 160);
            this.lbAngle.Name = "lbAngle";
            this.lbAngle.Size = new System.Drawing.Size(56, 16);
            this.lbAngle.TabIndex = 75;
            this.lbAngle.Text = "Angle:";
            this.lbAngle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // numericUpDownAngle
            // 
			//this.numericUpDownAngle.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.dsBVSetup1, "tb_BloodVesselSetup.Angle", true, DataSourceUpdateMode.OnValidation));
            this.numericUpDownAngle.Location = new System.Drawing.Point(173, 160);
            this.numericUpDownAngle.Name = "numericUpDownAngle";
            this.numericUpDownAngle.Size = new System.Drawing.Size(101, 20);
            this.numericUpDownAngle.TabIndex = 76;
            // 
            // tmpDirection
            // 
			//this.tmpDirection.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.dsBVSetup1, "tb_BloodVesselSetup.DirectionToProbe", true, DataSourceUpdateMode.OnValidation));
            this.tmpDirection.Location = new System.Drawing.Point(278, 194);
            this.tmpDirection.Name = "tmpDirection";
            this.tmpDirection.Size = new System.Drawing.Size(36, 16);
            this.tmpDirection.TabIndex = 77;
            this.tmpDirection.CheckedChanged += new System.EventHandler(this.tmpDirection_CheckedChanged);
            // 
            // dsBVSetup2
            // 
            this.dsBVSetup2.DataSetName = "dsBVSetup";
            this.dsBVSetup2.Locale = new System.Globalization.CultureInfo("he-IL");
            this.dsBVSetup2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // numericUpDownZeroLine
            // 
            this.numericUpDownZeroLine.Increment = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDownZeroLine.Location = new System.Drawing.Point(360, 160);
            this.numericUpDownZeroLine.Maximum = new decimal(new int[] {
            96,
            0,
            0,
            0});
            this.numericUpDownZeroLine.Minimum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numericUpDownZeroLine.Name = "numericUpDownZeroLine";
            this.numericUpDownZeroLine.Size = new System.Drawing.Size(72, 20);
            this.numericUpDownZeroLine.TabIndex = 68;
            this.numericUpDownZeroLine.Value = new decimal(new int[] {
            6,
            0,
            0,
            0});
            // 
            // chkUseEnvelope
            // 
            this.chkUseEnvelope.Checked = true;
            this.chkUseEnvelope.CheckState = System.Windows.Forms.CheckState.Checked;
			//this.chkUseEnvelope.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.dsBVSetup1, "tb_BloodVesselSetup.UseEnvelopesForRCPDisplay", true, DataSourceUpdateMode.OnValidation));
            this.chkUseEnvelope.Location = new System.Drawing.Point(112, 230);
            this.chkUseEnvelope.Name = "chkUseEnvelope";
            this.chkUseEnvelope.Size = new System.Drawing.Size(336, 26);
            this.chkUseEnvelope.TabIndex = 78;
            this.chkUseEnvelope.Text = "Use Envelopes For Clinical Parameters Display";
            // 
            // SetUpBloodVessels
            // 
            this.Controls.Add(this.chkUseEnvelope);
            this.Controls.Add(this.numericUpDownAngle);
            this.Controls.Add(this.lbAngle);
            this.Controls.Add(this.pictureBoxFromProbe);
            this.Controls.Add(this.chkDisplaySpectrum);
            this.Controls.Add(this.gbDisplayEnvelopes);
            this.Controls.Add(this.comboBoxPower);
            this.Controls.Add(this.numericUpDownExtDepth);
            this.Controls.Add(this.numericUpDownExtWidth);
            this.Controls.Add(this.numericUpDownExtGain);
            this.Controls.Add(this.lbPower);
            this.Controls.Add(this.lbSampleVolume);
            this.Controls.Add(this.lbDepth);
            this.Controls.Add(this.lbFilter);
            this.Controls.Add(this.comboBoxThump);
            this.Controls.Add(this.lbGain);
            this.Controls.Add(this.albDirection);
            this.Controls.Add(this.lbUnits);
            this.Controls.Add(this.comboBoxUnits);
            this.Controls.Add(this.lbZeroLine);
            this.Controls.Add(this.lbScale);
            this.Controls.Add(this.comboBoxFreqRange);
            this.Controls.Add(this.lbProbe);
            this.Controls.Add(this.comboBoxProbe);
            this.Controls.Add(this.lbCaption);
            this.Controls.Add(this.gradientPanel1);
            this.Controls.Add(this.gbHitsDetection);
            this.Controls.Add(this.pictureBoxToProbe);
            this.Controls.Add(this.tmpDirection);
            this.Controls.Add(this.numericUpDownZeroLine);
            this.Name = "SetUpBloodVessels";
            this.Size = new System.Drawing.Size(464, 368);
            ((System.ComponentModel.ISupportInitialize)(this.dsBVSetup1)).EndInit();
            this.gbDisplayEnvelopes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownExtDepth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownExtWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownExtGain)).EndInit();
            this.gradientPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridDataBoundGrid1)).EndInit();
            this.gbHitsDetection.ResumeLayout(false);
            this.gbHitsDetection.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textBoxHitsThreshold)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxFromProbe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxToProbe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAngle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsBVSetup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownZeroLine)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.CheckBox chkDisplaySpectrum;
        public System.Windows.Forms.GroupBox gbDisplayEnvelopes;
        public System.Windows.Forms.CheckBox checkBoxReverseMode;
        public System.Windows.Forms.CheckBox checkBoxForwardMode;
        public System.Windows.Forms.CheckBox checkBoxReversePeak;
        public System.Windows.Forms.CheckBox checkBoxForwardPeak;
        public System.Windows.Forms.NumericUpDown numericUpDownExtDepth;
        public System.Windows.Forms.NumericUpDown numericUpDownExtWidth;
        public System.Windows.Forms.NumericUpDown numericUpDownExtGain;
        public System.Windows.Forms.Label lbPower;
        public System.Windows.Forms.Label lbSampleVolume;
        public System.Windows.Forms.Label lbDepth;
        public System.Windows.Forms.Label lbFilter;
        public System.Windows.Forms.Label lbGain;
        public System.Windows.Forms.Label albDirection;
        public System.Windows.Forms.Label lbUnits;
        public System.Windows.Forms.ComboBox comboBoxUnits;
        public System.Windows.Forms.Label lbZeroLine;
        public System.Windows.Forms.Label lbScale;
        public System.Windows.Forms.ComboBox comboBoxFreqRange;
        public System.Windows.Forms.Label lbProbe;
        public System.Windows.Forms.ComboBox comboBoxProbe;
        public System.Windows.Forms.Label lbCaption;
        public System.Windows.Forms.Panel gradientPanel1;
        public System.Windows.Forms.ToolBar xpToolBar1;
        private System.Windows.Forms.ImageList imageListBV;
        private System.Windows.Forms.ToolBarButton barItemAdd;
        private System.Windows.Forms.ToolBarButton barItemDelete;
        private System.Windows.Forms.GroupBox gbHitsDetection;
        private Syncfusion.Windows.Forms.Tools.IntegerTextBox textBoxHitsThreshold;
        private System.Windows.Forms.Label autoLabel1;
        private TCD.DAL.dsBVSetup dsBVSetup1;
        private Syncfusion.Windows.Forms.Grid.GridDataBoundGrid gridDataBoundGrid1;
        public System.Windows.Forms.ComboBox comboBoxPower;
        public System.Windows.Forms.ComboBox comboBoxThump;
        private System.Windows.Forms.PictureBox pictureBoxFromProbe;
        private System.Windows.Forms.PictureBox pictureBoxToProbe;
        private System.Windows.Forms.Label lbAngle;
        private System.Windows.Forms.NumericUpDown numericUpDownAngle;
        private System.ComponentModel.IContainer components;
        private System.Windows.Forms.CheckBox tmpDirection;
        private TCD.DAL.dsBVSetup dsBVSetup2;
        public System.Windows.Forms.NumericUpDown numericUpDownZeroLine;
        public System.Windows.Forms.CheckBox chkUseEnvelope;
    }
}