namespace Rimed.TCD.GUI.Setup
{
    partial class SetUpMMode
    {
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.albThreshold1 = new Syncfusion.Windows.Forms.Tools.AutoLabel();
            this.lbCaption = new System.Windows.Forms.Label();
            this.oleDbConnection1 = new System.Data.OleDb.OleDbConnection();
            this.numericUpDownMModeThres1 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownMModeThres2 = new System.Windows.Forms.NumericUpDown();
            this.albThreshold2 = new Syncfusion.Windows.Forms.Tools.AutoLabel();
            this.dsConfiguation1 = new TCD.DAL.dsConfiguation();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMModeThres1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMModeThres2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsConfiguation1)).BeginInit();
            this.SuspendLayout();
            // 
            // albThreshold1
            // 
            this.albThreshold1.DX = 0;
            this.albThreshold1.DY = 0;
            this.albThreshold1.Location = new System.Drawing.Point(64, 72);
            this.albThreshold1.Name = "albThreshold1";
            this.albThreshold1.Size = new System.Drawing.Size(108, 23);
            this.albThreshold1.TabIndex = 23;
            this.albThreshold1.Text = "Threshold1";
            this.albThreshold1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbCaption
            // 
            this.lbCaption.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbCaption.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.lbCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lbCaption.Location = new System.Drawing.Point(40, 19);
            this.lbCaption.Name = "lbCaption";
            this.lbCaption.Size = new System.Drawing.Size(376, 23);
            this.lbCaption.TabIndex = 15;
            this.lbCaption.Text = "M-mode Setup";
            this.lbCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numericUpDownMModeThres1
            // 
            this.numericUpDownMModeThres1.Location = new System.Drawing.Point(184, 72);
            this.numericUpDownMModeThres1.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownMModeThres1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownMModeThres1.Name = "numericUpDownMModeThres1";
            this.numericUpDownMModeThres1.Size = new System.Drawing.Size(64, 20);
            this.numericUpDownMModeThres1.TabIndex = 26;
            this.numericUpDownMModeThres1.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // numericUpDownMModeThres2
            // 
            this.numericUpDownMModeThres2.Location = new System.Drawing.Point(184, 104);
            this.numericUpDownMModeThres2.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownMModeThres2.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDownMModeThres2.Name = "numericUpDownMModeThres2";
            this.numericUpDownMModeThres2.Size = new System.Drawing.Size(64, 20);
            this.numericUpDownMModeThres2.TabIndex = 28;
            this.numericUpDownMModeThres2.Value = new decimal(new int[] {
            9,
            0,
            0,
            0});
            // 
            // albThreshold2
            // 
            this.albThreshold2.DX = 0;
            this.albThreshold2.DY = 0;
            this.albThreshold2.Location = new System.Drawing.Point(64, 104);
            this.albThreshold2.Name = "albThreshold2";
            this.albThreshold2.Size = new System.Drawing.Size(108, 23);
            this.albThreshold2.TabIndex = 27;
            this.albThreshold2.Text = "Threshold2";
            this.albThreshold2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dsConfiguation1
            // 
            this.dsConfiguation1.DataSetName = "dsConfiguation";
            this.dsConfiguation1.Locale = new System.Globalization.CultureInfo("en-US");
            this.dsConfiguation1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // SetUpMMode
            // 
            this.Controls.Add(this.numericUpDownMModeThres2);
            this.Controls.Add(this.albThreshold2);
            this.Controls.Add(this.numericUpDownMModeThres1);
            this.Controls.Add(this.albThreshold1);
            this.Controls.Add(this.lbCaption);
            this.Name = "SetUpMMode";
            this.Load += new System.EventHandler(this.SetUpMMode_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMModeThres1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMModeThres2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsConfiguation1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Syncfusion.Windows.Forms.Tools.AutoLabel albThreshold1;
        private System.Windows.Forms.Label lbCaption;
        private System.Data.OleDb.OleDbConnection oleDbConnection1;
        private System.Windows.Forms.NumericUpDown numericUpDownMModeThres1;
        private System.Windows.Forms.NumericUpDown numericUpDownMModeThres2;
        private Syncfusion.Windows.Forms.Tools.AutoLabel albThreshold2;
        private TCD.DAL.dsConfiguation dsConfiguation1;
        private System.ComponentModel.Container components = null;
    }
}