namespace Rimed.TCD.GUI.Setup
{
    partial class SetupStudies
    {
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControlExt1 = new Syncfusion.Windows.Forms.Tools.TabControlAdv();
            this.tabPageBVSelect = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.setupBVSelect1 = new SetupBVSelect();
            this.tabPageClinicalParameters = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.setupClinicalParameters1 = new SetupClinicalParameters();
            this.tabPageEvents = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.setUpEvents1 = new SetUpEvents();
            this.tabPageRecording = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.setUpRecording1 = new SetUpRecording();
            this.tabPageTrend = new Syncfusion.Windows.Forms.Tools.TabPageAdv();
            this.setupTrend1 = new SetupTrend();
            ((System.ComponentModel.ISupportInitialize)(this.tabControlExt1)).BeginInit();
            this.tabControlExt1.SuspendLayout();
            this.tabPageBVSelect.SuspendLayout();
            this.tabPageClinicalParameters.SuspendLayout();
            this.tabPageEvents.SuspendLayout();
            this.tabPageRecording.SuspendLayout();
            this.tabPageTrend.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControlExt1
            // 
            this.tabControlExt1.Controls.Add(this.tabPageBVSelect);
            this.tabControlExt1.Controls.Add(this.tabPageClinicalParameters);
            this.tabControlExt1.Controls.Add(this.tabPageEvents);
            this.tabControlExt1.Controls.Add(this.tabPageRecording);
            this.tabControlExt1.Controls.Add(this.tabPageTrend);
            this.tabControlExt1.Location = new System.Drawing.Point(0, 0);
            this.tabControlExt1.Name = "tabControlExt1";
            this.tabControlExt1.Size = new System.Drawing.Size(424, 328);
            this.tabControlExt1.TabIndex = 0;
            this.tabControlExt1.Click += new System.EventHandler(this.tabControlExt1_Click);
            this.tabControlExt1.TabIndexChanged += new System.EventHandler(this.tabControlExt1_TabIndexChanged);
            this.tabControlExt1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tabControlExt1_MouseDown);
            // 
            // tabPageBVSelect
            // 
            this.tabPageBVSelect.Controls.Add(this.setupBVSelect1);
            this.tabPageBVSelect.Location = new System.Drawing.Point(0, 0);
            this.tabPageBVSelect.Name = "tabPageBVSelect";
            this.tabPageBVSelect.Size = new System.Drawing.Size(0, 0);
            this.tabPageBVSelect.TabIndex = 2;
            this.tabPageBVSelect.ThemesEnabled = false;
            // 
            // setupBVSelect1
            // 
            this.setupBVSelect1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(216)), ((System.Byte)(228)), ((System.Byte)(248)));
            this.setupBVSelect1.Location = new System.Drawing.Point(8, 16);
            this.setupBVSelect1.Name = "setupBVSelect1";
            this.setupBVSelect1.Size = new System.Drawing.Size(458, 344);
            this.setupBVSelect1.StudyIndex = new System.Guid("00000000-0000-0000-0000-000000000000");
            this.setupBVSelect1.TabIndex = 0;
            // 
            // tabPageClinicalParameters
            // 
            this.tabPageClinicalParameters.Controls.Add(this.setupClinicalParameters1);
            this.tabPageClinicalParameters.Location = new System.Drawing.Point(1, 26);
            this.tabPageClinicalParameters.Name = "tabPageClinicalParameters";
            this.tabPageClinicalParameters.Size = new System.Drawing.Size(421, 300);
            this.tabPageClinicalParameters.TabIndex = 3;
            this.tabPageClinicalParameters.ThemesEnabled = false;
            // 
            // setupClinicalParameters1
            // 
            this.setupClinicalParameters1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(216)), ((System.Byte)(228)), ((System.Byte)(248)));
            this.setupClinicalParameters1.Location = new System.Drawing.Point(0, 0);
            this.setupClinicalParameters1.Name = "setupClinicalParameters1";
            this.setupClinicalParameters1.Size = new System.Drawing.Size(421, 300);
            this.setupClinicalParameters1.StudyIndex = new System.Guid("00000000-0000-0000-0000-000000000000");
            this.setupClinicalParameters1.TabIndex = 0;
            // 
            // tabPageEvents
            // 
            this.tabPageEvents.Controls.Add(this.setUpEvents1);
            this.tabPageEvents.Location = new System.Drawing.Point(1, 26);
            this.tabPageEvents.Name = "tabPageEvents";
            this.tabPageEvents.Size = new System.Drawing.Size(421, 300);
            this.tabPageEvents.TabIndex = 0;
            this.tabPageEvents.ThemesEnabled = false;
            // 
            // setUpEvents1
            // 
            this.setUpEvents1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(216)), ((System.Byte)(228)), ((System.Byte)(248)));
            this.setUpEvents1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.setUpEvents1.Location = new System.Drawing.Point(0, 0);
            this.setUpEvents1.Name = "setUpEvents1";
            this.setUpEvents1.Size = new System.Drawing.Size(421, 300);
            this.setUpEvents1.TabIndex = 0;
            // 
            // tabPageRecording
            // 
            this.tabPageRecording.Controls.Add(this.setUpRecording1);
            this.tabPageRecording.Location = new System.Drawing.Point(1, 26);
            this.tabPageRecording.Name = "tabPageRecording";
            this.tabPageRecording.Size = new System.Drawing.Size(421, 300);
            this.tabPageRecording.TabIndex = 1;
            this.tabPageRecording.ThemesEnabled = false;
            // 
            // setUpRecording1
            // 
            this.setUpRecording1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(216)), ((System.Byte)(228)), ((System.Byte)(248)));
            this.setUpRecording1.Location = new System.Drawing.Point(0, 0);
            this.setUpRecording1.Name = "setUpRecording1";
            this.setUpRecording1.Size = new System.Drawing.Size(421, 300);
            this.setUpRecording1.TabIndex = 0;
            // 
            // tabPageTrend
            // 
            this.tabPageTrend.Controls.Add(this.setupTrend1);
            this.tabPageTrend.Location = new System.Drawing.Point(1, 26);
            this.tabPageTrend.Name = "tabPageTrend";
            this.tabPageTrend.Size = new System.Drawing.Size(421, 300);
            this.tabPageTrend.TabIndex = 4;
            this.tabPageTrend.ThemesEnabled = false;
            // 
            // setupTrend1
            // 
            this.setupTrend1.BackColor = System.Drawing.Color.FromArgb(((System.Byte)(216)), ((System.Byte)(228)), ((System.Byte)(248)));
            this.setupTrend1.Location = new System.Drawing.Point(0, 0);
            this.setupTrend1.Name = "setupTrend1";
            this.setupTrend1.Size = new System.Drawing.Size(458, 344);
            this.setupTrend1.StudyIndex = new System.Guid("00000000-0000-0000-0000-000000000000");
            this.setupTrend1.TabIndex = 0;
            // 
            // SetupStudies
            // 
            this.Controls.Add(this.tabControlExt1);
            this.Name = "SetupStudies";
            this.Size = new System.Drawing.Size(424, 328);
            ((System.ComponentModel.ISupportInitialize)(this.tabControlExt1)).EndInit();
            this.tabControlExt1.ResumeLayout(false);
            this.tabPageBVSelect.ResumeLayout(false);
            this.tabPageClinicalParameters.ResumeLayout(false);
            this.tabPageEvents.ResumeLayout(false);
            this.tabPageRecording.ResumeLayout(false);
            this.tabPageTrend.ResumeLayout(false);
            this.ResumeLayout(false);
        }

        #endregion

        private SetUpEvents setUpEvents1;
        private SetUpRecording setUpRecording1;
        private SetupBVSelect setupBVSelect1;
        private Syncfusion.Windows.Forms.Tools.TabControlAdv tabControlExt1;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPageEvents;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPageRecording;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPageBVSelect;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPageClinicalParameters;
        private Syncfusion.Windows.Forms.Tools.TabPageAdv tabPageTrend;
        private SetupClinicalParameters setupClinicalParameters1;
        private SetupTrend setupTrend1;
        private System.ComponentModel.Container components = null;
    }
}