namespace Rimed.TCD.GUI.Setup
{
    partial class SetUpNextFunction
    {
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetUpNextFunction));
            this.bRemove = new System.Windows.Forms.Button();
            this.bAdd = new System.Windows.Forms.Button();
            this.lbFunctionsInOrder = new System.Windows.Forms.Label();
            this.listBoxFuncOrder = new System.Windows.Forms.ListBox();
            this.lbAvailableFunctions = new System.Windows.Forms.Label();
            this.listBoxFunctions = new System.Windows.Forms.ListBox();
            this.dsNextFunction1 = new TCD.DAL.dsNextFunction();
            this.lbCaption = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dsNextFunction1)).BeginInit();
            this.SuspendLayout();
            // 
            // bRemove
            // 
            this.bRemove.Enabled = false;
            this.bRemove.Image = ((System.Drawing.Image)(resources.GetObject("bRemove.Image")));
            this.bRemove.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.bRemove.Location = new System.Drawing.Point(192, 136);
            this.bRemove.Name = "bRemove";
            this.bRemove.Size = new System.Drawing.Size(75, 40);
            this.bRemove.TabIndex = 31;
            this.bRemove.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.bRemove.Click += new System.EventHandler(this.bRemove_Click);
            // 
            // bAdd
            // 
            this.bAdd.Enabled = false;
            this.bAdd.Image = ((System.Drawing.Image)(resources.GetObject("bAdd.Image")));
            this.bAdd.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.bAdd.Location = new System.Drawing.Point(192, 80);
            this.bAdd.Name = "bAdd";
            this.bAdd.Size = new System.Drawing.Size(75, 40);
            this.bAdd.TabIndex = 30;
            this.bAdd.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.bAdd.Click += new System.EventHandler(this.bAdd_Click);
            // 
            // lbFunctionsInOrder
            // 
            this.lbFunctionsInOrder.Location = new System.Drawing.Point(296, 48);
            this.lbFunctionsInOrder.Name = "lbFunctionsInOrder";
            this.lbFunctionsInOrder.Size = new System.Drawing.Size(144, 29);
            this.lbFunctionsInOrder.TabIndex = 29;
            this.lbFunctionsInOrder.Text = "Next function by order of execution";
            this.lbFunctionsInOrder.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listBoxFuncOrder
            // 
            this.listBoxFuncOrder.Location = new System.Drawing.Point(296, 80);
            this.listBoxFuncOrder.Name = "listBoxFuncOrder";
            this.listBoxFuncOrder.Size = new System.Drawing.Size(144, 225);
            this.listBoxFuncOrder.TabIndex = 28;
            this.listBoxFuncOrder.SelectedIndexChanged += new System.EventHandler(this.listBoxFuncOrder_SelectedIndexChanged);
            // 
            // lbAvailableFunctions
            // 
            this.lbAvailableFunctions.Location = new System.Drawing.Point(16, 48);
            this.lbAvailableFunctions.Name = "lbAvailableFunctions";
            this.lbAvailableFunctions.Size = new System.Drawing.Size(144, 24);
            this.lbAvailableFunctions.TabIndex = 27;
            this.lbAvailableFunctions.Text = "Available functions";
            this.lbAvailableFunctions.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listBoxFunctions
            // 
            this.listBoxFunctions.Location = new System.Drawing.Point(16, 80);
            this.listBoxFunctions.Name = "listBoxFunctions";
            this.listBoxFunctions.Size = new System.Drawing.Size(144, 225);
            this.listBoxFunctions.TabIndex = 26;
            this.listBoxFunctions.SelectedIndexChanged += new System.EventHandler(this.listBoxFunctions_SelectedIndexChanged);
            // 
            // dsNextFunction1
            // 
            this.dsNextFunction1.DataSetName = "dsNextFunction";
            this.dsNextFunction1.Locale = new System.Globalization.CultureInfo("he-IL");
            this.dsNextFunction1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lbCaption
            // 
            this.lbCaption.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbCaption.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.lbCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lbCaption.Location = new System.Drawing.Point(41, 8);
            this.lbCaption.Name = "lbCaption";
            this.lbCaption.Size = new System.Drawing.Size(376, 23);
            this.lbCaption.TabIndex = 32;
            this.lbCaption.Text = "Next Function Setup";
            this.lbCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SetUpNextFunction
            // 
            this.Controls.Add(this.lbCaption);
            this.Controls.Add(this.bRemove);
            this.Controls.Add(this.bAdd);
            this.Controls.Add(this.lbFunctionsInOrder);
            this.Controls.Add(this.listBoxFuncOrder);
            this.Controls.Add(this.lbAvailableFunctions);
            this.Controls.Add(this.listBoxFunctions);
            this.Name = "SetUpNextFunction";
            ((System.ComponentModel.ISupportInitialize)(this.dsNextFunction1)).EndInit();
            this.ResumeLayout(false);

        }
        
        #endregion

        private System.Windows.Forms.Button bRemove;
        private System.Windows.Forms.Button bAdd;
        private System.Windows.Forms.Label lbFunctionsInOrder;
        public System.Windows.Forms.ListBox listBoxFuncOrder;
        private System.Windows.Forms.Label lbAvailableFunctions;
        private System.Windows.Forms.ListBox listBoxFunctions;
        private System.ComponentModel.Container components = null;
        private System.Windows.Forms.Label lbCaption;
        private TCD.DAL.dsNextFunction dsNextFunction1;
    }
}