namespace Rimed.TCD.GUI.Setup
{
    partial class SetUpBasicConfiguration
    {
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbVolume = new System.Windows.Forms.GroupBox();
            this.trackBarHeadphoneVolume = new System.Windows.Forms.TrackBar();
            this.dsConfiguation1 = new TCD.DAL.dsConfiguation();
            this.radioButtonHeadphone = new System.Windows.Forms.RadioButton();
            this.trackBarLoudspeakerVolume = new System.Windows.Forms.TrackBar();
            this.radioButtonLoudspeaker = new System.Windows.Forms.RadioButton();
            this.gbDepth = new System.Windows.Forms.GroupBox();

            this.gbParameters = new System.Windows.Forms.GroupBox();
            this.radioButtonAveraging = new System.Windows.Forms.RadioButton();
            this.radioButtonBeatByBeat = new System.Windows.Forms.RadioButton();
            this.checkBoxAutoMuteEnable = new System.Windows.Forms.CheckBox();

            this.numericUpDownAutoscanMinDepth1Mhz = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxDepthStep16Mhz = new System.Windows.Forms.ComboBox();
            this.comboBoxDepthStep2Mhz = new System.Windows.Forms.ComboBox();
            this.comboBoxDepthStep1Mhz = new System.Windows.Forms.ComboBox();
            this.lbProbe = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBoxDepthStep4Mhz = new System.Windows.Forms.ComboBox();
            this.comboBoxDepthStep8Mhz = new System.Windows.Forms.ComboBox();
            this.lbDepthStep = new System.Windows.Forms.Label();
            this.lbMModeMinDepth = new System.Windows.Forms.Label();
            this.numericUpDownAutoscanMinDepth2Mhz = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownAutoscanMinDepth4Mhz = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownAutoscanMinDepth8Mhz = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownAutoscanMinDepth16Mhz = new System.Windows.Forms.NumericUpDown();
            this.numericUpDownAutoscanMinDepth20Mhz = new System.Windows.Forms.NumericUpDown();
            this.lbMModeMaxDepth = new System.Windows.Forms.Label();
            this.labelAutoscanMaxDepth2Mhz = new System.Windows.Forms.Label();
            this.labelAutoscanMaxDepth1Mhz = new System.Windows.Forms.Label();
            this.labelAutoscanMaxDepth4Mhz = new System.Windows.Forms.Label();
            this.labelAutoscanMaxDepth8Mhz = new System.Windows.Forms.Label();
            this.labelAutoscanMaxDepth16Mhz = new System.Windows.Forms.Label();
            this.labelAutoscanMaxDepth20Mhz = new System.Windows.Forms.Label();
            this.comboBoxDepthStep20Mhz = new System.Windows.Forms.ComboBox();
            this.comboBoxFftSize = new System.Windows.Forms.ComboBox();
            this.albFFTSize = new System.Windows.Forms.Label();
            this.textBoxTimeDomainTime = new System.Windows.Forms.TextBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.checkBoxEnableDefaultBVDepth = new System.Windows.Forms.CheckBox();
            this.dsProbe1 = new TCD.DAL.dsProbe();
            this.gbVolume.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarHeadphoneVolume)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsConfiguation1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarLoudspeakerVolume)).BeginInit();

            this.gbParameters.SuspendLayout();
            //((System.ComponentModel.ISupportInitialize)(this.trackBarHeadphoneVolume)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.dsConfiguation1)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.trackBarLoudspeakerVolume)).BeginInit();

            this.gbDepth.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAutoscanMinDepth1Mhz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAutoscanMinDepth2Mhz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAutoscanMinDepth4Mhz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAutoscanMinDepth8Mhz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAutoscanMinDepth16Mhz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAutoscanMinDepth20Mhz)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsProbe1)).BeginInit();
            this.SuspendLayout();
            // 
            // gbVolume
            // 
            this.gbVolume.Controls.Add(this.trackBarHeadphoneVolume);
            this.gbVolume.Controls.Add(this.radioButtonHeadphone);
            this.gbVolume.Controls.Add(this.trackBarLoudspeakerVolume);
            this.gbVolume.Controls.Add(this.radioButtonLoudspeaker);
            this.gbVolume.Controls.Add(this.checkBoxAutoMuteEnable);
            this.gbVolume.Location = new System.Drawing.Point(17, 270);
            this.gbVolume.Name = "gbVolume";
            this.gbVolume.Size = new System.Drawing.Size(423, 88);
            this.gbVolume.TabIndex = 12;
            this.gbVolume.TabStop = false;
            this.gbVolume.Text = "Volume";
            // 
            // trackBarHeadphoneVolume
            // 
            this.trackBarHeadphoneVolume.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.dsConfiguation1, "tb_BasicConfiguration.HeadphoneVolume", true));
            this.trackBarHeadphoneVolume.Location = new System.Drawing.Point(232, 32);
            this.trackBarHeadphoneVolume.Name = "trackBarHeadphoneVolume";
            this.trackBarHeadphoneVolume.Size = new System.Drawing.Size(136, 42);
            this.trackBarHeadphoneVolume.TabIndex = 3;
            // 
            // dsConfiguation1
            // 
            this.dsConfiguation1.DataSetName = "dsConfiguation";
            this.dsConfiguation1.Locale = new System.Globalization.CultureInfo("en-US");
            this.dsConfiguation1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // radioButtonHeadphone
            // 
            this.radioButtonHeadphone.Location = new System.Drawing.Point(232, 16);
            this.radioButtonHeadphone.Name = "radioButtonHeadphone";
            this.radioButtonHeadphone.Size = new System.Drawing.Size(89, 16);
            this.radioButtonHeadphone.TabIndex = 2;
            this.radioButtonHeadphone.Text = "Headphone";
            this.radioButtonHeadphone.CheckedChanged += new System.EventHandler(this.radioButtonHeadphone_CheckedChanged);
            // 
            // trackBarLoudspeakerVolume
            // 
            this.trackBarLoudspeakerVolume.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.dsConfiguation1, "tb_BasicConfiguration.SpeakerVolume", true));
            this.trackBarLoudspeakerVolume.Location = new System.Drawing.Point(24, 32);
            this.trackBarLoudspeakerVolume.Name = "trackBarLoudspeakerVolume";
            this.trackBarLoudspeakerVolume.Size = new System.Drawing.Size(136, 42);
            this.trackBarLoudspeakerVolume.TabIndex = 1;
            // 
            // radioButtonLoudspeaker
            // 
            this.radioButtonLoudspeaker.Checked = true;
            this.radioButtonLoudspeaker.Location = new System.Drawing.Point(32, 16);
            this.radioButtonLoudspeaker.Name = "radioButtonLoudspeaker";
            this.radioButtonLoudspeaker.Size = new System.Drawing.Size(88, 16);
            this.radioButtonLoudspeaker.TabIndex = 0;
            this.radioButtonLoudspeaker.TabStop = true;
            this.radioButtonLoudspeaker.Text = "Loudspeaker";
            this.radioButtonLoudspeaker.CheckedChanged += new System.EventHandler(this.radioButtonLoudspeaker_CheckedChanged);

            // 
            // checkBoxAutoMuteEnable
            // 
            this.checkBoxAutoMuteEnable.Location = new System.Drawing.Point(160, 9);
            this.checkBoxAutoMuteEnable.Name = "checkBoxAutoMuteEnable";
            this.checkBoxAutoMuteEnable.Size = new System.Drawing.Size(100, 30);
            this.checkBoxAutoMuteEnable.TabIndex = 4;
            this.checkBoxAutoMuteEnable.TabStop = true;
            this.checkBoxAutoMuteEnable.Text = "Auto-mute";
            this.checkBoxAutoMuteEnable.CheckedChanged += new System.EventHandler(this.checkBoxAutoMuteEnable_CheckedChanged);

            // 
            // gbDepth
            // 
            this.gbDepth.Controls.Add(this.numericUpDownAutoscanMinDepth1Mhz);
            this.gbDepth.Controls.Add(this.label1);
            this.gbDepth.Controls.Add(this.comboBoxDepthStep16Mhz);
            this.gbDepth.Controls.Add(this.comboBoxDepthStep2Mhz);
            this.gbDepth.Controls.Add(this.comboBoxDepthStep1Mhz);
            this.gbDepth.Controls.Add(this.lbProbe);
            this.gbDepth.Controls.Add(this.label3);
            this.gbDepth.Controls.Add(this.label4);
            this.gbDepth.Controls.Add(this.label5);
            this.gbDepth.Controls.Add(this.label6);
            this.gbDepth.Controls.Add(this.label7);
            this.gbDepth.Controls.Add(this.comboBoxDepthStep4Mhz);
            this.gbDepth.Controls.Add(this.comboBoxDepthStep8Mhz);
            this.gbDepth.Controls.Add(this.lbDepthStep);
            this.gbDepth.Controls.Add(this.lbMModeMinDepth);
            this.gbDepth.Controls.Add(this.numericUpDownAutoscanMinDepth2Mhz);
            this.gbDepth.Controls.Add(this.numericUpDownAutoscanMinDepth4Mhz);
            this.gbDepth.Controls.Add(this.numericUpDownAutoscanMinDepth8Mhz);
            this.gbDepth.Controls.Add(this.numericUpDownAutoscanMinDepth16Mhz);
            this.gbDepth.Controls.Add(this.numericUpDownAutoscanMinDepth20Mhz);
            this.gbDepth.Controls.Add(this.lbMModeMaxDepth);
            this.gbDepth.Controls.Add(this.labelAutoscanMaxDepth2Mhz);
            this.gbDepth.Controls.Add(this.labelAutoscanMaxDepth1Mhz);
            this.gbDepth.Controls.Add(this.labelAutoscanMaxDepth4Mhz);
            this.gbDepth.Controls.Add(this.labelAutoscanMaxDepth8Mhz);
            this.gbDepth.Controls.Add(this.labelAutoscanMaxDepth16Mhz);
            this.gbDepth.Controls.Add(this.labelAutoscanMaxDepth20Mhz);
            this.gbDepth.Controls.Add(this.comboBoxDepthStep20Mhz);
            this.gbDepth.Location = new System.Drawing.Point(17, 30);
            this.gbDepth.Name = "gbDepth";
            this.gbDepth.Size = new System.Drawing.Size(423, 195);
            this.gbDepth.TabIndex = 11;
            this.gbDepth.TabStop = false;
            this.gbDepth.Text = "Depth";
            // 
            // numericUpDownAutoscanMinDepth1Mhz
            // 
            this.numericUpDownAutoscanMinDepth1Mhz.Location = new System.Drawing.Point(200, 54);
            this.numericUpDownAutoscanMinDepth1Mhz.Maximum = new decimal(new int[] {
            400,
            0,
            0,
            0});
            this.numericUpDownAutoscanMinDepth1Mhz.Minimum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.numericUpDownAutoscanMinDepth1Mhz.Name = "numericUpDownAutoscanMinDepth1Mhz";
            this.numericUpDownAutoscanMinDepth1Mhz.Size = new System.Drawing.Size(64, 20);
            this.numericUpDownAutoscanMinDepth1Mhz.TabIndex = 7;
            this.numericUpDownAutoscanMinDepth1Mhz.Value = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.numericUpDownAutoscanMinDepth1Mhz.ValueChanged += new System.EventHandler(this.numericUpDownAutoscanMinDepth1Mhz_ValueChanged);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(24, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 16);
            this.label1.TabIndex = 6;
            this.label1.Text = "1Mhz";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comboBoxDepthStep16Mhz
            // 
            this.comboBoxDepthStep16Mhz.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDepthStep16Mhz.Location = new System.Drawing.Point(96, 149);
            this.comboBoxDepthStep16Mhz.Name = "comboBoxDepthStep16Mhz";
            this.comboBoxDepthStep16Mhz.Size = new System.Drawing.Size(80, 21);
            this.comboBoxDepthStep16Mhz.TabIndex = 1;
            this.comboBoxDepthStep16Mhz.SelectedValueChanged += new System.EventHandler(this.comboBoxDepthStep16Mhz_SelectedValueChanged);
            // 
            // comboBoxDepthStep2Mhz
            // 
            this.comboBoxDepthStep2Mhz.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDepthStep2Mhz.Location = new System.Drawing.Point(96, 77);
            this.comboBoxDepthStep2Mhz.Name = "comboBoxDepthStep2Mhz";
            this.comboBoxDepthStep2Mhz.Size = new System.Drawing.Size(80, 21);
            this.comboBoxDepthStep2Mhz.TabIndex = 0;
            this.comboBoxDepthStep2Mhz.SelectedValueChanged += new System.EventHandler(this.comboBoxDepthStep2Mhz_SelectedValueChanged);
            // 
            // comboBoxDepthStep1Mhz
            // 
            this.comboBoxDepthStep1Mhz.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDepthStep1Mhz.Location = new System.Drawing.Point(96, 53);
            this.comboBoxDepthStep1Mhz.Name = "comboBoxDepthStep1Mhz";
            this.comboBoxDepthStep1Mhz.Size = new System.Drawing.Size(80, 21);
            this.comboBoxDepthStep1Mhz.TabIndex = 0;
            this.comboBoxDepthStep1Mhz.SelectedValueChanged += new System.EventHandler(this.comboBoxDepthStep1Mhz_SelectedValueChanged);
            // 
            // lbProbe
            // 
            this.lbProbe.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lbProbe.Location = new System.Drawing.Point(24, 20);
            this.lbProbe.Name = "lbProbe";
            this.lbProbe.Size = new System.Drawing.Size(48, 16);
            this.lbProbe.TabIndex = 6;
            this.lbProbe.Text = "Probe";
            this.lbProbe.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(24, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "2Mhz";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(24, 106);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 16);
            this.label4.TabIndex = 6;
            this.label4.Text = "4Mhz";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(24, 130);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 16);
            this.label5.TabIndex = 6;
            this.label5.Text = "8Mhz";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(24, 154);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 16);
            this.label6.TabIndex = 6;
            this.label6.Text = "16Mhz";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(24, 173);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 16);
            this.label7.TabIndex = 6;
            this.label7.Text = "20Mhz";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label7.Visible = false;
            // 
            // comboBoxDepthStep4Mhz
            // 
            this.comboBoxDepthStep4Mhz.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDepthStep4Mhz.Items.AddRange(new object[] {
            ""});
            this.comboBoxDepthStep4Mhz.Location = new System.Drawing.Point(96, 101);
            this.comboBoxDepthStep4Mhz.Name = "comboBoxDepthStep4Mhz";
            this.comboBoxDepthStep4Mhz.Size = new System.Drawing.Size(80, 21);
            this.comboBoxDepthStep4Mhz.TabIndex = 0;
            this.comboBoxDepthStep4Mhz.SelectedValueChanged += new System.EventHandler(this.comboBoxDepthStep4Mhz_SelectedValueChanged);
            // 
            // comboBoxDepthStep8Mhz
            // 
            this.comboBoxDepthStep8Mhz.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDepthStep8Mhz.Location = new System.Drawing.Point(96, 125);
            this.comboBoxDepthStep8Mhz.Name = "comboBoxDepthStep8Mhz";
            this.comboBoxDepthStep8Mhz.Size = new System.Drawing.Size(80, 21);
            this.comboBoxDepthStep8Mhz.TabIndex = 0;
            this.comboBoxDepthStep8Mhz.SelectedValueChanged += new System.EventHandler(this.comboBoxDepthStep8Mhz_SelectedValueChanged);
            // 
            // lbDepthStep
            // 
            this.lbDepthStep.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lbDepthStep.Location = new System.Drawing.Point(93, 16);
            this.lbDepthStep.Name = "lbDepthStep";
            this.lbDepthStep.Size = new System.Drawing.Size(83, 24);
            this.lbDepthStep.TabIndex = 6;
            this.lbDepthStep.Text = "Depth Step";
            this.lbDepthStep.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbMModeMinDepth
            // 
            this.lbMModeMinDepth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lbMModeMinDepth.Location = new System.Drawing.Point(186, 16);
            this.lbMModeMinDepth.Name = "lbMModeMinDepth";
            this.lbMModeMinDepth.Size = new System.Drawing.Size(85, 35);
            this.lbMModeMinDepth.TabIndex = 6;
            this.lbMModeMinDepth.Text = "M-Mode Min Depth";
            this.lbMModeMinDepth.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numericUpDownAutoscanMinDepth2Mhz
            // 
            this.numericUpDownAutoscanMinDepth2Mhz.Location = new System.Drawing.Point(200, 78);
            this.numericUpDownAutoscanMinDepth2Mhz.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.numericUpDownAutoscanMinDepth2Mhz.Minimum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.numericUpDownAutoscanMinDepth2Mhz.Name = "numericUpDownAutoscanMinDepth2Mhz";
            this.numericUpDownAutoscanMinDepth2Mhz.Size = new System.Drawing.Size(64, 20);
            this.numericUpDownAutoscanMinDepth2Mhz.TabIndex = 7;
            this.numericUpDownAutoscanMinDepth2Mhz.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.numericUpDownAutoscanMinDepth2Mhz.ValueChanged += new System.EventHandler(this.numericUpDownAutoscanMinDepth2Mhz_ValueChanged);
            // 
            // numericUpDownAutoscanMinDepth4Mhz
            // 
            this.numericUpDownAutoscanMinDepth4Mhz.DecimalPlaces = 1;
            this.numericUpDownAutoscanMinDepth4Mhz.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDownAutoscanMinDepth4Mhz.Location = new System.Drawing.Point(200, 102);
            this.numericUpDownAutoscanMinDepth4Mhz.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.numericUpDownAutoscanMinDepth4Mhz.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDownAutoscanMinDepth4Mhz.Name = "numericUpDownAutoscanMinDepth4Mhz";
            this.numericUpDownAutoscanMinDepth4Mhz.Size = new System.Drawing.Size(64, 20);
            this.numericUpDownAutoscanMinDepth4Mhz.TabIndex = 7;
            this.numericUpDownAutoscanMinDepth4Mhz.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDownAutoscanMinDepth4Mhz.ValueChanged += new System.EventHandler(this.numericUpDownAutoscanMinDepth4Mhz_ValueChanged);
            // 
            // numericUpDownAutoscanMinDepth8Mhz
            // 
            this.numericUpDownAutoscanMinDepth8Mhz.DecimalPlaces = 1;
            this.numericUpDownAutoscanMinDepth8Mhz.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.numericUpDownAutoscanMinDepth8Mhz.Location = new System.Drawing.Point(200, 126);
            this.numericUpDownAutoscanMinDepth8Mhz.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.numericUpDownAutoscanMinDepth8Mhz.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDownAutoscanMinDepth8Mhz.Name = "numericUpDownAutoscanMinDepth8Mhz";
            this.numericUpDownAutoscanMinDepth8Mhz.Size = new System.Drawing.Size(64, 20);
            this.numericUpDownAutoscanMinDepth8Mhz.TabIndex = 7;
            this.numericUpDownAutoscanMinDepth8Mhz.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDownAutoscanMinDepth8Mhz.ValueChanged += new System.EventHandler(this.numericUpDownAutoscanMinDepth8hz_ValueChanged);
            // 
            // numericUpDownAutoscanMinDepth16Mhz
            // 
            this.numericUpDownAutoscanMinDepth16Mhz.DecimalPlaces = 1;
            this.numericUpDownAutoscanMinDepth16Mhz.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownAutoscanMinDepth16Mhz.Location = new System.Drawing.Point(200, 150);
            this.numericUpDownAutoscanMinDepth16Mhz.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.numericUpDownAutoscanMinDepth16Mhz.Minimum = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numericUpDownAutoscanMinDepth16Mhz.Name = "numericUpDownAutoscanMinDepth16Mhz";
            this.numericUpDownAutoscanMinDepth16Mhz.Size = new System.Drawing.Size(64, 20);
            this.numericUpDownAutoscanMinDepth16Mhz.TabIndex = 7;
            this.numericUpDownAutoscanMinDepth16Mhz.Value = new decimal(new int[] {
            8,
            0,
            0,
            65536});
            this.numericUpDownAutoscanMinDepth16Mhz.ValueChanged += new System.EventHandler(this.numericUpDownAutoscanMinDepth16Mhz_ValueChanged);
            // 
            // numericUpDownAutoscanMinDepth20Mhz
            // 
            this.numericUpDownAutoscanMinDepth20Mhz.DecimalPlaces = 1;
            this.numericUpDownAutoscanMinDepth20Mhz.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericUpDownAutoscanMinDepth20Mhz.Location = new System.Drawing.Point(200, 169);
            this.numericUpDownAutoscanMinDepth20Mhz.Maximum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.numericUpDownAutoscanMinDepth20Mhz.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            65536});
            this.numericUpDownAutoscanMinDepth20Mhz.Name = "numericUpDownAutoscanMinDepth20Mhz";
            this.numericUpDownAutoscanMinDepth20Mhz.Size = new System.Drawing.Size(64, 20);
            this.numericUpDownAutoscanMinDepth20Mhz.TabIndex = 7;
            this.numericUpDownAutoscanMinDepth20Mhz.Value = new decimal(new int[] {
            10,
            0,
            0,
            65536});
            this.numericUpDownAutoscanMinDepth20Mhz.Visible = false;
            this.numericUpDownAutoscanMinDepth20Mhz.ValueChanged += new System.EventHandler(this.numericUpDownAutoscanMinDepth20Mhz_ValueChanged);
            // 
            // lbMModeMaxDepth
            // 
            this.lbMModeMaxDepth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lbMModeMaxDepth.Location = new System.Drawing.Point(277, 15);
            this.lbMModeMaxDepth.Name = "lbMModeMaxDepth";
            this.lbMModeMaxDepth.Size = new System.Drawing.Size(121, 34);
            this.lbMModeMaxDepth.TabIndex = 6;
            this.lbMModeMaxDepth.Text = "M-Mode Max Depth (64 Gates)";
            this.lbMModeMaxDepth.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelAutoscanMaxDepth2Mhz
            // 
            this.labelAutoscanMaxDepth2Mhz.Location = new System.Drawing.Point(312, 82);
            this.labelAutoscanMaxDepth2Mhz.Name = "labelAutoscanMaxDepth2Mhz";
            this.labelAutoscanMaxDepth2Mhz.Size = new System.Drawing.Size(48, 16);
            this.labelAutoscanMaxDepth2Mhz.TabIndex = 6;
            this.labelAutoscanMaxDepth2Mhz.Text = "2Mhz";
            this.labelAutoscanMaxDepth2Mhz.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelAutoscanMaxDepth1Mhz
            // 
            this.labelAutoscanMaxDepth1Mhz.Location = new System.Drawing.Point(312, 58);
            this.labelAutoscanMaxDepth1Mhz.Name = "labelAutoscanMaxDepth1Mhz";
            this.labelAutoscanMaxDepth1Mhz.Size = new System.Drawing.Size(48, 16);
            this.labelAutoscanMaxDepth1Mhz.TabIndex = 6;
            this.labelAutoscanMaxDepth1Mhz.Text = "1Mhz";
            this.labelAutoscanMaxDepth1Mhz.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelAutoscanMaxDepth4Mhz
            // 
            this.labelAutoscanMaxDepth4Mhz.Location = new System.Drawing.Point(312, 106);
            this.labelAutoscanMaxDepth4Mhz.Name = "labelAutoscanMaxDepth4Mhz";
            this.labelAutoscanMaxDepth4Mhz.Size = new System.Drawing.Size(48, 16);
            this.labelAutoscanMaxDepth4Mhz.TabIndex = 6;
            this.labelAutoscanMaxDepth4Mhz.Text = "4Mhz";
            this.labelAutoscanMaxDepth4Mhz.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelAutoscanMaxDepth8Mhz
            // 
            this.labelAutoscanMaxDepth8Mhz.Location = new System.Drawing.Point(312, 130);
            this.labelAutoscanMaxDepth8Mhz.Name = "labelAutoscanMaxDepth8Mhz";
            this.labelAutoscanMaxDepth8Mhz.Size = new System.Drawing.Size(48, 16);
            this.labelAutoscanMaxDepth8Mhz.TabIndex = 6;
            this.labelAutoscanMaxDepth8Mhz.Text = "8Mhz";
            this.labelAutoscanMaxDepth8Mhz.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelAutoscanMaxDepth16Mhz
            // 
            this.labelAutoscanMaxDepth16Mhz.Location = new System.Drawing.Point(312, 154);
            this.labelAutoscanMaxDepth16Mhz.Name = "labelAutoscanMaxDepth16Mhz";
            this.labelAutoscanMaxDepth16Mhz.Size = new System.Drawing.Size(48, 16);
            this.labelAutoscanMaxDepth16Mhz.TabIndex = 6;
            this.labelAutoscanMaxDepth16Mhz.Text = "16Mhz";
            this.labelAutoscanMaxDepth16Mhz.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelAutoscanMaxDepth20Mhz
            // 
            this.labelAutoscanMaxDepth20Mhz.Location = new System.Drawing.Point(312, 173);
            this.labelAutoscanMaxDepth20Mhz.Name = "labelAutoscanMaxDepth20Mhz";
            this.labelAutoscanMaxDepth20Mhz.Size = new System.Drawing.Size(48, 16);
            this.labelAutoscanMaxDepth20Mhz.TabIndex = 6;
            this.labelAutoscanMaxDepth20Mhz.Text = "20Mhz";
            this.labelAutoscanMaxDepth20Mhz.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelAutoscanMaxDepth20Mhz.Visible = false;
            // 
            // comboBoxDepthStep20Mhz
            // 
            this.comboBoxDepthStep20Mhz.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxDepthStep20Mhz.Location = new System.Drawing.Point(96, 168);
            this.comboBoxDepthStep20Mhz.Name = "comboBoxDepthStep20Mhz";
            this.comboBoxDepthStep20Mhz.Size = new System.Drawing.Size(80, 21);
            this.comboBoxDepthStep20Mhz.TabIndex = 1;
            this.comboBoxDepthStep20Mhz.Visible = false;
            this.comboBoxDepthStep20Mhz.SelectedValueChanged += new System.EventHandler(this.comboBoxDepthStep20Mhz_SelectedValueChanged);
            // 
            // comboBoxFftSize
            // 
            this.comboBoxFftSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxFftSize.Location = new System.Drawing.Point(84, 8);
            this.comboBoxFftSize.Name = "comboBoxFftSize";
            this.comboBoxFftSize.Size = new System.Drawing.Size(64, 21);
            this.comboBoxFftSize.TabIndex = 8;
            // 
            // albFFTSize
            // 
            this.albFFTSize.Location = new System.Drawing.Point(16, 8);
            this.albFFTSize.Name = "albFFTSize";
            this.albFFTSize.Size = new System.Drawing.Size(73, 23);
            this.albFFTSize.TabIndex = 7;
            this.albFFTSize.Text = "FFT Size";
            this.albFFTSize.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxTimeDomainTime
            // 
            this.textBoxTimeDomainTime.Location = new System.Drawing.Point(192, 264);
            this.textBoxTimeDomainTime.Name = "textBoxTimeDomainTime";
            this.textBoxTimeDomainTime.Size = new System.Drawing.Size(48, 20);
            this.textBoxTimeDomainTime.TabIndex = 16;
            this.textBoxTimeDomainTime.Visible = false;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.dsConfiguation1, "tb_BasicConfiguration.Diagnostic_Buffer", true));
            this.numericUpDown1.Location = new System.Drawing.Point(168, 8);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(56, 20);
            this.numericUpDown1.TabIndex = 17;
            this.numericUpDown1.Visible = false;
            // 
            // checkBoxEnableDefaultBVDepth
            // 
            this.checkBoxEnableDefaultBVDepth.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.dsConfiguation1, "tb_BasicConfiguration.EnableDefaultBVDepth", true));
            this.checkBoxEnableDefaultBVDepth.Location = new System.Drawing.Point(232, 16);
            this.checkBoxEnableDefaultBVDepth.Name = "checkBoxEnableDefaultBVDepth";
            this.checkBoxEnableDefaultBVDepth.Size = new System.Drawing.Size(216, 40);
            this.checkBoxEnableDefaultBVDepth.TabIndex = 18;
            this.checkBoxEnableDefaultBVDepth.Text = "Enable Blood Vessels Default Depth";
            this.checkBoxEnableDefaultBVDepth.Visible = false;
            // 
            // dsProbe1
            // 
            this.dsProbe1.DataSetName = "dsProbe";
            this.dsProbe1.Locale = new System.Globalization.CultureInfo("he-IL");
            this.dsProbe1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;


            // 
            // gbParameters
            // 

            this.gbParameters.Controls.Add(this.radioButtonAveraging);
            this.gbParameters.Controls.Add(this.radioButtonBeatByBeat);

            this.gbParameters.Location = new System.Drawing.Point(17, 228);
            this.gbParameters.Name = "gbParameters";
            this.gbParameters.Size = new System.Drawing.Size(423, 40);
            this.gbParameters.TabIndex = 11;
            this.gbParameters.TabStop = false;
            this.gbParameters.Text = "Clinical parameters calculation";
            // 
            // radioButtonAveraging
            // 
            this.radioButtonAveraging.Checked = true;
            this.radioButtonAveraging.Location = new System.Drawing.Point(32, 16);
            this.radioButtonAveraging.Name = "radioButtonAveraging";
            this.radioButtonAveraging.Size = new System.Drawing.Size(89, 16);
            this.radioButtonAveraging.TabIndex = 0;
            this.radioButtonAveraging.TabStop = true;
            this.radioButtonAveraging.Text = "Averaging";
            //this.radioButtonAveraging.CheckedChanged += new System.EventHandler(this.radioButtonAveraging_CheckedChanged);
            

            // 
            // radioButtonBeatByBeat
            // 
            this.radioButtonBeatByBeat.Location = new System.Drawing.Point(232, 16);
            this.radioButtonBeatByBeat.Name = "radioButtonBeatByBeat";
            this.radioButtonBeatByBeat.Size = new System.Drawing.Size(88, 16);
            this.radioButtonBeatByBeat.TabIndex = 1;
            this.radioButtonBeatByBeat.TabStop = true;
            this.radioButtonBeatByBeat.Text = "Beat by beat";
            this.radioButtonBeatByBeat.CheckedChanged += new System.EventHandler(this.radioButtonAveraging_CheckedChanged);

            // 
            // SetUpBasicConfiguration
            // 
            this.Controls.Add(this.checkBoxEnableDefaultBVDepth);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.textBoxTimeDomainTime);
            this.Controls.Add(this.gbVolume);
            this.Controls.Add(this.gbDepth);
            this.Controls.Add(this.gbParameters);
            this.Controls.Add(this.comboBoxFftSize);
            this.Controls.Add(this.albFFTSize);
            this.Name = "SetUpBasicConfiguration";
            this.Size = new System.Drawing.Size(464, 368);
            this.Load += new System.EventHandler(this.SetUpBasicConfiguration_Load);
            this.gbVolume.ResumeLayout(false);
            this.gbVolume.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarHeadphoneVolume)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsConfiguation1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarLoudspeakerVolume)).EndInit();
            this.gbDepth.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAutoscanMinDepth1Mhz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAutoscanMinDepth2Mhz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAutoscanMinDepth4Mhz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAutoscanMinDepth8Mhz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAutoscanMinDepth16Mhz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownAutoscanMinDepth20Mhz)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsProbe1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbVolume;
        private System.Windows.Forms.RadioButton radioButtonHeadphone;
        private System.Windows.Forms.TrackBar trackBarLoudspeakerVolume;
        private System.Windows.Forms.RadioButton radioButtonLoudspeaker;

        private System.Windows.Forms.GroupBox gbParameters;
        private System.Windows.Forms.RadioButton radioButtonAveraging;
        private System.Windows.Forms.RadioButton radioButtonBeatByBeat;
        public System.Windows.Forms.CheckBox checkBoxAutoMuteEnable;
        
        private System.Windows.Forms.GroupBox gbDepth;
        private System.Windows.Forms.ComboBox comboBoxDepthStep16Mhz;
        private System.Windows.Forms.ComboBox comboBoxDepthStep2Mhz;
        private System.Windows.Forms.ComboBox comboBoxFftSize;
        private System.Windows.Forms.Label albFFTSize;
        private System.Windows.Forms.TextBox textBoxTimeDomainTime;
        private System.Windows.Forms.ComboBox comboBoxDepthStep1Mhz;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbProbe;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBoxDepthStep4Mhz;
        private System.Windows.Forms.ComboBox comboBoxDepthStep8Mhz;
        private System.Windows.Forms.Label lbDepthStep;
        private System.Windows.Forms.Label lbMModeMinDepth;
        private System.Windows.Forms.NumericUpDown numericUpDownAutoscanMinDepth1Mhz;
        private System.Windows.Forms.NumericUpDown numericUpDownAutoscanMinDepth2Mhz;
        private System.Windows.Forms.NumericUpDown numericUpDownAutoscanMinDepth4Mhz;
        private System.Windows.Forms.NumericUpDown numericUpDownAutoscanMinDepth16Mhz;
        private System.Windows.Forms.NumericUpDown numericUpDownAutoscanMinDepth20Mhz;
        private System.Windows.Forms.Label lbMModeMaxDepth;
        private System.Windows.Forms.Label labelAutoscanMaxDepth2Mhz;
        private System.Windows.Forms.Label labelAutoscanMaxDepth1Mhz;
        private System.Windows.Forms.Label labelAutoscanMaxDepth4Mhz;
        private System.Windows.Forms.Label labelAutoscanMaxDepth8Mhz;
        private System.Windows.Forms.Label labelAutoscanMaxDepth16Mhz;
        private System.Windows.Forms.Label labelAutoscanMaxDepth20Mhz;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private TCD.DAL.dsConfiguation dsConfiguation1;
        private System.Windows.Forms.NumericUpDown numericUpDownAutoscanMinDepth8Mhz;
        private TCD.DAL.dsProbe dsProbe1;
        private System.Windows.Forms.CheckBox checkBoxEnableDefaultBVDepth;
        private System.Windows.Forms.TrackBar trackBarHeadphoneVolume;
        private System.Windows.Forms.ComboBox comboBoxDepthStep20Mhz;
        private System.ComponentModel.IContainer components = null;
    }
}