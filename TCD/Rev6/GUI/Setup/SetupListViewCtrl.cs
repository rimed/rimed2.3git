namespace Rimed.TCD.GUI.Setup
{
    public partial class SetupListViewCtrl : SetUpBase
    {
        private bool m_restoreManufacturerDefaults = false;

        public SetupListViewCtrl()
        {
            this.InitializeComponent();
            this.reloadControlLabels();
            this.BackColor = GlobalSettings.BackgroundDlg;
        }

        private void reloadControlLabels()
        {
            var strRes = MainForm.StringManager;
            this.lbCaption.Text = strRes.GetString("Empty Form");
            this.labelTextWnd.Text = strRes.GetString("In the selected item there is nothing to do, please select sub item in order to change the configuration of the system.");
            this.chkBoxRestoreManufacturerDefaults.Text = strRes.GetString("Restore Manufacturer Defaults");
        }

        private void chkBoxRestoreManufacturerDefaults_CheckedChanged(object sender, System.EventArgs e)
        {
            this.m_restoreManufacturerDefaults = chkBoxRestoreManufacturerDefaults.Checked;
        }

        public bool RestoreManufacturerDefaults()
        {
            return this.m_restoreManufacturerDefaults;
        }

        public string Caption
        {
            set
            {
                this.lbCaption.Text = value;
            }
        }
    }
}

