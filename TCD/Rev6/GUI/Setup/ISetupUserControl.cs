﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rimed.TCD.GUI.Setup
{
	public interface ISetupUserControl
	{
		void PrepareCommitChanges();
	}
}
