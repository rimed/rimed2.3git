using Rimed.TCD.GUI.DSPMgr;
using Rimed.TCD.DAL;

namespace Rimed.TCD.GUI.Setup
{
    /// <summary>
    /// Summary description for SystemSetUp.
    /// </summary>
    public partial class SetUpMMode : SetUpBase
    {
        public SetUpMMode()
        {
            this.InitializeComponent();
            this.reloadControlLabels();
            this.BackColor = GlobalSettings.BackgroundDlg;
            this.dsConfiguation1.Merge(RimedDal.Instance.ConfigDS);
        }

        private void reloadControlLabels()
        {
            var strRes = MainForm.StringManager;
            albThreshold1.Text = strRes.GetString("Threshold") + " 1";
            lbCaption.Text = strRes.GetString("M-mode Setup");
            albThreshold2.Text = strRes.GetString("Threshold") + " 2";
        }

        public override bool Verify()
        {
            setMModeProperties();

            // ... TODO
            BindingContext[dsConfiguation1.tb_BasicConfiguration].EndCurrentEdit();
            if (dsConfiguation1.HasChanges())
            {
                RimedDal.Instance.ConfigDS.Merge(dsConfiguation1);

				DspManager.Instance.WriteMModeThreshold2DSP(dsConfiguation1.tb_BasicConfiguration[0].M_ModeThreshould1, dsConfiguation1.tb_BasicConfiguration[0].M_ModeThreshould2);
            }
            return true;
        }

        private void SetUpMMode_Load(object sender, System.EventArgs e)
        {
            numericUpDownMModeThres1.Value = dsConfiguation1.tb_BasicConfiguration[0].M_ModeThreshould1;
            numericUpDownMModeThres2.Value = dsConfiguation1.tb_BasicConfiguration[0].M_ModeThreshould2;
        }

        private void setMModeProperties()
        {
            dsConfiguation1.tb_BasicConfiguration[0].M_ModeThreshould1 = (int)numericUpDownMModeThres1.Value;
            dsConfiguation1.tb_BasicConfiguration[0].M_ModeThreshould2 = (int)numericUpDownMModeThres2.Value;
        }
    }
}
