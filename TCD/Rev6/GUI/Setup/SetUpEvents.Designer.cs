﻿namespace Rimed.TCD.GUI.Setup
{
    partial class SetUpEvents
    {
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetUpEvents));
            this.listBoxEvents = new System.Windows.Forms.ListBox();
            this.lbPredEventList = new System.Windows.Forms.Label();
            this.textBoxNewEvent = new System.Windows.Forms.TextBox();
            this.bMoveDown = new System.Windows.Forms.Button();
            this.bMoveUp = new System.Windows.Forms.Button();
            this.bRemove = new System.Windows.Forms.Button();
            this.bAdd = new System.Windows.Forms.Button();
            this.dsEvents1 = new TCD.DAL.dsEvents();
            this.lbDuration = new System.Windows.Forms.Label();
            this.cbDuration = new AutoCompleteComboBox();
            this.lbSec = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dsEvents1)).BeginInit();
            this.SuspendLayout();
            // 
            // listBoxEvents
            // 
            this.listBoxEvents.Items.AddRange(new object[] {
            "CO2",
            "CO2 Stimulation",
            "Drug",
            "Hits"});
            this.listBoxEvents.Location = new System.Drawing.Point(40, 56);
            this.listBoxEvents.Name = "listBoxEvents";
            this.listBoxEvents.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listBoxEvents.Size = new System.Drawing.Size(216, 134);
            this.listBoxEvents.TabIndex = 29;
            this.listBoxEvents.SelectedIndexChanged += new System.EventHandler(this.listBoxEvents_SelectedIndexChanged);
            // 
            // lbPredEventList
            // 
            this.lbPredEventList.Location = new System.Drawing.Point(40, 24);
            this.lbPredEventList.Name = "lbPredEventList";
            this.lbPredEventList.Size = new System.Drawing.Size(265, 23);
            this.lbPredEventList.TabIndex = 30;
            this.lbPredEventList.Text = "Predefined Event List";
            // 
            // textBoxNewEvent
            // 
            this.textBoxNewEvent.Location = new System.Drawing.Point(40, 272);
            this.textBoxNewEvent.Name = "textBoxNewEvent";
            this.textBoxNewEvent.Size = new System.Drawing.Size(216, 20);
            this.textBoxNewEvent.TabIndex = 31;
            this.textBoxNewEvent.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // bMoveDown
            // 
            this.bMoveDown.Enabled = false;
            this.bMoveDown.Image = ((System.Drawing.Image)(resources.GetObject("bMoveDown.Image")));
            this.bMoveDown.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.bMoveDown.Location = new System.Drawing.Point(304, 224);
            this.bMoveDown.Name = "bMoveDown";
            this.bMoveDown.Size = new System.Drawing.Size(85, 50);
            this.bMoveDown.TabIndex = 37;
            this.bMoveDown.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.bMoveDown.Click += new System.EventHandler(this.bMoveDown_Click);
            // 
            // bMoveUp
            // 
            this.bMoveUp.Enabled = false;
            this.bMoveUp.Image = ((System.Drawing.Image)(resources.GetObject("bMoveUp.Image")));
            this.bMoveUp.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.bMoveUp.Location = new System.Drawing.Point(304, 168);
            this.bMoveUp.Name = "bMoveUp";
            this.bMoveUp.Size = new System.Drawing.Size(85, 50);
            this.bMoveUp.TabIndex = 36;
            this.bMoveUp.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.bMoveUp.Click += new System.EventHandler(this.bMoveUp_Click);
            // 
            // bRemove
            // 
            this.bRemove.Enabled = false;
            this.bRemove.Image = ((System.Drawing.Image)(resources.GetObject("bRemove.Image")));
            this.bRemove.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.bRemove.Location = new System.Drawing.Point(304, 112);
            this.bRemove.Name = "bRemove";
            this.bRemove.Size = new System.Drawing.Size(85, 50);
            this.bRemove.TabIndex = 35;
            this.bRemove.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.bRemove.Click += new System.EventHandler(this.bRemove_Click);
            // 
            // bAdd
            // 
            this.bAdd.Enabled = false;
            this.bAdd.Image = ((System.Drawing.Image)(resources.GetObject("bAdd.Image")));
            this.bAdd.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.bAdd.Location = new System.Drawing.Point(304, 56);
            this.bAdd.Name = "bAdd";
            this.bAdd.Size = new System.Drawing.Size(85, 50);
            this.bAdd.TabIndex = 34;
            this.bAdd.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.bAdd.Click += new System.EventHandler(this.bAdd_Click);
            // 
            // dsEvents1
            // 
            this.dsEvents1.DataSetName = "dsEvents";
            this.dsEvents1.Locale = new System.Globalization.CultureInfo("he-IL");
            this.dsEvents1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lbDuration
            // 
            this.lbDuration.Location = new System.Drawing.Point(-9, 244);
            this.lbDuration.Margin = new System.Windows.Forms.Padding(0);
            this.lbDuration.Name = "lbDuration";
            this.lbDuration.Size = new System.Drawing.Size(206, 18);
            this.lbDuration.TabIndex = 40;
            this.lbDuration.Text = "Start Play Event Before and After";
            this.lbDuration.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cbDuration
            // 
            this.cbDuration.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDuration.LimitToList = true;
            this.cbDuration.Location = new System.Drawing.Point(200, 241);
            this.cbDuration.Name = "cbDuration";
            this.cbDuration.Size = new System.Drawing.Size(56, 21);
            this.cbDuration.TabIndex = 41;
            this.cbDuration.SelectedIndexChanged += new System.EventHandler(this.cbDuration_SelectedIndexChanged);
            // 
            // lbSec
            // 
            this.lbSec.Location = new System.Drawing.Point(262, 244);
            this.lbSec.Name = "lbSec";
            this.lbSec.Size = new System.Drawing.Size(31, 16);
            this.lbSec.TabIndex = 42;
            this.lbSec.Text = "Sec";
            // 
            // SetUpEvents
            // 
            this.Controls.Add(this.lbSec);
            this.Controls.Add(this.cbDuration);
            this.Controls.Add(this.lbDuration);
            this.Controls.Add(this.bMoveDown);
            this.Controls.Add(this.bMoveUp);
            this.Controls.Add(this.bRemove);
            this.Controls.Add(this.bAdd);
            this.Controls.Add(this.textBoxNewEvent);
            this.Controls.Add(this.lbPredEventList);
            this.Controls.Add(this.listBoxEvents);
            this.Name = "SetUpEvents";
            this.Load += new System.EventHandler(this.SetUpEvents_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dsEvents1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ListBox listBoxEvents;
        private System.Windows.Forms.Label lbPredEventList;
        private System.Windows.Forms.Button bMoveDown;
        private System.Windows.Forms.Button bMoveUp;
        private System.Windows.Forms.Button bRemove;
        private System.Windows.Forms.Button bAdd;
        private System.Windows.Forms.TextBox textBoxNewEvent;
        private TCD.DAL.dsEvents dsEvents1;
        private System.ComponentModel.Container components = null;
        private System.Windows.Forms.Label lbDuration;
        private AutoCompleteComboBox cbDuration;
        private System.Windows.Forms.Label lbSec;
    }
}