namespace Rimed.TCD.GUI.Setup
{
    public partial class SetupClinicalParameters : SetUpBase
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetupClinicalParameters));
            this.m_CPListBox = new System.Windows.Forms.ListBox();
            this.bMoveDown = new System.Windows.Forms.Button();
            this.bMoveUp = new System.Windows.Forms.Button();
            this.m_CPListBoxOrder = new System.Windows.Forms.ListBox();
            this.bRemove = new System.Windows.Forms.Button();
            this.bAdd = new System.Windows.Forms.Button();
            this.dsClinicalParamSetup1 = new TCD.DAL.dsClinicalParamSetup();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dsClinicalParamSetup1)).BeginInit();
            this.SuspendLayout();
            // 
            // m_CPListBox
            // 
            this.m_CPListBox.Items.AddRange(new object[] {
            "Peak",
            "Mean",
            "DV",
            "RV",
            "S/D",
            "P.I."});
            this.m_CPListBox.Location = new System.Drawing.Point(32, 64);
            this.m_CPListBox.Name = "m_CPListBox";
            this.m_CPListBox.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.m_CPListBox.Size = new System.Drawing.Size(112, 225);
            this.m_CPListBox.TabIndex = 27;
            this.m_CPListBox.SelectedIndexChanged += new System.EventHandler(this.m_CPListBox_SelectedIndexChanged);
            // 
            // bMoveDown
            // 
            this.bMoveDown.Enabled = false;
            this.bMoveDown.Image = ((System.Drawing.Image)(resources.GetObject("bMoveDown.Image")));
            this.bMoveDown.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.bMoveDown.Location = new System.Drawing.Point(176, 232);
            this.bMoveDown.Name = "bMoveDown";
            this.bMoveDown.Size = new System.Drawing.Size(85, 50);
            this.bMoveDown.TabIndex = 35;
            this.bMoveDown.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.bMoveDown.Click += new System.EventHandler(this.bMoveDown_Click);
            // 
            // bMoveUp
            // 
            this.bMoveUp.Enabled = false;
            this.bMoveUp.Image = ((System.Drawing.Image)(resources.GetObject("bMoveUp.Image")));
            this.bMoveUp.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.bMoveUp.Location = new System.Drawing.Point(176, 176);
            this.bMoveUp.Name = "bMoveUp";
            this.bMoveUp.Size = new System.Drawing.Size(85, 50);
            this.bMoveUp.TabIndex = 34;
            this.bMoveUp.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.bMoveUp.Click += new System.EventHandler(this.bMoveUp_Click);
            // 
            // m_CPListBoxOrder
            // 
            this.m_CPListBoxOrder.ColumnWidth = 10;
            this.m_CPListBoxOrder.Location = new System.Drawing.Point(288, 64);
            this.m_CPListBoxOrder.Name = "m_CPListBoxOrder";
            this.m_CPListBoxOrder.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.m_CPListBoxOrder.Size = new System.Drawing.Size(112, 225);
            this.m_CPListBoxOrder.TabIndex = 36;
            this.m_CPListBoxOrder.SelectedIndexChanged += new System.EventHandler(this.m_CPListBoxOrder_SelectedIndexChanged);
            // 
            // bRemove
            // 
            this.bRemove.Enabled = false;
            this.bRemove.Image = ((System.Drawing.Image)(resources.GetObject("bRemove.Image")));
            this.bRemove.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.bRemove.Location = new System.Drawing.Point(176, 120);
            this.bRemove.Name = "bRemove";
            this.bRemove.Size = new System.Drawing.Size(85, 50);
            this.bRemove.TabIndex = 38;
            this.bRemove.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.bRemove.Click += new System.EventHandler(this.bRemove_Click);
            // 
            // bAdd
            // 
            this.bAdd.Enabled = false;
            this.bAdd.Image = ((System.Drawing.Image)(resources.GetObject("bAdd.Image")));
            this.bAdd.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.bAdd.Location = new System.Drawing.Point(176, 64);
            this.bAdd.Name = "bAdd";
            this.bAdd.Size = new System.Drawing.Size(85, 50);
            this.bAdd.TabIndex = 37;
            this.bAdd.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.bAdd.Click += new System.EventHandler(this.bAdd_Click);
            // 
            // dsClinicalParamSetup1
            // 
            this.dsClinicalParamSetup1.DataSetName = "dsClinicalParamSetup";
            this.dsClinicalParamSetup1.Locale = new System.Globalization.CultureInfo("en-US");
            this.dsClinicalParamSetup1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(32, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 37);
            this.label1.TabIndex = 39;
            this.label1.Text = "Available Clinical Parameters";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(288, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 37);
            this.label2.TabIndex = 39;
            this.label2.Text = "Clinical Parameters By Order Of Appearance";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // SetupClinicalParameters
            // 
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bRemove);
            this.Controls.Add(this.bAdd);
            this.Controls.Add(this.m_CPListBoxOrder);
            this.Controls.Add(this.bMoveDown);
            this.Controls.Add(this.bMoveUp);
            this.Controls.Add(this.m_CPListBox);
            this.Controls.Add(this.label2);
            this.Name = "SetupClinicalParameters";
            this.Size = new System.Drawing.Size(424, 296);
            ((System.ComponentModel.ISupportInitialize)(this.dsClinicalParamSetup1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bMoveDown;
        private System.Windows.Forms.Button bMoveUp;
        private System.Windows.Forms.ListBox m_CPListBox;
        private System.Windows.Forms.Button bRemove;
        private System.Windows.Forms.Button bAdd;
        private System.Windows.Forms.ListBox m_CPListBoxOrder;
        private System.ComponentModel.IContainer components = null;
        private TCD.DAL.dsClinicalParamSetup dsClinicalParamSetup1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}