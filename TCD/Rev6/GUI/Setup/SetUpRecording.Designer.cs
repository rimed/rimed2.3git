namespace Rimed.TCD.GUI.Setup
{
    partial class SetUpRecording
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbRecord = new System.Windows.Forms.GroupBox();
            this.checkBoxReversePeak = new System.Windows.Forms.CheckBox();
            this.checkBoxReverseMode = new System.Windows.Forms.CheckBox();
            this.checkBoxForwardMode = new System.Windows.Forms.CheckBox();
            this.checkBoxForwardPeak = new System.Windows.Forms.CheckBox();
            this.dsStudyRecordingSetup1 = new TCD.DAL.dsStudyRecordingSetup();
            this.gbAutomaticEvent = new System.Windows.Forms.GroupBox();
            this.checkBoxHits = new System.Windows.Forms.CheckBox();
            this.checkBoxPredefinedEvents = new System.Windows.Forms.CheckBox();
            this.lbMin = new System.Windows.Forms.Label();
            this.numericUpDownMin = new System.Windows.Forms.NumericUpDown();
            this.checkBoxTime = new System.Windows.Forms.CheckBox();
            this.checkBoxAlarms = new System.Windows.Forms.CheckBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.gbRecordSpectrum = new System.Windows.Forms.GroupBox();
            this.numericUpDownSec = new System.Windows.Forms.NumericUpDown();
            this.rbAroundEvents = new System.Windows.Forms.RadioButton();
            this.rbContinuous = new System.Windows.Forms.RadioButton();
            this.lbSec = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbMin1 = new System.Windows.Forms.Label();
            this.numericUpDownEndTime = new System.Windows.Forms.NumericUpDown();
            this.checkBoxEndRecording = new System.Windows.Forms.CheckBox();
            this.gbRecord.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dsStudyRecordingSetup1)).BeginInit();
            this.gbAutomaticEvent.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMin)).BeginInit();
            this.gbRecordSpectrum.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSec)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownEndTime)).BeginInit();
            this.SuspendLayout();
            // 
            // gbRecord
            // 
            this.gbRecord.Controls.Add(this.checkBoxReversePeak);
            this.gbRecord.Controls.Add(this.checkBoxReverseMode);
            this.gbRecord.Controls.Add(this.checkBoxForwardMode);
            this.gbRecord.Controls.Add(this.checkBoxForwardPeak);
            this.gbRecord.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.gbRecord.Location = new System.Drawing.Point(24, 8);
            this.gbRecord.Name = "gbRecord";
            this.gbRecord.Size = new System.Drawing.Size(408, 80);
            this.gbRecord.TabIndex = 0;
            this.gbRecord.TabStop = false;
            this.gbRecord.Text = "Record Envelopes";
            this.gbRecord.Visible = false;
            // 
            // checkBoxReversePeak
            // 
            this.checkBoxReversePeak.Location = new System.Drawing.Point(184, 24);
            this.checkBoxReversePeak.Name = "checkBoxReversePeak";
            this.checkBoxReversePeak.Size = new System.Drawing.Size(152, 16);
            this.checkBoxReversePeak.TabIndex = 10;
            this.checkBoxReversePeak.Text = "Reverse Peak";
            // 
            // checkBoxReverseMode
            // 
            this.checkBoxReverseMode.Location = new System.Drawing.Point(184, 56);
            this.checkBoxReverseMode.Name = "checkBoxReverseMode";
            this.checkBoxReverseMode.Size = new System.Drawing.Size(185, 23);
            this.checkBoxReverseMode.TabIndex = 9;
            this.checkBoxReverseMode.Text = "Reverse Mode";
            // 
            // checkBoxForwardMode
            // 
            this.checkBoxForwardMode.Location = new System.Drawing.Point(24, 56);
            this.checkBoxForwardMode.Name = "checkBoxForwardMode";
            this.checkBoxForwardMode.Size = new System.Drawing.Size(154, 18);
            this.checkBoxForwardMode.TabIndex = 8;
            this.checkBoxForwardMode.Text = "Forward Mode";
            // 
            // checkBoxForwardPeak
            // 
            this.checkBoxForwardPeak.Location = new System.Drawing.Point(24, 24);
            this.checkBoxForwardPeak.Name = "checkBoxForwardPeak";
            this.checkBoxForwardPeak.Size = new System.Drawing.Size(145, 16);
            this.checkBoxForwardPeak.TabIndex = 7;
            this.checkBoxForwardPeak.Text = "Forward Peak";
            // 
            // dsStudyRecordingSetup1
            // 
            this.dsStudyRecordingSetup1.DataSetName = "dsStudyRecordingSetup";
            this.dsStudyRecordingSetup1.Locale = new System.Globalization.CultureInfo("he-IL");
            this.dsStudyRecordingSetup1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gbAutomaticEvent
            // 
            this.gbAutomaticEvent.Controls.Add(this.checkBoxHits);
            this.gbAutomaticEvent.Controls.Add(this.checkBoxPredefinedEvents);
            this.gbAutomaticEvent.Controls.Add(this.lbMin);
            this.gbAutomaticEvent.Controls.Add(this.numericUpDownMin);
            this.gbAutomaticEvent.Controls.Add(this.checkBoxTime);
            this.gbAutomaticEvent.Controls.Add(this.checkBoxAlarms);
	        this.gbAutomaticEvent.Enabled = true;	//OFER: false;
            this.gbAutomaticEvent.Location = new System.Drawing.Point(24, 152);
            this.gbAutomaticEvent.Name = "gbAutomaticEvent";
            this.gbAutomaticEvent.Size = new System.Drawing.Size(384, 88);
            this.gbAutomaticEvent.TabIndex = 1;
            this.gbAutomaticEvent.TabStop = false;
            this.gbAutomaticEvent.Text = "Automatic Event On :";
            // 
            // checkBoxHits
            // 
            this.checkBoxHits.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.dsStudyRecordingSetup1, "tb_StudyRecordingSetup.SaveSpectrumAroundHits", true));
            this.checkBoxHits.Location = new System.Drawing.Point(16, 23);
            this.checkBoxHits.Name = "checkBoxHits";
            this.checkBoxHits.Size = new System.Drawing.Size(75, 24);
            this.checkBoxHits.TabIndex = 18;
            this.checkBoxHits.Text = "Hits";
            // 
            // checkBoxPredefinedEvents
            // 
            this.checkBoxPredefinedEvents.Location = new System.Drawing.Point(137, 56);
            this.checkBoxPredefinedEvents.Name = "checkBoxPredefinedEvents";
            this.checkBoxPredefinedEvents.Size = new System.Drawing.Size(199, 24);
            this.checkBoxPredefinedEvents.TabIndex = 23;
            this.checkBoxPredefinedEvents.Text = "Predefined Events";
            this.checkBoxPredefinedEvents.Visible = false;
            // 
            // lbMin
            // 
            this.lbMin.Location = new System.Drawing.Point(345, 28);
            this.lbMin.Name = "lbMin";
            this.lbMin.Size = new System.Drawing.Size(24, 16);
            this.lbMin.TabIndex = 22;
            this.lbMin.Text = "Min";
            // 
            // numericUpDownMin
            // 
			this.numericUpDownMin.Enabled = true;	//OFER:  false;
            this.numericUpDownMin.Location = new System.Drawing.Point(291, 24);
            this.numericUpDownMin.Maximum = new decimal(new int[] {
            240,
            0,
            0,
            0});
            this.numericUpDownMin.Name = "numericUpDownMin";
            this.numericUpDownMin.Size = new System.Drawing.Size(48, 20);
            this.numericUpDownMin.TabIndex = 21;
            this.numericUpDownMin.KeyUp += new System.Windows.Forms.KeyEventHandler(this.numericUpDownMin_KeyUp);
            // 
            // checkBoxTime
            // 
            this.checkBoxTime.Location = new System.Drawing.Point(137, 23);
            this.checkBoxTime.Name = "checkBoxTime";
            this.checkBoxTime.Size = new System.Drawing.Size(164, 24);
            this.checkBoxTime.TabIndex = 20;
            this.checkBoxTime.Text = "Time - Event every";
            this.checkBoxTime.CheckedChanged += new System.EventHandler(this.checkBoxTime_CheckedChanged);
            // 
            // checkBoxAlarms
            // 
            this.checkBoxAlarms.Location = new System.Drawing.Point(16, 56);
            this.checkBoxAlarms.Name = "checkBoxAlarms";
            this.checkBoxAlarms.Size = new System.Drawing.Size(96, 24);
            this.checkBoxAlarms.TabIndex = 19;
            this.checkBoxAlarms.Text = "Alarms";
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(0, 100);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(200, 100);
            this.panel3.TabIndex = 0;
            // 
            // gbRecordSpectrum
            // 
            this.gbRecordSpectrum.Controls.Add(this.numericUpDownSec);
            this.gbRecordSpectrum.Controls.Add(this.rbAroundEvents);
            this.gbRecordSpectrum.Controls.Add(this.rbContinuous);
            this.gbRecordSpectrum.Controls.Add(this.lbSec);
            this.gbRecordSpectrum.Controls.Add(this.label3);
            this.gbRecordSpectrum.Location = new System.Drawing.Point(24, 47);
            this.gbRecordSpectrum.Name = "gbRecordSpectrum";
            this.gbRecordSpectrum.Size = new System.Drawing.Size(384, 80);
            this.gbRecordSpectrum.TabIndex = 3;
            this.gbRecordSpectrum.TabStop = false;
            this.gbRecordSpectrum.Text = "Record Spectrum";
            // 
            // numericUpDownSec
            // 
			this.numericUpDownSec.Enabled = true;	//OFER:  false;
            this.numericUpDownSec.Location = new System.Drawing.Point(168, 48);
            this.numericUpDownSec.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDownSec.Name = "numericUpDownSec";
            this.numericUpDownSec.Size = new System.Drawing.Size(40, 20);
            this.numericUpDownSec.TabIndex = 68;
            this.numericUpDownSec.KeyUp += new System.Windows.Forms.KeyEventHandler(this.numericUpDownSec_KeyUp);
            // 
            // rbAroundEvents
            // 
			this.rbAroundEvents.Enabled = true;	//OFER:  false;
            this.rbAroundEvents.Location = new System.Drawing.Point(16, 48);
            this.rbAroundEvents.Name = "rbAroundEvents";
            this.rbAroundEvents.Size = new System.Drawing.Size(104, 28);
            this.rbAroundEvents.TabIndex = 16;
            this.rbAroundEvents.Text = "Around Events";
            // 
            // rbContinuous
            // 
            this.rbContinuous.Checked = true;
            this.rbContinuous.Location = new System.Drawing.Point(16, 16);
            this.rbContinuous.Name = "rbContinuous";
            this.rbContinuous.Size = new System.Drawing.Size(104, 24);
            this.rbContinuous.TabIndex = 15;
            this.rbContinuous.TabStop = true;
            this.rbContinuous.Text = "Continuous";
            this.rbContinuous.CheckedChanged += new System.EventHandler(this.rbContinuous_CheckedChanged);
            // 
            // lbSec
            // 
            this.lbSec.Location = new System.Drawing.Point(216, 53);
            this.lbSec.Name = "lbSec";
            this.lbSec.Size = new System.Drawing.Size(32, 16);
            this.lbSec.TabIndex = 14;
            this.lbSec.Text = "Sec";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.label3.Location = new System.Drawing.Point(144, 53);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label3.Size = new System.Drawing.Size(16, 16);
            this.label3.TabIndex = 13;
            this.label3.Text = "+";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbMin1
            // 
            this.lbMin1.Location = new System.Drawing.Point(262, 295);
            this.lbMin1.Name = "lbMin1";
            this.lbMin1.Size = new System.Drawing.Size(32, 16);
            this.lbMin1.TabIndex = 24;
            this.lbMin1.Text = "Min";
            this.lbMin1.Visible = false;
            // 
            // numericUpDownEndTime
            // 
			this.numericUpDownEndTime.Enabled = true;	//OFER:  false;
            this.numericUpDownEndTime.Location = new System.Drawing.Point(208, 293);
            this.numericUpDownEndTime.Maximum = new decimal(new int[] {
            240,
            0,
            0,
            0});
            this.numericUpDownEndTime.Name = "numericUpDownEndTime";
            this.numericUpDownEndTime.Size = new System.Drawing.Size(48, 20);
            this.numericUpDownEndTime.TabIndex = 23;
            this.numericUpDownEndTime.Visible = false;
            // 
            // checkBoxEndRecording
            // 
            this.checkBoxEndRecording.Location = new System.Drawing.Point(48, 296);
            this.checkBoxEndRecording.Name = "checkBoxEndRecording";
            this.checkBoxEndRecording.Size = new System.Drawing.Size(154, 17);
            this.checkBoxEndRecording.TabIndex = 22;
            this.checkBoxEndRecording.Text = "End Recording After";
            this.checkBoxEndRecording.Visible = false;
            // 
            // SetUpRecording
            // 
            this.Controls.Add(this.lbMin1);
            this.Controls.Add(this.numericUpDownEndTime);
            this.Controls.Add(this.checkBoxEndRecording);
            this.Controls.Add(this.gbRecordSpectrum);
            this.Controls.Add(this.gbAutomaticEvent);
            this.Controls.Add(this.gbRecord);
            this.Name = "SetUpRecording";
            this.gbRecord.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dsStudyRecordingSetup1)).EndInit();
            this.gbAutomaticEvent.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMin)).EndInit();
            this.gbRecordSpectrum.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownSec)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownEndTime)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        private System.Windows.Forms.GroupBox gbRecord;
        private System.Windows.Forms.GroupBox gbAutomaticEvent;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.CheckBox checkBoxReversePeak;
        private System.Windows.Forms.CheckBox checkBoxReverseMode;
        private System.Windows.Forms.CheckBox checkBoxForwardMode;
        private System.Windows.Forms.CheckBox checkBoxForwardPeak;
        private System.Windows.Forms.GroupBox gbRecordSpectrum;
        private System.Windows.Forms.Label lbSec;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkBoxHits;
        private System.Windows.Forms.CheckBox checkBoxPredefinedEvents;
        private System.Windows.Forms.Label lbMin;
        private System.Windows.Forms.NumericUpDown numericUpDownMin;
        private System.Windows.Forms.CheckBox checkBoxTime;
        private System.Windows.Forms.CheckBox checkBoxAlarms;
        private System.Windows.Forms.Label lbMin1;
        private System.Windows.Forms.NumericUpDown numericUpDownEndTime;
        private System.Windows.Forms.CheckBox checkBoxEndRecording;
        private System.Windows.Forms.RadioButton rbContinuous;
        private TCD.DAL.dsStudyRecordingSetup dsStudyRecordingSetup1;
        public System.Windows.Forms.NumericUpDown numericUpDownSec;
        private System.Windows.Forms.RadioButton rbAroundEvents;
        private System.ComponentModel.IContainer components = null;
    }
}