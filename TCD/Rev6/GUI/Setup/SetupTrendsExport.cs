using System;
using Rimed.TCD.DAL;

namespace Rimed.TCD.GUI.Setup
{
	public partial class SetupTrendsExport : SetUpBase
	{
	    private string m_exportSeparator = string.Empty;

        public SetupTrendsExport()
		{
			InitializeComponent();
		}

	    public int ExportInterval { get; private set; }

	    public string ExportSeparator
		{
			get
			{
                return m_exportSeparator;
			}
		}

		private void cbExportInterval_SelectedIndexChanged(object sender, EventArgs e)
		{
            if (cbExportInterval.SelectedIndex == 2)
                ExportInterval = GlobalSettings.LONG_EXPORT_INTERVAL;
            else if (cbExportInterval.SelectedIndex == 1)
                ExportInterval = GlobalSettings.SHORT_EXPORT_INTERVAL;
            else
                ExportInterval = GlobalSettings.VERY_SHORT_EXPORT_INTERVAL;
		}

	    private void SetupTrendsExport_Load(object sender, EventArgs e)
		{
			var strRes = MainForm.StringManager;
			lbHeader.Text = strRes.GetString("Export trends to Excel");
			lbExport.Text = strRes.GetString("Export Clinical Parameters to Excel every:");
            cbExportInterval.Items.Add("2 " + strRes.GetString("Sec"));
			cbExportInterval.Items.Add("30 " + strRes.GetString("Sec"));
			cbExportInterval.Items.Add("1 " + strRes.GetString("minutes"));

            switch (this.ExportInterval)
			{
				case 0:
						ExportInterval = RimedDal.GeneralSettings.ExportInterval;
						if (ExportInterval <= 0)
						{
                            ExportInterval = GlobalSettings.VERY_SHORT_EXPORT_INTERVAL;
                            RimedDal.GeneralSettings.ExportInterval = GlobalSettings.VERY_SHORT_EXPORT_INTERVAL;
						}
						//var myKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Rimed", true);
						//if (myKey != null)
						//{
						//    if (myKey.GetValue("ExportInterval") != null)
						//    {
						//        ExportInterval	 = Convert.ToInt32(myKey.GetValue("ExportInterval"));
						//    }
						//    else
						//    {
						//        //Default time interval for export = 30 sec
						//        ExportInterval = GlobalSettings.SHORT_EXPORT_INTERVAL;
						//        myKey.SetValue("ExportInterval", GlobalSettings.SHORT_EXPORT_INTERVAL);
						//    }
						//}
						//else
						//    ExportInterval = GlobalSettings.SHORT_EXPORT_INTERVAL;
                        if (ExportInterval == GlobalSettings.SHORT_EXPORT_INTERVAL) 
                            cbExportInterval.SelectedIndex = 1;
                        else if (ExportInterval == GlobalSettings.LONG_EXPORT_INTERVAL)
                            cbExportInterval.SelectedIndex = 2;
                        else
                            cbExportInterval.SelectedIndex = 0;
					break;
                case GlobalSettings.VERY_SHORT_EXPORT_INTERVAL:
                    cbExportInterval.SelectedIndex = 0;
                    break;
				case GlobalSettings.LONG_EXPORT_INTERVAL:
                        cbExportInterval.SelectedIndex = 2;
					break;
				default:
                        cbExportInterval.SelectedIndex = 0;
					break;
			}

			lbSeparator.Text = strRes.GetString("Field separator:");
			cbSeparator.Items.Add(GlobalSettings.COMMA_SEPARATOR);
			cbSeparator.Items.Add(GlobalSettings.SEMICOLUMN_SEPARATOR);

			switch (m_exportSeparator)
			{
				case "":
					m_exportSeparator = RimedDal.GeneralSettings.ExportSeparator;
					if (string.IsNullOrWhiteSpace(m_exportSeparator))
					{
						m_exportSeparator						= GlobalSettings.COMMA_SEPARATOR;
						RimedDal.GeneralSettings.ExportSeparator = GlobalSettings.COMMA_SEPARATOR;
					}

					//var myKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Rimed", true);
					//    if (myKey != null)
					//    {
					//        if (myKey.GetValue("ExportSeparator") != null)
					//        {
					//            m_exportSeparator = Convert.ToString(myKey.GetValue("ExportSeparator"));
					//        }
					//        else
					//        {
					//            //Default separator for the export file is comma
					//            m_exportSeparator = GlobalSettings.COMMA_SEPARATOR;
					//            myKey.SetValue("ExportSeparator", GlobalSettings.COMMA_SEPARATOR);
					//        }
					//    }
					//    else
					//        m_exportSeparator = GlobalSettings.COMMA_SEPARATOR;

                        cbSeparator.SelectedIndex = (m_exportSeparator == GlobalSettings.COMMA_SEPARATOR) ? 0 : 1;
					break;
				case GlobalSettings.SEMICOLUMN_SEPARATOR:
                        cbSeparator.SelectedIndex = 1;
					break;
				default:
                        cbSeparator.SelectedIndex = 0;
					break;
			}
		}

		private void cbSeparator_SelectedIndexChanged(object sender, EventArgs e)
		{
		    m_exportSeparator = (cbSeparator.SelectedIndex == 1) ? GlobalSettings.SEMICOLUMN_SEPARATOR : GlobalSettings.COMMA_SEPARATOR;
		}
	}
}

