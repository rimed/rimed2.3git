using System;
using Rimed.TCD.DAL;

namespace Rimed.TCD.GUI.Setup
{
    public partial class SetupSummaryScreen : SetUpBase
    {
        public SetupSummaryScreen()
        {
            InitializeComponent();
            reloadControlLabels();
            BackColor = GlobalSettings.BackgroundDlg;
            dsConfiguation1.Merge(RimedDal.Instance.ConfigDS);
        }

        private void reloadControlLabels()
        {
            var strRes = MainForm.StringManager;
            lbCaption.Text = strRes.GetString("Summary Screen Setup");
            radioButtonAddAllOpenSpectrums.Text = strRes.GetString("Add all open spectrums");
            radioButtonAddOnlyselectedSpectrum.Text = strRes.GetString("Add only selected spectrum");
            radioButtonCustomizeMultipleTestsOnSameBVAndDepth.Text = strRes.GetString("Customize multiple tests on same BV and depth");
            radioButtonReplaceOld.Text = strRes.GetString("Replace old");
            radioButtonAddNew.Text = strRes.GetString("Add new");
            radioButtonAskForEachTest.Text = strRes.GetString("Ask for each test");
            checkSaveOnlyImages.Text = strRes.GetString("Save only summary images (No replay Option)");
        }

        private void radioButtonCustomizeMultipleTestsOnSameBVAndDepth_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonCustomizeMultipleTestsOnSameBVAndDepth.Checked)
            {
                radioButtonAddNew.Enabled = true;
                radioButtonAskForEachTest.Enabled = true;
                radioButtonReplaceOld.Enabled = true;

                radioButtonReplaceOld.Checked = true;
            }
            else
            {
                radioButtonAddNew.Enabled = false;
                radioButtonAskForEachTest.Enabled = false;
                radioButtonReplaceOld.Enabled = false;

                radioButtonAddNew.Checked = false;
                radioButtonAskForEachTest.Checked = false;
                radioButtonReplaceOld.Checked = false;
            }
        }

        private void SetupSummaryScreen_Load(object sender, System.EventArgs e)
        {
            radioButtonAddNew.Enabled = false;
            radioButtonAskForEachTest.Enabled = false;
            radioButtonReplaceOld.Enabled = false;

            checkSaveOnlyImages.Checked = dsConfiguation1.tb_BasicConfiguration[0].SaveOnlySummaryImages;

            switch (dsConfiguation1.tb_BasicConfiguration[0].Add2SummaryScreen)
            {
                case (int)GlobalTypes.EAdd2SummaryScreen.OpenSpectrum:
                    {
                        radioButtonAddAllOpenSpectrums.Select();
                        break;
                    }
                case (int)GlobalTypes.EAdd2SummaryScreen.SelectedSpectrum:
                    {
                        radioButtonAddOnlyselectedSpectrum.Select();
                        break;
                    }
                case (int)GlobalTypes.EAdd2SummaryScreen.CustomizeReplaceOld:
                    {
                        radioButtonCustomizeMultipleTestsOnSameBVAndDepth.Select();
                        radioButtonReplaceOld.Select();
                        break;
                    }
                case (int)GlobalTypes.EAdd2SummaryScreen.CustomizeAddNew:
                    {
                        radioButtonCustomizeMultipleTestsOnSameBVAndDepth.Select();
                        radioButtonAddNew.Select();
                        break;
                    }
                case (int)GlobalTypes.EAdd2SummaryScreen.CustomizeAsk:
                    {
                        radioButtonCustomizeMultipleTestsOnSameBVAndDepth.Select();
                        radioButtonAskForEachTest.Select();
                        break;
                    }
            }
        }
        public override bool Verify()
        {
            BindingContext[dsConfiguation1.tb_BasicConfiguration].EndCurrentEdit();

            if (radioButtonAddAllOpenSpectrums.Checked)
                dsConfiguation1.tb_BasicConfiguration[0].Add2SummaryScreen = (int)GlobalTypes.EAdd2SummaryScreen.OpenSpectrum;
            else if (radioButtonAddOnlyselectedSpectrum.Checked)
                dsConfiguation1.tb_BasicConfiguration[0].Add2SummaryScreen = (int)GlobalTypes.EAdd2SummaryScreen.SelectedSpectrum;
            else if (radioButtonCustomizeMultipleTestsOnSameBVAndDepth.Checked)
            {
                if (radioButtonReplaceOld.Checked)
                    dsConfiguation1.tb_BasicConfiguration[0].Add2SummaryScreen = (int)GlobalTypes.EAdd2SummaryScreen.CustomizeReplaceOld;
                else if (radioButtonAddNew.Checked)
                    dsConfiguation1.tb_BasicConfiguration[0].Add2SummaryScreen = (int)GlobalTypes.EAdd2SummaryScreen.CustomizeAddNew;
                else if (radioButtonAskForEachTest.Checked)
                    dsConfiguation1.tb_BasicConfiguration[0].Add2SummaryScreen = (int)GlobalTypes.EAdd2SummaryScreen.CustomizeAsk;
            }

            this.dsConfiguation1.tb_BasicConfiguration[0].SaveOnlySummaryImages = this.checkSaveOnlyImages.Checked;

            if (this.dsConfiguation1.HasChanges())
            {
                RimedDal.Instance.ConfigDS.Merge(dsConfiguation1);
                BVListViewCtrl.Instance.InSummaryStatus = (GlobalTypes.EAdd2SummaryScreen)this.dsConfiguation1.tb_BasicConfiguration[0].Add2SummaryScreen;
            }
            return true;
        }
    }
}

