namespace Rimed.TCD.GUI.Setup
{
    partial class SetUpExternChannelConfig
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbCaption = new System.Windows.Forms.Label();
            this.gradientPanel1 = new Syncfusion.Windows.Forms.Tools.GradientPanel();
            this.gridDataBoundGrid1 = new Syncfusion.Windows.Forms.Grid.GridDataBoundGrid();
            this.dsChannel1 = new TCD.DAL.dsChannel();
            this.xpToolBar1 = new Syncfusion.Windows.Forms.Tools.XPMenus.XPToolBar();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.gbAlarmSettings = new System.Windows.Forms.GroupBox();
            this.numAlarmHigh = new System.Windows.Forms.NumericUpDown();
            this.numAlarmLow = new System.Windows.Forms.NumericUpDown();
            this.lblAlarmHigh = new System.Windows.Forms.Label();
            this.lblAlarmLow = new System.Windows.Forms.Label();
            this.chkActiveAlarm = new System.Windows.Forms.CheckBox();
            this.chcIsExternal = new System.Windows.Forms.CheckBox();
            this.grpExternalChannelSettings = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.numLinearB = new System.Windows.Forms.NumericUpDown();
            this.numLinearA = new System.Windows.Forms.NumericUpDown();
            this.lblScaling = new System.Windows.Forms.Label();
            this.lblChannelNumber = new System.Windows.Forms.Label();
            this.txtChannelNumber = new System.Windows.Forms.TextBox();
            this.numMax = new System.Windows.Forms.NumericUpDown();
            this.numMin = new System.Windows.Forms.NumericUpDown();
            this.lblMax = new System.Windows.Forms.Label();
            this.lblMin = new System.Windows.Forms.Label();
            this.lblColor = new System.Windows.Forms.Label();
            this.lblChannelName = new System.Windows.Forms.Label();
            this.txtChannelName = new System.Windows.Forms.TextBox();
            this.colorPickerButtonColor = new Syncfusion.Windows.Forms.ColorPickerButton();
            this.gbGeneralSetting = new System.Windows.Forms.GroupBox();
            this.gradientPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridDataBoundGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsChannel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.gbAlarmSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numAlarmHigh)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAlarmLow)).BeginInit();
            this.grpExternalChannelSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numLinearB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLinearA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMin)).BeginInit();
            this.gbGeneralSetting.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbCaption
            // 
            this.lbCaption.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbCaption.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lbCaption.Location = new System.Drawing.Point(88, 0);
            this.lbCaption.Name = "lbCaption";
            this.lbCaption.Size = new System.Drawing.Size(376, 23);
            this.lbCaption.TabIndex = 50;
            this.lbCaption.Text = "Channel Settings";
            this.lbCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gradientPanel1
            // 
            this.gradientPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(109)))), ((int)(((byte)(169)))));
            this.gradientPanel1.BackgroundColor = new Syncfusion.Drawing.BrushInfo(Syncfusion.Drawing.GradientStyle.Vertical, new Syncfusion.Drawing.BrushInfoColorArrayList(new System.Drawing.Color[] {
                System.Drawing.Color.FromArgb(((int)(((byte)(86)))), ((int)(((byte)(109)))), ((int)(((byte)(169))))),
                System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(225)))))}));
            this.gradientPanel1.Border3DStyle = System.Windows.Forms.Border3DStyle.RaisedOuter;
            this.gradientPanel1.BorderColor = System.Drawing.Color.Black;
            this.gradientPanel1.Controls.Add(this.gridDataBoundGrid1);
            this.gradientPanel1.Controls.Add(this.xpToolBar1);
            this.gradientPanel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.gradientPanel1.Location = new System.Drawing.Point(0, 0);
            this.gradientPanel1.Name = "gradientPanel1";
            this.gradientPanel1.Size = new System.Drawing.Size(88, 368);
            this.gradientPanel1.TabIndex = 49;
            // 
            // gridDataBoundGrid1
            // 
            this.gridDataBoundGrid1.AllowDragSelectedCols = true;
            this.gridDataBoundGrid1.AllowResizeToFit = false;
            this.gridDataBoundGrid1.AllowSelection = ((Syncfusion.Windows.Forms.Grid.GridSelectionFlags)((Syncfusion.Windows.Forms.Grid.GridSelectionFlags.Cell | Syncfusion.Windows.Forms.Grid.GridSelectionFlags.AlphaBlend)));
            this.gridDataBoundGrid1.DataMember = "tb_Channel";
            this.gridDataBoundGrid1.DataSource = this.dsChannel1;
            this.gridDataBoundGrid1.EnableAddNew = false;
            this.gridDataBoundGrid1.EnableEdit = false;
            this.gridDataBoundGrid1.EnableRemove = false;
            this.gridDataBoundGrid1.Location = new System.Drawing.Point(0, 40);
            this.gridDataBoundGrid1.Name = "gridDataBoundGrid1";
            this.gridDataBoundGrid1.ResizeColsBehavior = Syncfusion.Windows.Forms.Grid.GridResizeCellsBehavior.None;
            this.gridDataBoundGrid1.ResizeRowsBehavior = Syncfusion.Windows.Forms.Grid.GridResizeCellsBehavior.None;
            this.gridDataBoundGrid1.ShowCurrentCellBorderBehavior = Syncfusion.Windows.Forms.Grid.GridShowCurrentCellBorder.GrayWhenLostFocus;
            this.gridDataBoundGrid1.Size = new System.Drawing.Size(84, 296);
            this.gridDataBoundGrid1.SortBehavior = Syncfusion.Windows.Forms.Grid.GridSortBehavior.DoubleClick;
            this.gridDataBoundGrid1.TabIndex = 2;
            this.gridDataBoundGrid1.Text = "gridDataBoundGrid1";
            // 
            // dsChannel1
            // 
            this.dsChannel1.DataSetName = "dsChannel";
            this.dsChannel1.Locale = new System.Globalization.CultureInfo("en-US");
            // 
            // xpToolBar1
            // 
            this.xpToolBar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.xpToolBar1.ForeColor = System.Drawing.Color.White;
            this.xpToolBar1.Location = new System.Drawing.Point(0, 0);
            this.xpToolBar1.Name = "xpToolBar1";
            this.xpToolBar1.Size = new System.Drawing.Size(84, 33);
            this.xpToolBar1.TabIndex = 0;
            this.xpToolBar1.Text = "Channel";
            this.xpToolBar1.ThemesEnabled = true;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.dsChannel1, "tb_Channel.Color", true));
            this.numericUpDown1.Location = new System.Drawing.Point(104, 336);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1000000000,
            0,
            0,
            -2147483648});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(0, 20);
            this.numericUpDown1.TabIndex = 55;
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // gbAlarmSettings
            // 
            this.gbAlarmSettings.Controls.Add(this.numAlarmHigh);
            this.gbAlarmSettings.Controls.Add(this.numAlarmLow);
            this.gbAlarmSettings.Controls.Add(this.lblAlarmHigh);
            this.gbAlarmSettings.Controls.Add(this.lblAlarmLow);
            this.gbAlarmSettings.Controls.Add(this.chkActiveAlarm);
            this.gbAlarmSettings.Location = new System.Drawing.Point(280, 32);
            this.gbAlarmSettings.Name = "gbAlarmSettings";
            this.gbAlarmSettings.Size = new System.Drawing.Size(168, 152);
            this.gbAlarmSettings.TabIndex = 64;
            this.gbAlarmSettings.TabStop = false;
            this.gbAlarmSettings.Text = "Alarm Settings";
            // 
            // numAlarmHigh
            // 
            this.numAlarmHigh.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.dsChannel1, "tb_Channel.AlarmHigh", true));
            this.numAlarmHigh.Location = new System.Drawing.Point(88, 80);
            this.numAlarmHigh.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numAlarmHigh.Minimum = new decimal(new int[] {
            1000000000,
            0,
            0,
            -2147483648});
            this.numAlarmHigh.Name = "numAlarmHigh";
            this.numAlarmHigh.Size = new System.Drawing.Size(72, 20);
            this.numAlarmHigh.TabIndex = 67;
            this.numAlarmHigh.Leave += new System.EventHandler(this.numAlarmHigh_Leave);
            // 
            // numAlarmLow
            // 
            this.numAlarmLow.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.dsChannel1, "tb_Channel.AlarmLow", true));
            this.numAlarmLow.Location = new System.Drawing.Point(88, 48);
            this.numAlarmLow.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numAlarmLow.Minimum = new decimal(new int[] {
            1000000000,
            0,
            0,
            -2147483648});
            this.numAlarmLow.Name = "numAlarmLow";
            this.numAlarmLow.Size = new System.Drawing.Size(72, 20);
            this.numAlarmLow.TabIndex = 66;
            this.numAlarmLow.Leave += new System.EventHandler(this.numAlarmLow_Leave);
            // 
            // lblAlarmHigh
            // 
            this.lblAlarmHigh.Location = new System.Drawing.Point(16, 82);
            this.lblAlarmHigh.Name = "lblAlarmHigh";
            this.lblAlarmHigh.Size = new System.Drawing.Size(72, 35);
            this.lblAlarmHigh.TabIndex = 65;
            this.lblAlarmHigh.Text = "Alarm High:";
            // 
            // lblAlarmLow
            // 
            this.lblAlarmLow.Location = new System.Drawing.Point(16, 50);
            this.lblAlarmLow.Name = "lblAlarmLow";
            this.lblAlarmLow.Size = new System.Drawing.Size(72, 29);
            this.lblAlarmLow.TabIndex = 64;
            this.lblAlarmLow.Text = "Alarm Low:";
            // 
            // chkActiveAlarm
            // 
            this.chkActiveAlarm.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.dsChannel1, "tb_Channel.ActiveAlarm", true));
            this.chkActiveAlarm.Enabled = false;
            this.chkActiveAlarm.Location = new System.Drawing.Point(8, 27);
            this.chkActiveAlarm.Name = "chkActiveAlarm";
            this.chkActiveAlarm.Size = new System.Drawing.Size(152, 17);
            this.chkActiveAlarm.TabIndex = 0;
            this.chkActiveAlarm.Text = "Active Alarm";
            // 
            // chcIsExternal
            // 
            this.chcIsExternal.Checked = true;
            this.chcIsExternal.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chcIsExternal.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.dsChannel1, "tb_Channel.IsExternal", true));
            this.chcIsExternal.Location = new System.Drawing.Point(224, 336);
            this.chcIsExternal.Name = "chcIsExternal";
            this.chcIsExternal.Size = new System.Drawing.Size(0, 16);
            this.chcIsExternal.TabIndex = 65;
            this.chcIsExternal.Text = "IsExternal";
            this.chcIsExternal.CheckedChanged += new System.EventHandler(this.chcIsExternal_CheckedChanged);
            // 
            // grpExternalChannelSettings
            // 
            this.grpExternalChannelSettings.Controls.Add(this.label1);
            this.grpExternalChannelSettings.Controls.Add(this.numLinearB);
            this.grpExternalChannelSettings.Controls.Add(this.numLinearA);
            this.grpExternalChannelSettings.Controls.Add(this.lblScaling);
            this.grpExternalChannelSettings.Controls.Add(this.lblChannelNumber);
            this.grpExternalChannelSettings.Controls.Add(this.txtChannelNumber);
            this.grpExternalChannelSettings.Location = new System.Drawing.Point(185, 200);
            this.grpExternalChannelSettings.Name = "grpExternalChannelSettings";
            this.grpExternalChannelSettings.Size = new System.Drawing.Size(183, 120);
            this.grpExternalChannelSettings.TabIndex = 66;
            this.grpExternalChannelSettings.TabStop = false;
            this.grpExternalChannelSettings.Text = "External Channel Settings";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(58, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 24);
            this.label1.TabIndex = 63;
            this.label1.Text = "X      +";
            // 
            // numLinearB
            // 
            this.numLinearB.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.dsChannel1, "tb_Channel.LinearB", true));
            this.numLinearB.Location = new System.Drawing.Point(104, 72);
            this.numLinearB.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numLinearB.Minimum = new decimal(new int[] {
            1000000000,
            0,
            0,
            -2147483648});
            this.numLinearB.Name = "numLinearB";
            this.numLinearB.Size = new System.Drawing.Size(48, 20);
            this.numLinearB.TabIndex = 62;
            this.numLinearB.Leave += new System.EventHandler(this.numLinearB_Leave);
            // 
            // numLinearA
            // 
            this.numLinearA.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.dsChannel1, "tb_Channel.LinearA", true));
            this.numLinearA.DecimalPlaces = 4;
            this.numLinearA.Location = new System.Drawing.Point(9, 72);
            this.numLinearA.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numLinearA.Minimum = new decimal(new int[] {
            1000000000,
            0,
            0,
            -2147483648});
            this.numLinearA.Name = "numLinearA";
            this.numLinearA.Size = new System.Drawing.Size(48, 20);
            this.numLinearA.TabIndex = 61;
            this.numLinearA.Leave += new System.EventHandler(this.numLinearA_Leave);
            // 
            // lblScaling
            // 
            this.lblScaling.Location = new System.Drawing.Point(8, 48);
            this.lblScaling.Name = "lblScaling";
            this.lblScaling.Size = new System.Drawing.Size(144, 24);
            this.lblScaling.TabIndex = 2;
            this.lblScaling.Text = "Linear Scaling:";
            // 
            // lblChannelNumber
            // 
            this.lblChannelNumber.Location = new System.Drawing.Point(8, 24);
            this.lblChannelNumber.Name = "lblChannelNumber";
            this.lblChannelNumber.Size = new System.Drawing.Size(106, 24);
            this.lblChannelNumber.TabIndex = 1;
            this.lblChannelNumber.Text = "Channel Number:";
            // 
            // txtChannelNumber
            // 
            this.txtChannelNumber.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsChannel1, "tb_Channel.ExternalChannelNumber", true));
            this.txtChannelNumber.Enabled = false;
            this.txtChannelNumber.Location = new System.Drawing.Point(120, 24);
            this.txtChannelNumber.Name = "txtChannelNumber";
            this.txtChannelNumber.Size = new System.Drawing.Size(32, 20);
            this.txtChannelNumber.TabIndex = 0;
            // 
            // numMax
            // 
            this.numMax.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.dsChannel1, "tb_Channel.YMax", true));
            this.numMax.Location = new System.Drawing.Point(200, 112);
            this.numMax.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numMax.Minimum = new decimal(new int[] {
            1000000000,
            0,
            0,
            -2147483648});
            this.numMax.Name = "numMax";
            this.numMax.Size = new System.Drawing.Size(72, 20);
            this.numMax.TabIndex = 77;
            this.numMax.Leave += new System.EventHandler(this.numMax_Leave);
            // 
            // numMin
            // 
            this.numMin.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.dsChannel1, "tb_Channel.YMin", true));
            this.numMin.Location = new System.Drawing.Point(200, 80);
            this.numMin.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.numMin.Minimum = new decimal(new int[] {
            1000000000,
            0,
            0,
            -2147483648});
            this.numMin.Name = "numMin";
            this.numMin.Size = new System.Drawing.Size(72, 20);
            this.numMin.TabIndex = 76;
            this.numMin.Leave += new System.EventHandler(this.numMin_Leave);
            // 
            // lblMax
            // 
            this.lblMax.Location = new System.Drawing.Point(8, 80);
            this.lblMax.Name = "lblMax";
            this.lblMax.Size = new System.Drawing.Size(106, 20);
            this.lblMax.TabIndex = 75;
            this.lblMax.Text = "Max Value:";
            // 
            // lblMin
            // 
            this.lblMin.Location = new System.Drawing.Point(8, 48);
            this.lblMin.Name = "lblMin";
            this.lblMin.Size = new System.Drawing.Size(100, 20);
            this.lblMin.TabIndex = 74;
            this.lblMin.Text = "Min Value:";
            // 
            // lblColor
            // 
            this.lblColor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblColor.Location = new System.Drawing.Point(121, 112);
            this.lblColor.Name = "lblColor";
            this.lblColor.Size = new System.Drawing.Size(32, 24);
            this.lblColor.TabIndex = 72;
            // 
            // lblChannelName
            // 
            this.lblChannelName.Location = new System.Drawing.Point(104, 56);
            this.lblChannelName.Name = "lblChannelName";
            this.lblChannelName.Size = new System.Drawing.Size(90, 24);
            this.lblChannelName.TabIndex = 71;
            this.lblChannelName.Text = "Channel Name:";
            // 
            // txtChannelName
            // 
            this.txtChannelName.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsChannel1, "tb_Channel.Name", true));
            this.txtChannelName.Location = new System.Drawing.Point(200, 56);
            this.txtChannelName.Name = "txtChannelName";
            this.txtChannelName.Size = new System.Drawing.Size(72, 20);
            this.txtChannelName.TabIndex = 70;
            // 
            // colorPickerButtonColor
            // 
            this.colorPickerButtonColor.BackColor = System.Drawing.SystemColors.Control;
            this.colorPickerButtonColor.ColorUISize = new System.Drawing.Size(208, 230);
            this.colorPickerButtonColor.Location = new System.Drawing.Point(104, 144);
            this.colorPickerButtonColor.Name = "colorPickerButtonColor";
            this.colorPickerButtonColor.SelectedColorGroup = Syncfusion.Windows.Forms.ColorUISelectedGroup.None;
            this.colorPickerButtonColor.Size = new System.Drawing.Size(100, 24);
            this.colorPickerButtonColor.TabIndex = 78;
            this.colorPickerButtonColor.Text = "Pick Color";
            this.colorPickerButtonColor.UseVisualStyleBackColor = false;
            this.colorPickerButtonColor.ColorSelected += new System.EventHandler(this.colorPickerButtonColor_ColorSelected_1);
            // 
            // gbGeneralSetting
            // 
            this.gbGeneralSetting.Controls.Add(this.lblColor);
            this.gbGeneralSetting.Controls.Add(this.lblMax);
            this.gbGeneralSetting.Controls.Add(this.lblMin);
            this.gbGeneralSetting.Location = new System.Drawing.Point(96, 32);
            this.gbGeneralSetting.Name = "gbGeneralSetting";
            this.gbGeneralSetting.Size = new System.Drawing.Size(184, 152);
            this.gbGeneralSetting.TabIndex = 79;
            this.gbGeneralSetting.TabStop = false;
            this.gbGeneralSetting.Text = "General Setting";
            // 
            // SetUpExternChannelConfig
            // 
            this.Controls.Add(this.colorPickerButtonColor);
            this.Controls.Add(this.numMax);
            this.Controls.Add(this.numMin);
            this.Controls.Add(this.lblChannelName);
            this.Controls.Add(this.txtChannelName);
            this.Controls.Add(this.grpExternalChannelSettings);
            this.Controls.Add(this.chcIsExternal);
            this.Controls.Add(this.gbAlarmSettings);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.lbCaption);
            this.Controls.Add(this.gradientPanel1);
            this.Controls.Add(this.gbGeneralSetting);
            this.Name = "SetUpExternChannelConfig";
            this.Size = new System.Drawing.Size(464, 368);
            this.gradientPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridDataBoundGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsChannel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.gbAlarmSettings.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numAlarmHigh)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAlarmLow)).EndInit();
            this.grpExternalChannelSettings.ResumeLayout(false);
            this.grpExternalChannelSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numLinearB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLinearA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMin)).EndInit();
            this.gbGeneralSetting.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        
        #endregion

        public Syncfusion.Windows.Forms.Tools.GradientPanel gradientPanel1;
        public Syncfusion.Windows.Forms.Tools.XPMenus.XPToolBar xpToolBar1;
        private Syncfusion.Windows.Forms.Grid.GridDataBoundGrid gridDataBoundGrid1;
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.GroupBox gbAlarmSettings;
        private System.Windows.Forms.CheckBox chkActiveAlarm;
        private System.Windows.Forms.NumericUpDown numAlarmHigh;
        private System.Windows.Forms.NumericUpDown numAlarmLow;
        private System.Windows.Forms.Label lblAlarmHigh;
        private System.Windows.Forms.Label lblAlarmLow;
        private System.Windows.Forms.CheckBox chcIsExternal;
        private System.Windows.Forms.GroupBox grpExternalChannelSettings;
        private System.Windows.Forms.TextBox txtChannelNumber;
        private System.Windows.Forms.Label lblChannelNumber;
        private System.Windows.Forms.Label lblScaling;
        private System.Windows.Forms.NumericUpDown numLinearA;
        private System.Windows.Forms.NumericUpDown numLinearB;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label lbCaption;
        private Syncfusion.Windows.Forms.ColorPickerButton colorPickerButtonColor;
        private System.Windows.Forms.NumericUpDown numMax;
        private System.Windows.Forms.NumericUpDown numMin;
        private System.Windows.Forms.Label lblMax;
        private System.Windows.Forms.Label lblMin;
        private System.Windows.Forms.Label lblColor;
        private System.Windows.Forms.Label lblChannelName;
        private System.Windows.Forms.TextBox txtChannelName;
        private System.Windows.Forms.GroupBox gbGeneralSetting;
        private TCD.DAL.dsChannel dsChannel1;
    }
}