using Rimed.Framework.Common;

namespace Rimed.TCD.GUI.Setup
{
    partial class SetUpDICOM
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);

            if ((this.m_tmpLogoFile != null) && (System.IO.File.Exists(this.m_tmpLogoFile)))
            {
                try { Files.Delete(m_tmpLogoFile); }
                catch { }
            }
        }

        #region Component Designer generated code
        
        /// <summary> 
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dsConfiguation1 = new TCD.DAL.dsConfiguation();
            this.lbCaption = new System.Windows.Forms.Label();
            this.groupDICOM = new System.Windows.Forms.GroupBox();
            this.btnEcho = new System.Windows.Forms.Button();
            this.btnEchoWL = new System.Windows.Forms.Button();
            this.ipAddressControl1 = new IPAddressControlLib.IPAddressControl();
            this.txtAETitle = new System.Windows.Forms.TextBox();
            this.txtPortNumber = new System.Windows.Forms.TextBox();
            this.lblAE_Address = new System.Windows.Forms.Label();
            this.lblPortNumber = new System.Windows.Forms.Label();
            this.lblIpAddress = new System.Windows.Forms.Label();
            this.lblLocalAE_Title = new System.Windows.Forms.Label();
            this.txtLocalAETitle = new System.Windows.Forms.TextBox();

            this.groupDICOMWL = new System.Windows.Forms.GroupBox();
            this.ipAddressControl1WL = new IPAddressControlLib.IPAddressControl();
            this.txtAETitleWL = new System.Windows.Forms.TextBox();
            this.txtPortNumberWL = new System.Windows.Forms.TextBox();
            this.lblAE_AddressWL = new System.Windows.Forms.Label();
            this.lblPortNumberWL = new System.Windows.Forms.Label();
            this.lblIpAddressWL = new System.Windows.Forms.Label();
            this.lblLocalAE_TitleWL = new System.Windows.Forms.Label();
            this.txtLocalAETitleWL = new System.Windows.Forms.TextBox();
            
            ((System.ComponentModel.ISupportInitialize)(this.dsConfiguation1)).BeginInit();
            this.groupDICOM.SuspendLayout();
            this.groupDICOMWL.SuspendLayout();
            this.SuspendLayout();
            // 
            // dsConfiguation1
            // 
            this.dsConfiguation1.DataSetName = "dsConfiguation";
            this.dsConfiguation1.Locale = new System.Globalization.CultureInfo("en-US");
            this.dsConfiguation1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lbCaption
            // 
            this.lbCaption.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbCaption.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.lbCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lbCaption.Location = new System.Drawing.Point(40, 5);
            this.lbCaption.Name = "lbCaption";
            this.lbCaption.Size = new System.Drawing.Size(376, 19);
            this.lbCaption.TabIndex = 15;
            this.lbCaption.Text = "DICOM & Worklist";
            this.lbCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupDICOM
            // 
            this.groupDICOM.Controls.Add(this.btnEcho);
            this.groupDICOM.Controls.Add(this.ipAddressControl1);
            this.groupDICOM.Controls.Add(this.txtAETitle);
            this.groupDICOM.Controls.Add(this.txtPortNumber);
            this.groupDICOM.Controls.Add(this.lblAE_Address);
            this.groupDICOM.Controls.Add(this.lblPortNumber);
            this.groupDICOM.Controls.Add(this.lblIpAddress);
            this.groupDICOM.Controls.Add(this.lblLocalAE_Title);
            this.groupDICOM.Controls.Add(this.txtLocalAETitle);
            this.groupDICOM.Location = new System.Drawing.Point(50, 50);
            this.groupDICOM.Name = "groupDICOM";
            this.groupDICOM.Size = new System.Drawing.Size(224, 110);
            this.groupDICOM.TabIndex = 29;
            this.groupDICOM.TabStop = false;
            // 
            // btnEcho
            // 
            this.btnEcho.Location = new System.Drawing.Point(160, 88);
            this.btnEcho.Name = "btnEcho";
            this.btnEcho.Size = new System.Drawing.Size(56, 20);
            this.btnEcho.TabIndex = 9;
            this.btnEcho.Text = "ECHO";
            this.btnEcho.Click += new System.EventHandler(this.btnEcho_Click);
            // ipAddressControl1
            // 
            this.ipAddressControl1.AutoHeight = true;
            this.ipAddressControl1.BackColor = System.Drawing.SystemColors.Window;
            this.ipAddressControl1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ipAddressControl1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsConfiguation1, "tb_HospitalDetails.IP_Address", true));
            this.ipAddressControl1.Location = new System.Drawing.Point(72, 16);
            this.ipAddressControl1.Name = "ipAddressControl1";
            this.ipAddressControl1.ReadOnly = false;
            this.ipAddressControl1.Size = new System.Drawing.Size(144, 20);
            this.ipAddressControl1.TabIndex = 6;
            this.ipAddressControl1.Text = "...";
            // 
            // txtAETitle
            // 
            this.txtAETitle.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsConfiguation1, "tb_HospitalDetails.AE_Title", true));
            this.txtAETitle.Location = new System.Drawing.Point(72, 64);
            this.txtAETitle.Name = "txtAETitle";
            this.txtAETitle.Size = new System.Drawing.Size(144, 20);
            this.txtAETitle.TabIndex = 5;
            // 
            // txtPortNumber
            // 
            this.txtPortNumber.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsConfiguation1, "tb_HospitalDetails.Port_Number", true));
            this.txtPortNumber.Location = new System.Drawing.Point(72, 40);
            this.txtPortNumber.Name = "txtPortNumber";
            this.txtPortNumber.Size = new System.Drawing.Size(144, 20);
            this.txtPortNumber.TabIndex = 4;
            // 
            // lblAE_Address
            // 
            this.lblAE_Address.Location = new System.Drawing.Point(2, 69);
            this.lblAE_Address.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.lblAE_Address.Name = "lblAE_Address";
            this.lblAE_Address.Size = new System.Drawing.Size(74, 17);
            this.lblAE_Address.TabIndex = 2;
            this.lblAE_Address.Text = "AE Title";
            // 
            // lblPortNumber
            // 
            this.lblPortNumber.Location = new System.Drawing.Point(2, 45);
            this.lblPortNumber.Name = "lblPortNumber";
            this.lblPortNumber.Size = new System.Drawing.Size(72, 24);
            this.lblPortNumber.TabIndex = 1;
            this.lblPortNumber.Text = "Port Number";
            // 
            // lblIpAddress
            // 
            this.lblIpAddress.Location = new System.Drawing.Point(2, 20);
            this.lblIpAddress.Name = "lblIpAddress";
            this.lblIpAddress.Size = new System.Drawing.Size(64, 16);
            this.lblIpAddress.TabIndex = 0;
            this.lblIpAddress.Text = "IP Address";
            // 
            // lblLocalAE_Title
            // 
            this.lblLocalAE_Title.Location = new System.Drawing.Point(2, 91);
            this.lblLocalAE_Title.Name = "lblLocalAE_Title";
            this.lblLocalAE_Title.Size = new System.Drawing.Size(64, 16);
            this.lblLocalAE_Title.TabIndex = 8;
            this.lblLocalAE_Title.Text = "Local AE";
            // 
            // txtLocalAETitle
            // 
            this.txtLocalAETitle.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dsConfiguation1, "tb_HospitalDetails.localAE_Title", true));
            this.txtLocalAETitle.Location = new System.Drawing.Point(72, 88);
            this.txtLocalAETitle.Name = "txtLocalAETitle";
            this.txtLocalAETitle.Size = new System.Drawing.Size(80, 20);
            this.txtLocalAETitle.TabIndex = 7;

            
////
            // 
            // 
            // btnEchoWL
            // 
            this.btnEchoWL.Location = new System.Drawing.Point(160, 88);
            this.btnEchoWL.Name = "btnEchoWL";
            this.btnEchoWL.Size = new System.Drawing.Size(56, 20);
            this.btnEchoWL.TabIndex = 9;
            this.btnEchoWL.Text = "Accept";
            this.btnEchoWL.Click += new System.EventHandler(this.btnEchoWL_Click);
            
            // 
            // groupDICOMWL
            // 
            this.groupDICOMWL.Controls.Add(this.btnEchoWL);
            this.groupDICOMWL.Controls.Add(this.ipAddressControl1WL);
            this.groupDICOMWL.Controls.Add(this.txtAETitleWL);
            this.groupDICOMWL.Controls.Add(this.txtPortNumberWL);
            this.groupDICOMWL.Controls.Add(this.lblAE_AddressWL);
            this.groupDICOMWL.Controls.Add(this.lblPortNumberWL);
            this.groupDICOMWL.Controls.Add(this.lblIpAddressWL);
            this.groupDICOMWL.Controls.Add(this.lblLocalAE_TitleWL);
            this.groupDICOMWL.Controls.Add(this.txtLocalAETitleWL);
            this.groupDICOMWL.Location = new System.Drawing.Point(50, 200);
            this.groupDICOMWL.Name = "groupDICOMWL";
            this.groupDICOMWL.Size = new System.Drawing.Size(224, 110);
            this.groupDICOMWL.TabIndex = 29;
            this.groupDICOMWL.TabStop = false;
            // 
            // ipAddressControl1WL
            // 
            this.ipAddressControl1WL.AutoHeight = true;
            this.ipAddressControl1WL.BackColor = System.Drawing.SystemColors.Window;
            this.ipAddressControl1WL.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ipAddressControl1WL.Location = new System.Drawing.Point(72, 16);
            this.ipAddressControl1WL.Name = "ipAddressControl1WL";
            this.ipAddressControl1WL.ReadOnly = false;
            this.ipAddressControl1WL.Size = new System.Drawing.Size(144, 20);
            this.ipAddressControl1WL.TabIndex = 6;
            this.ipAddressControl1WL.Text = "...";
            // 
            // txtAETitleWL
            // 
            this.txtAETitleWL.Location = new System.Drawing.Point(72, 64);
            this.txtAETitleWL.Name = "txtAETitleWL";
            this.txtAETitleWL.Size = new System.Drawing.Size(144, 20);
            this.txtAETitleWL.TabIndex = 5;
            // 
            // txtPortNumberWL
            // 
            this.txtPortNumberWL.Location = new System.Drawing.Point(72, 40);
            this.txtPortNumberWL.Name = "txtPortNumberWL";
            this.txtPortNumberWL.Size = new System.Drawing.Size(144, 20);
            this.txtPortNumberWL.TabIndex = 4;
            // 
            // lblAE_AddressWL
            // 
            this.lblAE_AddressWL.Location = new System.Drawing.Point(2, 69);
            this.lblAE_AddressWL.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.lblAE_AddressWL.Name = "lblAE_AddressWL";
            this.lblAE_AddressWL.Size = new System.Drawing.Size(74, 17);
            this.lblAE_AddressWL.TabIndex = 2;
            this.lblAE_AddressWL.Text = "AE Title";
            // 
            // lblPortNumberWL
            // 
            this.lblPortNumberWL.Location = new System.Drawing.Point(2, 45);
            this.lblPortNumberWL.Name = "lblPortNumberWL";
            this.lblPortNumberWL.Size = new System.Drawing.Size(72, 24);
            this.lblPortNumberWL.TabIndex = 1;
            this.lblPortNumberWL.Text = "Port Number";
            // 
            // lblIpAddressWL
            // 
            this.lblIpAddressWL.Location = new System.Drawing.Point(2, 20);
            this.lblIpAddressWL.Name = "lblIpAddressWL";
            this.lblIpAddressWL.Size = new System.Drawing.Size(64, 16);
            this.lblIpAddressWL.TabIndex = 0;
            this.lblIpAddressWL.Text = "IP Address";
            // 
            // lblLocalAE_TitleWL
            // 
            this.lblLocalAE_TitleWL.Location = new System.Drawing.Point(2, 91);
            this.lblLocalAE_TitleWL.Name = "lblLocalAE_TitleWL";
            this.lblLocalAE_TitleWL.Size = new System.Drawing.Size(64, 16);
            this.lblLocalAE_TitleWL.TabIndex = 8;
            this.lblLocalAE_TitleWL.Text = "Local AE";
            // 
            // txtLocalAETitleWL
            // 
            this.txtLocalAETitleWL.Location = new System.Drawing.Point(72, 88);
            this.txtLocalAETitleWL.Name = "txtLocalAETitleWL";
            this.txtLocalAETitleWL.Size = new System.Drawing.Size(80, 20);
            this.txtLocalAETitleWL.TabIndex = 7;
            
////
            // 
            // SetUpSystem
            // 
            this.Controls.Add(this.groupDICOM);
            this.Controls.Add(this.groupDICOMWL);
            this.Controls.Add(this.lbCaption);
            this.Name = "SetUpSystem";
            this.Load += new System.EventHandler(this.SetUpSystem_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dsConfiguation1)).EndInit();
            this.groupDICOM.ResumeLayout(false);
            this.groupDICOM.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        
        #endregion

        private System.Windows.Forms.Label lbCaption;
        private TCD.DAL.dsConfiguation dsConfiguation1;
        private System.Windows.Forms.GroupBox groupDICOM;
        private System.Windows.Forms.Label lblIpAddress;
        private System.Windows.Forms.Label lblPortNumber;
        private System.Windows.Forms.Label lblAE_Address;
        private System.Windows.Forms.TextBox txtPortNumber;
        private System.Windows.Forms.TextBox txtAETitle;
        private System.Windows.Forms.TextBox txtLocalAETitle;

        private System.Windows.Forms.GroupBox groupDICOMWL;
        private System.Windows.Forms.Label lblIpAddressWL;
        private System.Windows.Forms.Label lblPortNumberWL;
        private System.Windows.Forms.Label lblAE_AddressWL;
        private System.Windows.Forms.TextBox txtPortNumberWL;
        private System.Windows.Forms.TextBox txtAETitleWL;
        private System.Windows.Forms.TextBox txtLocalAETitleWL;

        
        private IPAddressControlLib.IPAddressControl ipAddressControl1;
        private IPAddressControlLib.IPAddressControl ipAddressControl1WL;

        private System.Windows.Forms.Label lblLocalAE_Title;
        private System.Windows.Forms.Label lblLocalAE_TitleWL;

        private System.Windows.Forms.Button btnEcho;
        private System.Windows.Forms.Button btnEchoWL;
        private System.ComponentModel.Container components = null;
    }
}