namespace Rimed.TCD.GUI.Setup
{
    partial class SetUpHits
    {
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.albHitsRate = new Syncfusion.Windows.Forms.Tools.AutoLabel();
            this.albHistogramSteps = new Syncfusion.Windows.Forms.Tools.AutoLabel();
            this.textBoxStep = new System.Windows.Forms.TextBox();
            this.lbCaption = new System.Windows.Forms.Label();
            this.comboBoxHitsRate = new System.Windows.Forms.ComboBox();
            this.oleDbConnection1 = new System.Data.OleDb.OleDbConnection();
            this.dsHits1 = new TCD.DAL.dsHits();
            ((System.ComponentModel.ISupportInitialize)(this.dsHits1)).BeginInit();
            this.SuspendLayout();
            // 
            // albHitsRate
            // 
            this.albHitsRate.DX = 0;
            this.albHitsRate.DY = 0;
            this.albHitsRate.Location = new System.Drawing.Point(60, 128);
            this.albHitsRate.Name = "albHitsRate";
            this.albHitsRate.Size = new System.Drawing.Size(123, 23);
            this.albHitsRate.TabIndex = 23;
            this.albHitsRate.Text = "Hits Rate (per minute)";
            this.albHitsRate.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // albHistogramSteps
            // 
            this.albHistogramSteps.DX = -147;
            this.albHistogramSteps.DY = -1;
            this.albHistogramSteps.LabeledControl = this.textBoxStep;
            this.albHistogramSteps.Location = new System.Drawing.Point(40, 96);
            this.albHistogramSteps.Name = "albHistogramSteps";
            this.albHistogramSteps.Size = new System.Drawing.Size(143, 23);
            this.albHistogramSteps.TabIndex = 19;
            this.albHistogramSteps.Text = "Histogram Steps (dB)";
            this.albHistogramSteps.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // textBoxStep
            // 
            this.textBoxStep.Location = new System.Drawing.Point(187, 97);
            this.textBoxStep.Name = "textBoxStep";
            this.textBoxStep.Size = new System.Drawing.Size(120, 20);
            this.textBoxStep.TabIndex = 18;
            this.textBoxStep.Validating += new System.ComponentModel.CancelEventHandler(this.textBoxStep_Validating);
            // 
            // lbCaption
            // 
            this.lbCaption.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbCaption.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.lbCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.lbCaption.Location = new System.Drawing.Point(40, 19);
            this.lbCaption.Name = "lbCaption";
            this.lbCaption.Size = new System.Drawing.Size(376, 23);
            this.lbCaption.TabIndex = 15;
            this.lbCaption.Text = "Hits Setup";
            this.lbCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comboBoxHitsRate
            // 
            this.comboBoxHitsRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxHitsRate.Location = new System.Drawing.Point(186, 130);
            this.comboBoxHitsRate.Name = "comboBoxHitsRate";
            this.comboBoxHitsRate.Size = new System.Drawing.Size(121, 21);
            this.comboBoxHitsRate.TabIndex = 25;
            // 
            // dsHits1
            // 
            this.dsHits1.DataSetName = "dsHits";
            this.dsHits1.Locale = new System.Globalization.CultureInfo("en-US");
            this.dsHits1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // SetUpHits
            // 
            this.Controls.Add(this.comboBoxHitsRate);
            this.Controls.Add(this.albHitsRate);
            this.Controls.Add(this.albHistogramSteps);
            this.Controls.Add(this.textBoxStep);
            this.Controls.Add(this.lbCaption);
            this.Name = "SetUpHits";
            this.Load += new System.EventHandler(this.SetUpHits_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dsHits1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Syncfusion.Windows.Forms.Tools.AutoLabel albHitsRate;
        private Syncfusion.Windows.Forms.Tools.AutoLabel albHistogramSteps;
        private System.Windows.Forms.Label lbCaption;
        private System.Windows.Forms.TextBox textBoxStep;
        private System.Windows.Forms.ComboBox comboBoxHitsRate;
        private System.Data.OleDb.OleDbConnection oleDbConnection1;
        private TCD.DAL.dsHits dsHits1;
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
    }
}