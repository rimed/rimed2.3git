using System;
using System.ComponentModel;
using System.Windows.Forms;
using Rimed.TCD.DAL;

namespace Rimed.TCD.GUI.Setup
{
    /// <summary>
    /// Summary description for SystemSetUp.
    /// </summary>
    public partial class SetUpHits : SetUpBase
    {
        public SetUpHits()
        {
            InitializeComponent();

            var strRes = MainForm.StringManager;
            albHitsRate.Text = strRes.GetString("Hits Rate (per minute)");
            albHistogramSteps.Text = strRes.GetString("Histogram Steps (dB)");
            lbCaption.Text = strRes.GetString("Hits Setup");

            BackColor = GlobalSettings.BackgroundDlg;
        }

        public override bool Verify()
        {
            setHitsProperties();

            // ... TODO
            BindingContext[dsHits1.tb_Hits].EndCurrentEdit();
            if (dsHits1.HasChanges())
                RimedDal.Instance.HitsDS.Merge(dsHits1);

            return true;
        }

        private void SetUpHits_Load(object sender, EventArgs e)
        {
            dsHits1.Merge(RimedDal.Instance.HitsDS);

            // 2Mhz
            comboBoxHitsRate.Items.Clear();
            int[] hitsRateValues =
            {
                GlobalSettings.HITS_RATE_1,
				GlobalSettings.HITS_RATE_2,
				GlobalSettings.HITS_RATE_3,
				GlobalSettings.HITS_RATE_4
            };
            comboBoxHitsRate.DataSource = hitsRateValues;


            var hitRate = HitsRate;
            var index = 0;
            switch (hitRate)
            {
                case GlobalSettings.HITS_RATE_1:
                    index = 0;
                    break;
                case GlobalSettings.HITS_RATE_2:
                    index = 1;
                    break;
                case GlobalSettings.HITS_RATE_3:
                    index = 2;
                    break;
                case GlobalSettings.HITS_RATE_4:
                    index = 3;
                    break;
                default:
                    index = 0;
                    break;
            }

            comboBoxHitsRate.SelectedIndex = index;
            textBoxStep.Text = HitsSteps.ToString();
        }

        private void setHitsProperties()
        {
			var r = Convert.ToInt32(comboBoxHitsRate.SelectedValue);
            HitsRate = r;

            var str = textBoxStep.Text;
            var s = Convert.ToInt32(str);
            HitsSteps = s;

            MainForm.Instance.HitsHistogram.SetHitsParameters(new HitsParameters(s, r, MainForm.Instance.GetBVThreshold()));
        }

        private void textBoxStep_Validating(object sender, CancelEventArgs e)
        {
	        try
            {
                var str = ((TextBox)sender).Text;
            }
            catch
            {
                e.Cancel = true;
            }
        }

        private int HitsRate
        {
            get
            {
                var rows = getRows();
	            return (rows == null) ? GlobalSettings.HITS_RATE_1 : rows[0].Rate;
            }
            set
            {
                var rows = getRows();
				if (rows != null)
					rows[0].Rate = value;
            }
        }
        private int HitsSteps
        {
            get
            {
                var rows = getRows();
				return (rows == null) ? 1 : rows[0].Step;
            }
            set
            {
                var rows = getRows();
				if (rows != null)
					rows[0].Step = value;
            }
        }

        private dsHits.tb_HitsRow[] getRows()
        {
            var rows = (dsHits.tb_HitsRow[])dsHits1.tb_Hits.Select("ID = '1'");

            if (rows.Length > 0)
                return rows;
            
            MessageBox.Show(MainForm.StringManager.GetString("Error in database:table HITS is missing"));
            return null;
        }
    }
}
