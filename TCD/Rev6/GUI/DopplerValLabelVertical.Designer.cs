namespace Rimed.TCD.GUI
{
    partial class DopplerValLabelVertical
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(DopplerValLabelVertical));
            this.verticalTextCtrl1 = new OrientedTextLabel();
            this.pictureBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelHeader
            // 
            this.labelHeader.Dock = System.Windows.Forms.DockStyle.None;
            this.labelHeader.Name = "labelHeader";
            this.labelHeader.Size = new System.Drawing.Size(80, 24);
            this.labelHeader.Text = "";
            this.labelHeader.Visible = false;
            // 
            // labelVar
            // 
            this.labelVar.Name = "labelVar";
            this.labelVar.Size = new System.Drawing.Size(56, 56);
            //this.labelVar.TextChanged += new System.EventHandler(this.labelVar_TextChanged);
            this.labelVar.FontChanged += new System.EventHandler(this.labelVar_FontChanged);
            // 
            // imageListLarge
            // 
            this.imageListLarge.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListLarge.ImageStream")));
            // 
            // imageListSmall
            // 
            this.imageListSmall.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListSmall.ImageStream")));
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(32, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(56, 56);
            // 
            // verticalTextCtrl1
            // 
            this.verticalTextCtrl1.RotationAngle = 270F;
            this.verticalTextCtrl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.verticalTextCtrl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.verticalTextCtrl1.Location = new System.Drawing.Point(0, 0);
            this.verticalTextCtrl1.Name = "verticalTextCtrl1";
            this.verticalTextCtrl1.Size = new System.Drawing.Size(32, 56);
            this.verticalTextCtrl1.TabIndex = 3;
            this.verticalTextCtrl1.Text = "Depth";
            this.verticalTextCtrl1.Click += new System.EventHandler(this.verticalTextCtrl1_Click);
            // 
            // DopplerValLabelVertical
            // 
            this.Controls.Add(this.verticalTextCtrl1);
            this.Name = "DopplerValLabelVertical";
            this.Size = new System.Drawing.Size(88, 56);
            this.SizeChanged += new System.EventHandler(this.DopplerValLabelVertical_SizeChanged);
            this.Controls.SetChildIndex(this.labelHeader, 0);
            this.Controls.SetChildIndex(this.verticalTextCtrl1, 0);
            this.Controls.SetChildIndex(this.pictureBox1, 0);
            this.pictureBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private OrientedTextLabel verticalTextCtrl1;
        private System.ComponentModel.IContainer components = null;
    }
}