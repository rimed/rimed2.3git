﻿using System;
using System.Windows.Forms;
using Rimed.Framework.Common;
using Rimed.Framework.WinAPI;

namespace Rimed.TCD.GUI
{
    public partial class SplashForm : Form
    {
        private SplashForm()
        {
			User32.WindowsTaskBarHide();
			
			InitializeComponent();
        }

        private static SplashForm s_splashForm = null;

        public static void On()
        {
			#if DEBUG
				return;
			#endif

			if (s_splashForm == null)
                s_splashForm = new SplashForm();

			s_splashForm.TopMost = true;
			s_splashForm.Show();

			SetText();
        }

        public static void Off()
        {
            if (s_splashForm == null)
                return;
			
			s_splashForm.TopMost = false;
			s_splashForm.Hide();
			//s_splashForm.Dispose();
			//s_splashForm = null;
        }

		public static void SetText(string msg = "", bool writeToLog = true)
        {
            if (s_splashForm == null)
                return;

			if (!string.IsNullOrWhiteSpace(msg) && writeToLog)
                Logger.LogInfo(msg);

			s_splashForm.TopMost = true;
			s_splashForm.Show();

			s_splashForm.label1.Text = msg;
			s_splashForm.label1.Invalidate();
			s_splashForm.label1.Refresh();
		}
    }
}
