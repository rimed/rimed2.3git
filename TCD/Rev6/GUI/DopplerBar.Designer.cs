namespace Rimed.TCD.GUI
{
    partial class DopplerBar
    {
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DopplerBar));
            this.imageListVolume = new System.Windows.Forms.ImageList(this.components);
            this.pictureBoxVolume = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBoxHeartRate = new System.Windows.Forms.PictureBox();
            this.autoLabelHeartRate = new System.Windows.Forms.Label();
            this.labelBV = new System.Windows.Forms.Label();
            this.picManualCalculations = new System.Windows.Forms.PictureBox();
            this.MC_label = new System.Windows.Forms.Label();
            this.cachedcrTableRep1 = new Rimed.TCD.ReportMgr.CachedcrTableRep();
            this.cachedcrTableRep2 = new Rimed.TCD.ReportMgr.CachedcrTableRep();
            this.cachedcrTableRep3 = new Rimed.TCD.ReportMgr.CachedcrTableRep();
            this.cachedcrTableRep4 = new Rimed.TCD.ReportMgr.CachedcrTableRep();
            this.LindegaardRatio = new System.Windows.Forms.PictureBox();
            this.LR_label = new System.Windows.Forms.Label();
            this.LR_ValuesPanel = new System.Windows.Forms.Panel();
            this.LR_Rval = new System.Windows.Forms.Label();
            this.LR_Lval = new System.Windows.Forms.Label();
            this.probeIndication1 = new Rimed.TCD.GUI.ProbeIndication();
            this.dopplerVarLabelDepth = new Rimed.TCD.GUI.DopplerValLabelVertical();
            this.dopplerVarLabelThump = new Rimed.TCD.GUI.DopplerVarLabel();
            this.dopplerVarLabelWidth = new Rimed.TCD.GUI.DopplerVarLabel();
            this.dopplerVarLabelPower = new Rimed.TCD.GUI.DopplerVarLabel();
            this.dopplerVarLabelRange = new Rimed.TCD.GUI.DopplerVarLabel();
            this.dopplerVarLabelGain = new Rimed.TCD.GUI.DopplerVarLabel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxVolume)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHeartRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picManualCalculations)).BeginInit();
            this.picManualCalculations.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LindegaardRatio)).BeginInit();
            this.LindegaardRatio.SuspendLayout();
            this.LR_ValuesPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageListVolume
            // 
            this.imageListVolume.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListVolume.ImageStream")));
            this.imageListVolume.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListVolume.Images.SetKeyName(0, "");
            this.imageListVolume.Images.SetKeyName(1, "");
            this.imageListVolume.Images.SetKeyName(2, "");
            this.imageListVolume.Images.SetKeyName(3, "");
            this.imageListVolume.Images.SetKeyName(4, "");
            this.imageListVolume.Images.SetKeyName(5, "");
            this.imageListVolume.Images.SetKeyName(6, "");
            this.imageListVolume.Images.SetKeyName(7, "");
            this.imageListVolume.Images.SetKeyName(8, "");
            this.imageListVolume.Images.SetKeyName(9, "");
            this.imageListVolume.Images.SetKeyName(10, "");
            this.imageListVolume.Images.SetKeyName(11, "");
            this.imageListVolume.Images.SetKeyName(12, "");
            this.imageListVolume.Images.SetKeyName(13, "");
            this.imageListVolume.Images.SetKeyName(14, "");
            this.imageListVolume.Images.SetKeyName(15, "");
            this.imageListVolume.Images.SetKeyName(16, "");
            this.imageListVolume.Images.SetKeyName(17, "");
            this.imageListVolume.Images.SetKeyName(18, "");
            this.imageListVolume.Images.SetKeyName(19, "");
            this.imageListVolume.Images.SetKeyName(20, "VOLUME_019.bmp");
            this.imageListVolume.Images.SetKeyName(21, "VOLUME_000.BW.bmp");
            this.imageListVolume.Images.SetKeyName(22, "VOLUME_001.BW.bmp");
            this.imageListVolume.Images.SetKeyName(23, "VOLUME_002.BW.bmp");
            this.imageListVolume.Images.SetKeyName(24, "VOLUME_003.BW.bmp");
            this.imageListVolume.Images.SetKeyName(25, "VOLUME_004.BW.bmp");
            this.imageListVolume.Images.SetKeyName(26, "VOLUME_005.BW.bmp");
            this.imageListVolume.Images.SetKeyName(27, "VOLUME_006.BW.bmp");
            this.imageListVolume.Images.SetKeyName(28, "VOLUME_007.BW.bmp");
            this.imageListVolume.Images.SetKeyName(29, "VOLUME_008.BW.bmp");
            this.imageListVolume.Images.SetKeyName(30, "VOLUME_009.BW.bmp");
            this.imageListVolume.Images.SetKeyName(31, "VOLUME_010.BW.bmp");
            this.imageListVolume.Images.SetKeyName(32, "VOLUME_011.BW.bmp");
            this.imageListVolume.Images.SetKeyName(33, "VOLUME_012.BW.bmp");
            this.imageListVolume.Images.SetKeyName(34, "VOLUME_013.BW.bmp");
            this.imageListVolume.Images.SetKeyName(35, "VOLUME_014.BW.bmp");
            this.imageListVolume.Images.SetKeyName(36, "VOLUME_015.BW.bmp");
            this.imageListVolume.Images.SetKeyName(37, "VOLUME_016.BW.bmp");
            this.imageListVolume.Images.SetKeyName(38, "VOLUME_017.BW.bmp");
            this.imageListVolume.Images.SetKeyName(39, "VOLUME_018.BW.bmp");
            this.imageListVolume.Images.SetKeyName(40, "VOLUME_019.BW.bmp");
            // 
            // pictureBoxVolume
            // 
            this.pictureBoxVolume.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBoxVolume.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxVolume.Image")));
            this.pictureBoxVolume.Location = new System.Drawing.Point(0, 0);
            this.pictureBoxVolume.Name = "pictureBoxVolume";
            this.pictureBoxVolume.Size = new System.Drawing.Size(41, 58);
            this.pictureBoxVolume.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBoxVolume.TabIndex = 1;
            this.pictureBoxVolume.TabStop = false;
            this.pictureBoxVolume.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBoxVolume_MouseDown);
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.ForeColor = System.Drawing.Color.Transparent;
            this.panel1.Location = new System.Drawing.Point(41, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1, 58);
            this.panel1.TabIndex = 8;
            // 
            // pictureBoxHeartRate
            // 
            this.pictureBoxHeartRate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxHeartRate.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxHeartRate.Image")));
            this.pictureBoxHeartRate.Location = new System.Drawing.Point(640, 0);
            this.pictureBoxHeartRate.Name = "pictureBoxHeartRate";
            this.pictureBoxHeartRate.Size = new System.Drawing.Size(30, 25);
            this.pictureBoxHeartRate.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBoxHeartRate.TabIndex = 9;
            this.pictureBoxHeartRate.TabStop = false;
            // 
            // autoLabelHeartRate
            // 
            this.autoLabelHeartRate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.autoLabelHeartRate.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
            this.autoLabelHeartRate.Location = new System.Drawing.Point(672, 0);
            this.autoLabelHeartRate.Name = "autoLabelHeartRate";
            this.autoLabelHeartRate.Size = new System.Drawing.Size(56, 25);
            this.autoLabelHeartRate.TabIndex = 10;
            this.autoLabelHeartRate.Text = "HR - 180";
            this.autoLabelHeartRate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelBV
            // 
            this.labelBV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelBV.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.labelBV.Location = new System.Drawing.Point(552, 26);
            this.labelBV.Name = "labelBV";
            this.labelBV.Size = new System.Drawing.Size(100, 22);
            this.labelBV.TabIndex = 12;
            this.labelBV.Text = "MCA-L";
            this.labelBV.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picManualCalculations
            // 
            this.picManualCalculations.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(235)))), ((int)(((byte)(250)))));
            this.picManualCalculations.Controls.Add(this.MC_label);
            this.picManualCalculations.ErrorImage = ((System.Drawing.Image)(resources.GetObject("picManualCalculations.ErrorImage")));
            this.picManualCalculations.Image = ((System.Drawing.Image)(resources.GetObject("picManualCalculations.Image")));
            this.picManualCalculations.Location = new System.Drawing.Point(544, 16);
            this.picManualCalculations.Name = "picManualCalculations";
            this.picManualCalculations.Size = new System.Drawing.Size(72, 27);
            this.picManualCalculations.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picManualCalculations.TabIndex = 17;
            this.picManualCalculations.TabStop = false;
            this.picManualCalculations.Click += new System.EventHandler(this.picManualCalculations_Click);
            this.picManualCalculations.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picManualCalculations_MouseDown);
            this.picManualCalculations.MouseLeave += new System.EventHandler(this.picManualCalculations_MouseLeave);
            this.picManualCalculations.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picManualCalculations_MouseUp);
            // 
            // MC_label
            // 
            this.MC_label.BackColor = System.Drawing.Color.Transparent;
            this.MC_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.MC_label.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MC_label.Location = new System.Drawing.Point(0, 0);
            this.MC_label.Name = "MC_label";
            this.MC_label.Padding = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.MC_label.Size = new System.Drawing.Size(72, 27);
            this.MC_label.TabIndex = 0;
            this.MC_label.Text = "M.C.";
            this.MC_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.MC_label.Click += new System.EventHandler(this.picManualCalculations_Click);
            this.MC_label.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picManualCalculations_MouseDown);
            this.MC_label.MouseLeave += new System.EventHandler(this.picManualCalculations_MouseLeave);
            this.MC_label.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picManualCalculations_MouseUp);
            // 
            // LindegaardRatio
            // 
            this.LindegaardRatio.BackColor = System.Drawing.Color.Transparent;
            this.LindegaardRatio.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("LindegaardRatio.BackgroundImage")));
            this.LindegaardRatio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.LindegaardRatio.Controls.Add(this.LR_label);
            this.LindegaardRatio.Location = new System.Drawing.Point(469, 16);
            this.LindegaardRatio.Name = "LindegaardRatio";
            this.LindegaardRatio.Size = new System.Drawing.Size(72, 27);
            this.LindegaardRatio.TabIndex = 18;
            this.LindegaardRatio.TabStop = false;
            this.LindegaardRatio.Click += new System.EventHandler(this.LindegaardRatio_Click);
            // 
            // LR_label
            // 
            this.LR_label.BackColor = System.Drawing.Color.Transparent;
            this.LR_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LR_label.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LR_label.Location = new System.Drawing.Point(0, 0);
            this.LR_label.Name = "LR_label";
            this.LR_label.Padding = new System.Windows.Forms.Padding(0, 0, 0, 0);
            this.LR_label.Size = new System.Drawing.Size(72, 27);
            this.LR_label.TabIndex = 0;
            this.LR_label.Text = "L.R.";
            this.LR_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.LR_label.Click += new System.EventHandler(this.LindegaardRatio_Click);
            // 
            // LR_ValuesPanel
            // 
            this.LR_ValuesPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.LR_ValuesPanel.Controls.Add(this.LR_Rval);
            this.LR_ValuesPanel.Controls.Add(this.LR_Lval);
            this.LR_ValuesPanel.Location = new System.Drawing.Point(448, 44);
            this.LR_ValuesPanel.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.LR_ValuesPanel.Name = "LR_ValuesPanel";
            this.LR_ValuesPanel.Size = new System.Drawing.Size(0, 0);
            this.LR_ValuesPanel.TabIndex = 19;
            this.LR_ValuesPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.LR_ValuesPanel_Paint);
            // 
            // LR_Rval
            // 
            this.LR_Rval.AutoSize = true;
            this.LR_Rval.Dock = System.Windows.Forms.DockStyle.Right;
            this.LR_Rval.Font = new System.Drawing.Font("Arial", 15.75F);
            this.LR_Rval.Location = new System.Drawing.Point(0, 0);
            this.LR_Rval.Name = "LR_Rval";
            this.LR_Rval.Size = new System.Drawing.Size(0, 24);
            this.LR_Rval.TabIndex = 1;
            this.LR_Rval.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // LR_Lval
            // 
            this.LR_Lval.AutoSize = true;
            this.LR_Lval.Dock = System.Windows.Forms.DockStyle.Left;
            this.LR_Lval.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LR_Lval.Location = new System.Drawing.Point(0, 0);
            this.LR_Lval.Name = "LR_Lval";
            this.LR_Lval.Size = new System.Drawing.Size(0, 24);
            this.LR_Lval.TabIndex = 0;
            // 
            // probeIndication1
            // 
            this.probeIndication1.Location = new System.Drawing.Point(664, 32);
            this.probeIndication1.Name = "probeIndication1";
            this.probeIndication1.ProbeColor = System.Drawing.Color.Empty;
            this.probeIndication1.ProbeName = null;
            this.probeIndication1.Size = new System.Drawing.Size(64, 16);
            this.probeIndication1.TabIndex = 16;
            // 
            // dopplerVarLabelDepth
            // 
            this.dopplerVarLabelDepth.Active = false;
            this.dopplerVarLabelDepth.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(235)))), ((int)(((byte)(250)))));
            this.dopplerVarLabelDepth.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.dopplerVarLabelDepth.ImageLabel = Rimed.TCD.GUI.GlobalTypes.EImageLabelStyle.ImageAndLabel;
            this.dopplerVarLabelDepth.Location = new System.Drawing.Point(44, 0);
            this.dopplerVarLabelDepth.Name = "dopplerVarLabelDepth";
            this.dopplerVarLabelDepth.Padding = new System.Windows.Forms.Padding(3);
            this.dopplerVarLabelDepth.Size = new System.Drawing.Size(100, 60);
            this.dopplerVarLabelDepth.SizeMode = Rimed.TCD.GUI.GlobalTypes.ESizeMode.Large;
            this.dopplerVarLabelDepth.TabIndex = 2;
            this.dopplerVarLabelDepth.VarText = "Depth";
            this.dopplerVarLabelDepth.VarValue = 55;
            this.dopplerVarLabelDepth.VarValueDouble = 55D;
            this.dopplerVarLabelDepth.Visible = false;
            // 
            // dopplerVarLabelThump
            // 
            this.dopplerVarLabelThump.Active = false;
            this.dopplerVarLabelThump.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(235)))), ((int)(((byte)(250)))));
            this.dopplerVarLabelThump.ImageLabel = Rimed.TCD.GUI.GlobalTypes.EImageLabelStyle.ImageAndLabel;
            this.dopplerVarLabelThump.Location = new System.Drawing.Point(449, 0);
            this.dopplerVarLabelThump.Name = "dopplerVarLabelThump";
            this.dopplerVarLabelThump.Padding = new System.Windows.Forms.Padding(3);
            this.dopplerVarLabelThump.Size = new System.Drawing.Size(80, 60);
            this.dopplerVarLabelThump.SizeMode = Rimed.TCD.GUI.GlobalTypes.ESizeMode.Large;
            this.dopplerVarLabelThump.TabIndex = 7;
            this.dopplerVarLabelThump.VarText = "Filter";
            this.dopplerVarLabelThump.VarValue = 100;
            this.dopplerVarLabelThump.VarValueDouble = 100D;
            // 
            // dopplerVarLabelWidth
            // 
            this.dopplerVarLabelWidth.Active = false;
            this.dopplerVarLabelWidth.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(235)))), ((int)(((byte)(250)))));
            this.dopplerVarLabelWidth.ImageLabel = Rimed.TCD.GUI.GlobalTypes.EImageLabelStyle.ImageAndLabel;
            this.dopplerVarLabelWidth.Location = new System.Drawing.Point(369, 0);
            this.dopplerVarLabelWidth.Name = "dopplerVarLabelWidth";
            this.dopplerVarLabelWidth.Padding = new System.Windows.Forms.Padding(3);
            this.dopplerVarLabelWidth.Size = new System.Drawing.Size(80, 60);
            this.dopplerVarLabelWidth.SizeMode = Rimed.TCD.GUI.GlobalTypes.ESizeMode.Large;
            this.dopplerVarLabelWidth.TabIndex = 6;
            this.dopplerVarLabelWidth.VarText = "Sample";
            this.dopplerVarLabelWidth.VarValue = 15;
            this.dopplerVarLabelWidth.VarValueDouble = 15D;
            // 
            // dopplerVarLabelPower
            // 
            this.dopplerVarLabelPower.Active = false;
            this.dopplerVarLabelPower.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(235)))), ((int)(((byte)(250)))));
            this.dopplerVarLabelPower.ImageLabel = Rimed.TCD.GUI.GlobalTypes.EImageLabelStyle.ImageAndLabel;
            this.dopplerVarLabelPower.Location = new System.Drawing.Point(289, 0);
            this.dopplerVarLabelPower.Name = "dopplerVarLabelPower";
            this.dopplerVarLabelPower.Padding = new System.Windows.Forms.Padding(3);
            this.dopplerVarLabelPower.Size = new System.Drawing.Size(80, 60);
            this.dopplerVarLabelPower.SizeMode = Rimed.TCD.GUI.GlobalTypes.ESizeMode.Large;
            this.dopplerVarLabelPower.TabIndex = 13;
            this.dopplerVarLabelPower.VarText = "Power";
            this.dopplerVarLabelPower.VarValue = 100;
            this.dopplerVarLabelPower.VarValueDouble = 100D;
            // 
            // dopplerVarLabelRange
            // 
            this.dopplerVarLabelRange.Active = false;
            this.dopplerVarLabelRange.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(235)))), ((int)(((byte)(250)))));
            this.dopplerVarLabelRange.ImageLabel = Rimed.TCD.GUI.GlobalTypes.EImageLabelStyle.ImageAndLabel;
            this.dopplerVarLabelRange.Location = new System.Drawing.Point(209, 0);
            this.dopplerVarLabelRange.Name = "dopplerVarLabelRange";
            this.dopplerVarLabelRange.Padding = new System.Windows.Forms.Padding(3);
            this.dopplerVarLabelRange.Size = new System.Drawing.Size(80, 60);
            this.dopplerVarLabelRange.SizeMode = Rimed.TCD.GUI.GlobalTypes.ESizeMode.Large;
            this.dopplerVarLabelRange.TabIndex = 4;
            this.dopplerVarLabelRange.VarText = "Scale";
            this.dopplerVarLabelRange.VarValue = 6;
            this.dopplerVarLabelRange.VarValueDouble = 6D;
            // 
            // dopplerVarLabelGain
            // 
            this.dopplerVarLabelGain.Active = false;
            this.dopplerVarLabelGain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(235)))), ((int)(((byte)(250)))));
            this.dopplerVarLabelGain.ImageLabel = Rimed.TCD.GUI.GlobalTypes.EImageLabelStyle.ImageAndLabel;
            this.dopplerVarLabelGain.Location = new System.Drawing.Point(129, 0);
            this.dopplerVarLabelGain.Name = "dopplerVarLabelGain";
            this.dopplerVarLabelGain.Padding = new System.Windows.Forms.Padding(3);
            this.dopplerVarLabelGain.Size = new System.Drawing.Size(80, 60);
            this.dopplerVarLabelGain.SizeMode = Rimed.TCD.GUI.GlobalTypes.ESizeMode.Large;
            this.dopplerVarLabelGain.TabIndex = 3;
            this.dopplerVarLabelGain.VarText = "Gain";
            this.dopplerVarLabelGain.VarValue = 4;
            this.dopplerVarLabelGain.VarValueDouble = 4D;
            // 
            // DopplerBar
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(235)))), ((int)(((byte)(250)))));
            this.Controls.Add(this.LR_ValuesPanel);
            this.Controls.Add(this.LindegaardRatio);
            this.Controls.Add(this.picManualCalculations);
            this.Controls.Add(this.probeIndication1);
            this.Controls.Add(this.dopplerVarLabelDepth);
            this.Controls.Add(this.labelBV);
            this.Controls.Add(this.autoLabelHeartRate);
            this.Controls.Add(this.pictureBoxHeartRate);
            this.Controls.Add(this.dopplerVarLabelThump);
            this.Controls.Add(this.dopplerVarLabelWidth);
            this.Controls.Add(this.dopplerVarLabelPower);
            this.Controls.Add(this.dopplerVarLabelRange);
            this.Controls.Add(this.dopplerVarLabelGain);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBoxVolume);
            this.Name = "DopplerBar";
            this.Size = new System.Drawing.Size(752, 58);
            this.Load += new System.EventHandler(this.DopplerBar_Load);
            this.SizeChanged += new System.EventHandler(this.DopplerBar_SizeChanged);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxVolume)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxHeartRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picManualCalculations)).EndInit();
            this.picManualCalculations.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.LindegaardRatio)).EndInit();
            this.LindegaardRatio.ResumeLayout(false);
            this.LR_ValuesPanel.ResumeLayout(false);
            this.LR_ValuesPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList imageListVolume;
        private System.Windows.Forms.PictureBox pictureBoxVolume;
        private DopplerVarLabel dopplerVarLabelGain;
        private DopplerVarLabel dopplerVarLabelRange;
        private DopplerVarLabel dopplerVarLabelWidth;
        private DopplerVarLabel dopplerVarLabelThump;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBoxHeartRate;
        private System.Windows.Forms.Label autoLabelHeartRate;
        private System.Windows.Forms.Label labelBV;
        private DopplerVarLabel dopplerVarLabelPower;
        private System.ComponentModel.IContainer components;
        private DopplerValLabelVertical dopplerVarLabelDepth;
        private System.Windows.Forms.PictureBox picManualCalculations;
        private System.Windows.Forms.Label MC_label;
        public ProbeIndication probeIndication1;
        private ReportMgr.CachedcrTableRep cachedcrTableRep1;
        private ReportMgr.CachedcrTableRep cachedcrTableRep2;
        private ReportMgr.CachedcrTableRep cachedcrTableRep3;
        private ReportMgr.CachedcrTableRep cachedcrTableRep4;
        private System.Windows.Forms.PictureBox LindegaardRatio;
        private System.Windows.Forms.Label LR_label;
        private System.Windows.Forms.Panel LR_ValuesPanel;
        private System.Windows.Forms.Label LR_Rval;
        private System.Windows.Forms.Label LR_Lval;
    }
}