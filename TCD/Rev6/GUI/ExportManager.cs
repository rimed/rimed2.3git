using System;
using System.Collections.Generic;
using System.IO;

using Rimed.Framework.Common;
using Rimed.TCD.DAL;

namespace Rimed.TCD.GUI
{
    public static class ExportManager
    {
        public static void AddRecord(ExportPackage package, DateTime currentTime, List<double> parameters)
        {
            var next = new ExportElement(parameters.Count);
            next.CurrentTime = currentTime;

            for (var i = 0; i < parameters.Count; i++)
                next.Parameters[i] = parameters[i];

            package.Log.Add(next);
        }

        public static void AddHeader(ExportPackage package, List<string> captions)
        {
            package.Header.Add("Trends");
            package.Header.AddRange(captions);
        }


		//TODO [Ofer]: move identical code from saveToFile(...) methods to a seperate method.

		/// <summary>Save log file for the Unilateral mode</summary>
		/// <param name="package">data from TrendChart</param>
		/// <param name="fileName">name of csv-file</param>
		public static bool SaveToFile(ExportPackage package, string fileName)
		{
			try
			{
				saveToFile(package, fileName);
				return true;
			}
			catch (Exception ex)
			{
				Logger.LogError(string.Format("Unilateral SaveToFile(fileName={0}) Fail.", fileName), ex);
				return false;
			}
		}

        /// <summary>Save log file for the Bilateral mode</summary>
		/// <param name="package1">data from Right TrendChart</param>
		/// <param name="package2">data from Left TrendChart</param>
		/// <param name="fileName">name of csv-file</param>
		public static bool SaveToFile(ExportPackage package1, ExportPackage package2, string fileName)
        {
	        try
	        {
		        saveToFile(package1, package2, fileName);
				return true;
			}
	        catch (Exception ex)
	        {
				Logger.LogError(string.Format("Bilateral SaveToFile(fileName={0}) Fail.", fileName), ex);
		        return false;
	        }
        }

		private static void saveToFile(ExportPackage package, string fileName)
		{
			var separator = getSeparator();
			using (var sw = File.CreateText(fileName))
			{
				if (package.Header.Count <= 0)
				{
					sw.Write("<Package headers are missing>");
				}
				else
				{
					sw.Write(package.Header[0]);
					for (var i = 1; i < package.Header.Count; i++)
					{
						sw.Write(separator);
						sw.Write(package.Header[i]);
					}
				}

				sw.Write("\n");

				foreach (var item in package.Log)
				{
					sw.Write(item.CurrentTime.ToLongTimeString());
					for (var i = 0; i < item.Parameters.Length; i++)
					{
						sw.Write(separator);
						sw.Write(item.Parameters[i].ToString());
					}

					sw.Write("\n");
				}
			}
		}

		/// <summary>Save log file for the Bilateral mode</summary>
		private static void saveToFile(ExportPackage package1, ExportPackage package2, string fileName)
		{
			var separator = getSeparator();
			using (var sw = File.CreateText(fileName))
			{
				var preHeader = separator + "Probe_1";
				for (var i = 0; i < package1.Header.Count - 1; i++)
				{
					preHeader += separator;
				}

				preHeader += "Probe_2\n";
				sw.Write(preHeader);

				var fileHeader = String.Empty;
				for (var i = 0; i < package1.Header.Count; i++)
				{
					fileHeader += package1.Header[i];
					fileHeader += separator;
				}

				for (var i = 1; i < package2.Header.Count; i++)
				{
					fileHeader += package2.Header[i];
					if (i == package2.Header.Count - 1)
						fileHeader += "\n";
					else
						fileHeader += separator;
				}

				sw.Write(fileHeader);

				for (var j = 0; j < package1.Log.Count && j < package2.Log.Count; j++)
				{
					var item = package1.Log[j];
					var data = item.CurrentTime.ToLongTimeString() + separator;
					for (var i = 0; i < item.Parameters.Length; i++)
					{
						data += item.Parameters[i].ToString();
						data += separator;
					}

					item = package2.Log[j];
					for (var i = 0; i < item.Parameters.Length; i++)
					{
						data += item.Parameters[i].ToString();
						if (i == item.Parameters.Length - 1)
							data += "\n";
						else
							data += separator;
					}

					sw.Write(data);
				}
			}
		}
		
		private static string getSeparator()
		{
			var separator = RimedDal.GeneralSettings.ExportSeparator;
			if (string.IsNullOrWhiteSpace(separator))
			{
				separator = GlobalSettings.COMMA_SEPARATOR;
				RimedDal.GeneralSettings.ExportSeparator = separator;
			}

			return separator;
		}
    }
}
