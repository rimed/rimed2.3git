using Rimed.Framework.Common;

namespace Rimed.TCD.GUI
{
    partial class SummaryClinicalBar
    {
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(SummaryClinicalBar));
            
            this.gradientPanelSmall = new Syncfusion.Windows.Forms.Tools.GradientPanel();
            this.Modelabel = new System.Windows.Forms.Label();
            this.ModeTopVal = new System.Windows.Forms.Label();
            this.ModeBottomVal = new System.Windows.Forms.Label();
            this.DVBottomVal = new System.Windows.Forms.Label();
            this.DVTopVal = new System.Windows.Forms.Label();
            this.MeanBottomVal = new System.Windows.Forms.Label();
            this.MeanTopVal = new System.Windows.Forms.Label();
            this.DVLabel = new System.Windows.Forms.Label();
            this.MeanLabel = new System.Windows.Forms.Label();
            this.PeakLabel = new System.Windows.Forms.Label();
            this.PeakTopVal = new System.Windows.Forms.Label();
            this.PeakBottomVal = new System.Windows.Forms.Label();
            this.gradientPanelLarge = new Syncfusion.Windows.Forms.Tools.GradientPanel();
            this.HitsRatelabel = new System.Windows.Forms.Label();
            this.HITrTopVal = new System.Windows.Forms.Label();
            this.HITrBottomVal = new System.Windows.Forms.Label();
            this.Hitslabel = new System.Windows.Forms.Label();
            this.HitsTopVal = new System.Windows.Forms.Label();
            this.HitsBottomVal = new System.Windows.Forms.Label();
            this.Avrglabel = new System.Windows.Forms.Label();
            this.AvrgTopVal = new System.Windows.Forms.Label();
            this.AvrgBottomVal = new System.Windows.Forms.Label();
            this.RIBottomVal = new System.Windows.Forms.Label();
            this.RITopVal = new System.Windows.Forms.Label();
            this.RIlabel = new System.Windows.Forms.Label();
            this.SDLabel = new System.Windows.Forms.Label();
            this.SDTopVal = new System.Windows.Forms.Label();
            this.SDBottomVal = new System.Windows.Forms.Label();
            this.SWLabel = new System.Windows.Forms.Label();
            this.SWTopVal = new System.Windows.Forms.Label();
            this.SWBottomVal = new System.Windows.Forms.Label();
            this.PIBottomVal = new System.Windows.Forms.Label();
            this.PITopVal = new System.Windows.Forms.Label();
            this.PILabel = new System.Windows.Forms.Label();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.dsClinicalParamSetup1 = new DAL.dsClinicalParamSetup();

            this.gradientPanelSmall.SuspendLayout();
            this.gradientPanelLarge.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dsClinicalParamSetup1)).BeginInit();
            this.SuspendLayout();
            // 
            // gradientPanelSmall
            // 
            this.gradientPanelSmall.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("gradientPanelSmall.BackgroundImage")));
            this.gradientPanelSmall.BorderColor = System.Drawing.Color.Black;
            this.gradientPanelSmall.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gradientPanelSmall.Controls.Add(this.Modelabel);
            this.gradientPanelSmall.Controls.Add(this.ModeTopVal);
            this.gradientPanelSmall.Controls.Add(this.ModeBottomVal);
            this.gradientPanelSmall.Controls.Add(this.DVBottomVal);
            this.gradientPanelSmall.Controls.Add(this.DVTopVal);
            this.gradientPanelSmall.Controls.Add(this.MeanBottomVal);
            this.gradientPanelSmall.Controls.Add(this.MeanTopVal);
            this.gradientPanelSmall.Controls.Add(this.DVLabel);
            this.gradientPanelSmall.Controls.Add(this.MeanLabel);
            this.gradientPanelSmall.Controls.Add(this.PeakLabel);
            this.gradientPanelSmall.Controls.Add(this.PeakTopVal);
            this.gradientPanelSmall.Controls.Add(this.PeakBottomVal);
            this.gradientPanelSmall.Dock = System.Windows.Forms.DockStyle.Right;
            this.gradientPanelSmall.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.gradientPanelSmall.Location = new System.Drawing.Point(64, 0);
            this.gradientPanelSmall.Name = "gradientPanelSmall";
            this.gradientPanelSmall.Size = new System.Drawing.Size(64, 104);
            this.gradientPanelSmall.TabIndex = 0;
            // 
            // Modelabel
            // 
            this.Modelabel.BackColor = System.Drawing.Color.Transparent;
            this.Modelabel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.Modelabel.Location = new System.Drawing.Point(-1, 62);
            this.Modelabel.Name = "Modelabel";
            this.Modelabel.Size = new System.Drawing.Size(31, 13);
            this.Modelabel.TabIndex = 17;
            this.Modelabel.Text = "Mode";
            // 
            // ModeTopVal
            // 
            this.ModeTopVal.BackColor = System.Drawing.Color.Transparent;
            this.ModeTopVal.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.ModeTopVal.Location = new System.Drawing.Point(26, 53);
            this.ModeTopVal.Name = "ModeTopVal";
            this.ModeTopVal.Size = new System.Drawing.Size(30, 14);
            this.ModeTopVal.TabIndex = 18;
            this.ModeTopVal.Text = "8888";
            this.ModeTopVal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ModeBottomVal
            // 
            this.ModeBottomVal.BackColor = System.Drawing.Color.Transparent;
            this.ModeBottomVal.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.ModeBottomVal.Location = new System.Drawing.Point(26, 70);
            this.ModeBottomVal.Name = "ModeBottomVal";
            this.ModeBottomVal.Size = new System.Drawing.Size(30, 14);
            this.ModeBottomVal.TabIndex = 19;
            this.ModeBottomVal.Text = "8888";
            this.ModeBottomVal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DVBottomVal
            // 
            this.DVBottomVal.BackColor = System.Drawing.Color.Transparent;
            this.DVBottomVal.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.DVBottomVal.Location = new System.Drawing.Point(26, 88);
            this.DVBottomVal.Name = "DVBottomVal";
            this.DVBottomVal.Size = new System.Drawing.Size(30, 14);
            this.DVBottomVal.TabIndex = 10;
            this.DVBottomVal.Text = "8888";
            this.DVBottomVal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DVTopVal
            // 
            this.DVTopVal.BackColor = System.Drawing.Color.Transparent;
            this.DVTopVal.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.DVTopVal.Location = new System.Drawing.Point(26, 69);
            this.DVTopVal.Name = "DVTopVal";
            this.DVTopVal.Size = new System.Drawing.Size(30, 14);
            this.DVTopVal.TabIndex = 9;
            this.DVTopVal.Text = "8888";
            this.DVTopVal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeanBottomVal
            // 
            this.MeanBottomVal.BackColor = System.Drawing.Color.Transparent;
            this.MeanBottomVal.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.MeanBottomVal.Location = new System.Drawing.Point(26, 53);
            this.MeanBottomVal.Name = "MeanBottomVal";
            this.MeanBottomVal.Size = new System.Drawing.Size(30, 14);
            this.MeanBottomVal.TabIndex = 8;
            this.MeanBottomVal.Text = "8888";
            this.MeanBottomVal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeanTopVal
            // 
            this.MeanTopVal.BackColor = System.Drawing.Color.Transparent;
            this.MeanTopVal.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.MeanTopVal.Location = new System.Drawing.Point(26, 36);
            this.MeanTopVal.Name = "MeanTopVal";
            this.MeanTopVal.Size = new System.Drawing.Size(30, 14);
            this.MeanTopVal.TabIndex = 7;
            this.MeanTopVal.Text = "8888";
            this.MeanTopVal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // DVLabel
            // 
            this.DVLabel.BackColor = System.Drawing.Color.Transparent;
            this.DVLabel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.DVLabel.Location = new System.Drawing.Point(3, 80);
            this.DVLabel.Name = "DVLabel";
            this.DVLabel.Size = new System.Drawing.Size(27, 15);
            this.DVLabel.TabIndex = 4;
            this.DVLabel.Text = "DV";
            // 
            // MeanLabel
            // 
            this.MeanLabel.BackColor = System.Drawing.Color.Transparent;
            this.MeanLabel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.MeanLabel.Location = new System.Drawing.Point(1, 44);
            this.MeanLabel.Name = "MeanLabel";
            this.MeanLabel.Size = new System.Drawing.Size(34, 13);
            this.MeanLabel.TabIndex = 3;
            this.MeanLabel.Text = "Mean";
            // 
            // PeakLabel
            // 
            this.PeakLabel.BackColor = System.Drawing.Color.Transparent;
            this.PeakLabel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.PeakLabel.Location = new System.Drawing.Point(2, 12);
            this.PeakLabel.Name = "PeakLabel";
            this.PeakLabel.Size = new System.Drawing.Size(34, 13);
            this.PeakLabel.TabIndex = 2;
            this.PeakLabel.Text = "Peak";
            // 
            // PeakTopVal
            // 
            this.PeakTopVal.BackColor = System.Drawing.Color.Transparent;
            this.PeakTopVal.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.PeakTopVal.Location = new System.Drawing.Point(26, 1);
            this.PeakTopVal.Name = "PeakTopVal";
            this.PeakTopVal.Size = new System.Drawing.Size(30, 14);
            this.PeakTopVal.TabIndex = 5;
            this.PeakTopVal.Text = "8888";
            this.PeakTopVal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PeakBottomVal
            // 
            this.PeakBottomVal.BackColor = System.Drawing.Color.Transparent;
            this.PeakBottomVal.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.PeakBottomVal.Location = new System.Drawing.Point(26, 18);
            this.PeakBottomVal.Name = "PeakBottomVal";
            this.PeakBottomVal.Size = new System.Drawing.Size(30, 14);
            this.PeakBottomVal.TabIndex = 6;
            this.PeakBottomVal.Text = "8888";
            this.PeakBottomVal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gradientPanelLarge
            // 
            this.gradientPanelLarge.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("gradientPanelLarge.BackgroundImage")));
            this.gradientPanelLarge.BorderColor = System.Drawing.Color.Black;
            this.gradientPanelLarge.Controls.Add(this.HitsRatelabel);
            this.gradientPanelLarge.Controls.Add(this.HITrTopVal);
            this.gradientPanelLarge.Controls.Add(this.HITrBottomVal);
            this.gradientPanelLarge.Controls.Add(this.Hitslabel);
            this.gradientPanelLarge.Controls.Add(this.HitsTopVal);
            this.gradientPanelLarge.Controls.Add(this.HitsBottomVal);
            this.gradientPanelLarge.Controls.Add(this.Avrglabel);
            this.gradientPanelLarge.Controls.Add(this.AvrgTopVal);
            this.gradientPanelLarge.Controls.Add(this.AvrgBottomVal);
            this.gradientPanelLarge.Controls.Add(this.RIBottomVal);
            this.gradientPanelLarge.Controls.Add(this.RITopVal);
            this.gradientPanelLarge.Controls.Add(this.RIlabel);
            this.gradientPanelLarge.Controls.Add(this.SDLabel);
            this.gradientPanelLarge.Controls.Add(this.SDTopVal);
            this.gradientPanelLarge.Controls.Add(this.SDBottomVal);
            this.gradientPanelLarge.Controls.Add(this.SWLabel);
            this.gradientPanelLarge.Controls.Add(this.SWTopVal);
            this.gradientPanelLarge.Controls.Add(this.SWBottomVal);
            this.gradientPanelLarge.Controls.Add(this.PIBottomVal);
            this.gradientPanelLarge.Controls.Add(this.PITopVal);
            this.gradientPanelLarge.Controls.Add(this.PILabel);
            this.gradientPanelLarge.Location = new System.Drawing.Point(-1, -2);
            this.gradientPanelLarge.Name = "gradientPanelLarge";
            this.gradientPanelLarge.Size = new System.Drawing.Size(128, 118);
            this.gradientPanelLarge.TabIndex = 1;
            // 
            // HitsRatelabel
            // 
            this.HitsRatelabel.BackColor = System.Drawing.Color.Transparent;
            this.HitsRatelabel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.HitsRatelabel.Location = new System.Drawing.Point(29, 51);
            this.HitsRatelabel.Name = "HitsRatelabel";
            this.HitsRatelabel.Size = new System.Drawing.Size(31, 13);
            this.HitsRatelabel.TabIndex = 29;
            this.HitsRatelabel.Text = "HITr";
            // 
            // HITrTopVal
            // 
            this.HITrTopVal.BackColor = System.Drawing.Color.Transparent;
            this.HITrTopVal.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.HITrTopVal.Location = new System.Drawing.Point(54, 42);
            this.HITrTopVal.Name = "HITrTopVal";
            this.HITrTopVal.Size = new System.Drawing.Size(30, 14);
            this.HITrTopVal.TabIndex = 30;
            this.HITrTopVal.Text = "8888";
            this.HITrTopVal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // HITrBottomVal
            // 
            this.HITrBottomVal.BackColor = System.Drawing.Color.Transparent;
            this.HITrBottomVal.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.HITrBottomVal.Location = new System.Drawing.Point(54, 59);
            this.HITrBottomVal.Name = "HITrBottomVal";
            this.HITrBottomVal.Size = new System.Drawing.Size(30, 14);
            this.HITrBottomVal.TabIndex = 31;
            this.HITrBottomVal.Text = "8888";
            this.HITrBottomVal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Hitslabel
            // 
            this.Hitslabel.BackColor = System.Drawing.Color.Transparent;
            this.Hitslabel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.Hitslabel.Location = new System.Drawing.Point(29, 51);
            this.Hitslabel.Name = "Hitslabel";
            this.Hitslabel.Size = new System.Drawing.Size(31, 13);
            this.Hitslabel.TabIndex = 26;
            this.Hitslabel.Text = "HITS";
            // 
            // HitsTopVal
            // 
            this.HitsTopVal.BackColor = System.Drawing.Color.Transparent;
            this.HitsTopVal.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.HitsTopVal.Location = new System.Drawing.Point(54, 42);
            this.HitsTopVal.Name = "HitsTopVal";
            this.HitsTopVal.Size = new System.Drawing.Size(30, 14);
            this.HitsTopVal.TabIndex = 27;
            this.HitsTopVal.Text = "8888";
            this.HitsTopVal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // HitsBottomVal
            // 
            this.HitsBottomVal.BackColor = System.Drawing.Color.Transparent;
            this.HitsBottomVal.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.HitsBottomVal.Location = new System.Drawing.Point(54, 59);
            this.HitsBottomVal.Name = "HitsBottomVal";
            this.HitsBottomVal.Size = new System.Drawing.Size(30, 14);
            this.HitsBottomVal.TabIndex = 28;
            this.HitsBottomVal.Text = "8888";
            this.HitsBottomVal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Avrglabel
            // 
            this.Avrglabel.BackColor = System.Drawing.Color.Transparent;
            this.Avrglabel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.Avrglabel.Location = new System.Drawing.Point(34, 19);
            this.Avrglabel.Name = "Avrglabel";
            this.Avrglabel.Size = new System.Drawing.Size(32, 13);
            this.Avrglabel.TabIndex = 23;
            this.Avrglabel.Text = "Avrg";
            // 
            // AvrgTopVal
            // 
            this.AvrgTopVal.BackColor = System.Drawing.Color.Transparent;
            this.AvrgTopVal.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.AvrgTopVal.Location = new System.Drawing.Point(54, 8);
            this.AvrgTopVal.Size = new System.Drawing.Size(30, 14);
            this.AvrgTopVal.Name = "AvrgTopVal";
            this.AvrgTopVal.TabIndex = 32;
            this.AvrgTopVal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // AvrgBottomVal
            // 
            this.AvrgBottomVal.BackColor = System.Drawing.Color.Transparent;
            this.AvrgBottomVal.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.AvrgBottomVal.Location = new System.Drawing.Point(54, 25);
            this.AvrgBottomVal.Size = new System.Drawing.Size(30, 14);
            this.AvrgBottomVal.Name = "AvrgBottomVal";
            this.AvrgBottomVal.TabIndex = 33;
            this.AvrgBottomVal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // RIBottomVal
            // 
            this.RIBottomVal.BackColor = System.Drawing.Color.Transparent;
            this.RIBottomVal.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.RIBottomVal.Location = new System.Drawing.Point(54, 59);
            this.RIBottomVal.Name = "RIBottomVal";
            this.RIBottomVal.Size = new System.Drawing.Size(30, 14);
            this.RIBottomVal.TabIndex = 22;
            this.RIBottomVal.Text = "8888";
            this.RIBottomVal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // RITopVal
            // 
            this.RITopVal.BackColor = System.Drawing.Color.Transparent;
            this.RITopVal.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.RITopVal.Location = new System.Drawing.Point(54, 42);
            this.RITopVal.Name = "RITopVal";
            this.RITopVal.Size = new System.Drawing.Size(30, 14);
            this.RITopVal.TabIndex = 21;
            this.RITopVal.Text = "8888";
            this.RITopVal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // RIlabel
            // 
            this.RIlabel.BackColor = System.Drawing.Color.Transparent;
            this.RIlabel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.RIlabel.Location = new System.Drawing.Point(34, 53);
            this.RIlabel.Name = "RIlabel";
            this.RIlabel.Size = new System.Drawing.Size(23, 13);
            this.RIlabel.TabIndex = 20;
            this.RIlabel.Text = "RI";
            // 
            // SDLabel
            // 
            this.SDLabel.BackColor = System.Drawing.Color.Transparent;
            this.SDLabel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.SDLabel.Location = new System.Drawing.Point(-1, 46);
            this.SDLabel.Name = "SDLabel";
            this.SDLabel.Size = new System.Drawing.Size(28, 13);
            this.SDLabel.TabIndex = 17;
            this.SDLabel.Text = "S/D";
            // 
            // SDTopVal
            // 
            this.SDTopVal.BackColor = System.Drawing.Color.Transparent;
            this.SDTopVal.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.SDTopVal.Location = new System.Drawing.Point(26, 35);
            this.SDTopVal.Name = "SDTopVal";
            this.SDTopVal.Size = new System.Drawing.Size(30, 14);
            this.SDTopVal.TabIndex = 18;
            this.SDTopVal.Text = "8888";
            this.SDTopVal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SDBottomVal
            // 
            this.SDBottomVal.BackColor = System.Drawing.Color.Transparent;
            this.SDBottomVal.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.SDBottomVal.Location = new System.Drawing.Point(26, 52);
            this.SDBottomVal.Name = "SDBottomVal";
            this.SDBottomVal.Size = new System.Drawing.Size(30, 14);
            this.SDBottomVal.TabIndex = 19;
            this.SDBottomVal.Text = "8888";
            this.SDBottomVal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SWLabel
            // 
            this.SWLabel.BackColor = System.Drawing.Color.Transparent;
            this.SWLabel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.SWLabel.Location = new System.Drawing.Point(-4, 78);
            this.SWLabel.Name = "SWLabel";
            this.SWLabel.Size = new System.Drawing.Size(31, 13);
            this.SWLabel.TabIndex = 14;
            // Changed by Alex 29/12/2015 bug VMR in report v 2.2.3.18
            //this.SWLabel.Text = "SW";
            this.SWLabel.Text = "VMR";
            // 
            // SWTopVal
            // 
            this.SWTopVal.BackColor = System.Drawing.Color.Transparent;
            this.SWTopVal.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.SWTopVal.Location = new System.Drawing.Point(26, 69);
            this.SWTopVal.Name = "SWTopVal";
            this.SWTopVal.Size = new System.Drawing.Size(30, 14);
            this.SWTopVal.TabIndex = 15;
            this.SWTopVal.Text = "8888";
            this.SWTopVal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SWBottomVal
            // 
            this.SWBottomVal.BackColor = System.Drawing.Color.Transparent;
            this.SWBottomVal.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.SWBottomVal.Location = new System.Drawing.Point(26, 86);
            this.SWBottomVal.Name = "SWBottomVal";
            this.SWBottomVal.Size = new System.Drawing.Size(30, 14);
            this.SWBottomVal.TabIndex = 16;
            this.SWBottomVal.Text = "8888";
            this.SWBottomVal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PIBottomVal
            // 
            this.PIBottomVal.BackColor = System.Drawing.Color.Transparent;
            this.PIBottomVal.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.PIBottomVal.Location = new System.Drawing.Point(26, 18);
            this.PIBottomVal.Name = "PIBottomVal";
            this.PIBottomVal.Size = new System.Drawing.Size(30, 14);
            this.PIBottomVal.TabIndex = 13;
            this.PIBottomVal.Text = "8888";
            this.PIBottomVal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PITopVal
            // 
            this.PITopVal.BackColor = System.Drawing.Color.Transparent;
            this.PITopVal.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.PITopVal.Location = new System.Drawing.Point(26, 1);
            this.PITopVal.Name = "PITopVal";
            this.PITopVal.Size = new System.Drawing.Size(30, 14);
            this.PITopVal.TabIndex = 12;
            this.PITopVal.Text = "8888";
            this.PITopVal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PILabel
            // 
            this.PILabel.BackColor = System.Drawing.Color.Transparent;
            this.PILabel.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(177)));
            this.PILabel.Location = new System.Drawing.Point(1, 44);
            this.PILabel.Name = "PILabel";
            this.PILabel.Size = new System.Drawing.Size(34, 13);
            this.PILabel.TabIndex = 11;
            this.PILabel.Text = "P.I.";
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // dsClinicalParamSetup1
            // 
            this.dsClinicalParamSetup1.DataSetName = "dsClinicalParamSetup";
            this.dsClinicalParamSetup1.Locale = new System.Globalization.CultureInfo("en-US");
            // 
            // SummaryClinicalBar
            // 
            this.Controls.Add(this.gradientPanelSmall);
            this.Controls.Add(this.gradientPanelLarge);
            this.Name = "SummaryClinicalBar";
            this.Size = new System.Drawing.Size(128, 104);
            this.gradientPanelSmall.ResumeLayout(false);
            this.gradientPanelLarge.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dsClinicalParamSetup1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Syncfusion.Windows.Forms.Tools.GradientPanel gradientPanelSmall;
        private Syncfusion.Windows.Forms.Tools.GradientPanel gradientPanelLarge;
        private System.Windows.Forms.Label PeakLabel;
        private System.Windows.Forms.Label MeanLabel;
        private System.Windows.Forms.Label DVLabel;
        private System.Windows.Forms.Label PeakTopVal;
        private System.Windows.Forms.Label PeakBottomVal;
        private System.Windows.Forms.Label MeanTopVal;
        private System.Windows.Forms.Label MeanBottomVal;
        private System.Windows.Forms.Label DVTopVal;
        private System.Windows.Forms.Label DVBottomVal;
        private System.Windows.Forms.Label SWLabel;
        private System.Windows.Forms.Label SWTopVal;
        private System.Windows.Forms.Label SWBottomVal;
        private System.Windows.Forms.Label SDLabel;
        private System.Windows.Forms.Label SDTopVal;
        private System.Windows.Forms.Label SDBottomVal;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label PIBottomVal;
        private System.Windows.Forms.Label PITopVal;
        private System.Windows.Forms.Label PILabel;

        private System.Windows.Forms.Label RIBottomVal;
        private System.Windows.Forms.Label RITopVal;
        private System.Windows.Forms.Label RIlabel;

        private System.Windows.Forms.Label Modelabel;
        private System.Windows.Forms.Label ModeTopVal;
        private System.Windows.Forms.Label ModeBottomVal;

        private System.Windows.Forms.Label Avrglabel;
        private System.Windows.Forms.Label AvrgTopVal;
        private System.Windows.Forms.Label AvrgBottomVal;

        private System.Windows.Forms.Label Hitslabel;
        private System.Windows.Forms.Label HitsTopVal;
        private System.Windows.Forms.Label HitsBottomVal;

        private System.Windows.Forms.Label HitsRatelabel;
        private System.Windows.Forms.Label HITrTopVal;
        private System.Windows.Forms.Label HITrBottomVal;

        private System.ComponentModel.IContainer components = null;
        private DAL.dsClinicalParamSetup dsClinicalParamSetup1;
    }
}