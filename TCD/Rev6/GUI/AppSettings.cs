﻿using System;
using System.Windows.Forms;
using Rimed.Framework.Config;
using Rimed.TCD.Utils;

namespace Rimed.TCD.GUI
{
	public class AppSettings
	{
		private const int DEF_PLAYBACK_RAW_DATA_CACHE_FORWARD	= 1;
		private const int DEF_PLAYBACK_RAW_DATA_CACHE_BACKWARD	= 0;


		private int					m_audioUnfreezMuteVolume;
		private bool				m_isDspOffMode;
		private General.EAppProduct m_appProduct;

		public enum EAppWorkMode
		{
			NONE			= 0,
			FULL			= 1,
			DEMO			= 3,
		}

		public static AppSettings Active { get; private set; }

		static AppSettings()
		{
			Active = new AppSettings();
		}


		public bool		ApplicationTitle { get; private set; }
		public int		ApplicatioWidth { get; private set; }
		public int		ApplicationHeight { get; private set; }

		public bool		LoggerPerApplicationRun { get; private set; }
		public bool		LoggerConfigParamsDump { get; private set; }
		
		public bool		AudioUnfreezMuteEnable { get; private set; }
		public int		AudioUnfreezMuteVolume
		{
			get { return m_audioUnfreezMuteVolume; }
			private set
			{
				if (value > GlobalSettings.VOLUME_MAX)
					m_audioUnfreezMuteVolume = GlobalSettings.VOLUME_MAX;
				else if (value < GlobalSettings.VOLUME_MIN)
					m_audioUnfreezMuteVolume = GlobalSettings.VOLUME_MIN;
				else
					m_audioUnfreezMuteVolume = value;
			}
		}
		public int		AudioUnfreezMuteRestoreSeconds { get; private set; }
		public double	AudioPCGainMultiplier { get; private set; }

		public bool		DSPCopyRxMessageBuffer { get; private set; }
		public bool		DSPCopyTxMessageBuffer { get; private set; }
		public bool		DSPInvalidMessageCheckNumerator { get; private set; }
        public bool     DSPInvalidMessageAccept { get; private set; }
        public bool     DSPInvalidMessageDrop { get; private set; }
		public bool		DSPInvalidMessageUsePrevious { get; private set; }
		public bool		DSPPerformanceCountersEnabled { get; private set; }

		public int		EventPlayOffsetSeconds { get; private set; }

		public bool		PlaybackSendAudioDataToDSP { get; private set; }
		public int		PlaybackRawDataCacheForward { get; private set; }
		public int		PlaybackRawDataCacheBackward { get; private set; }

		public int		PACSEchoRetryCount { get; private set; }
		public bool		PACSRetainSentImage { get; private set; }
		public bool		PACSRetainSentDicom { get; private set; }

		public int		DiskSysFreeGBStop { get; private set; }
		public int		DiskSysFreeGBWarning { get; private set; }
		public int		DiskSysFreePercentWarinig { get; private set; }

		public int		DiskDataFreeGBStop { get; private set; }
		public int		DiskDataFreeGBWarning { get; private set; }
		public int		DiskDataFreePercentWarinig { get; private set; }

		public General.EAppOperationMode	AppOperationMode { get; private set; }
		public EAppWorkMode					AppWorkMode { get; set; }
		public string						ApplicationProductName { get; private set; }
		private General.EAppProduct			AppProduct
		{
			get { return m_appProduct; }
			set
			{
				m_appProduct			= value;
				ApplicationProductName	= General.AppProductName(m_appProduct);
			}
		}

		public bool					IsDspOffMode
		{
			set { m_isDspOffMode = value; }
			get { return m_isDspOffMode || IsReviewStation || IsDemoMode; }
		}

		public bool					IsDemoMode			{ get { return AppWorkMode == EAppWorkMode.DEMO; } }

		public bool					IsReviewStation { get { return AppProduct == General.EAppProduct.REVIEW_STATION; } }
		public bool					IsDigiLite { get { return AppProduct == General.EAppProduct.DIGI_LITE; } }
		public bool					IsDigiOne { get { return AppProduct == General.EAppProduct.DIGI_ONE; } }

		public bool					AutoScanYAxisReverse { get; private set; }
		public bool					AutoScanYAxisToggle { get; private set; }
		public bool					AutoScanRangeAutoChange { get; private set; }

		/// <summary># of blocks to mute.</summary>
		public int					AudioNoiseMuteDuration{ get; private set; }
		/// <summary>Column cells average size</summary>
		public int		AudioNoiseMuteExtent{ get; private set; }
		/// <summary>Mute threshold</summary>
		public int		AudioNoiseMuteThreshold { get; private set; }
		public bool		AudioNoiseMuteEnabled { get; private set; }
		
		public bool		DiagnosticPostSaveEdit { get; set; }
		public int		BVListVisibleSelectedOffset { get; private set; }

		public int		ClinicalParamZeroDurationThreshold { get; private set; }

		public bool		IsUseEventsRelativeTime { get; private set; }

		private AppSettings()
		{
			load();
		}


		private General.EApplicationSize ApplicationSize { get; set; }

		private void init()
		{
			ApplicationSize					= General.EApplicationSize.NONE;
			ApplicatioWidth					= Constants.DIGI_LITE_SCREEN_WIDTH;
			ApplicationHeight				= Constants.DIGI_LITE_SCREEN_HEIGHT;
			ApplicationTitle				= true;

			LoggerPerApplicationRun			= false;
			LoggerConfigParamsDump			= true;

			AudioUnfreezMuteEnable			= false;
			AudioUnfreezMuteVolume			= 1;
			AudioUnfreezMuteRestoreSeconds	= 5;

			PlaybackSendAudioDataToDSP		= true;
			PlaybackRawDataCacheForward		= DEF_PLAYBACK_RAW_DATA_CACHE_FORWARD;
			PlaybackRawDataCacheBackward	= DEF_PLAYBACK_RAW_DATA_CACHE_BACKWARD;

			PACSEchoRetryCount				= 1;
			PACSRetainSentImage				= false;
			PACSRetainSentDicom				= false;

			AudioPCGainMultiplier			= 1D;

			EventPlayOffsetSeconds			= 2;

			DSPInvalidMessageAccept			= true;
			DSPInvalidMessageDrop			= false;
			DSPInvalidMessageUsePrevious	= false;
		    DSPInvalidMessageCheckNumerator = false;
			DSPCopyRxMessageBuffer			= true;
			DSPCopyTxMessageBuffer			= true;

			IsDspOffMode						= false;
			DSPPerformanceCountersEnabled	= false;

			AppOperationMode				= General.EAppOperationMode.PRODUCTION;
			AppWorkMode						= EAppWorkMode.FULL;
			AppProduct						= General.EAppProduct.TCD;

			DiskSysFreeGBStop				= 10;
			DiskSysFreeGBWarning			= 10;
			DiskSysFreePercentWarinig		= 10;

			DiskDataFreeGBStop				= 25;
			DiskDataFreeGBWarning			= 50;
			DiskDataFreePercentWarinig		= 15;

			AutoScanYAxisReverse			= false;
			AutoScanYAxisToggle				= false;
			AutoScanRangeAutoChange			= false;

			AudioNoiseMuteDuration			= 10;
			AudioNoiseMuteExtent			= 30;
			AudioNoiseMuteThreshold			= 190;
			AudioNoiseMuteEnabled			= false;
			
			DiagnosticPostSaveEdit			= false;
			BVListVisibleSelectedOffset		= 10;

			ClinicalParamZeroDurationThreshold = 2500;

			IsUseEventsRelativeTime			= true;
		}

		private void load()
		{
			init();

            ApplicationTitle				= AppConfig.GetConfigValue("Application:Title", ApplicationTitle);

			LoggerPerApplicationRun			= AppConfig.GetConfigValue("Logger:PerApplicationRun", LoggerPerApplicationRun);
			LoggerConfigParamsDump			= AppConfig.GetConfigValue("Logger:ConfigurationParameters.Dump", LoggerConfigParamsDump);

			AudioUnfreezMuteEnable			= AppConfig.GetConfigValue("Audio:UnfreezMute.Enabled", AudioUnfreezMuteEnable); ;
			AudioUnfreezMuteVolume			= AppConfig.GetConfigValue("Audio:UnfreezMute.Volume", AudioUnfreezMuteVolume);
			AudioUnfreezMuteRestoreSeconds	= AppConfig.GetConfigValue("Audio:UnfreezMute.RestoreSeconds", AudioUnfreezMuteVolume);
			AudioPCGainMultiplier			= AppConfig.GetConfigValue("Audio:PCSpeakers.Gain.Multiplier", AudioPCGainMultiplier);

			PACSEchoRetryCount				= AppConfig.GetConfigValue("PACS:Echo.RetryCount", PACSEchoRetryCount);
			PACSRetainSentImage				= AppConfig.GetConfigValue("PACS:Send.Image.Retain", PACSRetainSentImage);
			PACSRetainSentDicom				= AppConfig.GetConfigValue("PACS:Send.Dicom.File.Retain", PACSRetainSentDicom);
			
			PlaybackSendAudioDataToDSP = AppConfig.GetConfigValue("Playback:SendAudioDataToDSP", PlaybackSendAudioDataToDSP);
			PlaybackRawDataCacheForward		= AppConfig.GetConfigValue("Playback:RawData.Cache.Forward", PlaybackRawDataCacheForward);
			if (PlaybackRawDataCacheForward < 0)
				PlaybackRawDataCacheForward = DEF_PLAYBACK_RAW_DATA_CACHE_FORWARD;

			PlaybackRawDataCacheBackward = AppConfig.GetConfigValue("Playback:RawData.Cache.Backword", PlaybackRawDataCacheBackward);
			if (PlaybackRawDataCacheBackward < 0)
				PlaybackRawDataCacheBackward = DEF_PLAYBACK_RAW_DATA_CACHE_BACKWARD;

			var value						= AppConfig.GetConfigValue("Application:Window.Size");
			General.EApplicationSize enumVal;
			if (Enum.TryParse(value, true, out enumVal))
				loadApplicationSize(enumVal);
			else
				loadApplicationSize(General.EApplicationSize.NONE);

			DSPPerformanceCountersEnabled	= AppConfig.GetConfigValue("DSPManager:PerformanceCounters.Enable", DSPPerformanceCountersEnabled);
			DSPCopyRxMessageBuffer			= AppConfig.GetConfigValue("DSPManager:CopyRxMessageBuffer", DSPCopyRxMessageBuffer);
			DSPCopyTxMessageBuffer			= AppConfig.GetConfigValue("DSPManager:CopyTxMessageBuffer", DSPCopyTxMessageBuffer);
			DSPInvalidMessageCheckNumerator = AppConfig.GetConfigValue("DSPManager:InvalidMessage.CheckNumerator", DSPInvalidMessageCheckNumerator);
			DSPInvalidMessageAccept			= AppConfig.GetConfigValue("DSPManager:InvalidMessage", "ACCEPT", true);
			if (!DSPInvalidMessageAccept)
			{
				DSPInvalidMessageDrop			= AppConfig.GetConfigValue("DSPManager:InvalidMessage", "DROP", true);
				DSPInvalidMessageUsePrevious	= !DSPInvalidMessageDrop;
			}

			EventPlayOffsetSeconds			= AppConfig.GetConfigValue("EventPlayOffsetSeconds", EventPlayOffsetSeconds);

			value = AppConfig.GetConfigValue("Application:OperationMode");
			General.EAppOperationMode operationMode;
			if (Enum.TryParse(value, true, out operationMode))
				AppOperationMode = operationMode;

			value = AppConfig.GetConfigValue("Application:WorkMode");
			EAppWorkMode worknMode;
			if (Enum.TryParse(value, true, out worknMode))
				AppWorkMode = worknMode;

			value = AppConfig.GetConfigValue("Application:Product");
			General.EAppProduct appProduct;
			if (Enum.TryParse(value, true, out appProduct))
				AppProduct = appProduct;

			IsDspOffMode					= (IsReviewStation || IsDemoMode);

			DiskSysFreeGBStop				= AppConfig.GetConfigValue("Disk:Sys.FreeGB.Stop", DiskSysFreeGBStop);
			DiskSysFreeGBWarning			= AppConfig.GetConfigValue("Disk:Sys.FreeGB.Warning", DiskSysFreeGBWarning);
			DiskSysFreePercentWarinig		= AppConfig.GetConfigValue("Disk:Sys.FreePercent.Warning", DiskSysFreePercentWarinig);

			DiskDataFreeGBStop				= AppConfig.GetConfigValue("Disk:Data.FreeGB.Stop", DiskDataFreeGBStop);
			DiskDataFreeGBWarning			= AppConfig.GetConfigValue("Disk:Data.FreeGB.Warning", DiskDataFreeGBWarning);
			DiskDataFreePercentWarinig		= AppConfig.GetConfigValue("Disk:Data.FreePercent.Warning", DiskDataFreePercentWarinig);

			AutoScanYAxisReverse			= AppConfig.GetConfigValue("AutoScan.YAxis.Reverse", AutoScanYAxisReverse);
			AutoScanYAxisToggle				= AppConfig.GetConfigValue("AutoScan.YAxis.Toggle", AutoScanYAxisToggle);
			AutoScanRangeAutoChange			= AppConfig.GetConfigValue("AutoScan.Range.AutoChange", AutoScanRangeAutoChange);
			

			AudioNoiseMuteDuration			= AppConfig.GetConfigValue("Audio:Noise.Mute.Duration", AudioNoiseMuteDuration);
			AudioNoiseMuteExtent			= AppConfig.GetConfigValue("Audio:Noise.Mute.Extent", AudioNoiseMuteExtent);
			AudioNoiseMuteThreshold			= AppConfig.GetConfigValue("Audio:Noise.Mute.Threshold", AudioNoiseMuteThreshold);

			if (AudioNoiseMuteExtent < 5)
				AudioNoiseMuteExtent = 5;
			else if (AudioNoiseMuteExtent > 75)
				AudioNoiseMuteExtent = 75;
					
			AudioNoiseMuteEnabled				= (AudioNoiseMuteThreshold > 100 && AudioNoiseMuteThreshold < 255 && AppConfig.GetConfigValue("Audio:Noise.Mute.Enabled", AudioNoiseMuteEnabled));
			DiagnosticPostSaveEdit				= AppConfig.GetConfigValue("Diagnostic:PostSaveEdit", DiagnosticPostSaveEdit);
			BVListVisibleSelectedOffset			= AppConfig.GetConfigValue("BVList:VisibleSelectedOffset", BVListVisibleSelectedOffset);
			ClinicalParamZeroDurationThreshold	= AppConfig.GetConfigValue("ClinicalParameters:Value.ZeroDurationThreshold", ClinicalParamZeroDurationThreshold);
			IsUseEventsRelativeTime				= AppConfig.GetConfigValue("Events:UseRelativeTime", IsUseEventsRelativeTime);
		}

		private void loadApplicationSize(General.EApplicationSize resizeMode)
		{
			ApplicationSize = resizeMode;

			if (ApplicationSize == General.EApplicationSize.CUSTOM)
			{
				ApplicatioWidth = AppConfig.GetConfigValue("Application:Window.Size.Custom.Width", ApplicatioWidth);
				ApplicationHeight = AppConfig.GetConfigValue("Application:Window.Size.Custom.Height", ApplicationHeight);
			}
			else if (ApplicationSize == General.EApplicationSize.SCREEN)
			{
				var srn = Screen.PrimaryScreen;
				ApplicatioWidth	= srn.Bounds.Width;
				ApplicationHeight = srn.Bounds.Height;					
			}
			else if (ApplicationSize == General.EApplicationSize.AUTO)
			{
				var srn = Screen.PrimaryScreen;
				ApplicatioWidth = srn.Bounds.Width;
				ApplicationHeight = 800;
			}

			if (ApplicatioWidth < Constants.DIGI_LITE_SCREEN_WIDTH)
				ApplicatioWidth = Constants.DIGI_LITE_SCREEN_WIDTH;

			if (ApplicationHeight < Constants.DIGI_LITE_SCREEN_HEIGHT)
				ApplicationHeight = Constants.DIGI_LITE_SCREEN_HEIGHT;
		}
	}
}
