﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Rimed.TCD.GUI
{
	public partial class PopupProgress : Form
	{
		private readonly static PopupProgress s_popupProgress = new PopupProgress();
		private PopupProgress()
		{
			InitializeComponent();
		}

		public static void Show(string text, string detiles, int max = 100, int min=0)
		{
			s_popupProgress.labelMsg.Text			= text;
			s_popupProgress.labelDetiles.Text		= detiles;

			s_popupProgress.progressBar1.Minimum	= min;
			s_popupProgress.progressBar1.Value		= min;
			s_popupProgress.progressBar1.Maximum	= max;
			
			s_popupProgress.CenterToScreen();
			s_popupProgress.Show();
		}

		public static void HidePopup()
		{
			s_popupProgress.Hide();

			s_popupProgress.labelMsg.Text		= string.Empty;
			s_popupProgress.labelDetiles.Text	= string.Empty;
			s_popupProgress.progressBar1.Value	= s_popupProgress.progressBar1.Minimum;
		}

		public static void Update(int value, string detiles = null, string text = null)
		{
			if (text != null)
				s_popupProgress.labelMsg.Text = text;

			if (detiles != null)
				s_popupProgress.labelDetiles.Text = detiles;

			if (value < s_popupProgress.progressBar1.Minimum)
				value = s_popupProgress.progressBar1.Minimum;

			if (value > s_popupProgress.progressBar1.Maximum)
				value = s_popupProgress.progressBar1.Maximum;
			
			var percentage = 0;
			var range = s_popupProgress.progressBar1.Maximum - s_popupProgress.progressBar1.Minimum;
			if (range > 0)
				percentage = 100 * (value - s_popupProgress.progressBar1.Minimum) / range;

			s_popupProgress.progressBar1.Value = value;
			s_popupProgress.labelPercent.Text = string.Format("{0}%", percentage);
			
			s_popupProgress.Refresh();
		}

		public static void UpdateFile(int value, string file)
		{
			Update(value, file);
		}
	}
}
