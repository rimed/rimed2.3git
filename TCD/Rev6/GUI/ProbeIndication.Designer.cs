﻿namespace Rimed.TCD.GUI
{
    public partial class ProbeIndication
    {
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code
        
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ProbeRect = new System.Windows.Forms.PictureBox();
            this.Probelabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ProbeRect
            // 
            this.ProbeRect.BackColor = System.Drawing.Color.Red;
            this.ProbeRect.Dock = System.Windows.Forms.DockStyle.Left;
            this.ProbeRect.Location = new System.Drawing.Point(0, 0);
            this.ProbeRect.Name = "ProbeRect";
            this.ProbeRect.Size = new System.Drawing.Size(16, 16);
            this.ProbeRect.TabIndex = 1;
            this.ProbeRect.TabStop = false;
            // 
            // Probelabel
            // 
            this.Probelabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Probelabel.Location = new System.Drawing.Point(16, 0);
            this.Probelabel.Name = "Probelabel";
            this.Probelabel.Size = new System.Drawing.Size(48, 16);
            this.Probelabel.TabIndex = 2;
            this.Probelabel.Text = "label1";
            this.Probelabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ProbeIndication
            // 
            this.Controls.Add(this.Probelabel);
            this.Controls.Add(this.ProbeRect);
            this.Name = "ProbeIndication";
            this.Size = new System.Drawing.Size(64, 16);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox ProbeRect;
        private System.Windows.Forms.Label Probelabel;
    }
}