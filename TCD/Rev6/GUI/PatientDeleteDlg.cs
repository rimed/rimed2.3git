using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Rimed.Framework.Common;
using Rimed.Framework.GUI;
using Rimed.TCD.DAL;
using Rimed.TCD.Utils;

namespace Rimed.TCD.GUI
{
    public partial class PatientDeleteDlg : Form
    {
        public PatientDeleteDlg()
        {
            InitializeComponent();

            var strRes = MainForm.StringManager;
            columnHeaderName.Text = strRes.GetString("Patient Name");
            columnHeaderID.Text = strRes.GetString("Patient ID");
            buttonDelete.Text = strRes.GetString("Delete");
            buttonCancel.Text = strRes.GetString("Close");
            buttonHelp.Text = strRes.GetString("Help");
            Text = strRes.GetString("Delete Patient");

            BackColor = GlobalSettings.BackgroundDlg;
            
            int widthCtrl = listView1.Width;
            listView1.Columns[0].Width = (int)(widthCtrl / 2);
            listView1.Columns[1].Width = widthCtrl - listView1.Columns[0].Width;
        }

        private void PatientDeleteDlg_Load(object sender, System.EventArgs e)
        {
            // fill the combobox.
            const string SELECT = "Select tb_Patient.* FROM tb_Patient WHERE (Deleted = false AND First_Name <> 'Undefined')";
            dsPatient dsP = RimedDal.Instance.GetPatients(SELECT);

            foreach (dsPatient.tb_PatientRow row in dsP.tb_Patient.Rows)
            {
                var item = listView1.Items.Add(row.Last_Name + " " + row.First_Name);
                item.SubItems.Add(row.ID);
                item.Tag = row.Patient_Index;
            }
        }

        private void buttonDelete_Click(object sender, System.EventArgs e)
        {
            ListView.SelectedIndexCollection sc = listView1.SelectedIndices;
            if (sc.Count == 0)
            {
                LoggedDialog.ShowError(this, MainForm.StringManager.GetString("No patient was selected!"));

                return;
            }

            var fields = LoggerUserActions.GetFilesList();
            fields.Add("listView1", listView1.Items[sc[0]].Text);
            LoggerUserActions.MouseClick("buttonDelete", Name, fields);

            if (MainForm.Instance.SelectedPatientIndex == ((Guid)this.listView1.Items[sc[0]].Tag))
            {
                LoggedDialog.ShowError(this, MainForm.StringManager.GetString("You are trying to delete the current patient, please change the current patient and try again."));
                return;
            }

            if (LoggedDialog.ShowWarning(this, MainForm.StringManager.GetString("Are you sure you want to delete the flowing patients:") + "\n" + listView1.Items[sc[0]].Text, buttons: MessageBoxButtons.OKCancel) == DialogResult.OK)
            {

	            using (new CursorSafeChange(this, true))
	            {
		            // delete from the database.
		            RimedDal.Instance.DeletePatient((Guid) listView1.Items[sc[0]].Tag);

		            // delete from the list view.
		            listView1.Items.Remove(listView1.Items[sc[0]]);
		            RimedDal.Instance.UpdatePatientsToDB();
	            }
            }
        }

        /// <summary>Deny delete test patient (issue RIMD-409)</summary>
        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var sc = listView1.SelectedIndices;
            buttonDelete.Enabled = sc.Count == 0 || ((Guid)listView1.Items[sc[0]].Tag != Guid.Empty);
        }
    }
}
