﻿namespace Rimed.TCD.GUI
{
    partial class ProbesCombobox
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            BVListViewCtrl.Instance.BVChangedEvent -= new BVChangedDelegate(ProbesCombobox_BVChanged);
            this.SelectedIndexChanged -= new System.EventHandler(this.ProbesCombobox_SelectedIndexChanged);
            this.SelectionChangeCommitted -= new System.EventHandler(ProbesCombobox_SelectionChangeCommitted);

            if (disposing)
            {
                if (components != null)
                    components.Dispose();
            }
            base.Dispose(disposing);
        }

        private System.ComponentModel.Container components = null;
    }
}