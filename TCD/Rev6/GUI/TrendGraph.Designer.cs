using System.Windows.Forms;

namespace Rimed.TCD.GUI
{
    partial class TrendGraph
    {
        /// <summary>Clean up any resources being used.</summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }

                if (s_yelloPenThick != null)
                {
					s_yelloPenThick.Dispose();
					s_yelloPenThick = null;
				}

                if (m_invalidateTimer != null)
                {
                    m_invalidateTimer.Stop();
                    m_invalidateTimer.Dispose();
					m_invalidateTimer = null;
                }

                this.ReleaseResources();
            }

            base.Dispose(disposing);
        }

        #region Component Designer generated code

        private void InitializeComponent()
        {
			this.PictureBoxToShow = new System.Windows.Forms.PictureBox();
			((System.ComponentModel.ISupportInitialize)(this.PictureBoxToShow)).BeginInit();
			this.SuspendLayout();
			// 
			// PictureBoxToShow
			// 
			this.PictureBoxToShow.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.PictureBoxToShow.BackColor = System.Drawing.Color.Black;
			this.PictureBoxToShow.Location = new System.Drawing.Point(0, 0);
			this.PictureBoxToShow.Name = "PictureBoxToShow";
			this.PictureBoxToShow.Size = new System.Drawing.Size(580, 228);
			this.PictureBoxToShow.TabIndex = 1;
			this.PictureBoxToShow.TabStop = false;
			this.PictureBoxToShow.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PictureBoxToShow_MouseDown);
			this.PictureBoxToShow.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PictureBoxToShow_MouseMove);
			this.PictureBoxToShow.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PictureBoxToShow_MouseUp);
			// 
			// TrendGraph
			// 
			this.BackColor = System.Drawing.Color.Black;
			this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.Controls.Add(this.PictureBoxToShow);
			this.ForeColor = System.Drawing.SystemColors.InactiveCaption;
			this.Name = "TrendGraph";
			this.Size = new System.Drawing.Size(580, 228);
			this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.PictureBoxToShow_MouseDown);
			this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.PictureBoxToShow_MouseMove);
			this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PictureBoxToShow_MouseUp);
			this.Resize += new System.EventHandler(this.TrendGraph_Resize);
			((System.ComponentModel.ISupportInitialize)(this.PictureBoxToShow)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private System.ComponentModel.Container components = null;
        /// <summary>
        /// PictureBox for trends imaging 
        /// </summary>
		private System.Windows.Forms.PictureBox PictureBoxToShow;
    }
}