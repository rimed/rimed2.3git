using System;

namespace Rimed.TCD.GUI
{
	public class SingleEvent
	{
	    public  SingleEvent(Guid index, string name)
		{
			Index = index;
			Name = name;
		}

	    public string Name { get; private set; }

	    public Guid Index { get; private set; }
	}
}
