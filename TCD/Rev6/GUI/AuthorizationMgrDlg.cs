using System;
using System.Windows.Forms;
using Rimed.Framework.Common;
using Rimed.TCD.DAL;
using Rimed.TCD.Utils;

namespace Rimed.TCD.GUI
{
    public partial class AuthorizationMgrDlg : Form
    {
        #region Constants

        private const int VALIDATION_a_z = 219;
        private const int VALIDATION_A_Z = 155;
        private const int VALIDATION_0_9 = 105;

        private const int VALID1_POS = 0;
        private const int MONITORING_POS = 1;
        private const int AUTOSCAN_POS = 2;
        private const int HITS_POS = 3;
        private const int PROBE_8MHZ_POS = 4;
        private const int EXTRACRANIAL_POS = 5;
        private const int INTRACRANIA_UNILATERAL_POS = 6;
        private const int INTRACRANIAL_BILATERAL_POS = 7;
        private const int SEPERATOR1_POS = 8;

        private const int PERIPHERAL_POS = 9;
        private const int MULTIFREQUENCY_TWO_PROBES_POS = 10;
        private const int INTRAOPERATIVE_POS = 11;
        private const int M_EXTRACRANIAL_POS = 12;
        private const int SEPERATOR2_POS = 13;

        private const int M_INTRACRANIAL_UNILATERAL_POS = 14;
        private const int M_INTRACRANIAL_BILATERAL_POS = 15;
        private const int PROBE_1MHZ_POS = 16;
        private const int M_PERIPHERAL_POS = 17;
        private const int SEPERATOR3_POS = 18;

        private const int M_MULTIFREQUENCY_TWO_PROBES_POS = 19;
        private const int M_INTRAOPERATIVE_POS = 20;
        private const int PROBE_16MHZ_POS = 21;
        private const int VMRCO2_REACTIVITY_POS = 22;
        private const int SEPERATOR4_POS = 23;

        private const int VMR_DIAMOXTEST_POS = 24;
        private const int MONITORING_OFT_PA_POS = 25;
        private const int EVOKEDFLOW_BILATERALTEST_POS = 26;
        private const int SERIAL_NUMBER1_POS = 27;
        private const int VMR_BILATERAL_DIAMOXTEST_POS = 28;
        private const int DICOM_POS = 29;
        private const int PFO_TEST_POS = 30;
        //private const int VALID5_POS					= 31;
        private const int SERIAL_NUMBER2_POS = 31;
        private const int EVOKEDFLOWTEST_POS = 32;
        private const int PROBES_NUM_POS = 33;
        private const int VALID7_POS = 34;
        private const int VALID8_POS = 35;
        private const int VALID9_POS = 36;
        
        #endregion

        public AuthorizationMgrDlg()
        {
            InitializeComponent();
            BackColor = GlobalSettings.BackgroundDlg;
        }

        public bool OkButtonPressed { get; private set; }

        private void buttonOK_Click(object sender, System.EventArgs e)
        {
            var fields = LoggerUserActions.GetFilesList();
            fields.Add("textBox1", textBox1.Text);
            fields.Add("textBox2", textBox2.Text);
            fields.Add("textBox3", textBox3.Text);
            fields.Add("textBox4", textBox4.Text);
            fields.Add("textBox5", textBox5.Text);
            fields.Add("textBox6", textBox6.Text);
            fields.Add("textBox7", textBox7.Text);
            fields.Add("textBox8", textBox8.Text);
            fields.Add("textBox9", textBox9.Text);
            fields.Add("textBox10", textBox10.Text);
            fields.Add("textBoxSerialNumber", textBoxSerialNumber.Text);

            LoggerUserActions.MouseClick("buttonOK", Name, fields);

            // get the system key and the product key from the controls.
            var sysKey      = textBox6.Text + "-" + textBox7.Text + "-" + textBox8.Text + "-" + textBox9.Text + "-" + textBox10.Text;
            var productKey  = textBox1.Text + "-" + textBox2.Text + "-" + textBox3.Text + "-" + textBox4.Text + "-" + textBox5.Text;

            if (calculatePermission(sysKey, productKey))
            {
                RimedDal.Instance.AuthorizationMgrDS.tb_AuthorizationMgr[0].ProductKey = productKey;
                RimedDal.Instance.AuthorizationMgrDS.tb_AuthorizationMgr[0].SystemKey = sysKey;
                RimedDal.Instance.UpdateAuthorizationMgr();

                OkButtonPressed = true;
            }
        }

        static public bool CheckSerialNumber()
        {
            try
            {
                var info        = VolumeInfo.CurrentVolume();
                var serialNumber = info.SerialNumber;
                
                var n1          = (int)(serialNumber / 854127) % 10;
                var n2          = (int)(serialNumber / 54127) % 10;

                var productKey  = RimedDal.Instance.AuthorizationMgrDS.tb_AuthorizationMgr[0].ProductKey;
                var str1        = Convert.ToString(productKey[SERIAL_NUMBER1_POS]);
                var str2        = Convert.ToString(productKey[SERIAL_NUMBER2_POS]);

                if (str1.Equals(n1.ToString()) && str2.Equals(n2.ToString()))
                    return true;

                Logger.LogDebug("AuthorizationMgrDlg.CheckSerialNumber(): false. SN={0}, PK='{1}'", serialNumber, productKey);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }

            return false;
        }

        private void AuthorizationMgrDlg_Load(object sender, System.EventArgs e)
        {
            var strRes = MainForm.StringManager;// MainForm.StringManager;
            
            lblProductKey.Text  = strRes.GetString("Product Key:");
            lblSystemKey.Text   = strRes.GetString("System Key:");
            buttonCancel.Text   = strRes.GetString("Close");
            buttonOK.Text       = strRes.GetString("Apply");
            labelHD.Text        = strRes.GetString("HD Serial Number:");
            Text                = strRes.GetString("Authorization Manager");

            OkButtonPressed = false;
            string sysKey;
            try
            {
                sysKey = RimedDal.Instance.AuthorizationMgrDS.tb_AuthorizationMgr[0].SystemKey;
            }
            catch (Exception ex)
            {
                Logger.LogFatalError(ex);
                return;
            }

            fillSystemKey(sysKey);

            try
            {
                var productKey = RimedDal.Instance.AuthorizationMgrDS.tb_AuthorizationMgr[0].ProductKey;
                fillProductKey(productKey);

                var info = VolumeInfo.CurrentVolume();
                textBoxSerialNumber.Text = info.SerialNumber.ToString();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
                // if the product key does not exist in the database - do nothing.
            }
        }

        private bool calculatePermission(string sysKey, string proKey)
        {
            byte validation = 0;

            for (var i = 0; i < sysKey.Length; i++)
            {
                if (sysKey[i] != '-')
                {
                    if (sysKey[i] >= 97 && sysKey[i] <= 122)		// a..z
                        validation = VALIDATION_a_z;
                    else if (sysKey[i] >= 65 && sysKey[i] <= 90)	// A..Z
                        validation = VALIDATION_A_Z;
                    else if (sysKey[i] >= 48 && sysKey[i] <= 57)	// 0..1
                        validation = VALIDATION_0_9;

                    int enable = sysKey[i] + proKey[i] - validation;

                    if (i == VALID1_POS && enable != 1 || (i == VALID7_POS || i == VALID8_POS || i == VALID9_POS) && enable != 0)
                    {
                        Logger.LogError(string.Format("Permission calculation error (char). sysKey={0}, proKey={1}, i={2}, enable={3}", sysKey, proKey, i, enable));

                        return false;
                    }

                    if (i == MONITORING_POS)
                        RimedDal.Instance.AuthorizationMgrDS.tb_AuthorizationMgr[0].AMmonitoring = (enable == 1);
                    else if (i == AUTOSCAN_POS)
                        RimedDal.Instance.AuthorizationMgrDS.tb_AuthorizationMgr[0].AMautoscan = (enable == 1);
                    else if (i == HITS_POS)
                        RimedDal.Instance.AuthorizationMgrDS.tb_AuthorizationMgr[0].AMhits = (enable == 1);
                    else if (i == PROBE_8MHZ_POS)
                        RimedDal.Instance.EnableProbeAppearance(8, (enable == 1));
                    else if (i == PROBE_1MHZ_POS)
                        RimedDal.Instance.EnableProbeAppearance(1, (enable == 1));
                    else if (i == PROBE_16MHZ_POS)
                        RimedDal.Instance.EnableProbeAppearance(16, (enable == 1));
                    else if (i == EXTRACRANIAL_POS)
                        RimedDal.Instance.EnableStudyAppearance(Constants.StudyType.STUDY_EXTRACRANIAL, false, (enable == 1));
                    else if (i == INTRACRANIA_UNILATERAL_POS)
                        RimedDal.Instance.EnableStudyAppearance(Constants.StudyType.STUDY_INTERCRANIAL_UNILATERAL, false, (enable == 1));
                    else if (i == INTRACRANIAL_BILATERAL_POS)
						RimedDal.Instance.EnableStudyAppearance(Constants.StudyType.STUDY_INTERCRANIAL_BILATERAL, false, (enable == 1));
                    else if (i == PERIPHERAL_POS)
						RimedDal.Instance.EnableStudyAppearance(Constants.StudyType.STUDY_PERIPHERAL, false, (enable == 1));
                    else if (i == MULTIFREQUENCY_TWO_PROBES_POS)
						RimedDal.Instance.EnableStudyAppearance(Constants.StudyType.STUDY_MULTIFREQ_TWO_PROBES, false, (enable == 1));
                    else if (i == INTRAOPERATIVE_POS)
						RimedDal.Instance.EnableStudyAppearance(Constants.StudyType.STUDY_INTRAOPERATIVE, false, (enable == 1));
                    else if (i == M_EXTRACRANIAL_POS)
                        RimedDal.Instance.EnableStudyAppearance(Constants.StudyType.STUDY_EXTRACRANIAL, true, (enable == 1));
                    else if (i == M_INTRACRANIAL_UNILATERAL_POS)
						RimedDal.Instance.EnableStudyAppearance(Constants.StudyType.STUDY_MONITORING_INTRACRANIAL_UNILATERAL, true, (enable == 1));
                    else if (i == M_INTRACRANIAL_BILATERAL_POS)
						RimedDal.Instance.EnableStudyAppearance(Constants.StudyType.STUDY_MONITORING_INTRACRANIAL_BILATERAL, true, (enable == 1));
                    else if (i == M_PERIPHERAL_POS)
                        RimedDal.Instance.EnableStudyAppearance(Constants.StudyType.STUDY_PERIPHERAL, true, (enable == 1));
                    else if (i == M_MULTIFREQUENCY_TWO_PROBES_POS)
                        RimedDal.Instance.EnableStudyAppearance(Constants.StudyType.STUDY_MULTIFREQ_TWO_PROBES, true, (enable == 1));
                    else if (i == M_INTRAOPERATIVE_POS)
						RimedDal.Instance.EnableStudyAppearance(Constants.StudyType.STUDY_INTRAOPERATIVE, true, (enable == 1));
                    else if (i == VMRCO2_REACTIVITY_POS)
                        RimedDal.Instance.EnableStudyAppearance("VMR CO2 Reactivity", true, (enable == 1));
                    else if (i == VMR_DIAMOXTEST_POS)
						RimedDal.Instance.EnableStudyAppearance(Constants.StudyType.STUDY_VMR, true, (enable == 1));
                    else if (i == MONITORING_OFT_PA_POS)
                        RimedDal.Instance.EnableStudyAppearance("Monitoring Of tPA", true, (enable == 1));
                    else if (i == EVOKEDFLOW_BILATERALTEST_POS)
						RimedDal.Instance.EnableStudyAppearance(Constants.StudyType.STUDY_EVOKED_FLOW_BILATERAL, true, (enable == 1));
                    else if (i == VMR_BILATERAL_DIAMOXTEST_POS)
						RimedDal.Instance.EnableStudyAppearance(Constants.StudyType.STUDY_VMR_BILATERAL, true, (enable == 1));
                    else if (i == DICOM_POS)
                        RimedDal.Instance.AuthorizationMgrDS.tb_AuthorizationMgr[0].Dicom = (enable == 1);
                    else if (i == PFO_TEST_POS)
                        RimedDal.Instance.EnableStudyAppearance("PFO Test", false, (enable == 1));
                    else if (i == EVOKEDFLOWTEST_POS)
						RimedDal.Instance.EnableStudyAppearance(Constants.StudyType.STUDY_EVOKED_FLOW_UNILATERAL, true, (enable == 1));
                    else if (i == PROBES_NUM_POS)
                        RimedDal.Instance.AuthorizationMgrDS.tb_AuthorizationMgr[0].ProbesNum = (byte)(sysKey[i] + proKey[i] - validation);
                }
                else
                    if (i != SEPERATOR1_POS && i != SEPERATOR2_POS && i != SEPERATOR3_POS && i != SEPERATOR4_POS)
                    {
                        Logger.LogError(string.Format("Permission calculation error (separator). sysKey={0}, proKey={1}, i={2}", sysKey, proKey, i));

                        return false;
                    }
            }

            return true;
        }

        private void buttonCancel_Click(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick("buttonCancel", Name);
            OkButtonPressed = false;
        }

        private void btnPasteSystemKey_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            btnPasteSystemKey.BorderStyle = BorderStyle.Fixed3D;
        }

        private void btnPasteProductKey_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            btnPasteProductKey.BorderStyle = BorderStyle.Fixed3D;
        }

        private void btnPasteProductKey_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            btnPasteProductKey.BorderStyle = BorderStyle.None;
            IDataObject dataObj = Clipboard.GetDataObject();
            try
            {
                var productKey = dataObj.GetData(DataFormats.Text, false).ToString();
                fillProductKey(productKey);
            }
            catch
            {
                textBox1.Text = textBox2.Text = textBox3.Text = textBox4.Text = textBox5.Text = "";
            }

        }

        private void btnPasteSystemKey_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            btnPasteSystemKey.BorderStyle = BorderStyle.None;
            IDataObject dataObj = Clipboard.GetDataObject();
            try
            {
                var sysKey = dataObj.GetData(DataFormats.Text, false).ToString();
                fillSystemKey(sysKey);
            }
            catch
            {
                textBox6.Text = textBox7.Text = textBox8.Text = textBox9.Text = textBox10.Text = "";
            }
        }

        private void fillSystemKey(string systemKey)
        {
            var tmpArr = systemKey.Split('-');
            textBox6.Text = tmpArr[0];
            textBox7.Text = tmpArr[1];
            textBox8.Text = tmpArr[2];
            textBox9.Text = tmpArr[3];
            textBox10.Text = tmpArr[4];
        }

        private void fillProductKey(string productKey)
        {
            var tmpArr = productKey.Split('-');
            textBox1.Text = tmpArr[0];
            textBox2.Text = tmpArr[1];
            textBox3.Text = tmpArr[2];
            textBox4.Text = tmpArr[3];
            textBox5.Text = tmpArr[4];
        }

        private void AuthorizationMgrDlg_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            e.Handled = true;
            switch (e.KeyCode)
            {
                case Keys.V:
                    if (e.Control)
                    {
                        var dataObj = Clipboard.GetDataObject();
                        if (dataObj == null)
                            return;

                        var key     = dataObj.GetData(DataFormats.Text, false).ToString();
                        if (textBox6.Focused || textBox7.Focused || textBox8.Focused || textBox9.Focused || textBox10.Focused)
                        {
                            try
                            {
                                fillSystemKey(key);
                            }
                            catch
                            {
                                textBox6.Text = textBox7.Text = textBox8.Text = textBox9.Text = textBox10.Text = "";
                            }
                        }

                        if (textBox1.Focused || textBox2.Focused || textBox3.Focused || textBox4.Focused || textBox5.Focused)
                        {
                            try
                            {
                                fillProductKey(key);
                            }
                            catch
                            {
                                textBox1.Text = textBox2.Text = textBox3.Text = textBox4.Text = textBox5.Text = "";
                            }
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
