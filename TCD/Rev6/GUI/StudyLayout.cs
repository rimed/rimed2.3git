using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;


namespace Rimed.TCD.GUI
{
	/// <summary>
	/// Summary description for StudyLayout.
	/// </summary>
	public class StudyLayout
	{
		public int GateNumLeft = 0;
		public int GateNumRight = 0;
	    private Guid m_probeLeftIndex = Guid.Empty;
	    private Guid m_probeRightIndex = Guid.Empty;
		public bool DispAutoscan0 = false;
		public bool DispAutoscan1 = false;
		public void Clear()
		{
			GateNumLeft = 0;
			GateNumRight = 0;
			m_probeLeftIndex = Guid.Empty;
			m_probeRightIndex = Guid.Empty;
			DispAutoscan0 = false;
			DispAutoscan1 = false;
		}
		public void Serialize( Stream ms )
		{
			IFormatter formatter = new BinaryFormatter();
			formatter.Serialize(ms, GateNumLeft);
			formatter.Serialize(ms, GateNumRight);
			formatter.Serialize(ms, m_probeLeftIndex);
			formatter.Serialize(ms, m_probeRightIndex);
			formatter.Serialize(ms, DispAutoscan0);
			formatter.Serialize(ms, DispAutoscan1);
		}
		public void Deserialize( Stream ms )
		{
			IFormatter formatter = new BinaryFormatter();
			GateNumLeft = (int)formatter.Deserialize(ms);
			GateNumRight = (int)formatter.Deserialize(ms);
			m_probeLeftIndex = (Guid)formatter.Deserialize(ms);
			m_probeRightIndex = (Guid)formatter.Deserialize(ms);
			DispAutoscan0 = (bool)formatter.Deserialize(ms);
			DispAutoscan1 = (bool)formatter.Deserialize(ms);
		}
	}
}
