﻿namespace Rimed.TCD.GUI
{
    partial class AddSecondSpectrumDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCaption = new System.Windows.Forms.Label();
            this.btnMModeClose = new System.Windows.Forms.Button();
            this.btnTrendClose = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblCaption
            // 
            this.lblCaption.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblCaption.Location = new System.Drawing.Point(12, 9);
            this.lblCaption.Name = "lblCaption";
            this.lblCaption.Size = new System.Drawing.Size(479, 53);
            this.lblCaption.TabIndex = 0;
            this.lblCaption.Text = "You need to close the M-Mode window or the Trend window in order to add Spectrum " +
                "window";
            this.lblCaption.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnMModeClose
            // 
            this.btnMModeClose.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnMModeClose.AutoSize = true;
            this.btnMModeClose.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnMModeClose.Location = new System.Drawing.Point(41, 78);
            this.btnMModeClose.Name = "btnMModeClose";
            this.btnMModeClose.Size = new System.Drawing.Size(100, 23);
            this.btnMModeClose.TabIndex = 1;
            this.btnMModeClose.Text = "Close M-Mode";
            this.btnMModeClose.UseVisualStyleBackColor = true;
            // 
            // btnTrendClose
            // 
            this.btnTrendClose.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnTrendClose.AutoSize = true;
            this.btnTrendClose.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.btnTrendClose.Location = new System.Drawing.Point(198, 78);
            this.btnTrendClose.Name = "btnTrendClose";
            this.btnTrendClose.Size = new System.Drawing.Size(100, 23);
            this.btnTrendClose.TabIndex = 2;
            this.btnTrendClose.Text = "Close Trend";
            this.btnTrendClose.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnCancel.AutoSize = true;
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(354, 78);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 23);
            this.btnCancel.TabIndex = 3;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // AddSecondSpectrumDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(503, 128);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnTrendClose);
            this.Controls.Add(this.btnMModeClose);
            this.Controls.Add(this.lblCaption);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(500, 100);
            this.Name = "AddSecondSpectrumDlg";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCaption;
        private System.Windows.Forms.Button btnMModeClose;
        private System.Windows.Forms.Button btnTrendClose;
        private System.Windows.Forms.Button btnCancel;
    }
}