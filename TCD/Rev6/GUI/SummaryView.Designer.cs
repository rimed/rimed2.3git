﻿namespace Rimed.TCD.GUI
{
    partial class SummaryView
    {
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                HardDriveSpaceKeeper.Instance.RowDataModeChanged -= new RowDataModeChangedHandler(HardDriveSpaceKeeper_RowDataModeChanged);
				
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SummaryView));
			this.panel1 = new System.Windows.Forms.Panel();
			this.probeColor = new System.Windows.Forms.Label();
			this.probeName = new System.Windows.Forms.Label();
			this.BVLabel = new System.Windows.Forms.Label();
			this.btnRowDataStatus = new System.Windows.Forms.Label();
			this.depthValue = new System.Windows.Forms.Label();
			this.depthLabel = new System.Windows.Forms.Label();
			this.probeImage = new System.Windows.Forms.Label();
			this.probeImageList = new System.Windows.Forms.ImageList(this.components);
			this.btnMoreClinical = new System.Windows.Forms.Button();
			this.paramButtonImageList = new System.Windows.Forms.ImageList(this.components);
			this.SummarySpectrumChart1 = new Rimed.TCD.GUI.SummarySpectrumChart();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.probeColor);
			this.panel1.Controls.Add(this.probeName);
			this.panel1.Controls.Add(this.BVLabel);
			this.panel1.Controls.Add(this.btnRowDataStatus);
			this.panel1.Controls.Add(this.depthValue);
			this.panel1.Controls.Add(this.depthLabel);
			this.panel1.Controls.Add(this.probeImage);
			this.panel1.Controls.Add(this.btnMoreClinical);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(340, 25);
			this.panel1.TabIndex = 1;
			// 
			// probeColor
			// 
			this.probeColor.BackColor = System.Drawing.Color.Blue;
			this.probeColor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.probeColor.Location = new System.Drawing.Point(269, 8);
			this.probeColor.Name = "probeColor";
			this.probeColor.Size = new System.Drawing.Size(11, 12);
			this.probeColor.TabIndex = 19;
			// 
			// probeName
			// 
			this.probeName.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(177)));
			this.probeName.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.probeName.Location = new System.Drawing.Point(283, 6);
			this.probeName.Name = "probeName";
			this.probeName.Size = new System.Drawing.Size(54, 14);
			this.probeName.TabIndex = 18;
			this.probeName.Text = "1MGz PW";
			this.probeName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// BVLabel
			// 
			this.BVLabel.Location = new System.Drawing.Point(5, 7);
			this.BVLabel.Name = "BVLabel";
			this.BVLabel.Size = new System.Drawing.Size(90, 19);
			this.BVLabel.TabIndex = 4;
			this.BVLabel.Text = "MCA-L";
			// 
			// btnRowDataStatus
			// 
			this.btnRowDataStatus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnRowDataStatus.ForeColor = System.Drawing.Color.Transparent;
			this.btnRowDataStatus.Location = new System.Drawing.Point(96, 0);
			this.btnRowDataStatus.Name = "btnRowDataStatus";
			this.btnRowDataStatus.Size = new System.Drawing.Size(24, 24);
			this.btnRowDataStatus.TabIndex = 0;
			this.btnRowDataStatus.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnRowDataStatus_MouseDown);
			// 
			// depthValue
			// 
			this.depthValue.Location = new System.Drawing.Point(167, 5);
			this.depthValue.Name = "depthValue";
			this.depthValue.Size = new System.Drawing.Size(26, 19);
			this.depthValue.TabIndex = 3;
			this.depthValue.Text = "48";
			this.depthValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// depthLabel
			// 
			this.depthLabel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
			this.depthLabel.Location = new System.Drawing.Point(134, 5);
			this.depthLabel.Name = "depthLabel";
			this.depthLabel.Size = new System.Drawing.Size(65, 19);
			this.depthLabel.TabIndex = 2;
			this.depthLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// probeImage
			// 
			this.probeImage.ImageIndex = 0;
			this.probeImage.ImageList = this.probeImageList;
			this.probeImage.Location = new System.Drawing.Point(206, 7);
			this.probeImage.Name = "probeImage";
			this.probeImage.Size = new System.Drawing.Size(28, 14);
			this.probeImage.TabIndex = 1;
			// 
			// probeImageList
			// 
			this.probeImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("probeImageList.ImageStream")));
			this.probeImageList.TransparentColor = System.Drawing.Color.Transparent;
			this.probeImageList.Images.SetKeyName(0, "");
			this.probeImageList.Images.SetKeyName(1, "");
			this.probeImageList.Images.SetKeyName(2, "");
			this.probeImageList.Images.SetKeyName(3, "");
			// 
			// btnMoreClinical
			// 
			this.btnMoreClinical.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnMoreClinical.ForeColor = System.Drawing.Color.Transparent;
			this.btnMoreClinical.ImageIndex = 4;
			this.btnMoreClinical.ImageList = this.paramButtonImageList;
			this.btnMoreClinical.Location = new System.Drawing.Point(239, 0);
			this.btnMoreClinical.Name = "btnMoreClinical";
			this.btnMoreClinical.Size = new System.Drawing.Size(24, 24);
			this.btnMoreClinical.TabIndex = 0;
			this.btnMoreClinical.TabStop = false;
			this.btnMoreClinical.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MoreClinical_MouseDown);
			this.btnMoreClinical.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MoreClinical_MouseUp);
			// 
			// paramButtonImageList
			// 
			this.paramButtonImageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("paramButtonImageList.ImageStream")));
			this.paramButtonImageList.TransparentColor = System.Drawing.Color.Transparent;
			this.paramButtonImageList.Images.SetKeyName(0, "");
			this.paramButtonImageList.Images.SetKeyName(1, "");
			this.paramButtonImageList.Images.SetKeyName(2, "");
			this.paramButtonImageList.Images.SetKeyName(3, "");
			this.paramButtonImageList.Images.SetKeyName(4, "");
			this.paramButtonImageList.Images.SetKeyName(5, "");
			this.paramButtonImageList.Images.SetKeyName(6, "");
			this.paramButtonImageList.Images.SetKeyName(7, "");
			// 
			// SummarySpectrumChart1
			// 
			this.SummarySpectrumChart1.BackColor = System.Drawing.Color.Black;
			this.SummarySpectrumChart1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.SummarySpectrumChart1.Location = new System.Drawing.Point(0, 25);
			this.SummarySpectrumChart1.Name = "SummarySpectrumChart1";
			this.SummarySpectrumChart1.Size = new System.Drawing.Size(340, 95);
			this.SummarySpectrumChart1.TabIndex = 0;
			// 
			// SummaryView
			// 
			this.Controls.Add(this.SummarySpectrumChart1);
			this.Controls.Add(this.panel1);
			this.Name = "SummaryView";
			this.Size = new System.Drawing.Size(340, 120);
			this.Load += new System.EventHandler(this.SummaryView_Load);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ImageList paramButtonImageList;
        private System.Windows.Forms.ImageList probeImageList;
        private System.Windows.Forms.Label probeImage;
        private System.Windows.Forms.Label depthLabel;
        private System.Windows.Forms.Label depthValue;
        public System.Windows.Forms.Label BVLabel;
        private System.Windows.Forms.Label probeColor;
        private System.Windows.Forms.Label probeName;
        private System.Windows.Forms.Button btnMoreClinical;
        private System.Windows.Forms.Label btnRowDataStatus;
        public SummarySpectrumChart SummarySpectrumChart1;
        private System.ComponentModel.IContainer components;
    }
}