﻿using System;
using Rimed.Framework.Common;
using Rimed.TCD.DspBlock;

namespace Rimed.TCD.GUI
{
	public class AudioConverter
	{
		private const string CLASS_NAME = "AudioConverter";

		private const int		GAIN_MULTIPLIER_DEFAULT = 1;
		private const double	GAIN_MULTIPLIER_MIN = 1.0;
		private const double	GAIN_MULTIPLIER_MAX = 50.0;

		private const int ARRAY_SIZE = 31;
		private const int PRF_SAMPLE_COUNT_MULTIPLIER = 8;

		private static readonly int[] s_hilbertCo = new int[]{111, 0, 192, 0, 440, 0, 922, 0, 1752, 0, 3212, 0, 6343, 0, 20651
																, 0
																, -20651, 0, -6343, 0, -3212, 0, -1752, 0, -922, 0, -440, 0, -192, 0, -111
															};
		private static readonly int[] s_lfpCo = new int[] { -34, 56, -90, 132, -167, 164, -85, -103, 426, -884, 1450, -2073, 2678, -3186, 3526
															, 27197 
															, 3526, -3186, 2678, -2073, 1450, -884, 426, -103, -85, 164, -167, 132, -90, 56,-34
														  };

		private struct RTimeDomainValue
		{
			public short I;
			public short Q;

			public void Reset()
			{
				I = 0;
				Q = 0;
			}
		}


		private readonly	RTimeDomainValue[] m_samplesDataValue = new RTimeDomainValue[ARRAY_SIZE];
		private				RTimeDomainValue m_prevIQSample;

		private byte[]		m_waveBuffer;
		private int			m_iqSamplesByteCount;
		private int			m_wavBufferByteCount;
		private double		m_gainMultiplier;
		private int			m_prf;

		public AudioConverter(int prf, int gainMultiplier = GAIN_MULTIPLIER_DEFAULT)
		{
			GainMultiplier			= gainMultiplier;
			m_prf					= prf;
			reallocateBuffers();
		}

		public double GainMultiplier
		{
			get { return m_gainMultiplier; }
			set
			{
				if (value < GAIN_MULTIPLIER_MIN)
					value = GAIN_MULTIPLIER_MIN;
				else if (value > GAIN_MULTIPLIER_MAX)
					value = GAIN_MULTIPLIER_MAX;

				m_gainMultiplier = value;
			}
		}

		public int PRF
		{
			get { return m_prf; }
			set
			{
				if (value == m_prf)
					return;

				m_prf = value;
				reallocateBuffers();
			}
		}

		public int AudioSampleCount { get; private set; }

		public int AudioSampleRate { get; private set; }

		//public byte[] ConvertBuffer(byte[] iqBuffer)
		//{
		//    if (iqBuffer == null ||m_iqSamplesByteCount > iqBuffer.Length)
		//        return new byte[0];

		//    var readPos = 0;
		//    var writePos = 0;
		//    for (var i = 0; i < m_iqSamplesByteCount; i += 2)
		//    {
		//        var currI = BitConverter.ToInt16(iqBuffer, readPos);
		//        var currQ = BitConverter.ToInt16(iqBuffer, readPos + 2);
		//        readPos += 4;

		//        m_samplesDataValue[0].I = (short)((currI + m_prevIQSample.I) / 2);
		//        m_samplesDataValue[0].Q = (short)((currQ + m_prevIQSample.Q) / 2);
		//        writePos = processIQSamples(writePos);

		//        m_samplesDataValue[0].I = currI;
		//        m_samplesDataValue[0].Q = currQ;
		//        writePos = processIQSamples(writePos);

		//        m_prevIQSample.I = currI;
		//        m_prevIQSample.Q = currQ;
		//    }

		//    var result = new byte[m_waveBuffer.Length];
		//    Buffer.BlockCopy(m_waveBuffer,0,result,0,result.Length);
		//    return result;
		//}

		/// <summary>Convert IQ samples buffer to audio raw data</summary>
		/// <param name="buffer">Buffer containes I & Q samples [short, short]: (I0, Q0), (I1, Q1), (I2, Q2) ......(In, Qn)</param>
		/// <param name="startPos">input buffer IQ sampels start position.</param>
		/// <returns>Audio raw buffer [int, int]: (F-0.5, R-0.5), (F0, R0), (F0.5, R0.5), (F1, R1), (F1.5, R1.5), (F2, R2), (F2.5, R2.5).......(Fn, Rn)</returns>
		public byte[] ConvertIQBuffer(byte[] buffer, int startPos = 0)
		{
			if (buffer == null || m_iqSamplesByteCount + startPos > buffer.Length)
				return new byte[0];

			var prf = m_prf;
			var readPos = startPos;
			var writePos = 0;
			for (var i = 0; i < m_iqSamplesByteCount; i += 2)
			{
				var currI = BitConverter.ToInt16(buffer, readPos);
				var currQ = BitConverter.ToInt16(buffer, readPos + 2);
				readPos += 4;

				if (prf > 12 && i % 2 == 1)	//Skip every second sample for PRF > 12 (a.k.a=24) - reduce high pitch
					continue;

				// Add I/Q samples for PRF 4 and PRF 6:
				//	 PRF 4:	Two samples - (32 *3) * 3 = 288		// 32 samples per column * column count
				//	 PRF 6:	One sample  - (48 *3) * 2 = 288		// 48 samples per column * column count
				if (prf == 6)
				{
					m_samplesDataValue[0].I = (short)((currI + m_prevIQSample.I) / 2);
					m_samplesDataValue[0].Q = (short)((currQ + m_prevIQSample.Q) / 2);
					writePos = processIQSamples(writePos);
				}
				else if (prf == 4)
				{
					m_samplesDataValue[0].I = (short)((currI + m_prevIQSample.I * 2) / 3);
					m_samplesDataValue[0].Q = (short)((currQ + m_prevIQSample.Q * 2) / 3);
					writePos = processIQSamples(writePos);

					m_samplesDataValue[0].I = (short)((currI * 2 + m_prevIQSample.I) / 3);
					m_samplesDataValue[0].Q = (short)((currQ * 2 + m_prevIQSample.Q) / 3);
					writePos = processIQSamples(writePos);
				}

				m_samplesDataValue[0].I = currI;
				m_samplesDataValue[0].Q = currQ;
				writePos = processIQSamples(writePos);

				m_prevIQSample.I = currI;
				m_prevIQSample.Q = currQ;
			}

			var result = new byte[m_waveBuffer.Length];
			Buffer.BlockCopy(m_waveBuffer, 0, result, 0, result.Length);

			return result;
		}

		private void reset()
		{
			m_prevIQSample.Reset();

			for (var i = 0; i < m_samplesDataValue.Length; i++)
			{
				m_samplesDataValue[i].Reset();
			}
		}


		private void reallocateBuffers()
		{
			var iqSampleCount =		m_prf * PRF_SAMPLE_COUNT_MULTIPLIER * DspBlockConst.NUM_OF_COLUMNS;
			m_iqSamplesByteCount	= iqSampleCount * sizeof(short);

			//increase sample rate for prf 6 and 4
			var samplingMultiplier = 1;
			if (m_prf == 4)
				samplingMultiplier = 3;
			else if (m_prf == 6)
				samplingMultiplier = 2;

			AudioSampleCount		= iqSampleCount * samplingMultiplier;			//samples * (add sample between every 2 samples)
			m_wavBufferByteCount	= AudioSampleCount * sizeof(int) * 2;			//samples * sizeof(int) * 2 (Fwd, Rev) 
			AudioSampleRate			= (int)Math.Round(1000.0 * AudioSampleCount / (DspBlockConst.COLUMN_DURATION * DspBlockConst.NUM_OF_COLUMNS));
			m_waveBuffer			= new byte[m_wavBufferByteCount];

			reset();

			Logger.LogDebug("{0}.reallocateBuffers(): PRF={1}, m_iqSamplesByteCount={2}, AudioSampleCount={3}, m_wavBufferByteCount={4}, AudioSampleRate={5}"
								, CLASS_NAME, m_prf, m_iqSamplesByteCount, AudioSampleCount, m_wavBufferByteCount, AudioSampleRate);
		}

		private int processIQSamples(int pos)
		{
			var iLpf = 0;
			var qLpf = 0;

			var iHil = 0;
			var qHil = 0;

			for (var i = 0; i <= ARRAY_SIZE / 2; i++)
			{
				var coeffHil = s_hilbertCo[i];
				var coeffLpf = s_lfpCo[i];
				var item1 = m_samplesDataValue[i];
				var item2 = m_samplesDataValue[ARRAY_SIZE - 1 - i];

				iLpf += (item1.I + item2.I) * coeffLpf;
				qLpf += (item1.Q + item2.Q) * coeffLpf;

				iHil += (item1.I - item2.I) * coeffHil;
				qHil += (item1.Q - item2.Q) * coeffHil;
			}

			var fwd = clamp((iLpf + qHil) * GainMultiplier);
			var rev = clamp((iHil + qLpf) * GainMultiplier);

			m_waveBuffer[pos++] = (byte)(fwd & 0x000000FF);
			m_waveBuffer[pos++] = (byte)((fwd & 0x0000FF00) >> 8);
			m_waveBuffer[pos++] = (byte)((fwd & 0x00FF0000) >> 16);
			m_waveBuffer[pos++] = (byte)((fwd & 0xFF000000) >> 24);

			m_waveBuffer[pos++] = (byte)(rev & 0x000000FF);
			m_waveBuffer[pos++] = (byte)((rev & 0x0000FF00) >> 8);
			m_waveBuffer[pos++] = (byte)((rev & 0x00FF0000) >> 16);
			m_waveBuffer[pos++] = (byte)((rev & 0xFF000000) >> 24);

			for (var i = m_samplesDataValue.Length - 1; i > 0; i--)
			{
				m_samplesDataValue[i] = m_samplesDataValue[i - 1];
			}

			return pos;
		}

		private const int CLAMP_MIN = int.MinValue +1;
		private const int CLAMP_MAX = int.MaxValue -1;
		private int clamp(double value)
		{

			if (value > CLAMP_MAX)
				return CLAMP_MAX;

			if (value < CLAMP_MIN)
				return CLAMP_MIN;

			return (int) value;
		}
	}
}
