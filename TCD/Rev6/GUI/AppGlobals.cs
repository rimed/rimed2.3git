﻿using System;
using System.Reflection;
using Rimed.Framework.Assemblies;
using Rimed.Framework.Common;
using Rimed.TCD.DAL;

namespace Rimed.TCD.GUI
{
	public static class AppGlobals
	{
		private static GlobalTypes.ESpectrumUnits s_units;

		static AppGlobals()
		{
			var asm						= Assembly.GetExecutingAssembly();
			AppVersion					= AssemblyInformation.GetVersion(asm).ToString();
			FileVersion					= AssemblyInformation.GetFileVersionInfo(asm).FileVersion;
			AppBuildTimestamp			= AssemblyInformation.GetBuildTimestamp(asm);
			AppFormatedBuildDate		= AppBuildTimestamp.ToString("yyyy.MM.dd");
			AppFormatedBuildTime		= AppBuildTimestamp.ToString("HH:mm:ss");
			AppFormatedBuildTimestamp	= AppFormatedBuildDate + " " + AppFormatedBuildTime;

			Units						= GlobalTypes.ESpectrumUnits.CmPerSec;
		}

		private const double	UNITS_CM_PER_SEC_MULTIPLIER		= 770.0 * 100.0 / LayoutManager.FFT;
		private const double	UNITS_KHZ_MULTIPLIER			= 1.0 / (1000.0 * LayoutManager.FFT);


		public static GlobalTypes.ESpectrumUnits Units
		{
			get { return s_units; }
			set
			{
				s_units = value;
				Logger.LogInfo("System units set to: {0}", UnitsText);
			}
		}

		public static string UnitsText
		{
			get
			{
				return GetUnitsText(Units);
			}
		}
		public static string GetUnitsText(GlobalTypes.ESpectrumUnits units)
		{
			if (units == GlobalTypes.ESpectrumUnits.CmPerSec)
				return "cm/s";

			if (units == GlobalTypes.ESpectrumUnits.KHz)
				return "KHz";

			return "?";
		}

		public static double UnitsConversionFactor(double freq, double range, GlobalTypes.ESpectrumUnits units = GlobalTypes.ESpectrumUnits.NONE)
		{
			if (units == GlobalTypes.ESpectrumUnits.NONE)
				units = Units;

			if (units == GlobalTypes.ESpectrumUnits.CmPerSec) 
				range = ((2.0 * freq * range) / Probe.MULTIPLIER_FOR_CONVERSION_TO_M_HZ);

			var fs = range * 1000.0;
			var fc = freq * 1000000.0;

			double factore;
			if (units == GlobalTypes.ESpectrumUnits.CmPerSec)
			{
				if (Math.Abs(fc - 0) < 0.000001)
					return 0;

				factore = UNITS_CM_PER_SEC_MULTIPLIER*fs/fc;	//fs * 770.0 * 100.0 / (fc * LayoutManager.FFT);
			}
			else
				factore = UNITS_KHZ_MULTIPLIER*fs;				// (fs / (1000.0 * LayoutManager.FFT));

			return factore;
		}


		public static double SpectrumYMin { get; set; }
		public static double SpectrumYMax { get; set; }

		public static DateTime AppBuildTimestamp { get; private set; }
		public static string AppFormatedBuildTime { get; private set; }
		public static string AppFormatedBuildDate { get; private set; }
		public static string AppFormatedBuildTimestamp { get; private set; }
		public static string AppVersion { get; private set; }
		public static string FileVersion { get; private set; }
	}
}
