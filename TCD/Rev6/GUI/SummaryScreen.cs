using System;
using System.Collections.Generic;
using System.Data;
using Rimed.Framework.Common;
using Rimed.Framework.GUI;
using Rimed.Framework.WinAPI;
using Rimed.TCD.DAL;
using System.Diagnostics;
using System.Drawing;
using System.Collections;
using System.IO;
using System.Windows.Forms;

using Syncfusion.Windows.Forms.Tools.XPMenus;
using Rimed.TCD.ReportMgr;
using Rimed.TCD.Utils;
using Rimed.TCD.DAL;
using Constants = Rimed.TCD.Utils.Constants;

namespace Rimed.TCD.GUI
{
    /// <summary>
    /// The Summary screen is mostly used by the user after performing examinations. 
    /// From this screen user can edit the different gates (delete/move etc.). 
    /// This is also the default screen when the user loads an old test. 
    /// The user can replay the specific gate from the summary screen; 
    /// another important option is the "Patient Report" button that generates the final report of the examination. 
    /// </summary>
    public partial class SummaryScreen : Form
    {
        public const string FORM_CAPTION_PREFIX     = "Summary Screen";

        private const string FMT_SPECTRUM_WINDOW    = "{0} Spectrum window ({1})";
	    private const int TOP_IMAGE_BORDER_SIZE		= 2;

        private readonly static string s_captionPrefix = FORM_CAPTION_PREFIX;

        public static bool JustExit = false; // added by Alex 04/09/2014 fixed bug #548 ver. 2.2.0.7

        #region Private Fields

        private static bool s_isFirstLoad;

        private readonly Hashtable m_imageHash = new Hashtable();
        private Cursor m_dragCursor;

        private bool m_moreClinicalParamFlag = false;
        private SummaryView m_selectedView = null;
        private bool m_dragInProg = false;
        private bool m_isLogDragInProg = false;
        private Control m_lastControl = null;
        private Color m_lastControlColor;
        private bool m_dontImpementMouseMove = false;
        private Panel m_selectedPanel = null;
        private bool m_showLindegaardRatio;

        #endregion Private Fields

        #region Constructors
        static SummaryScreen()
        {
            Instance = new SummaryScreen();
            if (!Instance.DesignMode)
                s_captionPrefix = MainForm.StringManager.GetString(FORM_CAPTION_PREFIX);
        }

        private SummaryScreen()
        {
	        Visible = false;
            InitializeComponent();

            if (!AppSettings.Active.ApplicationTitle)
            {
                FormBorderStyle = FormBorderStyle.None;
                labelCaption.Top = 3;
                labelCaption.Left = Width - labelCaption.Width - 3;
                labelCaption.Visible = true;
                labelCaption.BringToFront();
            }

            var strRes          = MainForm.StringManager;
            menuItemSave.Text   = strRes.GetString("Save") + "&" + strRes.GetString("Examination");
            menuItemReport.Text = "&" + strRes.GetString("Report");
            menuItemPatientReport.Text = "&" + strRes.GetString("Patient Report");
            menuItemExamination.Text = "&" + strRes.GetString("Examination Summary");
            menuItemSumInsertNotes.Text = "&" + strRes.GetString("Insert Notes");
            menuItem7.Text = "&" + strRes.GetString("Send To");
            menuItemReplay.Text = "&" + strRes.GetString("Replay");
            menuItemDelete.Text = "&" + strRes.GetString("Delete Spectrum");
            parentBarItemPatient.Text = "&" + strRes.GetString("Patient");
            barItemPatientNew.Text = "&" + strRes.GetString("New") + "...";
            barItemPatientLoad.Text = "&" + strRes.GetString("Load") + "...";
            // added by Alex 24/08/2015 fixed bug #801 v 2.2.3.4
            barItemPatientWorkList.Text = "&" + strRes.GetString("Work List...") + "...";
            barItemPatientSearch.Text = "&" + strRes.GetString("Search") + "...";
            barItemPatientDelete.Text = "&" + strRes.GetString("Delete") + "...";
            parentBarItemExport.Text = strRes.GetString("Export");
            barItemFullSceen.Text = strRes.GetString("Full Screen");
            barItemExportLog.Text = strRes.GetString("Export Log File");
            barItemBackup.Text = strRes.GetString("Backup") + "...";
            barItemRestore.Text = "&" + strRes.GetString("Restore") + "...";
            barItemPrint.Text = "&" + strRes.GetString("Print") + "...";
            barItemExit.Text = strRes.GetString("Main Menu");
            barItemExitWindows.Text = strRes.GetString("Exit windows");
            parentBarItemStudies.Text = strRes.GetString("Studies");
            parentBarItemSetup.Text = strRes.GetString("Setup");
            barItemPatientReportWizard.Text = strRes.GetString("Report Generator Wizard");
            barItemSetupGeneral.Text = "&" + strRes.GetString("General") + "...";
            barItemSetupStudies.Text = "&" + strRes.GetString("Studies") + "...";
            barItemSaveStudy.Text = "&" + strRes.GetString("Save Study");
            parentBarItemFunctions.Text = strRes.GetString("Functions");
            barItemNotes.Text = strRes.GetString("Notes") + "...";
            barItemPatientReport.Text = strRes.GetString("Patient Report") + "...";
            barItemSave.Text = strRes.GetString("Save") + "...";
            barItemExpandClinicalParameters.Text = strRes.GetString("Expand Clinical Parameters");
            barItemReturn.Text = strRes.GetString("Return");

            // Added by Alex 04/04/2016 fixed CR #843 v. 2.2.3.28
            barItemAccessionNumber.Text = strRes.GetString("Accession Number");

            parentBarItemHelp.Text = strRes.GetString("Help");
            barItemHelpContext.Text = "&" + strRes.GetString("Context") + "...";
            barItemHelpIndex.Text = "&" + strRes.GetString("Index") + "...";
            barItemHelpSearch.Text = "&" + strRes.GetString("Search") + "...";
            barItemHelpWhatIsThis.Text = strRes.GetString("What is this?");
            barItemHelpAbout.Text = "&" + strRes.GetString("About") + "...";
            parentBarItem2.Text = strRes.GetString("Keyboard ShortCuts");
            barItemDepthDown.Text = strRes.GetString("Depth Down");
            barItemDepthUp.Text = strRes.GetString("Depth Up");
            barItemGainUp.Text = strRes.GetString("Gain Up");
            barItemGainDown.Text = strRes.GetString("Gain Down");
            barItem4.Text = strRes.GetString("Remote Station") + " ...";
            barItemAuthorizationMgr.Text = strRes.GetString("Authorization Manager") + "...";
            parentBarItemUtilities.Text = strRes.GetString("Utilities");
            barItemAddPrinter.Text = strRes.GetString("Add Printer") + "...";
            parentBarItem1.Text = "&" + strRes.GetString("Debug");
            barItemUnFreeze.Text = strRes.GetString("UnFreeze");
            BarItem_UpZeroLine.Text = strRes.GetString("Up Zero Line");
            barItem_DownZeroLine.Text = strRes.GetString("Down Zero Line");
            barItemPrintScreen.Text = strRes.GetString("Print Screen");
            barItemFlashPrining.Text = strRes.GetString("Flash Printing");
            barItemNextFunction.Text = strRes.GetString("Next Function");
            barItemPatientPrintPreview.Text = strRes.GetString("Print Preview") + "...";

            loadDragCursor();
            loadListImages();

            // TO DO: add the rest of the bmp of the BV [consider to use separate image list.

			toolBarPanel1.ButtonReturn.Button.Click += new EventHandler(buttonReturn_Click);
			toolBarPanel1.ButtonNewPatient.Click += new EventHandler(buttonNewPatient_Click);
			toolBarPanel1.ButtonLoad.Click += new EventHandler(buttonLoad_Click);
			toolBarPanel1.ButtonStudies.Click += new EventHandler(buttonStudies_Click);
			toolBarPanel1.ButtonClinicalParameters.Click += new EventHandler(buttonClinicalParameters_Click);
			toolBarPanel1.ButtonSaveCtrl.Button.Click += new EventHandler(ButtonSave_Click);
			toolBarPanel1.ButtonNotes.Click += new EventHandler(buttonNotes_Click);
			toolBarPanel1.ButtonDicom.Button.Click += new EventHandler(buttonDicom_Click);
			toolBarPanel1.ButtonPrint.Click += new EventHandler(buttonPrint_Click);
			toolBarPanel1.ButtonPatientRep.Click += new EventHandler(buttonPatientRep_Click);

            ReturnForReplay = false;
		}

        #endregion Constructors

        private void loadDragCursor()
        {
            var file = string.Format("{0}\\{1}", Constants.CONFIG_PATH, "CursorDragGV.cur");
            try
            {
                m_dragCursor = new Cursor(file);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, string.Format("Cursor file ('{0}') load fail.", file));
                m_dragCursor = Cursors.Hand;
            }
        }

        private void loadListImages()
        {
            loadImage("ACA-L");
            loadImage("ACA-R");

            loadImage("MCA-L");
            loadImage("MCA-R");

            loadImage("PCA-L");
            loadImage("PCA-R");

            loadImage("Opht-L");
			loadImage("Opht-R");

            loadImage("VA-L");
            loadImage("VA-R");

            loadImage("BA");

            loadImage("NoAnim", "png");

            loadImage(Constants.StudyType.STUDY_EXTRACRANIAL, "JPG");

            loadImage("Periferal", "JPG");
        }

        private void loadImage(string key, string fileExt = "gif")
        {
            var file    = string.Format(@"{0}\{1}.{2}", Constants.CONFIG_PATH, key, fileExt);
            //Logger.LogTrace(Logger.ETraceLevel.L1, Name, "Loading image file: {0}...", file);

            try
            {
                var img = Image.FromFile(file);
                m_imageHash.Add(key.ToUpper(), img);
            }
            catch (Exception ex)
            {
                Logger.LogWarning(ex);
            }
        }

        private Image getImage(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
                return null;

            key = key.ToUpper();
            if (!m_imageHash.ContainsKey(key))
                return null;

            return m_imageHash[key] as Image;
        }

        private Image NoAnimImage 
        { 
            get { return getImage("NoAnim"); } 
        }

        private Image ExtracranialImage
        {
            get { return getImage(Constants.StudyType.STUDY_EXTRACRANIAL); }
        }

        private Image PeriferalImage
        {
            get { return getImage("Periferal"); }
        }


        private static bool canRemoveSv()
        {
            var removeSV = true;
            if (MainForm.Instance.IsNotUnilateralStudy)	
            {
                //Cannot delete one side spectrum. Delete both sides' spectrum? YES, NO
                using (var dlg = new AskDlg())
                {
					dlg.TopMost = true;
	                var res = dlg.ShowDialog(Instance);
	                Instance.Focus();
					removeSV = (res == DialogResult.Yes);
                }
            }

            return removeSV;
        }


        private static SummaryScreen Instance { get; set; }

        public static ToolBarPanel ToolBar { get { return Instance.toolBarPanel1; } }

        public static void Clear()
        {
            if (Instance == null)
                return;

            Instance.clearAll();
        }

        public static void LoadExam(bool isMonitoring, bool isTemporary, bool showSummaryScreen = false)
        {
            if (Instance == null)
                return;

            Instance.loadExamination(isMonitoring, isTemporary);
            if (!isMonitoring && !isTemporary && showSummaryScreen)
                MainForm.Instance.BeginInvoke((Action)OpenDialog);
        }

        public static bool IsReturnForReplay
        {
            get
            {
                if (Instance == null)
                    return false;

                return Instance.ReturnForReplay;
            }
        }

        public static MainFrameBarManager   FrameBarManager
        {
            get
            {
                if (Instance == null)
                    return null;

                return Instance.mainFrameBarManager1;
            }
        }

        public static ParentBarItem         ParentBarStudies
        {
            get
            {
                if (Instance == null)
                    return null;

                return Instance.parentBarItemStudies;
            }
        }

	    private static void updateCaption(string caption = null)
        {
            if (Instance == null)
                return;

            if (string.IsNullOrWhiteSpace(caption))
                caption = s_captionPrefix;

            if (Instance.InvokeRequired)
                Instance.BeginInvoke((Action) delegate()
                    {
                        Instance.Text               = caption;
                        Instance.labelCaption.Text  = caption;
                        Instance.labelCaption.Left  = Instance.Width - Instance.labelCaption.Width - 3;
                    });
            else
            {
                Instance.Text               = caption;
                Instance.labelCaption.Text  = caption;
                Instance.labelCaption.Left  = Instance.Width - Instance.labelCaption.Width - 3;
            }
        }

        public static void ActivatePatientReport()
        {
            if (Instance == null || MainForm.Instance == null)
                return;

			var examRow = RimedDal.Instance.LoadedExamination();
			if (examRow == null)
				return;

            MainForm.Instance.DisplayPatientRep(MainForm.Instance.SelectedPatientIndex, examRow.Examination_Index, BVListViewCtrl.Instance.ExaminationWasSaved);
        }

        public static void SaveGateImage2HD(dsGateExamination.tb_GateExaminationRow gateRow)
        {
            if (Instance == null)
                return;

            var side = "_M";
            if (gateRow.Side == (byte)GlobalTypes.ESide.Left)
                side = "_L";
            else if (gateRow.Side == (byte)GlobalTypes.ESide.Right)
                side = "_R";

            var p = Instance.getSidePanel(side);

            var fileName = Constants.GATES_IMAGE_PATH + gateRow.Gate_Index + side + ".jpg";
            foreach (Control c in p.Controls)
            {
                var sv = c as SummaryView;
                if (sv != null && sv.GateId.Equals(gateRow.Gate_Index))
                {
                    sv.SaveReportSummaryImage(fileName, side);

                    break;
                }
            }
        }

        /// <summary>This function saves Gate View images to HD</summary>
        /// <param name="forceSaving">Used to initialize re-saving</param>
        public static void SaveImage2HD(bool forceSaving = false)	
        {
            if (Instance == null)
                return;

            var success = true;
            var errMsg = string.Empty;
            foreach (SummaryView gv in Instance.RightPanel.Controls)
            {
	            if (gv == null || !gv.IsValid)
		            continue;

                var fileName = Constants.GATES_IMAGE_PATH + gv.GateId + "_R" + ".jpg";
                if (forceSaving || !File.Exists(fileName))
                {
                    //Saved the file if the temp file exist (Want be exist if the call for the function is after manual calculation and this is not the calculated gate
                    var side = string.Empty;
					if (File.Exists(string.Format("{0}{1}_R_MC.jpg", Constants.TEMP_IMG_PATH, gv.GateId)) || File.Exists(Constants.TEMP_IMG_PATH + gv.GateId + "_R" + ".jpg"))
						side = "_R";

					if (File.Exists(Constants.TEMP_IMG_PATH + gv.GateId + "_M" + ".jpg"))
						side = "_M";

                    if (!string.IsNullOrEmpty(side))
                    {
                        // v.2.02.05.000 updated Patient report improvement (summary section) feature.
                        success = gv.SaveReportSummaryImage(fileName, side);

                        if (side == "_M")
                        {
                            Files.Delete(Constants.TEMP_IMG_PATH + gv.GateId + "_M" + ".jpg");
                            Files.Delete(Constants.GATES_IMAGE_PATH + gv.GateId + "_M" + ".jpg");
                        }
                    }
                }
            }

            if (!success)
            {
                errMsg = "Error saving RightPanel image(s).";
                success = true;
            }

            foreach (SummaryView gv in Instance.MidPanel.Controls)
            {
				if (gv == null || !gv.IsValid)
					continue;

				var fileName = Constants.GATES_IMAGE_PATH + gv.GateId + "_M" + ".jpg";
                if (forceSaving || !File.Exists(fileName))
                {
                    if (File.Exists(string.Format("{0}{1}_M_MC.jpg", Constants.TEMP_IMG_PATH, gv.GateId)) || File.Exists(Constants.TEMP_IMG_PATH + gv.GateId + "_M" + ".jpg"))
                    {
                        success = gv.SaveReportSummaryImage(fileName, "_M");
                    }
                }
            }

            if (!success)
            {
                errMsg += "\nError saving MiddlePanel image(s).";
                success = false;
            }

            foreach (SummaryView gv in Instance.LeftPanel.Controls)
            {
				if (gv == null || !gv.IsValid)
					continue;

				var fileName = Constants.GATES_IMAGE_PATH + gv.GateId + "_L" + ".jpg";
                if (forceSaving || !File.Exists(fileName))
                {
                    var side = String.Empty;
                    if (File.Exists(string.Format("{0}{1}_L_MC.jpg", Constants.TEMP_IMG_PATH, gv.GateId)) || File.Exists(Constants.TEMP_IMG_PATH + gv.GateId + "_L" + ".jpg"))
                        side = "_L";

                    if (File.Exists(Constants.TEMP_IMG_PATH + gv.GateId + "_M" + ".jpg"))
                        side = "_M";

                    if (side != string.Empty)
                    {
                        success = gv.SaveReportSummaryImage(fileName, side);

                        if (side == "_M")
                        {
                            Files.Delete(Constants.TEMP_IMG_PATH + gv.GateId + "_M" + ".jpg");
                            Files.Delete(Constants.GATES_IMAGE_PATH + gv.GateId + "_M" + ".jpg");
                        }
                    }
                }
            }
            if (!success)
            {
                errMsg += "\nError saving LeftPanel image(s).";
            }

            if (!string.IsNullOrWhiteSpace(errMsg))
				LoggedDialog.ShowWarning(errMsg);
        }

		public static void RemoveGate(dsGateExamination.tb_GateExaminationRow gateRow)
        {
            if (Instance == null || gateRow == null)
                return;

            var p = Instance.getSidePanel(gateRow.Side);

            foreach (Control c in p.Controls)
            {
                var svCtrl = c as SummaryView;
                if (svCtrl != null && svCtrl.GateId.Equals(gateRow.Gate_Index))
                {
                    Instance.removeMouseEventsHandlerRecursive(svCtrl.Controls);
                    p.Controls.Remove(svCtrl);
                    break;
                }
            }
            Instance.updateLindegaardRatio();
        }

        /// <summary>This function saves Gate View images to HD in Monitoring mode</summary>
        public static void SaveImage2HD(GateView gv, Guid eventId, string depthStr)
        {
            if (Instance == null || gv == null)
                return;

            try
            {
                var side = gv.BVSideSuffix;
                var p = Instance.getSidePanel(side);

                SummaryView currentView = null;
                foreach (var ctrl in p.Controls)
                {
                    var sumView = ctrl as SummaryView;
                    if (sumView != null && sumView.GateId.Equals(gv.GateIndex))
                    {
                        currentView = sumView;
                        break;
                    }
                }

                if (currentView != null)
                {
                    var fileName = string.Format("{0}{1}{2}{3}", Constants.GATES_IMAGE_PATH, eventId, side, ".jpg");
                    currentView.SaveReportSummaryImage(fileName, side, depthStr);
                }
            }
            catch (Exception e)
            {
                Logger.LogError(e);
            }
        }

		public static void UpdateSummaryScreenWithNewValues(dsGateExamination.tb_GateExaminationRow gateRow, ClinicalBar clinalBar = null)
		{
			if (Instance == null || gateRow == null)
				return;

			var p = Instance.getSidePanel(gateRow.Side);

			var isSummarySpectrumsLoadded = true;
			foreach (Control ctrl in p.Controls)
			{
				var svCtrl = ctrl as SummaryView;
				if (svCtrl != null && svCtrl.GateId.Equals(gateRow.Gate_Index))
					isSummarySpectrumsLoadded = isSummarySpectrumsLoadded && svCtrl.SummarySpectrumChart1.CopyFrom(gateRow, clinalBar);
			}

			if (!isSummarySpectrumsLoadded)
				LoggedDialog.ShowWarning("NOTE: Summary screen loaded with errors.");
		}

        public static void RenameGate(dsGateExamination.tb_GateExaminationRow gateRow, GlobalTypes.ESide oldSide)
        {
            if (Instance == null || gateRow == null)
                return;

            var p = Instance.getSidePanel(oldSide);
            foreach (Control ctrl in p.Controls)
            {
                var svCtrl = ctrl as SummaryView;
                if (svCtrl != null && svCtrl.GateId.Equals(gateRow.Gate_Index))
                {
                    svCtrl.GateName = gateRow.Name;

                    //If it is not in the same side delete and Add in the other Side
                    if (oldSide != (GlobalTypes.ESide)gateRow.Side)
                    {
                        // get the selected summary view.
                        p.Controls.Remove(ctrl);
                        svCtrl.Side = (GlobalTypes.ESide)gateRow.Side;
                        Instance.addGv2Panel(svCtrl);
                        gateRow.OnSummaryScreenIndex = Instance.GetNextOnSummaryScreenIndex(); // added by Alex 07/09/2014 fixed bug #548 v 2.2.0.8
                        break;
                    }
                }
            }

            Instance.updateLindegaardRatio();
        }

        public static void AddGate(dsGateExamination.tb_GateExaminationRow gateRow)
        {
            if (Instance == null || gateRow == null)
                return;

			var newSV = new SummaryView();
            newSV.CopyFrom(gateRow);
            newSV.ContextMenu = Instance.contextMenuSpectrum;
            Instance.registerMouseEventsHandlerRecursive(newSV.Controls);
            Instance.addGv2Panel(newSV);
            Instance.updateLindegaardRatio();
        }


        public static void OpenDialog()
        {
            if (Instance == null)
                return;

            HideDialog();
            try
            {
				if (Instance.Modal)
                    Instance.Visible = true;
                else
                {
                    Instance.ReturnForReplay = false;       //v2.0.5.6: Bug fix - set default value
                    Instance.ShowDialog(MainForm.Instance);
                }
            }
            catch (Exception ex)
            {
				Logger.LogError(ex);
                LoggedDialog.ShowError("Fail to open Summary screen.");
            }
        }

        public static void HideDialog()
        {
            if (Instance == null)
                return;

            Instance.Visible = false;

			if (Instance != null && MainForm.Instance.Visible)
				MainForm.Instance.Focus();
		}

        public static void CloseDialog()
        {
            if (Instance == null)
                return;

            Instance.Close();
            Instance = null;
        }



        private Panel getSidePanel(string side)
        {
            Panel p;
            if (string.Compare(side, "_L", StringComparison.InvariantCultureIgnoreCase) == 0)
                p = LeftPanel;
            else if (string.Compare(side, "_R", StringComparison.InvariantCultureIgnoreCase) == 0)
                p = RightPanel;
            else
                p = MidPanel;

            return p;
        }

        private Panel getSidePanel(byte side)
        {
            return getSidePanel((GlobalTypes.ESide)side);
        }

        private Panel getSidePanel(GlobalTypes.ESide side)
        {
            Panel p;

            if (side == GlobalTypes.ESide.Left)
                p = Instance.LeftPanel;
            else if (side == GlobalTypes.ESide.Right)
                p = Instance.RightPanel;
            else
                p = Instance.MidPanel;

            return p;
        }

        private void buttonNewPatient_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("buttonNewPatient", Name);
			MainForm.Instance.OpenNewPatientDialog();
        }

        private void buttonLoad_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("buttonLoad", Name);
			MainForm.Instance.OpenLoadDialog();
        }

        // added by Alex 24/08/2015 fixed bug #801 v 2.2.3.4
        private void buttonWorkList_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("buttonWorkList", Name);
            MainForm.Instance.OpenWorkListDialog();
        }

        private void buttonPrint_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("buttonPrint", Name);
            generateExamReport(false);
        }

        private void buttonStudies_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("buttonStudies", Name);
            MainForm.Instance.LoadStudy();
        }

        private void buttonNotes_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("buttonNotes", Name);
            String notes = BVListViewCtrl.Instance.ExamNotes;
            BVListViewCtrl.Instance.ExamNotes = MainForm.Instance.DoNotes(GlobalTypes.ENotesType.ExamNotes, notes);
        }

		private void buttonDicom_Click(object sender, EventArgs e)
        {
			LoggerUserActions.MouseClick("buttonDicom", Name);
            // Changed by Alex 04/04/2016 fixed bug #843 v. 2.2.3.28
            if (!MainForm.Instance.CanSendToDicom())
            {
                LoggedDialog.Show(this, "Please save the examination before sending it to a DICOM server.", "DICOM", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

			toolBarPanel1.DisableDicomForPeriod();

			Image img = HelperMethods.CaptureControlSnapshot(this);
			if (img == null)
			{
				LoggedDialog.ShowNotification(this, "Full screen image dump fail.");
				return;
			}

			using (img)
				PacsManager.SendImageToPacsServer(img);
        }

		private void ButtonSave_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("ButtonSave", Name);
            saveExamination();
        }

        private void saveExamination()
        {
            var res = BVListViewCtrl.Instance.Save(true);
            if (res) //RIMD-325: Disable Save button after first click
            {
	            disableSaveButtons(true);
				MainForm.Instance.WorkingMode = GlobalTypes.EWorkingMode.LoadMode;
			}
        }

        private void disposeChildControls(Control panel)
        {
            if (panel == null)
                return;

	        var removeLst = new List<SummaryView>();

			foreach (var ctrl in panel.Controls)
			{
				var sv = ctrl as SummaryView;
				if (sv == null || sv.IsDummy)
					continue;

				removeLst.Add(sv);
			}

	        foreach (var summaryView in removeLst)
	        {
				panel.Controls.Remove(summaryView);
		        summaryView.Dispose();
	        }
        }

        private void buttonClinicalParameters_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("buttonClinicalParameters", Name);

            var sizeMode = m_moreClinicalParamFlag ? GlobalTypes.ESizeMode.Small : GlobalTypes.ESizeMode.Large;

            LoggerUserActions.MouseClick(string.Format("buttonClinicalParameters(size={0})", sizeMode), Name);

            foreach (SummaryView gv in RightPanel.Controls)
            {
                gv.MoreClinicalParam(sizeMode);
            }

            foreach (SummaryView gv in MidPanel.Controls)
            {
                gv.MoreClinicalParam(sizeMode);
            }

            foreach (SummaryView gv in LeftPanel.Controls)
            {
                gv.MoreClinicalParam(sizeMode);
            }

            m_moreClinicalParamFlag = !m_moreClinicalParamFlag;
        }
        
        private void buttonReturn_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("buttonReturn", Name);
			BVListViewCtrl.Instance.SelectTopBV();

            JustExit = true;// added by Alex 04/09/2014 fixed bug #548 ver. 2.2.0.7
			
            exitDialog(false);
        }

        // Added by Alex 04/04/2016 fixed CR #843 v. 2.2.3.28
        private void buttonAccessionNumber_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("buttonAccessionNumber", Name);
            MainForm.Instance.EditAccessionNumber();
        }
        
        private void SummaryScreen_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible)
            {
				Location	= MainForm.Instance.Location;
				Size		= MainForm.Instance.Size;
				MaximumSize = Size;
				MinimumSize = Size;

				updatePicture();
                toolBarPanel1.EnableButtons();
            }
        }

        private void SummaryScreen_Load(object sender, EventArgs e)
        {
			Cursor = Cursors.WaitCursor;
			
			Visible = false;

            toolBarPanel1.ComboBoxSensitivity.Visible = false;
            toolBarPanel1.ComboBoxSensitivitySecondary.Visible = false;

            SetDesktopLocation(0, 0);
            WindowState = FormWindowState.Normal;

	        Location	= MainForm.Instance.Location;
	        Size		= MainForm.Instance.Size;

			resetPanelsSize();
			m_isPanelsResizeing = false;

            toolBarPanel1.ShowToolbar(GlobalTypes.ETooolBarType.TB3_Summary);
            toolBarPanel1.ComboBoxTime.Visible = false;
            toolBarPanel1.autoLabel1.Visible = false;
            toolBarPanel1.ComboBoxProbes.Visible = false;


            if (MainForm.Instance != null)
            {
	            var name = MainForm.Instance.Text.Replace("Unilateral", "Bilateral"); //CR #421
				updateCaption(name);
            }
            else
            {
	            updateCaption(s_captionPrefix);
            }

            if (!s_isFirstLoad)
            {
                registerKeyEventsHandlerRecursive(Controls);
                s_isFirstLoad = true;
            }
            //RIMD-325: Disable Save button after first click
            disableSaveButtons(BVListViewCtrl.Instance.ExaminationWasSaved);

			addDummyViewes();

			//if (AppSettings.Active.IsDspOffMode)
			//    setReadOnlyMode();
			if (AppSettings.Active.IsReviewStation)		//Alex, v2.2.0.1 Bug# 525
			{
				barItemSetupStudies.Enabled = false;
			}

            m_showLindegaardRatio = RimedDal.Instance.IsIntracranialUnilateralStudy(MainForm.Instance.CurrentStudyId);


			Visible = true;

			Cursor = Cursors.Default;
		}

	    private void setReadOnlyMode()
	    {
		    menuItemSave.Visible = false;
			menuItemDelete.Visible = false;
			menuItemSumInsertNotes.Visible = false;

			parentBarItemStudies.Visible = false;
			parentBarItemSetup.Visible = false;
			parentBarItemFunctions.Visible = false;
			parentBarItemUtilities.Visible = false;

			barItemPatientNew.Visible = false;
			barItemPatientDelete.Visible = false;
			barItemPatientReportWizard.Visible = false;
			barItemSetupGeneral.Visible = false;
			barItemSetupStudies.Visible = false;
			barItemRestore.Visible = false;
			barItemBackup.Visible = false;
			barItemSaveStudy.Visible = false;
			barItemSave.Visible = false;
			SaveDebugData.Visible = false;
			barItemDuplicateBV.Visible = false;
			barItemAddPrinter.Visible = false;
	    }

	    //RIMD-417: Save button and menu items aren't disabled when we load Study
        //Function enables/disables all Save items on the Summary Screen
        private delegate void DisableSaveButtonsDelegate(bool examinationWasSaved);
        private void disableSaveButtons(bool examinationWasSaved)
        {
            if (InvokeRequired)
            {
                BeginInvoke((DisableSaveButtonsDelegate)disableSaveButtons, examinationWasSaved);
                return;
            }
			
			toolBarPanel1.ButtonSaveCtrl.Enabled	= !examinationWasSaved;
			menuItemSave.Enabled					= !examinationWasSaved;
			barItemSave.Enabled						= !examinationWasSaved;
			toolBarPanel1.ButtonReturn.Enabled		= !examinationWasSaved;
            // Added by Alex 04/04/2016 fixed CR #843 v. 2.2.3.28
            barItemAccessionNumber.Visible = examinationWasSaved;


			// OFER: Enable diagnostic post save edit
			var b = !examinationWasSaved || LayoutManager.IsPostSaveEditEnabled;
			barItemReturn.Enabled					= b;
			LayoutManager.Instance.PicManualCalculationsEnabled = b;
        }

		private bool m_isPanelsResizeing = false;
		private void resetPanelsSize()
		{
			if (m_isPanelsResizeing)
				return;

			m_isPanelsResizeing = true;

			RightPanel.Width = Width / 3;
			LeftPanel.Width = Width / 3;
			MidPanel.Height = (int)(RightPanel.Height * 0.4);

			var height = (int)(RightPanel.Height * 0.6);
			if (RightPanel.Controls.Count > 0)
				height = RightPanel.Controls[0].Height * 3;

			TopPanel.Height = height;	

			m_isPanelsResizeing = false;
		}

        private void moveGateInPanel(SummaryView src, SummaryView target, Panel panel)
        {
            if (src == null || target == null || panel == null)
                return;

            SuspendLayout();
            panel.SuspendLayout();

            var p = panel.AutoScrollPosition;
            panel.AutoScroll = false;
            var targetIdx = panel.Controls.IndexOf(target);
            panel.Controls.SetChildIndex(src, targetIdx);
            panel.AutoScroll = true;
            panel.AutoScrollPosition = new Point(Math.Abs(p.X), Math.Abs(p.Y));
            
            panel.ResumeLayout();
            ResumeLayout();

            RimedDal.Instance.UpdateGateExaminationsDisplayOrder(src.GateId, target.GateId);
        }

        private void SummaryScreen_MouseUp(object sender, MouseEventArgs e)
        {
            m_dragInProg = false;

            if (m_isLogDragInProg)
            {
                m_isLogDragInProg = false;
                LoggerUserActions.MouseDrag(string.Format(FMT_SPECTRUM_WINDOW, m_selectedView.GateName, m_selectedView.GateId), Name, false);
            }

            Cursor.Current = Cursors.Arrow;
			
			if (m_lastControl == m_selectedView || MainForm.Instance.StudyType == GlobalTypes.EStudyType.Bilateral)
			    return;

			// OFER: Enable diagnostic post save edit
			if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.DiagnosticLoad && !LayoutManager.IsPostSaveEditEnabled)
				return;

            if (m_lastControl != null)
            {
                moveGateInPanel(m_selectedView, m_lastControl as SummaryView, m_selectedPanel);
                m_lastControl.BackColor = m_lastControlColor;
                m_lastControl = null;
            }
        }

        private void SummaryScreen_MouseMove(object sender, MouseEventArgs e)
        {
            if (m_dontImpementMouseMove)
            {
                m_dontImpementMouseMove = false;
                return;
            }

            if (!m_dragInProg) 
                return;

            var senderControl = sender as Control;
            if (senderControl == null)
                return;

            if (!m_isLogDragInProg)
            {
                LoggerUserActions.MouseDrag(string.Format(FMT_SPECTRUM_WINDOW, m_selectedView.GateName, m_selectedView.GateId), Name, true);
                m_isLogDragInProg = true;
            }

            var screenPt = senderControl.PointToScreen(new Point(e.X, e.Y));
            while (!(senderControl is SummaryView))
            {
                senderControl = senderControl.Parent;
            }

            if (m_lastControl != null && m_lastControl != m_selectedView)
                m_lastControl.BackColor = GlobalSettings.P2;

            var ctrl = m_selectedPanel.GetChildAtPoint(m_selectedPanel.PointToClient(screenPt));
            if (ctrl != null)
            {
                m_lastControlColor = ctrl.BackColor;
                Cursor.Current = m_dragCursor;
                if (ctrl != m_selectedView)
                    ctrl.BackColor = GlobalSettings.PB;
            }
            else
            {
                Cursor.Current = Cursors.No;
            }

            m_lastControl = ctrl;
        }

        private void SummaryScreen_MouseDown(object sender, MouseEventArgs e)
        {
            var senderControl = sender as Control;
            if (senderControl == null)
                return;

            while (!(senderControl is SummaryView))
            {
                senderControl = senderControl.Parent;
            }

            foreach (SummaryView sv in senderControl.Parent.Controls)
            {
                sv.BackColor = GlobalSettings.P2;
            }

            if (m_selectedView != null)
                m_selectedView.UnMarkSelect();

            m_selectedView = senderControl as SummaryView;

            LoggerUserActions.SelectSpectrum(m_selectedView.BVLabel.Text);

            m_selectedView.MarkSelect();
            Control c = m_selectedView;
            while (!(c is Panel))
                c = c.Parent;

            if (c != null)
                m_selectedPanel = c as Panel;
            else
                m_selectedPanel = null;

            //if (m_imageHash.ContainsKey(m_selectedView.BVLabel.Text))

            // Changed by Alex 31/01/2016 fixed bug #607 v 2.2.3.21
            Image image = getImage(m_selectedView.BVLabel.Text);
            if (image == null)
                updatePicture();
            else
                setTopPanelImage(getImage(m_selectedView.BVLabel.Text));

            if (e.Clicks > 1)
            {
                LoggerUserActions.MouseDoubleClick(m_selectedView.BVLabel.Text + " spectrum", Name);
                doReplay();
            }
            else
            {
                m_dragInProg = true;
                //LoggerUserActions.MouseDrag(m_selectedView.BVLabel.Text + " spectrum", Name, true);
            }
        }

        /// <summary>For shift+Esc exit</summary>
        private void registerKeyEventsHandlerRecursive(Control.ControlCollection controls)
        {
            foreach (Control c in controls)
            {
                c.KeyDown += new KeyEventHandler(SummaryScreen_KeyDown);
                registerKeyEventsHandlerRecursive(c.Controls);
            }
        }

        private void registerMouseEventsHandlerRecursive(Control.ControlCollection controls)
        {
            foreach (Control c in controls)
            {
                c.MouseDown += new MouseEventHandler(this.SummaryScreen_MouseDown);
                c.MouseMove += new MouseEventHandler(this.SummaryScreen_MouseMove);
                c.MouseUp += new MouseEventHandler(this.SummaryScreen_MouseUp);
                registerMouseEventsHandlerRecursive(c.Controls);
            }
        }

        private void removeMouseEventsHandlerRecursive(Control.ControlCollection controls)
        {
            foreach (Control c in controls)
            {
                c.MouseDown -= new MouseEventHandler(this.SummaryScreen_MouseDown);
                c.MouseMove -= new MouseEventHandler(this.SummaryScreen_MouseMove);
                c.MouseUp -= new MouseEventHandler(this.SummaryScreen_MouseUp);
                removeMouseEventsHandlerRecursive(c.Controls);
            }
        }

        private delegate void AddGv2PanelDelegate(SummaryView sv);
        private void addGv2Panel(SummaryView sv)
        {
            if (InvokeRequired)
            {
                BeginInvoke((AddGv2PanelDelegate)addGv2Panel, sv);
                return;
            }

            sv.Dock = DockStyle.Top;
            Panel p;

            if (sv.Side == GlobalTypes.ESide.Left)
            {
                p = LeftPanel;
                sv.Top = p.Controls.Count * p.Height / GlobalSettings.VIEWS_IN_SUMMARY_SECONDARY;
                sv.Left = 0;
                sv.Height = /*p.Height*/676 / GlobalSettings.VIEWS_IN_SUMMARY_SECONDARY;
            }
            else if (sv.Side == GlobalTypes.ESide.Right)
            {
                p = RightPanel;
                sv.Top = p.Controls.Count * p.Height / GlobalSettings.VIEWS_IN_SUMMARY_SECONDARY;
                sv.Left = 0;
                sv.Height = /*p.Height*/676 / GlobalSettings.VIEWS_IN_SUMMARY_SECONDARY;
            }
            else
            {
                p = MidPanel;
                sv.Top = p.Controls.Count * p.Height / 2;
                sv.Left = 0;
                sv.Height = /*p.Height*/271 / 2;
            }

			sv.BorderStyle = BorderStyle.FixedSingle;				//2013.10.13 Ofer
			p.Controls.Add(sv);
			p.Controls.SetChildIndex(sv, 0);
        }

		private List<SummaryView> getPanelDummyViewes(GlobalTypes.ESide side)
		{
			var panel = gePanel(side);
			var dummyViewes = new List<SummaryView>();
			foreach (var obj in panel.Controls)
			{
				var sv = obj as SummaryView;
				if (sv == null)
					continue;

				if (sv.IsDummy)
					dummyViewes.Add(sv);
			}

			return dummyViewes;
		}

		private void addPanelDummyViews(GlobalTypes.ESide side, int maxDummyViews)
		{
			var panel = gePanel(side);

			var dummyViewes = getPanelDummyViewes(side);
			foreach (var sv in dummyViewes)
			{
				panel.Controls.Remove(sv);
				m_dummySVs.Enqueue(sv);
			}

			for (var i = panel.Controls.Count; i < maxDummyViews; i++)
			{
				var sv		= (m_dummySVs.Count > 0) ? m_dummySVs.Dequeue() : new SummaryView();
				sv.Side		= side;
				sv.IsDummy	= true;
				
				addGv2Panel(sv);
			}
		}

		private readonly Queue<SummaryView> m_dummySVs = new Queue<SummaryView>(); 

		private void addDummyViewes()
		{
			addPanelDummyViews(GlobalTypes.ESide.Right, RIGHT_PANEL_MIN_VIEWES);
			addPanelDummyViews(GlobalTypes.ESide.Left, LEFT_PANEL_MIN_VIEWES);
			addPanelDummyViews(GlobalTypes.ESide.Middle, MID_PANEL_MIN_VIEWES);
		}

		private Panel gePanel(GlobalTypes.ESide side)
		{
			if (side == GlobalTypes.ESide.Left)
				return LeftPanel;

			if (side == GlobalTypes.ESide.Right)
				return RightPanel;

			return MidPanel;
		}

        private void menuItemSave_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("menuItemSave", Name);
            saveExamination();
        }

        private void menuItemPatientReport_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("menuItemPatientReport", Name);
            ActivatePatientReport();
        }

        private void menuItemExamination_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("menuItemExamination", Name);

            generateExamReport(true);
        }

        private void menuItemDelete_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("menuItemDelete", Name);

			// OFER: Enable diagnostic post save edit
			if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.DiagnosticLoad)
			{
				if (LoggedDialog.ShowQuestion(this, "Are you sure you want to delete this gate?") != DialogResult.Yes)
					return;
			}

			if (BVListViewCtrl.Instance.Count <= 1)
			{
				LoggedDialog.ShowNotification(this, "Study last blood vessel cannot be removed.");
				return;
			}

            if (isCanDeleteFromSummaryScreen(m_selectedView.SubExamIndex, m_selectedView.GateName) && canRemoveSv())
            {
				using (new CursorSafeChange(this))
				{
					BVListViewCtrl.Instance.DeleteFromSummaryScreen(m_selectedView.SubExamIndex);
					//if bilateral the other gate is deleted from summary screen in procedure: BilateralRemoveSV
                    if (MainForm.Instance.IsUnilateralStudy)
                    {
                        removeSv(m_selectedView);
                        Instance.updateLindegaardRatio();
                    }
                    else
                        bilateralRemoveSv(m_selectedView);
				}
            }
        }


        private void removeSv(SummaryView sv)
        {
            Panel p;
            var subExaminationIndex = sv.SubExamIndex;
            var bvName              = sv.GateName;

            if (sv.Side == GlobalTypes.ESide.Left)
                p = LeftPanel;
            else if (sv.Side == GlobalTypes.ESide.Right)
                p = RightPanel;
            else
                p = MidPanel;

            var removeCtrls = new List<Control>();
            for (var i = p.Controls.Count - 1; i >= 0; i--)
            {
                var svCtrl = p.Controls[i] as SummaryView;
                if (svCtrl != null && svCtrl.SubExamIndex == subExaminationIndex && svCtrl.GateName == bvName)
                    removeCtrls.Add(svCtrl);  
            }

            foreach (var removeCtrl in removeCtrls)
            {
                p.Controls.Remove(removeCtrl);
            }
        }

        private void bilateralRemoveSv(SummaryView sv)
        {
            Panel p, o;
            var subExaminationIndex = sv.SubExamIndex;
            var bvName = sv.GateName;

            if (sv.Side == GlobalTypes.ESide.Left)
            {
                p = LeftPanel;
                o = RightPanel;
            }
            else
            {
                p = RightPanel;
                o = LeftPanel;
            }

            for (var i = p.Controls.Count - 1; i >= 0; i--)
            {
                var svCtrl = p.Controls[i] as SummaryView;
                if (svCtrl != null && svCtrl.SubExamIndex == subExaminationIndex && svCtrl.GateName == bvName)
                {
                    p.Controls.Remove(svCtrl);
                    svCtrl = o.Controls[i] as SummaryView;
                    if (svCtrl != null)
                        o.Controls.Remove(svCtrl);
                    break;
                }
            }
        }

        private void contextMenuPicture_Popup(object sender, EventArgs e)
        {
            foreach (MenuItem mi in contextMenuPicture.MenuItems)
                mi.Visible = (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Offline);
        }

        private void exitDialog(bool forReplay)
        {
			//if this line is missing the second time you load a patient the summary screen hung on WindowsFormsParkingWindow.
            ReturnForReplay = forReplay;
            toolBarPanel1.Focus();

            //NOTE: Form used as a singleton thus can NOT be closed until application is terminated. 
            HideDialog();
        }
		
        private void menuItemReplay_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("menuItemReplay", Name);
            doReplay();
        }

        private void doReplay()
        {
            LayoutManager.Instance.ReplayMode = true;
            BVListViewCtrl.Instance.DoubleLists.FirstList.SelectedItems.Clear();

            //RIMD-531: BV list is accessible in just saved Diagnostic examination
			BVListViewCtrl.Instance.UpdateBVList(Guid.Empty, BVListViewCtrl.Instance.ExaminationWasSaved);
			BVListViewCtrl.Instance.SelectBVbyIndex(m_selectedView.SubExamIndex);

            exitDialog(true);
        }

        private void barItemBackup_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemBackup", Name);
			MainForm.Instance.OpenBackupDialog(this);
        }

        private void barItemRestore_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemRestore", Name);
			MainForm.Instance.OpenRestoreDialog(this);
        }

        private void barItemPatientPrintPreview_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemPatientPrintPreview", Name);
            generateExamReport(true);
        }

        private void barItemPrint_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemPrint", Name);
            generateExamReport(false);
        }

        private void barItemSetupGeneral_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemSetupGeneral", Name);
			MainForm.Instance.OpenSetupDialog(this);
        }

        private void barItemPatientReportWizard_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemPatientReportWizard", Name);
			MainForm.Instance.OpenPatientReportWizard(this);
        }

        private void barItemSetupStudies_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemSetupStudies", Name);
			MainForm.Instance.OpenSetupStudiesDialog(this);
        }

        private void barItemHelpAbout_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemHelpAbout", Name);
			MainForm.Instance.OpenHelpAboutDialog(this);
        }

        private void barItemPatientSearch_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemPatientSearch", Name);
			MainForm.Instance.OpenPatientSearchDialog(this);
        }

		private void barItemPatientDelete_Click(object sender, EventArgs e)
        {
			LoggerUserActions.MouseClick("barItemPatientDelete", Name);
			MainForm.Instance.OpenDeletePatientDeleteDlg(this);
        }
		
        private void generateExamReport(bool preview)
        {
			var examRow = RimedDal.Instance.LoadedExamination();
			if (examRow == null)
			{
				Logger.LogInfo("No examination selected. Report generation aborted.");
				return;
			}

            var dvPatient       = new DataView(RimedDal.Instance.PatientsDS.tb_Patient, "Patient_Index = '" + MainForm.Instance.SelectedPatientIndex + "'", null, DataViewRowState.CurrentRows);
            var dvHospital      = new DataView(RimedDal.Instance.ConfigDS.tb_HospitalDetails);
            var dsGateExam      = new dsGateExamination();
			var gateExamRows	= RimedDal.Instance.GateExaminationsDS.tb_GateExamination.Select("InSummaryScreen = TRUE", "OnSummaryScreenIndex ASC");	//Fix bug #152
			dsGateExam.Merge(gateExamRows);																												//Fix bug #152

			var studyName = RimedDal.Instance.GetStudyName(examRow.Study_Index);	//= RimedDal.Instance.GetStudyName((Guid)RimedDal.Instance.ExaminationsDS.tb_Examination.Rows[0]["Study_Index"]);

            //Assaf - 21/11/06 : Creating all of the object for the SummaryScreen Part in the report Without Displaying it
            MainForm.Instance.DisplayPatientReport = false;
            ActivatePatientReport();
            MainForm.Instance.DisplayPatientReport = true;

			BaseReport report = new SummaryExamReport(studyName, examRow.Examination_Index, dvHospital, dvPatient, RimedDal.Instance.ExaminationsDS, dsGateExam);	//= new SummaryExamReport(studyName, (Guid)RimedDal.Instance.ExaminationsDS.tb_Examination.Rows[0]["Examination_Index"], dvHospital, dvPatient, RimedDal.Instance.ExaminationsDS, dsGateExam);

			report.Display(preview);
        }

        private void buttonPatientRep_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("buttonPatientRep", Name);
            ActivatePatientReport();
        }

        private void barItemFullSceen_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemFullSceen", Name);
            // Changed by Alex bug #794 v.2.2.3.21 31/01/2016
			MainForm.Instance.DumpFullScreenImage(true);
        }

        private void barItemExitWindows_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemExitWindows", Name);

             MainForm.Instance.ExitWindow();
        }

        private void barItemExit_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemExit", Name);
            if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Online) 
                return;

            MainForm.Instance.ExitDigiLite(true);
        }

        private void barItemPatientReport_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemPatientReport", Name);
            ActivatePatientReport();
        }

        private void menuItemSumInsertNotes_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("menuItemSumInsertNotes", Name);
            var notes = BVListViewCtrl.Instance.ExamNotes;
            BVListViewCtrl.Instance.ExamNotes = MainForm.Instance.DoNotes(GlobalTypes.ENotesType.ExamNotes, notes);
        }

        private void contextMenuSpectrum_Popup(object sender, EventArgs e)
        {
            //Assaf (03/12/06) - Hiding the Delete Option from the popup spectrum menu at replay mode
			// OFER: Enable diagnostic post save edit
			menuItemDelete.Visible = LayoutManager.SystemLayoutMode != GlobalTypes.ESystemLayoutMode.DiagnosticLoad || LayoutManager.IsPostSaveEditEnabled;

            //Assaf (12/06/07) to prevent considering opening the popup menu as a mouse move (which causes moving of spectrum  latter on)
            m_dontImpementMouseMove = true;
        }

        private void SummaryScreen_KeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = true;
            if (e.KeyCode == Keys.Escape && e.Shift && LayoutManager.SystemMode != GlobalTypes.ESystemModes.Online) 
                MainForm.Instance.ExitDigiLite(true);
        }

        private void barItemExportLog_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemExportLog", Name);

            var explorer = new Process();
            explorer.StartInfo.FileName = "explorer";
            explorer.StartInfo.Arguments = @"/e, C:\Rimed\SRC\TCD2003\GUI\Logs";
            explorer.Start();
        }

        private void barItemSave_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("barItemSave", Name);
            saveExamination();
        }


        protected override void WndProc(ref Message m)
        {
			if (m.Msg == User32.WinMessage.WM_NCHITTEST)
				return;

			// This code was added to make Main Form fixed in Aero Themes of Windows 7
			if ((m.Msg == User32.WinMessage.WM_NCLBUTTONDOWN || m.Msg == User32.WinMessage.WM_NCLBUTTONDBLCLK) && m.WParam == (IntPtr)User32.WinMessage.WMParam.HTCAPTION)
				return;

            try	
            {
                base.WndProc(ref m);
            }
            catch (Exception e)
            {
                var ex = new ApplicationException(string.Format("{0}.WndProc({1})", Name, m), e);
                Logger.LogError(ex);
            }
        }


        private bool ReturnForReplay { get; set; }

        private void updatePicture()
        {
            if (InvokeRequired)
            {
                BeginInvoke((Action)updatePicture);
                return;
            }

            switch (MainForm.Instance.CurrentStudyName)
            {
				case Constants.StudyType.STUDY_INTERCRANIAL_BILATERAL:
				case Constants.StudyType.STUDY_INTRAOPERATIVE:
                case Constants.StudyType.STUDY_INTERCRANIAL_UNILATERAL:
					setTopPanelImage(NoAnimImage);
                    break;
                case Constants.StudyType.STUDY_EXTRACRANIAL:
					setTopPanelImage(ExtracranialImage);
                    break;
                case Constants.StudyType.STUDY_PERIPHERAL:
                    setTopPanelImage(PeriferalImage);
                    break;
                default:
                    if (MainForm.Instance.CurrentStudyName.IndexOf(Constants.StudyType.STUDY_EXTRACRANIAL, StringComparison.InvariantCultureIgnoreCase) > -1)
                        setTopPanelImage(ExtracranialImage);
                    else if (MainForm.Instance.CurrentStudyName.IndexOf(Constants.StudyType.STUDY_PERIPHERAL, StringComparison.InvariantCultureIgnoreCase) > -1)
                        setTopPanelImage(PeriferalImage);
                    else
                        setTopPanelImage(NoAnimImage);
                    break;
            }
        }

		private void setTopPanelImage(Image image)
		{
			if (image == null)
				image = NoAnimImage;

			//If image is null set empty image
			if (image == null)					
				image = new Bitmap(200, 200);

			var imgRatio = (double)image.Height / image.Width;
			var parentSize	= pictureBox1.Parent.ClientSize;

			var left	= TOP_IMAGE_BORDER_SIZE;
			var width	= parentSize.Width - TOP_IMAGE_BORDER_SIZE * 2;
			var height	= (int)(width * imgRatio);
			var top		= (parentSize.Height - height) / 2;

			if (height > parentSize.Height)
			{
				top		= TOP_IMAGE_BORDER_SIZE;
				height	= parentSize.Height - TOP_IMAGE_BORDER_SIZE * 2;
				width	= (int)(height / imgRatio);
				left	= (parentSize.Width - width) / 2;
			}

			pictureBox1.SuspendLayout();
			pictureBox1.Image	= image;
			pictureBox1.Top		= top;
			pictureBox1.Left	= left;
			pictureBox1.Width	= width;
			pictureBox1.Height	= height;
			pictureBox1.ResumeLayout(true);

            if (m_showLindegaardRatio)
            {
                // Lindegaarg Ratio inside picture
                LRleftLabel.SuspendLayout();
                LRleftLabel.Top = AppSettings.Active.ApplicatioWidth > Constants.DIGI_LITE_SCREEN_WIDTH ? 
                    (int)Math.Round(0.125 * height): (int)Math.Round(0.1 * height);
                LRleftLabel.Left = TOP_IMAGE_BORDER_SIZE;
                LRleftLabel.ResumeLayout(true);
                LRleftLabel.BringToFront();
                LRrightLabel.SuspendLayout();
                LRrightLabel.Top = LRleftLabel.Top;
                LRrightLabel.Left = pictureBox1.Parent.ClientSize.Width - LRrightLabel.Width - TOP_IMAGE_BORDER_SIZE;
                LRrightLabel.ResumeLayout(true);
                LRrightLabel.BringToFront();
            }
		}

        private void loadExamination(bool isMonitoring, bool isTemporary)
        {
            // in case the user activates the new study dialog box from the summary screen -> close the summary screen. add gates to summary screen.
            clearAll();

            //RIMD-286: Incorrect order of BVs in Report after Save (sorting by OmSummaryScreenIndex for Summary Screen)
            RimedDal.Instance.GateExaminationsDS.tb_GateExamination.DefaultView.Sort = "OnSummaryScreenIndex ASC";
            foreach (DataRowView view in RimedDal.Instance.GateExaminationsDS.tb_GateExamination.DefaultView)
            {
                var gateRow = view.Row as dsGateExamination.tb_GateExaminationRow;
                if (gateRow != null && gateRow.InSummaryScreen)
                    AddGate(gateRow);
            }

            if (!isMonitoring)
            {
                updatePicture();
                updateCaption();
                disableSaveButtons(!isTemporary);
            }
        }

        private void clearAll()
        {
            if (InvokeRequired)
            {
                BeginInvoke((Action)clearAll);
                return;
            }

            HideDialog();

            //TODO : add question to save unsaved data
            disposeChildControls(LeftPanel);
            disposeChildControls(RightPanel);
            disposeChildControls(MidPanel);

            LRrightLabel.Visible = LRleftLabel.Visible = false;

            m_selectedView = null;
        }

        private bool isCanDeleteFromSummaryScreen(string subExaminationIndex, string bvName)
        {
            var filter = string.Format("SubExaminationIndex = '{0}' AND Name = '{1}'", subExaminationIndex, bvName);
            var rows = RimedDal.Instance.GateExaminationsDS.tb_GateExamination.Select(filter);
            var result = true;

            if (rows.Length == 0)
                result = false;
            else if (rows.Length > 1)
            {
                var strRes = MainForm.StringManager;
                var title = string.Format(strRes.GetString("DeleteMultipleSpectrumsTitle"), rows.Length);
                var msg = string.Format(strRes.GetString("DeleteMultipleSpectrumsMessage"), rows.Length);

				if (LoggedDialog.Show(this, msg, title, MessageBoxButtons.YesNo) == DialogResult.No)
                    result = false;
            }

            return result;
        }


        private int GetNextOnSummaryScreenIndex() // added by Alex 07/09/2014 fixed bug #548 v 2.2.0.8
        {
            int maxInd = 0;
            foreach (DataRowView view in RimedDal.Instance.GateExaminationsDS.tb_GateExamination.DefaultView)
            {
                var gateRow = view.Row as dsGateExamination.tb_GateExaminationRow;
                if (gateRow != null && maxInd < gateRow.OnSummaryScreenIndex)
                    maxInd = gateRow.OnSummaryScreenIndex;
            }
            return maxInd + 1;
        }

        public static void UpdateLindegaardRatio()
        {
            if (Instance == null)
                return;

            Instance.updateLindegaardRatio();
        }

        public void updateLindegaardRatio()
        {

            double leftRatio, rightRatio;
            if (!RimedDal.Instance.GetLindegaardRatio(out leftRatio, out rightRatio))
            {
                LRleftLabel.Visible = LRrightLabel.Visible = false;
                return;
            }

            if (leftRatio != 0)
            {
                LRleftLabel.Text = string.Format("{0}: {1:N1}", MainForm.StringManager.GetString("Lindegaard Ratio"), leftRatio);
                LRleftLabel.Visible = true;

            }
            else
                LRleftLabel.Visible = false;

            if (rightRatio != 0)
            {
                LRrightLabel.Text = string.Format("{0}: {1:N1}", MainForm.StringManager.GetString("Lindegaard Ratio"), rightRatio);
                LRrightLabel.Visible = true;
            }
            else
                LRrightLabel.Visible = false;

        }

        private const int RIGHT_PANEL_MIN_VIEWES = 5;
		private const int LEFT_PANEL_MIN_VIEWES		= 5;
		private const int MID_PANEL_MIN_VIEWES		= 2;
	}
}
