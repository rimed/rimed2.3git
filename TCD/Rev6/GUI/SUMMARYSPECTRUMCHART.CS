using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using Rimed.Framework.Common;
using Rimed.TCD.DAL;
using Rimed.TCD.Utils;

namespace Rimed.TCD.GUI
{
    public partial class SummarySpectrumChart : System.Windows.Forms.UserControl
    {
        private static readonly Brush s_brush = new SolidBrush(GlobalSettings.P2);

        /// <summary>Constructor</summary>
        public SummarySpectrumChart()
        {
            // This call is required by the Windows.Forms Form Designer.
            InitializeComponent();
        }

	    /// <summary>Copy from the gate into the summarySpectrum chard in order to display the gateView.</summary>
	    /// <param name="gateRow"></param>
	    /// <param name="clinalBar"></param>
	    public bool CopyFrom(dsGateExamination.tb_GateExaminationRow gateRow, ClinicalBar clinalBar = null)
		{
			try
			{
				// clinical bar
				if (clinalBar != null)
					summaryClinicalBar1.CopyFrom(clinalBar);
				else
					summaryClinicalBar1.CopyFrom(gateRow);

				// Y Axis.
				spectrumYAxis1.MaxValue = gateRow.MaxVal;
				spectrumYAxis1.MinValue = gateRow.MinVal;
				spectrumYAxis1.Update(); // there is call to the update function in order to work around the copy from of the Y axis control.

				if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Diagnostic || LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.DiagnosticLoad)
				{
					// picture.
					string side;
					if (gateRow.Side == (byte)GlobalTypes.ESide.Left)
						side = "_L";
					else if (gateRow.Side == (byte)GlobalTypes.ESide.Right)
						side = "_R";
					else
						side = "_M";

					var name = string.Format("{0}{1}{2}", Constants.SUMMARY_IMG_PATH, gateRow.Gate_Index, side);
					name += (File.Exists(name + "_MC.jpg") ? "_MC.jpg" : ".jpg");

					using (var img = Image.FromFile(name))
					{
						// this work around in order to release the file in case of backup.
						using (var ms = new MemoryStream())
						{
							img.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
							pictureBox1.Image = Image.FromStream(ms);
							pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
						}
					}
				}
			}
			catch (FileNotFoundException ex)
			{
				Logger.LogError(ex, "Not all the files of the summary were found");

				return false;
			}
			catch (Exception ex)
			{
				Logger.LogError(ex);
				return false;
			}

			return true;
		}

		public Image Convert2Image(string side)
        {
            if (string.IsNullOrWhiteSpace(side))    
                side = string.Empty;

            var gateId = Guid.Empty;
            var view = Parent as SummaryView;
            if (view != null)
                gateId = view.GateId;

            var fileName = string.Format("{0}{1}{2}_MC.jpg", Constants.TEMP_IMG_PATH, gateId, side);
            if (!File.Exists(fileName))
                fileName = string.Format("{0}{1}{2}.jpg", Constants.TEMP_IMG_PATH, gateId, side);

            var isSpectrumFileExists = File.Exists(fileName);
            if (!isSpectrumFileExists)
                Logger.LogError(string.Format("{0}.Convert2Image({1}): Spectrum image file ('{2}') not found.", Name, side, fileName));

            //Create one image that consist everything.
            //OFER, v1.18.2.9: System.Runtime.InteropServices.ExternalException Message: A generic error occurred in GDI+. Source: System.Drawing at System.Drawing.Bitmap.GetHbitmap(Color background) at System.Drawing.Bitmap.GetHbitmap() at TCD2003.GUI.SummarySpectrumChart.Convert2Image(String side)
            var bmp = new Bitmap(GlobalSettings.REPORT_GATE_VIEW_WIDTH, GlobalSettings.REPORT_GATE_VIEW_HEIGHT);

            using (var grp = Graphics.FromImage(bmp))
            {
                lock (s_brush) 
                    grp.FillRectangle(s_brush, 0, 0, bmp.Width, bmp.Height);

                var height = GlobalSettings.REPORT_CLINICAL_BAR_SIZE;
                var barHeight = bmp.Height - height;
                int axisWidth;
                // get the image of Y axis.
                using (var imgYaxis = spectrumYAxis1.Convert2Image())
                {
                    axisWidth = (int)(imgYaxis.Width * (height / (double)imgYaxis.Height));
                    grp.DrawImage(imgYaxis, 0, barHeight, axisWidth, height);
                }

                var width = bmp.Width - axisWidth - 12 + 10 - GlobalSettings.REPORT_CLINICAL_BAR_SIZE;
                var X = axisWidth -10;
                using (var imgSpectrum = (isSpectrumFileExists ? Image.FromFile(fileName) : new Bitmap(width, GlobalSettings.REPORT_CLINICAL_BAR_SIZE)))
                {
                    grp.DrawImage(imgSpectrum, X, barHeight, width, height);
                    X += width;
                }

                // get the image of the Gradient
                width = 12;
                grp.DrawImage(SpectrumGradient.Image, X, barHeight, width, height);
                X += width;

                // get the image of the clinical param.
                using (var imgClinicParam = summaryClinicalBar1.Convert2Image())
                {
                    grp.DrawImage(imgClinicParam, X, barHeight, GlobalSettings.REPORT_CLINICAL_BAR_SIZE, height);
                }
            }

            return bmp;
        }

        public Image Convert2ImageV2(string side)
        {
            if (string.IsNullOrWhiteSpace(side))
                side = string.Empty;

            var gateId = Guid.Empty;
            var view = Parent as SummaryView;
            if (view != null)
                gateId = view.GateId;

            var fileName = string.Format("{0}{1}{2}_MC.jpg", Constants.TEMP_IMG_PATH, gateId, side);
            if (!File.Exists(fileName))
                fileName = string.Format("{0}{1}{2}.jpg", Constants.TEMP_IMG_PATH, gateId, side);

            var isSpectrumFileExists = File.Exists(fileName);
            if (!isSpectrumFileExists)
                Logger.LogError(string.Format("{0}.Convert2Image({1}): Spectrum image file ('{2}') not found.", Name, side, fileName));

            //Create one image that consist everything.
            var bmp = new Bitmap(GlobalSettings.REPORT_GATE_VIEW_WIDTH2, GlobalSettings.REPORT_GATE_VIEW_HEIGHT2);

            using (var grp = Graphics.FromImage(bmp))
            {
                lock (s_brush)
                    grp.FillRectangle(s_brush, 0, 0, bmp.Width, bmp.Height);

                var barHeight = 38;
                var height = bmp.Height - barHeight;
                int axisWidth;
                // get the image of Y axis.
                using (var imgYaxis = spectrumYAxis1.Convert2Image())
                {
                    axisWidth = (int)( imgYaxis.Width * (height / (double)imgYaxis.Height));
                    grp.DrawImage(imgYaxis, 5, barHeight, axisWidth, height);
                }

                var width = bmp.Width - axisWidth - 5 + 12;
                using (var imgSpectrum = (isSpectrumFileExists ? Image.FromFile(fileName) : new Bitmap(width, height)))
                {
                    grp.DrawImage(imgSpectrum, axisWidth, barHeight, width, height);
                }

                // get the image of the Gradient
                grp.DrawImage(SpectrumGradient.Image, bmp.Width - 12, barHeight, 12, height);
            }

            return bmp;
        }
    }
}
