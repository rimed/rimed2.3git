using System;
using System.Drawing;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

using Rimed.TCD.Utils;

namespace Rimed.TCD.GUI
{
    public partial class UserMgrDlg : Form
    {
        private const String OPRATOR_NAME = "Administrator";
        private const String OPERATOR_PASSWD = "gfsoft";

        public UserMgrDlg()
        {
            InitializeComponent();
            BackColor = GlobalSettings.BackgroundDlg;
        }

        private void buttonOK_Click(object sender, System.EventArgs e)
        {
            var fields = LoggerUserActions.GetFilesList();
            fields.Add("textBoxUserName", textBoxUserName.Text);
            fields.Add("textBoxPassword", textBoxPassword.Text);
            LoggerUserActions.MouseClick("buttonOK", Name, fields);

            if (textBoxUserName.Text == OPRATOR_NAME && textBoxPassword.Text == OPERATOR_PASSWD)
                DialogResult = DialogResult.OK;
            else
                DialogResult = DialogResult.Cancel;
        }
    }
}
