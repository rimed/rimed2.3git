using System.Windows.Forms;

namespace Rimed.TCD.GUI
{
    partial class GateView
    {
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            GateView.GateViewCursorsPosChanged -= new GateView.CursorsPosChangedHandler(this.gateView_CursorsPosChangedEvent);

            base.Dispose(disposing);

            removeEvents();
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.contextMenuSpectrum = new System.Windows.Forms.ContextMenu();
            this.AddToSummary = new System.Windows.Forms.MenuItem();
            this.RemoveFromSummary = new System.Windows.Forms.MenuItem();
            this.menuItemSpecInsertCursor = new System.Windows.Forms.MenuItem();
            this.menuItemSpecInsertNotes = new System.Windows.Forms.MenuItem();
            this.menuItemAutoscan = new System.Windows.Forms.MenuItem();
            this.menuItemHitsDetection = new System.Windows.Forms.MenuItem();
            this.menuItemSendTo = new System.Windows.Forms.MenuItem();
            this.menuItemExportSpectrumOnly = new System.Windows.Forms.MenuItem();
            this.menuItemExportFullScreen = new System.Windows.Forms.MenuItem();
            this.menuItemNextBV = new System.Windows.Forms.MenuItem();
            this.menuItemPrint = new System.Windows.Forms.MenuItem();
			this.menuItemClose = new System.Windows.Forms.MenuItem();
			this.menuItemCloseAll = new System.Windows.Forms.MenuItem();
			this.menuItemAddEvent = new System.Windows.Forms.MenuItem();
            this.menuItemDeleteEvent = new System.Windows.Forms.MenuItem();
            this.menuItemAddHit = new System.Windows.Forms.MenuItem();
            this.menuItemDeleteHit = new System.Windows.Forms.MenuItem();
			this.menuItemLine01 = new System.Windows.Forms.MenuItem();
			this.labelId = new System.Windows.Forms.Label();
            this.m_dopplerBar = new DopplerBar();
            this.m_spectrumChart = new SpectrumChart();
            this.SuspendLayout();
            // 
            // contextMenuSpectrum
            // 
            this.contextMenuSpectrum.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.AddToSummary,
            this.RemoveFromSummary,
            this.menuItemSpecInsertCursor,
            this.menuItemSpecInsertNotes,
            this.menuItemAutoscan,
            this.menuItemHitsDetection,
            this.menuItemSendTo,
            this.menuItemNextBV,
            this.menuItemPrint,
			this.menuItemLine01,
            this.menuItemClose,
            this.menuItemCloseAll,
            this.menuItemAddEvent,
            this.menuItemDeleteEvent,
            this.menuItemAddHit,
            this.menuItemDeleteHit});
            this.contextMenuSpectrum.Popup += new System.EventHandler(this.contextMenuSpectrum_Popup);

			// 
			// menuItemClose
			// 
	        this.menuItemClose.Index = 0;
			this.menuItemClose.Text = "Close";
			this.menuItemClose.Click += new System.EventHandler(this.menuItemClose_Click);
			// 
			// menuItemCloseAll
			// 
			this.menuItemCloseAll.Index = 1;
			this.menuItemCloseAll.Text = "Close All";
			this.menuItemCloseAll.Click += new System.EventHandler(this.menuItemCloseAll_Click);
			// 
			// menuItemLine02
			// 
			this.menuItemLine01.Index = 2;
			this.menuItemLine01.Text = "-";
			this.menuItemLine01.Enabled = false;
			this.menuItemLine01.Visible = true;
			// 
            // AddToSummary
            // 
			this.AddToSummary.Index = 3;
            this.AddToSummary.Text = "Update Summary Image";
            this.AddToSummary.Click += new System.EventHandler(this.AddToSummary_Click);
            // 
            // RemoveFromSummary
            // 
			this.RemoveFromSummary.Index = 4;
            this.RemoveFromSummary.Text = "Remove from Summary";
            this.RemoveFromSummary.Click += new System.EventHandler(this.RemoveFromSummary_Click);
            // 
            // menuItemSpecInsertCursor
            // 
			this.menuItemSpecInsertCursor.Index = 5;
            this.menuItemSpecInsertCursor.Text = "Insert Cursors";
            this.menuItemSpecInsertCursor.Click += new System.EventHandler(this.menuItemSpecInsertCursor_Click);
            // 
            // menuItemSpecInsertNotes
            // 
			this.menuItemSpecInsertNotes.Index = 6;
            this.menuItemSpecInsertNotes.Text = "Insert Notes";
            this.menuItemSpecInsertNotes.Click += new System.EventHandler(this.menuItemSpecInsertNotes_Click);
            // 
            // menuItemAutoscan
            // 
			this.menuItemAutoscan.Index = 7;
            this.menuItemAutoscan.Text = "Autoscan";
            this.menuItemAutoscan.Click += new System.EventHandler(this.menuItemAutoscan_Click);
            // 
            // menuItemHitsDetection
            // 
			this.menuItemHitsDetection.Index = 8;
            this.menuItemHitsDetection.Text = "HITS Detection";
            this.menuItemHitsDetection.Click += new System.EventHandler(this.menuItemHitsDetection_Click);
            // 
            // menuItemSendTo
            // 
			this.menuItemSendTo.Index = 9;
            this.menuItemSendTo.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItemExportSpectrumOnly,
            this.menuItemExportFullScreen});
            this.menuItemSendTo.Text = "Export";
            // 
            // menuItemExportSpectrumOnly
            // 
            this.menuItemExportSpectrumOnly.Index = 0;
            this.menuItemExportSpectrumOnly.Text = "Spectrum Only";
            this.menuItemExportSpectrumOnly.Click += new System.EventHandler(this.menuItemExportSpectrumOnly_Click);
            // 
            // FullScreen
            // 
            this.menuItemExportFullScreen.Index = 1;
            this.menuItemExportFullScreen.Text = "Full Screen";
            this.menuItemExportFullScreen.Click += new System.EventHandler(this.menuItemExportFullScreen_Click);
            // 
            // menuItemNextBV
            // 
			this.menuItemNextBV.Index = 10;
            this.menuItemNextBV.Text = "Next BV";
            // 
            // menuItemPrint
            // 
			this.menuItemPrint.Index = 11;
            this.menuItemPrint.Text = "Print";
            this.menuItemPrint.Click += new System.EventHandler(this.menuItemPrint_Click);
			// 
            // menuItemAddEvent
            // 
			this.menuItemAddEvent.Index = 12;
            this.menuItemAddEvent.Text = "Add Event";
            this.menuItemAddEvent.Click += new System.EventHandler(this.menuItemAddEvent_Click);
            // 
            // menuItemDeleteEvent
            // 
			this.menuItemDeleteEvent.Index = 13;
            this.menuItemDeleteEvent.Text = "Delete Event";
            this.menuItemDeleteEvent.Click += new System.EventHandler(this.menuItemDeleteEvent_Click);
            // 
            // menuItemAddHit
            // 
			this.menuItemAddHit.Index = 14;
            this.menuItemAddHit.Text = "Add HITS";
            this.menuItemAddHit.Click += new System.EventHandler(this.menuItemAddHit_Click);
            // 
            // menuItemDeleteHit
            // 
			this.menuItemDeleteHit.Index = 15;
            this.menuItemDeleteHit.Text = "Delete HITS";
            this.menuItemDeleteHit.Click += new System.EventHandler(this.menuItemDeleteHit_Click);

			// 
            // labelId
            // 
            this.labelId.Location = new System.Drawing.Point(0, 0);
            this.labelId.Name = "labelId";
            this.labelId.Size = new System.Drawing.Size(32, 16);
            this.labelId.TabIndex = 2;
            this.labelId.Text = "labelId";
            // 
            // m_dopplerBar
            // 
            this.m_dopplerBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(235)))), ((int)(((byte)(250)))));
            this.m_dopplerBar.BVName = "";
            this.m_dopplerBar.DepthVar = 55D;
            this.m_dopplerBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.m_dopplerBar.GainVar = 4;
            this.m_dopplerBar.HeartRate = 0;
            this.m_dopplerBar.Location = new System.Drawing.Point(0, 0);
            this.m_dopplerBar.Name = "DopplerBar";
            this.m_dopplerBar.PowerVar = 100;
            this.m_dopplerBar.RangeVar = 6;
            this.m_dopplerBar.Size = new System.Drawing.Size(928, 58);
            this.m_dopplerBar.SizeMode = GlobalTypes.ESizeMode.Large;
            this.m_dopplerBar.TabIndex = 3;
            this.m_dopplerBar.ThumpVar = 100D;
            this.m_dopplerBar.VolumeLevel = 0;
            this.m_dopplerBar.WidthVar = 15D;
            // 
            // spectrumChart1
            // 
            this.m_spectrumChart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(170)))), ((int)(((byte)(190)))), ((int)(((byte)(230)))));
            this.m_spectrumChart.ChartMode = GlobalTypes.ESizeMode.Large;
            this.m_spectrumChart.FlowDirection = GlobalTypes.EFlowDirection.FromProbe;
            this.m_spectrumChart.IndexInProbe = 0;
            this.m_spectrumChart.Location = new System.Drawing.Point(8, 72);
            this.m_spectrumChart.Name = "SpectrumChart";
            this.m_spectrumChart.Size = new System.Drawing.Size(808, 432);
            this.m_spectrumChart.SpeedFactor = 2;
            this.m_spectrumChart.TabIndex = 5;
            // 
            // GateView
            // 
            //this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.ContextMenu = this.contextMenuSpectrum;
            this.Controls.Add(this.m_dopplerBar);
            this.Controls.Add(this.labelId);
            this.Controls.Add(this.m_spectrumChart);
            this.Name = "GateView";
            this.Size = new System.Drawing.Size(928, 800);
            this.SizeChanged += new System.EventHandler(this.spectrumChart1_SizeChanged);
            this.VisibleChanged += new System.EventHandler(this.GateView_VisibleChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GateView_KeyDown);
            this.ResumeLayout(false);

        }
        #endregion

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
        private System.Windows.Forms.MenuItem menuItemAutoscan;
        private System.Windows.Forms.Label labelId;
        private System.Windows.Forms.MenuItem AddToSummary;
		private System.Windows.Forms.MenuItem menuItemLine01;
		private System.Windows.Forms.MenuItem RemoveFromSummary;
        private System.Windows.Forms.ContextMenu contextMenuSpectrum;
        private System.Windows.Forms.MenuItem menuItemSpecInsertCursor;
        private System.Windows.Forms.MenuItem menuItemSpecInsertNotes;
        private System.Windows.Forms.MenuItem menuItemHitsDetection;
        private System.Windows.Forms.MenuItem menuItemSendTo;
		private System.Windows.Forms.MenuItem menuItemClose;
		private System.Windows.Forms.MenuItem menuItemCloseAll;				//CR #422
		private System.Windows.Forms.MenuItem menuItemPrint;
        private System.Windows.Forms.MenuItem menuItemNextBV;
        private System.Windows.Forms.MenuItem menuItemAddEvent;
        private System.Windows.Forms.MenuItem menuItemDeleteEvent;
        private System.Windows.Forms.MenuItem menuItemExportSpectrumOnly;
        private System.Windows.Forms.MenuItem menuItemExportFullScreen;
        private System.Windows.Forms.MenuItem menuItemAddHit;
        private System.Windows.Forms.MenuItem menuItemDeleteHit;
		private DopplerBar m_dopplerBar;
		private SpectrumChart m_spectrumChart;
	}
}