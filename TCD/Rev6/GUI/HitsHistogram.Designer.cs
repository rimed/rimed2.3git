namespace Rimed.TCD.GUI
{
    partial class HitsHistogram
    {
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.CustomLabel customLabel11 = new System.Windows.Forms.DataVisualization.Charting.CustomLabel();
            System.Windows.Forms.DataVisualization.Charting.CustomLabel customLabel12 = new System.Windows.Forms.DataVisualization.Charting.CustomLabel();
            System.Windows.Forms.DataVisualization.Charting.CustomLabel customLabel13 = new System.Windows.Forms.DataVisualization.Charting.CustomLabel();
            System.Windows.Forms.DataVisualization.Charting.CustomLabel customLabel14 = new System.Windows.Forms.DataVisualization.Charting.CustomLabel();
            System.Windows.Forms.DataVisualization.Charting.CustomLabel customLabel15 = new System.Windows.Forms.DataVisualization.Charting.CustomLabel();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint9 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0.5D, "5,5");
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint10 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(0.5D, "1,0");
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint11 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1.5D, "5,5");
            System.Windows.Forms.DataVisualization.Charting.DataPoint dataPoint12 = new System.Windows.Forms.DataVisualization.Charting.DataPoint(1.5D, "1,0");
            this.labelOnOff = new System.Windows.Forms.Label();
            this.m_dsHits1 = new DAL.dsHits();
            this.buttonReset = new System.Windows.Forms.Button();
            this.buttonPause = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.axMSChart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.cbProbe0 = new System.Windows.Forms.CheckBox();
            this.cbProbe1 = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.m_dsHits1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axMSChart1)).BeginInit();
            this.SuspendLayout();
            // 
            // labelOnOff
            // 
            this.labelOnOff.AutoSize = true;
            this.labelOnOff.ForeColor = System.Drawing.Color.Red;
            this.labelOnOff.Location = new System.Drawing.Point(10, 6);
            this.labelOnOff.Name = "labelOnOff";
            this.labelOnOff.Size = new System.Drawing.Size(58, 13);
            this.labelOnOff.TabIndex = 1;
            this.labelOnOff.Text = "HITS: OFF";
            // 
            // dsHits1
            // 
            this.m_dsHits1.DataSetName = "dsHits";
            this.m_dsHits1.Locale = new System.Globalization.CultureInfo("en-US");
            this.m_dsHits1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // buttonReset
            // 
            this.buttonReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonReset.Location = new System.Drawing.Point(6, 392);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(70, 24);
            this.buttonReset.TabIndex = 2;
            this.buttonReset.Text = "Reset";
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // buttonPause
            // 
            this.buttonPause.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPause.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPause.Location = new System.Drawing.Point(82, 392);
            this.buttonPause.Name = "buttonPause";
            this.buttonPause.Size = new System.Drawing.Size(70, 24);
            this.buttonPause.TabIndex = 3;
            this.buttonPause.Text = "Pause";
            this.buttonPause.Click += new System.EventHandler(this.buttonPause_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.axMSChart1);
            this.panel1.Location = new System.Drawing.Point(0, 46);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(160, 338);
            this.panel1.TabIndex = 11;
            // 
            // axMSChart1
            // 
            this.axMSChart1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(235)))), ((int)(((byte)(248)))));
            customLabel11.Text = "R1";
            customLabel11.ToPosition = 1D;
            customLabel12.FromPosition = 1D;
            customLabel12.Text = "R2";
            customLabel12.ToPosition = 2D;
            customLabel13.FromPosition = 2D;
            customLabel13.Text = "R3";
            customLabel13.ToPosition = 3D;
            customLabel14.FromPosition = 3D;
            customLabel14.Text = "R4";
            customLabel14.ToPosition = 4D;
            customLabel15.FromPosition = 4D;
            customLabel15.Text = "R5";
            customLabel15.ToPosition = 5D;
            chartArea3.AxisX.CustomLabels.Add(customLabel11);
            chartArea3.AxisX.CustomLabels.Add(customLabel12);
            chartArea3.AxisX.CustomLabels.Add(customLabel13);
            chartArea3.AxisX.CustomLabels.Add(customLabel14);
            chartArea3.AxisX.CustomLabels.Add(customLabel15);
            chartArea3.AxisX.Interval = 1D;
            chartArea3.AxisX.IntervalOffsetType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea3.AxisX.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea3.AxisX.IsLabelAutoFit = false;
            chartArea3.AxisX.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            chartArea3.AxisX.LabelStyle.Interval = 1D;
            chartArea3.AxisX.LabelStyle.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea3.AxisX.Maximum = 5D;
            chartArea3.AxisX.Minimum = 0D;
            chartArea3.AxisX.ScaleBreakStyle.Spacing = 1D;
            chartArea3.AxisX.ScaleBreakStyle.StartFromZero = System.Windows.Forms.DataVisualization.Charting.StartFromZero.Yes;
            chartArea3.AxisX.Title = "Energy";
            chartArea3.AxisX.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            chartArea3.AxisY.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea3.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.True;
            chartArea3.AxisY2.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea3.AxisY2.IsLabelAutoFit = false;
            chartArea3.AxisY2.LabelAutoFitMinFontSize = 8;
            chartArea3.AxisY2.LabelStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            chartArea3.AxisY2.ScaleBreakStyle.LineColor = System.Drawing.Color.Empty;
            chartArea3.AxisY2.Title = "Number of HITS";
            chartArea3.AxisY2.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            chartArea3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(235)))), ((int)(((byte)(248)))));
            chartArea3.Name = "ChartArea1";
            this.axMSChart1.ChartAreas.Add(chartArea3);
            this.axMSChart1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axMSChart1.Location = new System.Drawing.Point(0, 0);
            this.axMSChart1.Name = "axMSChart1";
            this.axMSChart1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.RangeColumn;
            series5.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(250)))));
            series5.CustomProperties = "DrawSideBySide=False, PixelPointWidth=14";
            series5.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            series5.LabelForeColor = System.Drawing.Color.White;
            series5.Name = "Series1";
            dataPoint9.Label = "13";
            dataPoint10.Label = "9";
            series5.Points.Add(dataPoint9);
            series5.Points.Add(dataPoint10);
            series5.YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Secondary;
            series5.YValuesPerPoint = 2;
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.RangeColumn;
            series6.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(250)))));
            series6.CustomProperties = "DrawSideBySide=False, PixelPointWidth=14";
            series6.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            series6.LabelForeColor = System.Drawing.Color.White;
            series6.Name = "Series2";
            dataPoint12.Label = "17";
            series6.Points.Add(dataPoint11);
            series6.Points.Add(dataPoint12);
            series6.YAxisType = System.Windows.Forms.DataVisualization.Charting.AxisType.Secondary;
            series6.YValuesPerPoint = 2;
            this.axMSChart1.Series.Add(series5);
            this.axMSChart1.Series.Add(series6);
            this.axMSChart1.Size = new System.Drawing.Size(160, 338);
            this.axMSChart1.TabIndex = 1;
            this.axMSChart1.Text = "chart1";
            // 
            // cbProbe0
            // 
            this.cbProbe0.AutoSize = true;
            this.cbProbe0.Checked = true;
            this.cbProbe0.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbProbe0.Location = new System.Drawing.Point(8, 24);
            this.cbProbe0.Name = "cbProbe0";
            this.cbProbe0.Size = new System.Drawing.Size(60, 17);
            this.cbProbe0.TabIndex = 14;
            this.cbProbe0.Text = "Probe0";
            // 
            // cbProbe1
            // 
            this.cbProbe1.AutoSize = true;
            this.cbProbe1.Checked = true;
            this.cbProbe1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbProbe1.Location = new System.Drawing.Point(88, 24);
            this.cbProbe1.Name = "cbProbe1";
            this.cbProbe1.Size = new System.Drawing.Size(60, 17);
            this.cbProbe1.TabIndex = 15;
            this.cbProbe1.Text = "Probe1";
            // 
            // HitsHistogram
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(236)))), ((int)(((byte)(235)))), ((int)(((byte)(248)))));
            this.Controls.Add(this.cbProbe1);
            this.Controls.Add(this.cbProbe0);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.buttonPause);
            this.Controls.Add(this.buttonReset);
            this.Controls.Add(this.labelOnOff);
            this.Name = "HitsHistogram";
            this.Size = new System.Drawing.Size(160, 424);
            this.Load += new System.EventHandler(this.hitsHistogram_Load);
            ((System.ComponentModel.ISupportInitialize)(this.m_dsHits1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axMSChart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.Container components = null;
        private System.Windows.Forms.Label labelOnOff;
        private System.Windows.Forms.Button buttonReset;
        private System.Windows.Forms.Button buttonPause;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataVisualization.Charting.Chart axMSChart1;
        private System.Windows.Forms.CheckBox cbProbe0;
        private System.Windows.Forms.CheckBox cbProbe1;
    }
}