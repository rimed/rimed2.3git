﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Rimed.TCD.GUI
{
    partial class PackRawdataFilesDlg
    {
        private System.Windows.Forms.Label lbInfo;
        private System.Windows.Forms.ImageList imageList1;
        public Syncfusion.Windows.Forms.Tools.ProgressBarAdv progressBarAdv1;
        private System.ComponentModel.IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			this.lbInfo = new System.Windows.Forms.Label();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.progressBarAdv1 = new Syncfusion.Windows.Forms.Tools.ProgressBarAdv();
			this.buttonAbort = new System.Windows.Forms.Button();
			this.labelFile = new System.Windows.Forms.Label();
			this.labelRemainingTime = new System.Windows.Forms.Label();
			this.labelEstimatedTime = new System.Windows.Forms.Label();
			this.labelElapseTime = new System.Windows.Forms.Label();
			this.labelEstimatedTimeValue = new System.Windows.Forms.Label();
			this.labelElapseTimeValue = new System.Windows.Forms.Label();
			this.labelRemainingTimeValue = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.progressBarAdv1)).BeginInit();
			this.SuspendLayout();
			// 
			// lbInfo
			// 
			this.lbInfo.Location = new System.Drawing.Point(16, 16);
			this.lbInfo.Name = "lbInfo";
			this.lbInfo.Size = new System.Drawing.Size(363, 40);
			this.lbInfo.TabIndex = 2;
			this.lbInfo.Text = "Archiving raw data files. Please wait....";
			this.lbInfo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.lbInfo.UseWaitCursor = true;
			// 
			// imageList1
			// 
			this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
			this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
			this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// progressBarAdv1
			// 
			this.progressBarAdv1.BackColor = System.Drawing.SystemColors.Control;
			this.progressBarAdv1.BackGradientEndColor = System.Drawing.SystemColors.ControlLightLight;
			this.progressBarAdv1.BackGradientStartColor = System.Drawing.SystemColors.ControlDark;
			this.progressBarAdv1.BackgroundStyle = Syncfusion.Windows.Forms.Tools.ProgressBarBackgroundStyles.VerticalGradient;
			this.progressBarAdv1.BackMultipleColors = new System.Drawing.Color[] {
        System.Drawing.SystemColors.ControlDark,
        System.Drawing.SystemColors.ControlLightLight,
        System.Drawing.SystemColors.Control};
			this.progressBarAdv1.BackSegments = false;
			this.progressBarAdv1.BackTubeEndColor = System.Drawing.SystemColors.ControlLight;
			this.progressBarAdv1.BackTubeStartColor = System.Drawing.SystemColors.ControlDark;
			this.progressBarAdv1.Border3DStyle = System.Windows.Forms.Border3DStyle.RaisedOuter;
			this.progressBarAdv1.BorderColor = System.Drawing.Color.Black;
			this.progressBarAdv1.FontColor = System.Drawing.SystemColors.HighlightText;
			this.progressBarAdv1.ForeColor = System.Drawing.Color.MediumBlue;
			this.progressBarAdv1.ForegroundImage = null;
			this.progressBarAdv1.GradientEndColor = System.Drawing.Color.Lime;
			this.progressBarAdv1.GradientStartColor = System.Drawing.Color.Red;
			this.progressBarAdv1.Location = new System.Drawing.Point(12, 71);
			this.progressBarAdv1.MultipleColors = new System.Drawing.Color[] {
        System.Drawing.Color.DarkRed,
        System.Drawing.Color.Red,
        System.Drawing.Color.Black};
			this.progressBarAdv1.Name = "progressBarAdv1";
			this.progressBarAdv1.ProgressStyle = Syncfusion.Windows.Forms.Tools.ProgressBarStyles.Tube;
			this.progressBarAdv1.SegmentWidth = 20;
			this.progressBarAdv1.Size = new System.Drawing.Size(367, 23);
			this.progressBarAdv1.StretchImage = false;
			this.progressBarAdv1.StretchMultGrad = false;
			this.progressBarAdv1.TabIndex = 4;
			this.progressBarAdv1.TextShadow = false;
			this.progressBarAdv1.ThemesEnabled = false;
			this.progressBarAdv1.TubeEndColor = System.Drawing.SystemColors.Control;
			this.progressBarAdv1.TubeStartColor = System.Drawing.SystemColors.ControlDark;
			this.progressBarAdv1.UseWaitCursor = true;
			this.progressBarAdv1.Value = 0;
			this.progressBarAdv1.WaitingGradientWidth = 400;
			// 
			// buttonAbort
			// 
			this.buttonAbort.Cursor = System.Windows.Forms.Cursors.WaitCursor;
			this.buttonAbort.Location = new System.Drawing.Point(304, 135);
			this.buttonAbort.Name = "buttonAbort";
			this.buttonAbort.Size = new System.Drawing.Size(75, 23);
			this.buttonAbort.TabIndex = 5;
			this.buttonAbort.Text = "Abort";
			this.buttonAbort.UseVisualStyleBackColor = true;
			this.buttonAbort.UseWaitCursor = true;
			this.buttonAbort.Click += new System.EventHandler(this.buttonAbort_Click);
			// 
			// labelFile
			// 
			this.labelFile.AutoSize = true;
			this.labelFile.Location = new System.Drawing.Point(12, 97);
			this.labelFile.Name = "labelFile";
			this.labelFile.Size = new System.Drawing.Size(67, 13);
			this.labelFile.TabIndex = 6;
			this.labelFile.Text = "ArchivingFile";
			this.labelFile.UseWaitCursor = true;
			// 
			// labelRemainingTime
			// 
			this.labelRemainingTime.AutoSize = true;
			this.labelRemainingTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelRemainingTime.Location = new System.Drawing.Point(144, 144);
			this.labelRemainingTime.Name = "labelRemainingTime";
			this.labelRemainingTime.Size = new System.Drawing.Size(85, 13);
			this.labelRemainingTime.TabIndex = 7;
			this.labelRemainingTime.Text = "Remaining time: ";
			this.labelRemainingTime.UseWaitCursor = true;
			// 
			// labelEstimatedTime
			// 
			this.labelEstimatedTime.AutoSize = true;
			this.labelEstimatedTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelEstimatedTime.Location = new System.Drawing.Point(11, 130);
			this.labelEstimatedTime.Name = "labelEstimatedTime";
			this.labelEstimatedTime.Size = new System.Drawing.Size(81, 13);
			this.labelEstimatedTime.TabIndex = 9;
			this.labelEstimatedTime.Text = "Estimated time: ";
			this.labelEstimatedTime.UseWaitCursor = true;
			// 
			// labelElapseTime
			// 
			this.labelElapseTime.AutoSize = true;
			this.labelElapseTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelElapseTime.Location = new System.Drawing.Point(11, 145);
			this.labelElapseTime.Name = "labelElapseTime";
			this.labelElapseTime.Size = new System.Drawing.Size(73, 13);
			this.labelElapseTime.TabIndex = 10;
			this.labelElapseTime.Text = "Elapsed time: ";
			this.labelElapseTime.UseWaitCursor = true;
			// 
			// labelEstimatedTimeValue
			// 
			this.labelEstimatedTimeValue.AutoSize = true;
			this.labelEstimatedTimeValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelEstimatedTimeValue.Location = new System.Drawing.Point(89, 130);
			this.labelEstimatedTimeValue.Name = "labelEstimatedTimeValue";
			this.labelEstimatedTimeValue.Size = new System.Drawing.Size(49, 13);
			this.labelEstimatedTimeValue.TabIndex = 11;
			this.labelEstimatedTimeValue.Text = "00:00:00";
			this.labelEstimatedTimeValue.UseWaitCursor = true;
			// 
			// labelElapseTimeValue
			// 
			this.labelElapseTimeValue.AutoSize = true;
			this.labelElapseTimeValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelElapseTimeValue.Location = new System.Drawing.Point(89, 145);
			this.labelElapseTimeValue.Name = "labelElapseTimeValue";
			this.labelElapseTimeValue.Size = new System.Drawing.Size(49, 13);
			this.labelElapseTimeValue.TabIndex = 12;
			this.labelElapseTimeValue.Text = "00:00:00";
			this.labelElapseTimeValue.UseWaitCursor = true;
			// 
			// labelRemainingTimeValue
			// 
			this.labelRemainingTimeValue.AutoSize = true;
			this.labelRemainingTimeValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.labelRemainingTimeValue.Location = new System.Drawing.Point(226, 145);
			this.labelRemainingTimeValue.Name = "labelRemainingTimeValue";
			this.labelRemainingTimeValue.Size = new System.Drawing.Size(49, 13);
			this.labelRemainingTimeValue.TabIndex = 13;
			this.labelRemainingTimeValue.Text = "00:00:00";
			this.labelRemainingTimeValue.UseWaitCursor = true;
			// 
			// PackRawdataFilesDlg
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(391, 169);
			this.ControlBox = false;
			this.Controls.Add(this.labelRemainingTimeValue);
			this.Controls.Add(this.labelElapseTimeValue);
			this.Controls.Add(this.labelEstimatedTimeValue);
			this.Controls.Add(this.labelElapseTime);
			this.Controls.Add(this.labelEstimatedTime);
			this.Controls.Add(this.labelRemainingTime);
			this.Controls.Add(this.labelFile);
			this.Controls.Add(this.buttonAbort);
			this.Controls.Add(this.progressBarAdv1);
			this.Controls.Add(this.lbInfo);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "PackRawdataFilesDlg";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Raw-Data Files archive";
			this.TopMost = true;
			this.UseWaitCursor = true;
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ZipRawdataFilesDlg_FormClosing);
			this.Load += new System.EventHandler(this.ZipRawdataFilesDlg_Load);
			((System.ComponentModel.ISupportInitialize)(this.progressBarAdv1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }
        #endregion

		private System.Windows.Forms.Button buttonAbort;
		private System.Windows.Forms.Label labelFile;
		private System.Windows.Forms.Label labelRemainingTime;
		private System.Windows.Forms.Label labelEstimatedTime;
		private System.Windows.Forms.Label labelElapseTime;
		private System.Windows.Forms.Label labelEstimatedTimeValue;
		private System.Windows.Forms.Label labelElapseTimeValue;
		private System.Windows.Forms.Label labelRemainingTimeValue;

    }
}
