﻿namespace Rimed.TCD.GUI
{
	partial class AutoScanGraph
    {
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (this.components != null)
                {
                    this.components.Dispose();
                }

                lock (m_lockCacheBmp)
                {
                    if (this.m_cacheBmp != null)
                    {
                        this.m_cacheBmp.Dispose();
                        m_cacheBmp = null;
                    }
                }

				//if (this.m_zoomBmp != null)	//Ofer: Zoom has no activation - never used
				//{
				//    this.m_zoomBmp.Dispose();
				//    m_zoomBmp = null;
				//}	
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code
        
        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            // 
            // AutoScanGraph
            // 
            this.BackColor = System.Drawing.SystemColors.Desktop;
            this.Name = "AutoScanGraph";
            this.Size = new System.Drawing.Size(702, 341);
            this.SizeChanged += new System.EventHandler(this.AutoScanGraph_SizeChanged);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.AutoScanGraph_MouseDown);
        }
        
        #endregion

        private System.ComponentModel.Container components = null;
    }
}