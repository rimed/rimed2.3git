using System;
using System.Collections.Generic;
using Rimed.TCD.Utils;

namespace Rimed.TCD.GUI
{
    public class CopyFilesDlg
    {
        public enum EFileOperation { MOVE, COPY }

        private readonly List<string> m_source = null;
        private readonly List<string> m_dest = null;
        private readonly IntPtr m_parentHandle = IntPtr.Zero;

        public void AddFile(string src, string dest)
        {
            m_source.Add(src);
            m_dest.Add(dest);
        }

        public CopyFilesDlg(IntPtr pHandle)
        {
            m_parentHandle = pHandle;
            m_source = new List<string>();
            m_dest = new List<string>();
        }

        public bool DoOperation(EFileOperation operation)
        {
            var fo = new ShellFileOperation();

            //Operation does Copy for Copy and Copy+Delete for Move
            fo.Operation = ShellFileOperation.EFileOperations.FO_COPY;
            fo.OwnerWindow = m_parentHandle;

            fo.SourceFiles = m_source;
            fo.DestFiles = m_dest;

            bool res = fo.DoOperation();

            if (res && operation == EFileOperation.MOVE)
            {
                fo.Operation = ShellFileOperation.EFileOperations.FO_DELETE;
                res = res && fo.DoOperation();
            }

            return res;
        }

		//public void UnZip()
		//{
		//    foreach (var fileName in m_dest)
		//    {
		//        if (string.IsNullOrWhiteSpace(fileName) || !File.Exists(fileName))
		//            continue;

		//        if (string.Compare(".tmp", Path.GetExtension(fileName), StringComparison.InvariantCultureIgnoreCase) != 0)
		//            continue;

		//        try
		//        {
		//            var unzippedFile = Path.Combine(Path.GetDirectoryName(fileName),Path.GetFileNameWithoutExtension(fileName));
		//            Files.Delete(unzippedFile);

		//            XZipManager.theXZip.UnzipFile(fileName, unzippedFile);
		//        }
		//        catch (Exception ex)
		//        {
		//            Logger.LogError(ex, string.Format("Fail to unzip '{0}' file.", fileName));
		//        }
		//        Files.Delete(fileName);
		//    }
		//}
    }
}
