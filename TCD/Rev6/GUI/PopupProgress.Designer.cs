﻿namespace Rimed.TCD.GUI
{
	partial class PopupProgress
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.labelMsg = new System.Windows.Forms.Label();
			this.progressBar1 = new System.Windows.Forms.ProgressBar();
			this.labelDetiles = new System.Windows.Forms.Label();
			this.labelPercent = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// labelMsg
			// 
			this.labelMsg.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.labelMsg.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
			this.labelMsg.Location = new System.Drawing.Point(15, 9);
			this.labelMsg.Name = "labelMsg";
			this.labelMsg.Size = new System.Drawing.Size(533, 16);
			this.labelMsg.TabIndex = 0;
			this.labelMsg.Text = "label1";
			this.labelMsg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// progressBar1
			// 
			this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.progressBar1.ForeColor = System.Drawing.Color.Green;
			this.progressBar1.Location = new System.Drawing.Point(12, 37);
			this.progressBar1.Name = "progressBar1";
			this.progressBar1.Size = new System.Drawing.Size(506, 18);
			this.progressBar1.TabIndex = 1;
			// 
			// labelDetiles
			// 
			this.labelDetiles.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.labelDetiles.Location = new System.Drawing.Point(12, 58);
			this.labelDetiles.Name = "labelDetiles";
			this.labelDetiles.Size = new System.Drawing.Size(506, 41);
			this.labelDetiles.TabIndex = 2;
			this.labelDetiles.Text = "label1";
			// 
			// labelPercent
			// 
			this.labelPercent.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.labelPercent.Location = new System.Drawing.Point(516, 37);
			this.labelPercent.Name = "labelPercent";
			this.labelPercent.Size = new System.Drawing.Size(34, 18);
			this.labelPercent.TabIndex = 3;
			this.labelPercent.Text = "100%";
			this.labelPercent.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// PopupProgress
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(560, 106);
			this.ControlBox = false;
			this.Controls.Add(this.labelDetiles);
			this.Controls.Add(this.progressBar1);
			this.Controls.Add(this.labelMsg);
			this.Controls.Add(this.labelPercent);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "PopupProgress";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.Text = "Restore";
			this.TopMost = true;
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label labelMsg;
		private System.Windows.Forms.ProgressBar progressBar1;
		private System.Windows.Forms.Label labelDetiles;
		private System.Windows.Forms.Label labelPercent;
	}
}