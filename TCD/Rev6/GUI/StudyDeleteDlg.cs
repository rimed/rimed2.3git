using System;
using System.Drawing;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using Rimed.Framework.Common;
using Syncfusion.Windows.Forms.Tools.XPMenus;
using Rimed.TCD.DAL;
using Rimed.TCD.Utils;

namespace Rimed.TCD.GUI
{
    public partial class StudyDeleteDlg : Form
    {
        /// <summary>Constructor</summary>
        public StudyDeleteDlg()
        {
//            System.Diagnostics.Trace.WriteLine(string.Format("Window handle: {0} created.", Handle));   // Ofer, Force creation of Window handle

            InitializeComponent();

            var strRes = MainForm.StringManager;
            buttonDelete.Text = strRes.GetString("Delete");
            buttonCancel.Text = strRes.GetString("Close");
            Text = strRes.GetString("Delete Study");

            BackColor = GlobalSettings.BackgroundDlg;
        }

        public void InitData(BarItems items)
        {
            this.treeViewStudies.Nodes.Clear();
            foreach (ParentBarItem ib in items)
            {
                var n = new TreeNode(ib.Text);
                n.Tag = ib.Tag;
                treeViewStudies.Nodes.Add(n);
                foreach (BarItem bi in ib.Items)
                {
                    var nn = new TreeNode(bi.Text);
                    nn.Tag = bi.Tag;
                    n.Nodes.Add(nn);
                }
            }
        }

        /// <summary>Method for selected study deleting</summary>
        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (treeViewStudies.SelectedNode == null || treeViewStudies.SelectedNode.Nodes.Count != 0 || treeViewStudies.SelectedNode.Text == "Default") 
                return;

            var strRes = MainForm.StringManager;
            if ( LoggedDialog.ShowWarning(this, strRes.GetString("Are you sure you want to delete the selected study?"), buttons: MessageBoxButtons.YesNo) != DialogResult.Yes) 
                return;

            var fields = LoggerUserActions.GetFilesList();
            fields.Add("treeViewStudies", treeViewStudies.SelectedNode.Text);
            LoggerUserActions.MouseClick("buttonDelete", Name, fields);

            var selectedNodeTag = (Guid)treeViewStudies.SelectedNode.Tag;
            if (RimedDal.Instance.DeleteStudy(selectedNodeTag))
            {
                // delete the selected study from the tree.
                treeViewStudies.Nodes.Remove(treeViewStudies.SelectedNode);
            }
        }

        private void treeViewStudies_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (treeViewStudies.SelectedNode != null && treeViewStudies.SelectedNode.Index != 0 && treeViewStudies.SelectedNode.FirstNode == null && (Guid)treeViewStudies.SelectedNode.Tag != MainForm.Instance.CurrentStudyId)
                buttonDelete.Enabled = true;
            else
                buttonDelete.Enabled = false;
        }
    }
}
