using System;
using System.Drawing;

namespace Rimed.TCD.GUI
{
	/// <summary>
	/// Summary description for ProbeIndication.
	/// </summary>
	public partial class ProbeIndication : System.Windows.Forms.UserControl
	{
		private Color m_probeColor;
		private String m_probeName;
		/// <summary>Required designer variable.</summary>
		private readonly System.ComponentModel.Container components = null;

		public ProbeIndication()
		{
//            System.Diagnostics.Trace.WriteLine(string.Format("Window handle: {0} created.", Handle));   // Ofer, Force creation of Window handle

            // This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitializeComponent call
		}

		public Color ProbeColor
		{
			get { return m_probeColor; }
			set 
			{
				m_probeColor = value;
				ProbeRect.BackColor = m_probeColor;
			}
		}
		
        public string ProbeName
		{
			get { return m_probeName; }
			set
            { 
				m_probeName = value; 
				Probelabel.Text = m_probeName;
			}
		}
		
        public Font LabelFont
		{
			set { Probelabel.Font = value; }
		}
	}
}
