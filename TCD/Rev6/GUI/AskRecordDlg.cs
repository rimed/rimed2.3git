﻿using System.Windows.Forms;

namespace Rimed.TCD.GUI
{
    public partial class AskRecordDlg : Form
    {
        public AskRecordDlg()
        {
            InitializeComponent();

            var strRes = MainForm.StringManager;// MainForm.StringManager;
            lblMessage.Text = strRes.GetString("Would you like to save the examination?");
            btnSave.Text = strRes.GetString("Save");
            btnNotSave.Text = strRes.GetString("Don't Save");
            btnContinue.Text = strRes.GetString("Continue Recording");

            BackColor = GlobalSettings.BackgroundDlg;
        }
    }
}
