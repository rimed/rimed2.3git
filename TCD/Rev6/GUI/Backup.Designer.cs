namespace Rimed.TCD.GUI
{
    partial class Backup
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.patientListBox = new System.Windows.Forms.CheckedListBox();
            this.lblPatientList = new System.Windows.Forms.Label();
            this.dsExamination1 = new Rimed.TCD.DAL.dsExamination();
            this.lblName = new System.Windows.Forms.Label();
            this.bDetails = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.listViewExamination = new System.Windows.Forms.ListView();
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.coLDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colTime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colSize = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colComments = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblExaminationList = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.gbBackupInformation = new System.Windows.Forms.GroupBox();
            this.labelZipSize = new System.Windows.Forms.Label();
            this.lblSelectedSize = new System.Windows.Forms.Label();
            this.lblLocation = new System.Windows.Forms.Label();
            this.bBrowseFolder = new System.Windows.Forms.Button();
            this.location = new System.Windows.Forms.TextBox();
            this.cdDrive = new System.Windows.Forms.ComboBox();
            this.lblTimeValue = new System.Windows.Forms.Label();
            this.lblEstimation = new System.Windows.Forms.Label();
            this.labelSizeValue = new System.Windows.Forms.Label();
            this.lblSize = new System.Windows.Forms.Label();
            this.rbCD = new System.Windows.Forms.RadioButton();
            this.rbHardDisk = new System.Windows.Forms.RadioButton();
            this.bCopy = new System.Windows.Forms.Button();
            this.bMove = new System.Windows.Forms.Button();
            this.bHelp = new System.Windows.Forms.Button();
            this.bClose = new System.Windows.Forms.Button();
            this.gbSearchPatient = new System.Windows.Forms.GroupBox();
            this.lblID = new System.Windows.Forms.Label();
            this.gbMode = new System.Windows.Forms.GroupBox();
            this.dateTimePickerByDateTo = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerByDateFrom = new System.Windows.Forms.DateTimePicker();
            this.rbToday = new System.Windows.Forms.RadioButton();
            this.rbUserDefined = new System.Windows.Forms.RadioButton();
            this.rbIncrement = new System.Windows.Forms.RadioButton();
            this.rbAll = new System.Windows.Forms.RadioButton();
            this.folderBrowser1 = new System.Windows.Forms.FolderBrowserDialog();
            this.autoCompleteComboBoxID = new Rimed.TCD.GUI.AutoCompleteComboBox();
            this.autoCompleteComboBoxName = new Rimed.TCD.GUI.AutoCompleteComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dsExamination1)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.gbBackupInformation.SuspendLayout();
            this.gbSearchPatient.SuspendLayout();
            this.gbMode.SuspendLayout();
            this.SuspendLayout();
            // 
            // patientListBox
            // 
            this.patientListBox.Location = new System.Drawing.Point(16, 192);
            this.patientListBox.Name = "patientListBox";
            this.patientListBox.Size = new System.Drawing.Size(200, 214);
            this.patientListBox.TabIndex = 0;
            this.patientListBox.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.patientListBox_ItemCheck);
            this.patientListBox.SelectedIndexChanged += new System.EventHandler(this.patientListBox_SelectedIndexChanged);
            // 
            // lblPatientList
            // 
            this.lblPatientList.Location = new System.Drawing.Point(16, 168);
            this.lblPatientList.Name = "lblPatientList";
            this.lblPatientList.Size = new System.Drawing.Size(200, 16);
            this.lblPatientList.TabIndex = 2;
            this.lblPatientList.Text = "Patient List";
            this.lblPatientList.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dsExamination1
            // 
            this.dsExamination1.DataSetName = "dsExamination";
            this.dsExamination1.Locale = new System.Globalization.CultureInfo("he-IL");
            this.dsExamination1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lblName
            // 
            this.lblName.Location = new System.Drawing.Point(32, 40);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(66, 20);
            this.lblName.TabIndex = 11;
            this.lblName.Text = "Name:";
            this.lblName.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // bDetails
            // 
            this.bDetails.AutoSize = true;
            this.bDetails.Enabled = false;
            this.bDetails.Location = new System.Drawing.Point(272, 48);
            this.bDetails.Name = "bDetails";
            this.bDetails.Size = new System.Drawing.Size(96, 32);
            this.bDetails.TabIndex = 12;
            this.bDetails.Text = "Details";
            this.bDetails.Click += new System.EventHandler(this.bDetails_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(216, 248);
            this.panel2.TabIndex = 5;
            // 
            // listViewExamination
            // 
            this.listViewExamination.CheckBoxes = true;
            this.listViewExamination.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader6,
            this.coLDate,
            this.colTime,
            this.colType,
            this.colSize,
            this.colComments});
            this.listViewExamination.FullRowSelect = true;
            this.listViewExamination.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listViewExamination.HideSelection = false;
            this.listViewExamination.Location = new System.Drawing.Point(224, 32);
            this.listViewExamination.Name = "listViewExamination";
            this.listViewExamination.Size = new System.Drawing.Size(432, 214);
            this.listViewExamination.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.listViewExamination.TabIndex = 4;
            this.listViewExamination.UseCompatibleStateImageBehavior = false;
            this.listViewExamination.View = System.Windows.Forms.View.Details;
            this.listViewExamination.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.listViewExamination_ItemCheck);
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "";
            this.columnHeader6.Width = 20;
            // 
            // coLDate
            // 
            this.coLDate.Text = "Date";
            this.coLDate.Width = 75;
            // 
            // colTime
            // 
            this.colTime.Text = "Time";
            this.colTime.Width = 50;
            // 
            // colType
            // 
            this.colType.Text = "Type";
            this.colType.Width = 100;
            // 
            // colSize
            // 
            this.colSize.Text = "Size, K";
            // 
            // colComments
            // 
            this.colComments.Text = "Comments";
            this.colComments.Width = 151;
            // 
            // lblExaminationList
            // 
            this.lblExaminationList.BackColor = System.Drawing.SystemColors.Control;
            this.lblExaminationList.Location = new System.Drawing.Point(138, 3);
            this.lblExaminationList.Name = "lblExaminationList";
            this.lblExaminationList.Size = new System.Drawing.Size(168, 21);
            this.lblExaminationList.TabIndex = 3;
            this.lblExaminationList.Text = "Examination List";
            this.lblExaminationList.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.lblExaminationList);
            this.panel3.Location = new System.Drawing.Point(216, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(440, 248);
            this.panel3.TabIndex = 6;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.listViewExamination);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Location = new System.Drawing.Point(8, 160);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(664, 256);
            this.panel1.TabIndex = 9;
            // 
            // gbBackupInformation
            // 
            this.gbBackupInformation.Controls.Add(this.labelZipSize);
            this.gbBackupInformation.Controls.Add(this.lblSelectedSize);
            this.gbBackupInformation.Controls.Add(this.lblLocation);
            this.gbBackupInformation.Controls.Add(this.bBrowseFolder);
            this.gbBackupInformation.Controls.Add(this.location);
            this.gbBackupInformation.Controls.Add(this.cdDrive);
            this.gbBackupInformation.Controls.Add(this.lblTimeValue);
            this.gbBackupInformation.Controls.Add(this.lblEstimation);
            this.gbBackupInformation.Controls.Add(this.labelSizeValue);
            this.gbBackupInformation.Controls.Add(this.lblSize);
            this.gbBackupInformation.Controls.Add(this.rbCD);
            this.gbBackupInformation.Controls.Add(this.rbHardDisk);
            this.gbBackupInformation.Location = new System.Drawing.Point(8, 424);
            this.gbBackupInformation.Name = "gbBackupInformation";
            this.gbBackupInformation.Size = new System.Drawing.Size(664, 112);
            this.gbBackupInformation.TabIndex = 13;
            this.gbBackupInformation.TabStop = false;
            this.gbBackupInformation.Text = "Backup Information";
            // 
            // labelZipSize
            // 
            this.labelZipSize.AutoSize = true;
            this.labelZipSize.Location = new System.Drawing.Point(568, 56);
            this.labelZipSize.Name = "labelZipSize";
            this.labelZipSize.Size = new System.Drawing.Size(32, 13);
            this.labelZipSize.TabIndex = 10;
            this.labelZipSize.Text = "50Kb";
            // 
            // lblSelectedSize
            // 
            this.lblSelectedSize.Location = new System.Drawing.Point(367, 56);
            this.lblSelectedSize.Name = "lblSelectedSize";
            this.lblSelectedSize.Size = new System.Drawing.Size(193, 16);
            this.lblSelectedSize.TabIndex = 9;
            this.lblSelectedSize.Text = "Size of backup selected files:";
            this.lblSelectedSize.Click += new System.EventHandler(this.lblSelectedSize_Click);
            // 
            // lblLocation
            // 
            this.lblLocation.AutoSize = true;
            this.lblLocation.Location = new System.Drawing.Point(16, 24);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(51, 13);
            this.lblLocation.TabIndex = 8;
            this.lblLocation.Text = "Location:";
            // 
            // bBrowseFolder
            // 
            this.bBrowseFolder.Enabled = false;
            this.bBrowseFolder.Location = new System.Drawing.Point(277, 48);
            this.bBrowseFolder.Name = "bBrowseFolder";
            this.bBrowseFolder.Size = new System.Drawing.Size(24, 24);
            this.bBrowseFolder.TabIndex = 7;
            this.bBrowseFolder.Text = "...";
            this.bBrowseFolder.Click += new System.EventHandler(this.bBrowseFolder_Click);
            // 
            // location
            // 
            this.location.Enabled = false;
            this.location.Location = new System.Drawing.Point(109, 48);
            this.location.Name = "location";
            this.location.Size = new System.Drawing.Size(160, 20);
            this.location.TabIndex = 6;
            this.location.TextChanged += new System.EventHandler(this.location_TextChanged);
            // 
            // cdDrive
            // 
            this.cdDrive.Location = new System.Drawing.Point(109, 80);
            this.cdDrive.Name = "cdDrive";
            this.cdDrive.Size = new System.Drawing.Size(64, 21);
            this.cdDrive.TabIndex = 5;
            // 
            // lblTimeValue
            // 
            this.lblTimeValue.AutoSize = true;
            this.lblTimeValue.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.lblTimeValue.Location = new System.Drawing.Point(568, 80);
            this.lblTimeValue.Name = "lblTimeValue";
            this.lblTimeValue.Size = new System.Drawing.Size(49, 13);
            this.lblTimeValue.TabIndex = 3;
            this.lblTimeValue.Text = "00:00:00";
            this.lblTimeValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblEstimation
            // 
            this.lblEstimation.Location = new System.Drawing.Point(367, 77);
            this.lblEstimation.Name = "lblEstimation";
            this.lblEstimation.Size = new System.Drawing.Size(157, 16);
            this.lblEstimation.TabIndex = 2;
            this.lblEstimation.Text = "Estimated Time:";
            // 
            // labelSizeValue
            // 
            this.labelSizeValue.AutoSize = true;
            this.labelSizeValue.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.labelSizeValue.Location = new System.Drawing.Point(568, 32);
            this.labelSizeValue.Name = "labelSizeValue";
            this.labelSizeValue.Size = new System.Drawing.Size(38, 13);
            this.labelSizeValue.TabIndex = 1;
            this.labelSizeValue.Text = "100Kb";
            this.labelSizeValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSize
            // 
            this.lblSize.Location = new System.Drawing.Point(367, 32);
            this.lblSize.Name = "lblSize";
            this.lblSize.Size = new System.Drawing.Size(195, 16);
            this.lblSize.TabIndex = 0;
            this.lblSize.Text = "Size of selected files";
            // 
            // rbCD
            // 
            this.rbCD.Location = new System.Drawing.Point(16, 80);
            this.rbCD.Name = "rbCD";
            this.rbCD.Size = new System.Drawing.Size(72, 24);
            this.rbCD.TabIndex = 0;
            this.rbCD.Text = "CD-RW";
            this.rbCD.Click += new System.EventHandler(this.rbCD_Click);
            // 
            // rbHardDisk
            // 
            this.rbHardDisk.Checked = true;
            this.rbHardDisk.Location = new System.Drawing.Point(16, 48);
            this.rbHardDisk.Name = "rbHardDisk";
            this.rbHardDisk.Size = new System.Drawing.Size(87, 24);
            this.rbHardDisk.TabIndex = 1;
            this.rbHardDisk.TabStop = true;
            this.rbHardDisk.Text = "Hard Disk";
            this.rbHardDisk.Click += new System.EventHandler(this.rbHardDisk_Click);
            // 
            // bCopy
            // 
            this.bCopy.Enabled = false;
            this.bCopy.Location = new System.Drawing.Point(16, 552);
            this.bCopy.Name = "bCopy";
            this.bCopy.Size = new System.Drawing.Size(75, 23);
            this.bCopy.TabIndex = 14;
            this.bCopy.Text = "Copy";
            this.bCopy.Click += new System.EventHandler(this.bCopy_Click);
            // 
            // bMove
            // 
            this.bMove.Enabled = false;
            this.bMove.Location = new System.Drawing.Point(128, 552);
            this.bMove.Name = "bMove";
            this.bMove.Size = new System.Drawing.Size(75, 23);
            this.bMove.TabIndex = 15;
            this.bMove.Text = "Move";
            this.bMove.Click += new System.EventHandler(this.bMove_Click);
            // 
            // bHelp
            // 
            this.bHelp.Location = new System.Drawing.Point(496, 552);
            this.bHelp.Name = "bHelp";
            this.bHelp.Size = new System.Drawing.Size(75, 23);
            this.bHelp.TabIndex = 16;
            this.bHelp.Text = "Help";
            // 
            // bClose
            // 
            this.bClose.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.bClose.Location = new System.Drawing.Point(592, 552);
            this.bClose.Name = "bClose";
            this.bClose.Size = new System.Drawing.Size(75, 23);
            this.bClose.TabIndex = 17;
            this.bClose.Text = "Close";
            // 
            // gbSearchPatient
            // 
            this.gbSearchPatient.Controls.Add(this.autoCompleteComboBoxID);
            this.gbSearchPatient.Controls.Add(this.autoCompleteComboBoxName);
            this.gbSearchPatient.Controls.Add(this.bDetails);
            this.gbSearchPatient.Controls.Add(this.lblID);
            this.gbSearchPatient.Controls.Add(this.lblName);
            this.gbSearchPatient.Location = new System.Drawing.Point(253, 24);
            this.gbSearchPatient.Name = "gbSearchPatient";
            this.gbSearchPatient.Size = new System.Drawing.Size(419, 120);
            this.gbSearchPatient.TabIndex = 18;
            this.gbSearchPatient.TabStop = false;
            this.gbSearchPatient.Text = "Search Patient";
            // 
            // lblID
            // 
            this.lblID.Location = new System.Drawing.Point(32, 73);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(66, 20);
            this.lblID.TabIndex = 19;
            this.lblID.Text = "ID#:";
            this.lblID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gbMode
            // 
            this.gbMode.Controls.Add(this.dateTimePickerByDateTo);
            this.gbMode.Controls.Add(this.dateTimePickerByDateFrom);
            this.gbMode.Controls.Add(this.rbToday);
            this.gbMode.Controls.Add(this.rbUserDefined);
            this.gbMode.Controls.Add(this.rbIncrement);
            this.gbMode.Controls.Add(this.rbAll);
            this.gbMode.Location = new System.Drawing.Point(8, 24);
            this.gbMode.Name = "gbMode";
            this.gbMode.Size = new System.Drawing.Size(239, 120);
            this.gbMode.TabIndex = 19;
            this.gbMode.TabStop = false;
            this.gbMode.Text = "Mode For Backup";
            // 
            // dateTimePickerByDateTo
            // 
            this.dateTimePickerByDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerByDateTo.Location = new System.Drawing.Point(127, 73);
            this.dateTimePickerByDateTo.Name = "dateTimePickerByDateTo";
            this.dateTimePickerByDateTo.Size = new System.Drawing.Size(101, 20);
            this.dateTimePickerByDateTo.TabIndex = 5;
            this.dateTimePickerByDateTo.Visible = false;
            this.dateTimePickerByDateTo.CloseUp += new System.EventHandler(this.dateTimePickerByDate_CloseUp);
            this.dateTimePickerByDateTo.DropDown += new System.EventHandler(this.dateTimePickerByDate_DropDown);
            // 
            // dateTimePickerByDateFrom
            // 
            this.dateTimePickerByDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerByDateFrom.Location = new System.Drawing.Point(16, 73);
            this.dateTimePickerByDateFrom.Name = "dateTimePickerByDateFrom";
            this.dateTimePickerByDateFrom.Size = new System.Drawing.Size(101, 20);
            this.dateTimePickerByDateFrom.TabIndex = 4;
            this.dateTimePickerByDateFrom.Visible = false;
            this.dateTimePickerByDateFrom.CloseUp += new System.EventHandler(this.dateTimePickerByDate_CloseUp);
            this.dateTimePickerByDateFrom.DropDown += new System.EventHandler(this.dateTimePickerByDate_DropDown);
            // 
            // rbToday
            // 
            this.rbToday.AutoSize = true;
            this.rbToday.Location = new System.Drawing.Point(16, 48);
            this.rbToday.Name = "rbToday";
            this.rbToday.Size = new System.Drawing.Size(64, 17);
            this.rbToday.TabIndex = 3;
            this.rbToday.Text = "By date:";
            this.rbToday.Click += new System.EventHandler(this.rbToday_Click);
            // 
            // rbUserDefined
            // 
            this.rbUserDefined.AutoSize = true;
            this.rbUserDefined.Checked = true;
            this.rbUserDefined.Location = new System.Drawing.Point(16, 97);
            this.rbUserDefined.Name = "rbUserDefined";
            this.rbUserDefined.Size = new System.Drawing.Size(87, 17);
            this.rbUserDefined.TabIndex = 2;
            this.rbUserDefined.TabStop = true;
            this.rbUserDefined.Text = "User Defined";
            this.rbUserDefined.Click += new System.EventHandler(this.rbUserDefined_Click);
            // 
            // rbIncrement
            // 
            this.rbIncrement.AutoSize = true;
            this.rbIncrement.Location = new System.Drawing.Point(127, 19);
            this.rbIncrement.Name = "rbIncrement";
            this.rbIncrement.Size = new System.Drawing.Size(72, 17);
            this.rbIncrement.TabIndex = 1;
            this.rbIncrement.Text = "Increment";
            this.rbIncrement.Visible = false;
            this.rbIncrement.Click += new System.EventHandler(this.rbIncrement_Click);
            // 
            // rbAll
            // 
            this.rbAll.AutoSize = true;
            this.rbAll.Location = new System.Drawing.Point(16, 19);
            this.rbAll.Name = "rbAll";
            this.rbAll.Size = new System.Drawing.Size(36, 17);
            this.rbAll.TabIndex = 0;
            this.rbAll.Text = "All";
            this.rbAll.Click += new System.EventHandler(this.rbAll_Click);
            // 
            // folderBrowser1
            // 
            this.folderBrowser1.Description = "Backup Location";
            this.folderBrowser1.RootFolder = System.Environment.SpecialFolder.MyComputer;
            // 
            // autoCompleteComboBoxID
            // 
            this.autoCompleteComboBoxID.LimitToList = true;
            this.autoCompleteComboBoxID.Location = new System.Drawing.Point(104, 72);
            this.autoCompleteComboBoxID.Name = "autoCompleteComboBoxID";
            this.autoCompleteComboBoxID.Size = new System.Drawing.Size(121, 21);
            this.autoCompleteComboBoxID.TabIndex = 22;
            this.autoCompleteComboBoxID.Text = "autoCompleteComboBox2";
            this.autoCompleteComboBoxID.SelectedIndexChanged += new System.EventHandler(this.autoCompleteComboBoxID_SelectedIndexChanged);
            // 
            // autoCompleteComboBoxName
            // 
            this.autoCompleteComboBoxName.LimitToList = true;
            this.autoCompleteComboBoxName.Location = new System.Drawing.Point(104, 40);
            this.autoCompleteComboBoxName.Name = "autoCompleteComboBoxName";
            this.autoCompleteComboBoxName.Size = new System.Drawing.Size(121, 21);
            this.autoCompleteComboBoxName.TabIndex = 21;
            this.autoCompleteComboBoxName.Text = "autoCompleteComboBox1";
            this.autoCompleteComboBoxName.SelectedIndexChanged += new System.EventHandler(this.autoCompleteComboBoxName_SelectedIndexChanged);
            // 
            // Backup
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(680, 584);
            this.Controls.Add(this.gbMode);
            this.Controls.Add(this.bClose);
            this.Controls.Add(this.bHelp);
            this.Controls.Add(this.bMove);
            this.Controls.Add(this.bCopy);
            this.Controls.Add(this.gbBackupInformation);
            this.Controls.Add(this.lblPatientList);
            this.Controls.Add(this.patientListBox);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.gbSearchPatient);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Backup";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Backup Patient Data";
            ((System.ComponentModel.ISupportInitialize)(this.dsExamination1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.gbBackupInformation.ResumeLayout(false);
            this.gbBackupInformation.PerformLayout();
            this.gbSearchPatient.ResumeLayout(false);
            this.gbSearchPatient.PerformLayout();
            this.gbMode.ResumeLayout(false);
            this.gbMode.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblPatientList;
        private System.ComponentModel.IContainer components = null;
        private DAL.dsExamination dsExamination1;
        private System.Windows.Forms.CheckedListBox patientListBox;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ListView listViewExamination;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader coLDate;
        private System.Windows.Forms.ColumnHeader colTime;
        private System.Windows.Forms.ColumnHeader colType;
        private System.Windows.Forms.ColumnHeader colSize;
        private System.Windows.Forms.ColumnHeader colComments;
        private System.Windows.Forms.Label lblExaminationList;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox gbBackupInformation;
        private System.Windows.Forms.Button bClose;
        private System.Windows.Forms.Label lblSize;
        private System.Windows.Forms.Label lblEstimation;
        private System.Windows.Forms.Label lblTimeValue;
        private System.Windows.Forms.GroupBox gbSearchPatient;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.GroupBox gbMode;
        private System.Windows.Forms.Button bDetails;
        private System.Windows.Forms.RadioButton rbUserDefined;
        private System.Windows.Forms.RadioButton rbIncrement;
        private System.Windows.Forms.RadioButton rbAll;
        private System.Windows.Forms.FolderBrowserDialog folderBrowser1;
        private System.Windows.Forms.Button bBrowseFolder;
        private System.Windows.Forms.TextBox location;
        private System.Windows.Forms.RadioButton rbCD;
        private System.Windows.Forms.RadioButton rbHardDisk;
        private System.Windows.Forms.ComboBox cdDrive;
        private System.Windows.Forms.Label lblLocation;
        private System.Windows.Forms.Label labelSizeValue;
        private System.Windows.Forms.Button bCopy;
        private System.Windows.Forms.Button bMove;
        private System.Windows.Forms.Button bHelp;
        private AutoCompleteComboBox autoCompleteComboBoxName;
        private AutoCompleteComboBox autoCompleteComboBoxID;
        private System.Windows.Forms.Label lblSelectedSize;
        private System.Windows.Forms.Label labelZipSize;
        private System.Windows.Forms.RadioButton rbToday;
        private System.Windows.Forms.DateTimePicker dateTimePickerByDateFrom;
        private System.Windows.Forms.DateTimePicker dateTimePickerByDateTo;
    }
}