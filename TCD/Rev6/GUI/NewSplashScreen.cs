﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using Microsoft.Win32;
using System.Runtime.InteropServices;
using Rimed.TCD.GUI.Properties;

namespace Rimed.TCD.GUI
{
    public partial class NewSplashScreen : Form
    {
        #region Member Variables
        // Threading
        private static NewSplashScreen ms_frmSplash = null;
        private static Thread ms_oThread = null;

        // Fade in and out.
        private double m_dblOpacityIncrement = .05;
        private double m_dblOpacityDecrement = .08;
        private const int TIMER_INTERVAL = 50;

        // Status and progress bar
        private string m_sStatus;
        private string m_sTimeRemaining;
        private double m_dblCompletionFraction = 0.0;
        private Rectangle m_rProgress;

        // Progress smoothing
        private double m_dblLastCompletionFraction = 0.0;
        private double m_dblPBIncrementPerTimerInterval = .015;

        // Self-calibration support
        private int m_iIndex = 1;
        private int m_iActualTicks = 0;
        private ArrayList m_alPreviousCompletionFraction;
        private ArrayList m_alActualTimes = new ArrayList();
        private DateTime m_dtStart;
        private bool m_bFirstLaunch = false;
        private bool m_bDTSet = false;

        ProgressBarStyle state = ProgressBarStyle.Continuous;
        private int posPercent;
        private int posWait;
        private int percentWeidth;
        private bool moveLeft = true;
        private string oldString = "";


        private int twoHD = 2;
        private int fiveHD = 5;
        private int fourHD = 4;
        private int fortyHD = 40;
        private int tenHD = 10;

        #endregion Member Variables


        public NewSplashScreen()
        {
            InitializeComponent();
            //this.Opacity = 0.0;
            UpdateTimer.Interval = TIMER_INTERVAL;
            UpdateTimer.Start();

            this.ClientSize = new Size(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);

            string appType = AppSettings.Active.IsDigiOne ? "O" : "L";
            if ((double) Screen.PrimaryScreen.Bounds.Width/Screen.PrimaryScreen.Bounds.Height > 1.5)
            {
                SetHD();
                BackgroundImage = GetResizedImage((Image)Resources.ResourceManager.GetObject("Rimed_screen_00_" + appType + "_HD"), Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            }
            else
            {
                BackgroundImage = GetResizedImage((Image)Resources.ResourceManager.GetObject("Rimed_screen_00_" + appType), Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            }
        }

        #region Public Static Methods
        // A static method to create the thread and 
        // launch the SplashScreen.
        static public void ShowSplashScreen()
        {
            // Make sure it's only launched once.
            if (ms_frmSplash != null)
                return;
            ms_oThread = new Thread(new ThreadStart(NewSplashScreen.ShowForm));
            ms_oThread.IsBackground = true;
            ms_oThread.SetApartmentState(ApartmentState.STA);
            ms_oThread.Start();
            while (ms_frmSplash == null || ms_frmSplash.IsHandleCreated == false)
            {
                System.Threading.Thread.Sleep(TIMER_INTERVAL);
            }
        }

        // Close the form without setting the parent.
        static public void Off()
        {
            if (ms_frmSplash != null && ms_frmSplash.IsDisposed == false)
            {
                // Make it start going away.
                ms_frmSplash.m_dblOpacityIncrement = -ms_frmSplash.m_dblOpacityDecrement;
            }
            ms_oThread = null;	// we don't need these any more.
            ms_frmSplash = null;
        }

        // A static method to set the status and update the reference.
        static public void SetText(string newStatus)
        {
            SetText(newStatus, true);
        }

        // A static method to set the status and optionally update the reference.
        // This is useful if you are in a section of code that has a variable
        // set of status string updates.  In that case, don't set the reference.
        static public void SetText(string newStatus, bool setReference)
        {
            if (ms_frmSplash == null)
                return;

            ms_frmSplash.m_sStatus = newStatus;

            if (setReference)
                ms_frmSplash.SetReferenceInternal();
        }

        // Static method called from the initializing application to 
        // give the splash screen reference points.  Not needed if
        // you are using a lot of status strings.
        static public void SetReferencePoint()
        {
            if (ms_frmSplash == null)
                return;
            ms_frmSplash.SetReferenceInternal();

        }
        #endregion Public Static Methods

        public static Image GetResizedImage(Image img, int width, int height)
        {
            Bitmap b = new Bitmap(width, height);
            Graphics g = Graphics.FromImage((Image)b);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;
            g.DrawImage(img, 0, 0, width, height);
            g.Dispose();

            try
            {
                return (Image)b.Clone();
            }
            finally
            {
                b.Dispose();
                b = null;
                g = null;
            }
        }

        #region Private Methods

        private void SetHD()
        {
            double coeffHD_X = (double)Screen.PrimaryScreen.Bounds.Width / 1024;
            double coeffHD_Y = (double)Screen.PrimaryScreen.Bounds.Height / 768;
            double coeffHD_W = (double)Screen.PrimaryScreen.Bounds.Width / 1920;
            double coeffHD_H = (double)Screen.PrimaryScreen.Bounds.Height / 1080;

            Image img = (Image) Resources.ResourceManager.GetObject("Rimed_prog_bar_1920x1080");
            double coeffHD_ImageW = (double)img.Width / pnlStatus.Width * coeffHD_W;
            double coeffHD_ImageH = (double)img.Height / pnlStatus.Height * coeffHD_H;
            pnlStatus.Image = GetResizedImage(img, (int)(img.Width * coeffHD_W), (int)(img.Height * coeffHD_H));

            pnlStatus.Left = (int)(pnlStatus.Left * coeffHD_X);
            pnlStatus.Top = (int)(pnlStatus.Top * coeffHD_Y);
            pnlStatus.Width = (int)(pnlStatus.Image.Width);
            pnlStatus.Height = (int)(pnlStatus.Image.Height);

            lblStatus.Left = (int)Math.Round((lblStatus.Left * coeffHD_X));
            lblStatus.Top = (int)Math.Round((lblStatus.Top * coeffHD_Y));
            lblStatus.Width = (int)Math.Round((lblStatus.Width * coeffHD_ImageW));
            lblStatus.Height = (int)Math.Round((lblStatus.Height * coeffHD_ImageH));
            lblStatus.Font = new Font(lblStatus.Font.Name, (float)(lblStatus.Font.Size * coeffHD_ImageH));

            twoHD = (int)Math.Round(2 * coeffHD_ImageH);
            fourHD = (int)Math.Round(4 * coeffHD_ImageH);
            fiveHD = (int)Math.Round(5 * coeffHD_ImageH);
            fortyHD = (int)Math.Round(40 * coeffHD_ImageW);
            tenHD = (int)Math.Round(10 * coeffHD_ImageW);
        }


        // A private entry point for the thread.
        static private void ShowForm()
        {
            ms_frmSplash = new NewSplashScreen();
            //Deleted by Alex  v 2.2.3.36 18/05/2016 
            //ms_frmSplash.TopMost = true;
            ms_frmSplash.Width = Screen.PrimaryScreen.Bounds.Width;
            ms_frmSplash.Height = Screen.PrimaryScreen.Bounds.Height;
            Application.Run(ms_frmSplash);
        }

        // Internal method for setting reference points.
        private void SetReferenceInternal()
        {
            if (m_bDTSet == false)
            {
                m_bDTSet = true;
                m_dtStart = DateTime.Now;
                ReadIncrements();
            }
            double dblMilliseconds = ElapsedMilliSeconds();
            m_alActualTimes.Add(dblMilliseconds);
            m_dblLastCompletionFraction = m_dblCompletionFraction;
            if (m_alPreviousCompletionFraction != null && m_iIndex < m_alPreviousCompletionFraction.Count)
                m_dblCompletionFraction = (double)m_alPreviousCompletionFraction[m_iIndex++];
            else
                m_dblCompletionFraction = (m_iIndex > 0) ? 1 : 0;
        }

        // Utility function to return elapsed Milliseconds since the 
        // SplashScreen was launched.
        private double ElapsedMilliSeconds()
        {
            TimeSpan ts = DateTime.Now - m_dtStart;
            return ts.TotalMilliseconds;
        }

        // Function to read the checkpoint intervals from the previous invocation of the
        // splashscreen from the XML file.
        private void ReadIncrements()
        {
            string sPBIncrementPerTimerInterval = SplashScreenXMLStorage.Interval;
            double dblResult;

            if (Double.TryParse(sPBIncrementPerTimerInterval, System.Globalization.NumberStyles.Float, System.Globalization.NumberFormatInfo.InvariantInfo, out dblResult) == true)
                m_dblPBIncrementPerTimerInterval = dblResult;
            else
                m_dblPBIncrementPerTimerInterval = .0015;

            string sPBPreviousPctComplete = SplashScreenXMLStorage.Percents;

            if (sPBPreviousPctComplete != "")
            {
                string[] aTimes = sPBPreviousPctComplete.Split(null);
                m_alPreviousCompletionFraction = new ArrayList();

                for (int i = 0; i < aTimes.Length; i++)
                {
                    double dblVal;
                    if (Double.TryParse(aTimes[i], System.Globalization.NumberStyles.Float, System.Globalization.NumberFormatInfo.InvariantInfo, out dblVal) == true)
                        m_alPreviousCompletionFraction.Add(dblVal);
                    else
                        m_alPreviousCompletionFraction.Add(1.0);
                }
            }
            else
            {
                m_bFirstLaunch = true;
                m_sTimeRemaining = "";
            }
        }

        // Method to store the intervals (in percent complete) from the current invocation of
        // the splash screen to XML storage.
        private void StoreIncrements()
        {
            string sPercent = "";
            double dblElapsedMilliseconds = ElapsedMilliSeconds();
            for (int i = 0; i < m_alActualTimes.Count; i++)
                sPercent += ((double)m_alActualTimes[i] / dblElapsedMilliseconds).ToString("0.####", System.Globalization.NumberFormatInfo.InvariantInfo) + " ";

            SplashScreenXMLStorage.Percents = sPercent;

            m_dblPBIncrementPerTimerInterval = 1.0 / (double)m_iActualTicks;

            SplashScreenXMLStorage.Interval = m_dblPBIncrementPerTimerInterval.ToString("#.000000", System.Globalization.NumberFormatInfo.InvariantInfo);
        }

        public static NewSplashScreen GetSplashScreen()
        { 
            return ms_frmSplash;
        }

        #endregion Private Methods

        #region Event Handlers
        // Tick Event handler for the Timer control.  Handle fade in and fade out and paint progress bar. 
        private void UpdateTimer_Tick(object sender, System.EventArgs e)
        {

            if (m_sStatus != null && m_sStatus.StartsWith("Connecting ("))
                state = ProgressBarStyle.Marquee;
            else
            {
                state = ProgressBarStyle.Continuous;
                if (oldString != m_sStatus)
                {
                    oldString = m_sStatus;
                    if (m_sStatus.IndexOf("%") > -1)
                        posPercent = Convert.ToInt16(m_sStatus.TrimEnd('%'));
                }
            }


            lblStatus.Text = m_sStatus;

            // Calculate opacity
            if (m_dblOpacityIncrement > 0)		// Starting up splash screen
            {
                m_iActualTicks++;
                if (this.Opacity < 1)
                    this.Opacity += m_dblOpacityIncrement;
            }
            else // Closing down splash screen
            {
                if (this.Opacity > 0)
                    this.Opacity += m_dblOpacityIncrement;
                else
                {
                    StoreIncrements();
                    UpdateTimer.Stop();
                    this.Close();
                }
            }

            try
            {

                // Paint progress bar
                Graphics g = pnlStatus.CreateGraphics();
                if (state == ProgressBarStyle.Continuous)
                {
                    percentWeidth = pnlStatus.Width / 100 * posPercent;
                    g.FillRectangle(Brushes.White, fiveHD, fourHD, pnlStatus.Width - (int)(1.5 * fiveHD), fiveHD);
                    g.FillRectangle(Brushes.LawnGreen, fiveHD, fourHD, percentWeidth, fiveHD);
                }
                else
                {
                    // Changed by Alex 27/04/2016 bug #850 v 2.2.3.31
                    //if (moveLeft && posWait + fortyHD >= pnlStatus.Width - 2 * fiveHD)
                    //    moveLeft = false;
                    //else if (!moveLeft && posWait + fortyHD <= fortyHD + 2 * fiveHD)
                    //    moveLeft = true;
                    //posWait = (moveLeft ? posWait + tenHD : posWait - tenHD);

                    if (posWait + fortyHD >= pnlStatus.Width - twoHD * fiveHD)
                        posWait = 0;
                    posWait = posWait + tenHD;
                    g.FillRectangle(Brushes.White, fiveHD, fourHD, pnlStatus.Width - (int)(1.5 * fiveHD), fiveHD);
                    g.FillRectangle(Brushes.LawnGreen, fiveHD + posWait, fourHD, fortyHD, fiveHD);
                }
            }
            catch (Exception)
            {
            }
        }

        #endregion Event Handlers

    }

    #region Auxiliary Classes
    /// <summary>
    /// A specialized class for managing XML storage for the splash screen.
    /// </summary>
    internal class SplashScreenXMLStorage
    {
        private static string ms_StoredValues = "SplashScreen.xml";
        private static string ms_DefaultPercents = "";
        private static string ms_DefaultIncrement = ".015";


        // Get or set the string storing the percentage complete at each checkpoint.
        static public string Percents
        {
            get { return GetValue("Percents", ms_DefaultPercents); }
            set { SetValue("Percents", value); }
        }
        // Get or set how much time passes between updates.
        static public string Interval
        {
            get { return GetValue("Interval", ms_DefaultIncrement); }
            set { SetValue("Interval", value); }
        }

        // Store the file in a location where it can be written with only User rights. (Don't use install directory).
        static private string StoragePath
        {
            get { return Path.Combine(Application.UserAppDataPath, ms_StoredValues); }
        }

        // Helper method for getting inner text of named element.
        static private string GetValue(string name, string defaultValue)
        {
            if (!File.Exists(StoragePath))
                return defaultValue;

            try
            {
                XmlDocument docXML = new XmlDocument();
                docXML.Load(StoragePath);
                XmlElement elValue = docXML.DocumentElement.SelectSingleNode(name) as XmlElement;
                return (elValue == null) ? defaultValue : elValue.InnerText;
            }
            catch
            {
                return defaultValue;
            }
        }

        // Helper method for setting inner text of named element.  Creates document if it doesn't exist.
        static public void SetValue(string name,
             string stringValue)
        {
            XmlDocument docXML = new XmlDocument();
            XmlElement elRoot = null;
            if (!File.Exists(StoragePath))
            {
                elRoot = docXML.CreateElement("root");
                docXML.AppendChild(elRoot);
            }
            else
            {
                docXML.Load(StoragePath);
                elRoot = docXML.DocumentElement;
            }
            XmlElement value = docXML.DocumentElement.SelectSingleNode(name) as XmlElement;
            if (value == null)
            {
                value = docXML.CreateElement(name);
                elRoot.AppendChild(value);
            }
            value.InnerText = stringValue;
            docXML.Save(StoragePath);
        }
    }
    #endregion Auxiliary Classes
}

