using System;
using System.Windows.Forms;

namespace Rimed.TCD.GUI
{
    partial class SpectrumChart
    {
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SpectrumChart));
            this.gradientPanelCM = new Syncfusion.Windows.Forms.Tools.GradientPanel();
            this.bUnits = new System.Windows.Forms.Button();
            this.probImage = new System.Windows.Forms.Label();
            this.m_spectrumYAxis1 = new Rimed.TCD.GUI.SpectrumYAxis();
            this.imageListProbLarge = new System.Windows.Forms.ImageList(this.components);
            this.imageListProbSmall = new System.Windows.Forms.ImageList(this.components);
            this.BackGroundPanel = new Syncfusion.Windows.Forms.Tools.GradientPanel();
            this.labelLastDrawnTime = new System.Windows.Forms.Label();
            this.SpectrumGradient = new System.Windows.Forms.PictureBox();
            this.m_graph = new Rimed.TCD.GUI.SpectrumGraph();
            this.m_clinicalBar1 = new Rimed.TCD.GUI.ClinicalBar();
            this.gradientPanelCM.SuspendLayout();
            this.BackGroundPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SpectrumGradient)).BeginInit();
            this.SuspendLayout();
            // 
            // gradientPanelCM
            // 
            this.gradientPanelCM.BackColor = System.Drawing.Color.Transparent;
            this.gradientPanelCM.Border3DStyle = System.Windows.Forms.Border3DStyle.Flat;
            this.gradientPanelCM.BorderColor = System.Drawing.Color.Black;
            this.gradientPanelCM.BorderSides = System.Windows.Forms.Border3DSide.Middle;
            this.gradientPanelCM.BorderSingle = System.Windows.Forms.ButtonBorderStyle.None;
            this.gradientPanelCM.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gradientPanelCM.Controls.Add(this.bUnits);
            this.gradientPanelCM.Controls.Add(this.probImage);
            this.gradientPanelCM.Controls.Add(this.m_spectrumYAxis1);
            this.gradientPanelCM.Dock = System.Windows.Forms.DockStyle.Left;
            this.gradientPanelCM.Location = new System.Drawing.Point(0, 0);
            this.gradientPanelCM.Name = "gradientPanelCM";
            this.gradientPanelCM.Size = new System.Drawing.Size(50, 432);
            this.gradientPanelCM.TabIndex = 2;
            // 
            // bUnits
            // 
            this.bUnits.BackColor = System.Drawing.Color.Transparent;
            this.bUnits.Font = new System.Drawing.Font("Arial", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.bUnits.Image = ((System.Drawing.Image)(resources.GetObject("bUnits.Image")));
            this.bUnits.Location = new System.Drawing.Point(0, 343);
            this.bUnits.Name = "bUnits";
            this.bUnits.Size = new System.Drawing.Size(44, 25);
            this.bUnits.TabIndex = 0;
            this.bUnits.Text = "CM/S";
            this.bUnits.UseVisualStyleBackColor = false;
            this.bUnits.Click += new System.EventHandler(this.bUnits_Click);
            // 
            // probImage
            // 
            this.probImage.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.probImage.Location = new System.Drawing.Point(6, 190);
            this.probImage.Name = "probImage";
            this.probImage.Size = new System.Drawing.Size(16, 69);
            this.probImage.TabIndex = 1;
            this.probImage.MouseUp += new System.Windows.Forms.MouseEventHandler(this.probImage_MouseUp);
            // 
            // m_spectrumYAxis1
            // 
            this.m_spectrumYAxis1.BackColor = System.Drawing.SystemColors.Control;
            this.m_spectrumYAxis1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.m_spectrumYAxis1.IsReversed = false;
            this.m_spectrumYAxis1.Location = new System.Drawing.Point(0, 0);
            this.m_spectrumYAxis1.MaxValue = 32768D;
            this.m_spectrumYAxis1.MinValue = -32768D;
            this.m_spectrumYAxis1.Name = "m_spectrumYAxis1";
            this.m_spectrumYAxis1.Size = new System.Drawing.Size(30, 240);
            this.m_spectrumYAxis1.TabIndex = 0;
            // 
            // imageListProbLarge
            // 
            this.imageListProbLarge.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListProbLarge.ImageStream")));
            this.imageListProbLarge.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListProbLarge.Images.SetKeyName(0, "");
            this.imageListProbLarge.Images.SetKeyName(1, "");
            this.imageListProbLarge.Images.SetKeyName(2, "");
            this.imageListProbLarge.Images.SetKeyName(3, "");
            // 
            // imageListProbSmall
            // 
            this.imageListProbSmall.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListProbSmall.ImageStream")));
            this.imageListProbSmall.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListProbSmall.Images.SetKeyName(0, "");
            this.imageListProbSmall.Images.SetKeyName(1, "");
            // 
            // BackGroundPanel
            // 
            this.BackGroundPanel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.BackGroundPanel.BorderColor = System.Drawing.Color.Black;
            this.BackGroundPanel.BorderSides = System.Windows.Forms.Border3DSide.Middle;
            this.BackGroundPanel.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.BackGroundPanel.Controls.Add(this.labelLastDrawnTime);
            this.BackGroundPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BackGroundPanel.Location = new System.Drawing.Point(0, 0);
            this.BackGroundPanel.Name = "BackGroundPanel";
            this.BackGroundPanel.Size = new System.Drawing.Size(808, 432);
            this.BackGroundPanel.TabIndex = 5;
            // 
            // labelLastDrawnTime
            // 
            this.labelLastDrawnTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelLastDrawnTime.AutoSize = true;
            this.labelLastDrawnTime.BackColor = System.Drawing.Color.Transparent;
            this.labelLastDrawnTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLastDrawnTime.ForeColor = System.Drawing.Color.Black;
            this.labelLastDrawnTime.Location = new System.Drawing.Point(563, 377);
            this.labelLastDrawnTime.Name = "labelLastDrawnTime";
            this.labelLastDrawnTime.Padding = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.labelLastDrawnTime.Size = new System.Drawing.Size(61, 15);
            this.labelLastDrawnTime.TabIndex = 4;
            this.labelLastDrawnTime.Text = "00:00:00";
            this.labelLastDrawnTime.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SpectrumGradient
            // 
            this.SpectrumGradient.BackColor = System.Drawing.Color.White;
            this.SpectrumGradient.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.SpectrumGradient.Image = ((System.Drawing.Image)(resources.GetObject("SpectrumGradient.Image")));
            this.SpectrumGradient.Location = new System.Drawing.Point(664, 32);
            this.SpectrumGradient.Name = "SpectrumGradient";
            this.SpectrumGradient.Size = new System.Drawing.Size(24, 360);
            this.SpectrumGradient.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.SpectrumGradient.TabIndex = 0;
            this.SpectrumGradient.TabStop = false;
            // 
            // m_graph
            // 
            this.m_graph.BackColor = System.Drawing.SystemColors.Desktop;
            this.m_graph.CursorsMode = Rimed.TCD.GUI.GlobalTypes.ECursorsMode.None;
            this.m_graph.DrawModeBckEnvelope = false;
            this.m_graph.DrawModeFwdEnvelope = false;
            this.m_graph.DrawPeakBckEnvelope = false;
            this.m_graph.DrawPeakFwdEnvelope = true;
            this.m_graph.DrawSpectrum = true;
            this.m_graph.JustGotFocus = false;
            this.m_graph.LastDrawTime = System.TimeSpan.Parse("00:00:00");
            this.m_graph.Location = new System.Drawing.Point(88, 72);
            this.m_graph.MasterYValue = 0D;
            this.m_graph.Name = "m_graph";
            this.m_graph.OverridenSpectrumImage = null;
            this.m_graph.SecondYValue = 0D;
            this.m_graph.Size = new System.Drawing.Size(536, 304);
            this.m_graph.SpeedFactor = 1;
            this.m_graph.TabIndex = 0;
            this.m_graph.YAxis = this.m_spectrumYAxis1;
            this.m_graph.ZeroLinePercent = 50;
            // 
            // m_clinicalBar1
            // 
            this.m_clinicalBar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(235)))), ((int)(((byte)(250)))));
            this.m_clinicalBar1.ClinicalParamLabelAverageLower = 0D;
            this.m_clinicalBar1.ClinicalParamLabelAverageUpper = 0D;
            this.m_clinicalBar1.ClinicalParamLabelExt1Lower = 0D;
            this.m_clinicalBar1.ClinicalParamLabelExt1Upper = 0D;
            this.m_clinicalBar1.ClinicalParamLabelExt2Lower = 0D;
            this.m_clinicalBar1.ClinicalParamLabelExt2Upper = 0D;
            this.m_clinicalBar1.ClinicalParamLabelExt3Lower = 0D;
            this.m_clinicalBar1.ClinicalParamLabelExt3Upper = 0D;
            this.m_clinicalBar1.ClinicalParamLabelExt4Lower = 0D;
            this.m_clinicalBar1.ClinicalParamLabelExt4Upper = 0D;
            this.m_clinicalBar1.ClinicalParamLabelExt5Lower = 0D;
            this.m_clinicalBar1.ClinicalParamLabelExt5Upper = 0D;
            this.m_clinicalBar1.ClinicalParamLabelExt6Lower = 0D;
            this.m_clinicalBar1.ClinicalParamLabelExt6Upper = 0D;
            this.m_clinicalBar1.ClinicalParamLabelExt7Lower = 0D;
            this.m_clinicalBar1.ClinicalParamLabelExt7Upper = 0D;
            this.m_clinicalBar1.ClinicalParamLabelExt8Lower = 0D;
            this.m_clinicalBar1.ClinicalParamLabelExt8Upper = 0D;
            this.m_clinicalBar1.ClinicalParamLabelHitsLower = 0D;
            this.m_clinicalBar1.ClinicalParamLabelHitsRateLower = 0D;
            this.m_clinicalBar1.ClinicalParamLabelHitsRateUpper = 0D;
            this.m_clinicalBar1.ClinicalParamLabelHitsUpper = 0D;
            this.m_clinicalBar1.ClinicalParamLabelModeLower = 0D;
            this.m_clinicalBar1.ClinicalParamLabelModeUpper = 0D;
            this.m_clinicalBar1.ClinicalParamLabelSwLower = 0D;
            this.m_clinicalBar1.ClinicalParamLabelSwUpper = 0D;
            this.m_clinicalBar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.m_clinicalBar1.Location = new System.Drawing.Point(704, 0);
            this.m_clinicalBar1.MinimumSize = new System.Drawing.Size(5, 5);
            this.m_clinicalBar1.Name = "m_clinicalBar1";
            this.m_clinicalBar1.SetupFlowToShow = ((Rimed.TCD.GUI.GlobalTypes.EFFlowToShow)((Rimed.TCD.GUI.GlobalTypes.EFFlowToShow.ShowTop | Rimed.TCD.GUI.GlobalTypes.EFFlowToShow.ShowBottom)));
            this.m_clinicalBar1.Size = new System.Drawing.Size(104, 432);
            this.m_clinicalBar1.TabIndex = 8;
            this.m_clinicalBar1.UseEnvelopesForRcpDisplay = false;
            // 
            // SpectrumChart
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.Controls.Add(this.m_graph);
            this.Controls.Add(this.SpectrumGradient);
            this.Controls.Add(this.m_clinicalBar1);
            this.Controls.Add(this.gradientPanelCM);
            this.Controls.Add(this.BackGroundPanel);
            this.Name = "SpectrumChart";
            this.Size = new System.Drawing.Size(808, 432);
            this.SizeChanged += new System.EventHandler(this.spectrumChart_SizeChanged);
            this.gradientPanelCM.ResumeLayout(false);
            this.BackGroundPanel.ResumeLayout(false);
            this.BackGroundPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SpectrumGradient)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Syncfusion.Windows.Forms.Tools.GradientPanel gradientPanelCM;
        private System.Windows.Forms.Button bUnits;
        private System.ComponentModel.IContainer components;
        private System.Windows.Forms.Label probImage;
        private System.Windows.Forms.ImageList imageListProbLarge;
        private System.Windows.Forms.ImageList imageListProbSmall;
        private Syncfusion.Windows.Forms.Tools.GradientPanel BackGroundPanel;
        private System.Windows.Forms.PictureBox SpectrumGradient;
		private SpectrumGraph m_graph;
		private ClinicalBar m_clinicalBar1;
		private SpectrumYAxis m_spectrumYAxis1;
        //private readonly String[] m_unitsText = new String[] { "CM/S", "KHz" };
        private System.Windows.Forms.Label labelLastDrawnTime;
    }
}