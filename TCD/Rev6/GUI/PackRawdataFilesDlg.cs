using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Rimed.Framework.Common;
using Rimed.Framework.Threads;
using Rimed.TCD.Utils;

namespace Rimed.TCD.GUI
{
    public partial class PackRawdataFilesDlg : Form
    {
        #region Private Fields

        private readonly string m_backupDir = null;
        private readonly FileInfo[] m_fileInfo;
        private Thread m_arcThread = null;

        #endregion Private Fields

        public PackRawdataFilesDlg()
        {
            InitializeComponent();
            
            BackColor = GlobalSettings.BackgroundDlg;

            var strRes = MainForm.StringManager;

            lbInfo.Text = strRes.GetString("Archiving raw data files. Please wait...");
			Text = strRes.GetString("Raw data Archiving");
        }

		 public PackRawdataFilesDlg(string backupDir, List<string> arcFilesList): this()
        {
            m_backupDir = backupDir;

			 m_fileInfo = new FileInfo[arcFilesList.Count];
			for (var i = 0; i < arcFilesList.Count; i++)
			{
				try
				{
					m_fileInfo[i] = new FileInfo(arcFilesList[i]);
				}
				catch (Exception ex)
				{
					Logger.LogError(ex, string.Format("Fail to load '{0}' file information", arcFilesList[i]));
				}
			}
		}

        private void ZipRawdataFilesDlg_Load(object sender, System.EventArgs e)
        {
            if (m_fileInfo == null)
                return;
            
            progressBarAdv1.Value = 0;

            m_arcThread        = new Thread( archiveThreadStart);
            m_arcThread.IsBackground = true;
            m_arcThread.SetName("TCD.Pack");
            m_arcThread.Priority = ThreadPriority.Normal;

            var x = Handle; //Ofer, v1.18.2.9: Force handle creation. Fix System.InvalidOperationException. Message: Invoke or BeginInvoke cannot be called on a control until the window handle has been created.
            m_arcThread.Start();
        }

        private void archiveThreadStart()
        {
			Logger.LogInfo("{0}.archiveThreadStart(): START.", Name);
			var finishedFilesNum = 0;

            Thread.Sleep(100);

            try
            {
	            var startTime = DateTime.UtcNow;
				for (int i = 0; i < m_fileInfo.Length; i++)
				{
					if (m_abortBackup)
						break;

					string label;
					string srcFile;
					string archiveFile;

					if (m_fileInfo[i] == null)
						continue;

					if (!string.IsNullOrWhiteSpace(m_backupDir))
					{
						if (m_fileInfo[i].Directory == null)
							continue;

						label		= m_fileInfo[i].Directory.Name;
						srcFile		= m_fileInfo[i].FullName;
						archiveFile = m_backupDir + @"\" + m_fileInfo[i].Directory.Name + @"\" + m_fileInfo[i].Name + Constants.ARCHIVE_FILE_7Z_EXTENTION;
					}
					else
					{
						label		= Constants.RAW_DATA_PATH;
						srcFile		= Constants.RAW_DATA_PATH + m_fileInfo[i].Name;
						archiveFile = srcFile + Constants.ARCHIVE_FILE_7Z_EXTENTION;
					}

					label += "\\" + m_fileInfo[i].Name + " (" + finishedFilesNum + "/" + m_fileInfo.Length + ")";
					progressBarAdv1.BeginInvoke((Action)delegate() { labelFile.Text = label; });

					SevenZip.Pack(srcFile, archiveFile);

					finishedFilesNum++;

					var elapseSec	 = DateTime.UtcNow.Subtract(startTime).TotalMilliseconds;
					var estimatedSec = (finishedFilesNum == 0) ? 0 : (elapseSec * m_fileInfo.Length / finishedFilesNum);
					var remainingSec = (finishedFilesNum == 0) ? 0 : estimatedSec - elapseSec;

					progressBarAdv1.BeginInvoke((Action)delegate()
						{
							progressBarAdv1.Value			= (finishedFilesNum * 100) / m_fileInfo.Length;
							labelEstimatedTimeValue.Text	= new TimeSpan(0, 0, 0, 0, (int) estimatedSec).ToString(@"hh\:mm\:ss");
							labelElapseTimeValue.Text		= new TimeSpan(0, 0, 0, 0, (int)elapseSec).ToString(@"hh\:mm\:ss");		
							labelRemainingTimeValue.Text	= new TimeSpan(0, 0, 0, 0, (int)remainingSec).ToString(@"hh\:mm\:ss");	
						});
				}

                BeginInvoke((Action)delegate(){progressBarAdv1.Value = 100;});

				if (m_abortBackup)
				{
					Logger.LogInfo(string.Format("Raw data files archiving aborted by the user. Target folder: '{0}'.", m_backupDir));
					DialogResult = DialogResult.Abort;
				}
				else
					DialogResult = DialogResult.OK;
			}
            catch (ThreadAbortException)
            {
                Logger.LogWarning(string.Format("Raw data files archiving ABORTED. Target folder: '{0}'.", m_backupDir));
				Logger.LogInfo("{0}.archiveThreadStart(): ThreadAbortException.", Name);
				DialogResult = DialogResult.Abort;
            }
            catch (Exception ex)
            {
                Logger.LogError(string.Format("Raw data files archiving error. Target folder: '{0}'.", m_backupDir), ex);
                DialogResult = DialogResult.Cancel;
            }

            BeginInvoke((Action)Close);

			Logger.LogInfo("{0}.archiveThreadStart(): END.", Name);
		}

	    private bool m_abortBackup;
		private void buttonAbort_Click(object sender, EventArgs e)
		{
			var res = LoggedDialog.ShowQuestion(this, "Ary you sure you want to abort back operation?");
			m_abortBackup = (res == DialogResult.Yes);
		}

		private void deleteArchivedFiles()
		{
			if (m_fileInfo.Length == 0)
				return;

			Logger.LogWarning("Deleting archived raw data files.");
			for (int i = 0; i < m_fileInfo.Length; i++)
			{
				if (m_fileInfo[i] == null)
					continue;

				var path = Constants.RAW_DATA_PATH;
				if (!string.IsNullOrWhiteSpace(m_backupDir))
				{
					if (m_fileInfo[i].Directory == null)
						continue;

					path = m_backupDir + @"\" + m_fileInfo[i].Directory.Name + @"\";	
				}

				var file = Path.Combine(path, m_fileInfo[i].Name + Constants.ARCHIVE_FILE_7Z_EXTENTION);

				Files.Delete(file);
			}

		}

		private void ZipRawdataFilesDlg_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (DialogResult != DialogResult.OK)
				deleteArchivedFiles();
		}
    }
}
