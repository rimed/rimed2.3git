using System;
using System.Drawing;
using System.Windows.Forms;

using Rimed.TCD.Utils;

namespace Rimed.TCD.GUI
{
    public partial class ProbesCombobox : ComboBox
    {
        private class ProbesComboboxItem
        {
            public Color ProbeColor { get; private set; }

            public String ProbeName { get; private set; }

            public Guid Index { get; private set; }

            public ProbesComboboxItem(Color probeColor, String probeName, Guid index)
            {
                ProbeColor = probeColor;
                ProbeName = probeName;
                Index = index;
            }
        }

        private bool m_initFlag = false;

        /// <summary>Constructor</summary>
        public ProbesCombobox()
        {
            DrawMode = DrawMode.OwnerDrawFixed;
            DropDownStyle = ComboBoxStyle.DropDownList;
        }

        public Guid SelectedItemIndex
        {
            get
            {
                if (DesignMode)
                    return Guid.Empty;

                var item = (ProbesComboboxItem)SelectedItem;
                return item.Index;
            }
        }

        #region Events and delegates

        public delegate void ProbeChangedDelegate(Guid probeIndex, object sender, int index);

        public event ProbeChangedDelegate ProbeChangedEvent;

        private delegate void TurnActivativationDelegate(bool offline);

        #endregion Events and delegates

        #region Public Methods

        public void AddItem(Color rgb, String text, Guid index)
        {
            if (!m_initFlag)
            {
                m_initFlag = true;
                init();
            }

            var item = new ProbesComboboxItem(rgb, text, index);
            Items.Add(item);
            SelectedIndex = 0;
        }

        public void ChangeCurrentProbe()
        {
            if (SelectedIndex >= (Items.Count - 1))
                SelectedIndex = 0;
            else
                SelectedIndex++;
        }

        #endregion Public Methods

        private void init()
        {
            BVListViewCtrl.Instance.BVChangedEvent += new BVChangedDelegate(ProbesCombobox_BVChanged);
            SelectedIndexChanged += new System.EventHandler(this.ProbesCombobox_SelectedIndexChanged);
            SelectionChangeCommitted += new EventHandler(ProbesCombobox_SelectionChangeCommitted);
        }

        private void fireProbeChangedEvent(Guid index)
        {
            if (MainForm.Instance.IsUnilateralStudy && ProbeChangedEvent != null)
                ProbeChangedEvent(index, this, 0);
        }

        protected override void OnDrawItem(DrawItemEventArgs e)
        {
            e.DrawBackground();
            e.DrawFocusRectangle();

            if (e.Index == -1)
                return;

			var item = Items[e.Index] as ProbesComboboxItem;
	        if (item == null)
		        return;

            using (var rectColor = new SolidBrush(item.ProbeColor))
            {
	            e.Graphics.FillRectangle(rectColor, e.Bounds.Left + 1, e.Bounds.Y + 1, e.Bounds.Height - 2, e.Bounds.Height - 2);
            }
			
            using (var rectBorder = new Pen(e.ForeColor))
            {
	            e.Graphics.DrawRectangle(rectBorder, e.Bounds.Left + 1, e.Bounds.Y + 1, e.Bounds.Height - 2, e.Bounds.Height - 2);
            }

            // Create point for upper-left corner of drawing.
            float x = e.Bounds.Left + e.Bounds.Height + 3;
            float y = e.Bounds.Y;

            // Set format of String.
            var drawFormat = new StringFormat();
            drawFormat.FormatFlags = StringFormatFlags.DirectionVertical;

			using (var drawBrush = new SolidBrush(e.ForeColor))
			{
				// Draw String to screen.
				e.Graphics.DrawString(item.ProbeName, e.Font, drawBrush, x, y);
			}

            base.OnDrawItem(e);
        }

        #region Event Handlers

        private void ProbesCombobox_BVChanged(BV[] bvArr, object sender)
        {
            if (!(sender is ListView || sender == this))
                return;

            var bv = bvArr[0];
			var selItem = SelectedItem as ProbesComboboxItem;
            if (selItem == null || selItem.Index == bv.BVProbe.id)
                return;

            for (var i = 0; i < Items.Count; i++)
            {
				var item = Items[i] as ProbesComboboxItem;
				if (item != null && item.Index == bv.BVProbe.id)
                {
                    SelectedItem = Items[i];
                    return;
                }
            }
        }

        private void ProbesCombobox_SelectedIndexChanged(object sender, System.EventArgs e)
        {
			var item = SelectedItem as ProbesComboboxItem;

			if (item != null)
				fireProbeChangedEvent(item.Index);
        }

        private void ProbesCombobox_SelectionChangeCommitted(object sender, System.EventArgs e)
        {
            var combo	= sender as ProbesCombobox;
			var item	= SelectedItem as ProbesComboboxItem;
	        if (combo == null || item == null)
		        return;

            LoggerUserActions.ComboBoxSelectedValueChanged(combo.Name, combo.Parent.Name, item.ProbeName);
        }

        public void ProbesCombobox_TurnActivativation(bool offline)
        {
            if (InvokeRequired)
            {
                BeginInvoke((TurnActivativationDelegate)ProbesCombobox_TurnActivativation, offline);
                return;
            }

            Enabled = offline;
            Visible = offline;
        }

        #endregion Event Handlers
    }
}
