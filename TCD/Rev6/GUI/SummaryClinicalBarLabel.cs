using System;
using System.Drawing;
using System.Windows.Forms;

namespace Rimed.TCD.GUI
{
	/// <summary>Summary Clinical Parameter label</summary>
	public class SummaryClinicalBarLabel
	{
		/// <summary>Param name label</summary>
		public Label Name { get; private set; }

		/// <summary>Param top value label</summary>
		public Label Top { get; private set; }

		/// <summary>Param bottom value label</summary>
		public Label Bottom { get; private set; }

		/// <summary>Param identifier label</summary>
		public String Id { get; private set; }

		public SummaryClinicalBarLabel(Label nameLabel, Label topLabel, Label bottomLabel, String id)
		{
			Name = nameLabel;
			Top = topLabel;
			Bottom = bottomLabel;
			Id = id;
			nameLabel.Font = GlobalSettings.FONT_09_ARIAL_BOLD;//GlobalSettings.FONT_11_ARIAL_BOLD;
			topLabel.Font = GlobalSettings.FONT_11_ARIAL_BOLD;//GlobalSettings.FONT_12_ARIAL;
			bottomLabel.Font = GlobalSettings.FONT_11_ARIAL_BOLD;//GlobalSettings.FONT_12_ARIAL;
		}

		/// <summary>Name label location</summary>
		public Point NameLocation
		{
			set
			{
				Name.Location = value;
			}
		}

		/// <summary>Top value location</summary>
		public Point TopLocation
		{
			set
			{
				Top.Location = value;
			}
		}

		/// <summary>Bottom value location</summary>
		public Point BottomLocation
		{
			set
			{
				Bottom.Location = value;
			}
		}

		/// <summary>Visible</summary>
		public bool Visible
		{
			set
			{
				Name.Visible = value;
				ShowValues = value;
				//Top.Visible = value;
				//Bottom.Visible = value;
			}
		}

		public bool ShowValues
		{
			set
			{
				Top.Visible = value;
				Bottom.Visible = value;
			}
		}
	}
}