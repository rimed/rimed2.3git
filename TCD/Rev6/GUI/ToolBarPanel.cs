using System;
using System.Threading;
using System.Windows.Forms;
using Rimed.TCD.Utils;

namespace Rimed.TCD.GUI
{
    /// <summary>
    /// The main tool bar of the application
    /// Holds the most important buttons/functions for the user.
    /// The buttons on the bar will change according to the study type
    /// There are several other elements on the bar, like the Probes combobox and the
    /// Analog Gain combobox
    /// </summary>
    public partial class ToolBarPanel : UserControl
    {
	    private const int TOOLBAR_CTRLS_LAYOUT_GAP = 2;

		#region Private Fields
        /// <summary>A Jagged-array of ToolBarButtonDef. It's an array of "arrays of toolbar buttons". The first indexer is the toolbar index, the 2nd is the index of the button in the toolbar.</summary>
        private readonly ToolBarBtn[][] m_toolBarBtnArr;

	    //private System.Threading.Timer m_dicomEnableTimer;
        private System.Windows.Forms.Timer timer1;

        private GlobalTypes.ETooolBarType m_currentToolBarType = (GlobalTypes.ETooolBarType)(-1);
        #endregion Private Fields

        #region Control Properties

        public PictureBox ButtonPlay { get { return buttonPlay.Button; } }
        public ToolBarBtn ButtonPause { get { return buttonPause; } }
        public PictureBox ButtonStopRecording { get { return buttonStopRecording.Button; } }
        public ToolBarBtn ButtonStartRecording { get { return buttonStartRecording; } }
        public PictureBox ButtonAddTrend { get { return buttonAddTrend.Button; } }
        //public PictureBox ButtonSendTo { get { return buttonSendTo.Button; } }
        public PictureBox ButtonSave { get { return buttonSave.Button; } }
        public ToolBarBtn ButtonAutoscan { get { return buttonAutoscan; } }
        public PictureBox ButtonAddSpectrum { get { return buttonAddSpectrum.Button; } }
        public ToolBarBtn ButtonHitsDetection { get { return buttonHitsDetection; } }
        public PictureBox ButtonNextFunction { get { return buttonNextFunction.Button; } }
        public PictureBox ButtonSummary { get { return buttonSummary.Button; } }
		public PictureBox ButtonNotes { get { return buttonNotes.Button; } }
		public ToolBarBtn ButtonDicom { get { return buttonDicom; } }
        public ToolBarBtn ButtonWMV { get { return buttonWMV; } }
        public PictureBox ButtonScrollForward { get { return buttonScrollForward.Button; } }
        public PictureBox ButtonScrollBackward { get { return buttonScrollBackWard.Button; } }
        public ToolBarBtn ButtonCursors { get { return buttonCursors; } }
        public PictureBox ButtonPrint { get { return buttonPrint.Button; } }
        public PictureBox ButtonLoad { get { return buttonLoad.Button; } }
        public PictureBox ButtonNewPatient { get { return buttonNewPatient.Button; } }
        public PictureBox ButtonStudies { get { return buttonStudies.Button; } }
        public PictureBox ButtonClinicalParameters { get { return buttonClinicalParameters.Button; } }
        public PictureBox ButtonPatientRep { get { return buttonPatientRep.Button; } }
        public ToolBarBtn ButtonFreeze { get { return buttonFreeze; } }

        public ToolBarBtn ButtonReturn { get { return buttonReturn; } }
        public ToolBarBtn ButtonStopRecordingCtrl { get { return buttonStopRecording; } }
        public ToolBarBtn ButtonSaveCtrl { get { return buttonSave; } }
        public ToolBarBtn ButtonPlayCtrl { get { return buttonPlay; } }
        public ToolBarBtn ButtonScrollBackWardCtrl { get { return buttonScrollBackWard; } }

        public ComboBox ComboBoxTime { get { return comboBoxTime; } }
        public ProbesCombobox ComboBoxProbes { get { return comboBoxProbes; } }
        public Label TimeLabel { get { return timeLabel; } }
        public string TimeLabelText
        {
            set
            {
                if (InvokeRequired)
                    BeginInvoke((Action)delegate() {timeLabel.Text = value;});
                else
                    timeLabel.Text = value;
            }
        }
        //sergey - all this file
        public ComboBox ComboBoxSensitivity { get { return comboBoxSensitivity; } }
        public ComboBox ComboBoxSensitivitySecondary { get { return comboBoxSensitivitySecondary; } }

        #endregion Control Properties


        /// <summary>Constructor</summary>
        public ToolBarPanel()
        {
            ProbeLocation16Mhz = 0;
            InitializeComponent();
            
            // Added by Alex 29/12/2015 bug #820 v 2.2.3.18
            timer1 = new System.Windows.Forms.Timer {Interval = 500};
            timer1.Tick += timer1_Tick;

			//m_toolBarBtnArr = (AppSettings.Active.IsReviewStationMode ? getReviewStationModeToolBar() : getFullModeToolBar());
			m_toolBarBtnArr = getFullModeToolBar();

            // Each button has an enable/disable state that is based on the current toolbar, and if there is any unsaved data (dirty-falg)
            // This state data is stored as a multi denominational array, at the Tag field of the button.
            // 
            // Note: Buttons that are always enabled don't have this state-data at all!

			// Print button
            buttonPrint.Tag = new bool[][] {
				// TB1_DiagnosticUnfreeze
				new bool [] { true, true },
				// TB2_DiagnosticFreeze
				new bool [] { false, true },
				// TB3_Summary
				new bool [] { true, true },
				// TB4_MonitoringUnfreeze
				new bool [] { true, true },
				// TB5_MonitoringFreeze
				new bool [] { false, true },
				// TB6_Replay
				new bool [] { true, true },
				//monmon
				// TB7_MonitoringReplay
				new bool [] { true, true },
				// TB8_MonitoringPause
				new bool [] { false, true }
											};

            // Cursors button
            buttonCursors.Tag = new bool[][] {
				// TB1_DiagnosticUnfreeze
				new bool [] { true, true },
				// TB2_DiagnosticFreeze
				new bool [] { false, true },
				// TB3_Summary
				new bool [] { true, true },
				// TB4_MonitoringUnfreeze
				new bool [] { true, true },
				// TB5_MonitoringFreeze
				new bool [] { false, true },
				// TB6_Replay
				new bool [] { true, true },
				// TB7_MonitoringReplay
			    new bool [] { true, true },
				// TB8_MonitoringPause
				new bool [] { false, true }
											};

            // Notes button
            buttonNotes.Tag = new bool[][] {
				// TB1_DiagnosticUnfreeze
				new bool [] { true, true },
				// TB2_DiagnosticFreeze
				new bool [] { true, true },
				// TB3_Summary
				new bool [] { true, true },
				// TB4_MonitoringUnfreeze
				new bool [] { true, true },
				// TB5_MonitoringFreeze
				new bool [] { true, true },
				// TB6_Replay
				new bool [] { true, true },
				// TB7_MonitoringReplay
				new bool [] { true, true },
				// TB8_MonitoringPause
				new bool [] { true, true }
											  };

			// Dicom button
			buttonDicom.Tag = new bool[][] {
				// TB1_DiagnosticUnfreeze
				new bool [] { true, true },
				// TB2_DiagnosticFreeze
				new bool [] { true, true },
				// TB3_Summary
				new bool [] { true, true },
				// TB4_MonitoringUnfreeze
				new bool [] { true, true },
				// TB5_MonitoringFreeze
				new bool [] { true, true },
				// TB6_Replay
				new bool [] { true, true },
				// TB7_MonitoringReplay
				new bool [] { true, true },
				// TB8_MonitoringPause
				new bool [] { true, true },
				//
				new bool [] { true, true }
											  };

            // WMV button
            buttonWMV.Tag = new bool[][] {
				// TB1_DiagnosticUnfreeze
				new bool [] { true, true },
				// TB2_DiagnosticFreeze
				new bool [] { true, true },
				// TB3_Summary
				new bool [] { true, true },
				// TB4_MonitoringUnfreeze
				new bool [] { true, true },
				// TB5_MonitoringFreeze
				new bool [] { true, true },
				// TB6_Replay
				new bool [] { true, true },
				// TB7_MonitoringReplay
				new bool [] { true, true },
				// TB8_MonitoringPause
				new bool [] { true, true },
				//
				new bool [] { true, true }
											  };


			// Send-to button
            buttonSendTo.Tag = new bool[][] {
				// TB1_DiagnosticUnfreeze
				new bool [] { true, true },
				// TB2_DiagnosticFreeze
				new bool [] { true, true },
				// TB3_Summary
				new bool [] { false, true },
				// TB4_MonitoringUnfreeze
				new bool [] { true, true },
				// TB5_MonitoringFreeze
				new bool [] { false, true },
				// TB6_Replay
				new bool [] { true, true },
				// TB7_MonitoringReplay
				new bool [] { true, true },
				// TB8_MonitoringPause
				new bool [] { true, true }
											};

            // Save button
            buttonSave.Tag = new bool[][] {
				// TB1_DiagnosticUnfreeze
				new bool [] { true, true },
				// TB2_DiagnosticFreeze
				new bool [] { false, true },
				// TB3_Summary
				new bool [] { true, true },
				// TB4_MonitoringUnfreeze
				new bool [] { true, true },
				// TB5_MonitoringFreeze
				new bool [] { true, true },
				// TB6_Replay
				new bool [] { true, true },
				// TB7_MonitoringReplay
				new bool [] { true, true },
				// TB8_MonitoringPause
				new bool [] { true, true }
											};

            buttonDicom.Enabled = Rimed.TCD.DAL.RimedDal.Instance.AuthorizationMgrDS.tb_AuthorizationMgr[0].Dicom; // added by Alex bug #582 v.2.2.1.02 21/10/2014 
        }

		private ToolBarBtn[][] getFullModeToolBar()
		{
			var toolBar = new ToolBarBtn[][]
				{
					// TB1_DiagnosticUnfreeze
					new ToolBarBtn [] 
					{ 
						buttonFreeze,
						buttonPrint,
						buttonHitsDetection,
						buttonAddSpectrum,
						buttonAutoscan,
						buttonNextFunction,
						buttonDicom,
				    },

					// TB2_DiagnosticFreeze
					new ToolBarBtn [] 
					{ 
						buttonFreeze,
						buttonStudies,
						buttonNewPatient,
						buttonLoad,
						buttonPrint,
						buttonCursors,
						buttonNotes,
						buttonSummary,
						buttonNextFunction,
						buttonHitsDetection, 
						buttonAddSpectrum,
						buttonAutoscan, 
						buttonScrollBackWard,
						buttonScrollForward,
						buttonDicom,
					},

					// TB3_Summary
					new ToolBarBtn [] 
					{ 
						buttonStudies,
						buttonNewPatient,
						buttonLoad,
						buttonPrint,
						buttonNotes,
						buttonClinicalParameters,
						buttonReturn,
						buttonSave,
						buttonPatientRep,
						buttonDicom,
					},

					// TB4_MonitoringUnfreeze
					new ToolBarBtn [] 
					{ 
						buttonFreeze,
						buttonPrint,
						buttonHitsDetection, 
						buttonAddSpectrum,
						buttonAddTrend,
						buttonAutoscan,
						buttonStartRecording,
						buttonStopRecording,
						buttonPatientRep,// Added by Alex 21.12.2015 v. 2.2.3.17 feature # 670
						buttonDicom,
					},

					// TB5_MonitoringFreeze
					new ToolBarBtn [] 
					{ 
						buttonFreeze,
						buttonStudies,
						buttonNewPatient,
						buttonLoad,
						buttonCursors,
						buttonNotes,
						buttonHitsDetection, 
						buttonAddSpectrum,
						buttonAddTrend,
						buttonAutoscan,
						buttonStartRecording,
						buttonStopRecording,
						buttonPatientRep,// Added by Alex 21.12.2015 v. 2.2.3.17 feature # 670
						buttonDicom,
					},

					// TB6_ReplayFreeze
					new ToolBarBtn [] 
					{ 
						buttonPlay,
						buttonPause,
						buttonStopRecording,
						buttonStudies,
						buttonNewPatient,
						buttonLoad,
						buttonPrint,
						buttonCursors,
						buttonNotes,
						buttonSummary,
						buttonAutoscan,
						buttonDicom,
                        buttonWMV,
					},
					// TB7_Monitoring_Replay
					new ToolBarBtn [] 
					{ 
						buttonPlay,
						buttonScrollBackWard,
						buttonPause,
						buttonScrollForward,
						buttonStudies,
						buttonNewPatient,
						buttonLoad,
						buttonPrint,
						buttonCursors,
						buttonNotes, 
						buttonAddTrend,
						buttonAutoscan,
						buttonPatientRep,// Added by Alex 21.12.2015 v. 2.2.3.17 feature # 670
						buttonDicom,
                        buttonWMV,
    				},

				    // TB8_MonitoringPause
				    new ToolBarBtn [] 
				    { 
						buttonFreeze,
						buttonPrint,
						buttonHitsDetection, 
						buttonAddSpectrum,
						buttonAddTrend,
						buttonAutoscan,
						buttonCursors,
						buttonStartRecording,
						buttonStopRecording,
						buttonPause,
						buttonPatientRep,// Added by Alex 21.12.2015 v. 2.2.3.17 feature # 670
						buttonDicom,
				    },

				    // TB9_ReplayUnfreeze
				    new ToolBarBtn [] 
					{ 
						buttonPlay,
						buttonPause,
						buttonStopRecording,
						buttonDicom,
                        buttonWMV,
				    },

					// TB10_MonitoringIntracranialFreeze
					new ToolBarBtn [] 
					{ 
						buttonFreeze,
						buttonStudies,
						buttonNewPatient,
						buttonLoad,
						buttonCursors,
						buttonNotes,
						buttonHitsDetection, 
						buttonAddSpectrum,
						buttonAddTrend,
						buttonAutoscan,
						buttonStartRecording,
						buttonStopRecording,
                        buttonPatientRep,
						buttonDicom,
					},
					// TB11_MonitoringIntracranialReplay
					new ToolBarBtn [] 
					{ 
						buttonPlay,
						buttonScrollBackWard,
						buttonPause,
						buttonScrollForward,
						buttonStudies,
						buttonNewPatient,
                        buttonSave,
						buttonLoad,
						buttonPrint,
						buttonCursors,
						buttonNotes, 
						buttonAddTrend,
						buttonAutoscan,
                        buttonPatientRep,
						buttonDicom,
                        buttonWMV,
    				},
                };

			return toolBar;
		}

		private ToolBarBtn[][] getReviewStationModeToolBar()
		{
			var toolBar = new ToolBarBtn[][]
				{
					// TB1_DiagnosticUnfreeze
					new ToolBarBtn [] 
					{ 
					    buttonFreeze,
					    buttonPrint,
					    buttonHitsDetection,
					    buttonAddSpectrum,
					    buttonAutoscan,
					    buttonNextFunction,
					    buttonDicom,
					},

					// TB2_DiagnosticFreeze
					new ToolBarBtn [] 
					{ 
					    buttonFreeze,
					    buttonStudies,
					    buttonNewPatient,
					    buttonLoad,
					    buttonPrint,
					    buttonCursors,
					    buttonNotes,
					    buttonSummary,
					    buttonNextFunction,
					    buttonHitsDetection, 
					    buttonAddSpectrum,
					    buttonAutoscan, 
					    buttonScrollBackWard,
					    buttonScrollForward,
					    buttonDicom,
					},

					// TB3_Summary
					new ToolBarBtn [] 
					{ 
						buttonStudies,
						//buttonNewPatient,
						buttonLoad,
						buttonPrint,
						buttonNotes,
						buttonClinicalParameters,
						buttonReturn,
						buttonSave,
						buttonPatientRep,
						buttonDicom,
					},

					// TB4_MonitoringUnfreeze
					new ToolBarBtn [] 
					{ 
						buttonFreeze,
						buttonPrint,
						buttonHitsDetection, 
						buttonAddSpectrum,
						buttonAddTrend,
						buttonAutoscan,
						buttonStartRecording,
						buttonStopRecording,
						buttonDicom,
					},

					// TB5_MonitoringFreeze
					new ToolBarBtn [] 
					{ 
						buttonFreeze,
						buttonStudies,
						//buttonNewPatient,
						buttonLoad,
						buttonCursors,
						buttonNotes,
						buttonHitsDetection, 
						buttonAddSpectrum,
						buttonAddTrend,
						buttonAutoscan,
						buttonStartRecording,
						buttonStopRecording,
						buttonDicom,
					},

					// TB6_ReplayFreeze
					new ToolBarBtn [] 
					{ 
						buttonPlay,
						buttonPause,
						buttonStopRecording,
						buttonStudies,
						//buttonNewPatient,
						buttonLoad,
						buttonPrint,
						buttonCursors,
						buttonNotes,
						buttonSummary,
						buttonAutoscan,
						buttonDicom,
                        buttonWMV,
					},
					// TB7_Monitoring_Replay
					new ToolBarBtn [] 
					{ 
						buttonPlay,
						buttonScrollBackWard,
						buttonPause,
						buttonScrollForward,
						buttonStudies,
						//buttonNewPatient,
						buttonLoad,
						buttonPrint,
						buttonCursors,
						buttonNotes, 
						buttonAddTrend,
						buttonAutoscan,
						buttonDicom,
                        buttonWMV,
    				},

				    // TB8_MonitoringPause
				    new ToolBarBtn [] 
				    { 
						//buttonFreeze,
						buttonPrint,
						//buttonHitsDetection, 
						buttonAddSpectrum,
						buttonAddTrend,
						buttonAutoscan,
						buttonCursors,
						buttonStartRecording,
						buttonStopRecording,
						buttonPause,
						buttonDicom,
				    },

				    // TB9_ReplayUnfreeze
				    new ToolBarBtn [] 
					{ 
						buttonPlay,
						buttonPause,
						buttonStopRecording,
						buttonDicom,
                        buttonWMV,
				    },

					// TB10_MonitoringIntracranialFreeze
					new ToolBarBtn [] 
					{ 
						buttonFreeze,
						buttonStudies,
						buttonNewPatient,
						buttonLoad,
						buttonCursors,
						buttonNotes,
						buttonHitsDetection, 
						buttonAddSpectrum,
						buttonAddTrend,
						buttonAutoscan,
						buttonStartRecording,
						buttonStopRecording,
                        buttonPatientRep,
						buttonDicom,
					},
					// TB11_MonitoringIntracranialReplay
					new ToolBarBtn [] 
					{ 
						buttonPlay,
						buttonScrollBackWard,
						buttonPause,
						buttonScrollForward,
						buttonStudies,
						//buttonNewPatient,
                        buttonSave,
						buttonLoad,
						buttonPrint,
						buttonCursors,
						buttonNotes, 
						buttonAddTrend,
						buttonAutoscan,
                        buttonPatientRep,
						buttonDicom,
                        buttonWMV, 
    				},
                };

			return toolBar;
		}


		private void ToolBarPanel_Load(object sender, EventArgs e)
		{
			refreshLayout();
			timeLabel.BringToFront();
			comboBoxSensitivity.BringToFront();
			comboBoxSensitivitySecondary.BringToFront();
		}

		private void refreshLayout()
		{
			ButtonHitsDetection.Enabled = LayoutManager.IsHitDetectionAvailableForCurrentStudy;

			var left = panelButtons.Width;

			timeLabel.Left		= left - timeLabel.Width;
			if (timeLabel.Visible)
				left -= timeLabel.Width;

			left -= TOOLBAR_CTRLS_LAYOUT_GAP;

			comboBoxTime.Left = left - comboBoxTime.Width;
			if (comboBoxTime.Visible)
				left -= (comboBoxTime.Width + TOOLBAR_CTRLS_LAYOUT_GAP);

			comboBoxSensitivity.Left = left - comboBoxSensitivity.Width;
			if (comboBoxSensitivity.Visible)
				left -= (comboBoxSensitivity.Width + TOOLBAR_CTRLS_LAYOUT_GAP);

			comboBoxSensitivitySecondary.Left = left - comboBoxSensitivitySecondary.Width;
			if (comboBoxSensitivitySecondary.Visible)
				left -= (comboBoxSensitivitySecondary.Width + TOOLBAR_CTRLS_LAYOUT_GAP);

			comboBoxProbes.Left = left - comboBoxProbes.Width;
		}

        public int ProbeLocation16Mhz { get; private set; }

        public void ShowToolbar(GlobalTypes.ETooolBarType type)
        {
			// Disable method for new studies on DSP Off mode
			if (AppSettings.Active.IsDspOffMode)
				Enabled = MainForm.Instance.bvListViewCtrl1.ExaminationWasSaved;

            if (MainForm.Instance.CurrentStudyName.StartsWith(Constants.StudyType.STUDY_MONITORING_INTRACRANIAL))
            {
                if (type == GlobalTypes.ETooolBarType.TB5_MonitoringFreeze)
                    type = GlobalTypes.ETooolBarType.TB10_MonitoringIntracranialFreeze;
                else if (type == GlobalTypes.ETooolBarType.TB7_MonitoringReplay)
                    type = GlobalTypes.ETooolBarType.TB11_MonitoringIntracranialReplay;
            }

            if (m_currentToolBarType == type)
                return;

            m_currentToolBarType = type;

            SuspendLayout();
            panelButtons.SuspendLayout();

            for (int j = panelButtons.Controls.Count - 1; j >= 0; j--)
            {
                if (panelButtons.Controls[j] is ToolBarBtn)
                    panelButtons.Controls.Remove(panelButtons.Controls[j]);
            }

            for (int i = 0; i < m_toolBarBtnArr[(int)type].Length; i++)
            {
                // We need to revert the adding order
                var j = m_toolBarBtnArr[(int)type].Length - i - 1;

                //Set features invisible refers to the authorization manager.
                if ((!GlobalSettings.AMhits && m_toolBarBtnArr[(int)type][j].ToolTip == "HITS Detection")
                    || (!GlobalSettings.AMautoscan && m_toolBarBtnArr[(int)type][j].ToolTip == "M-Mode"))
                    continue;

                panelButtons.Controls.Add(m_toolBarBtnArr[(int)type][j]);
                if (m_toolBarBtnArr[(int) type][j].LeftDoc != -1)
                {
                    m_toolBarBtnArr[(int) type][j].LeftDoc = m_toolBarBtnArr[(int) type][j].LeftDoc;
                    m_toolBarBtnArr[(int) type][j].Dock = DockStyle.None;
                }
                else
                {
                    m_toolBarBtnArr[(int)type][j].Dock = DockStyle.Left;
                }
            }

            EnableButtons();

            ResumeLayout();
            panelButtons.ResumeLayout();
        }

        /// <summary>Enable / disable buttons</summary>
        public void EnableButtons()
        {
            if (InvokeRequired)
            {
				BeginInvoke((Action)EnableButtons);
                return;
            }

            var ii = (int)m_currentToolBarType;
            if (m_currentToolBarType == GlobalTypes.ETooolBarType.TB10_MonitoringIntracranialFreeze)
                ii = (int)GlobalTypes.ETooolBarType.TB5_MonitoringFreeze;
            else if (m_currentToolBarType == GlobalTypes.ETooolBarType.TB11_MonitoringIntracranialReplay)
                ii = (int)GlobalTypes.ETooolBarType.TB7_MonitoringReplay;

            foreach (Control c in panelButtons.Controls)
            {
                if ((c is ToolBarBtn) && (c.Tag != null))
                {
		            var stateData = (bool[][])c.Tag;
					var b = (ii < stateData.Length) && stateData[ii][Convert.ToInt32(LayoutManager.Instance.ExaminationDirtyFlag)];
					c.Enabled = b;
                }
            }

            if (MainForm.Instance != null)
            {
                if (Parent != null && Parent.Name != "SummaryScreen")
                {
                    if (MainForm.Instance.StudyType != GlobalTypes.EStudyType.Bilateral)
                    {
                        if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Replay)
                            comboBoxProbes.ProbesCombobox_TurnActivativation(false);
                        else    //REMOVE COMBO WHEN in ONLINE
                            comboBoxProbes.ProbesCombobox_TurnActivativation(LayoutManager.SystemMode == GlobalTypes.ESystemModes.Offline);
                    }
                }

				if (MainForm.Instance.CurrentStudyName == Constants.StudyType.STUDY_INTRAOPERATIVE)
                    comboBoxProbes.Enabled = false;
            }

            if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Monitoring)
                comboBoxProbes.Visible = false;
        }

        public void Init()
        {
            var i = 0;
			foreach (var p in ProbesCollection.Instance.Probes)
            {
                comboBoxProbes.AddItem(p.Color, p.Name, p.id);
                if (p.Name == "16Mhz PW")
                    ProbeLocation16Mhz = i;
                i++;
            }

            ReInit();
        }

        public void DisableButtonsInMonitoringOnlineMode()
        {
            enableButtonsInMonitoringOnlineMode(false);
        }

        public void EnableButtonsInMonitoringOnlineMode()
        {
            enableButtonsInMonitoringOnlineMode(true);
        }

        private delegate void EnableButtonsDelegate(bool enabled);

        private void enableButtonsInMonitoringOnlineMode(bool enabled)
        {
            if (InvokeRequired)
            {
                BeginInvoke((EnableButtonsDelegate)enableButtonsInMonitoringOnlineMode, enabled);
                return;
            }

            buttonStudies.Enabled = enabled;
            buttonNewPatient.Enabled = enabled;
            buttonLoad.Enabled = enabled;
        }

        public void ReInit()
        {
            if (InvokeRequired)
            {
                BeginInvoke((Action)ReInit);
                return;
            }

            switch (MainForm.Instance.StudyType)
            {
                case GlobalTypes.EStudyType.Bilateral:
						comboBoxProbes.Visible = false;
                    break;

                case GlobalTypes.EStudyType.Unilateral:
						if (LayoutManager.Instance.ReplayMode)
							comboBoxProbes.Visible = false;
						else if (LayoutManager.SystemLayoutMode != GlobalTypes.ESystemLayoutMode.MonitoringLoad && LayoutManager.SystemLayoutMode != GlobalTypes.ESystemLayoutMode.Monitoring)
							comboBoxProbes.Visible = true;
                    break;

                case GlobalTypes.EStudyType.Multifrequency:
						comboBoxProbes.Visible = true;
                    break;
            }
        }

		public void DisableDicomForPeriod(int periodMSec = 3000)
		{
			buttonDicom.DisableForPeriod(periodMSec);
		}

        // Added by Alex 29/12/2015 bug #820 v 2.2.3.19
        void timer1_Tick(object sender, EventArgs e)
        {
            ButtonWMV.ImageIndex = (ButtonWMV.ImageIndex == 85 ? 86 : 85);
        }

        void buttonWMV_OnPressed(object sender, EventArgs e)
        {
            if (buttonWMV.IsPressed)
                timer1.Start();
            else
            {
                timer1.Stop();
                ButtonWMV.ImageIndex = 85;
            }
        }


        #region Event Handlers

        private void comboBoxSensitivity_SelectionChangeCommitted(object sender, System.EventArgs e)
        {
            var combo = sender as ComboBox;
			if (combo != null)
				LoggerUserActions.ComboBoxSelectedValueChanged(combo.Name, combo.Parent.Name, combo.Text);
        }

        private void comboBoxSensitivity_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            MainForm.Instance.SensitivityChanged(0);
        }

        private void comboBoxSensitivitySecondary_SelectionChangeCommitted(object sender, System.EventArgs e)
        {
            var combo = sender as ComboBox;
			if (combo != null)
				LoggerUserActions.ComboBoxSelectedValueChanged(combo.Name, combo.Parent.Name, combo.Text);
        }

        private void comboBoxSensitivitySecondary_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            MainForm.Instance.SensitivityChanged(1);
        }

		private void timeLabel_VisibleChanged(object sender, EventArgs e)
		{
			refreshLayout();
		}

		private void comboBoxTime_VisibleChanged(object sender, EventArgs e)
		{
			refreshLayout();
		}

		private void comboBoxSensitivitySecondary_VisibleChanged(object sender, EventArgs e)
		{
			refreshLayout();
		}

		private void comboBoxSensitivity_VisibleChanged(object sender, EventArgs e)
		{
			refreshLayout();
		}

		private void comboBoxProbes_VisibleChanged(object sender, EventArgs e)
		{
			refreshLayout();
		}
		#endregion Event Handlers
	}
}
