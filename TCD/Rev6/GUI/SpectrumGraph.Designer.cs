﻿namespace Rimed.TCD.GUI
{
    partial class SpectrumGraph
    {
        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.SuspendLayout();
			// 
			// SpectrumGraph
			// 
			this.BackColor = System.Drawing.SystemColors.Desktop;
			this.Name = "SpectrumGraph";
			this.Size = new System.Drawing.Size(542, 286);
			this.SizeChanged += new System.EventHandler(this.spectrumGraph_SizeChanged);
			this.VisibleChanged += new System.EventHandler(this.spectrumGraph_VisibleChanged);
			this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.spectrumGraph_MouseDown);
			this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.spectrumGraph_MouseMove);
			this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.spectrumGraph_MouseUp);
			this.ResumeLayout(false);

        }

        #endregion

        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
    }
}