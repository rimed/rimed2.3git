using System.Collections;
using System.ComponentModel;
using System.Drawing;

namespace Rimed.TCD.GUI.Images
{
	public partial class ImageManager : Component
	{
		private Hashtable _images = new Hashtable();
		public static ImageManager Instance = new ImageManager();

		ImageManager()
		{
			InitializeComponent();

			_images.Add(ImageKey.DeletedRowData_16x16, imageListSmall.Images[1]);
			_images.Add(ImageKey.DeleteRowData_16x16, imageListSmall.Images[1]);
			_images.Add(ImageKey.SavedRowData_16x16, imageListSmall.Images[3]);
			_images.Add(ImageKey.SaveRowData_16x16, imageListSmall.Images[3]);

			_images.Add(ImageKey.DeletedRowData, imageListLarge.Images[1]);
			_images.Add(ImageKey.DeleteRowData, imageListLarge.Images[1]);
			_images.Add(ImageKey.SavedRowData, imageListLarge.Images[3]);
			_images.Add(ImageKey.SaveRowData, imageListLarge.Images[3]);
		}

		public Image GetImage(ImageKey key)
		{
			return _images[key] as Image;
		}
	}

	public enum ImageKey
	{
		DeletedRowData,
		DeleteRowData,

		SavedRowData,
		SaveRowData,

		DeletedRowData_16x16,
		DeleteRowData_16x16,

		SavedRowData_16x16,
		SaveRowData_16x16,
	}
}
