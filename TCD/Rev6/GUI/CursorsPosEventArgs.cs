using System;
using System.Drawing;

namespace Rimed.TCD.GUI
{
	public class CursorsPosEventArgs : EventArgs
	{
		private Size m_clientSize;
		
		public CursorsPosEventArgs(Size clientSize, double masterCursorPos, double secondCursorPos, GraphCursors.EDirection cursorDirection)
        {

            MasterCursorPos = masterCursorPos;
            SecondCursorPos = secondCursorPos;
            CursorDirection = cursorDirection;
			m_clientSize	= clientSize;
        }

        /// <summary>the time/position in the graph</summary>
		public double MasterCursorPos { get; private set; }
        
        public double SecondCursorPos { get; private set; }
        
        /// <summary>horizontal or vertical</summary>
        public GraphCursors.EDirection CursorDirection { get; private set; }

		public double MasterCursorRelativePos(Size size)
		{
			return calcRelativePosition(MasterCursorPos, size);
		}

		public double SecondCursorRelativePos(Size size)
		{
			return calcRelativePosition(SecondCursorPos, size);
		}

		private double calcRelativePosition(double position, Size size)
		{
			if (CursorDirection == GraphCursors.EDirection.HORIZONTAL)
				return position * size.Height / m_clientSize.Height;

			return position * size.Width / m_clientSize.Width;
		}

	}
}
