using System;
using System.Windows.Forms;
using Rimed.TCD.DAL;
using Rimed.Framework.Common;
using Rimed.TCD.Utils;

namespace Rimed.TCD.GUI
{
    /// <summary>GUI for configuration of patient report template.</summary>
    public partial class ReportWizard : System.Windows.Forms.Form
    {
        public ReportWizard(Form owner)
        {
//            System.Diagnostics.Trace.WriteLine(string.Format("Window handle: {0} created.", Handle));   // Ofer, Force creation of Window handle
	        Owner = owner;

            InitializeComponent();

            BackColor = GlobalSettings.BackgroundDlg;

            dsPatientRepLayout1.Merge(RimedDal.Instance.PatientRepLayoutsDS);

            //Added by Natalie for Simple Report
            rBtn1.Checked					= true;
	        checkBoxOnePageReport.Checked	= RimedDal.GeneralSettings.IsOnePageReport;
            rBtnNo.Checked					= true;
			switch (RimedDal.GeneralSettings.IpImagesPerPage)
			{
				case 1:
					rBtn1.Checked = true;
					break;
				case 2:
					rBtn2.Checked = true;
					break;
				case 4:
					rBtn4.Checked = true;
					break;
				default:
					rBtnNo.Checked = true;
					break;
			}

			//Microsoft.Win32.RegistryKey myKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Rimed", true);
			//if (myKey != null)
			//{
			//    if (myKey.GetValue("OnePageReport") != null)
			//    {
			//        if (Convert.ToBoolean(myKey.GetValue("OnePageReport")))
			//        {
			//            checkBoxOnePageReport.Checked = true;
			//        }
			//    }
			//    //RIMD-321: United report for DigiLite and IP
			//    rBtnNo.Checked = true;
			//    if (myKey.GetValue("IpImagesPerPage") != null)
			//    {
			//        switch (Convert.ToInt16(myKey.GetValue("IpImagesPerPage")))
			//        {
			//            case 1:
			//                rBtn1.Checked = true;
			//                break;
			//            case 2:
			//                rBtn2.Checked = true;
			//                break;
			//            case 4:
			//                rBtn4.Checked = true;
			//                break;
			//            default:
			//                rBtnNo.Checked = true;
			//                break;
			//        }
			//    }
			//}
        }

        private void wizardControl1_CancelButton_Click(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick("wizardControl1_CancelButton", Name);

			var res = LoggedDialog.ShowQuestion(this, MainForm.StringManager.GetString("Are you sure you want to cancel the wizard?"));
            if (res == DialogResult.Yes)
                Close();
        }

        private void wizardControl1_FinishButton_Click(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick("wizardControl1_FinishButton", Name);

			var res = LoggedDialog.ShowConfirmation(this, MainForm.StringManager.GetString("Are you sure you finished to customize your patient report?"));
            if (res == DialogResult.Yes)
            {
                var reportLayout = RimedDal.Instance.PatientRepLayoutsDS.tb_PatientRepLayout[0];

                if (checkBoxSummaryScreen.Checked)
                    reportLayout.Layout = (byte)(checkBoxTable.Checked ? 2 : 0);
                else
                    reportLayout.Layout = (byte)(checkBoxTable.Checked ? 1 : 3);

                reportLayout.PatientDetails     = (radioButtonFullPatientDetails.Checked ? 1 : 0);
                reportLayout.DateDisplay        = checkBoxDate.Checked;
                reportLayout.HospitalDetails    = checkBoxHospitalDetails.Checked;
                reportLayout.HospitalLogo       = checkBoxHospitalLogo.Checked;
                reportLayout.TitleDisplay       = checkBoxTitle.Checked;
                reportLayout.PatientHistory     = checkBoxHistory.Checked;
                reportLayout.ExaminationHistory = checkBoxExaminationHistory.Checked;
                reportLayout.Indication         = checkBoxIndication.Checked;
                reportLayout.Interpretation     = checkBoxInterpretation.Checked;
                reportLayout.Signature          = checkBoxSignature.Checked;

	            RimedDal.GeneralSettings.IsOnePageReport = checkBoxOnePageReport.Checked;
				if (rBtn1.Checked)
					RimedDal.GeneralSettings.IpImagesPerPage = 1;
				else if (rBtn2.Checked)
					RimedDal.GeneralSettings.IpImagesPerPage = 2;
				else if (rBtn4.Checked)
					RimedDal.GeneralSettings.IpImagesPerPage = 4;
				else
				{
					rBtnNo.Checked = true;
					RimedDal.GeneralSettings.IpImagesPerPage = 0;
				}

				////Added by Natalie - writing key for One Page Report to reg 
				//var myKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Rimed", true);
				//if (myKey == null)
				//    myKey = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(@"SOFTWARE\Rimed");

				//myKey.SetValue("OnePageReport", checkBoxOnePageReport.Checked);

				////RIMD-321: United report for DigiLite and IP
				////Ofer, v1.18.2.17: default value = rBtnNo.Checked = NO Images
				//if (rBtnNo.Checked)
				//    myKey.SetValue("IpImagesPerPage", 0);
				//else if (rBtn1.Checked)
				//        myKey.SetValue("IpImagesPerPage", 1);
				//else if (rBtn2.Checked)
				//    myKey.SetValue("IpImagesPerPage", 2);
				//else if (rBtn4.Checked)
				//    myKey.SetValue("IpImagesPerPage", 4);
				//else
				//{
				//    rBtnNo.Checked = true;
				//    myKey.SetValue("IpImagesPerPage", 0);
				//}
            }

			if (res != DialogResult.Cancel)
				Close();
        }

        private void loadData(object sender, System.EventArgs e)
        {
            // update controls that r not bind to the data Set.
            if (dsPatientRepLayout1.tb_PatientRepLayout[0].PatientDetails == 0)
            {
                radioButtonFullPatientDetails.Checked = false;
                radioButtonMinimalPatientDetails.Checked = true;
            }
            else
            {
                radioButtonFullPatientDetails.Checked = true;
                radioButtonMinimalPatientDetails.Checked = false;
            }

            checkBoxSummaryScreen.Checked = false;
            checkBoxTable.Checked = false;

            if (dsPatientRepLayout1.tb_PatientRepLayout[0].Layout == 0)
                checkBoxSummaryScreen.Checked = true;
            else if (dsPatientRepLayout1.tb_PatientRepLayout[0].Layout == 1)
                checkBoxTable.Checked = true;
            else if (dsPatientRepLayout1.tb_PatientRepLayout[0].Layout == 2)
            {
                checkBoxSummaryScreen.Checked = true;
                checkBoxTable.Checked = true;
            }

            checkBoxIndication.Checked = dsPatientRepLayout1.tb_PatientRepLayout[0].Indication;
            checkBoxInterpretation.Checked = dsPatientRepLayout1.tb_PatientRepLayout[0].Interpretation;
        }

        private void checkVisibility()
        {
            pictureBoxLogo.Visible              = checkBoxHospitalLogo.Checked;
            labelHospitalDetails.Visible        = checkBoxHospitalDetails.Checked;
            labelDate.Visible                   = checkBoxDate.Checked;
            labelTitle.Visible                  = checkBoxTitle.Checked;

            labelMinimalPatientDetails.Visible  = radioButtonMinimalPatientDetails.Checked;
            labelFullPatientDeails.Visible      = radioButtonFullPatientDetails.Checked;

            labelPatientHistory.Visible         = checkBoxHistory.Checked;
            labelExaminationHistory.Visible     = checkBoxExaminationHistory.Checked;
        }

        private void checkBoxHospitalLogo_CheckedChanged(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick(string.Format("checkBoxHospitalLogo_CheckedChanged({0})", ((CheckBox)sender).Checked), Name, null); //Ofer, v1.18.2.15

            pictureBoxLogo.Visible = ((CheckBox)sender).Checked;
        }

        private void checkBoxHospitalDetails_CheckedChanged(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick(string.Format("checkBoxHospitalDetails_CheckedChanged({0})", ((CheckBox)sender).Checked), Name, null); //Ofer, v1.18.2.15

            labelHospitalDetails.Visible = ((CheckBox)sender).Checked;
        }

        private void checkBoxDate_CheckedChanged(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick(string.Format("checkBoxDate_CheckedChanged({0})", ((CheckBox)sender).Checked), Name, null); //Ofer, v1.18.2.15

            labelDate.Visible = ((CheckBox)sender).Checked;
        }

        private void wizardControlPage2_VisibleChanged(object sender, System.EventArgs e)
        {
            checkVisibility();
        }

        private void checkBoxTitle_CheckedChanged(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick(string.Format("checkBoxTitle_CheckedChanged({0})", ((CheckBox)sender).Checked), Name, null); //Ofer, v1.18.2.15

            labelTitle.Visible = ((CheckBox)sender).Checked;
        }

        private void radioButtonFullPatientDetails_CheckedChanged(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick(string.Format("radioButtonFullPatientDetails_CheckedChanged({0})", ((RadioButton)sender).Checked), Name, null); //Ofer, v1.18.2.15

            labelFullPatientDeails.Visible = ((RadioButton)sender).Checked;
            labelMinimalPatientDetails.Visible = !((RadioButton)sender).Checked;
        }

        private void radioButtonMinimalPatientDetails_CheckedChanged(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick(string.Format("radioButtonMinimalPatientDetails_CheckedChanged({0})", ((RadioButton)sender).Checked), Name, null); //Ofer, v1.18.2.15

            labelFullPatientDeails.Visible = !((RadioButton)sender).Checked;
            labelMinimalPatientDetails.Visible = ((RadioButton)sender).Checked;
        }

        private void checkBoxHistory_CheckedChanged(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick(string.Format("checkBoxHistory_CheckedChanged({0})", ((CheckBox)sender).Checked), Name, null); //Ofer, v1.18.2.15

            labelPatientHistory.Visible = ((CheckBox)sender).Checked;
        }

        private void checkBoxExaminationHistory_CheckedChanged(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick(string.Format("checkBoxExaminationHistory_CheckedChanged({0})", ((CheckBox)sender).Checked), Name, null); //Ofer, v1.18.2.15

            labelExaminationHistory.Visible = ((CheckBox)sender).Checked;
        }

        private void checkBoxIndication_CheckedChanged(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick(string.Format("checkBoxIndication_CheckedChanged({0})", ((CheckBox)sender).Checked), Name, null); //Ofer, v1.18.2.15

            labelIndication.Visible = ((CheckBox)sender).Checked;
            enableIndicationInterpretation();
        }

        private void checkBoxInterpretation_CheckedChanged(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick(string.Format("checkBoxInterpretation_CheckedChanged({0})", ((CheckBox)sender).Checked), Name, null); //Ofer, v1.18.2.15

            labelInterpretation.Visible = ((CheckBox)sender).Checked;
            enableIndicationInterpretation();
        }

        private void enableIndicationInterpretation()
        {
            if (checkBoxOnePageReport.Checked)
            {
                if (checkBoxIndication.Checked && checkBoxInterpretation.Checked)
                    checkBoxInterpretation.Checked = false;
                else
                {
                    checkBoxIndication.Enabled = !checkBoxInterpretation.Checked;
                    checkBoxInterpretation.Enabled = !checkBoxIndication.Checked;
                }
            }
        }

        private void checkBoxSignature_CheckedChanged(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick(string.Format("checkBoxSignature_CheckedChanged({0})", ((CheckBox)sender).Checked), Name, null); //Ofer, v1.18.2.15

            labelSignature.Visible = ((CheckBox)sender).Checked;
        }

        private void checkBoxSummaryScreen_CheckedChanged(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick("checkBoxSummaryScreen_CheckedChanged", Name); //Ofer, v1.18.2.15

            updateImage();
        }

        private void checkBoxTable_CheckedChanged(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick("checkBoxTable_CheckedChanged", Name); //Ofer, v1.18.2.15

            updateImage();
        }

        private void wizardControl1_NextButton_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("wizardControl1_NextButton_Click", Name); //Ofer, v1.18.2.15
        }

        private void wizardControl1_BackButton_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("wizardControl1_BackButton_Click", Name); //Ofer, v1.18.2.15
        }

        private void wizardControl1_HelpButton_Click(object sender, EventArgs e)
        {
            LoggerUserActions.MouseClick("wizardControl1_HelpButton_Click", Name); //Ofer, v1.18.2.15
        }
        private void checkBoxOnePageReport_CheckedChanged(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick(string.Format("checkBoxOnePageReport_CheckedChanged({0})", ((CheckBox)sender).Checked), Name); //Ofer, v1.18.2.15

            bool isNotOnePageReport = !((CheckBox)sender).Checked;
            checkBoxHistory.Checked = isNotOnePageReport;
            this.dsPatientRepLayout1.tb_PatientRepLayout[0].PatientHistory = checkBoxHistory.Checked;

            checkBoxExaminationHistory.Checked = isNotOnePageReport;
            this.dsPatientRepLayout1.tb_PatientRepLayout[0].ExaminationHistory = checkBoxExaminationHistory.Checked;

            checkBoxSummaryScreen.Checked = isNotOnePageReport;

            checkBoxTable.Checked = true;
            checkBoxSignature.Checked = true;

            if (!isNotOnePageReport)
                radioButtonMinimalPatientDetails.Checked = true;

            checkBoxHistory.Enabled = isNotOnePageReport;
            checkBoxExaminationHistory.Enabled = isNotOnePageReport;
            checkBoxSummaryScreen.Enabled = isNotOnePageReport;
            if (isNotOnePageReport)
            {
                checkBoxIndication.Enabled = true;
                checkBoxInterpretation.Enabled = true;
            }
            else
                enableIndicationInterpretation();

            checkBoxTable.Enabled = isNotOnePageReport;
            checkBoxSignature.Enabled = isNotOnePageReport;
            panelIpImages.Enabled = isNotOnePageReport;
        }

        private void updateImage()
        {
            if (!checkBoxTable.Checked)
            {
                if (!checkBoxSummaryScreen.Checked)
                    labelExaminationDisplay.Image = imageList1.Images[3];	// none.
                else
                    labelExaminationDisplay.Image = imageList1.Images[0];	// summary.
            }
            else
            {
                if (!checkBoxSummaryScreen.Checked)
                    labelExaminationDisplay.Image = imageList1.Images[1];
                else
                    labelExaminationDisplay.Image = imageList1.Images[2];
            }
        }

        private void ReportWizard_Load(object sender, System.EventArgs e)
        {
            var strRes = MainForm.StringManager;

            lIpImages.Text = strRes.GetString("Manage DigiLite IP Images");
            rBtnNo.Text = strRes.GetString("0 images per page");
            rBtn1.Text = strRes.GetString("1 images per page");
            rBtn2.Text = strRes.GetString("2 images per page");
            rBtn4.Text = strRes.GetString("4 images per page");
            wizardControl1.BackButton.Text = "<< " + strRes.GetString("Back");
            wizardControl1.CancelButton.Text = strRes.GetString("Cancel");
            wizardControl1.FinishButton.Text = strRes.GetString("Finish");
            wizardControl1.HelpButton.Text = strRes.GetString("Help");
            wizardControl1.NextButton.Text = strRes.GetString("Next") + " >>";
            label1.Text = strRes.GetString("Examination Display") + " ";
            wizardControlPage1.Description = strRes.GetString("This is the description of the Wizard Page");
            wizardControlPage1.Title = strRes.GetString("Page Title");
            label7.Text = strRes.GetString("To continue, click Next");
            label5.Text = strRes.GetString("Welcome to the Report Generator Wizard.");
            wizardControlPage2.Title = strRes.GetString("Report Header");
            groupBox1.Text = strRes.GetString("Patient Details");
            radioButtonFullPatientDetails.Text = strRes.GetString("Full");
            radioButtonMinimalPatientDetails.Text = strRes.GetString("Minimal");
            labelTitle.Text = strRes.GetString("Rimed TCD Patient Report");
            checkBoxHospitalLogo.Text = strRes.GetString("Hospital Logo");
            checkBoxHospitalDetails.Text = strRes.GetString("Hospital Details");
            checkBoxDate.Text = strRes.GetString("Date Display");
            checkBoxTitle.Text = strRes.GetString("Title Display");
            checkBoxHistory.Text = strRes.GetString("Patient History");
            checkBoxExaminationHistory.Text = strRes.GetString("Examinations History");
            checkBoxOnePageReport.Text = strRes.GetString("One Page Report");
            wizardControlPage3.Description = strRes.GetString("This is the description of the Wizard Page");
            wizardControlPage3.Title = strRes.GetString("Examination Display") + " ";
            checkBoxSummaryScreen.Text = strRes.GetString("Summary Screen");
            checkBoxIncludsNormalValues.Text = strRes.GetString("Includes Normal Values");
            checkBoxTable.Text = strRes.GetString("Table Report");
            wizardControlPage2.Description = this.wizardControlPage4.Description = strRes.GetString("Here you can determine which details will be display in the report.");
            wizardControlPage4.Title = strRes.GetString("Report Footer");
            checkBoxIndication.Text = strRes.GetString("Indication");
            labelIndication.Text = strRes.GetString("Indication") + ":_________________________________________________________________________________________________________________________________________";
            labelInterpretation.Text = strRes.GetString("Interpretation") + ":_________________________________________________________________________________________________________________________________________";
            labelSignature.Text = strRes.GetString("Technologist") + ": ______________" + strRes.GetString("Interpreted by") + ": ____________";
            checkBoxInterpretation.Text = strRes.GetString("Interpretation");
            checkBoxSignature.Text = strRes.GetString("Signature");
            Text = strRes.GetString("Report Generator Wizard");

            loadData(null, null);
        }
    }
}
