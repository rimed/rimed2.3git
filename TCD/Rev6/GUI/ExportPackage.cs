using System;
using System.Collections.Generic;
using Rimed.TCD.DAL;

namespace Rimed.TCD.GUI
{
	/// <summary>Contains clinical parameters data for one TrendChart</summary>
	public class ExportPackage
	{
		/// <summary>Time of start recording (and clinical parameters saving)</summary>
		public DateTime StartTime { get; set; }

		/// <summary>Time interval for clinical parameters recording (at seconds)</summary>
		public int Interval {get; private set;}

        public List<ExportElement> Log { get; private set; }
        public List<string> Header { get; private set; }

		public ExportPackage()
		{
            Log		= new List<ExportElement>();
            Header	= new List<string>();

			Interval = RimedDal.GeneralSettings.ExportInterval;
			if (Interval <= 0)
			{
				RimedDal.GeneralSettings.ExportInterval	= GlobalSettings.VERY_SHORT_EXPORT_INTERVAL;
                Interval = GlobalSettings.VERY_SHORT_EXPORT_INTERVAL;
			}
		}
	}
}
