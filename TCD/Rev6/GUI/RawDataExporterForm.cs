﻿using System;
using System.Text;
using System.Windows.Forms;
using Rimed.TCD.DAL;
using Rimed.TCD.DSPData;
using Rimed.TCD.DspBlock;

namespace Rimed.TCD.GUI
{
	public partial class RawDataExporterForm : Form
	{
		public RawDataExporterForm()
		{
			InitializeComponent();
		}


		private void button1_Click(object sender, EventArgs e)
		{
			//exportData(@"C:\Rimed\SRC\TCD2003\DATA\RawData\4efb519d-cc7f-4714-8ad8-9e47d5e46c51");
			exportData(@"C:\Rimed\SRC\TCD2003\DATA\RawData\cf9a89ef-cf4f-4e9a-a542-0f69e7366764");
			exportData(@"C:\Rimed\SRC\TCD2003\DATA\RawData\85de9a0c-8adc-46ed-b18f-c72b6e8216a0");
		}

		private void exportData(string path)
		{
			var examData = new ExaminationRawData(path, DateTime.UtcNow);


			var ts = new TimeSpan(0, 0, 0, 0, examData.LastFrame.MSec);
			System.Diagnostics.Debug.WriteLine("Path={0}, Files={1}, Samples={2}, Start time={3}, Duration={4}"
											   , examData.PathRoot, examData.FilesCount, (examData.LastFrame.BufferIndex + 1) * DspBlockConst.NUM_OF_COLUMNS
											   , examData.StartTime.ToString("yyyy.MM.dd HH:mm:ss"), ts);

			const double RESOLUTION = 1.0;	//1sec
			var nextPrint = 0.0;
			var frame = examData.CurrentFrameData;
			while (frame != null && frame.Data != null)
			{
				if (!frame.Data.IsValid)
					continue;

				var gateIx = frame.Data.PC2DSPEcho.GetCardInfo().Gate2Hear;
				var clinicalParamGate = frame.Data.ClinicalParametersBlock.get_Gate(gateIx);
				var hitsGate = frame.Data.HitsData.GetHitsGate(gateIx);
				var envModeGate = frame.Data.FFTEnvelopesBlock.Mode.GetGate(gateIx);
				var envPeakGate = frame.Data.FFTEnvelopesBlock.Peak.GetGate(gateIx);


				//ProbesCollection.Instance.Probes[0].Range;
				var probInfo = frame.Data.PC2DSPEcho.GetProbeInfo(0);
				if (gateIx >= 8)
					probInfo = frame.Data.PC2DSPEcho.GetProbeInfo(1);

				var sb = new StringBuilder();
				var colIndex = frame.BufferIndex * DspBlockConst.NUM_OF_COLUMNS;

				for (var col = 0; col < DspBlockConst.NUM_OF_COLUMNS; col++)
				{
					var colMSec = colIndex * DspBlockConst.COLUMN_DURATION;

					if (colMSec < nextPrint)
						continue;

					nextPrint += RESOLUTION;

					var colTime = new TimeSpan(0, 0, 0, 0, (int)Math.Round(colMSec)).ToString(@"hh\:mm\:ss\.fff");

					sb.Clear();


					sb.Append(probInfo).Append(", ").Append("GateToHear=").Append(gateIx);

					sb.Append(", colIndex=").Append(colIndex);
					sb.Append(", colTime=").Append(colTime);
					sb.Append(", colMSec=").Append(colMSec);

					sb.Append(", Heart_Rate=").Append(frame.Data.ClinicalParametersBlock.Heart_Rate);
					sb.Append(", Flags=").Append(frame.Data.ClinicalParametersBlock.Flage);

					//Fwd
					sb.Append(", Fwd.Peak=").Append(clinicalParamGate.Peak.Forward);
					sb.Append(", Fwd.Mean=").Append(clinicalParamGate.Mean.Forward);
					sb.Append(", Fwd.Mode=").Append(clinicalParamGate.Mode.Forward);
					sb.Append(", Fwd.Average=").Append(clinicalParamGate.Average.Forward);
					sb.Append(", Fwd.DV=").Append(clinicalParamGate.DV.Forward);
					sb.Append(", Fwd.SW=").Append(clinicalParamGate.SW.Forward);

					sb.Append(", Fwd.Env.Peak=").Append(envPeakGate.GetColumn(col).Forward);
					sb.Append(", Fwd.Env.Mode=").Append(envModeGate.GetColumn(col).Forward);

					//Rev
					sb.Append(", Rev.Peak=").Append(clinicalParamGate.Peak.Reverse);
					sb.Append(", Rev.Mean=").Append(clinicalParamGate.Mean.Reverse);
					sb.Append(", Rev.Mode=").Append(clinicalParamGate.Mode.Reverse);
					sb.Append(", Rev.Average=").Append(clinicalParamGate.Average.Reverse);
					sb.Append(", Rev.DV=").Append(clinicalParamGate.DV.Reverse);
					sb.Append(", Rev.SW=").Append(clinicalParamGate.SW.Reverse);

					sb.Append(", Rev.Env.Peak=").Append(envPeakGate.GetColumn(col).Reverse);
					sb.Append(", Rev.Env.Mode=").Append(envModeGate.GetColumn(col).Reverse);

					//Ext. channel
					var exChannelCol = frame.Data.ExChannelsArr.GetColumn(col);
					for (byte exChannel = 0; exChannel < 8; exChannel++)
					{
						var val = RimedDal.Instance.CalculateChannelValue(exChannelCol.GetValue(exChannel), exChannel);
						sb.Append(", ExChannel[").Append(exChannel).Append("]=").Append(exChannelCol.GetValue(exChannel)).Append(" VS. ").Append(val);
					}

					//Hits
					var hitColumn = hitsGate.GetColumn(col);
					if (hitColumn.IsHitDetected)
					{
						sb.Append(", HitType=").Append(hitColumn.HitType);
						sb.Append(", Energy=").Append(hitColumn.Energy);
						sb.Append(", Duration=").Append(hitColumn.Duration);
					}
					else
						sb.Append(", , ,");

					System.Diagnostics.Debug.WriteLine(sb.ToString());

					colIndex++;
				}

				frame = examData.Next();
			}

			examData.Dispose();

		}


	}
}
