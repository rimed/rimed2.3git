namespace Rimed.TCD.GUI
{
    public partial class ToolBarPanel
    {
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToolBarPanel));
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.imageListToolBar = new System.Windows.Forms.ImageList(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelButtons = new System.Windows.Forms.Panel();
            this.timeLabel = new System.Windows.Forms.Label();
            this.comboBoxSensitivitySecondary = new System.Windows.Forms.ComboBox();
            this.comboBoxSensitivity = new System.Windows.Forms.ComboBox();
            this.comboBoxTime = new System.Windows.Forms.ComboBox();
            this.buttonScrollForward = new Rimed.TCD.GUI.ToolBarBtn();
            this.buttonScrollBackWard = new Rimed.TCD.GUI.ToolBarBtn();
            this.comboBoxProbes = new Rimed.TCD.GUI.ProbesCombobox();
            this.buttonPatientRep = new Rimed.TCD.GUI.ToolBarBtn();
            this.buttonPlay = new Rimed.TCD.GUI.ToolBarBtn();
            this.buttonFreeze = new Rimed.TCD.GUI.ToolBarBtn();
            this.buttonLoad = new Rimed.TCD.GUI.ToolBarBtn();
            this.buttonAddTrend = new Rimed.TCD.GUI.ToolBarBtn();
            this.buttonClinicalParameters = new Rimed.TCD.GUI.ToolBarBtn();
            this.buttonReturn = new Rimed.TCD.GUI.ToolBarBtn();
            this.buttonPause = new Rimed.TCD.GUI.ToolBarBtn();
            this.buttonStartRecording = new Rimed.TCD.GUI.ToolBarBtn();
            this.buttonSendTo = new Rimed.TCD.GUI.ToolBarBtn();
            this.buttonSave = new Rimed.TCD.GUI.ToolBarBtn();
            this.buttonAutoscan = new Rimed.TCD.GUI.ToolBarBtn();
            this.buttonAddSpectrum = new Rimed.TCD.GUI.ToolBarBtn();
            this.buttonHitsDetection = new Rimed.TCD.GUI.ToolBarBtn();
            this.buttonNextFunction = new Rimed.TCD.GUI.ToolBarBtn();
            this.buttonSummary = new Rimed.TCD.GUI.ToolBarBtn();
            this.buttonNotes = new Rimed.TCD.GUI.ToolBarBtn();
            this.buttonDicom = new Rimed.TCD.GUI.ToolBarBtn();
            this.buttonWMV = new Rimed.TCD.GUI.ToolBarBtn();
            this.buttonCursors = new Rimed.TCD.GUI.ToolBarBtn();
            this.buttonPrint = new Rimed.TCD.GUI.ToolBarBtn();
            this.buttonNewPatient = new Rimed.TCD.GUI.ToolBarBtn();
            this.buttonStudies = new Rimed.TCD.GUI.ToolBarBtn();
            this.autoLabel1 = new Syncfusion.Windows.Forms.Tools.AutoLabel();
            this.buttonStopRecording = new Rimed.TCD.GUI.ToolBarBtn();
            this.buttonTD = new Rimed.TCD.GUI.ToolBarBtn();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panelButtons.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox2
            // 
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Right;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(1390, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(110, 36);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(18, 36);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // imageListToolBar
            // 
            this.imageListToolBar.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListToolBar.ImageStream")));
            this.imageListToolBar.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListToolBar.Images.SetKeyName(0, "");
            this.imageListToolBar.Images.SetKeyName(1, "");
            this.imageListToolBar.Images.SetKeyName(2, "");
            this.imageListToolBar.Images.SetKeyName(3, "");
            this.imageListToolBar.Images.SetKeyName(4, "");
            this.imageListToolBar.Images.SetKeyName(5, "");
            this.imageListToolBar.Images.SetKeyName(6, "");
            this.imageListToolBar.Images.SetKeyName(7, "");
            this.imageListToolBar.Images.SetKeyName(8, "");
            this.imageListToolBar.Images.SetKeyName(9, "");
            this.imageListToolBar.Images.SetKeyName(10, "");
            this.imageListToolBar.Images.SetKeyName(11, "");
            this.imageListToolBar.Images.SetKeyName(12, "");
            this.imageListToolBar.Images.SetKeyName(13, "");
            this.imageListToolBar.Images.SetKeyName(14, "");
            this.imageListToolBar.Images.SetKeyName(15, "");
            this.imageListToolBar.Images.SetKeyName(16, "");
            this.imageListToolBar.Images.SetKeyName(17, "");
            this.imageListToolBar.Images.SetKeyName(18, "");
            this.imageListToolBar.Images.SetKeyName(19, "");
            this.imageListToolBar.Images.SetKeyName(20, "");
            this.imageListToolBar.Images.SetKeyName(21, "");
            this.imageListToolBar.Images.SetKeyName(22, "");
            this.imageListToolBar.Images.SetKeyName(23, "");
            this.imageListToolBar.Images.SetKeyName(24, "");
            this.imageListToolBar.Images.SetKeyName(25, "");
            this.imageListToolBar.Images.SetKeyName(26, "");
            this.imageListToolBar.Images.SetKeyName(27, "");
            this.imageListToolBar.Images.SetKeyName(28, "");
            this.imageListToolBar.Images.SetKeyName(29, "");
            this.imageListToolBar.Images.SetKeyName(30, "");
            this.imageListToolBar.Images.SetKeyName(31, "");
            this.imageListToolBar.Images.SetKeyName(32, "");
            this.imageListToolBar.Images.SetKeyName(33, "");
            this.imageListToolBar.Images.SetKeyName(34, "");
            this.imageListToolBar.Images.SetKeyName(35, "");
            this.imageListToolBar.Images.SetKeyName(36, "");
            this.imageListToolBar.Images.SetKeyName(37, "");
            this.imageListToolBar.Images.SetKeyName(38, "");
            this.imageListToolBar.Images.SetKeyName(39, "");
            this.imageListToolBar.Images.SetKeyName(40, "");
            this.imageListToolBar.Images.SetKeyName(41, "");
            this.imageListToolBar.Images.SetKeyName(42, "");
            this.imageListToolBar.Images.SetKeyName(43, "");
            this.imageListToolBar.Images.SetKeyName(44, "");
            this.imageListToolBar.Images.SetKeyName(45, "");
            this.imageListToolBar.Images.SetKeyName(46, "");
            this.imageListToolBar.Images.SetKeyName(47, "");
            this.imageListToolBar.Images.SetKeyName(48, "");
            this.imageListToolBar.Images.SetKeyName(49, "");
            this.imageListToolBar.Images.SetKeyName(50, "");
            this.imageListToolBar.Images.SetKeyName(51, "");
            this.imageListToolBar.Images.SetKeyName(52, "");
            this.imageListToolBar.Images.SetKeyName(53, "");
            this.imageListToolBar.Images.SetKeyName(54, "");
            this.imageListToolBar.Images.SetKeyName(55, "");
            this.imageListToolBar.Images.SetKeyName(56, "");
            this.imageListToolBar.Images.SetKeyName(57, "");
            this.imageListToolBar.Images.SetKeyName(58, "");
            this.imageListToolBar.Images.SetKeyName(59, "");
            this.imageListToolBar.Images.SetKeyName(60, "");
            this.imageListToolBar.Images.SetKeyName(61, "");
            this.imageListToolBar.Images.SetKeyName(62, "");
            this.imageListToolBar.Images.SetKeyName(63, "");
            this.imageListToolBar.Images.SetKeyName(64, "");
            this.imageListToolBar.Images.SetKeyName(65, "");
            this.imageListToolBar.Images.SetKeyName(66, "");
            this.imageListToolBar.Images.SetKeyName(67, "");
            this.imageListToolBar.Images.SetKeyName(68, "");
            this.imageListToolBar.Images.SetKeyName(69, "");
            this.imageListToolBar.Images.SetKeyName(70, "");
            this.imageListToolBar.Images.SetKeyName(71, "");
            this.imageListToolBar.Images.SetKeyName(72, "");
            this.imageListToolBar.Images.SetKeyName(73, "");
            this.imageListToolBar.Images.SetKeyName(74, "");
            this.imageListToolBar.Images.SetKeyName(75, "");
            this.imageListToolBar.Images.SetKeyName(76, "");
            this.imageListToolBar.Images.SetKeyName(77, "ICON_BACKWARD_BAR - Disabled.bmp");
            this.imageListToolBar.Images.SetKeyName(78, "");
            this.imageListToolBar.Images.SetKeyName(79, "");
            this.imageListToolBar.Images.SetKeyName(80, "ICON_FORWARD_BAR - Disabled.bmp");
            this.imageListToolBar.Images.SetKeyName(81, "IconBarButton_Dicom 01.bmp");
            this.imageListToolBar.Images.SetKeyName(82, "IconBarButton_Dicom 02 [Click].bmp");
            this.imageListToolBar.Images.SetKeyName(83, "IconBarButton_Dicom 03 [Disabled].bmp");
            this.imageListToolBar.Images.SetKeyName(84, "RecordWMV_REC.bmp");
            this.imageListToolBar.Images.SetKeyName(85, "RecordWMV_REC [Clik].bmp");
            this.imageListToolBar.Images.SetKeyName(86, "RecordWMV_REC [Disabled].bmp");
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.Controls.Add(this.panelButtons);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1500, 36);
            this.panel1.TabIndex = 50;
            // 
            // panelButtons
            // 
            this.panelButtons.Controls.Add(this.timeLabel);
            this.panelButtons.Controls.Add(this.comboBoxSensitivitySecondary);
            this.panelButtons.Controls.Add(this.comboBoxSensitivity);
            this.panelButtons.Controls.Add(this.comboBoxTime);
            this.panelButtons.Controls.Add(this.buttonScrollForward);
            this.panelButtons.Controls.Add(this.buttonScrollBackWard);
            this.panelButtons.Controls.Add(this.comboBoxProbes);
            this.panelButtons.Controls.Add(this.buttonPatientRep);
            this.panelButtons.Controls.Add(this.buttonPlay);
            this.panelButtons.Controls.Add(this.buttonFreeze);
            this.panelButtons.Controls.Add(this.buttonLoad);
            this.panelButtons.Controls.Add(this.buttonAddTrend);
            this.panelButtons.Controls.Add(this.buttonClinicalParameters);
            this.panelButtons.Controls.Add(this.buttonReturn);
            this.panelButtons.Controls.Add(this.buttonPause);
            this.panelButtons.Controls.Add(this.buttonStartRecording);
            this.panelButtons.Controls.Add(this.buttonSendTo);
            this.panelButtons.Controls.Add(this.buttonSave);
            this.panelButtons.Controls.Add(this.buttonAutoscan);
            this.panelButtons.Controls.Add(this.buttonAddSpectrum);
            this.panelButtons.Controls.Add(this.buttonHitsDetection);
            this.panelButtons.Controls.Add(this.buttonNextFunction);
            this.panelButtons.Controls.Add(this.buttonSummary);
            this.panelButtons.Controls.Add(this.buttonNotes);
            this.panelButtons.Controls.Add(this.buttonDicom);
            this.panelButtons.Controls.Add(this.buttonWMV);
            this.panelButtons.Controls.Add(this.buttonCursors);
            this.panelButtons.Controls.Add(this.buttonPrint);
            this.panelButtons.Controls.Add(this.buttonNewPatient);
            this.panelButtons.Controls.Add(this.buttonStudies);
            this.panelButtons.Controls.Add(this.autoLabel1);
            this.panelButtons.Controls.Add(this.buttonStopRecording);
            this.panelButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelButtons.Location = new System.Drawing.Point(18, 0);
            this.panelButtons.Name = "panelButtons";
            this.panelButtons.Size = new System.Drawing.Size(1372, 36);
            this.panelButtons.TabIndex = 51;
            // 
            // timeLabel
            // 
            this.timeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timeLabel.Location = new System.Drawing.Point(1296, 7);
            this.timeLabel.Name = "timeLabel";
            this.timeLabel.Size = new System.Drawing.Size(80, 23);
            this.timeLabel.TabIndex = 52;
            this.timeLabel.Text = "00:00:00";
            this.timeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.timeLabel.VisibleChanged += new System.EventHandler(this.timeLabel_VisibleChanged);
            // 
            // comboBoxSensitivitySecondary
            // 
            this.comboBoxSensitivitySecondary.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxSensitivitySecondary.BackColor = System.Drawing.Color.Yellow;
            this.comboBoxSensitivitySecondary.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSensitivitySecondary.ForeColor = System.Drawing.SystemColors.ControlText;
            this.comboBoxSensitivitySecondary.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.comboBoxSensitivitySecondary.Location = new System.Drawing.Point(1076, 7);
            this.comboBoxSensitivitySecondary.Name = "comboBoxSensitivitySecondary";
            this.comboBoxSensitivitySecondary.Size = new System.Drawing.Size(35, 21);
            this.comboBoxSensitivitySecondary.TabIndex = 35;
            this.comboBoxSensitivitySecondary.SelectedIndexChanged += new System.EventHandler(this.comboBoxSensitivitySecondary_SelectedIndexChanged);
            this.comboBoxSensitivitySecondary.SelectionChangeCommitted += new System.EventHandler(this.comboBoxSensitivitySecondary_SelectionChangeCommitted);
            this.comboBoxSensitivitySecondary.VisibleChanged += new System.EventHandler(this.comboBoxSensitivitySecondary_VisibleChanged);
            // 
            // comboBoxSensitivity
            // 
            this.comboBoxSensitivity.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxSensitivity.BackColor = System.Drawing.Color.Green;
            this.comboBoxSensitivity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSensitivity.ForeColor = System.Drawing.SystemColors.ControlText;
            this.comboBoxSensitivity.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.comboBoxSensitivity.Location = new System.Drawing.Point(1030, 7);
            this.comboBoxSensitivity.Name = "comboBoxSensitivity";
            this.comboBoxSensitivity.Size = new System.Drawing.Size(35, 21);
            this.comboBoxSensitivity.TabIndex = 34;
            this.comboBoxSensitivity.SelectedIndexChanged += new System.EventHandler(this.comboBoxSensitivity_SelectedIndexChanged);
            this.comboBoxSensitivity.SelectionChangeCommitted += new System.EventHandler(this.comboBoxSensitivity_SelectionChangeCommitted);
            this.comboBoxSensitivity.VisibleChanged += new System.EventHandler(this.comboBoxSensitivity_VisibleChanged);
            // 
            // comboBoxTime
            // 
            this.comboBoxTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxTime.BackColor = System.Drawing.SystemColors.Window;
            this.comboBoxTime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTime.ForeColor = System.Drawing.SystemColors.ControlText;
            this.comboBoxTime.Location = new System.Drawing.Point(1122, 7);
            this.comboBoxTime.Name = "comboBoxTime";
            this.comboBoxTime.Size = new System.Drawing.Size(42, 21);
            this.comboBoxTime.TabIndex = 0;
            this.comboBoxTime.VisibleChanged += new System.EventHandler(this.comboBoxTime_VisibleChanged);
            // 
            // buttonScrollForward
            // 
            this.buttonScrollForward.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonScrollForward.ImageIndex = 78;
            this.buttonScrollForward.Images = this.imageListToolBar;
            this.buttonScrollForward.Location = new System.Drawing.Point(1150, 0);
            this.buttonScrollForward.Name = "buttonScrollForward";
            this.buttonScrollForward.Size = new System.Drawing.Size(46, 36);
            this.buttonScrollForward.TabIndex = 33;
            this.buttonScrollForward.ToolTip = "Forward";
            // 
            // buttonScrollBackWard
            // 
            this.buttonScrollBackWard.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonScrollBackWard.ImageIndex = 75;
            this.buttonScrollBackWard.Images = this.imageListToolBar;
            this.buttonScrollBackWard.Location = new System.Drawing.Point(1104, 0);
            this.buttonScrollBackWard.Name = "buttonScrollBackWard";
            this.buttonScrollBackWard.Size = new System.Drawing.Size(46, 36);
            this.buttonScrollBackWard.TabIndex = 32;
            this.buttonScrollBackWard.ToolTip = "Backward";
            // 
            // comboBoxProbes
            // 
            this.comboBoxProbes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxProbes.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.comboBoxProbes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxProbes.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.comboBoxProbes.ItemHeight = 16;
            this.comboBoxProbes.Location = new System.Drawing.Point(1170, 6);
            this.comboBoxProbes.Name = "comboBoxProbes";
            this.comboBoxProbes.Size = new System.Drawing.Size(112, 22);
            this.comboBoxProbes.TabIndex = 31;
            this.comboBoxProbes.VisibleChanged += new System.EventHandler(this.comboBoxProbes_VisibleChanged);
            // 
            // buttonPatientRep
            // 
            this.buttonPatientRep.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonPatientRep.ImageIndex = 69;
            this.buttonPatientRep.Images = this.imageListToolBar;
            this.buttonPatientRep.Location = new System.Drawing.Point(1058, 0);
            this.buttonPatientRep.Name = "buttonPatientRep";
            this.buttonPatientRep.Size = new System.Drawing.Size(46, 36);
            this.buttonPatientRep.TabIndex = 30;
            this.buttonPatientRep.ToolTip = "Patient Report";
            // 
            // buttonPlay
            // 
            this.buttonPlay.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonPlay.ImageIndex = 66;
            this.buttonPlay.Images = this.imageListToolBar;
            this.buttonPlay.Location = new System.Drawing.Point(1012, 0);
            this.buttonPlay.Name = "buttonPlay";
            this.buttonPlay.Size = new System.Drawing.Size(46, 36);
            this.buttonPlay.TabIndex = 29;
            this.buttonPlay.ToolTip = "Replay";
            // 
            // buttonFreeze
            // 
            this.buttonFreeze.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonFreeze.EnableToggle = true;
            this.buttonFreeze.ImageIndex = 63;
            this.buttonFreeze.Images = this.imageListToolBar;
            this.buttonFreeze.Location = new System.Drawing.Point(966, 0);
            this.buttonFreeze.Name = "buttonFreeze";
            this.buttonFreeze.Size = new System.Drawing.Size(46, 36);
            this.buttonFreeze.TabIndex = 28;
            this.buttonFreeze.ToolTip = "Freeze/Unfreeze";
            // 
            // buttonLoad
            // 
            this.buttonLoad.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonLoad.ImageIndex = 6;
            this.buttonLoad.Images = this.imageListToolBar;
            this.buttonLoad.Location = new System.Drawing.Point(920, 0);
            this.buttonLoad.Name = "buttonLoad";
            this.buttonLoad.Size = new System.Drawing.Size(46, 36);
            this.buttonLoad.TabIndex = 27;
            this.buttonLoad.ToolTip = "Load";
            // 
            // buttonAddTrend
            // 
            this.buttonAddTrend.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonAddTrend.ImageIndex = 57;
            this.buttonAddTrend.Images = this.imageListToolBar;
            this.buttonAddTrend.Location = new System.Drawing.Point(874, 0);
            this.buttonAddTrend.Name = "buttonAddTrend";
            this.buttonAddTrend.Size = new System.Drawing.Size(46, 36);
            this.buttonAddTrend.TabIndex = 26;
            this.buttonAddTrend.ToolTip = "Add Trend";
            // 
            // buttonClinicalParameters
            // 
            this.buttonClinicalParameters.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonClinicalParameters.ImageIndex = 54;
            this.buttonClinicalParameters.Images = this.imageListToolBar;
            this.buttonClinicalParameters.Location = new System.Drawing.Point(828, 0);
            this.buttonClinicalParameters.Name = "buttonClinicalParameters";
            this.buttonClinicalParameters.Size = new System.Drawing.Size(46, 36);
            this.buttonClinicalParameters.TabIndex = 25;
            this.buttonClinicalParameters.ToolTip = "Expand Clinical Parameters";
            // 
            // buttonReturn
            // 
            this.buttonReturn.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonReturn.ImageIndex = 51;
            this.buttonReturn.Images = this.imageListToolBar;
            this.buttonReturn.Location = new System.Drawing.Point(782, 0);
            this.buttonReturn.Name = "buttonReturn";
            this.buttonReturn.Size = new System.Drawing.Size(46, 36);
            this.buttonReturn.TabIndex = 24;
            this.buttonReturn.ToolTip = "Return";
            // 
            // buttonPause
            // 
            this.buttonPause.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonPause.EnableToggle = true;
            this.buttonPause.ImageIndex = 48;
            this.buttonPause.Images = this.imageListToolBar;
            this.buttonPause.Location = new System.Drawing.Point(736, 0);
            this.buttonPause.Name = "buttonPause";
            this.buttonPause.Size = new System.Drawing.Size(46, 36);
            this.buttonPause.TabIndex = 23;
            this.buttonPause.ToolTip = "Pause";
            // 
            // buttonStartRecording
            // 
            this.buttonStartRecording.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonStartRecording.EnableToggle = true;
            this.buttonStartRecording.ImageIndex = 42;
            this.buttonStartRecording.Images = this.imageListToolBar;
            this.buttonStartRecording.Location = new System.Drawing.Point(690, 0);
            this.buttonStartRecording.Name = "buttonStartRecording";
            this.buttonStartRecording.Size = new System.Drawing.Size(46, 36);
            this.buttonStartRecording.TabIndex = 19;
            this.buttonStartRecording.ToolTip = "Record";
            // 
            // buttonSendTo
            // 
            this.buttonSendTo.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonSendTo.ImageIndex = 36;
            this.buttonSendTo.Images = this.imageListToolBar;
            this.buttonSendTo.Location = new System.Drawing.Point(644, 0);
            this.buttonSendTo.Name = "buttonSendTo";
            this.buttonSendTo.Size = new System.Drawing.Size(46, 36);
            this.buttonSendTo.TabIndex = 17;
            this.buttonSendTo.ToolTip = "Send To...";
            // 
            // buttonSave
            // 
            this.buttonSave.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonSave.ImageIndex = 15;
            this.buttonSave.Images = this.imageListToolBar;
            this.buttonSave.Location = new System.Drawing.Point(598, 0);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(46, 36);
            this.buttonSave.TabIndex = 16;
            this.buttonSave.ToolTip = "Save";
            // 
            // buttonAutoscan
            // 
            this.buttonAutoscan.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonAutoscan.ImageIndex = 33;
            this.buttonAutoscan.Images = this.imageListToolBar;
            this.buttonAutoscan.Location = new System.Drawing.Point(552, 0);
            this.buttonAutoscan.Name = "buttonAutoscan";
            this.buttonAutoscan.Size = new System.Drawing.Size(46, 36);
            this.buttonAutoscan.TabIndex = 14;
            this.buttonAutoscan.ToolTip = "M-Mode";
            // 
            // buttonAddSpectrum
            // 
            this.buttonAddSpectrum.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonAddSpectrum.ImageIndex = 18;
            this.buttonAddSpectrum.Images = this.imageListToolBar;
            this.buttonAddSpectrum.Location = new System.Drawing.Point(506, 0);
            this.buttonAddSpectrum.Name = "buttonAddSpectrum";
            this.buttonAddSpectrum.Size = new System.Drawing.Size(46, 36);
            this.buttonAddSpectrum.TabIndex = 13;
            this.buttonAddSpectrum.ToolTip = "Add Spectrum";
            // 
            // buttonHitsDetection
            // 
            this.buttonHitsDetection.BlinkInterval = 1000;
            this.buttonHitsDetection.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonHitsDetection.EnableToggle = true;
            this.buttonHitsDetection.ImageIndex = 24;
            this.buttonHitsDetection.Images = this.imageListToolBar;
            this.buttonHitsDetection.IsBlinkable = true;
            this.buttonHitsDetection.Location = new System.Drawing.Point(460, 0);
            this.buttonHitsDetection.Name = "buttonHitsDetection";
            this.buttonHitsDetection.Size = new System.Drawing.Size(46, 36);
            this.buttonHitsDetection.TabIndex = 12;
            this.buttonHitsDetection.ToolTip = "HITS Detection";
            // 
            // buttonNextFunction
            // 
            this.buttonNextFunction.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonNextFunction.ImageIndex = 60;
            this.buttonNextFunction.Images = this.imageListToolBar;
            this.buttonNextFunction.Location = new System.Drawing.Point(414, 0);
            this.buttonNextFunction.Name = "buttonNextFunction";
            this.buttonNextFunction.Size = new System.Drawing.Size(46, 36);
            this.buttonNextFunction.TabIndex = 11;
            this.buttonNextFunction.ToolTip = "Next Function";
            // 
            // buttonSummary
            // 
            this.buttonSummary.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonSummary.ImageIndex = 12;
            this.buttonSummary.Images = this.imageListToolBar;
            this.buttonSummary.Location = new System.Drawing.Point(368, 0);
            this.buttonSummary.Name = "buttonSummary";
            this.buttonSummary.Size = new System.Drawing.Size(46, 36);
            this.buttonSummary.TabIndex = 10;
            this.buttonSummary.ToolTip = "Summary Screen";
            // 
            // buttonNotes
            // 
            this.buttonNotes.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonNotes.ImageIndex = 30;
            this.buttonNotes.Images = this.imageListToolBar;
            this.buttonNotes.Location = new System.Drawing.Point(322, 0);
            this.buttonNotes.Name = "buttonNotes";
            this.buttonNotes.Size = new System.Drawing.Size(46, 36);
            this.buttonNotes.TabIndex = 9;
            this.buttonNotes.ToolTip = "Notes";
            // 
            // buttonDicom
            // 
            this.buttonDicom.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonDicom.ImageIndex = 81;
            this.buttonDicom.Images = this.imageListToolBar;
            this.buttonDicom.Location = new System.Drawing.Point(276, 0);
            this.buttonDicom.Name = "buttonDicom";
            this.buttonDicom.Size = new System.Drawing.Size(46, 36);
            this.buttonDicom.TabIndex = 9;
            this.buttonDicom.ToolTip = "Send application screen image to the PACS server";
            // 
            // buttonWMV
            // 
            this.buttonWMV.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonWMV.EnableToggle = true;
            this.buttonWMV.ImageIndex = 84;
            this.buttonWMV.Images = this.imageListToolBar;
            this.buttonWMV.Location = new System.Drawing.Point(230, 0);
            this.buttonWMV.Name = "buttonWMV";
            this.buttonWMV.Size = new System.Drawing.Size(46, 36);
            this.buttonWMV.TabIndex = 9;
            this.buttonWMV.ToolTip = "Stop/Start import to WMV file";
            this.buttonWMV.Visible = false;
            this.buttonWMV.OnPressedChange += new System.EventHandler(this.buttonWMV_OnPressed);
            // 
            // buttonCursors
            // 
            this.buttonCursors.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonCursors.EnableToggle = true;
            this.buttonCursors.ImageIndex = 21;
            this.buttonCursors.Images = this.imageListToolBar;
            this.buttonCursors.Location = new System.Drawing.Point(184, 0);
            this.buttonCursors.Name = "buttonCursors";
            this.buttonCursors.Size = new System.Drawing.Size(46, 36);
            this.buttonCursors.TabIndex = 8;
            this.buttonCursors.ToolTip = "Cursors";
            // 
            // buttonPrint
            // 
            this.buttonPrint.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonPrint.ImageIndex = 9;
            this.buttonPrint.Images = this.imageListToolBar;
            this.buttonPrint.Location = new System.Drawing.Point(138, 0);
            this.buttonPrint.Name = "buttonPrint";
            this.buttonPrint.Size = new System.Drawing.Size(46, 36);
            this.buttonPrint.TabIndex = 7;
            this.buttonPrint.ToolTip = "Print";
            // 
            // buttonNewPatient
            // 
            this.buttonNewPatient.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonNewPatient.ImageIndex = 3;
            this.buttonNewPatient.Images = this.imageListToolBar;
            this.buttonNewPatient.Location = new System.Drawing.Point(92, 0);
            this.buttonNewPatient.Name = "buttonNewPatient";
            this.buttonNewPatient.Size = new System.Drawing.Size(46, 36);
            this.buttonNewPatient.TabIndex = 22;
            this.buttonNewPatient.ToolTip = "New Patient";
            // 
            // buttonStudies
            // 
            this.buttonStudies.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonStudies.ImageIndex = 0;
            this.buttonStudies.Images = this.imageListToolBar;
            this.buttonStudies.Location = new System.Drawing.Point(46, 0);
            this.buttonStudies.Name = "buttonStudies";
            this.buttonStudies.Size = new System.Drawing.Size(46, 36);
            this.buttonStudies.TabIndex = 0;
            this.buttonStudies.ToolTip = "New Study";
            // 
            // autoLabel1
            // 
            this.autoLabel1.DX = -36;
            this.autoLabel1.DY = 38;
            this.autoLabel1.LabeledControl = this.panel1;
            this.autoLabel1.Location = new System.Drawing.Point(-36, 38);
            this.autoLabel1.Name = "autoLabel1";
            this.autoLabel1.Size = new System.Drawing.Size(32, 23);
            this.autoLabel1.TabIndex = 1;
            this.autoLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buttonStopRecording
            // 
            this.buttonStopRecording.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonStopRecording.ImageIndex = 45;
            this.buttonStopRecording.Images = this.imageListToolBar;
            this.buttonStopRecording.Location = new System.Drawing.Point(0, 0);
            this.buttonStopRecording.Name = "buttonStopRecording";
            this.buttonStopRecording.Size = new System.Drawing.Size(46, 36);
            this.buttonStopRecording.TabIndex = 20;
            this.buttonStopRecording.ToolTip = "Stop";
            // 
            // buttonTD
            // 
            this.buttonTD.Location = new System.Drawing.Point(0, 0);
            this.buttonTD.Name = "buttonTD";
            this.buttonTD.Size = new System.Drawing.Size(96, 104);
            this.buttonTD.TabIndex = 0;
            // 
            // ToolBarPanel
            // 
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.Controls.Add(this.panel1);
            this.Name = "ToolBarPanel";
            this.Size = new System.Drawing.Size(1500, 36);
            this.Load += new System.EventHandler(this.ToolBarPanel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panelButtons.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ImageList imageListToolBar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panelButtons;
        private System.Windows.Forms.ComboBox comboBoxTime;
        public Syncfusion.Windows.Forms.Tools.AutoLabel autoLabel1;
        private System.Windows.Forms.ToolTip toolTip1;
        private ToolBarBtn buttonStudies;
        private ToolBarBtn buttonNewPatient;
        private ToolBarBtn buttonPrint;
        private ToolBarBtn buttonCursors;
		private ToolBarBtn buttonNotes;
		private ToolBarBtn buttonDicom;
        private ToolBarBtn buttonWMV;
        private ToolBarBtn buttonSummary;
        private ToolBarBtn buttonNextFunction;
        private ToolBarBtn buttonHitsDetection;
        private ToolBarBtn buttonAddSpectrum;
        private ToolBarBtn buttonAutoscan;
        private ToolBarBtn buttonTD;
        private ToolBarBtn buttonSave;
        private ToolBarBtn buttonSendTo;
        private ToolBarBtn buttonStartRecording;
        private ToolBarBtn buttonStopRecording;
        private ToolBarBtn buttonPause;
        private ToolBarBtn buttonReturn;
        private ToolBarBtn buttonClinicalParameters;
        private ToolBarBtn buttonAddTrend;
        private ToolBarBtn buttonLoad;
        private ToolBarBtn buttonFreeze;
        private ToolBarBtn buttonPlay;
        private ToolBarBtn buttonPatientRep;
        private ProbesCombobox comboBoxProbes;
        private ToolBarBtn buttonScrollBackWard;
        private ToolBarBtn buttonScrollForward;
        private System.Windows.Forms.Label timeLabel;
        private System.Windows.Forms.ComboBox comboBoxSensitivity;
        private System.Windows.Forms.ComboBox comboBoxSensitivitySecondary;
        private System.ComponentModel.IContainer components;
    }
}
