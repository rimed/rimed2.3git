using System;
using System.Collections.Generic;
using System.Drawing;
using System.Data;
using System.IO;
using System.Windows.Forms;
using Rimed.Framework.Common;
using Rimed.Framework.GUI;
using Rimed.TCD.DAL;
using Rimed.TCD.GUI.Images;
using Rimed.TCD.Utils;
using Constants = Rimed.TCD.Utils.Constants;

namespace Rimed.TCD.GUI
{
    /// <summary>
    /// The blood vessel control Allow the user to navigate through saved examinations
    /// to choose blood vessels to examine. To delete/return BVs in examination.
    /// The saving of the data is also from this control which probably the most significant in the application
    /// </summary>
    public partial class BVListViewCtrl : UserControl
    {
        private enum EListType { SINGLE_LIST, DUAL_LIST };

        public DoubleListView DoubleLists { get; private set; }
        
        #region Private Fields

        private static readonly Color EXAMINED_BV_COLOR = Color.Blue;
        private static readonly Color UNEXAMINED_BV_COLOR = Color.Black;

        private readonly List<BV> m_bvCollection = new List<BV>();
        private bool m_addGates2SavedExam = false;
        private double m_tmpDepth = -1;
        private bool m_probeChanged = false;
        private Probe m_tmpProbe = null;
        private bool m_enabledChangeSelection = true;
        private bool m_examinationWasSaved = true;
        private String m_subExamNotes = string.Empty;
        private int m_currentSummaryScreenIndex = 0;	
        #endregion Private Fields

        public event BVChangedDelegate BVChangedEvent;
        private delegate void UpdateBVListDelegate(Guid slectedBVId, bool aDisplayOnlyExamineBV);
        private delegate void ProbeChangedDelegate(Probe newProbe);
        private delegate void DeleteGateDataDelegate(dsGateExamination.tb_GateExaminationRow row);

        public static BVListViewCtrl Instance { get; private set; }

        public Guid RecordingExaminationId { get; private set; }	

        /// <summary>Constructor</summary>
        public BVListViewCtrl()
        {
            ListType = EListType.SINGLE_LIST;
            InSummaryStatus = GlobalTypes.EAdd2SummaryScreen.SelectedSpectrum;
            InitializeComponent();

            HardDriveSpaceKeeper.Instance.RowDataModeChanged += new RowDataModeChangedHandler(HardDriveSpaceKeeper_RowDataModeChanged);
            Instance = this;

            imageList1.Images.Add(ImageManager.Instance.GetImage(ImageKey.DeleteRowData));
            imageList1.Images.Add(ImageManager.Instance.GetImage(ImageKey.SaveRowData));

            imageList2.Images.Add(ImageManager.Instance.GetImage(ImageKey.DeletedRowData));
            imageList2.Images.Add(ImageManager.Instance.GetImage(ImageKey.SavedRowData));

            DoubleLists = new DoubleListView(listView1, listView2); //Two lists
        }
		  
        #region Public Properties

        private EListType ListType { get; set; }

        public GlobalTypes.EAdd2SummaryScreen InSummaryStatus { get; set; }

        public bool ExaminationWasSaved
        {
            get
            {
                return m_examinationWasSaved && !RimedDal.Instance.IsTemporaryLoadedExamination;	
            }
            set
            {
                m_examinationWasSaved = value;
            }
        }

        public bool DontLoadExamination { get; set; }

        public string SubExamNotes
        {
            get
            {
                return m_subExamNotes;
            }
            set
            {
                m_subExamNotes = value;

                var sc = listView1.SelectedIndices;
                if (sc.Count == 0)
                    return;

                var index = sc[0];
                if (index < 0 || index >= listView1.Items.Count)
                    return;

                var bv = listView1.Items[index].Tag as BV;
                if (bv == null) 
                    return;

                //RIMD-549: Notes are not saved for Monitoring examination	
                if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Monitoring) 	
                {
					if (!ExaminationWasSaved)
                        return;
                }
                else if (bv.BVStatus == BV.EBVStatus.NotExaminatedYet)									
                	return;
					
                var filter  = "SubExaminationIndex = '" + bv.SubExamIndex + "'";
                var rows    = RimedDal.Instance.SubExaminationsDS.tb_SubExamination.Select(filter);
                if (rows.Length != 0)
                {
                    rows[0]["Notes"] = m_subExamNotes;
                    if (ExaminationWasSaved)
                        RimedDal.Instance.UpdateCurSubExamination();
                }
            }
        }

        public string ExamNotes
        {
            get
            {
				var examRow = RimedDal.Instance.LoadedExamination();
	            return (examRow == null) ? string.Empty : examRow.Notes;

	            //if (RimedDal.Instance.ExaminationsDS.tb_Examination.Rows.Count == 0)
				//    return String.Empty;
                
				//return (String)RimedDal.Instance.ExaminationsDS.tb_Examination.Rows[0]["Notes"];
            }
            set
            {
				if (value == null)
					return;

				var examRow = RimedDal.Instance.LoadedExamination();
	            if (examRow == null)
		            return;

                RimedDal.Instance.UpdateExamination(examRow, "Notes", value);

                //Removed by Alex bug #713 v.2.2.3.19 03/01/2016 
                //try
                //{
                //    var str = (examRow.Notes == null) ? string.Empty : examRow.Notes;
                //    if (value.Length > str.Length)
                //    {
                //        str = value.Substring(str.Length, value.Length - str.Length);
                //        RimedDal.Instance.UpdateExamination(examRow, "Notes", str);
                //    }
                //}
                //catch (Exception ex)
                //{
                //    // do not update the String.
                //    Logger.LogError(ex);
                //}

				//if (value == null || RimedDal.Instance.ExaminationsDS.tb_Examination.Rows.Count == 0)
				//    return;

				//try
				//{
				//    var str = (string)RimedDal.Instance.ExaminationsDS.tb_Examination.Rows[0]["Notes"];
				//    if (value.Length > str.Length)
				//    {
				//        str = value.Substring(str.Length, value.Length - str.Length);
				//        RimedDal.Instance.UpdateExamination(RimedDal.Instance.ExaminationsDS.tb_Examination[0], "Notes", str);
				//    }
				//}
				//catch (Exception ex)
				//{
				//    // do not update the String.
				//    Logger.LogError(ex);
				//}
            }
        }

        public string SelectBVSubExamIndex // added by Alex 19/08/2014 fixed bug #507 v 2.1.7.6
        {
            get
            {
                return (listView1.SelectedItems.Count > 0 ? ((BV)listView1.SelectedItems[0].Tag).SubExamIndex.ToString() : "");
            }
        }

        #endregion Public Properties

        private void removeEvents()
        {
            if (MainForm.Instance != null) 
            {
                MainForm.Instance.NextBVEvent -= new MainForm.NextBVDelegate(nextBVNotification);
                MainForm.Instance.CurrentBVEvent -= new Action(currentBVNotification);
                MainForm.Instance.PrevBVEvent -= new Action(prevBVNotification);
            }

            GateView.GateViewDepthChanged -= new DepthChangedDelegate(displayDepth);
            LayoutManager.LoadExamEvent -= new LoadExamDelegate(LayoutManager_LoadExamEvent);
            LayoutManager.LoadBVExamEvent -= new LoadBVExamDelegate(LayoutManager_LoadBVExamEvent);
            LayoutManager.LoadGateExamEvent -= new LoadGateExamDelegate(LayoutManager_LoadGateExamEvent);
        }

        public void RegisterEvents()
        {
            InSummaryStatus = (GlobalTypes.EAdd2SummaryScreen)RimedDal.Instance.ConfigDS.tb_BasicConfiguration[0].Add2SummaryScreen;

            if (DesignMode || MainForm.Instance != null) 
            {
                MainForm.Instance.NextBVEvent += new MainForm.NextBVDelegate(nextBVNotification);
                MainForm.Instance.CurrentBVEvent += new Action(currentBVNotification);
                MainForm.Instance.PrevBVEvent += new Action(prevBVNotification);
            }

            GateView.GateViewDepthChanged += new DepthChangedDelegate(displayDepth);
            LayoutManager.LoadExamEvent += new LoadExamDelegate(LayoutManager_LoadExamEvent);
            LayoutManager.LoadBVExamEvent += new LoadBVExamDelegate(LayoutManager_LoadBVExamEvent);
            LayoutManager.LoadGateExamEvent += new LoadGateExamDelegate(LayoutManager_LoadGateExamEvent);
        }

        private dsExamination.tb_ExaminationRow createNewExam(Guid studyIndex, Guid patientIndex) 
        {
            Logger.LogTrace(Logger.ETraceLevel.L5, Name, "CreateNewExam(studyIndex={0}, patientIndex={1})", studyIndex, patientIndex);

            // create new row exam.
            var examRow = RimedDal.Instance.ExaminationsDS.tb_Examination.Newtb_ExaminationRow();
            examRow.Examination_Index = Guid.NewGuid();

            // the next variables can be changed, we set those value in case the user want to generate patient Report of unsaved examination.
            examRow.Patient_Index = patientIndex;	 
            examRow.BackupPath = String.Empty;
            examRow.Deleted = false;
            DateTime t = DateTime.Now;
            examRow.Date = t;
            examRow.Study_Index = studyIndex;	 
            examRow.TimeHours = t.Hour;
            examRow.TimeMinutes = t.Minute;
            examRow.Indication = "";
            examRow.Interpretation = "";
            examRow.Notes = "";
            examRow.IsTemporary = true;
            examRow.LR_ManualCalc = false;

            RimedDal.Instance.ExaminationsDS.tb_Examination.Rows.Add(examRow);
            EventListViewCtrl.Instance.ExaminationDate = t;

            m_subExamNotes = string.Empty;
            ExaminationWasSaved = false;
            m_addGates2SavedExam = false;
			
            return examRow;	
        }

        ///<remarks>Assaf (08/01/2007) : to prevent saving the study in replay mode (creates huge problems)</remarks>
        private void init(GlobalTypes.EStudyType type)	
        {
            ListType = (type == GlobalTypes.EStudyType.Unilateral) ? EListType.SINGLE_LIST : EListType.DUAL_LIST;

            initDisplay();
        }

        private void reloadControlLabels()
        {
            var strRes = MainForm.StringManager;
            BVName.Text = strRes.GetString("BV Name");
            GateDepth.Text = strRes.GetString("Depth");
            menuItemReplayBV.Text = strRes.GetString("Replay BV");
            menuItemRename.Text = strRes.GetString("Rename BV");
            menuItemReturn.Text = strRes.GetString("Return BV");
            BVName2.Text = strRes.GetString("BV Name");
            GateDepth2.Text = strRes.GetString("Depth");
        }

        private void initDisplay()
        {
            if (InvokeRequired)
            {
                BeginInvoke((Action)initDisplay);
                return;
            }

            if (ListType == EListType.DUAL_LIST)
            {
                listView1.Height = Height / 2;
                listView2.Height = Height / 2;
                listView1.Dock = DockStyle.Top;
                listView2.Dock = DockStyle.Bottom;
                listView1.Visible = true;
                listView2.Visible = true;
            }
            else
            {
                listView2.Visible = false;
                listView1.Dock = DockStyle.Fill;
            }
            reloadControlLabels();
        }

        private void verifyClearDataStructure()
        {
            if (!ExaminationWasSaved && m_addGates2SavedExam)
            {
                var ex = new Exception("Data structure is not clear");
                Logger.LogError(ex);
                throw ex;
            }
        }

        private void clearDataStructure()
        {
            SummaryScreen.Clear();
            
            ExaminationWasSaved = true;

            m_addGates2SavedExam = false;
        }

        private void addBV(BV bv)
        {
            m_bvCollection.Add(bv);
        }

        public void UpdateBVsDepth(double depth)
        {
            foreach (ListViewItem item in listView1.Items)
            {
                if (item == null)
                    continue;

                var bv = item.Tag as BV;
                if (bv == null)
                    continue;

                if (bv.BVStatus == BV.EBVStatus.Examinated)
                {
                    m_tmpDepth = depth;
                }
                else
                {
                    bv.DefaultDepth = depth;
                    setItemDepth(bv, item);
                }
            }

            foreach (ListViewItem item in listView2.Items)
            {
                if (item == null)
                    continue;

                var bv = item.Tag as BV;
                if (bv == null)
                    continue;

                if (bv.BVStatus == BV.EBVStatus.Examinated)
                {
                    m_tmpDepth = depth;
                }
                else
                {
                    bv.DefaultDepth = depth;
                    setItemDepth(bv, item);
                }
            }
        }

		public void SelectTopBV()
		{
			if (listView1.Items.Count > 0)
				listView1.Items[0].Selected = true;

		}
		
        /// <summary>This function fill listview1 control with the BVs items.</summary>
        /// <param name="selectedBVId">the guid id of the selected BV</param>
        /// <param name="displayOnlyExamineBV">flag</param>
        public void UpdateBVList(Guid selectedBVId, bool displayOnlyExamineBV)
        {
            if (InvokeRequired)
            {
                BeginInvoke((UpdateBVListDelegate)UpdateBVList, selectedBVId, displayOnlyExamineBV);
                return;
            }

            listView1.Items.Clear();
            listView2.Items.Clear();
            int selectedIndex = 0;
            int counter = 0;

            if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.DiagnosticLoad || LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.MonitoringLoad)
            {
                listView1.StateImageList = imageList2;
                listView2.StateImageList = imageList2;
            }
            else
            {
                listView1.StateImageList = imageList1;
                listView2.StateImageList = imageList1;
            }

            foreach (var bv in m_bvCollection)
            {
                if (displayOnlyExamineBV && bv.BVStatus != BV.EBVStatus.Examinated)
                    continue;

				// adding BV to the list view.
                var item = new ListViewItem();
                item.Text = bv.FullName;
                item.Font = GlobalSettings.FONT_16_ARIAL_BOLD;

                item.Tag = bv;
                // mark examined BV in blue color text.
                if (bv.BVStatus == BV.EBVStatus.Examinated)
                {
                    item.ForeColor = EXAMINED_BV_COLOR;
                    ((BV)item.Tag).SaveOnlySummaryImages = RimedDal.Instance.SaveOnlySummaryImages;
                    item.Checked = true; // only this way list starts show state icons
                    item.Checked = !RimedDal.Instance.SaveOnlySummaryImages;
                }
                else
                    item.ForeColor = UNEXAMINED_BV_COLOR;

                if (bv.BVExaminationIndex == selectedBVId)
                    selectedIndex = counter;

                // in case of CW probe the depth and the width will be 0 and there is no need to display the depth of the BV.
                addItemDepth(bv, item);

                addItem(item, counter);
                counter++;
            }

            if (listView1.Items.Count != 0 && selectedBVId.CompareTo(Guid.Empty) != 0)
            {
                if (ListType == EListType.DUAL_LIST)
                {
                    selectedIndex /= 2; // The counter was * 2 because it is Bilateral and we has two list
                    Visible = true;
                    Enabled = true;
                    listView2.Items[selectedIndex].Selected = false;
                    listView2.Items[selectedIndex].Selected = true;
                    listView2.Items[selectedIndex].Focused = true;
                }

                listView1.Items[selectedIndex].Selected = true;
                listView1.Items[selectedIndex].Focused = true;
            }
        }

        private static void addItemDepth(BV bv, ListViewItem item)
        {
			item.SubItems.Add(bv.BVProbe.IsCW ? string.Empty : bv.DefaultDepthString);
            item.SubItems.Add(bv.DefaultDepthString);
        }

        private static void setItemDepth(BV bv, ListViewItem item)
        {
            item.SubItems[1].Text = bv.BVProbe.IsCW ? string.Empty : bv.DefaultDepthString;
            item.SubItems[2].Text = bv.DefaultDepthString;
        }

        private void addItem(ListViewItem item, int counter)
        {
	        var lst = listView1;
			if (ListType != EListType.SINGLE_LIST && (counter % 2) != 0)
				lst = listView2;

			addItem(lst, lst.Items.Count, item);
        }

        private void addItem(ListView list, int position, ListViewItem item)
        {
            if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.DiagnosticLoad || LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.MonitoringLoad)
            {
                item.Checked = true; // only this way list starts show state icons
                item.Checked = !((BV)item.Tag).SaveOnlySummaryImages;
            }

            list.Items.Insert(position, item);
        }

        private void fireBVChangedEvent(BV[] bvArr, object sender)
        {
            if (BVChangedEvent != null)
            {
				using (new CursorSafeChange(this, true))
			        BVChangedEvent(bvArr, sender);
                LayoutManager.Instance.UpdateLindegaardRatio();
			}
        }

        /// <summary>This method select BV in list using guid of the SubExamination</summary>
        /// <param name="subExaminationIndex">SubExamination's guid; is equal for both of right and left BVs</param>
        public void SelectBVbyIndex(String subExaminationIndex)
        {
            for (var i = 0; i < listView1.Items.Count; i++)
            {
                var bv = (BV)listView1.Items[i].Tag;
                if (bv.SubExamIndex.ToString() == subExaminationIndex)
                {
                    listView1.Items[i].Selected = true;
                    break;
                }
            }
        }

        private int lastListItem()
        {
            int index = listView1.Items.Count;

            return index;
        }

        private bool nextBVNotification()
        {
            //Type of return value was changed to Boolean for last vessel marking changed by Natalie - 11/4/2008
            if (ListType == EListType.DUAL_LIST)
                return dualNextBVNotification();

            var sc = listView1.SelectedIndices;
            if (sc.Count != 0)
            {
                int bvIndex = sc[0];
                if ((bvIndex + 1) >= lastListItem()) // get to the end of the list view.
                {
					LoggedDialog.Show(MainForm.Instance, MainForm.StringManager.GetString("Blood vessel list is finished"));
                    return false;
                }

                listView1.Items[bvIndex + 1].Selected = true;
                listView1.Items[bvIndex + 1].Focused = true;
            }
            else
            {
                listView1.Items[0].Selected = true;
                listView1.Items[0].Focused = true;
            }

            listView1.Focus();

            return true;
        }

        private bool dualNextBVNotification()
        {
            //Type of return value was changed to Boolean for last vessel marking changed by Natalie - 11/4/2008
            var sc = listView1.SelectedIndices;
            if (sc.Count != 0)
            {
                var bvIndex = sc[0];
                if ((bvIndex + 1) >= lastListItem()) // get to the end of the list view.
                {
					LoggedDialog.Show(MainForm.Instance, MainForm.StringManager.GetString("Blood vessel list is finished"));
                    return false;
                }

                listView1.Items[bvIndex + 1].Selected = true;
                listView1.Items[bvIndex + 1].Focused = true;
                listView2.Items[bvIndex + 1].Selected = true;
                listView2.Items[bvIndex + 1].Focused = true;
            }
            else
            {
                listView1.Items[0].Selected = true;
                listView1.Items[0].Focused = true;
                listView2.Items[0].Selected = true;
                listView2.Items[0].Focused = true;
            }

            listView1.Focus();
            listView2.Focus();

            return true;
        }

        private void currentBVNotification()
        {
            if (ListType == EListType.DUAL_LIST)
            {
                dualCurrentBVNotification();
                return;
            }

            var sc = listView1.SelectedIndices;

            if (sc.Count != 0)
            {
                var bvIndex = sc[0];
                if (bvIndex >= lastListItem()) // get to the end of the list view.
                    return;

                m_enabledChangeSelection = false;
                listView1.Items[bvIndex].Selected = false;
                m_enabledChangeSelection = true;
                listView1.Items[bvIndex].Selected = true;
                listView1.Items[bvIndex].Focused = true;
            }
            else
            {
                m_enabledChangeSelection = false;
                listView1.Items[0].Selected = false;
                m_enabledChangeSelection = true;
                listView1.Items[0].Selected = true;
                listView1.Items[0].Focused = true;
            }
            
            listView1.Focus();
        }

        private void dualCurrentBVNotification()
        {
            var sc = listView1.SelectedIndices;
            if (sc.Count != 0)
            {
                var bvIndex = sc[0];
                if ((bvIndex) >= lastListItem()) // get to the end of the list view.
                    return;

                m_enabledChangeSelection = false;
                listView1.Items[bvIndex].Selected = false;
                m_enabledChangeSelection = true;
                listView1.Items[bvIndex].Selected = true;
                listView1.Items[bvIndex].Focused = true;

                listView2.Items[bvIndex].Selected = true;
                listView2.Items[bvIndex].Focused = true;
            }
            else
            {
                m_enabledChangeSelection = false;
                listView1.Items[0].Selected = false;
                m_enabledChangeSelection = true;

                listView1.Items[0].Selected = true;
                listView1.Items[0].Focused = true;
                listView2.Items[0].Selected = true;
                listView2.Items[0].Focused = true;
            }

            listView1.Focus();
            listView2.Focus();
        }

        private void prevBVNotification()
        {
            if (ListType == EListType.DUAL_LIST)
            {
                dualPrevBVNotification();
                return;
            }

            var sc = listView1.SelectedIndices;
            if (sc.Count != 0)
            {
                if ((sc[0] - 1) == -1) // get to the start of the list view.
                    return;

                listView1.Items[sc[0] - 1].Selected = true;
                listView1.Items[sc[0]].Focused = true;
            }
            else
            {
                listView1.Items[0].Selected = true;
                listView1.Items[0].Focused = true;
            }

            listView1.Focus();
        }

        private void dualPrevBVNotification()
        {
            var sc = listView1.SelectedIndices;

            if (sc.Count != 0)
            {
                if ((sc[0] - 1) == -1) // get to the start of the list view.
                    return;

                listView2.Items[sc[0] - 1].Selected = true;
                listView2.Items[sc[0]].Focused = true;
                listView1.Items[sc[0] - 1].Selected = true;
                listView1.Items[sc[0]].Focused = true;
            }
            else
            {
                listView2.Items[0].Selected = true;
                listView2.Items[0].Focused = true;
                listView1.Items[0].Selected = true;
                listView1.Items[0].Focused = true;
            }
            
            listView1.Focus();
        }

        /// <summary>this function mark the current BV with blue color and add bv after the current BV [the same as the current].</summary>
        private void duplicateBV()
        {
            if (ListType == EListType.DUAL_LIST)
            {
                dualDuplicateBV();
                return;
            }

            // mark the current selected BV with blue color.
            var sc = listView1.SelectedIndices;
            if (sc.Count == 0)
                return;

            var index = sc[0];
			var bvOrigin = listView1.Items[index].Tag as BV;
	        if (bvOrigin == null)
		        return;

			listView1.Items[index].ForeColor = EXAMINED_BV_COLOR;

			// add new item after the selected BV.
			var bv				= new BV(bvOrigin);		
			bv.BVProbe			= m_probeChanged ? m_tmpProbe : bvOrigin.BVProbe;

            bv.DuplicateFlag	= true;

            m_bvCollection.Insert(index + 1, bv);

            var item		= new ListViewItem();
            item.Text		= bv.FullName;
            item.Font		= GlobalSettings.FONT_16_ARIAL_BOLD;
            item.ForeColor	= (bv.BVStatus == BV.EBVStatus.NotExaminatedYet) ? UNEXAMINED_BV_COLOR : EXAMINED_BV_COLOR;
            item.Tag		= bv;

            //RIMD-331: Need to keep values for Depth Gain, Power etc when making Freeze and then Unfreeze
            //RIMD-399: Parameter values on screen not change for 4 Mhz CW probe (when using Remote or not)
            //Commented by Natalie: 01/28/2011
            //There was a lot of strange code here, but we deleted it and now Depth changes correctly ^^

			if (bvOrigin.BVProbe.IsCW)
            {
                var row			= RimedDal.Instance.GetBVProperties(bv.BVIndex);
                bv.DefaultDepth = (double)row["Depth"];
            }

            addItemDepth(bv, item);

            //Insert in a position
            addItem(listView1, index + 1, item); //this.listView1.Items.Insert(index + 1, item1);

            // set the new item selected.
            listView1.Items[index].ForeColor = EXAMINED_BV_COLOR;
            listView1.Items[index + 1].Selected = true;
            listView1.Items[index + 1].Focused = true; // this is work around so the previous item will be refresh and will be colored with blue color.
            listView1.Focus();
        }

        private void dualDuplicateBV()
        {
            // mark the current selected BV with blue color.
            var sc = listView1.SelectedIndices;
            if (sc.Count == 0)
                return;

            var index = sc[0];
			var bvOrigin1 = listView1.Items[index].Tag as BV;
			var bvOrigin2 = listView2.Items[index].Tag as BV;

	        if (bvOrigin1 == null || bvOrigin2 == null)
		        return;
			
			listView1.Items[index].ForeColor = EXAMINED_BV_COLOR;
            listView2.Items[index].ForeColor = EXAMINED_BV_COLOR;


			// add new item after the selected BV.
			var bv1 = new BV(bvOrigin1);
			var bv2 = new BV(bvOrigin2);

            if (m_probeChanged)
			{
				bv1.BVProbe = m_tmpProbe;
				bv2.BVProbe = m_tmpProbe;
			}
			else
            {
				bv1.BVProbe = bvOrigin1.BVProbe;
				bv2.BVProbe = bvOrigin2.BVProbe;
            }

            bv1.DuplicateFlag = true;
            bv2.DuplicateFlag = true;

            m_bvCollection.Insert(index + 1, bv1);
            m_bvCollection.Insert(index + 1 + 1, bv2);

            var item1 = new ListViewItem();
            var item2 = new ListViewItem();

            item1.Text = bv1.FullName;
            item1.Font = GlobalSettings.FONT_16_ARIAL_BOLD;

            item2.Text = bv2.FullName;
            item2.Font = GlobalSettings.FONT_16_ARIAL_BOLD;

            if (bv1.BVStatus == BV.EBVStatus.NotExaminatedYet)
            {
                item1.ForeColor = UNEXAMINED_BV_COLOR;
                item2.ForeColor = UNEXAMINED_BV_COLOR;
            }
            else
            {
                item1.ForeColor = EXAMINED_BV_COLOR;
                item2.ForeColor = EXAMINED_BV_COLOR;
            }

            item1.Tag = bv1;
            item2.Tag = bv2;

            if (m_tmpDepth != -1)
            {
                bv1.DefaultDepth = m_tmpDepth;
                bv2.DefaultDepth = m_tmpDepth;
                m_tmpDepth = -1;
            }

            setItemDepth(bv1, item1);
            setItemDepth(bv2, item2);

            //Insert in a position
            addItem(listView1, index + 1, item1); 
            addItem(listView2, index + 1, item2); 

			// set the new item selected.
            listView1.Items[index].ForeColor = EXAMINED_BV_COLOR;
            listView1.Items[index + 1].Selected = true;
            listView1.Items[index + 1].Focused = true; // this is work around so the previous item will be refresh and will be colored with blue color.
            listView1.Focus();

            listView2.Items[index].ForeColor = EXAMINED_BV_COLOR;
            listView2.Items[index + 1].Selected = true;
            listView2.Items[index + 1].Focused = true; // this is work around so the previous item will be refresh and will be colored with blue color.
            listView2.Focus();
        }
        #region rename BV
        List<Guid> m_BVRenameIndexes = new List<Guid>();
        List<string> m_BVRenameNames = new List<string>();
        /// <summary>Rename BV in the bv-list and in all the gates reference that BV</summary>
        public bool rename()
        {
            if (listView1 == null || listView1.Items.Count == 0)
                return false;

			//System.Diagnostics.Debug.WriteLine("SystemLayoutMode={0}, SystemMode={1}, WorkingMode={2}", LayoutManager.SystemLayoutMode, LayoutManager.SystemMode, MainForm.Instance.WorkingMode);

            var sc = listView1.SelectedIndices;
            if (sc.Count == 0)
				return false;

            var index = sc[0];
            if (index < 0 || index >= listView1.Items.Count)
				return false;

            var bv = listView1.Items[index].Tag as BV;
            if (bv == null || bv.BVStatus!=BV.EBVStatus.Examinated)                                     
                return false;


            int newBvIndex;
            PrepareBVRenameLists(bv, m_BVRenameNames, m_BVRenameIndexes);

            using (var dlg = new BVRenameDlg(bv.BVName, bv.FullName, m_BVRenameNames))
	        {
		        dlg.TopMost = true;

		        var res = dlg.ShowDialog(MainForm.Instance);

			    newBvIndex = dlg.SelectedIndex;
				Focus();
				if (res != DialogResult.OK)
					return false;
			}

            return rename(bv, index, newBvIndex);
        }

        // fill list of names and indeces of Rename options for given BV
        void PrepareBVRenameLists(BV bv , List<string> BVnames, List<Guid> BVindexes)
        {
            DAL.dsBVSetup dsBVSetup1 = new Rimed.TCD.DAL.dsBVSetup();
            DAL.dsBVSetup dsBVSetup2 = new Rimed.TCD.DAL.dsBVSetup();
            dsBVSetup2.Merge(RimedDal.Instance.BVSetupDS);

            BVindexes.Clear();
            BVnames.Clear();

            // fill dsSetup1
            bool bvExist = false;
            dsBVSetup1.Clear();
            for (var i = 0; i < dsBVSetup2.tb_BloodVesselSetup.Rows.Count; i++)
            {
                var row2 = dsBVSetup2.tb_BloodVesselSetup.Rows[i] as dsBVSetup.tb_BloodVesselSetupRow;
                if (row2 == null)
                    continue;

                for (int q = 0; q < dsBVSetup1.tb_BloodVesselSetup.Rows.Count; q++)
                {
                    var row1 = dsBVSetup1.tb_BloodVesselSetup.Rows[q] as dsBVSetup.tb_BloodVesselSetupRow;
                    if (row1 == null)
                        continue;

                    if (string.Compare(row2.Name, row1.Name, StringComparison.InvariantCultureIgnoreCase) == 0)
                    {
                        bvExist = true;
                        if ((row1.Side == (int)GlobalTypes.ESide.Left && row2.Side == (int)GlobalTypes.ESide.Right) ||
                            (row2.Side == (int)GlobalTypes.ESide.Left && row1.Side == (int)GlobalTypes.ESide.Right))
                            row1.Side = (int)GlobalTypes.ESide.BothSides;
                    }
                }

                if (!bvExist)
                {
                    var row = dsBVSetup1.tb_BloodVesselSetup.NewRow() as dsBVSetup.tb_BloodVesselSetupRow;
                    if (row != null)
                    {
                        row["Blood_Vessel_Index"] = row2["Blood_Vessel_Index"];
                        row["Name"] = row2["Name"];
                        row["Probe"] = row2["Probe"];
                        row["Depth"] = row2["Depth"];
                        row["Gain"] = row2["Gain"];
                        row["Thump"] = row2["Thump"];
                        row["Width"] = row2["Width"];
                        row["Width"] = row2["Width"];
                        row["Power"] = row2["Power"];
                        row["MinFrequency"] = row2["MinFrequency"];
                        row["MaxFrequency"] = row2["MaxFrequency"];
                        row["Units"] = row2["Units"];
                        row["ZeroLinePosition"] = row2["ZeroLinePosition"];
                        row["DirectionToProbe"] = row2["DirectionToProbe"];
                        row["Comments"] = row2["Comments"];
                        row["DisplayPeakFwdEnvelope"] = row2["DisplayPeakFwdEnvelope"];
                        row["DisplayPeakBckEnvelope"] = row2["DisplayPeakBckEnvelope"];
                        row["DisplayMeanFwdEnvelope"] = row2["DisplayMeanFwdEnvelope"];
                        row["DisplayMeanBckEnvelope"] = row2["DisplayMeanBckEnvelope"];
                        row["DisplaySecptrum"] = row2["DisplaySecptrum"];
                        row["HitsThreshold"] = row2["HitsThreshold"];
                        row["Angle"] = row2["Angle"];
                        row["FreqRang"] = row2["FreqRang"];
                        row["Side"] = row2["Side"];
                        row["Location"] = row2["Location"];

                        dsBVSetup1.tb_BloodVesselSetup.Addtb_BloodVesselSetupRow(row);
                    }
                }

                bvExist = false;
            }


            for (var i = 0; i < dsBVSetup2.tb_BloodVesselSetup.Rows.Count; i++)
            {
                var row = dsBVSetup2.tb_BloodVesselSetup.Rows[i] as dsBVSetup.tb_BloodVesselSetupRow;
                if (row == null)
                    continue;

                var strName = row.Name;

                //RIMD-352: No list of BV names for renaming in Extracranial and Intra-operative study
                var guid = row.Blood_Vessel_Index;
                if (!BVListViewCtrl.Instance.IsContained(guid) &&
                    !RimedDal.Instance.IsBVInStudy(MainForm.Instance.CurrentStudyId, guid))
                    //CR #434: Enable diagnostic study post save edit
                    continue;

                if (ListType == EListType.SINGLE_LIST && (int)bv.BVSide != row.Side)
                {
                    if (!((bv.BVName == "VA" && strName == "BA") || (bv.BVName == "BA" && strName == "VA")))
                        continue;
                }


                string strNameOther = strName;
                switch (row.Side)
                {
                    case (int)GlobalTypes.ESide.Left:
                        strName += "-L";
                        strNameOther += "-R";
                        break;
                    case (int)GlobalTypes.ESide.Right:
                        strName += "-R";
                        strNameOther += "-L";
                        break;
                    default:
                        break;
                }

                //if (string.Compare(strName, m_originalName, StringComparison.InvariantCultureIgnoreCase) == 0)
                //    continue;

                //al.Add(row.Blood_Vessel_Index);
                BVindexes.Insert(BVindexes.Count / 2, row.Blood_Vessel_Index);
                BVindexes.Add(row.Blood_Vessel_Index);
                //var ix = comboBox1.Items.Add(strName);
                BVnames.Insert(BVnames.Count / 2, strName);
                BVnames.Add(strNameOther);
            }

        }


        bool rename(BV bv, int index, int newBvIndex)
        {
            if (newBvIndex < 0 || newBvIndex > m_BVRenameNames.Count)
                return false;

            if (index == newBvIndex)
                return true;

            var gatesRows   = renameGetGates(bv.SubExamIndex);
            if (gatesRows == null || gatesRows.Length == 0)            
            {
				LoggedDialog.Show(MainForm.Instance, string.Format("No active gates found for blood vessels {0}. Rename operation aborted.", bv.BVName));
				return false;
            }

            var bvRows = renameGetBVExaminationRows(bv.BVExaminationIndex);
            if (bvRows == null || bvRows.Length == 0)           
            {
				LoggedDialog.Show(MainForm.Instance, string.Format("No active examinations found for blood vessels {0}. Rename operation aborted.", bv.BVName));
				return false;
            }
            string originalName = bv.FullName;
            if (string.IsNullOrWhiteSpace(originalName))
                originalName = bv.BVName;

            var strBV = m_BVRenameNames[newBvIndex];
            Guid dlgBVIndex = Guid.Empty;
            Guid dlgBVIndex2 = Guid.Empty;
            string dlgBVName ="";
            GlobalTypes.ESide dlgBVSide = GlobalTypes.ESide.BothSides;
            if (ListType == EListType.SINGLE_LIST)
            {
                dlgBVName = strBV.Length > 2 ? strBV.Remove(strBV.Length - 2, 2) : strBV;
                dlgBVIndex = m_BVRenameIndexes[newBvIndex];

                if (strBV.EndsWith("-L"))
                    dlgBVSide = GlobalTypes.ESide.Left;
                else
                    dlgBVSide = strBV.EndsWith("-R") ? GlobalTypes.ESide.Right : GlobalTypes.ESide.Middle;
            }
            else
            {
                if (strBV.Length > 2)
                    strBV = strBV.Remove(strBV.Length - 2, 2);
                dlgBVName = strBV;

                strBV = strBV + "-L";
                //Found the two BV guids
                for (int i = 0; i < m_BVRenameIndexes.Count; ++i)
                {
                    if (strBV.Equals(m_BVRenameNames[i]))
                    {
                        dlgBVIndex = m_BVRenameIndexes[i];
                        break;
                    }
                }

                strBV = BVName + "-R";
                for (int i = 0; i < m_BVRenameIndexes.Count; ++i)
                {
                    if (strBV.Equals(m_BVRenameNames[i]))
                    {
                        dlgBVIndex2 = m_BVRenameIndexes[i];
                        break;
                    }
                }
            }

            if (string.Compare(strBV, originalName, StringComparison.InvariantCultureIgnoreCase) == 0 && bv.BVSide.CompareTo(dlgBVSide) == 0)
                return false;

            // here start the real work: 
            // Rename BV in the bv-list and in all the gates reference that BV
            BV bv2 = null;
            dsBVExamination.tb_BVExaminationRow[] bvRows2 = null;
            if (ListType == EListType.DUAL_LIST)
            {
                if (listView2 == null)
                    return false;

                bv2 = listView2.Items[index].Tag as BV;
                if (bv2 == null)
                    return false;

                bvRows2 = renameGetBVExaminationRows(bv2.BVExaminationIndex);
                if (bvRows2 == null || bvRows2.Length == 0)
                {
                    LoggedDialog.Show(MainForm.Instance, string.Format("No active examinations found for blood vessels {0}. Rename operation aborted. (2)", bv.BVName));
                    return false;
                }
            }

            if (MainForm.Instance.WorkingMode == GlobalTypes.EWorkingMode.ExameMode)	// added by Alex 22/08/2014 fixed bug 467 v 2.2.0.1
                duplicateBV();

            var oldSide = GlobalTypes.ESide.BothSides;

            bv.BVIndex = dlgBVIndex;
            bv.BVName = dlgBVName;

            if (ListType == EListType.DUAL_LIST)
            {
                bv2.BVIndex = dlgBVIndex2;
                bv2.BVName = dlgBVName;
                bv2.IsRenamed = true;

                listView2.Items[index].Text = bv2.FullName;
                listView1.Items[index + 1].Selected = true;
                listView1.Items[index + 1].ForeColor = EXAMINED_BV_COLOR;
            }
            else
            {
                oldSide = bv.BVSide;
                bv.BVSide = dlgBVSide;
            }

            bv.IsRenamed = true;
            listView1.Items[index].Text = bv.FullName;
            bvRows[0].Name = bv.BVName;
            bvRows[0].BV_Index = bv.BVIndex;

            if (ListType == EListType.SINGLE_LIST)
            {
                bvRows[0].Side = (int)bv.BVSide;
            }
            else
            {
                bvRows2[0].Name = bv2.BVName;
                bvRows2[0].BV_Index = bv2.BVIndex;
            }

            foreach (var gateRow in gatesRows)
            {
                if (ListType == EListType.SINGLE_LIST)
                {
                    gateRow.Name = bv.FullName;
                    gateRow.Side = (byte)bv.BVSide;
                    gateRow.BVIndex = bv.BVIndex;

                    SummaryScreen.RenameGate(gateRow, oldSide);

                    foreach (var gv in LayoutManager.Instance.GateViews)
                    {
                        var oldside = ".jpg";
                        if (oldSide == GlobalTypes.ESide.Left)
                            oldside = "_L.jpg";
                        else if (oldSide == GlobalTypes.ESide.Right)
                            oldside = "_R.jpg";
                        else if (oldSide == GlobalTypes.ESide.Middle)
                            oldside = "_M.jpg";

                        var newside = ".jpg";
                        if (bv.BVSide == GlobalTypes.ESide.Left)
                            newside = "_L.jpg";
                        else if (bv.BVSide == GlobalTypes.ESide.Right)
                            newside = "_R.jpg";
                        else if (bv.BVSide == GlobalTypes.ESide.Middle)
                            newside = "_M.jpg";

                        var sourceFileName = Path.Combine(Constants.SUMMARY_IMG_PATH, gv.GateIndex + oldside);
                        var destFileName = Path.Combine(Constants.SUMMARY_IMG_PATH, gv.GateIndex + newside);
                        Files.Move(sourceFileName, destFileName);

                        //BugFix #399 - Rename modified BV gate and tmp images (if exits) force image regeneration.
                        sourceFileName = Path.Combine(Constants.GATES_IMAGE_PATH, gv.GateIndex + oldside);
                        destFileName = Path.Combine(Constants.GATES_IMAGE_PATH, gv.GateIndex + newside);
                        Files.Move(sourceFileName, destFileName);


                        // force creation of tmpImage for the patient report gate image.
                        if (MainForm.Instance.WorkingMode != GlobalTypes.EWorkingMode.ExameMode) // added by Alex 10/09/2014 fixed bug #566 v 2.2.0.10
                            gv.GenerateTmpImage();		//CR #434: Enable diagnostic study post save edit

                        sourceFileName = Path.Combine(Constants.TEMP_IMG_PATH, gv.GateIndex + oldside);
                        destFileName = Path.Combine(Constants.TEMP_IMG_PATH, gv.GateIndex + newside);
                        Files.Move(sourceFileName, destFileName);
                    }
                }
                else
                {
                    if (gateRow.Side == 0)
                    {
                        gateRow.Name = bv.FullName;
                        gateRow.BVIndex = bv.BVIndex;
                        oldSide = GlobalTypes.ESide.Left;
                    }
                    else
                    {
                        gateRow.Name = bv2.FullName;
                        gateRow.BVIndex = bv2.BVIndex;
                        oldSide = GlobalTypes.ESide.Right;
                    }

                    SummaryScreen.RenameGate(gateRow, oldSide);
                }
            }

            RimedDal.Instance.UpdateGateExamination();
            if (MainForm.Instance.WorkingMode == GlobalTypes.EWorkingMode.LoadMode)	// added by Alex 22/08/2014 fixed bug #468 & #509 v 2.2.0.1
                Save(true, false); // it must be "true, false" by Alex 09/09/2014 fixed bugs #547 and #509 v2.2.0.9

            return true;
        }

        #endregion rename BV

        public void ModeChanged()
        {
            if (InvokeRequired)
            {
                BeginInvoke((Action)ModeChanged);
                return;
            }

            if (ListType == EListType.DUAL_LIST)
            {
                dualModeChanged();
                return;
            }

            ListView.SelectedIndexCollection sc;
            if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Offline)
            {
                // the system move to FREEZE mode.
                sc = listView1.SelectedIndices;
                if (sc.Count == 0)
                    return;

                ((BV)listView1.Items[sc[0]].Tag).BVStatus = BV.EBVStatus.Examinated;
                //TODO: save BV to previous BV variable +_+ 
                listView1.Items[sc[0]].ForeColor = EXAMINED_BV_COLOR;
                listView1.Items[sc[0]].Focused = true; 
            }
            else
            {
                if (ExaminationWasSaved)
                    m_addGates2SavedExam = true;

                // the system move to UNFREEZE mode.
                sc = listView1.SelectedIndices;
                if (sc.Count == 0)
                    return;

                if (((BV)listView1.Items[sc[0]].Tag).BVStatus == BV.EBVStatus.Examinated)
                {
                    // TODO: check if we should duplicate the BV refers to user setting in the setup.

                    // this function display the new BV from the data Structure and set it as selected BV.
                    duplicateBV();

                    // update the gateViews in case the probe change.
                    if (m_probeChanged)
                        m_probeChanged = false;
                }
                else
                {
                    m_tmpDepth = -1;
                    ((BV)listView1.Items[sc[0]].Tag).UpdateIndex();	
                }
				
                sc = listView1.SelectedIndices;
                if (sc.Count == 0)
                    return;
					
                ((BV)listView1.Items[sc[0]].Tag).BVStatus = BV.EBVStatus.ExaminateNow;

                if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Online)
                {
                    ListViewItem item1 = listView1.Items[sc[0]];

                    ((BV)item1.Tag).SaveOnlySummaryImages = RimedDal.Instance.SaveOnlySummaryImages;
                    item1.Checked = true; // only this way list starts show state icons
                    item1.Checked = !RimedDal.Instance.SaveOnlySummaryImages;
                }
            }
        }

        private void dualModeChanged()
        {
            ListView.SelectedIndexCollection sc;
            if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Offline)
            {
                // the system move to FREEZE mode.
                sc = listView1.SelectedIndices;
                if (sc.Count == 0)
                    return;

                ((BV)listView1.Items[sc[0]].Tag).BVStatus = BV.EBVStatus.Examinated;
                listView1.Items[sc[0]].ForeColor = EXAMINED_BV_COLOR;
                listView1.Items[sc[0]].Focused = true;

                ((BV)listView2.Items[sc[0]].Tag).BVStatus = BV.EBVStatus.Examinated;
                listView2.Items[sc[0]].ForeColor = EXAMINED_BV_COLOR;
                listView2.Refresh();
            }
            else
            {
                if (ExaminationWasSaved)
                    m_addGates2SavedExam = true;

                // the system move to UNFREEZE mode.
                sc = listView1.SelectedIndices;
                if (sc.Count == 0)
                    return;
					
                if (((BV)listView1.Items[sc[0]].Tag).BVStatus == BV.EBVStatus.Examinated)
                {
                    // TODO: check if we should duplicate the BV refers to user setting in the setup.

                    // this function display the new BV from the data Structure and set it as selected BV.
                    dualDuplicateBV();

                    // update the gateViews in case the probe change.
                    if (m_probeChanged)
                        m_probeChanged = false;
                }
                else
                {
                    m_tmpDepth = -1;
                    ((BV)listView1.Items[sc[0]].Tag).UpdateIndex();			
                    ((BV)listView2.Items[sc[0]].Tag).UpdateIndex();			
                }
				
                sc = listView1.SelectedIndices;
                if (sc.Count == 0)
                    return;
					
                ((BV)listView1.Items[sc[0]].Tag).BVStatus = BV.EBVStatus.ExaminateNow;
                ((BV)listView2.Items[sc[0]].Tag).BVStatus = BV.EBVStatus.ExaminateNow;

                if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Online)
                {
                    ListViewItem item1 = listView1.Items[sc[0]];
                    ListViewItem item2 = listView2.Items[sc[0]];

                    ((BV)item1.Tag).SaveOnlySummaryImages = RimedDal.Instance.SaveOnlySummaryImages;
                    item1.Checked = true; // only this way list starts show state icons
                    item1.Checked = !RimedDal.Instance.SaveOnlySummaryImages;

                    ((BV)item2.Tag).SaveOnlySummaryImages = RimedDal.Instance.SaveOnlySummaryImages;
                    item2.Checked = true; // only this way list starts show state icons
                    item2.Checked = !RimedDal.Instance.SaveOnlySummaryImages;
                }
            }
        }
        
        private void deleteFromSummaryScreen(dsGateExamination.tb_GateExaminationRow row)
        {
            var filter = "Gate_Index <> '" + row.Gate_Index + "' AND Depth = " + row.Depth + " AND Name = '" + row.Name + "' AND InSummaryScreen = true";
            var rows = RimedDal.Instance.GateExaminationsDS.tb_GateExamination.Select(filter, null, DataViewRowState.CurrentRows);
            if (rows.Length != 0)
            {
                var gateRow = rows[0] as dsGateExamination.tb_GateExaminationRow;
                SummaryScreen.RemoveGate(gateRow);
                deleteGateData(gateRow);
            }

            foreach (var subRow in rows)
                subRow.Delete();

            RimedDal.Instance.UpdateCurExam2DB(RimedDal.Instance.IsTemporaryLoadedExamination);
        }

        public void DeleteFromSummaryScreen(string subExaminationIndex)
        {
            SelectBVbyIndex(subExaminationIndex); // added by Alex 19/08/2014 fixed bug 467 v 2.1.7.6
            duplicateBV(); // added by Alex 19/08/2014 fixed bug 467 v 2.1.7.6

            var filter = string.Format("SubExaminationIndex = '{0}'", subExaminationIndex);

            var rows = RimedDal.Instance.GateExaminationsDS.tb_GateExamination.Select(filter);
            foreach (var row in rows)
            {
                deleteGateData(row as dsGateExamination.tb_GateExaminationRow);
            }

            rows = RimedDal.Instance.SubExaminationsDS.tb_SubExamination.Select(filter);
            foreach (DataRow row in rows)
                row.Delete();

            RimedDal.Instance.UpdateCurExam2DB(RimedDal.Instance.IsTemporaryLoadedExamination);
            LayoutManager.Instance.UpdateLindegaardRatio();
        }
        
        /// <summary>
        /// Deletes GateExamination and BVExamination records for removed gate from DB
        /// Deletes all linked data and image files
        /// RIMD-343: If delete the BV from Summary screen - it is still saved in the list in Main Screen
        /// </summary>
        /// <param name="row"></param>
        private void deleteGateData(dsGateExamination.tb_GateExaminationRow row)
        {
            if (row == null)
                return;

            if (InvokeRequired)
            {
                BeginInvoke((DeleteGateDataDelegate)deleteGateData, row);
                return;
            }

            var gateIndex           = row.Gate_Index.ToString();
            var subexaminationIndex = row.SubExaminationIndex.ToString();
            var bvexaminationIndex  = row.BVExaminationIndex.ToString();
            var itemsCount          = listView1.Items.Count;

            for (var i = 0; i < itemsCount; i++)
            {
                if (((BV) (listView1.Items[i].Tag)).BVExaminationIndex != row.BVExaminationIndex) 
                    continue;

                var count = 0;
                for (var j = 0; j < itemsCount; j++)
                {
                    if (((BV)listView1.Items[i].Tag).BVIndex == ((BV)listView1.Items[j].Tag).BVIndex)
                        count++;
                }

				// OFER: Enable diagnostic post save edit
				if (count > 1 || (count == 1 && LayoutManager.IsPostSaveEditEnabled))
                {
                    listView1.Items.RemoveAt(i);
                    m_bvCollection.RemoveAt(i);
                    itemsCount--;
                }
				else
                {
                    m_bvCollection[i].BVStatus = BV.EBVStatus.NotExaminatedYet;
                    UpdateBVList(((BV)listView1.Items[i].Tag).BVIndex, false);
                }

                if (i < itemsCount)
                {
                    listView1.Items[i].Selected = true;
                    listView1.Items[i].Focused = true;
                }
                else
                {
                    listView1.Items[itemsCount - 1].Selected = true;
                    listView1.Items[itemsCount - 1].Focused = true;
                }

                break;
            }

            RimedDal.Instance.DeleteRow(row);

            var filterBVExamination = "BVExaminationIndex = '" + bvexaminationIndex + "'";
            var rows = RimedDal.Instance.BVExaminationsDS.tb_BVExamination.Select(filterBVExamination, null, DataViewRowState.CurrentRows);
            if (rows.Length > 0)
                RimedDal.Instance.DeleteRow(rows[0]);
            
            //remove all Gate files
            deleteFileByIndex(Constants.TEMP_IMG_PATH, gateIndex);
            deleteFileByIndex(Constants.REPLAY_IMG_PATH, gateIndex);
            deleteFileByIndex(Constants.SUMMARY_IMG_PATH, gateIndex);
            deleteExamRawDataFolder(subexaminationIndex);
        }

        private void deleteFileByIndex(string directory, string index)
        {
            var filelist = Directory.GetFiles(directory);
            foreach (var filename in filelist)
            {
                if (filename.IndexOf(index, StringComparison.InvariantCultureIgnoreCase) != -1)
                {
                    Files.Delete(filename);
                    break;
                }
            }
        }

        private void deleteExamRawDataFolder(string examIndex)
        {
            if (string.IsNullOrWhiteSpace(examIndex))
                return;

            var path = Path.Combine(Constants.RAW_DATA_PATH, examIndex);
            if (!Directory.Exists(path))
                return;

            if (string.Compare(Constants.RAW_DATA_PATH, path, StringComparison.InvariantCultureIgnoreCase) == 0)
                return;

            Files.DeleteFolder(path);
        }

        private dsBVExamination.tb_BVExaminationRow[] renameGetBVExaminationRows(Guid index)
        {
            var filter = "BVExaminationIndex = '" + index + "'";
            var rows = RimedDal.Instance.BVExaminationsDS.tb_BVExamination.Select(filter) as dsBVExamination.tb_BVExaminationRow[];
            
            return rows;
        }

        private dsGateExamination.tb_GateExaminationRow[] renameGetGates(Guid subExamIndex)
        {
            var filter = "SubExaminationIndex = '" + subExamIndex.ToString() + "'";
            var rows = RimedDal.Instance.GateExaminationsDS.tb_GateExamination.Select(filter) as dsGateExamination.tb_GateExaminationRow[];
            
            return rows;
        }

        private bool isGateExistInSummary(dsGateExamination.tb_GateExaminationRow row)
        {
            var filter = "Gate_Index <> '" + row.Gate_Index.ToString() + "' AND Depth = " + row.Depth.ToString() + " AND Name = '" + row.Name + "' AND InSummaryScreen = true";
            var rows = RimedDal.Instance.GateExaminationsDS.tb_GateExamination.Select(filter, null, DataViewRowState.CurrentRows);
            return rows.Length != 0;
        }

        private GlobalTypes.EArrayAction inSummaryScreen(dsGateExamination.tb_GateExaminationRow row)
        {
            if (InSummaryStatus == GlobalTypes.EAdd2SummaryScreen.OpenSpectrum)
                return GlobalTypes.EArrayAction.Add;

            if (InSummaryStatus == GlobalTypes.EAdd2SummaryScreen.SelectedSpectrum)
            {
                if ((bool)row["InSummaryScreen"])
                    return GlobalTypes.EArrayAction.Add;
            }
            else if (isGateExistInSummary(row))
            {
                switch (InSummaryStatus)
                {
                    case GlobalTypes.EAdd2SummaryScreen.CustomizeReplaceOld:
                            deleteFromSummaryScreen(row);
                        return GlobalTypes.EArrayAction.Add;

					case GlobalTypes.EAdd2SummaryScreen.CustomizeAddNew:
                        return GlobalTypes.EArrayAction.Add;

					case GlobalTypes.EAdd2SummaryScreen.CustomizeAsk:
                            using (var dlg = new AddGV2SummaryDlg())
                            {
								dlg.TopMost = true;
	                            var res = dlg.ShowDialog(MainForm.Instance);
	                            Focus();
								if (res == DialogResult.OK)
								{
									if (dlg.Add2SummaryScreen == GlobalTypes.EAdd2SummaryScreen2.AddNew)
										return GlobalTypes.EArrayAction.Add;

									if (dlg.Add2SummaryScreen == GlobalTypes.EAdd2SummaryScreen2.ReplaceOld)
									{
										deleteFromSummaryScreen(row);
										return GlobalTypes.EArrayAction.Add;
									}
								}
							}
                        break;
                }
            }
            else
                return GlobalTypes.EArrayAction.Add;

            return GlobalTypes.EArrayAction.DoNothing;
        }

        private delegate void SaveLayoutDelegate(Guid index, List<GateView> gatesViews, bool displayAutoscan0, bool displayAutoscan1);
        
		/// <summary>Save the current layout to the local dataset's. Invoked by the LayoutManager, when the system move to offline (Freeze mode).</summary>
        /// <param name="subExamId"></param>
        /// <param name="gatesViews"></param>
        /// <param name="displayAutoscan0"></param>
        /// <param name="displayAutoscan1"></param>
        public void SaveLayout(Guid subExamId, List<GateView> gatesViews, bool displayAutoscan0, bool displayAutoscan1)
        {
            if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Monitoring && MainForm.Instance.IsMonitoringSaved)
                return;

            if (gatesViews == null || gatesViews.Count == 0)    
                return;

            //RIMD-562: Digi-Lite is stuck after MC or Summary Screen when using Remote Control
            if (InvokeRequired)
            {
                Invoke((SaveLayoutDelegate)SaveLayout, subExamId, gatesViews, displayAutoscan0, displayAutoscan1);  //Ofer, v1.18.2.11 -  BugFix Unhandled exception System.InvalidOperationException: Collection was modified; enumeration operation may not execute (ErrorCode: 0x80131509). - Replace BeginInvoke with Invoke call
                return;
            }

			var examRow = RimedDal.Instance.LoadedExamination();
			if (examRow == null)
			{
				Logger.LogError("SaveLayout(): No examination loaded. Action aborted.");
				return;
			}

            //Ofer, v1.18.2.9: Hide System.Data.ConstraintException. Message: Column 'BVExaminationIndex' is constrained to be unique.  Value '' is already present.
            try
		    {
                if (MainForm.Instance.IsNotUnilateralStudy)
					saveBilateralLayout(examRow.Examination_Index, subExamId, gatesViews, displayAutoscan0, displayAutoscan1);
                else
					saveUnilateralLayout(examRow.Examination_Index, subExamId, gatesViews, displayAutoscan0, displayAutoscan1);
            }
		    catch (Exception ex)
		    {
		        Logger.LogError(ex);
		    }
        }

        private void saveUnilateralLayout(Guid examIndex, Guid subExamId, List<GateView> gatesViews, bool displayAutoscan0, bool displayAutoscan1)
        {
            var sc1 = listView1.SelectedIndices;

            if (sc1.Count == 0)        
                return;

            var index = sc1[0];        
            if (index < 0 || index >= listView1.Items.Count)
                return;

            //Ofer
            var bv = listView1.Items[index].Tag as BV;
            if (bv == null)
            {
                Logger.LogError(string.Format("{0}.saveUnilateralLayout(subExamId={1},...): NO BV Selected", Name, subExamId));
                return;
            }

	        //var examIndex					= (Guid)RimedDal.Instance.ExaminationsDS.tb_Examination.Rows[0]["Examination_Index"];
            var subExamRow                  = RimedDal.Instance.SubExaminationsDS.tb_SubExamination.Newtb_SubExaminationRow();
            subExamRow.SubExaminationIndex  = subExamId;
            subExamRow.ExaminationIndex     = examIndex;
            subExamRow.BackupPath           = string.Empty;
            subExamRow.Notes                = SubExamNotes;
            subExamRow.Deleted              = false;
            subExamRow.TotalMilliseconds    = gatesViews[0].Spectrum.Graph.LastDrawTime.TotalMilliseconds;

            // save the selected BV. It's important to save at the first an examination and only then start initialize summary view controls
            var bvExamRow               = RimedDal.Instance.BVExaminationsDS.tb_BVExamination.Newtb_BVExaminationRow();
            bvExamRow.ExaminationIndex  = examIndex;
            bv.SubExamIndex             = subExamId;
            bv.CopyTo(bvExamRow);
            bvExamRow.Deleted           = false;

            try
            {
                RimedDal.Instance.BVExaminationsDS.tb_BVExamination.Rows.Add(bvExamRow);
            }
            catch (Exception ex)
            {
                var msg = string.Format("{0}.saveUnilateralLayout(subExamId={1}....): BVExamination.Add(BVExaminationIndex={2}) FAIL. examIndex={3}, bvExamRow.Name={4}.", Name, subExamId, bvExamRow.BVExaminationIndex, examIndex, bvExamRow.Name);
                Logger.LogError(msg, ex);

	            return;
            }

            subExamRow.DisplayAutoscan0 = displayAutoscan0;
            subExamRow.DisplayAutoscan1 = displayAutoscan1;

            try
            {
                RimedDal.Instance.SubExaminationsDS.tb_SubExamination.Rows.Add(subExamRow);
            }
            catch (Exception ex)
            {
                var msg = string.Format("{0}.saveUnilateralLayout(subExamId={1}....): SubExamination.Add(SubExaminationIndex={2}) FAIL. examIndex={3}, bvExamRow.Name={4}.", Name, subExamId, subExamRow.SubExaminationIndex, examIndex, bvExamRow.Name);
                Logger.LogError(msg, ex);

	            return;
            }

            var gates = gatesViews.ToArray();   //Ofer, v1.18.2.9: Avoid - collection modified during iteration exception
            foreach (var gv in gates)
            {
                if (gv == null || !gv.Visible)  
                    continue;

                if (Guid.Empty.Equals(bv.BVExaminationIndex)) //Ofer, 2.0.4.12
                {
                    Logger.LogWarning("{0}.saveUnilateralLayout(subExamId={1},...): BVExaminationIndex is empty. Gate save aborted. Gate: {2}.", Name, subExamId, gv);
                    continue;
                }

                var gateExamRow         = RimedDal.Instance.GateExaminationsDS.tb_GateExamination.Newtb_GateExaminationRow();
                gateExamRow.Gate_Index  = Guid.NewGuid();

                // TODO: need to check if the new Guid already exist. - Gedalia.
                gv.CopyTo(gateExamRow);
                gateExamRow.ExaminationIndex		= examIndex;
                gateExamRow.BVExaminationIndex      = bv.BVExaminationIndex;
                gateExamRow.BVIndex                 = bv.BVIndex;
                gateExamRow.SubExaminationIndex     = subExamId;
                gateExamRow.Deleted                 = false;
                gateExamRow.OnSummaryScreenIndex    = m_currentSummaryScreenIndex;
                // v.2.02.05.000 Patient report improvement (summary section) feature.
                gateExamRow.ReportVersion = (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Monitoring) ? 1 : 2;
                m_currentSummaryScreenIndex++;										    

                try
                {
                    RimedDal.Instance.GateExaminationsDS.tb_GateExamination.Rows.Add(gateExamRow);
                }
                catch (Exception ex)
                {
                    var msg = string.Format("{0}.saveUnilateralLayout(subExamId={1}....): GateExamination.Add(GateIndex={2}) FAIL. examIndex={3}, bvExamRow.Name={4}.", Name, subExamId, gateExamRow.Gate_Index, examIndex, bvExamRow.Name);
                    Logger.LogError(msg, ex);
                }

                //RIMD-399: Parameter values on screen not change for 4 Mhz CW probe (when using Remote or not)
                if (gateExamRow.InSummaryScreen)
                {
                    SummaryScreen.AddGate(gateExamRow);
                }
                else if (inSummaryScreen(gateExamRow) == GlobalTypes.EArrayAction.Add)
                {
                    gateExamRow.InSummaryScreen = true;
                    SummaryScreen.AddGate(gateExamRow);
                }
            }
        }

	    /// <summary>Save the current layout to the local dataset's. Invoked by the LayoutManager, when the system move to offline (Freeze mode).</summary>
	    /// <param name="examIndex"></param>
	    /// <param name="subExamId"></param>
	    /// <param name="gatesViews"></param>
	    /// <param name="displayAutoscan0"></param>
	    /// <param name="displayAutoscan1"></param>
	    private void saveBilateralLayout(Guid examIndex, Guid subExamId, List<GateView> gatesViews, bool displayAutoscan0, bool displayAutoscan1)
        {
            var sc1 = listView1.SelectedIndices;
            var sc2 = listView2.SelectedIndices;

            if (sc1.Count == 0 || sc2.Count == 0)
                return;

            var bv = listView1.Items[sc1[0]].Tag as BV;
            if (bv == null)
            {
                Logger.LogError(string.Format("{0}.saveBilateralLayout(subExamIdex={1},...): NO BV Selected", Name, subExamId));
                return;
            }

            var bv2 = listView2.Items[sc2[0]].Tag as BV;
            if (bv2 == null)
            {
                Logger.LogError(string.Format("{0}.saveBilateralLayout(subExamIdex={1},...): NO BV2 Selected", Name, subExamId));
                return;
            }

	        //var examIndex					= (Guid)RimedDal.Instance.ExaminationsDS.tb_Examination.Rows[0]["Examination_Index"];
			var subExamRow					= RimedDal.Instance.SubExaminationsDS.tb_SubExamination.Newtb_SubExaminationRow();
            subExamRow.SubExaminationIndex  = subExamId;
            subExamRow.ExaminationIndex     = examIndex;
            subExamRow.BackupPath           = "";
            subExamRow.Notes                = SubExamNotes;
            subExamRow.Deleted              = false;
            subExamRow.TotalMilliseconds    = gatesViews[0].Spectrum.Graph.LastDrawTime.TotalMilliseconds;

            // save the selected BV.
            var bvExamRow               = RimedDal.Instance.BVExaminationsDS.tb_BVExamination.Newtb_BVExaminationRow();
            bvExamRow.ExaminationIndex  = examIndex;

            bv.SubExamIndex             = subExamId;
			bv.CopyTo(bvExamRow);
            bvExamRow.Deleted = false;

            try
            {
                RimedDal.Instance.BVExaminationsDS.tb_BVExamination.Rows.Add(bvExamRow);
            }
            catch (Exception ex)
            {
                var msg = string.Format("{0}.saveBilateralLayout(subExamId={1}....): BV1.BVExamination.Add(BVExaminationIndex={2}) FAIL. examIndex={3}, bvExamRow.Name={4}.",
                                            Name, subExamId, bvExamRow.BVExaminationIndex, examIndex, bvExamRow.Name);
                Logger.LogError(msg, ex);
				return;
            }

            // Port 2
            var bvExamRow2              = RimedDal.Instance.BVExaminationsDS.tb_BVExamination.Newtb_BVExaminationRow();
            bvExamRow2.ExaminationIndex = examIndex;

            bv2.SubExamIndex            = subExamId;
			bv2.CopyTo(bvExamRow2);
            bvExamRow2.Deleted          = false;

            try
            {
                RimedDal.Instance.BVExaminationsDS.tb_BVExamination.Rows.Add(bvExamRow2);
            }
            catch (Exception ex)
            {
                var msg = string.Format("{0}.saveBilateralLayout(subExamId={1}....): BV2.BVExamination.Add(BVExaminationIndex={2}) FAIL. examIndex={3}, bvExamRow.Name={4}.",
                                            Name, subExamId, bvExamRow2.BVExaminationIndex, examIndex, bvExamRow.Name);
                Logger.LogError(msg, ex);

				throw;	//TODO: check if throw required, can it be replaced with return?
            }

            subExamRow.DisplayAutoscan0 = displayAutoscan0;
            subExamRow.DisplayAutoscan1 = displayAutoscan1;
            try
            {
                RimedDal.Instance.SubExaminationsDS.tb_SubExamination.Rows.Add(subExamRow);
            }
            catch (Exception ex)
            {
                var msg = string.Format("{0}.saveBilateralLayout(subExamId={1}....): SubExamination.Add(SubExaminationIndex={2}) FAIL. examIndex={3}, bvExamRow.Name={4}.",
                                            Name, subExamId, subExamRow.SubExaminationIndex, examIndex, bvExamRow.Name);
                Logger.LogError(msg, ex);
				return;
            }

            var gates = gatesViews.ToArray();   //Ofer, v1.18.2.9: Overcome collection modified during iteration exception
            foreach (var gv in gates)
            {
                if (gv == null || !gv.Visible)
                    continue;

                var gateExamRow = RimedDal.Instance.GateExaminationsDS.tb_GateExamination.Newtb_GateExaminationRow();
                gateExamRow.Gate_Index = Guid.NewGuid();

                // TODO: need to check if the new Guid already exist. - Gedalia.
                gv.CopyTo(gateExamRow);
                gateExamRow.ExaminationIndex = examIndex;
                gateExamRow.OnSummaryScreenIndex = m_currentSummaryScreenIndex;
                gateExamRow.ReportVersion = (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Monitoring) ? 1 : 2;
                m_currentSummaryScreenIndex++;										

                if (gv.Id < 8)
                {
                    gateExamRow.BVExaminationIndex = bv.BVExaminationIndex;
                    gateExamRow.BVIndex = bv.BVIndex;
                }
                else
                {
                    gateExamRow.BVExaminationIndex = bv2.BVExaminationIndex;
                    gateExamRow.BVIndex = bv2.BVIndex;
                }

                gateExamRow.SubExaminationIndex = subExamId;
                gateExamRow.Deleted = false;

                // RIMD-399: Parameter values on screen not change for 4 Mhz CW probe (when using Remote or not)
                /*if (gv.Probe.IsCW)
                        gateExamRow.Depth = 0;*/

                if (gateExamRow.InSummaryScreen)
                {
                    SummaryScreen.AddGate(gateExamRow);
                }
                else if (inSummaryScreen(gateExamRow) == GlobalTypes.EArrayAction.Add)
                {
                    gateExamRow.InSummaryScreen = true;
                    SummaryScreen.AddGate(gateExamRow);
                }

                try
                {
                    RimedDal.Instance.GateExaminationsDS.tb_GateExamination.Rows.Add(gateExamRow);
                }
                catch (Exception ex)
                {
                    var msg = string.Format("Gate save error. GateExamination.Add(gateExamRow): saveBilateralLayout(subExamId={0}, gatesViews, displayAutoscan0={1}, displayAutoscan1={2}): gateExamRow.Name={3}, gateExamRow.BVName={4}.",    //Ofer, v1.18.2.9
                                                        subExamId, displayAutoscan0, displayAutoscan1, gateExamRow.Name, gateExamRow.BVName);
                    Logger.LogError(ex, msg);
                }
            }
        }

        private void displayDepth(object obj, double depth)
        {
            if (InvokeRequired)
            {
                BeginInvoke((DepthChangedDelegate)displayDepth, obj, depth);
                return;
            }

            if (obj is GateView)
                if (((GateView)obj).Selected) // added by Alex 11/06/2015 fixed bug #786 v 2.2.2.37
                    setSelectedIndexDepth((GateView.SideSelected == 0 ? listView1 : listView2), depth);
        }

        private void setSelectedIndexDepth(ListView listView, double depth) 
        {
            if (listView == null)
                return;

            var sc = listView.SelectedIndices;
            if (sc.Count == 0)
                return;

            var index = sc[0];
            if (index < 0 || index >= listView.Items.Count)
                return;

            var bv = listView.Items[index].Tag as BV;
            if (bv == null)
                return;

            if (bv.BVStatus == BV.EBVStatus.Examinated)
            {
                m_tmpDepth = depth;
            }
            else
            {
                bv.DefaultDepth = depth;
                setItemDepth(bv, listView.Items[index]);
            }
        }

        public StudyLayout GetLayout(BV[] bvArr)
        {
            var gates			= new int[bvArr.Length];
            var displayAutoscan = new bool[bvArr.Length];
            var studyLayout		= new StudyLayout();

            for (var i = 0; i < bvArr.Length; ++i)
            {
                var filter = "BVExaminationIndex = '" + bvArr[i].BVExaminationIndex + "' ";
                var gateExamRows = RimedDal.Instance.GateExaminationsDS.tb_GateExamination.Select(filter);

                gates[i] = gateExamRows.Length;

                if (gateExamRows.Length == 0)
                    continue;

                filter = "SubExaminationIndex = '" + ((Guid)gateExamRows[0]["SubExaminationIndex"]) + "'";
                var subExamRows = RimedDal.Instance.SubExaminationsDS.tb_SubExamination.Select(filter);

                var fw = subExamRows.Length;
                if (fw > 0)
                    displayAutoscan[i] = (bool)subExamRows[0]["DisplayAutoscan" + i];
                else
                    displayAutoscan[i] = true;
            }

            studyLayout.GateNumLeft = gates[0];
            studyLayout.DispAutoscan0 = displayAutoscan[0];

            if (bvArr.Length > 1)
            {
                studyLayout.GateNumRight = gates[1];
                studyLayout.DispAutoscan1 = displayAutoscan[1];
            }
            else
            {
                studyLayout.GateNumRight = 0;
                studyLayout.DispAutoscan1 = false;
            }
            return studyLayout;
        }

        public void ProbeChanged(Probe newProbe)
        {
            if (InvokeRequired)
            {
                BeginInvoke((ProbeChangedDelegate)ProbeChanged, newProbe);
                return;
            }

            var index = newProbe.ProbeIndex;

            //OFER [2013.03.27]: ListView listView = (ListView)this.ListViewArr[index];
            var listView = DoubleLists[index];
            var sc       = listView.SelectedIndices;

            // check if the selected BV was exterminated.
            if (sc.Count <= 0)
                return;

            var item = listView.Items[sc[0]];
            var bv = item.Tag as BV;
            if (bv == null)
                return;

            if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Online)
            {
                bv.BVProbe = newProbe;
                setItemDepth(bv, item);
            }
            else
            {
                if (bv.BVStatus == BV.EBVStatus.NotExaminatedYet)
                {
                    bv.BVProbe = newProbe;
                    setItemDepth(bv, item);
                }
                else
                {
                    m_tmpProbe = newProbe;
                    m_probeChanged = true;
                }
            }
        }

        public DataView GetGateExam(String filter)
        {
            var dv = new DataView(RimedDal.Instance.GateExaminationsDS.tb_GateExamination, filter, null, DataViewRowState.CurrentRows);
            return dv;
        }

        /// <summary>Save the DATA STRUCTURE to the DATA BASE. Called when the user press on the save button of the summary screen.</summary>
		public bool Save(bool directSaving, bool confirmationPopup = true)	// added by Alex 22/08/2014 fixed bug 468 & 509 v 2.2.0.1
        {
            var strRes = MainForm.StringManager;

            if (!directSaving)
            {
                // Added by Alex bug #838 v.2.2.3.26 31/03/2016
                MainForm.Instance.CurrentAccessionNumber = "";
				// OFER: Enable diagnostic post save edit
				if (	(!ExaminationWasSaved
							&& (LayoutManager.SystemLayoutMode != GlobalTypes.ESystemLayoutMode.DiagnosticLoad || LayoutManager.IsPostSaveEditEnabled)
							&& LayoutManager.SystemLayoutMode != GlobalTypes.ESystemLayoutMode.Monitoring
							&& RimedDal.Instance.GateExaminationsDS.tb_GateExamination.Rows.Count != 0
						)
						|| m_addGates2SavedExam 
						|| !EventListViewCtrl.Instance.EventListSaved
				   )
                {
					var res = LoggedDialog.ShowWarning(MainForm.Instance, strRes.GetString("You have unsaved tests. Do you want to save them?"), buttons: MessageBoxButtons.YesNo);
                    if (res == DialogResult.No)
                    {
						MainForm.Instance.FinishStudy(false);	

                        if (!EventListViewCtrl.Instance.EventListSaved)
                            EventListViewCtrl.Instance.DeleteCurEventFiles();
							
                        if (RimedDal.Instance.IsTemporaryLoadedExamination)
                            RimedDal.Instance.DeleteCurrentExamination();
                        
						return false; // examination was not saved.
                    }
                }
                else
                {
					// Must call this, MainForm is not a single caller for this func!
					MainForm.Instance.FinishStudy(false); //v2.0.0.0.37	Marina This is patch, objects must be reorganized and no other object may call methods of the MainForm
					
                    return false; // examination was not saved.
                }
            }

            EventListViewCtrl.Instance.SaveEventHistory();
            if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.MonitoringLoad)
            {
                MainForm.Instance.FinishStudy(false);
                return true;
            }
			
			var examRow = RimedDal.Instance.LoadedExamination();
			if (examRow == null)
            {
				LoggedDialog.ShowError(MainForm.Instance, "Save operation fail./nExamination data is invalid.");
                return false;
            }

            var patientIndex = examRow.Patient_Index;
            // check if the current user is undefined user.
			if (patientIndex == Guid.Empty && !m_addGates2SavedExam && confirmationPopup)	// added by Alex 22/08/2014 fixed bug 468 & 509 v 2.2.0.1
            {
                // open patient list.
                using (var dlg = new PatientListDlg())
                {
	                dlg.TopMost = true;
                    var res = dlg.ShowDialog(MainForm.Instance);
	                Focus();
                    if (res == DialogResult.OK)
                    {
                        patientIndex = dlg.SelPatientIndex;

                        ExaminationWasSaved = true; // this flag is set to true in order that the next time this function will be called, it will return immediately.
                        if (!(LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Monitoring && !MainForm.Instance.IsMonitoringSaved))
                            MainForm.Instance.SelectedPatientIndex = patientIndex;
                    }
                    else if (res == DialogResult.Cancel)
                    {
						if (!directSaving)									
                        	RimedDal.Instance.DeleteCurrentExamination();	

						LoggedDialog.ShowNotification(MainForm.Instance, strRes.GetString("Examination was not saved."));
                        MainForm.Instance.FinishStudy(false); 

                        return false;
                    }
                }
            }

            if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Monitoring && !MainForm.Instance.IsMonitoringSaved)
            {
                var g = Guid.NewGuid();
                SaveLayout(g, LayoutManager.Instance.GateViews, LayoutManager.Instance.Autoscans[Probe.PROB_SIDE_LEFT].Display, LayoutManager.Instance.Autoscans[Probe.PROB_SIDE_RIGHT].Display);
            }

			if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Diagnostic || LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.DiagnosticLoad) 
			{
                SummaryScreen.SaveImage2HD(MainForm.Instance.WorkingMode == GlobalTypes.EWorkingMode.LoadMode); // added by Alex 04/09/2014 fixed bug #547 v 2.2.0.7
				MainForm.Instance.FinishStudy(true);   
			}

            examRow.Patient_Index = patientIndex;
            examRow.Date = DateTime.Now;
            examRow.Study_Index = MainForm.Instance.CurrentStudyId;
            examRow.AccessionNumber = MainForm.Instance.CurrentAccessionNumber;
            MainForm.Instance.CurrentAccessionNumber = "";

            if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Diagnostic)		
	            LayoutManager.SystemLayoutMode = GlobalTypes.ESystemLayoutMode.DiagnosticLoad;

            RimedDal.Instance.UpdateCurExam2DB();
			
            if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Monitoring)
                MainForm.Instance.IsMonitoringSaved = true;

            ExaminationWasSaved             = true;
            RecordingExaminationId          = Guid.Empty;
            MainForm.Instance.WorkingMode   = GlobalTypes.EWorkingMode.LoadMode;    

            HardDriveSpaceKeeper.DeleteUnusedExamData();                            

            // Notification to the user.
            var filter = "Select * FROM tb_Patient WHERE (Patient_Index = '" + patientIndex.ToString() + "')";
            var ds = RimedDal.Instance.GetPatients(filter);

			if (confirmationPopup)	// added by Alex 22/08/2014 fixed bug 468 & 509 v 2.2.0.1
				LoggedDialog.ShowNotification(MainForm.Instance, strRes.GetString("Examination was saved under patient: ") + ((ds.tb_Patient[0].First_Name == "Undefined") ? "" : ds.tb_Patient[0].Last_Name) + " " + ds.tb_Patient[0].First_Name + " ID: " + ds.tb_Patient[0].ID);

            return true;
        }
        
        //Assaf : The saving mechanism of monitoring exam is a little bit different 
        public bool SaveMonitoringExam()
        {
            var patientIndex    = MainForm.Instance.SelectedPatientIndex;
            var guid            = Guid.NewGuid();

            SaveLayout(guid, LayoutManager.Instance.GateViews, LayoutManager.Instance.Autoscans[Probe.PROB_SIDE_LEFT].Display, LayoutManager.Instance.Autoscans[Probe.PROB_SIDE_RIGHT].Display);

            MainForm.Instance.IsMonitoringSaved = true;

			var examRow = RimedDal.Instance.LoadedExamination();
			if (examRow != null)
			{
				examRow.Patient_Index = patientIndex;
				examRow.Date = DateTime.Now;
				examRow.Study_Index = MainForm.Instance.CurrentStudyId;
			}
			
			//if (RimedDal.Instance.ExaminationsDS.tb_Examination.Rows.Count > 0)
			//{
			//    var row = RimedDal.Instance.ExaminationsDS.tb_Examination.Rows[0] as dsExamination.tb_ExaminationRow;
			//    if (row != null)
			//    {
			//        row.Patient_Index   = patientIndex;
			//        row.Date            = DateTime.Now;
			//        row.Study_Index     = MainForm.Instance.CurrentStudyId;
			//    }
			//}

            RimedDal.Instance.UpdateCurExam2DB();           
           	ExaminationWasSaved = true;
			
            return true;
        }

        public Guid GetSubExaminationId(BV bv)
        {
            var filter = "BVExaminationIndex = '" + bv.BVExaminationIndex + "'";
            var gateExamRows = RimedDal.Instance.GateExaminationsDS.tb_GateExamination.Select(filter);

            return (Guid)gateExamRows[0]["SubExaminationIndex"];
        }
        
        /// <summary>This function is called when the user load new study.</summary>
        private void clearBVList()
        {
            m_bvCollection.Clear();
        }

        private bool isExamineBVExist()
        {
            foreach (var bv in m_bvCollection)
            {
                if (bv.BVStatus == BV.EBVStatus.Examinated)
                    return true;
            }

            return false;
        }
        
        public void GVNotes(int gvId, string notes)
        {
            ListView.SelectedIndexCollection sc;
            ListView.ListViewItemCollection items;

            if (gvId % 2 == 0)
            {
                sc = listView1.SelectedIndices;
                items = listView1.Items;
            }
            else
            {
                sc = listView2.SelectedIndices;
                items = listView2.Items;
            }

            if (sc.Count == 0)
                return;

            var bv = (BV)items[sc[0]].Tag;

            if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Monitoring) 
            {
				if (!ExaminationWasSaved)
                    return;
            }
            else if (bv.BVStatus == BV.EBVStatus.NotExaminatedYet)
            	return;

            var filter = "ID = '" + gvId + "' AND BVExaminationIndex = '" + bv.BVExaminationIndex + "'";
            var rows = RimedDal.Instance.GateExaminationsDS.tb_GateExamination.Select(filter);
            if (rows.Length != 0)
            {
                rows[0]["Notes"] = notes;
                //RIMD-549: Notes are not saved for Monitoring examination v1.18.2.5
                if ((LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Monitoring || LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.MonitoringLoad) && ExaminationWasSaved)	// v1.18.2.5
                    RimedDal.Instance.UpdateGateExamination();	
            }
        }
        
        public void AddGv2SummaryScreen(int gvId)
        {
            ListView.SelectedIndexCollection sc;
            ListView.ListViewItemCollection items;

            if (gvId % 2 == 0)
            {
                sc = listView1.SelectedIndices;
                items = listView1.Items;
            }
            else
            {
                sc = listView2.SelectedIndices;
                items = listView2.Items;
            }

            if (sc.Count == 0)
                return;

            var bv = (BV)items[sc[0]].Tag;
            if (bv.BVStatus == BV.EBVStatus.NotExaminatedYet)
                return;

            var filter = "BVExaminationIndex = '" + bv.BVExaminationIndex + "' AND ID = " + gvId + " AND InSummaryScreen=false";
            var rows = RimedDal.Instance.GateExaminationsDS.tb_GateExamination.Select(filter, null, DataViewRowState.CurrentRows);
            if (rows.Length == 1)
                rows[0]["InSummaryScreen"] = true;
        }

        internal bool LoadStudy(Guid studyIndex, Guid patientIndex, bool isShowMsgBox = true)   
        {
            SelectedBV = Guid.NewGuid();
            return loadExamination(studyIndex, patientIndex, true, false, isShowMsgBox);
        }

        private bool loadExamination(Guid studyIndex, Guid patientIndex, bool createNew, bool showSummaryScreen, bool isShowMsgBox = true) 
        {
            Logger.LogTrace(Logger.ETraceLevel.L2, Name, "loadExamination(studyIndex={0}, patientIndex={1}, showSummaryScreen={2})", studyIndex, patientIndex, showSummaryScreen);
            
            try
            {
                //In case that a monitoring replay exam was terminated before it's end
                if (LayoutManager.Instance.IsPaused)
                {
                    LayoutManager.Instance.IsPaused                 = false;
                    LayoutManager.Instance.CancelWait12Thread       = false;
                    MainForm.ToolBar.ButtonPause.IsPressed   = false;
                }

                Save(false);

                dsExamination.tb_ExaminationRow examRow;
                if (createNew)
                {
                    RimedDal.Instance.ClearCurExam();
                    examRow 					= createNewExam(studyIndex, patientIndex);
                    studyIndex 					= examRow.Examination_Index;
                    RecordingExaminationId 		= studyIndex;
                    m_currentSummaryScreenIndex = 0;		
                }
                else
                {
                    examRow = RimedDal.Instance.LoadCurExam(studyIndex);
                    if (examRow == null)
                    {
						LoggedDialog.Show("Examination is already removed.\nStarting a new examination."); ; //Fix bug: message box hidden behind modal window

                        createNew                   = true;
                        RimedDal.Instance.ClearCurExam();
                        examRow                     = createNewExam(RimedDal.Instance.ConfigDS.tb_BasicConfiguration[0].LastStudy, patientIndex);
                        studyIndex                  = examRow.Examination_Index;
                        RecordingExaminationId      = studyIndex;
                        m_currentSummaryScreenIndex = 0;		
                    }
                    else
                    {
                  		patientIndex                = examRow.Patient_Index;
                        RecordingExaminationId      = Guid.Empty;
						m_currentSummaryScreenIndex = -1;														                              
                        foreach (dsGateExamination.tb_GateExaminationRow row in RimedDal.Instance.GateExaminationsDS.tb_GateExamination.Rows) 
                        {
                            if (row == null)
                                continue;

						    if (row.OnSummaryScreenIndex > m_currentSummaryScreenIndex)
                                m_currentSummaryScreenIndex = row.OnSummaryScreenIndex;
						}

                        m_currentSummaryScreenIndex++;		
                    }
                }

                var studyRow = RimedDal.Instance.GetStudy(examRow.Study_Index);
                LayoutManager.Instance.LoadExamination(createNew, examRow.IsTemporary, studyRow.Monitoring);
                MainForm.Instance.LoadStudy(studyRow, patientIndex, examRow.IsTemporary);

                clearDataStructure();
                verifyClearDataStructure();
                init(MainForm.Instance.StudyType);	

                // fill the BV List view Ctrl.
                LoadBVList(!examRow.IsTemporary);

                foreach (var autoscan in LayoutManager.Instance.Autoscans)
                {
                    autoscan.SetItemCloseVisibility(!studyRow.Monitoring);
                    autoscan.Graph.OverridenAutoScanImage = null;
                }
                
                EventListViewCtrl.Instance.LoadExamination(examRow);

                SummaryScreen.LoadExam(studyRow.Monitoring, examRow.IsTemporary, showSummaryScreen);
                LayoutManager.Instance.UpdateLindegaardRatio();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);    
                if (isShowMsgBox)
					LoggedDialog.ShowError(MainForm.StringManager.GetString("Study loading Failed.") + "\n\n" + ex.Message); ; //Fix bug: message box hidden behind modal window

                return false;   
            }

            return true;        
        }

        public bool IsCurrentBVHasOnlySummaryImages()
        {
            if (listView1 == null || listView1.Items.Count == 0) 
                return false;

            var isCurrentBVHasOnlySummaryImages = false;

            var sc = listView1.SelectedIndices;
            if (sc.Count > 0)
            {
                var index = sc[0];
                if (index >= 0 && index < listView1.Items.Count) 
                {
                    var bv = listView1.Items[index].Tag as BV;   
                    if (bv != null)                              
                        isCurrentBVHasOnlySummaryImages = bv.SaveOnlySummaryImages;
                }
            }

            return isCurrentBVHasOnlySummaryImages;
        }

        public bool IsContained(Guid bvIndex)
        {
            foreach (BV bv in m_bvCollection)
            {
                if (bv.BVIndex == bvIndex)
                    return true;
            }
            return false;
        }

        public void LoadBVList(bool displayOnlyExamineBV)
        {
            clearBVList();

            var examIndexes = new List<Guid>();
            foreach (dsBVExamination.tb_BVExaminationRow examBV in RimedDal.Instance.BVExaminationsDS.tb_BVExamination.Rows)
            {
                var bv = new BV(examBV);
                m_bvCollection.Add(bv);
                if (!examIndexes.Contains(bv.BVIndex))
                    examIndexes.Add(bv.BVIndex);
            }

            if (!displayOnlyExamineBV)
            {
                var dv = RimedDal.Instance.GetBVByStudyIndex(MainForm.Instance.CurrentStudyId);
                foreach (DataRowView row in dv)
                {
                    var bvIndex = (Guid)row["Blood_Vessel_Index"];
                    if (!examIndexes.Contains(bvIndex))
                    {
                        dsBVSetup.tb_BloodVesselSetupRow rowBV = RimedDal.Instance.GetBVProperties((Guid)row["Blood_Vessel_Index"]);
                        var bv = new BV(rowBV);
                        addBV(bv);
                    }
                }

                // the parameter of the following function was set to newGuid, so, the selected BV will be '0'.
                temporarySolutionToTheTracktorBug();
            }
			
            UpdateBVList(SelectedBV, displayOnlyExamineBV);
        }

	    public int Count
	    {
			get { return listView1.Items.Count; }
	    }

        private void temporarySolutionToTheTracktorBug()
        {
            //Assaf (31/12/12) - this function was made as a stupid, and hopefully temporary solution to the famous "Tractor" bug (No signal after renaming BV and moving from Unilateral to Bilateral or via versa)
            MainForm.Instance.PowerDown();
            MainForm.Instance.PowerUp();
            var gv = LayoutManager.Instance.SelectedGv;
			if (gv != null)
				gv.DopplerBar.DopplerVarLabelPower.Active = false;
        }

        #region Event Handlers

        private void listView1_MouseDown(object sender, MouseEventArgs e)
        {

            if (HardDriveSpaceKeeper.Instance.IsFakeDataRowReady)
                return;

            if (e.Button == MouseButtons.Left && e.Clicks == 1)
            {
                var pt = new Point(e.X, e.Y);

                for (int i = 0; i < listView1.Items.Count; i++)
                {
                    var rect = listView1.GetItemRect(i);

                    rect = new Rectangle(rect.Location, imageList1.ImageSize);

                    if (rect.Contains(pt))
                    {
                        HardDriveSpaceKeeper.Instance.ToggleSavingRowDataMode(((BV)listView1.Items[i].Tag).SubExamIndex);
                        break;
                    }
                }
            }

        }

        private void HardDriveSpaceKeeper_RowDataModeChanged(Guid subExaminationIndex, bool saveOnlySummaryImages)
        {
            foreach (ListViewItem item in listView1.Items)
            {
                var bv = item.Tag as BV;
                if (bv != null && bv.SubExamIndex == subExaminationIndex)
                {
                    item.Checked = !saveOnlySummaryImages;
                    bv.SaveOnlySummaryImages = saveOnlySummaryImages;
                    break;
                }
            }

            foreach (ListViewItem item in listView2.Items)
            {
                var bv = item.Tag as BV;
                if (bv != null && bv.SubExamIndex == subExaminationIndex)
                {
                    item.Checked = !saveOnlySummaryImages;
                    bv.SaveOnlySummaryImages = saveOnlySummaryImages;
                    break;
                }
            }
        }

		private void ensureListViewSelectedVisible(ListView lv, int offset = 0)
		{
			if (lv == null || lv.Items.Count == 0 || lv.SelectedIndices.Count == 0)
				return;

			var lst = lv.SelectedIndices;
			var ix = lst[lst.Count - 1] + offset;

			if (ix < 0)
				ix = 0;
			else if (ix >= lv.Items.Count)
				ix = lv.Items.Count - 1;

			lv.Items[ix].EnsureVisible();
		}

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            listView1.ContextMenu = listView1.SelectedIndices.Count > 0 ? contextMenu1 : null;
 
            if (!m_enabledChangeSelection)
                return;

            LayoutManager.Instance.ReplayMode = false; // changed by Alex 24/08/2014 fixed bug #496 v 2.2.0.2
            MainForm.Instance.SetHitsControls(LayoutManager.Instance.ReplayMode); // changed by Alex 24/08/2014 fixed bug #496 v 2.2.0.2
            LayoutManager.Instance.PointMode = false; // Added by Alex 30/12/2014 fixed bug #587 v 2.2.2.8

			listView1.Refresh();

            if (ListType == EListType.DUAL_LIST)
            {
                listView1_BilateralSelectedIndexChanged(sender, e);
                return;
            }

            //Solve problem of Exception where selected side is 1 when returning from Bilateral test
            GateView.SideSelected = 0;
            var sc = listView1.SelectedIndices;

            if (sc.Count == 0) 
                return;

            var index = sc[0];
            if (index < 0 || index >= listView1.Items.Count)    
                return;

            var bv = listView1.Items[index].Tag as BV;          
            if (bv == null)															   
                return;

            LoggerUserActions.SelectBloodVesel(bv.FullName);

            //RIMD-399: Parameter values on screen not change for 4 Mhz CW probe (when using Remote or not)
            double depth = 0;
            Double.TryParse(listView1.Items[sc[0]].SubItems[2].Text, out depth);
            bv.DefaultDepth = depth;

            if (bv.BVStatus == BV.EBVStatus.Examinated)
            {
                var filter = "SubExaminationIndex = '" + bv.SubExamIndex + "'";
                var rows = RimedDal.Instance.SubExaminationsDS.tb_SubExamination.Select(filter);
                if (rows.Length != 0)
                    SubExamNotes = (string)rows[0]["Notes"];
            }
            else
                SubExamNotes = string.Empty;

            var bvArr = new BV[1];
            bvArr[0] = bv;

            // added by Alex 08/04/2015 fixed bug #619 v. 2.2.2.29
            if (bvArr[0].BVStatus == BV.EBVStatus.Examinated)
            {
                // added by Alex 08/04/2015 fixed bug #619 v. 2.2.2.29
                if (DontLoadExamination)
                    bvArr[0].BVStatus = BV.EBVStatus.DontSaveBV;
            }
            else
            {
                DontLoadExamination = false;
            }

            fireBVChangedEvent(bvArr, sender);
            
            // This call is not necessary now but I didn't delete it
			if (MainForm.Instance.CurrentStudyName == Constants.StudyType.STUDY_INTRAOPERATIVE)
            {
                MainForm.ToolBar.BeginInvoke((Action)delegate()
                    {
                        MainForm.ToolBar.ComboBoxProbes.SelectedIndex = MainForm.ToolBar.ProbeLocation16Mhz;
                        MainForm.ToolBar.ComboBoxProbes.Enabled = false;
                    });
            }
            
            //Natalia: RIMD-89
            LayoutManager.Instance.CursorsMode = false;

			ensureListViewSelectedVisible(listView1, AppSettings.Active.BVListVisibleSelectedOffset);

        }

        private void listView1_BilateralSelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (!m_enabledChangeSelection)
                return;

            if (listView1.SelectedIndices.Count == 0 || listView2.SelectedIndices.Count == 0)
                return;

            if (MainForm.Instance.StudyType == GlobalTypes.EStudyType.Bilateral && listView1.SelectedIndices[0] != listView2.SelectedIndices[0])
                listView2.Items[listView1.SelectedIndices[0]].Selected = true;

            var bvArr = new BV[GlobalSettings.AProbesNum];
            for (int i = 0; i < GlobalSettings.AProbesNum; ++i)
            {
                //OFER [2013.03.27]: ListView listView = (ListView)this.ListViewArr[i];
                var listView    = DoubleLists[i];
                var sc          = listView.SelectedIndices;
                bvArr[i]        = (BV)listView.Items[sc[0]].Tag;

                LoggerUserActions.SelectBloodVesel(bvArr[i].FullName);

                //RIMD-399: Parameter values on screen not change for 4 Mhz CW probe (when using Remote or not)
                double depth = 0;
                Double.TryParse(listView.Items[sc[0]].SubItems[2].Text, out depth);
                bvArr[i].DefaultDepth = depth;

                if (bvArr[0].BVStatus == BV.EBVStatus.Examinated)
                {
                    var filter = "SubExaminationIndex = '" + bvArr[i].SubExamIndex + "'";
                    var rows = RimedDal.Instance.SubExaminationsDS.tb_SubExamination.Select(filter);
                    if (rows.Length != 0)
                        SubExamNotes = (string)rows[0]["Notes"];
                }
                else
                    SubExamNotes = string.Empty;
            }

            fireBVChangedEvent(bvArr, sender);
            //Was added by Natalia for RIMD-89
            LayoutManager.Instance.CursorsMode = false;
        }
        private void menuRename2_Click(object sender, System.EventArgs e)
        {
            var menuItem = sender as MenuItem;
            LoggerUserActions.ContextMenuClick("menuRename2", Name);
            try
            {
                var index = listView1.SelectedIndices[0];
                var bv = listView1.Items[index].Tag as BV;
 
                if (rename(bv, index, menuItem.Index))
                {
                    SummaryScreen.SaveImage2HD(true);	//Re-dump gate images to HD
                    LayoutManager.Instance.UpdateLindegaardRatio();
                }
            }
            catch (Exception ex)
            {
                LoggedDialog.Show(MainForm.Instance, MainForm.StringManager.GetString("This BV name can not be used."), exception: ex);
            }
        }

        private void menuRename_Click(object sender, System.EventArgs e)
        {
            LoggerUserActions.ContextMenuClick("menuRename", Name);
            try
            {
                if (rename())
                {
                    SummaryScreen.SaveImage2HD(true);	//Re-dump gate images to HD
                    LayoutManager.Instance.UpdateLindegaardRatio();
                }
            }
            catch (Exception ex)
            {
				LoggedDialog.Show(MainForm.Instance, MainForm.StringManager.GetString("This BV name can not be used."), exception: ex);
            }
        }

        private void menuItemReturn_Click(object sender, System.EventArgs e)
        {
            //LayoutManager.Instance.ReplayMode = false; // changed by Alex 19/08/2014 fixed bug #496 v 2.1.7.6 // changed by Alex 24/08/2014 fixed bug #496 v 2.2.0.2 (Not need see Ln 2322)
            //MainForm.Instance.SetHitsControls(LayoutManager.Instance.ReplayMode); // changed by Alex 19/08/2014 fixed bug #496 v 2.1.7.6 // changed by Alex 24/08/2014 fixed bug #496 v 2.2.0.2 (Not need Ln 2323)
            LoggerUserActions.ContextMenuClick("menuItemReturn", Name);
            if (MainForm.Instance.WorkingMode == GlobalTypes.EWorkingMode.LoadMode)
            {
				LoggedDialog.Show(MainForm.Instance, MainForm.StringManager.GetString("Cannot perform new test for loaded examination!"));
                return;
            }

            // this will rename the BV in the bv list and in all the gates that refers to that BV.
            var sc = listView1.SelectedIndices;

            if (sc.Count != 0)
            {
                int index = sc[0];

                var bv = (BV)listView1.Items[index].Tag;

                if (bv.BVStatus != BV.EBVStatus.Examinated)
                    return;

                var position = index;

                duplicateBV();

                if ((index + 1) < listView1.Items.Count)
                    ++position;

                if (ListType == EListType.DUAL_LIST)
                {
                    listView2.Items[position - 1].ForeColor = EXAMINED_BV_COLOR;
                    listView2.Items[position].Selected = true;
                    listView2.Items[position].Focused = true;
                }

                listView1.Items[position - 1].ForeColor = EXAMINED_BV_COLOR;
                listView1.Items[position].Selected = true;
                listView1.Items[position].Focused = true;
				
				foreach (var gateView in LayoutManager.Instance.GateViews)
				{
					ProbesCollection.Instance.AddGvToProbe(gateView.Probe, gateView.GateSide == GlobalTypes.EGateSide.Right ? 1 : 0, gateView);		
				}
            }
        }

        private void menuItemReplayBV_Click(object sender, System.EventArgs e)
        {
            LoggerUserActions.MouseClick("menuItemReplayBV", Name);
            
			// this will rename the BV in the bv list and in all the gates that refers to that BV.
            var sc = listView1.SelectedIndices;

            if (sc.Count != 0 && m_bvCollection[sc[0]].BVStatus == BV.EBVStatus.Examinated)
            {
                if (LayoutManager.Instance.ReplayMode)
                {
                    // move to regular mode [unReplay].
                    menuItemReplayBV.Checked = false;
                    UpdateBVList(Guid.Empty, false);
                }
                else
                {
                    // move to Replay mode.
                    menuItemReplayBV.Checked = true;
                    UpdateBVList(Guid.Empty, ExaminationWasSaved);	
                }
            }
        }

        private void contextMenu1_Popup(object sender, System.EventArgs e)
        {

            // enable/disable the right click items menu.
			menuItemReturn.Visible		= MainForm.Instance.WorkingMode != GlobalTypes.EWorkingMode.LoadMode;
            menuItemReplayBV.Enabled = isExamineBVExist();
            menuItemRename.Enabled = false;
            // add rename BV names options
            if (!(AppSettings.Active.IsDspOffMode && !AppSettings.Active.IsReviewStation))
            {
                if (listView1 == null || listView1.Items.Count == 0)
                    return;
                if (listView1.SelectedIndices.Count == 0 || listView1.SelectedIndices[0] < 0)
                {
                    return;
                }
                var bv = listView1.Items[listView1.SelectedIndices[0]].Tag as BV;
                if (bv == null || bv.BVStatus != BV.EBVStatus.Examinated)
                    return;
                PrepareBVRenameLists(bv, m_BVRenameNames, m_BVRenameIndexes);
                this.menuItemRename.MenuItems.Clear();
                for(int i=0; i<m_BVRenameNames.Count; i++)
                {
                    MenuItem nameMenuItem = new MenuItem();
                    nameMenuItem.Text = m_BVRenameNames[i];
                    nameMenuItem.Click += menuRename2_Click;
                    this.menuItemRename.MenuItems.Add(nameMenuItem);
                
                }
                menuItemRename.Enabled = true;
            }
        }

        private void LayoutManager_LoadExamEvent(Guid examIndex)
        {
            SelectedBV = m_bvCollection.Count > 0 ? m_bvCollection[0].BVExaminationIndex : Guid.Empty;		
            loadExamination(examIndex, Guid.Empty, false, true);											

            if (LayoutManager.SystemLayoutMode != GlobalTypes.ESystemLayoutMode.MonitoringLoad)				
                LayoutManager.Instance.Trends.Clear();		

            if (LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.Monitoring || LayoutManager.SystemLayoutMode == GlobalTypes.ESystemLayoutMode.MonitoringLoad)
            {
				LayoutManager.Instance.OnNewTrend(false);  

	            //Assaf (18/11/07) - Loading the trends pics into the trends objects zzzz
	            foreach (TrendChart trend in LayoutManager.Instance.Trends)
	            {
                    var fileName = Path.Combine(Constants.TRENDS_PATH, examIndex + "_" + trend.Id + ".bmp");
	                if (File.Exists(fileName + ".tmp"))
	                {
	                    Files.Delete(fileName);
	                    Files.Move(fileName + ".tmp", fileName);
	                }

	                trend.Graph.SetReplayFileName(fileName);
                    //Changed by Alex bug #605 v.2.2.3.20 20/01/2016 
                    if (!trend.Graph.IsSupported && LayoutManager.SystemLayoutMode != GlobalTypes.ESystemLayoutMode.MonitoringLoad) //TODO: Alex bug #605
						LoggedDialog.Show(MainForm.StringManager.GetString("This is an old monitoring exam. Trends would not be supported.")); //Fix bug: message box hidden behind modal window
	            }
				
	            if (MainForm.Instance.CurrentStudyName.StartsWith("Evoked Flow"))
	            {
	                var lastStopStimulationEvent = EventListViewCtrl.Instance.LastStopStimulationEvent;
	                if (lastStopStimulationEvent != null)
	                {
	                    //NULL check was added for empty event list (RIMD-243)
	                    if (LayoutManager.Instance.Trends.Count > 0)
	                        LayoutManager.Instance.Trends[0].EvokedFlowPeak = lastStopStimulationEvent.FlowChangePrimary;
	                    if (LayoutManager.Instance.Trends.Count > 1)
	                        LayoutManager.Instance.Trends[1].EvokedFlowPeak = lastStopStimulationEvent.FlowChangeSecondary;
                    }
                }
            }
        }

        private void LayoutManager_LoadBVExamEvent(Guid examIndex, Guid bvIndex)
        {
            SelectedBV = bvIndex;
            loadExamination(examIndex, Guid.Empty, false, false);
        }

        private void LayoutManager_LoadGateExamEvent(Guid examIndex, GateIndexTagData gitd)
        {
            SelectedBV = gitd.BVExamIndex;								
            loadExamination(gitd.ExamIndex, Guid.Empty, false, false);	

            if (MainForm.Instance.IsUnilateralStudy)
                LayoutManager.Instance.ReplayGate(gitd.GateIndex);
            else
                LayoutManager.Instance.BilateralReplayGate(gitd.GateIndex);
        }

        private void listView2_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (MainForm.Instance.StudyType != GlobalTypes.EStudyType.Multifrequency)
                return;

            listView1_BilateralSelectedIndexChanged(sender, e);
        }

        private void BVListViewCtrl_SizeChanged(object sender, System.EventArgs e)
        {
            if (Width < 50)
                Width = 50;

            if (MainForm.Instance != null)
                MainForm.Instance.ResizePanelsSize();

            var hight = listView1.Parent.Height;
            if (ListType == EListType.DUAL_LIST)
            {
                listView1.Height = hight / 2;
                listView2.Height = hight / 2;
            }
            else
            {
                listView1.Height = hight;
            }

            var widthCtrl = listView1.Width;
            listView1.Columns[0].Width = widthCtrl / 3 * 2;
            listView1.Columns[1].Width = widthCtrl - listView1.Columns[0].Width - 5;
            listView2.Columns[0].Width = widthCtrl / 3 * 2;
            listView2.Columns[1].Width = widthCtrl - listView2.Columns[0].Width - 5;
        }
        #endregion Event Handlers

        private Guid SelectedBV { get; set; }



    }

    public delegate void BVChangedDelegate(BV[] bvArr, object sender);
}
