using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Rimed.Framework.Common;
using Rimed.TCD.DAL;
using Rimed.TCD.Utils;

namespace Rimed.TCD.GUI
{
    /// <summary>
    /// Summary description for HardDriveSpaceKeeper.
    /// This class was wrote by Dima from MWDN and holds the logics for the option to 
    /// consume less HD space, by saving just partial data of the examination (Snapshots for the report and the spectum outlook
    /// and not the replay/rawdata).
    /// the designations are taking according to the setup and the user choice on the fly.
    /// </summary>
    public class HardDriveSpaceKeeper
    {
        public event RowDataModeChangedHandler RowDataModeChanged;

        private bool m_fakeDataRowReady = true;
        public static readonly HardDriveSpaceKeeper Instance = new HardDriveSpaceKeeper();

        public bool IsFakeDataRowReady
        {
            get
            {
                return m_fakeDataRowReady;
            }
        }

        /// <summary>Keeper should subscribe by LayoutManager events the fist then all</summary>
        public void PrepareForWork()
        {
            LayoutManager.LoadBVExamEvent += new LoadBVExamDelegate(LayoutManager_LoadBVExamEvent);
            LayoutManager.LoadExamEvent += new LoadExamDelegate(LayoutManager_LoadExamEvent);
            LayoutManager.LoadGateExamEvent += new LoadGateExamDelegate(LayoutManager_LoadGateExamEvent);
            LayoutManager.Instance.SystemModeChangedEvent += new Action(theLayoutManager_SystemModeChangedEvent);
        }

        private Image GetChartImage(string path)
        {
            if (!IsFakeDataRowReady || !File.Exists(path))
                return null;

            try
            {
                return Image.FromFile(path);
            }
            catch (Exception ex)
            {
				LoggedDialog.ShowError(MainForm.Instance, MainForm.StringManager.GetString("Failed to load the chart from an image file.") + "\n\n" + ex.Message, MainForm.StringManager.GetString("Error"));
                return null;
            }
        }

        public Image GetAutoscanImage(Guid subExaminationIndex, int index)
        {
            var path = GetAutoscanSnapshotPath(subExaminationIndex, index);
            return GetChartImage(path);
        }

        public Image GetSpectrumImage(GateView gate)
        {
            string path;

            if (LayoutManager.Instance.CurrentGateView != null && LayoutManager.Instance.CurrentGateView.GateIndex == gate.GateIndex)
                path = GetGateSnapshotPath(gate.GateIndex, true);
            else
                path = GetGateSnapshotPath(gate.GateIndex, false);

            return GetChartImage(path);
        }

        public bool IsSaveOnlySummaryImages(Guid subExamIndex)
        {
            foreach (dsBVExamination.tb_BVExaminationRow bvRow in RimedDal.Instance.BVExaminationsDS.tb_BVExamination.Rows)
            {
                if (bvRow == null || bvRow.RowState == DataRowState.Deleted)    //Ofer, v2.0.5.0: BugFix - Deleted row information cannot be accessed through the row
                    continue;

                if (bvRow.SubExamIndex == subExamIndex && bvRow.SaveOnlySummaryImages)
                    return true;
            }

            return false;
        }

        public bool IsReplayRowDataFake(Guid subExamIndex)
        {
            if (!IsFakeDataRowReady)
                return false;

            foreach (dsBVExamination.tb_BVExaminationRow bvRow in RimedDal.Instance.BVExaminationsDS.tb_BVExamination.Rows)
            {
                if (bvRow == null || bvRow.RowState == DataRowState.Deleted)    //Ofer, v2.0.5.0: BugFix - Deleted row information cannot be accessed through the row
                    continue;

                if (bvRow.SubExamIndex == subExamIndex && bvRow.SaveOnlySummaryImages)
                    return true;
            }

            return false;
        }
        
        /// <summary>Save the  snapshots of the rows. A snapshot will use in the replay mode if a row data will be removed</summary>
        public bool SaveReplayShaphots()
        {
            m_fakeDataRowReady = false;

			var examRow = RimedDal.Instance.LoadedExamination();
			if (examRow == null)
				return false;

	        var examIndex = examRow.Examination_Index;
			//if (RimedDal.Instance.ExaminationsDS.tb_Examination.Rows.Count == 0)
			//{
			//    Logger.LogError("SaveReplayShaphots(): Examination table is empty.");
			//    return false;
			//}

			//var examinationRow = RimedDal.Instance.ExaminationsDS.tb_Examination.Rows[0] as dsExamination.tb_ExaminationRow;
			//if (examRow.RowState == DataRowState.Deleted)  //Ofer, v2.0.5.0: BugFix - Deleted row information cannot be accessed through the row
			//{
			//    Logger.LogError("SaveReplayShaphots(): Examination[0] deleted or null.");
			//    return false;
			//}

            if (!Directory.Exists(Constants.REPLAY_IMG_PATH))
                Directory.CreateDirectory(Constants.REPLAY_IMG_PATH);

            var gateIndex = Guid.Empty;
            foreach (var view in LayoutManager.Instance.GateViews)
            {
                var path = GetGateSnapshotPath(view.GateIndex, false);
                view.Spectrum.Graph.SaveSnapshot(path);

                // for gate center view mode
                if (LayoutManager.Instance.CurrentGateView != null)
                {
                    path = GetGateSnapshotPath(view.GateIndex, true);
                    var size = LayoutManager.Instance.CurrentGateView.Spectrum.Graph.Size;
                    view.Spectrum.Graph.SaveSnapshot(path, size);
                }

                if (gateIndex == Guid.Empty)
                    gateIndex = view.GateIndex;
            }

			var gateRow = RimedDal.Instance.GateExaminationsDS.tb_GateExamination.FindByGate_IndexExaminationIndex(gateIndex, examIndex);
            if (gateRow == null || gateRow.RowState == DataRowState.Deleted)    //Ofer, v2.0.5.0: BugFix - Deleted row information cannot be accessed through the row
            {
				Logger.LogError(string.Format("SaveReplayShaphots(): GateExamination for Examination index '{0}' deleted or was not found.", examIndex));
                return false;
            }

            for (int i = 0; i < LayoutManager.Instance.Autoscans.Length; i++)
            {
                if (LayoutManager.Instance.Autoscans[i].Visible)
                {
                    var path = GetAutoscanSnapshotPath(gateRow.SubExaminationIndex, i);
                    LayoutManager.Instance.Autoscans[i].Graph.SaveSnapshot(path);
                }
            }

            return true;
        }

        public static void DeleteUnusedExamData()
        {
            try
            {
                deleteUnusedExamData();
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
            //  //TODO Combine path for Diagnostic
            //foreach (dsBVExamination.tb_BVExaminationRow bvRow in RimedDal.Instance.BVExaminationsDS.tb_BVExamination.Rows)
            //{
            //    if (bvRow == null || bvRow.RowState == DataRowState.Deleted)                //Ofer, v2.0.5.0: BugFix - Deleted row information cannot be accessed through the row   
            //        continue;

            //    string path;

            //    if (bvRow.SaveOnlySummaryImages) // remove row data fiels
            //    {
            //        path = Path.Combine(Constants.RAW_DATA_PATH, bvRow.SubExamIndex.ToString());
            //        if (Directory.Exists(path)) 
            //            Directory.Delete(path, true);
            //    }
            //    else //remove replay images
            //    {
            //        foreach (dsGateExamination.tb_GateExaminationRow gateRow in RimedDal.Instance.GateExaminationsDS.tb_GateExamination.Rows)
            //        {
            //            if (gateRow == null || gateRow.RowState == DataRowState.Deleted)    //Ofer, v2.0.5.0: BugFix - Deleted row information cannot be accessed through the row
            //                continue;

            //            if (gateRow.SubExaminationIndex == bvRow.SubExamIndex)
            //            {
            //                path = GetGateSnapshotPath(gateRow.Gate_Index, true);
            //                DeleteFile(path);

            //                path = GetGateSnapshotPath(gateRow.Gate_Index, false);
            //                DeleteFile(path);
            //            }
            //        }

            //        for (int i = 0; i < LayoutManager.Instance.Autoscans.Length; i++)
            //        {
            //            path = GetAutoscanSnapshotPath(bvRow.SubExamIndex, i);
            //            DeleteFile(path);
            //        }
            //    }
				
            //    //Merge, Nadiia
            //    foreach (dsGateExamination.tb_GateExaminationRow gateRow in RimedDal.Instance.GateExaminationsDS.tb_GateExamination.Rows)
            //    {
            //        if (gateRow == null || gateRow.RowState == DataRowState.Deleted)    //Ofer, v2.0.5.0: BugFix - Deleted row information cannot be accessed through the row
            //            continue;

            //        string fileName = gateRow.Gate_Index.ToString();
            //        if (gateRow.Name.EndsWith("-L"))
            //            fileName += "_L.jpg";
            //        else if (gateRow.Name.EndsWith("-R"))
            //            fileName += "_R.jpg";
            //        else
            //            fileName += "_M.jpg";

            //        string filePath = Path.Combine(Constants.TEMP_IMG_PATH, fileName);
            //        DeleteFile(filePath);
            //        DeleteFile(filePath.Insert(filePath.IndexOf('.'), "_MC"));
            //    }
            //}
        }
        private static void deleteUnusedExamData()
        {
            //TODO Combine path for Diagnostic
            foreach (dsBVExamination.tb_BVExaminationRow bvRow in RimedDal.Instance.BVExaminationsDS.tb_BVExamination.Rows)
            {
                if (bvRow == null || bvRow.RowState == DataRowState.Deleted)                //Ofer, v2.0.5.0: BugFix - Deleted row information cannot be accessed through the row   
                    continue;

                if (bvRow.SaveOnlySummaryImages) // remove row data fiels
                {
                    var path = Path.Combine(Constants.RAW_DATA_PATH, bvRow.SubExamIndex.ToString());
                    if (Directory.Exists(path))
                        Directory.Delete(path, true);
                }
                else //remove replay images
                {
                    foreach (dsGateExamination.tb_GateExaminationRow gateRow in RimedDal.Instance.GateExaminationsDS.tb_GateExamination.Rows)
                    {
                        if (gateRow == null || gateRow.RowState == DataRowState.Deleted)    //Ofer, v2.0.5.0: BugFix - Deleted row information cannot be accessed through the row
                            continue;

                        if (gateRow.SubExaminationIndex != bvRow.SubExamIndex) 
                            continue;
                        
                        var path = GetGateSnapshotPath(gateRow.Gate_Index, true);
                        DeleteFile(path);

                        path = GetGateSnapshotPath(gateRow.Gate_Index, false);
                        DeleteFile(path);
                    }

                    for (var i = 0; i < LayoutManager.Instance.Autoscans.Length; i++)
                    {
                        var path = GetAutoscanSnapshotPath(bvRow.SubExamIndex, i);
                        DeleteFile(path);
                    }
                }

                //Merge, Nadiia
                foreach (dsGateExamination.tb_GateExaminationRow gateRow in RimedDal.Instance.GateExaminationsDS.tb_GateExamination.Rows)
                {
                    if (gateRow == null || gateRow.RowState == DataRowState.Deleted)    //Ofer, v2.0.5.0: BugFix - Deleted row information cannot be accessed through the row
                        continue;

                    var fileName = gateRow.Gate_Index.ToString();
                    if (gateRow.Name.EndsWith("-L"))
                        fileName += "_L.jpg";
                    else if (gateRow.Name.EndsWith("-R"))
                        fileName += "_R.jpg";
                    else
                        fileName += "_M.jpg";

                    var filePath = Path.Combine(Constants.TEMP_IMG_PATH, fileName);
                    DeleteFile(filePath);
                    DeleteFile(filePath.Insert(filePath.IndexOf('.'), "_MC"));
                }
            }
        }

        private static string GetGateSnapshotPath(Guid gateIndex, bool inCenterView)
        {
            if (inCenterView)
                return Path.Combine(Constants.REPLAY_IMG_PATH, gateIndex + "_CenterView" + ".jpg");

            return Path.Combine(Constants.REPLAY_IMG_PATH, gateIndex + ".jpg");
        }

        private static string GetAutoscanSnapshotPath(Guid subExaminationIndex, int index)
        {
            return Path.Combine(Constants.REPLAY_IMG_PATH, subExaminationIndex + "_" + index + ".jpg");
        }

        private static void DeleteFile(string path)
        {
            try
            {
                if (File.Exists(path))
                    Files.Delete(path);
            }
            catch (Exception ex)
            {
                Logger.LogError(ex);
            }
        }

        public void ToggleSavingRowDataMode(Guid subExaminationIndex)
        {
            if (IsFakeDataRowReady)
                return;

            bool changed = false;
            bool saveOnlySummaryImages = false;

            foreach (dsBVExamination.tb_BVExaminationRow row in RimedDal.Instance.BVExaminationsDS.tb_BVExamination.Rows)
            {
                if (row == null || row.RowState == DataRowState.Deleted)    //Ofer, v2.0.5.0: BugFix - Deleted row information cannot be accessed through the row
                    continue;

                if (row.SubExamIndex == subExaminationIndex)
                {
                    row.SaveOnlySummaryImages = !row.SaveOnlySummaryImages;
                    saveOnlySummaryImages = row.SaveOnlySummaryImages;
                    changed = true;
                }
            }

            if (RowDataModeChanged != null && changed)
                RowDataModeChanged(subExaminationIndex, saveOnlySummaryImages);
        }

		//TODO, Ofer: merge LayoutManager_LoadBVExamEvent overload methods
        private void LayoutManager_LoadBVExamEvent(Guid examIndex, Guid bvExamIndex)
        {
            m_fakeDataRowReady = !RimedDal.Instance.IsTemporaryExamination(examIndex);	
        }

        private void LayoutManager_LoadExamEvent(Guid examIndex)
        {
            m_fakeDataRowReady = !RimedDal.Instance.IsTemporaryExamination(examIndex);	
        }

        private void LayoutManager_LoadGateExamEvent(Guid examIndex, GateIndexTagData gitd)
        {
            m_fakeDataRowReady = !RimedDal.Instance.IsTemporaryExamination(examIndex);	
        }
		 
        private void theLayoutManager_SystemModeChangedEvent()
        {
            if (LayoutManager.SystemLayoutMode != GlobalTypes.ESystemLayoutMode.DiagnosticLoad)
                m_fakeDataRowReady = false;
        } 
    }

    public delegate void RowDataModeChangedHandler(Guid subExaminationIndex, bool saveOnlySummaryImages);
}
