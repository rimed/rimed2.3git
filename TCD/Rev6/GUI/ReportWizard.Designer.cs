namespace Rimed.TCD.GUI
{
    partial class ReportWizard
    {
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReportWizard));
            this.wizardControl1 = new Syncfusion.Windows.Forms.Tools.WizardControl();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.gradientPanel1 = new Syncfusion.Windows.Forms.Tools.GradientPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.wizardContainer1 = new Syncfusion.Windows.Forms.Tools.WizardContainer();
            this.wizardControlPage1 = new Syncfusion.Windows.Forms.Tools.WizardControlPage(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.wizardControlPage2 = new Syncfusion.Windows.Forms.Tools.WizardControlPage(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButtonFullPatientDetails = new System.Windows.Forms.RadioButton();
            this.radioButtonMinimalPatientDetails = new System.Windows.Forms.RadioButton();
            this.gradientPanelPreview = new Syncfusion.Windows.Forms.Tools.GradientPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.labelFullPatientDeails = new System.Windows.Forms.Label();
            this.labelHospitalDetails = new System.Windows.Forms.Label();
            this.pictureBoxLogo = new System.Windows.Forms.PictureBox();
            this.labelDate = new System.Windows.Forms.Label();
            this.labelTitle = new System.Windows.Forms.Label();
            this.labelMinimalPatientDetails = new System.Windows.Forms.Label();
            this.labelPatientHistory = new System.Windows.Forms.Label();
            this.labelExaminationHistory = new System.Windows.Forms.Label();
            this.checkBoxHospitalLogo = new System.Windows.Forms.CheckBox();
            this.dsPatientRepLayout1 = new Rimed.TCD.DAL.dsPatientRepLayout();
            this.checkBoxHospitalDetails = new System.Windows.Forms.CheckBox();
            this.checkBoxDate = new System.Windows.Forms.CheckBox();
            this.checkBoxTitle = new System.Windows.Forms.CheckBox();
            this.checkBoxHistory = new System.Windows.Forms.CheckBox();
            this.checkBoxExaminationHistory = new System.Windows.Forms.CheckBox();
            this.checkBoxOnePageReport = new System.Windows.Forms.CheckBox();
            this.wizardControlPage3 = new Syncfusion.Windows.Forms.Tools.WizardControlPage(this.components);
            this.panelIpImages = new System.Windows.Forms.Panel();
            this.lIpImages = new System.Windows.Forms.Label();
            this.rBtn4 = new System.Windows.Forms.RadioButton();
            this.rBtn2 = new System.Windows.Forms.RadioButton();
            this.rBtn1 = new System.Windows.Forms.RadioButton();
            this.rBtnNo = new System.Windows.Forms.RadioButton();
            this.checkBoxSummaryScreen = new System.Windows.Forms.CheckBox();
            this.checkBoxIncludsNormalValues = new System.Windows.Forms.CheckBox();
            this.gradientPanel2 = new Syncfusion.Windows.Forms.Tools.GradientPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelExaminationDisplay = new System.Windows.Forms.Label();
            this.checkBoxTable = new System.Windows.Forms.CheckBox();
            this.wizardControlPage4 = new Syncfusion.Windows.Forms.Tools.WizardControlPage(this.components);
            this.checkBoxIndication = new System.Windows.Forms.CheckBox();
            this.gradientPanel3 = new Syncfusion.Windows.Forms.Tools.GradientPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.labelIndication = new System.Windows.Forms.Label();
            this.labelInterpretation = new System.Windows.Forms.Label();
            this.labelSignature = new System.Windows.Forms.Label();
            this.checkBoxInterpretation = new System.Windows.Forms.CheckBox();
            this.checkBoxSignature = new System.Windows.Forms.CheckBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.cachedcrTableRep1 = new Rimed.TCD.ReportMgr.CachedcrTableRep();
            ((System.ComponentModel.ISupportInitialize)(this.wizardControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wizardControl1.GridBagLayout)).BeginInit();
            this.wizardControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.gradientPanel1.SuspendLayout();
            this.wizardContainer1.SuspendLayout();
            this.wizardControlPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.wizardControlPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gradientPanelPreview.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsPatientRepLayout1)).BeginInit();
            this.wizardControlPage3.SuspendLayout();
            this.panelIpImages.SuspendLayout();
            this.gradientPanel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.wizardControlPage4.SuspendLayout();
            this.gradientPanel3.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // wizardControl1
            // 
            // 
            // 
            // 
            this.wizardControl1.BackButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.wizardControl1.BackButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.wizardControl1.BackButton.Location = new System.Drawing.Point(282, 402);
            this.wizardControl1.BackButton.Name = "backButton";
            this.wizardControl1.BackButton.Text = "<< Back";
            this.wizardControl1.BackButton.Visible = false;
            this.wizardControl1.BackButton.Click += new System.EventHandler(this.wizardControl1_BackButton_Click);
            this.wizardControl1.Banner = this.pictureBox1;
            this.wizardControl1.BannerPanel = this.gradientPanel1;
            // 
            // 
            // 
            this.wizardControl1.CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.wizardControl1.CancelButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.wizardControl1.CancelButton.Location = new System.Drawing.Point(0, 402);
            this.wizardControl1.CancelButton.Name = "cancelButton";
            this.wizardControl1.CancelButton.Text = "Cancel";
            this.wizardControl1.CancelButton.Click += new System.EventHandler(this.wizardControl1_CancelButton_Click);
            this.wizardControl1.Controls.Add(this.wizardContainer1);
            this.wizardControl1.Controls.Add(this.gradientPanel1);
            this.wizardControl1.Description = this.label2;
            this.wizardControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            // 
            // 
            // 
            this.wizardControl1.FinishButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.wizardControl1.FinishButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.wizardControl1.FinishButton.Location = new System.Drawing.Point(432, 402);
            this.wizardControl1.FinishButton.Name = "finishButton";
            this.wizardControl1.FinishButton.Text = "Finish";
            this.wizardControl1.FinishButton.Visible = false;
            this.wizardControl1.FinishButton.Click += new System.EventHandler(this.wizardControl1_FinishButton_Click);
            // 
            // 
            // 
            this.wizardControl1.GridBagLayout.ContainerControl = this.wizardControl1;
            // 
            // 
            // 
            this.wizardControl1.HelpButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.wizardControl1.HelpButton.Location = new System.Drawing.Point(512, 402);
            this.wizardControl1.HelpButton.Name = "helpButton";
            this.wizardControl1.HelpButton.Text = "Help";
            this.wizardControl1.HelpButton.Click += new System.EventHandler(this.wizardControl1_HelpButton_Click);
            this.wizardControl1.Location = new System.Drawing.Point(0, 0);
            this.wizardControl1.Name = "wizardControl1";
            // 
            // 
            // 
            this.wizardControl1.NextButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.wizardControl1.NextButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.wizardControl1.NextButton.Location = new System.Drawing.Point(357, 402);
            this.wizardControl1.NextButton.Name = "nextButton";
            this.wizardControl1.NextButton.Text = "Next >>";
            this.wizardControl1.NextButton.Click += new System.EventHandler(this.wizardControl1_NextButton_Click);
            this.wizardControl1.SelectedWizardPage = this.wizardControlPage1;
            this.wizardControl1.Size = new System.Drawing.Size(592, 430);
            this.wizardControl1.TabIndex = 0;
            this.wizardControl1.Title = this.label1;
            this.wizardControl1.WizardPageContainer = this.wizardContainer1;
            this.wizardControl1.WizardPages = new Syncfusion.Windows.Forms.Tools.WizardControlPage[] {
        this.wizardControlPage1,
        this.wizardControlPage2,
        this.wizardControlPage3,
        this.wizardControlPage4};
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(521, -202);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(61, 82);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // gradientPanel1
            // 
            this.gradientPanel1.BackgroundColor = new Syncfusion.Drawing.BrushInfo(Syncfusion.Drawing.GradientStyle.Vertical, new Syncfusion.Drawing.BrushInfoColorArrayList(new System.Drawing.Color[] {
                System.Drawing.Color.White,
                System.Drawing.SystemColors.Control}));
            this.gradientPanel1.BorderColor = System.Drawing.Color.Black;
            this.gradientPanel1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gradientPanel1.Controls.Add(this.pictureBox1);
            this.gradientPanel1.Controls.Add(this.label1);
            this.gradientPanel1.Controls.Add(this.label2);
            this.gradientPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.gradientPanel1.Location = new System.Drawing.Point(0, 0);
            this.gradientPanel1.Name = "gradientPanel1";
            this.gradientPanel1.Size = new System.Drawing.Size(592, 70);
            this.gradientPanel1.TabIndex = 11;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(20, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Page Title";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(203, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "This is the description of the Wizard Page";
            // 
            // wizardContainer1
            // 
            this.wizardContainer1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.wizardContainer1.Controls.Add(this.wizardControlPage1);
            this.wizardContainer1.Controls.Add(this.wizardControlPage2);
            this.wizardContainer1.Controls.Add(this.wizardControlPage3);
            this.wizardContainer1.Controls.Add(this.wizardControlPage4);
            this.wizardContainer1.Location = new System.Drawing.Point(0, 0);
            this.wizardContainer1.Name = "wizardContainer1";
            this.wizardContainer1.Size = new System.Drawing.Size(592, 392);
            this.wizardContainer1.TabIndex = 1;
            // 
            // wizardControlPage1
            // 
            this.wizardControlPage1.BackgroundColor = new Syncfusion.Drawing.BrushInfo(Syncfusion.Drawing.GradientStyle.Vertical, new Syncfusion.Drawing.BrushInfoColorArrayList(new System.Drawing.Color[] {
                System.Drawing.Color.White,
                System.Drawing.SystemColors.Control}));
            this.wizardControlPage1.BackVisible = false;
            this.wizardControlPage1.BorderColor = System.Drawing.Color.Black;
            this.wizardControlPage1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wizardControlPage1.Controls.Add(this.label7);
            this.wizardControlPage1.Controls.Add(this.linkLabel1);
            this.wizardControlPage1.Controls.Add(this.label5);
            this.wizardControlPage1.Controls.Add(this.pictureBox5);
            this.wizardControlPage1.Controls.Add(this.pictureBox2);
            this.wizardControlPage1.Description = "This is the description of the Wizard Page";
            this.wizardControlPage1.FullPage = true;
            this.wizardControlPage1.LayoutName = "Card1";
            this.wizardControlPage1.Location = new System.Drawing.Point(0, 0);
            this.wizardControlPage1.Name = "wizardControlPage1";
            this.wizardControlPage1.NextPage = null;
            this.wizardControlPage1.PreviousPage = null;
            this.wizardControlPage1.Size = new System.Drawing.Size(592, 392);
            this.wizardControlPage1.TabIndex = 0;
            this.wizardControlPage1.Title = "Page Title";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(256, 256);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(117, 13);
            this.label7.TabIndex = 10;
            this.label7.Text = "To continue, click Next";
            // 
            // linkLabel1
            // 
            this.linkLabel1.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel1.Location = new System.Drawing.Point(360, 160);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(120, 23);
            this.linkLabel1.TabIndex = 9;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "http://www.rimed.com";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(284, 35);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(272, 89);
            this.label5.TabIndex = 4;
            this.label5.Text = "Welcome to the Report Generator Wizard.";
            // 
            // pictureBox5
            // 
            this.pictureBox5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(88, 16);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(56, 48);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 3;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(224, 392);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // wizardControlPage2
            // 
            this.wizardControlPage2.BackgroundColor = new Syncfusion.Drawing.BrushInfo(Syncfusion.Drawing.GradientStyle.Vertical, new Syncfusion.Drawing.BrushInfoColorArrayList(new System.Drawing.Color[] {
                System.Drawing.Color.White,
                System.Drawing.SystemColors.Control}));
            this.wizardControlPage2.BorderColor = System.Drawing.Color.Black;
            this.wizardControlPage2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wizardControlPage2.Controls.Add(this.groupBox1);
            this.wizardControlPage2.Controls.Add(this.gradientPanelPreview);
            this.wizardControlPage2.Controls.Add(this.checkBoxHospitalLogo);
            this.wizardControlPage2.Controls.Add(this.checkBoxHospitalDetails);
            this.wizardControlPage2.Controls.Add(this.checkBoxDate);
            this.wizardControlPage2.Controls.Add(this.checkBoxTitle);
            this.wizardControlPage2.Controls.Add(this.checkBoxHistory);
            this.wizardControlPage2.Controls.Add(this.checkBoxExaminationHistory);
            this.wizardControlPage2.Controls.Add(this.checkBoxOnePageReport);
            this.wizardControlPage2.Description = "Here you can determine which details will be display in the report.";
            this.wizardControlPage2.FullPage = false;
            this.wizardControlPage2.LayoutName = "Card2";
            this.wizardControlPage2.Location = new System.Drawing.Point(0, 0);
            this.wizardControlPage2.Name = "wizardControlPage2";
            this.wizardControlPage2.NextPage = null;
            this.wizardControlPage2.PreviousPage = null;
            this.wizardControlPage2.Size = new System.Drawing.Size(592, 392);
            this.wizardControlPage2.TabIndex = 1;
            this.wizardControlPage2.Title = "Page Title";
            this.wizardControlPage2.VisibleChanged += new System.EventHandler(this.wizardControlPage2_VisibleChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.radioButtonFullPatientDetails);
            this.groupBox1.Controls.Add(this.radioButtonMinimalPatientDetails);
            this.groupBox1.Location = new System.Drawing.Point(240, 136);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(209, 56);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Patient Details";
            // 
            // radioButtonFullPatientDetails
            // 
            this.radioButtonFullPatientDetails.AutoSize = true;
            this.radioButtonFullPatientDetails.Checked = true;
            this.radioButtonFullPatientDetails.Location = new System.Drawing.Point(16, 24);
            this.radioButtonFullPatientDetails.Name = "radioButtonFullPatientDetails";
            this.radioButtonFullPatientDetails.Size = new System.Drawing.Size(41, 17);
            this.radioButtonFullPatientDetails.TabIndex = 0;
            this.radioButtonFullPatientDetails.TabStop = true;
            this.radioButtonFullPatientDetails.Text = "Full";
            this.radioButtonFullPatientDetails.CheckedChanged += new System.EventHandler(this.radioButtonFullPatientDetails_CheckedChanged);
            // 
            // radioButtonMinimalPatientDetails
            // 
            this.radioButtonMinimalPatientDetails.AutoSize = true;
            this.radioButtonMinimalPatientDetails.Location = new System.Drawing.Point(96, 24);
            this.radioButtonMinimalPatientDetails.Name = "radioButtonMinimalPatientDetails";
            this.radioButtonMinimalPatientDetails.Size = new System.Drawing.Size(60, 17);
            this.radioButtonMinimalPatientDetails.TabIndex = 0;
            this.radioButtonMinimalPatientDetails.Text = "Minimal";
            this.radioButtonMinimalPatientDetails.CheckedChanged += new System.EventHandler(this.radioButtonMinimalPatientDetails_CheckedChanged);
            // 
            // gradientPanelPreview
            // 
            this.gradientPanelPreview.BackgroundColor = new Syncfusion.Drawing.BrushInfo(Syncfusion.Drawing.GradientStyle.Vertical, new Syncfusion.Drawing.BrushInfoColorArrayList(new System.Drawing.Color[] {
                System.Drawing.Color.White,
                System.Drawing.SystemColors.Control}));
            this.gradientPanelPreview.Border3DStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.gradientPanelPreview.BorderColor = System.Drawing.Color.Black;
            this.gradientPanelPreview.Controls.Add(this.panel2);
            this.gradientPanelPreview.Dock = System.Windows.Forms.DockStyle.Left;
            this.gradientPanelPreview.Location = new System.Drawing.Point(0, 0);
            this.gradientPanelPreview.Name = "gradientPanelPreview";
            this.gradientPanelPreview.Size = new System.Drawing.Size(224, 392);
            this.gradientPanelPreview.TabIndex = 4;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.labelFullPatientDeails);
            this.panel2.Controls.Add(this.labelHospitalDetails);
            this.panel2.Controls.Add(this.pictureBoxLogo);
            this.panel2.Controls.Add(this.labelDate);
            this.panel2.Controls.Add(this.labelTitle);
            this.panel2.Controls.Add(this.labelMinimalPatientDetails);
            this.panel2.Controls.Add(this.labelPatientHistory);
            this.panel2.Controls.Add(this.labelExaminationHistory);
            this.panel2.Location = new System.Drawing.Point(16, 16);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(184, 288);
            this.panel2.TabIndex = 0;
            // 
            // labelFullPatientDeails
            // 
            this.labelFullPatientDeails.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelFullPatientDeails.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFullPatientDeails.Location = new System.Drawing.Point(8, 64);
            this.labelFullPatientDeails.Name = "labelFullPatientDeails";
            this.labelFullPatientDeails.Size = new System.Drawing.Size(168, 40);
            this.labelFullPatientDeails.TabIndex = 2;
            this.labelFullPatientDeails.Text = "First Name: nana       Last Name: nono    ID:33423                        Age: 55" +
    "            Address: fsdfsd             Tel :546456";
            // 
            // labelHospitalDetails
            // 
            this.labelHospitalDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelHospitalDetails.Location = new System.Drawing.Point(64, 8);
            this.labelHospitalDetails.Name = "labelHospitalDetails";
            this.labelHospitalDetails.Size = new System.Drawing.Size(80, 24);
            this.labelHospitalDetails.TabIndex = 1;
            this.labelHospitalDetails.Text = "Hospital: yada department: yodo";
            // 
            // pictureBoxLogo
            // 
            this.pictureBoxLogo.Image = ((System.Drawing.Image)(resources.GetObject("pictureBoxLogo.Image")));
            this.pictureBoxLogo.Location = new System.Drawing.Point(144, 8);
            this.pictureBoxLogo.Name = "pictureBoxLogo";
            this.pictureBoxLogo.Size = new System.Drawing.Size(32, 24);
            this.pictureBoxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxLogo.TabIndex = 0;
            this.pictureBoxLogo.TabStop = false;
            // 
            // labelDate
            // 
            this.labelDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDate.Location = new System.Drawing.Point(8, 16);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(48, 16);
            this.labelDate.TabIndex = 1;
            this.labelDate.Text = "7.9.2004";
            // 
            // labelTitle
            // 
            this.labelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitle.Location = new System.Drawing.Point(24, 40);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(136, 16);
            this.labelTitle.TabIndex = 1;
            // 
            // labelMinimalPatientDetails
            // 
            this.labelMinimalPatientDetails.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelMinimalPatientDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMinimalPatientDetails.Location = new System.Drawing.Point(8, 64);
            this.labelMinimalPatientDetails.Name = "labelMinimalPatientDetails";
            this.labelMinimalPatientDetails.Size = new System.Drawing.Size(168, 16);
            this.labelMinimalPatientDetails.TabIndex = 2;
            this.labelMinimalPatientDetails.Text = "First Name: nana       Last Name: nono";
            // 
            // labelPatientHistory
            // 
            this.labelPatientHistory.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelPatientHistory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPatientHistory.Location = new System.Drawing.Point(8, 120);
            this.labelPatientHistory.Name = "labelPatientHistory";
            this.labelPatientHistory.Size = new System.Drawing.Size(168, 56);
            this.labelPatientHistory.TabIndex = 2;
            this.labelPatientHistory.Text = "Smoking: no              Hypertension: no TIA: no                       CVA : Yes" +
    "      Atrial Fibrillation : Yes               Coronary By Pass : No";
            // 
            // labelExaminationHistory
            // 
            this.labelExaminationHistory.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelExaminationHistory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExaminationHistory.Location = new System.Drawing.Point(8, 192);
            this.labelExaminationHistory.Name = "labelExaminationHistory";
            this.labelExaminationHistory.Size = new System.Drawing.Size(168, 64);
            this.labelExaminationHistory.TabIndex = 2;
            this.labelExaminationHistory.Text = resources.GetString("labelExaminationHistory.Text");
            // 
            // checkBoxHospitalLogo
            // 
            this.checkBoxHospitalLogo.AutoSize = true;
            this.checkBoxHospitalLogo.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxHospitalLogo.Checked = true;
            this.checkBoxHospitalLogo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxHospitalLogo.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.dsPatientRepLayout1, "tb_PatientRepLayout.HospitalLogo", true));
            this.checkBoxHospitalLogo.Location = new System.Drawing.Point(248, 80);
            this.checkBoxHospitalLogo.Name = "checkBoxHospitalLogo";
            this.checkBoxHospitalLogo.Size = new System.Drawing.Size(91, 17);
            this.checkBoxHospitalLogo.TabIndex = 3;
            this.checkBoxHospitalLogo.Text = "Hospital Logo";
            this.checkBoxHospitalLogo.UseVisualStyleBackColor = false;
            this.checkBoxHospitalLogo.CheckedChanged += new System.EventHandler(this.checkBoxHospitalLogo_CheckedChanged);
            // 
            // dsPatientRepLayout1
            // 
            this.dsPatientRepLayout1.DataSetName = "dsPatientRepLayout";
            this.dsPatientRepLayout1.Locale = new System.Globalization.CultureInfo("en-US");
            this.dsPatientRepLayout1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // checkBoxHospitalDetails
            // 
            this.checkBoxHospitalDetails.AutoSize = true;
            this.checkBoxHospitalDetails.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxHospitalDetails.Checked = true;
            this.checkBoxHospitalDetails.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxHospitalDetails.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.dsPatientRepLayout1, "tb_PatientRepLayout.HospitalDetails", true));
            this.checkBoxHospitalDetails.Location = new System.Drawing.Point(248, 48);
            this.checkBoxHospitalDetails.Name = "checkBoxHospitalDetails";
            this.checkBoxHospitalDetails.Size = new System.Drawing.Size(99, 17);
            this.checkBoxHospitalDetails.TabIndex = 2;
            this.checkBoxHospitalDetails.Text = "Hospital Details";
            this.checkBoxHospitalDetails.UseVisualStyleBackColor = false;
            this.checkBoxHospitalDetails.CheckedChanged += new System.EventHandler(this.checkBoxHospitalDetails_CheckedChanged);
            // 
            // checkBoxDate 
            // 
            this.checkBoxDate.AutoSize = true;
            this.checkBoxDate.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxDate.Checked = true;
            this.checkBoxDate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxDate.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.dsPatientRepLayout1, "tb_PatientRepLayout.DateDisplay", true));
            this.checkBoxDate.Location = new System.Drawing.Point(248, 16);
            this.checkBoxDate.Name = "checkBoxDate";
            this.checkBoxDate.Size = new System.Drawing.Size(86, 17);
            this.checkBoxDate.TabIndex = 2;
            this.checkBoxDate.Text = "Date Display";
            this.checkBoxDate.UseVisualStyleBackColor = false;
            this.checkBoxDate.CheckedChanged += new System.EventHandler(this.checkBoxDate_CheckedChanged);
            this.checkBoxDate.Visible = false;
            // 
            // checkBoxTitle
            // 
            this.checkBoxTitle.AutoSize = true;
            this.checkBoxTitle.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxTitle.Checked = true;
            this.checkBoxTitle.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxTitle.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.dsPatientRepLayout1, "tb_PatientRepLayout.TitleDisplay", true));
            this.checkBoxTitle.Location = new System.Drawing.Point(248, 104);
            this.checkBoxTitle.Name = "checkBoxTitle";
            this.checkBoxTitle.Size = new System.Drawing.Size(83, 17);
            this.checkBoxTitle.TabIndex = 2;
            this.checkBoxTitle.Text = "Title Display";
            this.checkBoxTitle.UseVisualStyleBackColor = false;
            this.checkBoxTitle.CheckedChanged += new System.EventHandler(this.checkBoxTitle_CheckedChanged);
            // 
            // checkBoxHistory
            // 
            this.checkBoxHistory.AutoSize = true;
            this.checkBoxHistory.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxHistory.Checked = true;
            this.checkBoxHistory.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxHistory.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.dsPatientRepLayout1, "tb_PatientRepLayout.PatientHistory", true));
            this.checkBoxHistory.Location = new System.Drawing.Point(248, 200);
            this.checkBoxHistory.Name = "checkBoxHistory";
            this.checkBoxHistory.Size = new System.Drawing.Size(94, 17);
            this.checkBoxHistory.TabIndex = 2;
            this.checkBoxHistory.Text = "Patient History";
            this.checkBoxHistory.UseVisualStyleBackColor = false;
            this.checkBoxHistory.CheckedChanged += new System.EventHandler(this.checkBoxHistory_CheckedChanged);
            // 
            // checkBoxExaminationHistory
            // 
            this.checkBoxExaminationHistory.AutoSize = true;
            this.checkBoxExaminationHistory.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxExaminationHistory.Checked = true;
            this.checkBoxExaminationHistory.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxExaminationHistory.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.dsPatientRepLayout1, "tb_PatientRepLayout.ExaminationHistory", true));
            this.checkBoxExaminationHistory.Location = new System.Drawing.Point(248, 232);
            this.checkBoxExaminationHistory.Name = "checkBoxExaminationHistory";
            this.checkBoxExaminationHistory.Size = new System.Drawing.Size(123, 17);
            this.checkBoxExaminationHistory.TabIndex = 2;
            this.checkBoxExaminationHistory.Text = "Examinations History";
            this.checkBoxExaminationHistory.UseVisualStyleBackColor = false;
            this.checkBoxExaminationHistory.CheckedChanged += new System.EventHandler(this.checkBoxExaminationHistory_CheckedChanged);
            // 
            // checkBoxOnePageReport
            // 
            this.checkBoxOnePageReport.AutoSize = true;
            this.checkBoxOnePageReport.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxOnePageReport.Location = new System.Drawing.Point(248, 282);
            this.checkBoxOnePageReport.Name = "checkBoxOnePageReport";
            this.checkBoxOnePageReport.Size = new System.Drawing.Size(109, 17);
            this.checkBoxOnePageReport.TabIndex = 6;
            this.checkBoxOnePageReport.Text = "One Page Report";
            this.checkBoxOnePageReport.UseVisualStyleBackColor = false;
            this.checkBoxOnePageReport.CheckedChanged += new System.EventHandler(this.checkBoxOnePageReport_CheckedChanged);
            // 
            // wizardControlPage3
            // 
            this.wizardControlPage3.BackgroundColor = new Syncfusion.Drawing.BrushInfo(Syncfusion.Drawing.GradientStyle.Vertical, new Syncfusion.Drawing.BrushInfoColorArrayList(new System.Drawing.Color[] {
                System.Drawing.Color.White,
                System.Drawing.SystemColors.Control}));
            this.wizardControlPage3.BorderColor = System.Drawing.Color.Black;
            this.wizardControlPage3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wizardControlPage3.Controls.Add(this.panelIpImages);
            this.wizardControlPage3.Controls.Add(this.checkBoxSummaryScreen);
            this.wizardControlPage3.Controls.Add(this.checkBoxIncludsNormalValues);
            this.wizardControlPage3.Controls.Add(this.gradientPanel2);
            this.wizardControlPage3.Controls.Add(this.checkBoxTable);
            this.wizardControlPage3.Description = "This is the description of the Wizard Page";
            this.wizardControlPage3.FullPage = false;
            this.wizardControlPage3.LayoutName = "Card3";
            this.wizardControlPage3.Location = new System.Drawing.Point(0, 0);
            this.wizardControlPage3.Name = "wizardControlPage3";
            this.wizardControlPage3.NextPage = null;
            this.wizardControlPage3.PreviousPage = null;
            this.wizardControlPage3.Size = new System.Drawing.Size(592, 392);
            this.wizardControlPage3.TabIndex = 2;
            this.wizardControlPage3.Title = "Page Title";
            // 
            // panelIpImages
            // 
            this.panelIpImages.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelIpImages.Controls.Add(this.lIpImages);
            this.panelIpImages.Controls.Add(this.rBtn4);
            this.panelIpImages.Controls.Add(this.rBtn2);
            this.panelIpImages.Controls.Add(this.rBtn1);
            this.panelIpImages.Controls.Add(this.rBtnNo);
            this.panelIpImages.Location = new System.Drawing.Point(230, 208);
            this.panelIpImages.Name = "panelIpImages";
            this.panelIpImages.Size = new System.Drawing.Size(350, 100);
            this.panelIpImages.TabIndex = 11;
            // 
            // lIpImages
            // 
            this.lIpImages.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lIpImages.AutoSize = true;
            this.lIpImages.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lIpImages.Location = new System.Drawing.Point(83, 10);
            this.lIpImages.Name = "lIpImages";
            this.lIpImages.Size = new System.Drawing.Size(204, 16);
            this.lIpImages.TabIndex = 4;
            this.lIpImages.Text = "Manage DigiLite™ IP Images";
            // 
            // rBtn4
            // 
            this.rBtn4.AutoSize = true;
            this.rBtn4.Location = new System.Drawing.Point(186, 64);
            this.rBtn4.Name = "rBtn4";
            this.rBtn4.Size = new System.Drawing.Size(112, 17);
            this.rBtn4.TabIndex = 3;
            this.rBtn4.Text = "4 images per page";
            // 
            // rBtn2
            // 
            this.rBtn2.AutoSize = true;
            this.rBtn2.Location = new System.Drawing.Point(186, 40);
            this.rBtn2.Name = "rBtn2";
            this.rBtn2.Size = new System.Drawing.Size(112, 17);
            this.rBtn2.TabIndex = 2;
            this.rBtn2.Text = "2 images per page";
            // 
            // rBtn1
            // 
            this.rBtn1.AutoSize = true;
            this.rBtn1.Location = new System.Drawing.Point(16, 64);
            this.rBtn1.Name = "rBtn1";
            this.rBtn1.Size = new System.Drawing.Size(112, 17);
            this.rBtn1.TabIndex = 1;
            this.rBtn1.Text = "1 images per page";
            // 
            // rBtnNo
            // 
            this.rBtnNo.AutoSize = true;
            this.rBtnNo.Checked = true;
            this.rBtnNo.Location = new System.Drawing.Point(16, 40);
            this.rBtnNo.Name = "rBtnNo";
            this.rBtnNo.Size = new System.Drawing.Size(112, 17);
            this.rBtnNo.TabIndex = 0;
            this.rBtnNo.TabStop = true;
            this.rBtnNo.Text = "0 images per page";
            // 
            // checkBoxSummaryScreen
            // 
            this.checkBoxSummaryScreen.AutoSize = true;
            this.checkBoxSummaryScreen.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxSummaryScreen.Location = new System.Drawing.Point(248, 48);
            this.checkBoxSummaryScreen.Name = "checkBoxSummaryScreen";
            this.checkBoxSummaryScreen.Size = new System.Drawing.Size(106, 17);
            this.checkBoxSummaryScreen.TabIndex = 10;
            this.checkBoxSummaryScreen.Text = "Summary Screen";
            this.checkBoxSummaryScreen.UseVisualStyleBackColor = false;
            this.checkBoxSummaryScreen.CheckedChanged += new System.EventHandler(this.checkBoxSummaryScreen_CheckedChanged);
            // 
            // checkBoxIncludsNormalValues
            // 
            this.checkBoxIncludsNormalValues.AutoSize = true;
            this.checkBoxIncludsNormalValues.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxIncludsNormalValues.Checked = true;
            this.checkBoxIncludsNormalValues.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxIncludsNormalValues.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.dsPatientRepLayout1, "tb_PatientRepLayout.NormalValues", true));
            this.checkBoxIncludsNormalValues.Location = new System.Drawing.Point(248, 144);
            this.checkBoxIncludsNormalValues.Name = "checkBoxIncludsNormalValues";
            this.checkBoxIncludsNormalValues.Size = new System.Drawing.Size(137, 17);
            this.checkBoxIncludsNormalValues.TabIndex = 9;
            this.checkBoxIncludsNormalValues.Text = "Includes Normal Values";
            this.checkBoxIncludsNormalValues.UseVisualStyleBackColor = false;
            this.checkBoxIncludsNormalValues.Visible = false;
            // 
            // gradientPanel2
            // 
            this.gradientPanel2.BackgroundColor = new Syncfusion.Drawing.BrushInfo(Syncfusion.Drawing.GradientStyle.Vertical, new Syncfusion.Drawing.BrushInfoColorArrayList(new System.Drawing.Color[] {
                System.Drawing.Color.White,
                System.Drawing.SystemColors.Control}));
            this.gradientPanel2.Border3DStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.gradientPanel2.BorderColor = System.Drawing.Color.Black;
            this.gradientPanel2.Controls.Add(this.panel1);
            this.gradientPanel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.gradientPanel2.Location = new System.Drawing.Point(0, 0);
            this.gradientPanel2.Name = "gradientPanel2";
            this.gradientPanel2.Size = new System.Drawing.Size(224, 392);
            this.gradientPanel2.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelExaminationDisplay);
            this.panel1.Location = new System.Drawing.Point(16, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(184, 288);
            this.panel1.TabIndex = 0;
            // 
            // labelExaminationDisplay
            // 
            this.labelExaminationDisplay.Location = new System.Drawing.Point(16, 16);
            this.labelExaminationDisplay.Name = "labelExaminationDisplay";
            this.labelExaminationDisplay.Size = new System.Drawing.Size(152, 256);
            this.labelExaminationDisplay.TabIndex = 0;
            // 
            // checkBoxTable
            // 
            this.checkBoxTable.AutoSize = true;
            this.checkBoxTable.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxTable.Location = new System.Drawing.Point(248, 96);
            this.checkBoxTable.Name = "checkBoxTable";
            this.checkBoxTable.Size = new System.Drawing.Size(88, 17);
            this.checkBoxTable.TabIndex = 10;
            this.checkBoxTable.Text = "Table Report";
            this.checkBoxTable.UseVisualStyleBackColor = false;
            this.checkBoxTable.CheckedChanged += new System.EventHandler(this.checkBoxTable_CheckedChanged);
            // 
            // wizardControlPage4
            // 
            this.wizardControlPage4.BackgroundColor = new Syncfusion.Drawing.BrushInfo(Syncfusion.Drawing.GradientStyle.Vertical, new Syncfusion.Drawing.BrushInfoColorArrayList(new System.Drawing.Color[] {
                System.Drawing.Color.White,
                System.Drawing.SystemColors.Control}));
            this.wizardControlPage4.BorderColor = System.Drawing.Color.Black;
            this.wizardControlPage4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.wizardControlPage4.CancelOverFinish = false;
            this.wizardControlPage4.Controls.Add(this.checkBoxIndication);
            this.wizardControlPage4.Controls.Add(this.gradientPanel3);
            this.wizardControlPage4.Controls.Add(this.checkBoxInterpretation);
            this.wizardControlPage4.Controls.Add(this.checkBoxSignature);
            this.wizardControlPage4.Description = "This is the description of the Wizard Page";
            this.wizardControlPage4.FullPage = false;
            this.wizardControlPage4.LayoutName = "Card4";
            this.wizardControlPage4.Location = new System.Drawing.Point(0, 0);
            this.wizardControlPage4.Name = "wizardControlPage4";
            this.wizardControlPage4.NextEnabled = false;
            this.wizardControlPage4.NextPage = null;
            this.wizardControlPage4.PreviousPage = null;
            this.wizardControlPage4.Size = new System.Drawing.Size(592, 392);
            this.wizardControlPage4.TabIndex = 3;
            this.wizardControlPage4.Title = "Page Title";
            // 
            // checkBoxIndication
            // 
            this.checkBoxIndication.AutoSize = true;
            this.checkBoxIndication.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxIndication.Checked = true;
            this.checkBoxIndication.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxIndication.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.dsPatientRepLayout1, "tb_PatientRepLayout.Indication", true));
            this.checkBoxIndication.Location = new System.Drawing.Point(256, 48);
            this.checkBoxIndication.Name = "checkBoxIndication";
            this.checkBoxIndication.Size = new System.Drawing.Size(72, 17);
            this.checkBoxIndication.TabIndex = 7;
            this.checkBoxIndication.Text = "Indication";
            this.checkBoxIndication.UseVisualStyleBackColor = false;
            this.checkBoxIndication.CheckedChanged += new System.EventHandler(this.checkBoxIndication_CheckedChanged);
            // 
            // gradientPanel3
            // 
            this.gradientPanel3.BackgroundColor = new Syncfusion.Drawing.BrushInfo(Syncfusion.Drawing.GradientStyle.Vertical, new Syncfusion.Drawing.BrushInfoColorArrayList(new System.Drawing.Color[] {
                System.Drawing.Color.White,
                System.Drawing.SystemColors.Control}));
            this.gradientPanel3.Border3DStyle = System.Windows.Forms.Border3DStyle.Etched;
            this.gradientPanel3.BorderColor = System.Drawing.Color.Black;
            this.gradientPanel3.Controls.Add(this.panel3);
            this.gradientPanel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.gradientPanel3.Location = new System.Drawing.Point(0, 0);
            this.gradientPanel3.Name = "gradientPanel3";
            this.gradientPanel3.Size = new System.Drawing.Size(224, 392);
            this.gradientPanel3.TabIndex = 6;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.labelIndication);
            this.panel3.Controls.Add(this.labelInterpretation);
            this.panel3.Controls.Add(this.labelSignature);
            this.panel3.Location = new System.Drawing.Point(16, 16);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(184, 288);
            this.panel3.TabIndex = 0;
            // 
            // labelIndication
            // 
            this.labelIndication.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelIndication.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIndication.Location = new System.Drawing.Point(8, 24);
            this.labelIndication.Name = "labelIndication";
            this.labelIndication.Size = new System.Drawing.Size(168, 64);
            this.labelIndication.TabIndex = 3;
            // 
            // labelInterpretation
            // 
            this.labelInterpretation.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelInterpretation.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInterpretation.Location = new System.Drawing.Point(8, 120);
            this.labelInterpretation.Name = "labelInterpretation";
            this.labelInterpretation.Size = new System.Drawing.Size(168, 64);
            this.labelInterpretation.TabIndex = 3;
            // 
            // labelSignature
            // 
            this.labelSignature.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelSignature.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSignature.Location = new System.Drawing.Point(8, 224);
            this.labelSignature.Name = "labelSignature";
            this.labelSignature.Size = new System.Drawing.Size(168, 32);
            this.labelSignature.TabIndex = 3;
            // 
            // checkBoxInterpretation
            // 
            this.checkBoxInterpretation.AutoSize = true;
            this.checkBoxInterpretation.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxInterpretation.Checked = true;
            this.checkBoxInterpretation.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxInterpretation.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.dsPatientRepLayout1, "tb_PatientRepLayout.Interpretation", true));
            this.checkBoxInterpretation.Location = new System.Drawing.Point(256, 96);
            this.checkBoxInterpretation.Name = "checkBoxInterpretation";
            this.checkBoxInterpretation.Size = new System.Drawing.Size(88, 17);
            this.checkBoxInterpretation.TabIndex = 7;
            this.checkBoxInterpretation.Text = "Interpretation";
            this.checkBoxInterpretation.UseVisualStyleBackColor = false;
            this.checkBoxInterpretation.CheckedChanged += new System.EventHandler(this.checkBoxInterpretation_CheckedChanged);
            // 
            // checkBoxSignature
            // 
            this.checkBoxSignature.AutoSize = true;
            this.checkBoxSignature.BackColor = System.Drawing.Color.Transparent;
            this.checkBoxSignature.Checked = true;
            this.checkBoxSignature.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxSignature.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.dsPatientRepLayout1, "tb_PatientRepLayout.Signature", true));
            this.checkBoxSignature.Location = new System.Drawing.Point(256, 152);
            this.checkBoxSignature.Name = "checkBoxSignature";
            this.checkBoxSignature.Size = new System.Drawing.Size(71, 17);
            this.checkBoxSignature.TabIndex = 7;
            this.checkBoxSignature.Text = "Signature";
            this.checkBoxSignature.UseVisualStyleBackColor = false;
            this.checkBoxSignature.CheckedChanged += new System.EventHandler(this.checkBoxSignature_CheckedChanged);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "");
            this.imageList1.Images.SetKeyName(1, "");
            this.imageList1.Images.SetKeyName(2, "");
            this.imageList1.Images.SetKeyName(3, "");
            // 
            // ReportWizard
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(592, 430);
            this.Controls.Add(this.wizardControl1);
            this.Name = "ReportWizard";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.ReportWizard_Load);
            ((System.ComponentModel.ISupportInitialize)(this.wizardControl1.GridBagLayout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wizardControl1)).EndInit();
            this.wizardControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.gradientPanel1.ResumeLayout(false);
            this.gradientPanel1.PerformLayout();
            this.wizardContainer1.ResumeLayout(false);
            this.wizardControlPage1.ResumeLayout(false);
            this.wizardControlPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.wizardControlPage2.ResumeLayout(false);
            this.wizardControlPage2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gradientPanelPreview.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dsPatientRepLayout1)).EndInit();
            this.wizardControlPage3.ResumeLayout(false);
            this.wizardControlPage3.PerformLayout();
            this.panelIpImages.ResumeLayout(false);
            this.panelIpImages.PerformLayout();
            this.gradientPanel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.wizardControlPage4.ResumeLayout(false);
            this.wizardControlPage4.PerformLayout();
            this.gradientPanel3.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Syncfusion.Windows.Forms.Tools.WizardControl wizardControl1;
        private Syncfusion.Windows.Forms.Tools.GradientPanel gradientPanel1;
        private Syncfusion.Windows.Forms.Tools.WizardContainer wizardContainer1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Syncfusion.Windows.Forms.Tools.WizardControlPage wizardControlPage1;
        private Syncfusion.Windows.Forms.Tools.WizardControlPage wizardControlPage2;
        private Syncfusion.Windows.Forms.Tools.WizardControlPage wizardControlPage3;
        private Syncfusion.Windows.Forms.Tools.WizardControlPage wizardControlPage4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel2;
        private Syncfusion.Windows.Forms.Tools.GradientPanel gradientPanelPreview;
        private System.Windows.Forms.PictureBox pictureBoxLogo;
        private System.Windows.Forms.Label labelHospitalDetails;
        private System.Windows.Forms.CheckBox checkBoxHospitalLogo;
        private System.Windows.Forms.CheckBox checkBoxHospitalDetails;
        private System.Windows.Forms.CheckBox checkBoxDate;
        private System.Windows.Forms.Label labelDate;
        private Syncfusion.Windows.Forms.Tools.GradientPanel gradientPanel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox checkBoxTitle;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButtonFullPatientDetails;
        private System.Windows.Forms.RadioButton radioButtonMinimalPatientDetails;
        private System.Windows.Forms.Label labelFullPatientDeails;
        private System.Windows.Forms.Label labelMinimalPatientDetails;
        private System.Windows.Forms.Label labelPatientHistory;
        private System.Windows.Forms.CheckBox checkBoxHistory;
        private System.Windows.Forms.CheckBox checkBoxExaminationHistory;
        private System.Windows.Forms.CheckBox checkBoxOnePageReport;
        private System.Windows.Forms.Label labelExaminationHistory;
        private Syncfusion.Windows.Forms.Tools.GradientPanel gradientPanel3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label labelExaminationDisplay;
        private System.Windows.Forms.CheckBox checkBoxIndication;
        private System.Windows.Forms.CheckBox checkBoxInterpretation;
        private System.Windows.Forms.CheckBox checkBoxSignature;
        private System.Windows.Forms.Label labelIndication;
        private System.Windows.Forms.Label labelInterpretation;
        private System.Windows.Forms.Label labelSignature;
        private DAL.dsPatientRepLayout dsPatientRepLayout1;
        private System.Windows.Forms.CheckBox checkBoxIncludsNormalValues;
        private System.Windows.Forms.CheckBox checkBoxSummaryScreen;
        private System.Windows.Forms.CheckBox checkBoxTable;
        private System.Windows.Forms.Panel panelIpImages;
        private System.Windows.Forms.RadioButton rBtnNo;
        private System.Windows.Forms.RadioButton rBtn1;
        private System.Windows.Forms.RadioButton rBtn2;
        private System.Windows.Forms.RadioButton rBtn4;
        private System.Windows.Forms.Label lIpImages;
        private System.ComponentModel.IContainer components;
        private ReportMgr.CachedcrTableRep cachedcrTableRep1;
    }
}