using Rimed.Framework.Common;

namespace Rimed.TCD.GUI
{
    partial class SummarySpectrumChart
    {
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }

            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SummarySpectrumChart));
            this.summaryClinicalBar1 = new SummaryClinicalBar();
            this.SpectrumGradient = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.spectrumYAxis1 = new SpectrumYAxis();
            ((System.ComponentModel.ISupportInitialize)(this.SpectrumGradient)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // summaryClinicalBar1
            // 
            this.summaryClinicalBar1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("summaryClinicalBar1.BackgroundImage")));
            this.summaryClinicalBar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.summaryClinicalBar1.Location = new System.Drawing.Point(240, 0);
            this.summaryClinicalBar1.Name = "summaryClinicalBar1";
            this.summaryClinicalBar1.Size = new System.Drawing.Size(64, 104);
            this.summaryClinicalBar1.SizeMode = GlobalTypes.ESizeMode.Small;
            this.summaryClinicalBar1.TabIndex = 6;
            // 
            // SpectrumGradient
            // 
            this.SpectrumGradient.Dock = System.Windows.Forms.DockStyle.Right;
            this.SpectrumGradient.Image = ((System.Drawing.Image)(resources.GetObject("SpectrumGradient.Image")));
            this.SpectrumGradient.Location = new System.Drawing.Point(231, 0);
            this.SpectrumGradient.Name = "SpectrumGradient";
            this.SpectrumGradient.Size = new System.Drawing.Size(9, 104);
            this.SpectrumGradient.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.SpectrumGradient.TabIndex = 7;
            this.SpectrumGradient.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(304, 104);
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // spectrumYAxis1
            // 
            this.spectrumYAxis1.BackColor = System.Drawing.SystemColors.Control;
            this.spectrumYAxis1.Dock = System.Windows.Forms.DockStyle.Left;
            this.spectrumYAxis1.Location = new System.Drawing.Point(0, 0);
            this.spectrumYAxis1.MaxValue = 0D;
            this.spectrumYAxis1.MinValue = 0D;
            this.spectrumYAxis1.Name = "spectrumYAxis1";
            this.spectrumYAxis1.Size = new System.Drawing.Size(30, 104);
            this.spectrumYAxis1.TabIndex = 13;
            // 
            // SummarySpectrumChart
            // 
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.spectrumYAxis1);
            this.Controls.Add(this.SpectrumGradient);
            this.Controls.Add(this.summaryClinicalBar1);
            this.Name = "SummarySpectrumChart";
            this.Size = new System.Drawing.Size(304, 104);
            ((System.ComponentModel.ISupportInitialize)(this.SpectrumGradient)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }
        
        #endregion

        public SummaryClinicalBar summaryClinicalBar1;
        private System.Windows.Forms.PictureBox SpectrumGradient;
        public System.Windows.Forms.PictureBox pictureBox1;
        public SpectrumYAxis spectrumYAxis1;
        private System.ComponentModel.IContainer components = null;
    }
}
