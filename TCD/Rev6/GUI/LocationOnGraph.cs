﻿using System;
using Rimed.Framework.Common;

namespace Rimed.TCD.GUI
{
	public class LocationOnGraph
	{
		private const int	DEFAULT_SPEED_FACTOR = 2;
		private TimeSpan	m_startTime;
		private int			m_speedFactor = DEFAULT_SPEED_FACTOR;
		private int			m_width;

		public LocationOnGraph(int width, int speedFactor = DEFAULT_SPEED_FACTOR)
		{
			m_width			= width;
			m_speedFactor	= speedFactor;
			
			reset();
		}


		public override string ToString()
		{
			return string.Format("LocationOnGraph: Width={0}, StartTime={1}, TimeInGraph={2}, PixelPerSecond={3}, RelativeTime={4}, LastDrawTime={5}, LastDrawTimePosition={6}"
									, Width, StartTime, TimeInGraph, PixelPerSecond, RelativeTime, LastDrawTime, LastDrawTimePosition);
		}
		

		public int		Width
		{
			get
			{
				return m_width;
			}
			set
			{
				if (m_width == value)
					return;

				m_width = value;
				setTimeOnGraph();	//TimeInGraph = m_width * GlobalSettings.FFT_COLUMN_IN_SEC;
			}
		}
		public TimeSpan StartTime
		{
			get { return m_startTime; }
			set
			{
				m_startTime = value;
				if (m_startTime.TotalSeconds < 0)
					m_startTime = TimeSpan.Zero;//.FromSeconds(0);
			}
		}
		public TimeSpan LastDrawTime			{ get; set; }
		public TimeSpan RelativeTime			{ get { return LastDrawTime.Subtract(StartTime); } }
		public int		LastDrawTimePosition	{ get { return TimeToPosition(LastDrawTime); }}
		public double	TimeInGraph { get; private set; }
		public int		SpeedFactor
		{
			get
			{
				return m_speedFactor;
			}
			set
			{
				if (m_speedFactor == value)
					return;

				m_speedFactor = value;
				setTimeOnGraph();
			}
		}
		public int		PixelPerSecond
		{
			get
			{
				var secPerPixel = TimeInGraph / Width;
				return (int)Math.Round(1 / secPerPixel);
			}
		}


		public double	PositionToTime(int pos, int imageWidth, bool isDrawToBmp = false)
		{
			var timeInGraph = TimeInGraph;
			if (isDrawToBmp) // generate graph bitmap for the summaryScreen.
			{
				timeInGraph = LayoutManager.Instance.TimeInGraph;
				if (timeInGraph < 0)
				{
					timeInGraph = 0.1;
					Logger.LogError(new ArgumentException("Total graph time can not be zero or negative."));
				}
			}

			var relativeTime = RelativeTime.TotalSeconds;

			int curCol;
            if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Online)
				curCol = timeToPosition(LastDrawTime.TotalSeconds, imageWidth, isDrawToBmp);
            else
                curCol = (RelativeTime.TotalSeconds >= timeInGraph) ? imageWidth : timeToPosition(relativeTime, imageWidth, isDrawToBmp);

			double res;
			var secPerPixel = timeInGraph / imageWidth;
            if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Online)
            {
				if (pos <= curCol)
					res = (Math.Floor(relativeTime / timeInGraph)) * timeInGraph + pos * secPerPixel;
				else
					res = (Math.Floor(relativeTime / timeInGraph) - 1) * timeInGraph + pos * secPerPixel;
            }
            else
                res = Math.Round(relativeTime - (curCol - pos) * secPerPixel, 3);

			if (res < 0 || res >= relativeTime)
				return -1;

			res += StartTime.TotalSeconds;

			return res;
		}
		public double	PositionToTime(int pos)
		{
			return PositionToTime(pos, Width);
		}

		public int		TimeToPosition(TimeSpan timespan)
		{
			return timeToPosition(timespan.TotalSeconds, Width);
		}
		public int		TimeToPosition(double seconds)
		{
			return timeToPosition(seconds, Width);
		}

		public double	PositionToTimeSimple(int pos, int imageWidth)
		{
			//relative time in ms at last painted position
			var relativeTime	= TimeInGraph * 1000.0 * pos / imageWidth;

			//relative time in ms at start screen position
			var startScreenTime	= (new TimeSpan(MainForm.Instance.MonitoringReplayTimeCounter.Ticks) - new TimeSpan(EventListViewCtrl.Instance.ExaminationDate.Ticks)).TotalMilliseconds;

			return startScreenTime + relativeTime;
		}


		private void	reset()
		{
			StartTime		= TimeSpan.Zero;
			LastDrawTime	= TimeSpan.Zero;
			setTimeOnGraph();
		}

		private int		timeToPosition(double seconds, int imageWidth, bool isDrawToBmp = false)
		{
			var timeInGraph = TimeInGraph;
			if (isDrawToBmp) // Generate graph bitmap for the summaryScreen.
			{
				timeInGraph = LayoutManager.Instance.TimeInGraph;
				if (timeInGraph <= 0)
					timeInGraph = 0.1;
			}

			var res = 0;
			if (LayoutManager.SystemMode == GlobalTypes.ESystemModes.Online || LastDrawTime.TotalSeconds < timeInGraph)
			{
				var timeOnGraph = (seconds - StartTime.TotalSeconds) % timeInGraph;
				var secPerPixel = timeInGraph / imageWidth;

				res = (int)Math.Round(timeOnGraph / secPerPixel);
			}
			else // Offline || m_lastDrawTime > totalTimeInGraph_Sec
			{
				var timeDiff = LastDrawTime.TotalSeconds - seconds;
				var pixelPerSec = imageWidth / timeInGraph;

				res = imageWidth - (int)Math.Round(timeDiff * pixelPerSec);
			}

			return res;
		}

		private void	setTimeOnGraph()
		{
			var tmp = m_width * GlobalSettings.FFT_COLUMN_IN_SEC * m_speedFactor / 2;
			if (tmp <= 0)
			{
				Logger.LogWarning("Total graph time can not be zero or negative. Value set to 0.1. value={0}, Width={1}, SpeedFactor={2}", tmp, m_width, m_speedFactor);
				tmp = 0.1;
			}

			TimeInGraph = tmp;
		}
	}
}
