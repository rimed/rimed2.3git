#include "CommLayerBoundary.h"
#include "DemoMgr.h"

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
/*
	Boundary Objects should be minimized!
	.NET4 is not able to catch System.AccessViolationException

	It is difficult to debug in house and limited visibility at customer's site.
*/

using namespace Rimed::TCD::CommunicationLayer;
using namespace Rimed::TCD::Infrastructure;

CAutoScanDummyDataEntry::CAutoScanDummyDataEntry(array<int> ^pData)
{
	m_Data = pData;
}

Byte CAutoScanDummyDataEntry::DummyDataGetPixel(int PixelIndex)
{
	return m_Data[PixelIndex];
}

void CCommLayerBoundary::Init(CInfraBoundary ^pInfraBoundary, CDebugBoundary ^pDebugBoundary)
{
	IntPtr iptr  =pInfraBoundary->InfraGetNativeObj();
	m_InfraBoundary = pInfraBoundary;
	m_pInfra = (Infra *)iptr.ToPointer();
	iptr = pDebugBoundary->RmdDebugGetNativeObj();
	m_pRmdDebug = (CRmdDebug*)iptr.ToPointer();
	disposed  = false;
	m_pEthMgr = NULL;
	m_Config = nullptr;
	m_IsDemo = false;
	m_DemoAutoScanColumnArr = NULL;
	m_HwInfo = new R_IP_HARDWARE_INFO;
	m_HwInfo->fpgaSubVersion = 0;
	m_HwInfo->fpgaMainVersion = 0;
	m_HwInfo->dspVersionNum = 0;
	m_HwInfo->dspVersionFormat = 0;
	m_HwInfoValid = false;
}

CCommLayerBoundary::CCommLayerBoundary(CInfraBoundary ^pInfraBoundary, CDebugBoundary ^pDebugBoundary)
{
	Init(pInfraBoundary, pDebugBoundary);
}

CCommLayerBoundary::CCommLayerBoundary(CInfraBoundary ^pInfraBoundary, CDebugBoundary ^pDebugBoundary, array<CAutoScanDummyDataEntry^> ^AutoScanDummyDataArr)
{

	int i, pixelIndex;
	Init(pInfraBoundary, pDebugBoundary);
	m_IsDemo = true;
	if(AutoScanDummyDataArr==nullptr)
	{
		m_DemoAutoScanColumnArr = NULL;
		m_DemoAutoScanColumnArrLength = 0;
		return;
	}

	m_DemoAutoScanColumnArrLength = AutoScanDummyDataArr->Length;
	m_DemoAutoScanColumnArr = (R_Auto_Scan_Column *)m_pInfra->InfraMalloc(sizeof(R_Auto_Scan_Column)*AutoScanDummyDataArr->Length); 
	for (i=0; i<AutoScanDummyDataArr->Length; i++) 
	{
		for(pixelIndex = 0; pixelIndex<DspBlock::NUM_OF_PIXELS_IN_AUTOSCAN_COLMN; pixelIndex++)
		{
			m_DemoAutoScanColumnArr[i].Pixel[pixelIndex] = AutoScanDummyDataArr[i]->DummyDataGetPixel(pixelIndex);
		}
	}
}

CCommLayerBoundary::!CCommLayerBoundary(void)
{
	if(!disposed)
	{
		if(m_pEthMgr!=NULL) 
		{
			CommLayerBoundaryStop();
		}
		
		if(m_DemoAutoScanColumnArr!=NULL) 
		{
			m_pInfra->InfraFree(m_DemoAutoScanColumnArr, m_DemoAutoScanColumnArrLength*sizeof(R_Auto_Scan_Column));
			m_DemoAutoScanColumnArr = NULL;
		}
		delete m_HwInfo;
	}
}
CCommLayerBoundary::~CCommLayerBoundary(void)
{
	this->!CCommLayerBoundary();
	disposed = true;
}

void CCommLayerBoundary::CommLayerEthConfigSet(CComLayerConfigEth ^pConfigEth)
{
	m_Config = pConfigEth;
}
 
bool CCommLayerBoundary::CommLayerBoundaryStart()
{
	DBG_TRACE(m_pRmdDebug, INFO_TRC, "CCommLayerBoundary::CommLayerBoundaryStart: ENTER");

	RMD_ERROR_CODE RmdErr;
	CComLayerConfigEth ^pConfigEth;
	if(m_IsDemo) 
	{
		INFRA_NEW_OBJ(m_pInfra, m_pEthMgr, CDemoMgr, m_pInfra, m_pRmdDebug, m_DemoAutoScanColumnArr, m_DemoAutoScanColumnArrLength);
		RmdErr = m_pEthMgr->EthMgrStart(RMD_TCP_SERVER_PORT_DEFAULT, INADDR_ANY);
	}
	else 
	{
		INFRA_NEW_OBJ(m_pInfra, m_pEthMgr, CEthMgr, m_pInfra, m_pRmdDebug);

		if(m_Config ==nullptr)
			RmdErr = m_pEthMgr->EthMgrStart(RMD_TCP_SERVER_PORT_DEFAULT, INADDR_ANY);
		else  
		{
			pConfigEth = (CComLayerConfigEth ^)m_Config;
			RmdErr = m_pEthMgr->EthMgrStart(pConfigEth->usPort, pConfigEth->ulAddr);
		}
	}

	if(RmdErr != RMD_SUCCESS) 
	{
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CCommLayerBoundary::CommLayerBoundaryStart: EthMgrStart failed=%d", RmdErr);

		INFRA_DELETE_OBJ(m_pInfra, m_pEthMgr, CEthMgr);
		m_pEthMgr = NULL;

		return false;
	}	

	DBG_TRACE(m_pRmdDebug, INFO_TRC, "CCommLayerBoundary::CommLayerBoundaryStart: LEAVE");

	return true;
}

bool CCommLayerBoundary::CommLayerBoundaryStop()
{
	RMD_ERROR_CODE RmdErr;
	
	if(m_pEthMgr == NULL) 
	{
		return false;
	}

	RmdErr = m_pEthMgr->EthMgrStop();
	if(m_IsDemo) 
	{
		CDemoMgr *pDemoMgr = (CDemoMgr *)m_pEthMgr;
		INFRA_DELETE_OBJ(m_pInfra, pDemoMgr, CDemoMgr);
	}
	else 
	{
		INFRA_DELETE_OBJ(m_pInfra, m_pEthMgr, CEthMgr);
	}

	m_pEthMgr = NULL;	

	return true;
}

eRmdErrorCode CCommLayerBoundary::ComLayerBoundaryFrameRx(array<BYTE>^ %pData, int iMaxDataLen, eDspMsgType usMsgType, int %OutDataLen)
{
	eRmdErrorCode RmdErr;
	int Len;
	try 
	{
		if(!m_pEthMgr->EthMgrFrameRxReady((USHORT)usMsgType))
		{
			OutDataLen = 0;
			pData = nullptr;

			return eRmdErrorCode::eRMD_LIST_EMPTY;
		}

		pData = gcnew array<BYTE>(iMaxDataLen);
		pin_ptr<Byte> dest;
		dest = &pData[0];
		RmdErr = (eRmdErrorCode)m_pEthMgr->EthMgrFrameRx((char*)dest, iMaxDataLen, (USHORT)usMsgType, &Len);
		OutDataLen = Len;
	}
	catch(...)
	{
		try
		{
			m_pEthMgr->ClearRxFramesFifo();
		}
		catch(...)
		{
			DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CCommLayerBoundary::ComLayerBoundaryFrameRx: ClearRxFramesFifo() FAIL.");
		}

		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CCommLayerBoundary::ComLayerBoundaryFrameRx: eRMD_NOT_ENOUGH_SPACE_ALLOCATION");

		RmdErr = eRmdErrorCode::eRMD_NOT_ENOUGH_SPACE_ALLOCATION; 	
	}

	return RmdErr;
}

eRmdErrorCode CCommLayerBoundary::ComLayerBoundaryFrameRx(array<BYTE>^ pData, eDspMsgType usMsgType, int %OutDataLen)
{
	eRmdErrorCode RmdErr;
	int Len;
	try 
	{
		if(!m_pEthMgr->EthMgrFrameRxReady((USHORT)usMsgType))
		{
			OutDataLen = 0;

			return eRmdErrorCode::eRMD_LIST_EMPTY;
		}

		int iMaxDataLen = pData->Length;

		pin_ptr<Byte> dest	= &pData[0];
		RmdErr				= (eRmdErrorCode)m_pEthMgr->EthMgrFrameRx((char*)dest, iMaxDataLen, (USHORT)usMsgType, &Len);
		OutDataLen			= Len;
	}
	catch(...)
	{
		try
		{
			m_pEthMgr->ClearRxFramesFifo();
		}
		catch(...)
		{
			DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CCommLayerBoundary::ComLayerBoundaryFrameRx: ClearRxFramesFifo() FAIL.");
		}

		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CCommLayerBoundary::ComLayerBoundaryFrameRx: eRMD_NOT_ENOUGH_SPACE_ALLOCATION");

		RmdErr = eRmdErrorCode::eRMD_NOT_ENOUGH_SPACE_ALLOCATION;
	}

	return RmdErr;
}

RMD_ERROR_CODE CCommLayerBoundary::ComLayerBoundaryFrameTx(array<BYTE>^ pData, eDspMsgType usMsgType)
{
	char *UnmanagedSrc;
	RMD_ERROR_CODE RmdErr;

	int iDataLen		= pData->Length;
	pin_ptr<Byte> src	= &pData[0];
	UnmanagedSrc		= reinterpret_cast<char*>(src), pData->Length;
	RmdErr				= m_pEthMgr->EthMgrFrameTx(UnmanagedSrc, iDataLen, (USHORT)usMsgType);

	return RmdErr;
}

bool CCommLayerBoundary::HwConnected(void)
{
	int HwInfoRqst;
	int outDataLen = 0;
	RMD_ERROR_CODE RmdErr;
	if(m_pEthMgr->TcpServerGetNumOfClients()>0) 
	{
		if(m_HwInfoValid) 
		{
			return true;
		}
		else 
		{			
			RmdErr = m_pEthMgr->EthMgrRxHwInfoResponse((char*)m_HwInfo, sizeof(R_IP_HARDWARE_INFO), HW_INFO_RESPONSE, &outDataLen);
			if(RmdErr == RMD_SUCCESS) 
			{
				m_HwInfoValid = true;
				return true;
			}

			RmdErr = m_pEthMgr->EthMgrFrameTx((char*)(&HwInfoRqst), sizeof(int), HW_INFO_REQ);
		}
	}

	return false;
}

bool CCommLayerBoundary::GetHwInfo(unsigned int %fpgaSubVersion, unsigned int %fpgaMainVersion,	unsigned int %dspVersionNum, unsigned int %dspVersionFormat)
{
	if(!m_HwInfoValid) 
	{
		return false;
	}		

	fpgaSubVersion = m_HwInfo->fpgaSubVersion;
	fpgaMainVersion = m_HwInfo->fpgaMainVersion;
	dspVersionNum =  m_HwInfo->dspVersionNum;
	dspVersionFormat = m_HwInfo->dspVersionFormat;

	return true;
}