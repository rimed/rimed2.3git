#pragma once

//#include "RmdTcpServer.h"
#include "EthMgr.h"
//#include "InfraBoundary.h"
#include "TCD_DSP2PC.H"
#include "DemoMgr.h"

using namespace System;
using namespace Rimed::TCD::DspBlock;
using namespace Rimed::TCD::ManagedInfrastructure;

namespace Rimed
{
	namespace TCD 
{
	namespace CommunicationLayer
	{
		using namespace RmdTcpServer;
		public enum class eDspMsgType {

			HANDSHAKE_REQ_TYPE = HANDSHAKE_REQ,
			LOAD_DSP_REQ_TYPE = LOAD_DSP_REQ,
			PC2DSP_MSG_TYPE = PC2DSP_MSG,
			REPLAY_DSP2PC_TYPE = REPLAY_DSP2PC,
			HW_INFO_REQ_TYPE = HW_INFO_REQ,
			BURN_DSP_REQ_TYPE = BURN_DSP_REQ,
			BURN_FPGA_REQ_TYPE = BURN_FPGA_REQ,
			 
			 HANDSHAKE_RESPONSE_TYPE = HANDSHAKE_RESPONSE,
			 LOAD_DSP_RESPONSE_TYPE = LOAD_DSP_RESPONSE,
			 DSP2PC_MSG_TYPE = DSP2PC_MSG,
			 HW_INFO_RESPONSE_TYPE = HW_INFO_RESPONSE,
			 BURN_DSP_RESPONSE_TYPE = BURN_DSP_RESPONSE,
			 BURN_FPGA_RESPONSE_TYPE = BURN_FPGA_RESPONSE,
			 DEBUG_INFO_REQUEST_TYPE = DEBUG_INFO_REQUEST
		};
		
		public ref class CComLayerConfigEth {
		public:
			literal USHORT MNGD_RMD_TCP_SERVER_PORT_DEFAULT = RMD_TCP_SERVER_PORT_DEFAULT;
			literal ULONG MNGD_INADDR_ANY = INADDR_ANY;
			literal ULONG MNGD_INADDR_LOOPBACK = INADDR_LOOPBACK;
			CComLayerConfigEth() {;}

			USHORT usPort;
			ULONG ulAddr;
		};
		
		public ref class CAutoScanDummyDataEntry
		{
		public:
			CAutoScanDummyDataEntry(array<int>^ pData);
			Byte DummyDataGetPixel(int PixelIndex);
		private:
			array<int>^ m_Data;
		};

		public ref class CCommLayerBoundary : IDisposable {
		public:
			CCommLayerBoundary(CInfraBoundary ^pInfraBoundary, CDebugBoundary ^pDebugBoundary, array<CAutoScanDummyDataEntry^> ^AutoScanDummyDataArr);
			CCommLayerBoundary(CInfraBoundary ^pInfraBoundary, CDebugBoundary ^pDebugBoundary);
			~CCommLayerBoundary();
			!CCommLayerBoundary();
			
			void CommLayerEthConfigSet(CComLayerConfigEth ^pConfigEth);
			bool CommLayerBoundaryStart();
			bool CommLayerBoundaryStop();
			eRmdErrorCode ComLayerBoundaryFrameRx(array<BYTE>^ %pData, int iMaxDataLen, eDspMsgType usMsgType, int %OutDataLen);
			eRmdErrorCode ComLayerBoundaryFrameRx(array<BYTE>^ pData, eDspMsgType usMsgType, int %OutDataLen);
			RMD_ERROR_CODE ComLayerBoundaryFrameTx(array<BYTE>^ pData, eDspMsgType usMsgType);
			bool GetHwInfo(unsigned int %fpgaSubVersion, unsigned int %fpgaMainVersion,	unsigned int %dspVersionNum, unsigned int %dspVersionFormat); // MB_Buff[0][15]);
			bool HwConnected(void);
		private:
			//IntPtr m_pRmdTcpServer;
			CEthMgr *m_pEthMgr;
			Infra *m_pInfra;
			CRmdDebug *m_pRmdDebug;
			bool disposed;
			CInfraBoundary ^m_InfraBoundary;
			Object ^m_Config;
			bool m_IsDemo;
			R_Auto_Scan_Column *m_DemoAutoScanColumnArr;
			int m_DemoAutoScanColumnArrLength;
			CDemoMgr *m_DemoMgr;
			R_IP_HARDWARE_INFO *m_HwInfo;
			bool m_HwInfoValid;
			void Init(CInfraBoundary ^pInfraBoundary, CDebugBoundary ^pDebugBoundary);
		};
	}
}
}