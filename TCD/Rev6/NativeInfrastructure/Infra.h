#ifndef _INFRA_H
#define _INFRA_H

#include <map>
#include "ErrorCodes.h"
#include "RmdDebug.h"

//#define INFRA_NEW_STRUCT(InfraInst, VarName, TypeName) {VarName = new TypeName; Infra::GetInstance()->RegClass(#TypeName);}
//#define INFRA_DELETE_STRUCT(InfraInst, VarName, TypeName) delete VarName; Infra::GetInstance()->DeregClass (#TypeName)

#define INFRA_NEW_OBJ(InfraInst, VarName, TypeName, ...) {VarName = new TypeName(__VA_ARGS__); DBG_ASSERT(InfraInst->GetDebugInst(),VarName!=NULL); InfraInst->RegClass(#TypeName);}
#define INFRA_DELETE_OBJ(InfraInst, VarName, TypeName){DBG_ASSERT(InfraInst->GetDebugInst(),VarName!=NULL); delete VarName; InfraInst->DeregClass (#TypeName);}

typedef std::map<std::string,unsigned int> CLASS_MAP;

namespace Rimed
{
	namespace TCD 
{
	namespace Infrastructure
	{
		class Infra {
		public:
#pragma region Constructors
			Infra (CRmdDebug *RmdDebug);
			~Infra ();
#pragma endregion

#pragma region Methods
			//static RMD_ERROR_CODE Release ();

			RMD_ERROR_CODE SocketLibStart(void);
			RMD_ERROR_CODE SocketLibStop(void);
			//classes
			void RegClass (const char *szClassName);
			void DeregClass (const char *szClassName);
			//thread
			void InfraThreadRegister();
			void InfraThreadDeregister();
			//Mutex
			int InfraMutexRegister();
			void InfraMutexDeregister();
			//event
			void InfraEventRegister(void);
			void InfraEventDeregister(void);
			//memory
			void *InfraMalloc (unsigned int Size);
			void InfraFree (void *pMemory, unsigned int StatisticSize);
			CRmdDebug *GetDebugInst();
#pragma endregion
		private:
#pragma region Variables
			CRmdDebug *m_pRmdDebug;
			int m_ThreadInUse, m_HandlesInUse, m_ClassesInUse,m_EventsInUse, m_MutexInUse;
			long m_MemoryInUse;
		
#pragma endregion
#pragma region Methods
#if 0
			std::map<void*,unsigned int> MemoryMap;
			std::map<std::string,unsigned int> m_ClassMap;
#endif
#pragma endregion
		};


	};
}
}
#endif

