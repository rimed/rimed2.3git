#pragma once

#include "RmdDebug.h"
#include "Infra.h"

#define RMD_MSG_MAX 0x7FFF

#define RMD_MSG_BASE	WM_USER+100
#define RMD_MSG_KILL	RMD_MSG_BASE
#define RMD_MSG_EMPTY	RMD_MSG_KILL +1
#define RMD_MSG_INFRA_MAX RMD_MSG_EMPTY


//RMD_MSG_BASE		= 1124 (1024 +100)
//RMD_MSG_KILL		= 1124 (=RMD_MSG_BASE)
//RMD_MSG_EMPTY		= 1125 (=RMD_MSG_KILL +1)
//RMD_MSG_INFRA_MAX	= 1125 (=RMD_MSG_EMPTY)
namespace Rimed
{
	namespace TCD 
{
	namespace Infrastructure
	{
			typedef enum {
				RMD_THREAD_NOT_INITIALIZED,
				RMD_THREAD_STOP,
				RMD_THREAD_RUNNING,
			} E_RMD_THREAD_STATUS;

			class CRmdMutex {
			public:
				CRmdMutex(Infra *pInfra, CRmdDebug *pRmdDebug);
				~CRmdMutex();
				void RmdMutexCreate (int *Label);
				RMD_ERROR_CODE RmdMutexAcquire(DWORD dwMilliseconds);
				void RmdMutexRelease(void); 
			private:
				HANDLE m_hMutex;
				unsigned long m_ulAcquire;
				int m_Label;
				CRmdDebug *m_pRmdDebug;
				Infra *m_pInfra;
			};

			class CRmdEvent {
			public:
				CRmdEvent(Infra *pInfra, CRmdDebug *pRmdDebug);
				~CRmdEvent();
				void RmdEventCreate (bool boManualReset, bool boInitialState, int nEventLabel);
				void RmdEventSet();
				void RmdEventReset();
				DWORD	RmdEventWait(DWORD dwMilliseconds);
				unsigned long RmdWaitForMultipleEvents(int nCount, CRmdEvent **pObjs,BOOL boWaitAll, unsigned long uMilliseconds);
			private:
				HANDLE m_hEvent;
				int m_Label;
				int m_WaitCount;
				CRmdDebug *m_pRmdDebug;
				Infra *m_pInfra;
			};
			
			class CRmdThread {
			public:
				CRmdThread(Infra *m_pInfra, CRmdDebug *pRmdDebug);
				~CRmdThread();
				void RmdThreadStart (LPTHREAD_START_ROUTINE pfnThreadFn, void *pParameter, DWORD *dwThreadId);
				void RmdThreadStop ();
				void RmdPostMsg (unsigned int uMsg, unsigned int  uParam, unsigned long  ulParam);
				BOOL RmdPeekMsg (unsigned int* puMsg, unsigned int *puParam, unsigned long *pulParam);
				E_RMD_THREAD_STATUS RmdRunTimeFn (unsigned long uWaitEventMsecs);
				void  RmdThreadFinalize (unsigned long ulTimeOut);
			private:
				HANDLE m_hThread;
				DWORD m_dwThreadId;
				CRmdEvent *m_pInitEvent;
				CRmdEvent *m_pStopEvent;
				CRmdEvent *m_pMessageEvent;
				BOOL m_boAtFirstInit;
				LPTHREAD_START_ROUTINE m_pfnThreadFn;
				CRmdDebug *m_pRmdDebug;
				Infra *m_pInfra;
				unsigned int m_pendingMessages;
			};
		};
	}
}
