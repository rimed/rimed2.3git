#ifndef _ERROR_CODES_H
#define _ERROR_CODES_H

typedef  long RMD_ERROR_CODE;
#define RMD_SUCCESS                                  0
#define RMD_FAILED                                  -1
#define RMD_THREAD_TERMINATION_TIMEOUT              -3
#define RMD_ERR_FREE_NATIVE_RESOURCE                -4
#define RMD_DOUBLE_FAULT                            -5
#define RMD_NOT_ENOUGH_SPACE_ALLOCATION             -6
#define RMD_WRONG_STATE                             -8
#define RMD_DEVICE_NOT_READY                        -10
#define RMD_FILE_ERROR                              -12
#define RMD_FILE_NOT_FOUND                          -13
#define RMD_WAIT_TIMEOUT_ERR                        -18
#define RMD_OBJ_NOT_INITIALIZED                     -21
#define RMD_LIST_EMPTY                              -22
#define RMD_DEVICE_UNKNOWN_TYPE                     -23
#define RMD_INVALID_PARAM                           -24
#define RMD_MORE_DATA                               -25
#define RMD_DISCARD_UNKNOWN_MESSAGE                 -26
#define RMD_INPUT_BUFF_TOO_LARGE                    -27
#define RMD_SM_UNKNOWN_EVENT                        -29
#define RMD_SM_WRONG_EVENT4STATE                    -30
#define RMD_ABNORMAL_KERNEL_EXECUTION               -31
#define RMD_SOCKET_CLOSED                           -32
#define RMD_UNINTENDED_EXECUTION                    -33
#define RMD_ERR_FRAME_DISASSEMBLED					-34
#define RMD_ERR_BUFFER_SIZE_MISMATCH_SHORT			-35
#define RMD_ERR_BUFFER_SIZE_MISMATCH_LONG			-36
#define RMD_SOCKET_RECEIVE_ERROR                    -37
#define RMD_MESSAGE_NOT_SUPPORTED                   -38
#define RMD_DATA_NOT_READY			                -39
#endif
