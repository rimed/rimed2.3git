#pragma once
#include <cassert>
#include <string>


#include "NativeInfrastructureStdAfx.h"
#include "ErrorCodes.h"


//debug level bits
#define COMM_LAYER_TRC		0x01
#define ERROR_TRC		    0x02
#define INFRA_TRC			0x04
#define INFO_TRC			0x08
#define LOUD_TRC			0x10
#define LOW_LEVEL_TRC_LAST	0x20

#define MIN_DBG_LINE_LEN 120
#define MAX_DBG_LINE_LEN 1024
#define LOG_FILE_PATH "C:\\Rimed\\SRC\\TCD2003\\GUI\\Logs"
#define LOG_FILE_NAME "Rimed.TCD.Rev6.Comm.LOG"

typedef struct _DBG_TRACEREFIX 
{
	const char* szPrefix;
}DBG_TRACEREFIX;

#define DBG_ASSERT(DebugInst,_Expression) { \
	if(_Expression == FALSE) {\
		(DebugInst->DbgAssert(#_Expression,  __FILE__, __LINE__)); \
		assert(_Expression); \
	} \
}
#define DBG_TRACE(DebugInst, Level, StrFormat, ...) {\
	if(((DebugInst->GetDebugLevel()) & Level) == Level) {\
	DebugInst->DbgTrace(Level, StrFormat, ##__VA_ARGS__); \
	}}

namespace Rimed
{
	namespace TCD 
{
	namespace Infrastructure
	{
		class CRmdDebug 
		{
			public:
				#pragma region Constructors
				CRmdDebug ();
				~CRmdDebug ();
			 #pragma endregion
				//void DebugTraceStart(const char *szLogFileName);
				//void DebugTraceStop();

				//debuger 
				bool DbgAssert (const char* szMessage, const char *FileName, int LineNum);
				void SetDebugLevel (unsigned int uLevel)	{m_uLevel = uLevel;}
				unsigned int GetDebugLevel(void);
				void DbgTrace(unsigned int uLevel, const char *pszFmt, ...);
			private:
				unsigned int m_uLevel;
				void writeToErrorLog(const char* szMsg);
		};

		class CRmdException : public std::exception 
		{
			public:
				DWORD m_dwErrorCode;
				DWORD m_dwOriginalCode;
				DWORD m_dwLine;
				CHAR m_acFileName[100];

				CRmdException(CONST CRmdException& Other);

				CRmdException(DWORD dwErrorCode, DWORD dwLine, CONST CHAR* pcFileName);	

				CRmdException( DWORD dwErrorCode, DWORD dwOriginalCode, DWORD dwLine, CONST CHAR* pcFileName);

				virtual ~CRmdException();

			private:
				VOID Init(DWORD dwErrorCode, DWORD dwOriginalCode, DWORD dwLine, CONST CHAR* pcFileName);
				
				VOID writeToErrorLog(CONST CHAR* szMsg);

				CRmdException& operator=(CONST CRmdException&);
		};
	};
}
}