#include "RmdSync.h"
#include "RmdDebug.h"

using namespace Rimed::TCD::Infrastructure;

#define THREAD_INITIALIZATION_TIMEOUT  2000

/*
//
// CRmdMutex
// 
CRmdMutex::CRmdMutex(Infra *pInfra, CRmdDebug *pRmdDebug)
{
	m_pRmdDebug = pRmdDebug;
	m_pInfra =pInfra;
	m_Label = m_pInfra->InfraMutexRegister();
	m_hMutex = NULL;
	m_ulAcquire = 0;
}

CRmdMutex::~CRmdMutex()
{
	BOOL boRet;
	for (int i=0;i<m_ulAcquire; i++)
	{
		try{ RmdMutexRelease(); } catch(...){}
	}

	DBG_ASSERT(m_pRmdDebug,m_ulAcquire==0);		//OFER: sometime Assert fail m_ulAcquire==1

	if(m_hMutex!=NULL) 
	{
		boRet = CloseHandle (m_hMutex);
		m_hMutex = NULL;
		//DBG_ASSERT(m_pRmdDebug,boRet);	//OFER 2013.04.11
	}
	DBG_TRACE (m_pRmdDebug,INFRA_TRC,"<-~CRmdMutex");
	m_pInfra->InfraMutexDeregister();
}

void CRmdMutex::RmdMutexCreate (int *Label)
{
	m_hMutex = CreateMutex(NULL,0, NULL);
	DBG_ASSERT(m_pRmdDebug,m_hMutex);
	*Label = m_Label;
}

RMD_ERROR_CODE CRmdMutex::RmdMutexAcquire(DWORD dwMilliseconds)
{
	DWORD dWaitResult;
	DBG_ASSERT(m_pRmdDebug, m_hMutex);
	dWaitResult = WaitForSingleObject (m_hMutex,dwMilliseconds);
	switch (dWaitResult) 
	{
		// The thread got ownership of the mutex
	case WAIT_OBJECT_0:
		m_ulAcquire++;
		return RMD_SUCCESS;
	default:
	case WAIT_ABANDONED: 
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "Congratulations! You get the deadlock dWaitResult=%d", dWaitResult);
		//DBG_ASSERT(m_pRmdDebug,0);
		return RMD_FAILED;
	}
}

void CRmdMutex::RmdMutexRelease (void)
{
	BOOL boRet = ReleaseMutex (m_hMutex);
	DWORD dwRet;
	if(!boRet) 
	{
		dwRet = GetLastError();
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdMutex::RmdMutexRelease: ReleaseMutex failed %d,m_ulAcquire=%d, label=%d ", dwRet, m_ulAcquire, m_Label);
	}

	//DBG_ASSERT(m_pRmdDebug,boRet);	//OFER 2013.04.11
	m_ulAcquire--;
}
*/

//
// CRmdEvent
//
CRmdEvent::CRmdEvent(Infra *pInfra, CRmdDebug *pRmdDebug)
{
	m_pInfra =pInfra;
	m_pRmdDebug = pRmdDebug;
	m_pInfra->InfraEventRegister();
	m_hEvent = NULL;
	m_Label = 0xffff;
	m_WaitCount = 0;
}

void CRmdEvent::RmdEventCreate(bool boManualReset, bool boInitialState, int nEventLabel)
{
	m_Label = nEventLabel;
	m_hEvent = CreateEvent (NULL,TRUE,FALSE,NULL);
	DBG_ASSERT(m_pRmdDebug,m_hEvent!=NULL);
}	

CRmdEvent::~CRmdEvent()
{
	BOOL boRet;
	if(m_hEvent == NULL)
		return;

	boRet = CloseHandle (m_hEvent);
	if (!boRet)
		DBG_TRACE(m_pRmdDebug,ERROR_TRC,"CRmdEvent::~CRmdEvent(): CloseHandle() FAIL.");

	m_hEvent = NULL;
	m_pInfra->InfraEventDeregister();
}

void CRmdEvent::RmdEventSet(void)
{
	if (m_hEvent == NULL)		//Ofer, 2013.10.03
		return;

	//DBG_ASSERT(m_pRmdDebug,m_hEvent!=NULL);	//Ofer, 2013.10.03

	BOOL boRet = SetEvent(m_hEvent);
	DBG_ASSERT(m_pRmdDebug,boRet);	
}

void CRmdEvent::RmdEventReset(void)
{
	if (m_hEvent == NULL)		//Ofer, 2013.10.03
		return;

	BOOL boRet = ResetEvent(m_hEvent);
	DBG_ASSERT(m_pRmdDebug,boRet);
}

DWORD	CRmdEvent::RmdEventWait(DWORD dwMilliseconds)
{
	//DBG_TRACE (m_pRmdDebug,INFO_TRC,"CRmdEvent::RmdEventWait() ----> ENTER");

	DWORD dwRet;
	m_WaitCount++;
	dwRet = WaitForSingleObject(m_hEvent, dwMilliseconds);
	m_WaitCount--;

	//DBG_TRACE (m_pRmdDebug,INFO_TRC,"CRmdEvent::RmdEventWait() <---- LEAVE");

	return dwRet;
}


//
// CRmdThread
// 
CRmdThread::CRmdThread(Infra *pInfra, CRmdDebug *pRmdDebug)
{
	m_dwThreadId = -1;	//-1: NOT STARTED

	m_pRmdDebug = pRmdDebug;
	m_pInfra =pInfra;
	m_pInfra->InfraThreadRegister();
	m_boAtFirstInit = FALSE;
	INFRA_NEW_OBJ(m_pInfra, m_pInitEvent, CRmdEvent, m_pInfra, m_pRmdDebug);
	INFRA_NEW_OBJ(m_pInfra, m_pStopEvent, CRmdEvent, m_pInfra, m_pRmdDebug);
	INFRA_NEW_OBJ(m_pInfra, m_pMessageEvent, CRmdEvent, m_pInfra, m_pRmdDebug);
	m_pendingMessages = 0;

	DBG_TRACE(m_pRmdDebug,INFO_TRC,"CRmdThread::CRmdThread()");
}

CRmdThread::~CRmdThread(void)
{
	DBG_TRACE(m_pRmdDebug,INFO_TRC,"CRmdThread::~CRmdThread(). ThreadId=%d", m_dwThreadId);

	BOOL boRet = CloseHandle(m_hThread);
	if(!boRet)
		DBG_TRACE(m_pRmdDebug, ERROR_TRC,"CRmdThread::~CRmdThread: CloseHandle() FAIL. ThreadId=%d", m_dwThreadId);

	m_hThread = NULL;
	m_pInfra->InfraThreadDeregister();
	INFRA_DELETE_OBJ(m_pInfra, m_pInitEvent, CRmdEvent);
	INFRA_DELETE_OBJ(m_pInfra, m_pStopEvent, CRmdEvent);
	INFRA_DELETE_OBJ(m_pInfra, m_pMessageEvent, CRmdEvent);
	//DBG_ASSERT(m_pRmdDebug,m_eStatus==RMD_THREAD_NOT_INITIALIZED);
}

unsigned long CRmdEvent::RmdWaitForMultipleEvents(int nCount, CRmdEvent **pObjs,BOOL boWaitAll, unsigned long uMilliseconds)
{
	HANDLE hHandles[512] = {0};
	DWORD dwRet;
	int i;
	for ( i=0;i<512 && i< nCount;i++)
	{
		hHandles[i] = pObjs[i]->m_hEvent;
		pObjs[i]->m_WaitCount++;
	}

	if(nCount >= 512)
		nCount = 512;
	
	dwRet = WaitForMultipleObjects  (nCount,hHandles,boWaitAll,uMilliseconds);

	for ( i=0;i<512 && i< nCount;i++)
		pObjs[i]->m_WaitCount--;

	return dwRet;
}

void CRmdThread::RmdThreadStart (LPTHREAD_START_ROUTINE pfnThreadFn, void *pParameter, DWORD *dwThreadId)
{
	DBG_TRACE(m_pRmdDebug, INFO_TRC,"CRmdThread::RmdThreadStart(): ENTER.");

	DWORD dwRet;
	m_pStopEvent->RmdEventCreate(TRUE, FALSE,1);
	m_pMessageEvent->RmdEventCreate(TRUE, FALSE,2);
	m_pInitEvent->RmdEventCreate(TRUE,FALSE,3);
	m_pfnThreadFn = pfnThreadFn;
	SECURITY_ATTRIBUTES ThreadAttributes = {0};
	ThreadAttributes.nLength = sizeof ThreadAttributes;
	ThreadAttributes.bInheritHandle = TRUE;
	m_hThread =  CreateThread (&ThreadAttributes,0,pfnThreadFn, pParameter,0, &m_dwThreadId);
	DBG_ASSERT(m_pRmdDebug,m_hThread);

	dwRet = m_pInitEvent->RmdEventWait(THREAD_INITIALIZATION_TIMEOUT);
	if (dwRet != WAIT_OBJECT_0)
		DBG_TRACE(m_pRmdDebug,ERROR_TRC,"CRmdThread::RmdThreadStart(): ThreadId=%d - Initialization error=%d", m_dwThreadId, dwRet);

	DBG_ASSERT(m_pRmdDebug,dwRet == WAIT_OBJECT_0);

	*dwThreadId = m_dwThreadId;

	DBG_TRACE(m_pRmdDebug,INFO_TRC,"CRmdThread::RmdThreadStart(): LEAVE. ThreadId=%d", m_dwThreadId);
}

void CRmdThread::RmdThreadStop()
{
	DBG_TRACE(m_pRmdDebug,INFO_TRC,"CRmdThread::RmdThreadStop(): ThreadId=%i.",m_dwThreadId);
	
	RmdPostMsg(RMD_MSG_KILL,0,0);
	m_pStopEvent->RmdEventSet();
}

void CRmdThread::RmdThreadFinalize (unsigned long ulTimeOut)
{
	DBG_TRACE(m_pRmdDebug, INFO_TRC, "CRmdThread::RmdThreadFinalize(): ThreadId=%d", m_dwThreadId);

	DWORD dWaitResult;
	dWaitResult = WaitForSingleObject(m_hThread,ulTimeOut);
	//DBG_ASSERT(m_pRmdDebug,dWaitResult == WAIT_OBJECT_0);

	if (dWaitResult != WAIT_OBJECT_0)
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdThread::RmdThreadFinalize(): dWaitResult=%i.", dWaitResult);
}	

//void CRmdThread::RmdThreadExit (unsigned long ulTimeOut)

E_RMD_THREAD_STATUS CRmdThread::RmdRunTimeFn (unsigned long uWaitEventMsecs)
{
	E_RMD_THREAD_STATUS eStatus = RMD_THREAD_RUNNING;

	if (!m_boAtFirstInit)
	{
		MSG msg = {0};
		PeekMessage(&msg, NULL, WM_USER, WM_USER, PM_NOREMOVE);
		m_pInitEvent->RmdEventSet();
		m_boAtFirstInit = true;
	}

	CRmdEvent *aRmdEvent[] ={m_pStopEvent,m_pMessageEvent};
	unsigned long uWaitResult = m_pStopEvent->RmdWaitForMultipleEvents(2,aRmdEvent,false,uWaitEventMsecs);
	if (uWaitResult >=WAIT_OBJECT_0  && uWaitResult <= WAIT_OBJECT_0 -1)
	{
		switch (uWaitResult- WAIT_OBJECT_0)
		{
			case 0: //StopEvent
					DBG_TRACE(m_pRmdDebug, INFO_TRC, "CRmdThread::RmdRunTimeFn(): RMD_THREAD_STOP. ThreadId=%d", m_dwThreadId);
					eStatus = RMD_THREAD_STOP;
				break;
			case 1: //MessageEvent
					eStatus = RMD_THREAD_RUNNING;
					m_pMessageEvent->RmdEventReset();
				break;
			default:
					eStatus = RMD_THREAD_RUNNING;
				break;
		}
	}

	return eStatus;
}

void CRmdThread::RmdPostMsg (unsigned int uMsg, unsigned int  uParam, unsigned long  ulParam)
{

	if (m_pendingMessages > 4500 && (uMsg == RMD_MSG_EMPTY || uMsg == 1226))	//1226 = RMD_MSG_TCP_SERVER_SEND
	{
		//DBG_TRACE(m_pRmdDebug, INFO_TRC, "CRmdThread::RmdPostMsg(): m_dwThreadId=%i, uMsg=%i, uParam=%i, ulParam=%i. Pending messages=%i. Message aborted.", m_dwThreadId, uMsg, uParam, ulParam, m_pendingMessages);
		return;
	}

	int iRet;
	BOOL boRet = PostThreadMessage(m_dwThreadId,uMsg,uParam,ulParam);
	if(!boRet) 
	{
		iRet = GetLastError();
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "CRmdThread::RmdPostMsg(): Err=%i, m_dwThreadId=%i, uMsg=%i, uParam=%i, ulParam=%i. Pending messages=%i.",iRet, m_dwThreadId, uMsg, uParam, ulParam, m_pendingMessages);
	}
	else
	{
		try
		{
			InterlockedIncrement(&m_pendingMessages);
		}
		catch (...)	{}

		m_pMessageEvent->RmdEventSet();
	}
}

BOOL CRmdThread::RmdPeekMsg (unsigned int* puMsg, unsigned int *puParam, unsigned long *pulParam)
{
	MSG Msg = {0};
	BOOL retCode = PeekMessage (&Msg,(HWND)-1,0,0,PM_REMOVE);
	
	*puMsg		= Msg.message;
	*puParam	= Msg.wParam;
	*pulParam	= Msg.lParam;

	if (retCode)
	{
		try
		{
			InterlockedDecrement(&m_pendingMessages);
		}
		catch (...)	{}
	}

	return retCode;
}