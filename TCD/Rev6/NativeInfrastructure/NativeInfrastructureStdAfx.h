#pragma once

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#include <windows.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <io.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>

#ifdef COMMUNICATION_LAYER_EXPORTS
#define RMD_FNAPI __declspec(dllexport) __stdcall
#else
#define RMD_FNAPI __declspec(dllimport)
#endif