#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

#include "Infra.h"
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdio.h>

using namespace Rimed::TCD::Infrastructure;

Infra::~Infra ()
{
	_CrtDumpMemoryLeaks();

	//DBG_TRACE (m_pRmdDebug,INFRA_TRC,"Infra::Resouce in use: Threads %d MemoryAllocations %d Handles %d Classes %d Events %d Mutexes %d",  m_ThreadInUse, m_MemoryInUse, m_HandlesInUse, m_EventsInUse, m_ClassesInUse, m_MutexInUse);	//Ofer 2013.10.03
}

Infra::Infra (CRmdDebug *RmdDebug)
{
	m_pRmdDebug = RmdDebug;
	m_ThreadInUse =0;
	m_MemoryInUse =0;
	m_HandlesInUse =0;
   m_MutexInUse = 0;
	m_ClassesInUse = 0;
	m_EventsInUse =0;	
}

CRmdDebug *Infra::GetDebugInst()
{
	return m_pRmdDebug;
}

RMD_ERROR_CODE Infra::SocketLibStart(void)
{
	WORD wVersionRequested;
	WSADATA wsaData;
	int err;

	/* Use the MAKEWORD(lowbyte, highbyte) macro declared in Windef.h */
	wVersionRequested = MAKEWORD(2, 2);

	err = WSAStartup(wVersionRequested, &wsaData);
	if (err != 0) 
	{
		/* Tell the user that we could not find a usable Winsock DLL.                                  */
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "Infra::SocketLibStart(): WSAStartup failed with error: %d", err);
		
		return RMD_ABNORMAL_KERNEL_EXECUTION;
	}

	/* Confirm that the WinSock DLL supports 2.2.*/
	/* Note that if the DLL supports versions greater    */
	/* than 2.2 in addition to 2.2, it will still return */
	/* 2.2 in wVersion since that is the version we      */
	/* requested.                                        */

	if (LOBYTE(wsaData.wVersion) != 2 || HIBYTE(wsaData.wVersion) != 2) 
	{
		/* Tell the user that we could not find a usable WinSock DLL.                                  */
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "Infra::SocketLibStart(): Could not find a usable version of Winsock.dll");
		WSACleanup();
		//throw CRmdException(RMD_ABNORMAL_KERNEL_EXECUTION, err, __LINE__, __FILE__);	//Ofer, 2013.12.29
		return RMD_ABNORMAL_KERNEL_EXECUTION;
	}
	
	DBG_TRACE(m_pRmdDebug, INFRA_TRC,"Infra::SocketLibStart(): The Winsock 2.2 dll was found okay");

	return RMD_SUCCESS;
};

RMD_ERROR_CODE Infra::SocketLibStop(void)
{
	int err = WSACleanup();
	if (err != 0) 
	{
		// Fail to find a usable Winsock DLL.                                  
		DBG_TRACE(m_pRmdDebug, ERROR_TRC, "Infra::SocketLibStop: WSACleanup failed with error: %d", err);
		return RMD_ABNORMAL_KERNEL_EXECUTION;
	}
	return RMD_SUCCESS;
}

void Infra::RegClass (const char *szClassName)
{
	m_ClassesInUse++;
	DBG_TRACE(m_pRmdDebug,INFRA_TRC,"Infra::RegClass(): class %s registered. Class counter is %d",szClassName,m_ClassesInUse);
}


void Infra::DeregClass (const char *szClassName)
{
	m_ClassesInUse--;
	DBG_TRACE(m_pRmdDebug,INFRA_TRC,"Infra::DeregClass(): class %s de-registered. Class counter is %d",szClassName,m_ClassesInUse);
}

void* Infra::InfraMalloc (unsigned int Size)
{
	void *ret = malloc(Size);
	if (ret)
	{
		m_MemoryInUse += Size;
		DBG_TRACE(m_pRmdDebug,INFRA_TRC,"Infra::InfraMalloc() memory block of %u bytes allocated", Size);
	}
	
	return ret;
}

void Infra::InfraFree (void *pMemory, unsigned int StatisticSize)
{
	free(pMemory);

	//v2.0.5.1, Ofer if(m_MemoryInUse >=StatisticSize) 
	//v2.0.5.1, Ofer	DBG_TRACE (m_pRmdDebug, INFRA_TRC,"Infra::InfraFree(): memory block of %ld bytes freed", StatisticSize);
	//v2.0.5.1, Ofer else
	//v2.0.5.1, Ofer 	DBG_TRACE (m_pRmdDebug, ERROR_TRC,"Infra::InfraFree(): memory block of %ld MemSz=%ld", StatisticSize, m_MemoryInUse);
	m_MemoryInUse -=StatisticSize;
}

int Infra::InfraMutexRegister(void)
{
	m_MutexInUse++;
	return m_MutexInUse;
}

void Infra::InfraMutexDeregister(void)
{
	DBG_ASSERT(m_pRmdDebug, m_MutexInUse);
	m_MutexInUse--;
}

void Infra::InfraEventRegister(void)
{
	//pRmdEvent->RmdEventCreate(boManualReset, boInitialState, nEventLabel);
	m_EventsInUse++;
}

void Infra::InfraEventDeregister(void)
{
	DBG_ASSERT(m_pRmdDebug, m_EventsInUse);
	m_EventsInUse--;
	DBG_TRACE (m_pRmdDebug,INFRA_TRC," Infra::InfraEventDelete Event counter %d",m_EventsInUse);
}

void Infra::InfraThreadRegister()
{
	//v2.0.5.1, Ofer DBG_TRACE (m_pRmdDebug,INFRA_TRC,"CRmdThread::RmdThreadStart created new thread. Thread counter %d",m_ThreadInUse);
	m_ThreadInUse++;
}

void Infra::InfraThreadDeregister()
{
	m_ThreadInUse--;
	//v2.0.5.1, Ofer DBG_TRACE (m_pRmdDebug,INFRA_TRC,"Infra::CloseThreadHandle. Thread counter %d",m_ThreadInUse);
}

