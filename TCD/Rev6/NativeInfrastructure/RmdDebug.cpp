#include "RmdDebug.h"

using namespace Rimed::TCD::Infrastructure;

CRmdDebug::CRmdDebug()
{
	char szTimeStampAndStart[MIN_DBG_LINE_LEN] = {0};
	//m_fpTrace = NULL;
	//m_DspLog = NULL;
	m_uLevel = 0xffffffff;

	writeToErrorLog("\t\t--------------------- START LOG ---------------------\n");
}

CRmdDebug::~CRmdDebug()
{
	writeToErrorLog("\t\t--------------------- END   LOG ---------------------\n\n");
	//DebugTraceStop();
}

//void CRmdDebug::DebugTraceStop(void)
//{
	//v2.0.5.1, Ofer DBG_TRACE(this, INFO_TRC,"Stop tracing");
	
	//if (m_fpTrace) 
	//{
	//	try	{fclose (m_fpTrace);} catch (...) {}
	//	m_fpTrace = NULL;
	//}

	//if(m_DspLog) 
	//{
	//	try	{fclose(m_DspLog);} catch (...) {}
	//	m_DspLog = NULL;
	//}
//}

bool CRmdDebug::DbgAssert (const char* szMessage, const char *FileName, int LineNum)
{
	DbgTrace(ERROR_TRC, "ASSERT: %s (File: %s, Line: %d)", szMessage, FileName, LineNum);
	//DebugTraceStop();

	return true;
}

//void CRmdDebug::DebugTraceStart (const char *szFileName)
//{
	//errno_t errnoRet = fopen_s (&m_fpTrace, TRACE_LOG_FILE, "w");
	//if (m_fpTrace == NULL)
	//	DBG_TRACE(this, ERROR_TRC,"CRmdDebug::DebugTraceStart(). Fail to open '%s' trace file.", TRACE_LOG_FILE);
//}


#pragma warning(push)
#pragma warning(disable : 4793)
#pragma warning(disable : 4996)

void CRmdDebug::DbgTrace(unsigned int uLevel, const char *pszFmt, ...)
{
	va_list ptr; 
	char szResultBuffer[MAX_DBG_LINE_LEN];
	
	unsigned long ulOffset = _snprintf (szResultBuffer, MAX_DBG_LINE_LEN,"[RIMED] ");
	if(uLevel == ERROR_TRC)
		ulOffset += _snprintf (&szResultBuffer[ulOffset], MAX_DBG_LINE_LEN,"ERROR: ");

	va_start(ptr, pszFmt);
	ulOffset += vsnprintf (&szResultBuffer[ulOffset],MAX_DBG_LINE_LEN-ulOffset,pszFmt,ptr);
	va_end(ptr);

	sprintf (&szResultBuffer[ulOffset], "\n");
 	
	OutputDebugString(szResultBuffer);

	writeToErrorLog(&szResultBuffer[8]);	//8: skip "[RIMED] " prefix
}

void CRmdDebug::writeToErrorLog(const char* szMsg)
{
	SYSTEMTIME st;
	GetSystemTime (&st);
	char filename[MAX_PATH];
	char szLogRow[MAX_DBG_LINE_LEN];

	try	
	{
		sprintf_s(filename,"%s\\%s", LOG_FILE_PATH, LOG_FILE_NAME);
		unsigned int ulOffset = _snprintf(szLogRow, MAX_DBG_LINE_LEN,"%02d:%02d:%02d.%03d> %s", st.wHour, st.wMinute, st.wSecond, st.wMilliseconds, szMsg);

		FILE *pErrFile;
		fopen_s(&pErrFile, filename, "a");	
		if (pErrFile != NULL)
		{
			fputs(szLogRow, pErrFile);
			fclose(pErrFile);
		}
	} 
	catch (...) 
	{}
}

#pragma warning(pop)



unsigned int CRmdDebug::GetDebugLevel(void)
{
	return m_uLevel;
}

CRmdException::CRmdException(CONST CRmdException& Other)
{
	Init(Other.m_dwErrorCode, Other.m_dwOriginalCode, Other.m_dwLine, Other.m_acFileName);     
}

CRmdException::CRmdException(
	DWORD dwErrorCode,
	DWORD dwOriginalCode,
	DWORD dwLine,
	CONST CHAR* pcFileName)	:exception()
{
	Init(dwErrorCode, dwOriginalCode, dwLine, pcFileName);     
}

CRmdException::CRmdException(
	DWORD dwErrorCode, 
	DWORD dwLine, 
	CONST CHAR* pcFileName)
	:exception()
{   
	Init(dwErrorCode, 0, dwLine, pcFileName);
}

VOID CRmdException::Init(
	DWORD dwErrorCode,
	DWORD dwOriginalCode,
	DWORD dwLine,
	CONST CHAR* pcFileName)
{
	try 
	{
		this->m_dwErrorCode = dwErrorCode;
		this->m_dwOriginalCode = dwOriginalCode;
		this->m_dwLine	= dwLine;	

		if (pcFileName == pcFileName)
			return;

		if (pcFileName != NULL)
			strcpy_s(this->m_acFileName, 100, pcFileName);
		else
			strcpy_s(this->m_acFileName, 100, "UNKNOWN\n");
	}
	catch (...) {}
}

CRmdException::~CRmdException()
{
	;
}