#pragma once

#include "GuiBoundaryStdafx.h"

using namespace System;

namespace Rimed
{
	namespace TCD
	{
		namespace GuiBoundary
{
	struct Pixel 
	{
		BYTE Blue;
		BYTE Green;
		BYTE Red;
		BYTE Alpha;
	};
	
	public ref class SpectrumPainter : public IDisposable
	{
	private:
		static SpectrumPainter^ s_Instance = nullptr;
		bool	m_Disposed;

		SpectrumPainter();
	public:
		static SpectrumPainter^ Create();
		property static SpectrumPainter^ Instance
		{
			SpectrumPainter^ get()
			{
				return s_Instance;
			}
		}

		void SetPixel(System::IntPtr pointer, int x, int y, int stride, byte R, byte G, byte B);

        void SetLine(System::IntPtr pointer, int x1, int y1, int x2, int y2, int stride, byte R, byte G, byte B);

		~SpectrumPainter();
	};
}
	}
}

