﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Rimed.Tools.ScreenCapture;
using ScreenCapture_MedSim;

// Added by Alex 22/11/2015 feature #672 v 2.2.3.11
namespace Rimed.RecWMV
{
    public partial class Form1 : Form
    {
        private const string OUTPUTFILE = @"OutputFile.wmv";
        private IScreenCapture myEncoder = new ScreenCaptureAlex();
        private System.Timers.Timer MyTimer { get; set; }

        private string patientId = "";
        private string patientName = "";

        public Form1()
        {
            InitializeComponent();
            Thread oThread = new Thread(ThreadProc);
            oThread.SetApartmentState(ApartmentState.STA);
            oThread.IsBackground = true;
            oThread.Start();

            MyTimer = new System.Timers.Timer();
            MyTimer.Interval = 100;
            MyTimer.Elapsed += new System.Timers.ElapsedEventHandler(myTimer_Elapsed);

            myEncoder.OnProgressChange += myEncoder_OnProgressChange;
            myEncoder.OnFinished += myEncoder_OnFinished;
        }

        void myEncoder_OnFinished(object sender, EventArgs e)
        {
//)>        v.2.02.05.001 Updated MediaRecorder feature.
            if (InvokeRequired)
            {
                Invoke(new Action(() => myEncoder_OnFinished(sender, e)));
                return;
            }
            TopMost = false;
            Application.DoEvents();
//

            if (myEncoder.GetType() == typeof(ScreenCaptureAlex))
            {
                label1.Visible = false;
                pictureBox1.Visible = false;
            }
            if (MessageBox.Show("Do you want to save this video?", "Video", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                string pref = (patientName.IndexOf('*') > -1 ? patientId : patientName);
                pref =
                    pref.Replace('/', '_')
                        .Replace(':', '_')
                        .Replace('*', '_')
                        .Replace('?', '_')
                        .Replace('"', '_')
                        .Replace('<', '_')
                        .Replace('>', '_')
                        .Replace('|', '_')
                        .Trim();

                SaveFileDialog sfd = new SaveFileDialog();
                sfd.FileName = pref + "_" + DateTime.Now.ToString("ddMMyy") + ".wmv";
                sfd.Filter = "Windows Media Video  files (*.wmv)|*.wmv";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    File.Copy(OUTPUTFILE, sfd.FileName, true);
                }
                progressBar2.Value = 0;
                progressBar3.Value = 0;
            }
            // Added by Alex 31/03/2016 bug #839 v 2.2.3.26
            WindowState = FormWindowState.Minimized;
            TopMost = false;
        }

        void myEncoder_OnProgressChange(int encode, int decompress, int merge)
        {
//)>        v.2.02.05.001 Updated MediaRecorder feature.
            //progressBar2.Value = decompress;
            //progressBar3.Value = merge;
            if (InvokeRequired)
            {
                Invoke(new Action(() => myEncoder_OnProgressChange(encode, decompress, merge)));
                return;
            }
            if (encode > progressBar2.Maximum)
            {
                encode = progressBar2.Maximum - 1;
            }
            progressBar2.Value = encode;
//)>         
        }


        void myTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
             Close();
        }


        private void ThreadProc()
        {
            string standard_output;
             
            while (true)
            {
                standard_output = Console.ReadLine();
                if (standard_output != null)
                {
                    string[] data = standard_output.Split(';');
                    switch (data[0])
                    {
                        case "START":
                            string res = myEncoder.Start(OUTPUTFILE);
                            if (res != "")
                                MessageBox.Show(res, "Recording WMV", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                        case "STOP":
                            if (data.Length > 1)
                            {
                                try
                                {
                                    if (myEncoder.GetType() == typeof(ScreenCaptureAlex))
                                    {
//)>                                    v.2.02.05.001 Updated MediaRecorder feature.
                                        //label1.Visible = true;
                                        //pictureBox1.Visible = true;
                                        label1.Invoke(new Action(() => label1.Visible = true));
                                        progressBar2.Invoke(new Action(() => progressBar2.Visible = true));                                        
//)>
                                    }
                                    patientName = data[1];
                                    patientId = data[2];
                                    StartPosition = FormStartPosition.CenterScreen;
                                    WindowState = FormWindowState.Normal;
                                    TopMost = true;

                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(ex.Message);
                                }
                            }
                            myEncoder.Stop();
                            break;
                        case "SAVE":
                            File.Copy(OUTPUTFILE, data[1], true);
                            break;
                        // Added by Alex 31/03/2016 bug #839 v 2.2.3.26
                        case "HIDE":
                            WindowState = FormWindowState.Minimized;
                            TopMost = false;
                            break;
                        case "CLOSE":
                            goto done;

                    }
                }
                 Thread.Sleep(100);
            }
            done: MyTimer.Start();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string res = myEncoder.Start(OUTPUTFILE);
            if (res != "")
                MessageBox.Show(res, "Recording WMV", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            myEncoder.Stop();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            button1.Visible = ConfigurationManager.AppSettings["HideButtons"].ToLower() != "true";
            button2.Visible = button1.Visible;
            label2.Visible = ConfigurationManager.AppSettings["HideProgressBar"].ToLower() != "true";
            label3.Visible = label2.Visible;
            progressBar2.Visible = label2.Visible;
            progressBar3.Visible = label2.Visible;
//)>        v.2.02.05.001 Updated MediaRecorder feature. Progress bar was added.
            progressBar2.Visible = true;
//)>
        }
    }
}
