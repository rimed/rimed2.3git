﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;

namespace Rimed.RecWMV
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
//)>        v.2.02.05.001 Updated MediaRecorder feature. 
            var currProcess = Process.GetCurrentProcess();
            long AffinityMask = (long)currProcess.ProcessorAffinity;
            AffinityMask &= 0x000F; // use only any of the first 4 available processors
            currProcess.ProcessorAffinity = (IntPtr)AffinityMask;
            currProcess.PriorityClass = ProcessPriorityClass.AboveNormal;

            // Set application main thread (GUI thread) name and priority.
            var guiThread = Thread.CurrentThread;
            guiThread.Priority = ThreadPriority.AboveNormal;
//)>

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
